

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head id="ctl00_mainHeader"><title>
	Metal, Plastic, and Ceramic Search Index
</title><meta http-equiv="Content-Style-Type" content="text/css" /><meta http-equiv="Content-Language" content="en" />

        <script src="/includes/flash.js" type="text/javascript"></script>

        <script src="/includes/prototype.js" type="text/javascript"></script>
        <!-- script src="/includes/scriptaculous/effects.js" type="text/javascript"></script -->
        <script src="/includes/matweb.js" type="text/javascript"></script>
        <link rel="stylesheet" href="/includes/main.css" />

        <script type="text/javascript">

                function _onload(){
                        matweb.debug = false; // ;
                        //matweb.writeOverlay();
                }

                function _onkeydown(evt){
                        //PREVENT GLOBAL AUTO-SUBMIT OF FORMS
                        return matweb.DisableAutoSubmit(evt);
                }

                //EXAMPLE USAGE:
                //matweb.register_onload(function(){alert("hello from onload event");});
                //matweb.writeOverlay();


        </script>

        <style type="text/css" media="screen">
        @import url("/ad_IES/css/styles.css");
        </style>
<meta name="KEYWORDS" content="carbon steels, low alloy steels, corrosion resistant stainless, high temperature stainless, heat resistant stainless steels, AISI steel alloy specifications, high strength steel alloys, tensile strength of steels, maraging, superalloy, lead alloy, magnesium, tin alloy, tungsten, zinc, precious metal, pure metallic element, TPE elastomer, dielectric constant, plastic dissipation factor, nylon glass transition temperature, ionomer vicat softening point, poisson's ratio, polyetherimide, fluoropolymer, silicone, polyester dielectric constant" /><meta name="DESCRIPTION" content="Searchable list of plastics, metals, and ceramics categorized into a fields like polypropylene, nylon, ABS, aluminum alloys, oxides, etc.  Search results are detailed data sheets with tensile strength, density, dielectric constant, dissipation factor, poisson's ratio, glass transition temperature, and Vicat softening point." /><meta name="ROBOTS" content="NOFOLLOW" /><style type="text/css">
	.ctl00_ContentMain_ucMatGroupTree_msTreeView_0 { text-decoration:none; }
	.ctl00_ContentMain_ucMatGroupTree_msTreeView_1 { color:Black; }
	.ctl00_ContentMain_ucMatGroupTree_msTreeView_2 { padding:0px 3px 0px 3px; }

</style></head>

<body onload="_onload();" onkeydown="return _onkeydown(event);">

<a id="Top"></a>

<div id="divSiteName" style="position:absolute; top:0; left:0 ;">
<a href="/index.aspx"><img src="/images/sitelive.gif" alt="" /></a>
</div>

<div id="divPopupOverlay" style="display:none;"></div>

<div id="divPopupAlert" class="popupAlert" style="display:none;">
        <table style="width:100%; height:100%; border-style:outset; border-width:5px; border-color:blue;" >
                <tr><th style=" height:25px; background-color:Maroon; font-size:14px; color:White; "><span id="spanPopupAlertTitle">System Message</span></th></tr>
                <tr><td style=" vertical-align:top; padding:3px; background-color:White;" id="tdPopupAlertMessage" ></td></tr>
                <tr><td style=" height:25px; text-align:center; background-color:Silver;"><input type="button" id="btnPopupAlert" onclick="matweb.alertClose();" style="" value="OK" /></td></tr>
        </table>
</div>

<form name="aspnetForm" method="post" action="MaterialGroupSearch.aspx" onsubmit="javascript:return WebForm_OnSubmit();" id="aspnetForm">
<div>
<input type="hidden" name="__EVENTTARGET" id="__EVENTTARGET" value="" />
<input type="hidden" name="__EVENTARGUMENT" id="__EVENTARGUMENT" value="" />
<input type="hidden" name="__LASTFOCUS" id="__LASTFOCUS" value="" />
<input type="hidden" name="ctl00_ContentMain_ucMatGroupTree_msTreeView_ExpandState" id="ctl00_ContentMain_ucMatGroupTree_msTreeView_ExpandState" value="ccccccnc" />
<input type="hidden" name="ctl00_ContentMain_ucMatGroupTree_msTreeView_SelectedNode" id="ctl00_ContentMain_ucMatGroupTree_msTreeView_SelectedNode" value="" />
<input type="hidden" name="ctl00_ContentMain_ucMatGroupTree_msTreeView_PopulateLog" id="ctl00_ContentMain_ucMatGroupTree_msTreeView_PopulateLog" value="" />
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="/wEPDwULLTE2Nzc3MDc2MDAPZBYCZg9kFgICAQ9kFioCAQ9kFgJmDxYCHgRUZXh0BWdEYXRhIHNoZWV0cyBmb3Igb3ZlciA8c3BhbiBjbGFzcz0iZ3JlZXRpbmciPjE4MCwwMDA8L3NwYW4+ICBtZXRhbHMsIHBsYXN0aWNzLCBjZXJhbWljcywgYW5kIGNvbXBvc2l0ZXMuZAICD2QWAmYPFgIfAAUESE9NRWQCAw9kFgJmDxYCHwAFBlNFQVJDSGQCBA9kFgJmDxYCHwAFBVRPT0xTZAIFD2QWAmYPFgIfAAUJU1VQUExJRVJTZAIGD2QWAmYPFgIfAAUHRk9MREVSU2QCBw9kFgJmDxYCHwAFCEFCT1VUIFVTZAIID2QWAmYPFgIfAAUDRkFRZAIJD2QWAmYPFgIfAAUHQUNDT1VOVGQCCg9kFgJmDxYCHwAFBkxPRyBJTmQCCw8QZGQWAWZkAgwPZBYCZg8WAh8ABQhTZWFyY2hlc2QCDQ9kFgJmDxYCHwAFCEFkdmFuY2VkZAIOD2QWAmYPFgIfAAUIQ2F0ZWdvcnlkAg8PZBYCZg8WAh8ABQhQcm9wZXJ0eWQCEA9kFgJmDxYCHwAFBk1ldGFsc2QCEQ9kFgJmDxYCHwAFClRyYWRlIE5hbWVkAhIPZBYCZg8WAh8ABQxNYW51ZmFjdHVyZXJkAhMPZBYCAgEPZBYCZg8WAh8ABRlSZWNlbnRseSBWaWV3ZWQgTWF0ZXJpYWxzZAIVD2QWEAIDD2QWAmYPFgIfAAWXATxhIGhyZWY9Ii9jbGlja3Rocm91Z2guYXNweD9hZGRhdGFpZD0xMzUxIj48aW1nIHNyYz0iL2ltYWdlcy9hc3NldHMvbWV0YWxtZW4tTmlja2VsLnBuZyIgYm9yZGVyPTAgYWx0PSJNZXRhbG1lbiBTYWxlcyIgd2lkdGggPSAiNzI4IiBoZWlnaHQgPSAiOTAiPjwvYT5kAgQPZBYCAgIPFgIeBXN0eWxlBRlkaXNwbGF5Om5vbmU7d2lkdGg6MjUwcHg7FgYCAQ9kFgJmD2QWAmYPDxYCHwAFElVzZXIgSW5wdXQgUHJvYmxlbWRkAgMPDxYCHwBlZGQCBQ8WAh4FdmFsdWUFAk9LZAIFD2QWBAIBDw9kFgIfAQUMV2lkdGg6MzA0cHg7ZAICDxAWBB8BBStwb3NpdGlvbjphYnNvbHV0ZTtkaXNwbGF5Om5vbmU7V2lkdGg6MzA4cHg7HgRzaXplBQIxOGRkZAIGDw8WBB4IQ2xpZW50SUQCAR4KTGFuZ3VhZ2VJRAIBZBYCZg8UKwAJDxYOHhdQb3B1bGF0ZU5vZGVzRnJvbUNsaWVudGceEkVuYWJsZUNsaWVudFNjcmlwdGceCVNob3dMaW5lc2ceCE5vZGVXcmFwZx4NTmV2ZXJFeHBhbmRlZGQeDFNlbGVjdGVkTm9kZWQeCUxhc3RJbmRleAIIZBYIHgtOb2RlU3BhY2luZxsAAAAAAAAAAAEAAAAeCUZvcmVDb2xvcgojHhFIb3Jpem9udGFsUGFkZGluZxsAAAAAAAAIQAEAAAAeBF8hU0IChIAYZGRkZGRkFCsACQUjMjowLDA6MCwwOjEsMDoyLDA6MywwOjQsMDo1LDA6NiwwOjcUKwACFgwfAAUSQ2FyYm9uICg4NjYgbWF0bHMpHgVWYWx1ZQUDMjgzHghFeHBhbmRlZGgeDFNlbGVjdEFjdGlvbgsqLlN5c3RlbS5XZWIuVUkuV2ViQ29udHJvbHMuVHJlZU5vZGVTZWxlY3RBY3Rpb24CHgtOYXZpZ2F0ZVVybAVBamF2YXNjcmlwdDp1Y01hdEdyb3VwVHJlZV9Pbk5vZGVDbGljaygnQ2FyYm9uICg4NjYgbWF0bHMpJywnMjgzJykeEFBvcHVsYXRlT25EZW1hbmRnZBQrAAIWDB8ABRVDZXJhbWljICgxMDAwNCBtYXRscykfEQUCMTEfEmgfEwsrBAIfFAVDamF2YXNjcmlwdDp1Y01hdEdyb3VwVHJlZV9Pbk5vZGVDbGljaygnQ2VyYW1pYyAoMTAwMDQgbWF0bHMpJywnMTEnKR8VZ2QUKwACFgwfAAUSRmx1aWQgKDc1NjIgbWF0bHMpHxEFATUfEmgfEwsrBAIfFAU/amF2YXNjcmlwdDp1Y01hdEdyb3VwVHJlZV9Pbk5vZGVDbGljaygnRmx1aWQgKDc1NjIgbWF0bHMpJywnNScpHxVnZBQrAAIWDB8ABRNNZXRhbCAoMTcwNTIgbWF0bHMpHxEFATkfEmgfEwsrBAIfFAVAamF2YXNjcmlwdDp1Y01hdEdyb3VwVHJlZV9Pbk5vZGVDbGljaygnTWV0YWwgKDE3MDUyIG1hdGxzKScsJzknKR8VZ2QUKwACFgwfAAUnT3RoZXIgRW5naW5lZXJpbmcgTWF0ZXJpYWwgKDgwNjMgbWF0bHMpHxEFAzI4NR8SaB8TCysEAh8UBVZqYXZhc2NyaXB0OnVjTWF0R3JvdXBUcmVlX09uTm9kZUNsaWNrKCdPdGhlciBFbmdpbmVlcmluZyBNYXRlcmlhbCAoODA2MyBtYXRscyknLCcyODUnKR8VZ2QUKwACFgwfAAUVUG9seW1lciAoOTc2MzUgbWF0bHMpHxEFAjEwHxJoHxMLKwQCHxQFQ2phdmFzY3JpcHQ6dWNNYXRHcm91cFRyZWVfT25Ob2RlQ2xpY2soJ1BvbHltZXIgKDk3NjM1IG1hdGxzKScsJzEwJykfFWdkFCsAAhYMHwAFGFB1cmUgRWxlbWVudCAoNTA3IG1hdGxzKR8RBQMxODQfEmgfEwsrBAIfFAVHamF2YXNjcmlwdDp1Y01hdEdyb3VwVHJlZV9Pbk5vZGVDbGljaygnUHVyZSBFbGVtZW50ICg1MDcgbWF0bHMpJywnMTg0JykfFWhkFCsAAhYMHwAFJVdvb2QgYW5kIE5hdHVyYWwgUHJvZHVjdHMgKDM5OCBtYXRscykfEQUDMjc3HxJoHxMLKwQCHxQFVGphdmFzY3JpcHQ6dWNNYXRHcm91cFRyZWVfT25Ob2RlQ2xpY2soJ1dvb2QgYW5kIE5hdHVyYWwgUHJvZHVjdHMgKDM5OCBtYXRscyknLCcyNzcnKR8VZ2RkAgcPFgIeCWlubmVyaHRtbAUZTmlja2VsIEFsbG95ICgxMjEwIG1hdGxzKWQCDQ8PFgIeB1Zpc2libGVoZGQCDg8PFgIfF2dkFgQCAQ8PFgIfAAUEMTI0NWRkAgMPDxYCHwAFDE5pY2tlbCBBbGxveWRkAg8PDxYGHgtDdXJyZW50UGFnZQIFHg9DdXJyZW50U2VhcmNoSUQCsfODEx8XZ2QWBAICDw8WAh8AZWRkAgMPDxYCHxdnZBY+AgEPDxYCHwAFBDEyNDVkZAIDDxBkEBUFATEBMgEzATQBNRUFATEBMgEzATQBNRQrAwVnZ2dnZxYBAgRkAgUPDxYCHwAFATdkZAIHDw8WCB8ABQtbUHJldiBQYWdlXR8OCiMeB0VuYWJsZWRnHxACBGRkAgkPDxYIHwAFC1tOZXh0IFBhZ2VdHw4KTh8QAgQfGmhkZAILDxBkZBYBAgVkAg0PFgIfF2dkAg8PFgIfF2hkAhEPEA9kFgIeCG9uY2hhbmdlBRNTZXRTZWxlY3RlZEZvbGRlcigpZGRkAhUPFgIfF2gWAgIBDxBkZBYBZmQCFw9kFgQCAQ8PFgQfAGUfGmhkZAICDw8WAh8XaGRkAhkPDxYEHwAFDU1hdGVyaWFsIE5hbWUfGmhkZAIbDw8WAh8XaGRkAh0PDxYCHxdoZGQCHw9kFgYCAQ8PFgQfAAUFUHJvcDEfGmhkZAIDDw8WAh8XaGRkAgUPDxYCHxdoZGQCIQ9kFgYCAQ8PFgQfAAUFUHJvcDIfGmhkZAIDDw8WAh8XaGRkAgUPDxYCHxdoZGQCIw9kFgYCAQ8PFgQfAAUFUHJvcDMfGmhkZAIDDw8WAh8XaGRkAgUPDxYCHxdoZGQCJQ9kFgYCAQ8PFgQfAAUFUHJvcDQfGmhkZAIDDw8WAh8XaGRkAgUPDxYCHxdoZGQCJw9kFgYCAQ8PFgQfAAUFUHJvcDUfGmhkZAIDDw8WAh8XaGRkAgUPDxYCHxdoZGQCKQ9kFgYCAQ8PFgQfAAUFUHJvcDYfGmhkZAIDDw8WAh8XaGRkAgUPDxYCHxdoZGQCKw9kFgYCAQ8PFgQfAAUFUHJvcDcfGmhkZAIDDw8WAh8XaGRkAgUPDxYCHxdoZGQCLQ9kFgYCAQ8PFgQfAAUFUHJvcDgfGmhkZAIDDw8WAh8XaGRkAgUPDxYCHxdoZGQCLw9kFgYCAQ8PFgQfAAUFUHJvcDkfGmhkZAIDDw8WAh8XaGRkAgUPDxYCHxdoZGQCMQ9kFgYCAQ8PFgQfAAUGUHJvcDEwHxpoZGQCAw8PFgIfF2hkZAIFDw8WAh8XaGRkAjUPDxYCHwAFBDEyNDVkZAI3DxBkEBUFATEBMgEzATQBNRUFATEBMgEzATQBNRQrAwVnZ2dnZxYBAgRkAjkPDxYCHwAFATdkZAI7Dw8WCB8ABQtbUHJldiBQYWdlXR8OCiMfGmcfEAIEZGQCPQ8PFggfAAULW05leHQgUGFnZV0fDgpOHxACBB8aaGRkAj8PEGRkFgECBWQCQQ8WAh8XZxYCAgEPDxYCHwAFuAQ8YnIgLz4NCk1hdGVyaWFscyBmbGFnZ2VkIGFzIGRpc2NvbnRpbnVlZCAoPGltZyBzcmM9Ii9pbWFnZXMvYnV0dG9ucy9pY29uRGlzY29udGludWVkLmpwZyIgYWx0PSIiIC8+KSBhcmUgbm8gbG9uZ2VyIHBhcnQgb2YgdGhlIG1hbnVmYWN0dXJlcuKAmXMgc3RhbmRhcmQgcHJvZHVjdCBsaW5lIGFjY29yZGluZyB0byBvdXIgbGF0ZXN0IGluZm9ybWF0aW9uLiAgVGhlc2UgbWF0ZXJpYWxzIG1heSBiZSBhdmFpbGFibGUgYnkgc3BlY2lhbCBvcmRlciwgaW4gZGlzdHJpYnV0aW9uIGludmVudG9yeSwgb3IgcmVpbnN0YXRlZCBhcyBhbiBhY3RpdmUgcHJvZHVjdC4gIERhdGEgc2hlZXRzIGZyb20gbWF0ZXJpYWxzIHRoYXQgYXJlIG5vIGxvbmdlciBhdmFpbGFibGUgcmVtYWluIGluIE1hdFdlYiB0byBhc3Npc3QgdXNlcnMgaW4gZmluZGluZyByZXBsYWNlbWVudCBtYXRlcmlhbHMuICANCjxiciAvPjxiciAvPg0KVXNlcnMgb2Ygb3VyIEFkdmFuY2VkIFNlYXJjaCAocmVnaXN0cmF0aW9uIHJlcXVpcmVkKSBtYXkgZXhjbHVkZSBkaXNjb250aW51ZWQgbWF0ZXJpYWxzIGZyb20gc2VhcmNoIHJlc3VsdHMuZGQCFg9kFgJmDxYCHwAFcDxhIGhyZWY9Ii9jbGlja3Rocm91Z2guYXNweD9hZGRhdGFpZD0yNzciIGNsYXNzPSJmb290bGluayI+PHNwYW4gY2xhc3M9ImZvb3QiPlRyYWRlJm5ic3A7UHVibGljYXRpb25zPC9zcGFuPjwvYT5kGAEFHl9fQ29udHJvbHNSZXF1aXJlUG9zdEJhY2tLZXlfXxYEBTZjdGwwMCRDb250ZW50TWFpbiRVY01hdEdyb3VwRmluZGVyMSRzZWxlY3RDYXRlZ29yeUxpc3QFK2N0bDAwJENvbnRlbnRNYWluJHVjTWF0R3JvdXBUcmVlJG1zVHJlZVZpZXcFG2N0bDAwJENvbnRlbnRNYWluJGJ0blN1Ym1pdAUaY3RsMDAkQ29udGVudE1haW4kYnRuUmVzZXRJRBbKqy87+h0Md2mSBwnYu77/6w==" />
</div>

<script type="text/javascript">
//<![CDATA[
var theForm = document.forms['aspnetForm'];
if (!theForm) {
    theForm = document.aspnetForm;
}
function __doPostBack(eventTarget, eventArgument) {
    if (!theForm.onsubmit || (theForm.onsubmit() != false)) {
        theForm.__EVENTTARGET.value = eventTarget;
        theForm.__EVENTARGUMENT.value = eventArgument;
        theForm.submit();
    }
}
//]]>
</script>


<script src="/WebResource.axd?d=M68BSyLvwBMHRYzsSV62mvczr7w5VyKnq2JbgqcAAU1ioThlC6FNEVBncBdaA4oqf8cXPzy7QERzajvFfM67hSR6S0w1&amp;t=638313619312541215" type="text/javascript"></script>


<script src="/WebResource.axd?d=ClzPoKUn1iQnMGy7NeMJw70TvMayZ5w9nNmB7h_dY64i4BFFgjVcppyp69BFqhuzHctzc-rEantAjczzQ6UVMYkaWJw1&amp;t=638313619312541215" type="text/javascript"></script>
<script type="text/javascript">
//<![CDATA[

    function TreeView_PopulateNodeDoCallBack(context,param) {
        WebForm_DoCallback(context.data.treeViewID,param,TreeView_ProcessNodeData,context,TreeView_ProcessNodeData,false);
    }
var ctl00_ContentMain_ucMatGroupTree_msTreeView_Data = null;//]]>
</script>

<script src="/ScriptResource.axd?d=148LWaywfmset9PYVR0m-KYXX9JfyGYGqOtIYBGEAzoJh5VhFBwSI6JxnmoNvgtlAprhvntpc6lTDbRMgdKxckUmc8m2qw_fOCsX0G9LC8ojUpw4cTHWXgfuT_vU6m2Kpv3mt01iCczrTE2-J5X6EeBa2281&amp;t=637769794779625837" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=v_V1UgDrDAchg8Zuc1FL4qkvmjt-bwap7ZxwVGGPleYiJ6zfDOTL6wtCmVvILjbwo_sqb2PMUTNkjO4vp6nd-E7WWXTPgSI5-nvn3YDnHrVk5bgVX4Qv6F0fV4wTSslmaLZ3k98tckojDLWAEI7jm8h0H4d7BoGYxkqsyh8-Iujfrjjm0&amp;t=637769794779625837" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=V7Zma3AISNyGTbHVRPw8svJ4Ub7xUUaeoKwImcvnuDiy-XBR331l3eTjiEP8J_QlX2qgyAHxZFLywNbNyZ50fJLVEOCjx11lIDJOiQxc0oGA3qi7XXiggiwEcIWwzb9j2Is1n1D9sU40Zusk35AtkqRKL3Y1&amp;t=633177911400000000" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=wOguQo_yd3hCG4ajcSXPC6-XdeL0hmYWSMaFjvMXt1CBqR9neJqTXyTfPki4lIz4B29Zz_83btvS12BY6KpBKT-AgP0eaXp7V3h6XRdjiozV5w_n3oQRZg5Yq1QW8UNGj0Cy9jsap_7EcU2M99CapUyVG6I1&amp;t=633177911400000000" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=MAJxSy62Z3EtRwHXuuMc8KC6PFmrPqmkpcwdpcPGeqtqUQyJ3FBQ3cFyYWN1zNID_ngA53aMZdG_dpRu3TRDH8-iqKAjPAcc2BWe63WYGrGbyHc9syJxR6lypTgvRaYmwwEmkLEsaGUniMurHc16yr1VrjmzG3HWOeu9UXNr23Wv2xzh0&amp;t=633177911400000000" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=ukx9eyyt483y_CGgCF1OToQPWzPYyCynhpMvhxN7idzW3WaKjHjpgAOcMgtXZIEcspnbyDEADdRWCSHB-hF39Ycl13KWsqdlOHElw2qgSkGmkL8iNH37htb0BNS0lxw6oHvtqYiVpw-nZeYoQ1NkgX2qXOI1&amp;t=633177911400000000" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=Ovkr-MWDypjnq0C7FVbNgOs-Mt3NaktNVf8SEOX721PoQ5mQqTM6S4igSQL1_PXvh96km3stLiqrorOgm8nnnUU6y6edTkhxmLiwfFtEXQEAAnFFQpUepgsxEd0ahMvWcDfEac0EE7J8iDb2rqSMTxk51ydfY-c-8zcKvo5Mrn83cvfN0&amp;t=633177911400000000" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=pKQF4iQYwkSn5cAwTNDM8ez4TLepowWnai-WD6NVL5h0ay9Qtt0a9vPCaI-KN9OHcv1fL8VnwxEDM6EAVtgZY06V8XJTlVrbmb8WlMvGmhwBeL3_SVtKKOS-QExY8AnVbTFl_wOknplhy8bz-9sIWNCh4XPq1WhRj7ALNAKaeruPhS7d0&amp;t=633177911400000000" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=uM6eUuIuIa_NiCgVrtwgqczDeqTqI37XalY8Ri_W2ZjXEzIKi84afVBjFk8gneIDXcFnw8yjCD1qhE8mzDTzAUSdKnbDte0yC23kMoyvxPyu7vRH9E-6JMZtE8-gELUPmoZdOZJ9nXitHKYBLw2P6skU1IkzFRegK74bETu2FdEtuaeR0&amp;t=633177911400000000" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=ewF5FMb1ckbNCdJ9rpE5xMSXy-WecqVGE8uma1_520KusuW7jTUE2tnPMG7JyR0FDIFqDUMQbsfTZlsQ0sD4YAW8MnN90GKIMPvzaP7eOG3ss8E050_l7Rpnaz7I_U3ITzBXkcJZIOPcI1PxOReT8FSeWmo1&amp;t=633177911400000000" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=3lB_EBCMPyiwzDhzWdoE-i4mLHhkIMbbkaLwMr-U1WcAoDqBHS-oYFn4gz3ZwnpzK_KZp97Ev8ZbiUNo7WMJ45h2xgOH59erMFo1BpNb87-yu3X0mFyWs5rB3ANm4lsVSIu7RZd7XUEwfZXzugJ3CoJqxvRqiTGLpcXquEF24hK3OUH-0&amp;t=633177911400000000" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=Z7-pZSKJ6vCyr9H9S4r9baB7TLiJ-T4EoMOvlH-iQ5-icaO-vWMrjXk3EPctuPrcO5qwJSTaLQtHyguwle7Kty78exrSwHataIriNUEu1DnUkixv4pxU7Dknp_GDVHby5XcMA4lMwazAE0AFG9Cndu0An_gAGACGlYXsGUG9W6okCQsH0&amp;t=633177911400000000" type="text/javascript"></script>
<script src="../WebServices/Materials.asmx/js" type="text/javascript"></script>
<script src="../WebServices/MatGroups.asmx/js" type="text/javascript"></script>
<script src="../WebServices/Folders.asmx/js" type="text/javascript"></script>
<script type="text/javascript">
//<![CDATA[
function WebForm_OnSubmit() {
null;
return true;
}
//]]>
</script>

<div>

	<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="640EFF8C" />
</div>

<!--
The ScriptManager control is required for all ASP AJAX functionality - especially used by search pages.
It generates the javascript required.
Only one script manager is allowed on a page, and since this is the master page, this is the main script manager
However, a ScriptManagerProxy control can be used to set properties and settings on indivual pages.
For an example, see /WebControls/ucSearchResults.ascx
-->
<script type="text/javascript">
//<![CDATA[
Sys.WebForms.PageRequestManager._initialize('ctl00$ajxScriptManager', document.getElementById('aspnetForm'));
Sys.WebForms.PageRequestManager.getInstance()._updateControls([], [], [], 90);
//]]>
</script>


<table class="tabletight" style="width:100%; text-align:center;background-image: url(/images/gray.gif);">
  <tr >
    <td rowspan="2"><a href="/index.aspx"><img src="/images/logo1.gif" width="270" height="42" alt="MatWeb - Material Property Data"  /></a></td>

                
                <td style="text-align:right;vertical-align:middle; width:90%" ><span class='greeting'><span class='hiText'><a href ='/services/advertising.aspx'>Advertise with MatWeb!</a></span>&nbsp;&nbsp;&nbsp;&nbsp;</span></td>
                <td style="text-align:right;vertical-align:middle; padding-right:5px;" ><a href='/membership/regstart.aspx'><img src='/images/buttons/btnRegisterBSF.gif' width="62" height="20" alt="Register Now" /></a></td>

                
  </tr>
  <tr >
    <td  align="left" valign="bottom" colspan="2"><span class="tagline">
    
    Data sheets for over <span class="greeting">180,000</span>  metals, plastics, ceramics, and composites.</span></td>
  </tr>
  <tr style="background-image:url(/images/blue.gif); background-color:#000099; " >
    <td bgcolor="#000099"><a href="/index.aspx"><img src="/images/logo2.gif" width="270" height="23px" alt="MatWeb - Material Property Data" border="0" /></a></td>
    <td class="topnavig8" style=" background-color:#000099;vertical-align:middle; text-align:right; white-space:nowrap;" colspan="2" align="right" valign="middle">
        <a href="/index.aspx" class="topnavlink">HOME</a>&nbsp;&nbsp;&#8226;&nbsp;&nbsp;
                        <a href="/search/search.aspx" class="topnavlink">SEARCH</a>&nbsp;&nbsp;&#8226;&nbsp;&nbsp;
                        <a href="/tools/tools.aspx" class="topnavlink">TOOLS</a>&nbsp;&nbsp;&#8226;&nbsp;&nbsp;
                        <a href="/reference/suppliers.aspx" class="topnavlink">SUPPLIERS</a>&nbsp;&nbsp;&#8226;&nbsp;&nbsp;
                        <a href="/folders/ListFolders.aspx" class="topnavlink">FOLDERS</a>&nbsp;&nbsp;&#8226;&nbsp;&nbsp;
                        <a href="/services/services.aspx" class="topnavlink">ABOUT US</a>&nbsp;&nbsp;&#8226;&nbsp;&nbsp;
                        <a href="/reference/faq.aspx" class="topnavlink">FAQ</a>&nbsp;&nbsp;&#8226;&nbsp;&nbsp;
                        
                        <a href="/membership/login.aspx" class="topnavlink">LOG IN</a>
                        
                        &nbsp;
                        
                        &nbsp;
    </td>
  </tr>
</table>

<div id="divRecentMatls" class="tableborder tight" style=" display:none; position:absolute; top:95px; left:150px; width:400px; height:400px;" onmouseover="clearTimeout(tmrtmp);" onmouseout="tmrtmp=setTimeout('RecentMatls.Hide()', 800)">
        <table class="headerblock tabletight" style="vertical-align:top; font-size:13px; width:100%;"><tr>
                <td style="padding:3px;"> Recently Viewed Materials (most recent at top)</td>
                <td style="padding:3px;text-align:right;"><input type="image" src="/images/buttons/btnCloseWhite.gif" onclick="RecentMatls.Hide();return false;" style="cursor:pointer;" />&nbsp;</td>
        </tr></table>
        <div id="divRecentMatlsBody" style="height:375px; overflow:auto; background-color:White;">

                <table id="tblRecentMatls" class="tabledataformat tableloose" style="background-color:White;">
    
                <tr><td>
                <p />
                <a href="/membership/login.aspx">Login</a> to see your most recently viewed materials here.<p />
                Or if you don't have an account with us yet, then <a href="/membership/regstart.aspx">click here to register.</a>
                </td></tr>
                
                </table>

        </div>
</div>

<script type="text/javascript">

        var RecentMatls = new _RecentMatls();
        var tmrtmp;

        function _RecentMatls(){

                var divRecentMatls;
                var tblRecentMatls;
                var trRecentMatlsRowTemplate;
                var trRecentMatlsNoData;
                var DataIsLoaded=false;

                //Using prototype.js methods...
                divRecentMatls = $("divRecentMatls");
                tblRecentMatls = $("tblRecentMatls");
    

                divRecentMatls.style.display = "none";

                this.Toggle = function(){
                        //alert("Toggle()");
                        if(divRecentMatls.style.display == "block"){this.Hide();}
                        else{this.Show();}
                }

                this.Show = function(){
                        //alert("Show()");
                        divRecentMatls.style.display = "block";
                        matweb.toggleSelect(divRecentMatls,0);
                        if(!DataIsLoaded){
            
                        }
                }

                this.Hide = function(){
                        //alert("Hide()");
                        divRecentMatls.style.display = "none";
                        matweb.toggleSelect(divRecentMatls,1);
                }

    

        }//end class

</script>

<table class="tabletight t_ableborder t_ablegrid" style="width:100%;" >
  <tr>
   <td style="width:100%; height:27px; white-space:nowrap; text-align:left; vertical-align:middle;" class="tagline" >
      <span class="navlink"><b>&nbsp;&nbsp;Searches:</b></span>
      &nbsp;&nbsp;<a href="/search/AdvancedSearch.aspx" class="navlink"><span class="nav"><font color="#CC0000">Advanced</font></span></a>
      &nbsp;|&nbsp;<a href="/search/MaterialGroupSearch.aspx" class="navlink"><span class="nav">Category</span></a>
      &nbsp;|&nbsp;<a href="/search/PropertySearch.aspx" class="navlink"><span class="nav">Property</span></a>
      &nbsp;|&nbsp;<a href="/search/CompositionSearch.aspx" class="navlink"><span class="nav">Metals</span></a>
      &nbsp;|&nbsp;<a href="/search/SearchTradeName.aspx" class="navlink"><span class="nav">Trade Name</span></a>
      &nbsp;|&nbsp;<a href="/search/SearchManufacturerName.aspx" class="navlink"><span class="nav">Manufacturer</span></a>
                &nbsp;|&nbsp;<a href="javascript:RecentMatls.Toggle();" id="ctl00_lnkRecentMatls" class="navlink"><span class="nav"><font color="#CC0000">Recently Viewed Materials</font></span></a>
    </td>

    <td style="text-align:right; vertical-align:bottom; padding-right:5px;">

      <table class="tabletight t_ableborder" ><tr>
        <td style=" white-space:nowrap; vertical-align:bottom; " >
                                        &nbsp;&nbsp;
                                        <input name="ctl00$txtQuickText" type="text" id="ctl00_txtQuickText" style="width:100px;" maxlength="40" class="quicksearchinput" onkeydown="txtQuickText_OnKeyDown(event);" />
                                        <input type="image" id="btnQuickTextSearch" style="vertical-align:top; width:60px; height:25px;" src="/images/buttons/btnSearch.gif" alt="Search" onclick="return btnQuickTextSearch_Click()"  />
                                        <script type="text/javascript">

                                                //SETUP THE AUTOPOST ON ENTER KEY EVENT FOR txtQuickText FIELD
                                                //document.getElementById("ctl00_txtQuickText").onkeydown=txtQuickText_OnKeyDown;

                                                function txtQuickText_OnKeyDown(evt){
                                                        //SUBMIT THE FORM AS IF THE BUTTON WAS PUSHED
                                                        //var UniqueID = "";
                                                        var txtQuickText = document.getElementById('ctl00_txtQuickText');
                                                        if(matweb.IsEnterKey(evt))
                                                                if(checkSearchText(txtQuickText))
                                                                        //matweb.DoPostBack(UniqueID, "");
                                                                        window.location.href="/search/QuickText.aspx?SearchText=" + escape(txtQuickText.value);
                                                        return false;
                                                }

                                                function btnQuickTextSearch_Click(){
                                                        var txtQuickText = document.getElementById('ctl00_txtQuickText');
                                                        if(checkSearchText(txtQuickText))
                                                                window.location.href="/search/QuickText.aspx?SearchText=" + escape(txtQuickText.value);
                                                        return false;
                                                }

                                                function checkSearchText(searchbox){
                                                        searchbox.value = trim(searchbox.value);
                                                        if(searchbox.value == ''){
                                                                //alert('Please enter something to search for');
                                                                matweb.alert('Please enter something to search for', 'Search Error');
                                                                //searchbox.focus();
                                                                return false;
                                                        }
                                                        return true;
                                                }

                                                function trim(s)  { return ltrim(rtrim(s)) }
                                                function ltrim(s) { return s.replace(/^\s+/g, "") }
                                                function rtrim(s) { return s.replace(/\s+$/g, "") }

                                        </script>
                                </td>
                                </tr>
                        </table>

    </td>
  </tr>
  <tr style="background-image:url(/images/shad.gif);height:7px;" ><td colspan="2"></td></tr>
</table>

<!-- ====================================== MAIN CONTENT ============================================= -->
<div style="vertical-align:top; padding-left:10px; padding-right:10px;" >





	<center><a href="/clickthrough.aspx?addataid=1351"><img src="/images/assets/metalmen-Nickel.png" border=0 alt="Metalmen Sales" width = "728" height = "90"></a>
</center>
	
	<h2><a href="#help"><img src="/images/buttons/iconHelp.gif" alt="" style="vertical-align:bottom;" /></a>Material Category Search</h2>
	
	

<div id="ctl00_ContentMain_ucPopupMessage1_divPopupMessage" class="divPopupAlert" style="display:none;width:250px;">
	<table >
		<tr id="ctl00_ContentMain_ucPopupMessage1_trTitleRow" class="divPopupAlert_TitleRow" style="cursor:move;">
	<th><span id="ctl00_ContentMain_ucPopupMessage1_lblTitle">User Input Problem</span></th>
</tr>

		<tr class="divPopupAlert_ContentRow" ><td style="" ><span id="ctl00_ContentMain_ucPopupMessage1_lblMessage"></span></td></tr>
		<tr class="divPopupAlert_ButtonRow"><td><input name="ctl00$ContentMain$ucPopupMessage1$btnOK" type="button" id="ctl00_ContentMain_ucPopupMessage1_btnOK" value="OK" /></td></tr>
	</table>
</div>

<input type="hidden" name="ctl00$ContentMain$ucPopupMessage1$hndPopupControl" id="ctl00_ContentMain_ucPopupMessage1_hndPopupControl" />


	<table class="" style="width:100%;">
	<tr>
		<td style=" vertical-align:top; width:300px;" >
			<strong>Find a Material Category:</strong>
			

<input name="ctl00$ContentMain$UcMatGroupFinder1$txtSearchText" type="text" value="Nickel" id="ctl00_ContentMain_UcMatGroupFinder1_txtSearchText" style="Width:304px;" /><br />
<select name="ctl00$ContentMain$UcMatGroupFinder1$selectCategoryList" id="ctl00_ContentMain_UcMatGroupFinder1_selectCategoryList" style="position:absolute;display:none;Width:308px;" size="18">
</select>

<input type="hidden" name="ctl00$ContentMain$UcMatGroupFinder1$TextBoxWatermarkExtender2_ClientState" id="ctl00_ContentMain_UcMatGroupFinder1_TextBoxWatermarkExtender2_ClientState" />

<script type="text/javascript">

var ucMatGroupFinderManager = new _ucMatGroupFinderManager("ctl00_ContentMain_UcMatGroupFinder1_txtSearchText","ctl00_ContentMain_UcMatGroupFinder1_selectCategoryList");

function _ucMatGroupFinderManager(txtSearchTextID,selectCategoryListID){

	var txtSearchText = document.getElementById(txtSearchTextID);
	var selectCategoryList = document.getElementById(selectCategoryListID);
	var showtime;
	var blurtime;
	var MinTextLength = 4;
	var KeyStrokeActionDelay = 650;
	var FadeInOutDelay = 0.2;
	var LastUsedText;
	
	//The init function is currently run at the bottom of this class
	this.init=function(){

		txtSearchText.onfocus = txtSearchText_onfocus;
		txtSearchText.onkeyup = txtSearchText_checktext;
		txtSearchText.onblur = selectCategoryList_startblur;
		
		selectCategoryList.onfocus = selectCategoryList_clearblur;
		selectCategoryList.onchange = selectCategoryList_onchange;
		selectCategoryList.onblur = selectCategoryList_startblur;

	}

	txtSearchText_onfocus = function(){
		if(txtSearchText.value == "Type at least 4 characters here...")
			txtSearchText.value = "";
		selectCategoryList_clearblur();
		txtSearchText_checktext();
	}

	function txtSearchText_checktext(){
		searchText = txtSearchText.value;
		//alert(searchText);
		clearTimeout(showtime);
		if(searchText.length >= MinTextLength){
			showtime = setTimeout(function(){
				selectCategoryList.style.display = "block";
				if(txtSearchText.value!=LastUsedText){
					LastUsedText = searchText;
					aci.matweb.WebServices.MatGroups.FindMatGroups(searchText,DisplayCatData,matweb.WebServiceErrorHandler,"txtSearchText_checktext");
				}
				//AjaxControlToolkit.Animation.FadeInAnimation.play(selectCategoryList, FadeInOutDelay, 1, 25, 0, 1, true);
			},KeyStrokeActionDelay);
		}
		else{
			LastUsedText = "";
			showtime = setTimeout(function(){
				selectCategoryList.options.length = 0;
				selectCategoryList.style.display = "none";
				//AjaxControlToolkit.Animation.FadeOutAnimation.play(selectCategoryList,FadeInOutDelay);
				//AjaxControlToolkit.Animation.FadeOutAnimation.play(selectCategoryList,FadeInOutDelay, 25, 0, 1, true);
			},KeyStrokeActionDelay);
		}
	}

	function selectCategoryList_clearblur(){
		clearTimeout(blurtime);
	}

	function selectCategoryList_onchange(){
		var MatGroupID = selectCategoryList.options[selectCategoryList.selectedIndex].value;
		var MatGroupText = selectCategoryList.options[selectCategoryList.selectedIndex].text;
		//alert("Selected MatGroupID: " + MatGroupID);
		//alert("Selected MatGroupText: " + MatGroupText);
		//When no data is found, we have a "No Categories Contain Text..." message, but the MatGroupID is set to 0, so we can ignore if MatGroupID == 0.
		if(MatGroupID > 0){
			ucMatGroupFinder1_OnSelected(MatGroupID,MatGroupText)
		}
		selectCategoryList_startblur();
		//txtSearchText.value = "";
	}
	
	function selectCategoryList_startblur(){
		//alert("selectCategoryList_onblur()");
		clearTimeout(blurtime);
		blurtime = setTimeout(function(){
			selectCategoryList.style.display = "none";
			//AjaxControlToolkit.Animation.FadeOutAnimation.play(selectCategoryList,FadeInOutDelay, 25, 0, 1, true);
		},KeyStrokeActionDelay);
	}
	
	function DisplayCatData(MatGroupData){
		//alert("GetCatData()");
		//alert("vwMaterialGroupList: " + vwMaterialGroupList);
		var MatGroupID,GroupName, MatCount;
		selectCategoryList.options.length = 0;//clear any existing options....
		if(MatGroupData.length==0){
				//alert(vwMaterialGroupList[i].GroupName);
				MatGroupID = 0;
				GroupName = "No categories contain the text: " + txtSearchText.value
				selectCategoryList.options[0] = new Option(GroupName,MatGroupID);					
		}
		else{
			for(i=0;i<MatGroupData.length;i++){
				//alert(vwMaterialGroupList[i].GroupName);
				MatCount = MatGroupData[i].MatCount;
				MatGroupID = MatGroupData[i].MatGroupID;
				GroupName = MatGroupData[i].GroupName + " (" + MatCount + " matls)";
				selectCategoryList.options[i] = new Option(GroupName,MatGroupID);					
			}
		}
	}
	
	this.init();

}//end class
	
</script>



			<strong>Select a Material Category:</strong>
			
			<div class="tableborder" style=" height:225px; width:300px; overflow:scroll; ">
			<a href="#ctl00_ContentMain_ucMatGroupTree_msTreeView_SkipLink"><img alt="Skip Navigation Links." src="/WebResource.axd?d=vAkieGj3ugBpu6CKp7dqvCP5iHLzPbUd2XME-h1vQcC6d5hLg7Vc1kNVzB8A45Ui3K9XZ9m3-E2pxsaJQYh2uMkAul41&amp;t=638313619312541215" width="0" height="0" style="border-width:0px;" /></a><div id="ctl00_ContentMain_ucMatGroupTree_msTreeView">
	<table cellpadding="0" cellspacing="0" style="border-width:0;">
		<tr>
			<td><a id="ctl00_ContentMain_ucMatGroupTree_msTreeViewn0" href="javascript:TreeView_PopulateNode(ctl00_ContentMain_ucMatGroupTree_msTreeView_Data,0,document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewn0'),document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewt0'),null,'r','Carbon (866 matls)','283','f','','f')"><img src="/WebResource.axd?d=6v5qM6VKQYD3XNwwXEy7Tix7dzVdqDJ8QTRIlUPrzXQqNsXQgUwIfmn34ZzRORd_IZj0bF4vkZzID1HuSFE5uKyLgaPYeAYE8VT8HYfb3Rk6zsDw0&amp;t=638313619312541215" alt="Expand Carbon (866 matls)" style="border-width:0;" /></a></td><td class="ctl00_ContentMain_ucMatGroupTree_msTreeView_2"><a class="ctl00_ContentMain_ucMatGroupTree_msTreeView_0 ctl00_ContentMain_ucMatGroupTree_msTreeView_1" href="javascript:ucMatGroupTree_OnNodeClick('Carbon (866 matls)','283')" id="ctl00_ContentMain_ucMatGroupTree_msTreeViewt0">Carbon (866 matls)</a></td>
		</tr><tr style="height:0px;">
			<td></td>
		</tr>
	</table><table cellpadding="0" cellspacing="0" style="border-width:0;">
		<tr style="height:0px;">
			<td></td>
		</tr><tr>
			<td><a id="ctl00_ContentMain_ucMatGroupTree_msTreeViewn1" href="javascript:TreeView_PopulateNode(ctl00_ContentMain_ucMatGroupTree_msTreeView_Data,1,document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewn1'),document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewt1'),null,'t','Ceramic (10004 matls)','11','f','','f')"><img src="/WebResource.axd?d=TinC6igX1vPDgWh7DstWwBTHTyH4uuybDU4NW47jJD06wHdAO3YyJC_eKK1Zn5oIbwqRo4Cri8YwhA82G-LD0p14yEF1EfoIZvKleYMIuQ2K1hkE0&amp;t=638313619312541215" alt="Expand Ceramic (10004 matls)" style="border-width:0;" /></a></td><td class="ctl00_ContentMain_ucMatGroupTree_msTreeView_2"><a class="ctl00_ContentMain_ucMatGroupTree_msTreeView_0 ctl00_ContentMain_ucMatGroupTree_msTreeView_1" href="javascript:ucMatGroupTree_OnNodeClick('Ceramic (10004 matls)','11')" id="ctl00_ContentMain_ucMatGroupTree_msTreeViewt1">Ceramic (10004 matls)</a></td>
		</tr><tr style="height:0px;">
			<td></td>
		</tr>
	</table><table cellpadding="0" cellspacing="0" style="border-width:0;">
		<tr style="height:0px;">
			<td></td>
		</tr><tr>
			<td><a id="ctl00_ContentMain_ucMatGroupTree_msTreeViewn2" href="javascript:TreeView_PopulateNode(ctl00_ContentMain_ucMatGroupTree_msTreeView_Data,2,document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewn2'),document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewt2'),null,'t','Fluid (7562 matls)','5','f','','f')"><img src="/WebResource.axd?d=TinC6igX1vPDgWh7DstWwBTHTyH4uuybDU4NW47jJD06wHdAO3YyJC_eKK1Zn5oIbwqRo4Cri8YwhA82G-LD0p14yEF1EfoIZvKleYMIuQ2K1hkE0&amp;t=638313619312541215" alt="Expand Fluid (7562 matls)" style="border-width:0;" /></a></td><td class="ctl00_ContentMain_ucMatGroupTree_msTreeView_2"><a class="ctl00_ContentMain_ucMatGroupTree_msTreeView_0 ctl00_ContentMain_ucMatGroupTree_msTreeView_1" href="javascript:ucMatGroupTree_OnNodeClick('Fluid (7562 matls)','5')" id="ctl00_ContentMain_ucMatGroupTree_msTreeViewt2">Fluid (7562 matls)</a></td>
		</tr><tr style="height:0px;">
			<td></td>
		</tr>
	</table><table cellpadding="0" cellspacing="0" style="border-width:0;">
		<tr style="height:0px;">
			<td></td>
		</tr><tr>
			<td><a id="ctl00_ContentMain_ucMatGroupTree_msTreeViewn3" href="javascript:TreeView_PopulateNode(ctl00_ContentMain_ucMatGroupTree_msTreeView_Data,3,document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewn3'),document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewt3'),null,'t','Metal (17052 matls)','9','f','','f')"><img src="/WebResource.axd?d=TinC6igX1vPDgWh7DstWwBTHTyH4uuybDU4NW47jJD06wHdAO3YyJC_eKK1Zn5oIbwqRo4Cri8YwhA82G-LD0p14yEF1EfoIZvKleYMIuQ2K1hkE0&amp;t=638313619312541215" alt="Expand Metal (17052 matls)" style="border-width:0;" /></a></td><td class="ctl00_ContentMain_ucMatGroupTree_msTreeView_2"><a class="ctl00_ContentMain_ucMatGroupTree_msTreeView_0 ctl00_ContentMain_ucMatGroupTree_msTreeView_1" href="javascript:ucMatGroupTree_OnNodeClick('Metal (17052 matls)','9')" id="ctl00_ContentMain_ucMatGroupTree_msTreeViewt3">Metal (17052 matls)</a></td>
		</tr><tr style="height:0px;">
			<td></td>
		</tr>
	</table><table cellpadding="0" cellspacing="0" style="border-width:0;">
		<tr style="height:0px;">
			<td></td>
		</tr><tr>
			<td><a id="ctl00_ContentMain_ucMatGroupTree_msTreeViewn4" href="javascript:TreeView_PopulateNode(ctl00_ContentMain_ucMatGroupTree_msTreeView_Data,4,document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewn4'),document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewt4'),null,'t','Other Engineering Material (8063 matls)','285','f','','f')"><img src="/WebResource.axd?d=TinC6igX1vPDgWh7DstWwBTHTyH4uuybDU4NW47jJD06wHdAO3YyJC_eKK1Zn5oIbwqRo4Cri8YwhA82G-LD0p14yEF1EfoIZvKleYMIuQ2K1hkE0&amp;t=638313619312541215" alt="Expand Other Engineering Material (8063 matls)" style="border-width:0;" /></a></td><td class="ctl00_ContentMain_ucMatGroupTree_msTreeView_2"><a class="ctl00_ContentMain_ucMatGroupTree_msTreeView_0 ctl00_ContentMain_ucMatGroupTree_msTreeView_1" href="javascript:ucMatGroupTree_OnNodeClick('Other Engineering Material (8063 matls)','285')" id="ctl00_ContentMain_ucMatGroupTree_msTreeViewt4">Other Engineering Material (8063 matls)</a></td>
		</tr><tr style="height:0px;">
			<td></td>
		</tr>
	</table><table cellpadding="0" cellspacing="0" style="border-width:0;">
		<tr style="height:0px;">
			<td></td>
		</tr><tr>
			<td><a id="ctl00_ContentMain_ucMatGroupTree_msTreeViewn5" href="javascript:TreeView_PopulateNode(ctl00_ContentMain_ucMatGroupTree_msTreeView_Data,5,document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewn5'),document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewt5'),null,'t','Polymer (97635 matls)','10','f','','f')"><img src="/WebResource.axd?d=TinC6igX1vPDgWh7DstWwBTHTyH4uuybDU4NW47jJD06wHdAO3YyJC_eKK1Zn5oIbwqRo4Cri8YwhA82G-LD0p14yEF1EfoIZvKleYMIuQ2K1hkE0&amp;t=638313619312541215" alt="Expand Polymer (97635 matls)" style="border-width:0;" /></a></td><td class="ctl00_ContentMain_ucMatGroupTree_msTreeView_2"><a class="ctl00_ContentMain_ucMatGroupTree_msTreeView_0 ctl00_ContentMain_ucMatGroupTree_msTreeView_1" href="javascript:ucMatGroupTree_OnNodeClick('Polymer (97635 matls)','10')" id="ctl00_ContentMain_ucMatGroupTree_msTreeViewt5">Polymer (97635 matls)</a></td>
		</tr><tr style="height:0px;">
			<td></td>
		</tr>
	</table><table cellpadding="0" cellspacing="0" style="border-width:0;">
		<tr style="height:0px;">
			<td></td>
		</tr><tr>
			<td><img src="/WebResource.axd?d=7_V5n1KYSQsj6PoSqgIxb0egBsAa91jSXPkbmM6FaiF14-JsNnpUDnfSdxZcvfd0OQCLDtC24pMIXigO-41cL2rsq-0xea-SSG7yZUyRcliFFqe70&amp;t=638313619312541215" alt="" /></td><td class="ctl00_ContentMain_ucMatGroupTree_msTreeView_2"><a class="ctl00_ContentMain_ucMatGroupTree_msTreeView_0 ctl00_ContentMain_ucMatGroupTree_msTreeView_1" href="javascript:ucMatGroupTree_OnNodeClick('Pure Element (507 matls)','184')" id="ctl00_ContentMain_ucMatGroupTree_msTreeViewt6">Pure Element (507 matls)</a></td>
		</tr><tr style="height:0px;">
			<td></td>
		</tr>
	</table><table cellpadding="0" cellspacing="0" style="border-width:0;">
		<tr style="height:0px;">
			<td></td>
		</tr><tr>
			<td><a id="ctl00_ContentMain_ucMatGroupTree_msTreeViewn7" href="javascript:TreeView_PopulateNode(ctl00_ContentMain_ucMatGroupTree_msTreeView_Data,7,document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewn7'),document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewt7'),null,'l','Wood and Natural Products (398 matls)','277','f','','t')"><img src="/WebResource.axd?d=vUBrM2nNhECNbFO0AJKq52p2zWrY5XG8nWgCDL4AfsrexStb0HREDV6GHMvdkAGDInxYtTyWXuxFUIiQv3JX2fScVRwQsrVv5srHrjOIeC3ayAG20&amp;t=638313619312541215" alt="Expand Wood and Natural Products (398 matls)" style="border-width:0;" /></a></td><td class="ctl00_ContentMain_ucMatGroupTree_msTreeView_2"><a class="ctl00_ContentMain_ucMatGroupTree_msTreeView_0 ctl00_ContentMain_ucMatGroupTree_msTreeView_1" href="javascript:ucMatGroupTree_OnNodeClick('Wood and Natural Products (398 matls)','277')" id="ctl00_ContentMain_ucMatGroupTree_msTreeViewt7">Wood and Natural Products (398 matls)</a></td>
		</tr><tr style="height:0px;">
			<td></td>
		</tr>
	</table>
</div><a id="ctl00_ContentMain_ucMatGroupTree_msTreeView_SkipLink"></a>
<span id="ctl00_ContentMain_ucMatGroupTree_lblDebug"></span>

			</div>
			
			<strong>Selected Material Category: </strong>
			<div id="ctl00_ContentMain_divMatGroupName" class="selectedItem" style="width:306px;">Nickel Alloy (1210 matls)</div>
			<input name="ctl00$ContentMain$txtMatGroupID" type="hidden" id="ctl00_ContentMain_txtMatGroupID" value="193" />
			<input name="ctl00$ContentMain$txtMatGroupText" type="hidden" id="ctl00_ContentMain_txtMatGroupText" value="Nickel Alloy (1210 matls)" />
			
			

			<input type="image" name="ctl00$ContentMain$btnSubmit" id="ctl00_ContentMain_btnSubmit" src="../images/buttons/btnFind.gif" alt="Find" style="border-width:0px;" /> <!-- OnClientClick="return validate()" -->
			<input type="image" name="ctl00$ContentMain$btnReset" id="ctl00_ContentMain_btnReset" title="Clear Search" src="/images/buttons/btnReset.gif" alt="Clear Search" style="border-width:0px;" />

			<span id="ctl00_ContentMain_lblDebug"></span>

		</td>
		<td>&nbsp;</td>
		<td style=" vertical-align:top; width:80%;" >
			<strong>Search Results</strong>
			<div class="tableborder" id="divSearchResults" style="padding:5px;" >
				
				<div id="ctl00_ContentMain_pnlSearchResults">
	
					There are <span id="ctl00_ContentMain_lblMatlCount">1245</span> materials in the category 
					<span id="ctl00_ContentMain_lblMaterialGroupName" class="selectedItem">Nickel Alloy</span>.
					If your material is not listed, please refer to our <a href="/help/strategy.aspx">search strategy</a> page for assistance in limiting your search.
				
</div>
			</div>
			<p />




<div id="ctl00_ContentMain_UcSearchResults1_pnlResultsFound" class="tableborder">
	

	<table style="background-color:Silver; width:100%;" class="" >
		<tr>
			<td><strong>Found <span id="ctl00_ContentMain_UcSearchResults1_lblResultCount">1245</span> Results</strong> -- 
				Page 
				<select name="ctl00$ContentMain$UcSearchResults1$drpPageSelect1" onchange="javascript:setTimeout('__doPostBack(\'ctl00$ContentMain$UcSearchResults1$drpPageSelect1\',\'\')', 0)" id="ctl00_ContentMain_UcSearchResults1_drpPageSelect1">
		<option value="1">1</option>
		<option value="2">2</option>
		<option value="3">3</option>
		<option value="4">4</option>
		<option selected="selected" value="5">5</option>

	</select>
				of <span id="ctl00_ContentMain_UcSearchResults1_lblPageTotal">7</span> 
				-- 
				<a id="ctl00_ContentMain_UcSearchResults1_lnkPrevPage" href="javascript:__doPostBack('ctl00$ContentMain$UcSearchResults1$lnkPrevPage','')" style="color:Black;">[Prev Page]</a>
				<a id="ctl00_ContentMain_UcSearchResults1_lnkNextPage" disabled="disabled" style="color:Gray;">[Next Page]</a>
				--&nbsp;			
				view
				<select name="ctl00$ContentMain$UcSearchResults1$drpPageSize1" onchange="javascript:setTimeout('__doPostBack(\'ctl00$ContentMain$UcSearchResults1$drpPageSize1\',\'\')', 0)" id="ctl00_ContentMain_UcSearchResults1_drpPageSize1">
		<option value="25">25</option>
		<option value="50">50</option>
		<option value="75">75</option>
		<option value="100">100</option>
		<option value="150">150</option>
		<option selected="selected" value="200">200</option>

	</select> per page
			</td>
		</tr>
		<tr id="ctl00_ContentMain_UcSearchResults1_rowMaxResultsMessage" class="selectedItem">
		<td>
				<strong>You have reached the last page of viewable search results. Matweb limits search results to the first 1000 records.</strong>
				<br />To view more materials, please search again using more restrictive search criteria.
			</td>
	</tr>
	
	</table>
	
	
	
	
	<table class="t_ablegrid" style="width:100%;">
		<tr style="font-size:9px;">
			<td style="font-size:9px;">Use Folder</td>
			<td style="font-size:9px;">Contains</td>
			<td style="font-size:9px;">&nbsp;</td>
			
			<td style="font-size:9px;"><div id="divFolderName" style="display:none;">New Folder Name</div></td>
			<td style="font-size:9px;"></td>
			<td style="width:90%"></td>
		</tr>
		<tr style="vertical-align:top;">
			<td style="vertical-align:top;">
				<select name="ctl00$ContentMain$UcSearchResults1$drpFolderList" id="ctl00_ContentMain_UcSearchResults1_drpFolderList" onchange="SetSelectedFolder()" style="width:125px;">
		<option selected="selected" value="0">My Folder</option>

	</select>
			</td>
			<td style="vertical-align:top;">
				<strong><input name="ctl00$ContentMain$UcSearchResults1$txtFolderMatCount" type="text" id="ctl00_ContentMain_UcSearchResults1_txtFolderMatCount" readonly="readonly" style="border:none 0 white; background-color:White; color:Black; width: 35px; font-weight:bold;" value="0/0" /></strong>
			</td>
			<td>
				
				<input type="image" id="btnCompareFolders" src="/images/buttons/btnCompareMatls.gif" alt="Compare Checked Materials" onclick="CompareMaterials();return false;" />
			</td>
			
			<td style="white-space:nowrap;vertical-align:top;">
			
				<table id="tblFolderName" class="tabletight" style="display:none;"><tr><td><input type="text" id="txtFolderName2" style="display:inline; width:100px;" /><br /></td>
				<td><input type="image" id="btnApplyFolderName2" src="/images/buttons/btnSaveChanges.gif" style="display:inline;" onclick="ApplyFolderName();return false;" />
				<input type="image" id="btnHideFolderNameControls2" src="/images/buttons/btnCancel.gif" style="display:inline;" onclick="HideFolderNameControls();return false;" /></td>
				</tr></table>
				
			</td>
			<td style="white-space:nowrap;">
				<div class="usermessage" id="divUserMessage"></div>
			</td>
		</tr>
	</table>

	<table class="tabledataformat t_ablegrid" id="tblResults" style="table-layout:auto;"  >

		<tr class="">

			
		
			<th style="width:65px; white-space:nowrap;">Select</th>

			<th style="width:10px;">
				<!--Discontinued and Found Via Interpolated Indicators -->
			</th>
			
			<th style="width:auto;">
				<a id="ctl00_ContentMain_UcSearchResults1_lnkSortMatName" disabled="disabled" title="Click to Sort By Material Name">Material Name</a>
				<br />
				
				
			</th>
		
			
			
			

			

			

			

			

			

			

			

			
		</tr>

		<!-- this is where the search results are actually loaded.  See code behind -->		
		<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17963"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;801</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17963' href="/search/DataSheet.aspx?MatGUID=6946961616a649dda8e2d6667f1dc28c" >ATI Allegheny Ludlum Alloy 825™ Corrosion Resistant Alloy, UNS N08825</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17976"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;802</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17976' href="/search/DataSheet.aspx?MatGUID=5e1ebe9c0efb42c8926bf28b71c93f93" >Less Common Metals LaNi5 Cast Hydrogen Storage</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18004"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;803</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18004' href="/search/DataSheet.aspx?MatGUID=8bd09c0cb2014d88b7037e25eb8c99ac" >ATI Allegheny Ludlum Moly Permalloy Electrical Alloy</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18005"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;804</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18005' href="/search/DataSheet.aspx?MatGUID=f7c49bf5d307470783df2f4dddee9cd9" >ATI Allegheny Ludlum Alloy G3 Corrosion Resistant Alloy, UNS N06985</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18008"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;805</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18008' href="/search/DataSheet.aspx?MatGUID=ade4c4a11dd54fd3915581689d942c47" >ATI Allegheny Ludlum AL 200™ Nickel Alloy, UNS N02200</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18009"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;806</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18009' href="/search/DataSheet.aspx?MatGUID=2fb5563752f846bb8c8f694e19702c19" >ATI Allegheny Ludlum AL 201™ Nickel Alloy, UNS N02201</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18012"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;807</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18012' href="/search/DataSheet.aspx?MatGUID=fb43df6622cf4a788073897f6bc325f9" >ATI Allegheny Ludlum AL 22™ Nickel-Base Alloy, UNS N06022</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18013"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;808</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18013' href="/search/DataSheet.aspx?MatGUID=106f997231584de988f79a6fec82af27" >ATI Allegheny Ludlum AL 276™ Nickel-Base Alloy, UNS N10276</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18014"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;809</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18014' href="/search/DataSheet.aspx?MatGUID=c09f6da1bf32455aade3a1e351e9e0b0" >ATI Allegheny Ludlum AL 400™ Nickel-Base Alloy, Annealed Plate, Sheet, and Strip, UNS N04400</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18015"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;810</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18015' href="/search/DataSheet.aspx?MatGUID=8291be1d5e3e4c07ac57e1c73c5b01e4" >ATI Allegheny Ludlum AL 400™ Nickel-Base Alloy, Hot Rolled Plate, UNS N04400</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18016"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;811</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18016' href="/search/DataSheet.aspx?MatGUID=f06062a21d4b456885a3321e38a01a21" >ATI Allegheny Ludlum AL 400™ Nickel-Base Alloy, Heavily Cold Rolled Sheet and Strip, UNS N04400</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18017"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;812</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18017' href="/search/DataSheet.aspx?MatGUID=a4b8f9f14b5d4c9a8176c3aa4af71976" >ATI Allegheny Ludlum AL 600™ Nickel-Base Alloy, UNS N06600</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18018"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;813</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18018' href="/search/DataSheet.aspx?MatGUID=56f4bf345d3b4601b13ffefd4eb60996" >ATI Allegheny Ludlum AL 601™ Nickel-Base Alloy, Annealed at 982°C (1800°F), UNS N06601</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18019"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;814</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18019' href="/search/DataSheet.aspx?MatGUID=9689b81526a0455a82619279feb8737d" >ATI Allegheny Ludlum AL 601™ Nickel-Base Alloy, 1149°C (2100°F) Solution Treated, UNS N06601</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18020"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;815</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18020' href="/search/DataSheet.aspx?MatGUID=26b11513b2894c918c3bef53134e2972" >ATI Allegheny Ludlum AL Altemp® 625 Nickel-Base Superalloy, UNS N06625</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18021"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;816</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18021' href="/search/DataSheet.aspx?MatGUID=ee4326fa99f64053976aa7ddd461085b" >ATI Allegheny Ludlum Altemp® 718 Nickel-Base Superalloy, Solution Treated Sheet and Strip, UNS N07718</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18022"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;817</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18022' href="/search/DataSheet.aspx?MatGUID=c165aa62d21242219ed9dd750c881d99" >ATI Allegheny Ludlum Altemp® 718 Nickel-Base Superalloy, Solution Treated Plate, UNS N07718</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18023"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;818</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18023' href="/search/DataSheet.aspx?MatGUID=0e1ff5bdfa284b44870656d8d18d863d" >ATI Allegheny Ludlum Altemp® 718 Nickel-Base Superalloy, Solution Treated plus Precipitation Heat Treated, UNS N07718</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18024"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;819</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18024' href="/search/DataSheet.aspx?MatGUID=09ab2e0bf816492088b489c15a1a0564" >ATI Allegheny Ludlum AL 800™ Nickel-Base Alloy, UNS N08800</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18025"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;820</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18025' href="/search/DataSheet.aspx?MatGUID=5346552f19c74448aa0c28f288b010b4" >ATI Allegheny Ludlum AL 800H™ Nickel-Base Alloy, UNS N08810</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18026"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;821</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18026' href="/search/DataSheet.aspx?MatGUID=d59cb163e806401f809f98a0e01be52a" >ATI Allegheny Ludlum AL 825™ Nickel Alloy</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18027"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;822</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18027' href="/search/DataSheet.aspx?MatGUID=789150f6b06b4a369926cd35d0aca60d" >ATI Allegheny Ludlum AL 800AT™ Nickel-Base Alloy, UNS N08811</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_254769"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;823</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_254769' href="/search/DataSheet.aspx?MatGUID=2b5c44e2938748ee861d04aebdc1bab1" >Lucas-Milhaupt HI-TEMP® 820 Nickel-Chromium-Silicon-Boron-Iron Brazing Alloy</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_254770"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;824</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_254770' href="/search/DataSheet.aspx?MatGUID=a05bb1c8131f4bb594e3f31e0cc8ee03" >Lucas-Milhaupt HI-TEMP® 910 Nickel-Silicon-Boron-Iron Brazing Alloy (AWS BNi-3)</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_254801"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;825</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_254801' href="/search/DataSheet.aspx?MatGUID=b2fa280c48794aacb202debf057a52a8" >Lucas-Milhaupt HI-TEMP® 930 Nickel-Silicon-Boron-Iron Brazing Alloy (AWS BNi-4)</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_254803"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;826</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_254803' href="/search/DataSheet.aspx?MatGUID=e07479fdec2e45baaa42fb3403911ad4" >Lucas-Milhaupt HI-TEMP® 933 Nickel-Chromium-Phosphorus Brazing Alloy Powder</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18160"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;827</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18160' href="/search/DataSheet.aspx?MatGUID=aaf84234f1e54e4eb7f0a549d3b8adce" >Ametek PF20 Nickel Alloy Thermal Spray Powder</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18161"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;828</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18161' href="/search/DataSheet.aspx?MatGUID=64060c97ff174f1d87c3c1196899b62f" >Ametek PF25 Nickel Alloy Thermal Spray Powder</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18162"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;829</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18162' href="/search/DataSheet.aspx?MatGUID=6aefbea4e375408780c7d45019c5fd42" >Ametek PF35 Nickel Alloy Thermal Spray Powder</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18163"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;830</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18163' href="/search/DataSheet.aspx?MatGUID=754d16e835394685ab6535507cee6874" >Ametek PF40 Nickel Alloy Thermal Spray Powder</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18164"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;831</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18164' href="/search/DataSheet.aspx?MatGUID=4350da6ec8474399a7a57ca10af6dc28" >Ametek PF50 Nickel Alloy Thermal Spray Powder</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18165"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;832</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18165' href="/search/DataSheet.aspx?MatGUID=5f0bc0282d704ac2a6df35b16c9e5c64" >Ametek PF60 Nickel Alloy Thermal Spray Powder</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18166"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;833</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18166' href="/search/DataSheet.aspx?MatGUID=59246088d0d147e09814ffbd146d8940" >Ametek PI600 Nickel Alloy Thermal Spray Powder</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18194"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;834</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18194' href="/search/DataSheet.aspx?MatGUID=71d1b37f6c0049fc91dacb1476127155" >Ametek High Purity Nickel Strip 899A Annealed</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18195"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;835</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18195' href="/search/DataSheet.aspx?MatGUID=81e844561f8f4177a24966c36a788b15" >Ametek High Purity Nickel Strip 899A Skin Hard</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18196"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;836</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18196' href="/search/DataSheet.aspx?MatGUID=74741702eaeb47dcb7e0901bfe0f5458" >Ametek High Purity Nickel Strip 899A 1/4 Hard</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18197"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;837</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18197' href="/search/DataSheet.aspx?MatGUID=8bdcf2446a1c4362a57d06168880f285" >Ametek High Purity Nickel Strip 899A 1/2 Hard</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18198"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;838</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18198' href="/search/DataSheet.aspx?MatGUID=2d3450bcedf74538bfee695bec01471e" >Ametek High Purity Nickel Strip 899A 3/4 Hard</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18199"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;839</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18199' href="/search/DataSheet.aspx?MatGUID=aed944a24d6b4c86a165b36e149904a8" >Ametek High Purity Nickel Strip 899A Hard</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18200"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;840</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18200' href="/search/DataSheet.aspx?MatGUID=7430e6e8dcfb484794278d7d3f89025c" >Ametek High Purity Nickel Strip 899A Full Hard</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18201"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;841</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18201' href="/search/DataSheet.aspx?MatGUID=889071e4064a474db53c760f85e34716" >Ametek High Purity Nickel Strip 899L Annealed</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18202"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;842</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18202' href="/search/DataSheet.aspx?MatGUID=4630d7dbd8a74a568ab540cc4953d82a" >Ametek High Purity Nickel Strip 899L Skin Hard</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18203"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;843</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18203' href="/search/DataSheet.aspx?MatGUID=4803889440344d03b7948234019daf47" >Ametek High Purity Nickel Strip 899L 1/4 Hard</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18204"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;844</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18204' href="/search/DataSheet.aspx?MatGUID=2045a82c32ae4bc884ceb82f8530329a" >Ametek High Purity Nickel Strip 899L 1/2 Hard</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18205"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;845</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18205' href="/search/DataSheet.aspx?MatGUID=6e0fa9bcb01d48e6a7b6e64cfbf8dd29" >Ametek High Purity Nickel Strip 899L 3/4 Hard</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18206"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;846</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18206' href="/search/DataSheet.aspx?MatGUID=6aa8bab17e88443086ad4bc4dbbe86b9" >Ametek High Purity Nickel Strip 899L Hard</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18207"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;847</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18207' href="/search/DataSheet.aspx?MatGUID=d7c04d45eb5446a1b202c9ea7d502907" >Ametek High Purity Nickel Strip 899L Full Hard</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18208"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;848</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18208' href="/search/DataSheet.aspx?MatGUID=613235d95da4456b808c7c81a569f1ff" >Ametek High Purity Nickel Strip 899M Annealed</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18209"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;849</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18209' href="/search/DataSheet.aspx?MatGUID=0bdd72a0167c48c48ebf18073f728de7" >Ametek High Purity Nickel Strip 899M Skin Hard</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18210"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;850</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18210' href="/search/DataSheet.aspx?MatGUID=9411908f620b4e5c815cb56231dbf684" >Ametek High Purity Nickel Strip 899M 1/4 Hard</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18211"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;851</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18211' href="/search/DataSheet.aspx?MatGUID=957fb9b7b6f04837bd04710c17ceb062" >Ametek High Purity Nickel Strip 899M 1/2 Hard</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18212"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;852</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18212' href="/search/DataSheet.aspx?MatGUID=988289a427394c28b107b69fce7069c5" >Ametek High Purity Nickel Strip 899M 3/4 Hard</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18213"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;853</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18213' href="/search/DataSheet.aspx?MatGUID=adb54dd5ba864207b7697a11787ca0bb" >Ametek High Purity Nickel Strip 899M Hard</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18214"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;854</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18214' href="/search/DataSheet.aspx?MatGUID=33f3178d762d4e9f8fddfc22406567c7" >Ametek High Purity Nickel Strip 899M Full Hard</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18215"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;855</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18215' href="/search/DataSheet.aspx?MatGUID=6ff5489dc63b45e0af7ceb548df9f23f" >Ametek High Purity Nickel Strip 899D Annealed</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18216"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;856</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18216' href="/search/DataSheet.aspx?MatGUID=0ae97ab954204ccfa6104a7e3080b683" >Ametek High Purity Nickel Strip 899D 50% Cold-Rolled</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18217"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;857</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18217' href="/search/DataSheet.aspx?MatGUID=f679bd559e124cd28dfa25592f1fbada" >Ametek High Purity Nickel Strip 899E Annealed</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18218"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;858</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18218' href="/search/DataSheet.aspx?MatGUID=f16eebe405bf4986a1db565a0dc9fb75" >Ametek High Purity Nickel Strip 899E 50% Cold-Rolled</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18219"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;859</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18219' href="/search/DataSheet.aspx?MatGUID=204f9dbb924f49faa28ab92a79800cd0" >Ametek High Purity Nickel Strip 899G Annealed</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18220"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;860</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18220' href="/search/DataSheet.aspx?MatGUID=adee2bbef4884200bb085b0c5ad96d98" >Ametek High Purity Nickel Strip 899G 50% Cold-Rolled</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18355"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;861</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18355' href="/search/DataSheet.aspx?MatGUID=4b60584a0d034a4fa092d3754eb4593a" >MetalTek MTEK 50-50Nb Cast UNS R20501 Heat Resistant Alloy</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18372"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;862</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18372' href="/search/DataSheet.aspx?MatGUID=fc529205b32a44dba224cc527cb35d36" >MetalTek MTEK CZ-100 Cast UNS N02100 Severe Corrosion and Heat Resistant Alloy</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18373"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;863</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18373' href="/search/DataSheet.aspx?MatGUID=c56bb5c00b2149e5b26139eb63b2c0e0" >MetalTek MTEK N-7M Cast UNS N30007 Severe Corrosion and Heat Resistant Alloy</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18374"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;864</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18374' href="/search/DataSheet.aspx?MatGUID=62476181ee3741429c49d488a6ba3262" >MetalTek MTEK N-12MV Cast UNS N30012 Severe Corrosion and Heat Resistant Alloy</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18375"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;865</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18375' href="/search/DataSheet.aspx?MatGUID=7b2c56b40c47492da2b2f784cceee193" >MetalTek MTEK CW-12MW Cast UNS N30002 Severe Corrosion and Heat Resistant Alloy</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18376"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;866</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18376' href="/search/DataSheet.aspx?MatGUID=949336a39d8a4c8893c73915f12c99a6" >MetalTek MTEK CW-2M Cast UNS N26455 Severe Corrosion and Heat Resistant Alloy</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18377"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;867</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18377' href="/search/DataSheet.aspx?MatGUID=12e2e71648fc4d4ba154f04877a444bf" >MetalTek MTEK CX-2MW Cast UNS N26022 Severe Corrosion and Heat Resistant Alloy</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18378"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;868</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18378' href="/search/DataSheet.aspx?MatGUID=d922358f2a7b48269b18071afbffa933" >MetalTek MTEK ALLOY X Cast UNS N06002 Severe Corrosion and Heat Resistant Alloy</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18379"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;869</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18379' href="/search/DataSheet.aspx?MatGUID=dd41c5d9001f4c018bb3696083b5d6fe" >MetalTek HASTELLOY® S Severe Corrosion and Heat Resistant Alloy</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18380"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;870</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18380' href="/search/DataSheet.aspx?MatGUID=0d31dbaa78da433fb5e2a04df9e04b4a" >MetalTek HAYNES® HR120 Severe Corrosion and Heat Resistant Alloy</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18381"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;871</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18381' href="/search/DataSheet.aspx?MatGUID=30094a568255422fbfcfa773cc9e89ef" >MetalTek HAYNES® 230 Severe Corrosion and Heat Resistant Alloy</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18382"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;872</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18382' href="/search/DataSheet.aspx?MatGUID=d0518ee9d828427abdaa3114577fb213" >MetalTek MTEK CY-40 Cast UNS N06040 Severe Corrosion and Heat Resistant Alloy</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18383"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;873</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18383' href="/search/DataSheet.aspx?MatGUID=211a6fa5df0f4e36a5f47db8bb0255e6" >MetalTek MTEK 617 Severe Corrosion and Heat Resistant Alloy</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18384"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;874</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18384' href="/search/DataSheet.aspx?MatGUID=748762b96b494162b4b6a2f2c0a1b1f1" >MetalTek MTEK 625 Cast UNS N26625 Severe Corrosion and Heat Resistant Alloy</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18385"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;875</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18385' href="/search/DataSheet.aspx?MatGUID=7d370934d45040d8bf607c28b0feb38d" >MetalTek MTEK 718 Cast UNS N07718 Severe Corrosion and Heat Resistant Alloy</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18386"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;876</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18386' href="/search/DataSheet.aspx?MatGUID=73678a7c19a247008abae0342eb826a6" >MetalTek MTEK 825 Cast UNS N08826 Severe Corrosion and Heat Resistant Alloy</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18388"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;877</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18388' href="/search/DataSheet.aspx?MatGUID=0288be30c663406a8451b08fa3df349e" >MetalTek MTEK A Cast UNS N04020 Nickel-Copper Alloy</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18389"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;878</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18389' href="/search/DataSheet.aspx?MatGUID=392a97f6f2df4557a637dc47569468cf" >MetalTek MTEK B Cast UNS N24030 Nickel-Copper Alloy</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18390"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;879</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18390' href="/search/DataSheet.aspx?MatGUID=6d7a01b0f34044fab72b40c0497e6798" >MetalTek MTEK D Cast UNS N04019 Nickel-Copper Alloy</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18391"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;880</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18391' href="/search/DataSheet.aspx?MatGUID=ceac812ecc444f65be5ba69cdeacc41c" >MetalTek MTEK E Cast UNS N24130 Nickel-Copper Alloy</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18392"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;881</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18392' href="/search/DataSheet.aspx?MatGUID=46ad639b1a5940cca02d9122e5554e40" >MetalTek MTEK 70-30 UNS C96400 Nickel-Copper Alloy</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18397"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;882</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18397' href="/search/DataSheet.aspx?MatGUID=35dfbf1296214897966f4f2d1ceb7d0c" >MetalTek IN® 718 UNS N07718 Centri-Vac Nickel Cobalt Based Alloy</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18398"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;883</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18398' href="/search/DataSheet.aspx?MatGUID=b52264e65be44a318a94303797e35cad" >MetalTek IN® 909 Centri-Vac Nickel Cobalt Based Alloy</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18399"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;884</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18399' href="/search/DataSheet.aspx?MatGUID=705da93caed547be8ebe479a1c440f6f" >MetalTek GTD 222® Centri-Vac Nickel Cobalt Based Alloy</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18400"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;885</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18400' href="/search/DataSheet.aspx?MatGUID=b3236cf354eb408fb5b05b92c0ba6862" >MetalTek IN® 100 UNS N13100 Centri-Vac Nickel Based Alloy</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18401"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;886</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18401' href="/search/DataSheet.aspx?MatGUID=0db079605be04aa0af70334dc01528e5" >MetalTek IN® 713 UNS N07713 Centri-Vac Nickel Cobalt Based Alloy</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18404"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;887</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18404' href="/search/DataSheet.aspx?MatGUID=89c621b433f348689e746f503b66126c" >MetalTek MAR-M-247® Centri-Vac Nickel Cobalt Based Alloy</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18405"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;888</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18405' href="/search/DataSheet.aspx?MatGUID=10c55bc1e84542adb8c6c3fd39837390" >MetalTek RENE® 77 Centri-Vac Nickel Cobalt Based Alloy</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18406"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;889</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18406' href="/search/DataSheet.aspx?MatGUID=3c9002d9f3d543268174233ba14f7e5b" >MetalTek RENE® 220 Centri-Vac Nickel Cobalt Based Alloy</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18407"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;890</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18407' href="/search/DataSheet.aspx?MatGUID=f52f0a5f7d614742b8c52ae9769fc445" >MetalTek WASPALOY® Centri-Vac Nickel Cobalt Based Alloy</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18442"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;891</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18442' href="/search/DataSheet.aspx?MatGUID=42ed96d7d6fc4516b2b39ce9f8b75711" >Standex Engraving Tribocoat® Electroless Nickel Teflon Mold Coating</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_107059"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;892</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_107059' href="/search/DataSheet.aspx?MatGUID=416b3d030a644e0ab1fff3b82a07c46b" >NiCoForm NiColoy® E Electroformed Nickel-Cobalt</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18610"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;893</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18610' href="/search/DataSheet.aspx?MatGUID=cdea369777b14218b0c8f9c1fb6f864b" >Sandvik Osprey 80/20 Ni Cr Powder (Grade 80% - 22)</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18611"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;894</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18611' href="/search/DataSheet.aspx?MatGUID=d63f32038f3345898edb44b7b0e003c2" >Sandvik Osprey 80/20 Ni Cr Powder (Grade 90% - 22)</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18612"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;895</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18612' href="/search/DataSheet.aspx?MatGUID=2e81e4cebc654bd3ad9629cfb4d3e4fb" >Sandvik Osprey 80/20 Ni Cr Powder (Grade 90% - 16)</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18613"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;896</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18613' href="/search/DataSheet.aspx?MatGUID=cc2f0b5ccefc4b8d89f1cce65dffbc6a" >Sandvik Osprey 80/20 Ni Cr Powder (Grade 95% - 16)</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18670"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;897</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18670' href="/search/DataSheet.aspx?MatGUID=31878bd5af994b3486ce51ee7ee5b6a1" >Sandvik Osprey Fe50Ni Binary Alloy Powder (Grade 80% - 22)</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18671"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;898</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18671' href="/search/DataSheet.aspx?MatGUID=097fc08653b94dc497895ce915b902e6" >Sandvik Osprey Fe50Ni Binary Alloy Powder (Grade 90% - 22)</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18672"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;899</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18672' href="/search/DataSheet.aspx?MatGUID=4a010a1c7b5742ee82d42e7c8a7f5e16" >Sandvik Osprey Fe50Ni Binary Alloy Powder (Grade 90% - 16)</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18673"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;900</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18673' href="/search/DataSheet.aspx?MatGUID=377f7d9d6f2548109ae0405bec3d366b" >Sandvik Osprey Fe50Ni Binary Alloy Powder (Grade 95% - 16)</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18679"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;901</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18679' href="/search/DataSheet.aspx?MatGUID=e781cc00677d4756a22573ed378f1cc0" >Sandvik Osprey HTN 1A Brazing Powder</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18680"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;902</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18680' href="/search/DataSheet.aspx?MatGUID=4003528073a84d248f374bc60093b230" >Sandvik Osprey HTN 2 Brazing Powder</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18681"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;903</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18681' href="/search/DataSheet.aspx?MatGUID=25f1a957667e4db0aa368f4388068101" >Sandvik Osprey HTN 3 Brazing Powder</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18682"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;904</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18682' href="/search/DataSheet.aspx?MatGUID=86f6c88b5d984bf7b4d677e2abbe4537" >Sandvik Osprey HTN 4 Brazing Powder</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18683"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;905</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18683' href="/search/DataSheet.aspx?MatGUID=aa79a37f97c74f87b9c9460356e123ec" >Sandvik Osprey HTN 5 Brazing Powder</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18684"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;906</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18684' href="/search/DataSheet.aspx?MatGUID=b76c54d443de41579e9636adbbbd8e67" >Sandvik Osprey HTN 6 Brazing Powder</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18685"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;907</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18685' href="/search/DataSheet.aspx?MatGUID=a46292233f744685ac3ac56b8c121014" >Sandvik Osprey HTN 7 Brazing Powder</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18686"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;908</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18686' href="/search/DataSheet.aspx?MatGUID=10c19cfa62d84f6aa7f53fc0d7700334" >Sandvik Osprey HTN 8 Brazing Powder</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18687"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;909</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18687' href="/search/DataSheet.aspx?MatGUID=1004333faa034ccd9b80fdcff5915438" >Sandvik Osprey HTN 9 Brazing Powder</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18696"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;910</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18696' href="/search/DataSheet.aspx?MatGUID=d4a803140dcb4a1caa0043bfcb5d0e7c" >Sandvik Osprey Ni 401 Brazing Powder</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18697"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;911</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18697' href="/search/DataSheet.aspx?MatGUID=9c1db190f6284792977d7c79a218ae17" >Sandvik Osprey Ni40Cu Binary Alloy Powder (Grade 80% - 22)</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18698"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;912</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18698' href="/search/DataSheet.aspx?MatGUID=04afb9f37d4146a3881187fe99402a41" >Sandvik Osprey Ni40Cu Binary Alloy Powder (Grade 90% - 22)</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18699"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;913</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18699' href="/search/DataSheet.aspx?MatGUID=15e137a673664e689bcff51b93b73331" >Sandvik Osprey Ni40Cu Binary Alloy Powder (Grade 90% - 16)</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18700"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;914</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18700' href="/search/DataSheet.aspx?MatGUID=9589cde15a9a44c5bfe5c0c16b6f87f7" >Sandvik Osprey Ni40Cu Binary Alloy Powder (Grade 95% - 16)</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_86174"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;915</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_86174' href="/search/DataSheet.aspx?MatGUID=2134ef74c6344197955a801f9fa74a87" >PSM Industries Pacific Sintered Metals FN-250-S Nickel Iron Powdered Metal (MPIF FN-0200-20)</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_86175"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;916</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_86175' href="/search/DataSheet.aspx?MatGUID=5f4cbb8e11a14973b9a5eb75c34db125" >PSM Industries Pacific Sintered Metals FN-255-S Nickel Steel Powdered Metal (MPIF FN-0200-25)</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_86176"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;917</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_86176' href="/search/DataSheet.aspx?MatGUID=93799f56e2a9467e8daf19654818019a" >PSM Industries Pacific Sintered Metals FN-255-S(HT) Nickel Steel Powdered Metal (MPIF FN-0208-130HT)</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_86177"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;918</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_86177' href="/search/DataSheet.aspx?MatGUID=4a7a3c88b43f4239ba5a0b99e00f76a3" >PSM Industries Pacific Sintered Metals FN-262-S High Strength Nickel Steel Powdered Metal (MPIF FN-0405-35)</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_86178"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;919</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_86178' href="/search/DataSheet.aspx?MatGUID=eb61a7cb1ecc49f583c91ad549eb4d4a" >PSM Industries Pacific Sintered Metals FN-262-T High Strength Nickel Steel Powdered Metal (MPIF FN-0405-45)</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_86179"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;920</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_86179' href="/search/DataSheet.aspx?MatGUID=b2794c3b5f94456a8590105602085d81" >PSM Industries Pacific Sintered Metals FN-262-T(HT) High Strength Nickel Steel Powdered Metal (MPIF FN-0408-155HT)</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_194797"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;921</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_194797' href="/search/DataSheet.aspx?MatGUID=4c5bb0a36eaa4ed5ac4fd76f0e840963" >Aperam Permimphy SP Cold Rolled Strip</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_194798"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;922</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_194798' href="/search/DataSheet.aspx?MatGUID=cb9d643531434d95808c7cef0ca26c4f" >Aperam Supermimphy® L Cold Rolled Strip</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_194799"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;923</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_194799' href="/search/DataSheet.aspx?MatGUID=1f7d475eaaac45659312da5fc28450a9" >Aperam Supermimphy® LLS Cold Rolled Strip</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_194800"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;924</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_194800' href="/search/DataSheet.aspx?MatGUID=2f86c57b1dd04ec89ed3f4db644e4488" >Aperam Supermimphy® T Cold Rolled Strip</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_194807"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;925</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_194807' href="/search/DataSheet.aspx?MatGUID=5f7e719ed7254429a59aa7cbfd77aa36" >Aperam Superimphy® 22 Bar-Corrosion application</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_194808"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;926</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_194808' href="/search/DataSheet.aspx?MatGUID=88f32ec7011b487bbeaa20b21cbbd10d" >Aperam Superimphy® 90 Bar-Corrosion application</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_194809"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;927</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_194809' href="/search/DataSheet.aspx?MatGUID=38503c5ace564b828483b2e1c375887e" >Aperam Superimphy® 276 Bar-Corrosion application</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_194813"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;928</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_194813' href="/search/DataSheet.aspx?MatGUID=d95b62803ba140849ddf898a0983a0e9" >Aperam Superimphy® 600 Bar-Corrosion application</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_194820"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;929</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_194820' href="/search/DataSheet.aspx?MatGUID=d465ad35b9e44729a06a1d71115860d5" >Aperam Phyweld® 400 Bar-Corrosion application</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_194821"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;930</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_194821' href="/search/DataSheet.aspx?MatGUID=e8da773d216a40f18fd7a3ce194a84e1" >Aperam Superimphy® 750 Bar-Corrosion application</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_194823"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;931</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_194823' href="/search/DataSheet.aspx?MatGUID=6b6979c6e8034c2494fb85bc5efc6e9d" >Aperam Nicrimphy® 600 Cold Rolled Strip</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_194825"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;932</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_194825' href="/search/DataSheet.aspx?MatGUID=7f18af1ebd994a56832ff233ab074797" >Aperam Superimphy® 625 Heat and Corrosion Resistant Nickel Alloy</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_194869"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;933</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_194869' href="/search/DataSheet.aspx?MatGUID=68d95fd036164e52928f4d299abbd49e" >Aperam Resistohm® 80 Resistant Alloys-Nickel Chromium Alloys</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_194870"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;934</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_194870' href="/search/DataSheet.aspx?MatGUID=a5936c458e3a42d88b7eaaf87dc95c76" >Aperam Resistohm® 70 Resistant Alloys-Nickel Chromium Alloys</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_194871"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;935</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_194871' href="/search/DataSheet.aspx?MatGUID=a35de7ce474d45b18e985b20a42dcee0" >Aperam Resistohm® 60 Resistant Alloys-Nickel Chromium Alloys</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_194887"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;936</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_194887' href="/search/DataSheet.aspx?MatGUID=d6c8cf936eb04cb7b45afd60b2ce884f" >Aperam Nickel 200 Resistant Alloys Nickel Alloys</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_194888"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;937</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_194888' href="/search/DataSheet.aspx?MatGUID=c263aa575ab44a08a9b4acf37771a32b" >Aperam Nickel 201 Resistant Alloys Nickel Alloys</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_194889"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;938</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_194889' href="/search/DataSheet.aspx?MatGUID=02c1f4e9e4c7421db6d6b6e110c21901" >Aperam Nickel 212 Resistant Alloys Nickel Alloys</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18782"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;939</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18782' href="/search/DataSheet.aspx?MatGUID=ac84b1f3d4fe4d72813bc6ca0e125980" >HP Alloys Alloy C-276, 10% Cold Worked</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18783"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;940</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18783' href="/search/DataSheet.aspx?MatGUID=9ae4e75f94a24bdcbbf8d0cb93c11076" >HP Alloys Alloy C-276, 20% Cold Worked</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18784"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;941</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18784' href="/search/DataSheet.aspx?MatGUID=ecd29484c7fe456c91cfbe096700341a" >HP Alloys Alloy C-276, 30% Cold Worked</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18785"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;942</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18785' href="/search/DataSheet.aspx?MatGUID=3ca76e181ea74882956bb48764a3623a" >HP Alloys Alloy C-276, 40% Cold Worked</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18786"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;943</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18786' href="/search/DataSheet.aspx?MatGUID=e7c10be25ef54855a0556ea14234785c" >HP Alloys Alloy C-276, 50% Cold Worked</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18787"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;944</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18787' href="/search/DataSheet.aspx?MatGUID=c33f4c4d12fd4fe09101c45b86c16c8f" >HP Alloys Alloy 718, 0% Cold Worked, Not Aged</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18788"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;945</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18788' href="/search/DataSheet.aspx?MatGUID=78eadb60f15d4ae98e304b3b3048610c" >HP Alloys Alloy 718, 5% Cold Worked</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18789"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;946</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18789' href="/search/DataSheet.aspx?MatGUID=76a10d4292954e74ba6a0a6041e317fd" >HP Alloys Alloy 718, 10% Cold Worked</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18790"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;947</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18790' href="/search/DataSheet.aspx?MatGUID=53ff931f31354b4fbcb6813b39cf5adf" >HP Alloys Alloy 718, 15% Cold Worked</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18791"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;948</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18791' href="/search/DataSheet.aspx?MatGUID=14af93dea8fc41f5b6ce6905cd69d18e" >HP Alloys Alloy 718, 20% Cold Worked</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18792"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;949</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18792' href="/search/DataSheet.aspx?MatGUID=0b5eb4185235417ea2222f0896031628" >HP Alloys Alloy 718, 25% Cold Worked</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18793"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;950</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18793' href="/search/DataSheet.aspx?MatGUID=415838c995fa4588949e6d606a0e9edb" >HP Alloys Alloy 718, 30% Cold Worked</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18794"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;951</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18794' href="/search/DataSheet.aspx?MatGUID=49faf3758e5e4903a841446e6a88bf13" >HP Alloys Alloy 718, 50% Cold Worked</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18795"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;952</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18795' href="/search/DataSheet.aspx?MatGUID=cfb38b2fddd84222b98b3f5baaa829bb" >HP Alloys Alloy 718, 5% Cold Worked, Aged</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18796"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;953</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18796' href="/search/DataSheet.aspx?MatGUID=bc8212ea7b4b4af7afd292fb73a1b350" >HP Alloys Alloy 718, 10% Cold Worked, Aged</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18797"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;954</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18797' href="/search/DataSheet.aspx?MatGUID=d0bd906a4d074398ae5030abdd447793" >HP Alloys Alloy 718, 15% Cold Worked, Aged</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18798"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;955</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18798' href="/search/DataSheet.aspx?MatGUID=58b3965b3be6466683e5ee2fb7db7802" >HP Alloys Alloy 718, 20% Cold Worked, Aged</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18799"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;956</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18799' href="/search/DataSheet.aspx?MatGUID=0fae7d6503c74ec2ab566b79c42817fa" >HP Alloys Alloy 718, 25% Cold Worked, Aged</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18800"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;957</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18800' href="/search/DataSheet.aspx?MatGUID=ca6769311f7f457da50767e7c3396c10" >HP Alloys Alloy 718, 30% Cold Worked, Aged</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18801"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;958</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18801' href="/search/DataSheet.aspx?MatGUID=094801117c804a8f8bb416e5d9e5883e" >HP Alloys Alloy 718, 50% Cold Worked, Aged</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18802"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;959</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18802' href="/search/DataSheet.aspx?MatGUID=06c9421073784e8ba570ba7c27e85e03" >HP Alloys Alloy B-2, Solution Treated, 10% Cold Worked</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18803"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;960</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18803' href="/search/DataSheet.aspx?MatGUID=151356fca556418b941f2980e1ea94eb" >HP Alloys Alloy B-2, Solution Treated, 20% Cold Worked</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18804"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;961</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18804' href="/search/DataSheet.aspx?MatGUID=ed2e524dd0af405cb797467a6780d07b" >HP Alloys Alloy B-2, Solution Treated, 30% Cold Worked</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18805"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;962</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18805' href="/search/DataSheet.aspx?MatGUID=ed935f4a84944262aa1beeba19cc4a1b" >HP Alloys Alloy B-2, Solution Treated, 40% Cold Worked</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18806"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;963</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18806' href="/search/DataSheet.aspx?MatGUID=f2d8422f7e4741f9abbf437b727d7a42" >HP Alloys Alloy B-2, Solution Treated, 50% Cold Worked</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18807"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;964</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18807' href="/search/DataSheet.aspx?MatGUID=2b9f8fd5cb124241a83b895aa54f6296" >HP Alloys Alloy X, 5% Cold Worked</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18808"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;965</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18808' href="/search/DataSheet.aspx?MatGUID=5adbae3ecc044c7ca9a6b38f65cafab1" >HP Alloys Alloy X, 15% Cold Worked</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18809"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;966</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18809' href="/search/DataSheet.aspx?MatGUID=61dcd32f5ad54da39073a13cb2f3c770" >HP Alloys Alloy X, 30% Cold Worked</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18810"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;967</td><td style="width:auto;"><img src="/images/buttons/iconDiscontinued.jpg" alt="Discontinued" title="Discontinued"  /></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18810' href="/search/DataSheet.aspx?MatGUID=38184f5f192a466bb09dadcf803cbd9f" >Schwarzkopf Plansee PM 1000 ODS Nickel Alloy</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_161973"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;968</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_161973' href="/search/DataSheet.aspx?MatGUID=7b4da03d52054764aa12814c079678d7" >Potters Industries Conduct-O-Fil® SN40P18 Silver-coated Nickel Granules</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_161974"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;969</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_161974' href="/search/DataSheet.aspx?MatGUID=1e6bc4528fc8430c8a06f5e7a61c2c64" >Potters Industries Conduct-O-Fil® SN325P25-ALT1 Silver-coated Nickel Granules</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_161975"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;970</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_161975' href="/search/DataSheet.aspx?MatGUID=47643c5ea81e4842becf3210c323ca26" >Potters Industries Conduct-O-Fil® SN325P25-LD Silver-coated Nickel Granules</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_322916"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;971</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_322916' href="/search/DataSheet.aspx?MatGUID=396a826bbe9345ee90b46ebf83ceca01" >EOS NickelAlloy IN625 Powder Processed by DMSL on EOSINT M 270</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_322917"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;972</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_322917' href="/search/DataSheet.aspx?MatGUID=dd4726063acf4a489c30d4f1c8cd75e2" >EOS NickelAlloy IN718 Powder Processed by DMSL</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_232386"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;973</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_232386' href="/search/DataSheet.aspx?MatGUID=ccb8342b6b8b440e9bd2cc6534e985a2" >Proto Labs Inconel 718 - DMLS</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_93216"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;974</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_93216' href="/search/DataSheet.aspx?MatGUID=4fc7867191cd4ab4bf4c29c3b0627201" >PSM Industries Ferro-TiC® HT-6A Age Hardenable Nickel Base</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18827"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;975</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18827' href="/search/DataSheet.aspx?MatGUID=eb55974581ca4a74b3d72aed2ba8c4cd" >Resistalloy International Resistalloy 8020 Nickel Chromium Electrical Resistance Alloy</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18828"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;976</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18828' href="/search/DataSheet.aspx?MatGUID=323ff1e1068446d5b566fc96156d7717" >Resistalloy International Resistalloy 6015 Nickel Chromium Electrical Resistance Alloy</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18829"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;977</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18829' href="/search/DataSheet.aspx?MatGUID=f7c8873474bd41ea88f53e9ebd2dfcab" >Resistalloy International Resistalloy 3718 Nickel Chromium Electrical Resistance Alloy</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18888"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;978</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18888' href="/search/DataSheet.aspx?MatGUID=b9f8661d375c4bf5b5d11670b45a1c11" >Kennametal Stellite Tribaloy® 745</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18951"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;979</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18951' href="/search/DataSheet.aspx?MatGUID=d53639fdb004414881e34b156cb21cff" >Sandvik Nikrothal 60 Resistance Ribbon</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18952"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;980</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18952' href="/search/DataSheet.aspx?MatGUID=9e9a2447b3a147c3ab3283df58c8e1fb" >Sandvik Nikrothal 80 Resistance Ribbon</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18953"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;981</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18953' href="/search/DataSheet.aspx?MatGUID=1076caf9e602414595a1949a4e18a07a" >Sandvik KANTHAL 100% Nickel Resistance Strip</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18962"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;982</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18962' href="/search/DataSheet.aspx?MatGUID=45a3969f8e54462093e9d191dfc2c079" >Sandvik Nikrothal 60 Resistance Strip</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18963"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;983</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18963' href="/search/DataSheet.aspx?MatGUID=8c8abb1df4d1493fa61155a5ca8b6ee8" >Sandvik Nikrothal 70 Resistance Strip</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18970"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;984</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18970' href="/search/DataSheet.aspx?MatGUID=a4bbf143da764b58818974b42ff2f529" >Sandvik Nikrothal 60 Resistance Wire</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18971"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;985</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18971' href="/search/DataSheet.aspx?MatGUID=629d93f3e17f4084a3c1c57c4cf9650d" >Sandvik Nikrothal 80 Resistance Wire</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18972"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;986</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18972' href="/search/DataSheet.aspx?MatGUID=ad39a4b48a7445cfb54fc14fc37b8c9d" >Sandvik Nikrothal 80/20 Cb Resistance Wire</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18973"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;987</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18973' href="/search/DataSheet.aspx?MatGUID=2fe0f49f03284ec6b6fa823e293b31b0" >Sandvik Nikrothal 70 Resistance Wire</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18977"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;988</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18977' href="/search/DataSheet.aspx?MatGUID=b616c3035eb6425e915aaffe04c2bb92" >Sandvik Nikrothal LX Resistance Wire</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18983"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;989</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18983' href="/search/DataSheet.aspx?MatGUID=a52d2327a65144c28391859f4ef09afd" >Sandvik Nifethal 70 Resistance Wire</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18984"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;990</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18984' href="/search/DataSheet.aspx?MatGUID=f805af43f5a348afbb24ecbc76bb56f2" >Sandvik Nifethal 52 Resistance Wire</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18997"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;991</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18997' href="/search/DataSheet.aspx?MatGUID=87845881c44c481a98a8b83141f6a04c" >Sandvik Nisil Thermocouple Alloy</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18998"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;992</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18998' href="/search/DataSheet.aspx?MatGUID=4f2998dbef3b441c8872ef42510bc8b1" >Sandvik Nicrosil Thermocouple</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_19000"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;993</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_19000' href="/search/DataSheet.aspx?MatGUID=147bfe2c68af4c828cdc1a4af9fae014" >Sandvik Thermothal P Thermocouple</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_19074"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;994</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_19074' href="/search/DataSheet.aspx?MatGUID=5e55ea6a4e444c0f8ac5c911a95ec849" >Sandvik Sanicro 70 Billet</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_19146"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;995</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_19146' href="/search/DataSheet.aspx?MatGUID=5d794d8986734996a9f6cc3ecc1b6ada" >Sandvik Sanicro 69 Seamless tube and pipe</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_19147"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;996</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_19147' href="/search/DataSheet.aspx?MatGUID=3e08c07764d14960837d171ee023e884" >Sandvik Sanicro 70 Seamless tube and pipe</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_19152"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;997</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_19152' href="/search/DataSheet.aspx?MatGUID=a2ab932a93f84896879e49c0da7e19df" >Sandvik Kanthal SW 200 Spray Wire</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_19153"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;998</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_19153' href="/search/DataSheet.aspx?MatGUID=d8b356ae6cd4440c9110e9c824881235" >Sandvik Kanthal SW 210 Spray Wire</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_19155"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;999</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_19155' href="/search/DataSheet.aspx?MatGUID=1466db98d9394bedbf86e80921c43ba4" >Sandvik Kanthal SW 782 Spray Wire</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_19156"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;1000</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_19156' href="/search/DataSheet.aspx?MatGUID=2a8c6752eb61490bbc9f6c4e30c76b52" >Sandvik Kanthal SW 806 Spray Wire</a></td></tr>

		
	</table>

	<table style="background-color:Silver;" class="tabledataformat" >

		<tr class="">
			<th>&nbsp;</th>
		</tr>

		<tr>
			<td><strong>Found <span id="ctl00_ContentMain_UcSearchResults1_lblResultCount2">1245</span> Results</strong> -- 
				
				Page 
				<select name="ctl00$ContentMain$UcSearchResults1$drpPageSelect2" onchange="javascript:setTimeout('__doPostBack(\'ctl00$ContentMain$UcSearchResults1$drpPageSelect2\',\'\')', 0)" id="ctl00_ContentMain_UcSearchResults1_drpPageSelect2">
		<option value="1">1</option>
		<option value="2">2</option>
		<option value="3">3</option>
		<option value="4">4</option>
		<option selected="selected" value="5">5</option>

	</select>
				of <span id="ctl00_ContentMain_UcSearchResults1_lblPageTotal2">7</span> 
				
				-- 
				<a id="ctl00_ContentMain_UcSearchResults1_lnkPrevPage2" href="javascript:__doPostBack('ctl00$ContentMain$UcSearchResults1$lnkPrevPage2','')" style="color:Black;">[Prev Page]</a>
				<a id="ctl00_ContentMain_UcSearchResults1_lnkNextPage2" disabled="disabled" style="color:Gray;">[Next Page]</a>
				--&nbsp;

				view 
				<select name="ctl00$ContentMain$UcSearchResults1$drpPageSize2" onchange="javascript:setTimeout('__doPostBack(\'ctl00$ContentMain$UcSearchResults1$drpPageSize2\',\'\')', 0)" id="ctl00_ContentMain_UcSearchResults1_drpPageSize2">
		<option value="25">25</option>
		<option value="50">50</option>
		<option value="75">75</option>
		<option value="100">100</option>
		<option value="150">150</option>
		<option selected="selected" value="200">200</option>

	</select> per page
			</td>
		</tr>
	</table>
	
	<span id="ctl00_ContentMain_UcSearchResults1_divDiscontinued" style="font-size:11px;color:red">
	<span id="ctl00_ContentMain_UcSearchResults1_lblDisclaimer"><br />
Materials flagged as discontinued (<img src="/images/buttons/iconDiscontinued.jpg" alt="" />) are no longer part of the manufacturer’s standard product line according to our latest information.  These materials may be available by special order, in distribution inventory, or reinstated as an active product.  Data sheets from materials that are no longer available remain in MatWeb to assist users in finding replacement materials.  
<br /><br />
Users of our Advanced Search (registration required) may exclude discontinued materials from search results.</span>
	</span> 


</div>





<script type="text/javascript">

	//GLOBAL VARIABLES
	var gCurrentCheckbox;
	var gNewFolderID;
	var gOldFolderName;
	var gSelectedFolderID = 0;
	var gMaxFolders = 0;

	//GLOBAL FIELD REFERENCES
	var drpFolderList = document.getElementById("ctl00_ContentMain_UcSearchResults1_drpFolderList");
	var drpFolderAction = document.getElementById("ctl00_ContentMain_UcSearchResults1_drpFolderAction");
	var txtFolderMatCount = document.getElementById("ctl00_ContentMain_UcSearchResults1_txtFolderMatCount");
	var divUserMessage = document.getElementById("divUserMessage");
	var divFolderName = document.getElementById("divFolderName");
	var tblFolderName = document.getElementById("tblFolderName");
	var txtFolderName = document.getElementById("txtFolderName2");
	
	// See MS ajax.net client side documentation
	// http://ajax.asp.net/docs/ClientReference/Sys.Net/WebrequestManagerClass/	
	Sys.Net.WebRequestManager.add_invokingRequest(ucSearchResults_OnRequestStart);
	Sys.Net.WebRequestManager.add_completedRequest(ucSearchResults_OnRequestEnd);
	
	var timerID = 0;

	function ucSearchResults_OnRequestStart(){
		document.body.style.cursor="progress";
	}
	
	function ucSearchResults_OnRequestEnd(){
		document.body.style.cursor="auto";
	}

	//drpFolderAction pull down does not exist for anonymous users
	if(drpFolderAction) {
		drpFolderAction.onchange=RunFolderAction;
	}

	//RUN ONCE on page load
	//matweb.register_onload(SetSelectedFolder);
	
	SearchMatIDList = []; //an array of MatIDs
	//matweb.alert(MatIDList);
	
	//======================
	//Test Service 
	//======================
	//TestService("This is a test");
	//aci.matweb.WebServices.Folders.RaiseTestError(null,ErrorHandler,"testerror");
	
	function TestService(arg){
		aci.matweb.WebServices.Folders.HelloWorld(arg, TestService_CALLBACK,ErrorHandler,"TestService");
	}
	
	function TestService_CALLBACK(data,context){
		alert(data);
	}
	
	// =================================
	// This function is the onchange event for the material checkbox. See Code Behind.
	// The first time a material is checked, and there are no folders, the folder web service creates a NEW FOLDER
	// =================================
	function ToggleMat(chkBox,MatID,MaxFolderMats){
	
		HideFolderNameControls();

		var FolderID,Context;
		gCurrentCheckbox = chkBox;
		var matLink = $("lnkMatl_" + MatID);
		var MatName = matLink.innerHTML;
		//alert(MatName);

		var SelectedOption = drpFolderList.options[drpFolderList.selectedIndex];  

		if(gNewFolderID > 0)
			FolderID = gNewFolderID;
		else
			FolderID = SelectedOption.value; //this will be 0 if no folders exist yet
		//alert(FolderID);
		
		// call the folder web service 
		// See: ScriptManagerProxy object at the top of this page
		// Also See: /folders/FolderService.asmx & /App_Code/FolderService.cs
		if(chkBox.checked){
			Context = "AddMat";
			aci.matweb.WebServices.Folders.AddMaterial(FolderID, MatID, MatName, ToggleMat_CALLBACK,ErrorHandler,Context);
		}
		else{
			Context = "RemoveMat";
			aci.matweb.WebServices.Folders.RemoveMaterial(FolderID, MatID, ToggleMat_CALLBACK,ErrorHandler,Context);
		}
			
	}
	
	// =================================
	// CALLBACK FUNCTION
	// this function is called by the AJAX framework after the web service is invoked
	// The folder web service returns a ReturnData type object.
	// =================================
	function ToggleMat_CALLBACK(data,context){

		 //alert(data);
		// alert(context);

		var FolderMatCount = data.FolderMatCount
		var NewFolderID = data.NewFolderID
		var DisplayMessage = data.DisplayMessage
		var MaxFolderMats = data.FolderMaxMatls;
		
		txtFolderMatCount.value = FolderMatCount + "/" + MaxFolderMats;

		// If this is the first time the user checked the checkbox, and the user did not already have any folders, 
		// a new folder may have been created.
		// Save the new folderID to a hidden field, so we can reference it later.
		if(NewFolderID > 0){
			gNewFolderID = NewFolderID;
		}
		
		// alert(gNewFolderID);
		// alert(FolderCount);
		// alert(ReloadFolderList);
		// if(NewFolderID>0) alert(NewFolderID);
		// alert(MaxFolderMats);
		
		switch(context){
		case "AddMat":
			if(DisplayMessage){
				gCurrentCheckbox.checked = false;
				matweb.alert(DisplayMessage);
			}
			break    
		case "RemoveMat":
			break    
		default:
			matweb.Alert("Unhandeled Context in ToggleMat_CALLBACK() function: " + Context);
		}//end switch

	}//end function

	
	//=================================
	function CompareMaterials(){
	//=================================
		var SelectedFolderID = GetSelectedFolderID();
		window.location.href="/folders/Compare.aspx?FolderID=" + SelectedFolderID;
	}

	//=================================
	// Runs on drpFolderList.onchange, and also is called by other methods.
	// Sets the checkbox for each visible material in the folder.
	// Also sets txtFolderMatCount with the MatCount indicator for the selected folder.
	//=================================
	function SetSelectedFolder(){
		//alert("SetSelectedFolder()");
		var SelectedFolderID = GetSelectedFolderID();
		//alert("SelectedFolderID :" + SelectedFolderID)
		
		aci.matweb.WebServices.Folders.SetSelectedFolder(SelectedFolderID,SetSelectedFolder_CALLBACK,ErrorHandler,"update_txtFolderMatCount");
		
		HideFolderNameControls();
	}

	// =================================
	// CALLBACK FUNCTION
	// Returns a list of MatID's in the selected folder, and sets the checkbox for each material.
	// Also sets txtFolderMatCount with the MatCount indicator for the selected folder.
	// =================================
	function SetSelectedFolder_CALLBACK(data,context){

		// alert(data);
		// alert(context);

		// the folder web service returns an object with 4 fields
		//alert("data.FolderMatCount: " + data.FolderMatCount);
		//alert("data.FolderMaxMatls: " + data.FolderMaxMatls);
		//alert("data.DisplayMessage: " + data.DisplayMessage);
		//alert("data.MatIDList: " + data.FolderMatIDList);

		if(data.DisplayMessage){
			matweb.alert(data.DisplayMessage);
		}

		txtFolderMatCount.value = data.FolderMatCount + "/" + data.FolderMaxMatls;

		//alert(SearchMatIDList);
		var chkFolder;

		// Set checkboxes for any visible materials in the folder
		// loop over all materials in the search results for the current page...
		//alert('lenOfArray=' + SearchMatIDList.length);
		for(s=0;s<SearchMatIDList.length;s++){
			chkFolder = document.getElementById("chkFolder_" + SearchMatIDList[s]);
			//alert('index=' + SearchMatIDList[s]);
			chkFolder.checked = false; //clear any currently checked boxes...
			//loop over all materials in the folder...
			//If there is a match, make it checked.
			for(f=0;f<data.FolderMatIDList.length;f++){
				if(SearchMatIDList[s]==data.FolderMatIDList[f]){
					//alert("Match found: " + data.FolderMatIDList[f]);
					chkFolder.checked = true;
				}
			}
		}

	}//end function
	
	//=================================
	function RunFolderAction(){
	//=================================
	
		//alert("RunFolderAction()");
		var action = drpFolderAction.options[drpFolderAction.selectedIndex].value;
		var SelectedFolderName = drpFolderList.options[drpFolderList.selectedIndex].text;
		var FolderID = GetSelectedFolderID();
		var msg = "";
		
		switch(action){
			case "empty":
				aci.matweb.WebServices.Folders.EmptyFolder(FolderID,SetSelectedFolder_CALLBACK,ErrorHandler,action);
				msg = "Folder ["+SelectedFolderName+"] was cleared of materials";
				HideFolderNameControls();
				break;
			case "delete":
				var answer = confirm("Delete folder ["+SelectedFolderName+"] ?");
				if(answer){
					gOldFolderName = SelectedFolderName;
					aci.matweb.WebServices.Folders.DeleteFolder(FolderID,DeleteFolder_CALLBACK,ErrorHandler,action);
				}
				HideFolderNameControls();
				break;
			case "new":
				//setting up new folder controls
				if(drpFolderList.options.length >= gMaxFolders){
					matweb.alert("You have already added as many folders as you are currently allowed by the system (" + gMaxFolders + ").<p/> You may view our <a href=\"/membership/benefits.aspx\">benefits</a> page for the features that are allowed for your current user level.");
				}
				else{
				divFolderName.style.display="block";
				divFolderName.innerHTML="New Folder Name";
				tblFolderName.style.display="block";
				txtFolderName.value = "New Folder Name";
				txtFolderName.focus();
				txtFolderName.select();
				}
				break;
			case "rename":
				//setting up rename folder controls
				divFolderName.style.display="block";
				divFolderName.innerHTML="Rename Folder";
				tblFolderName.style.display="block";
				txtFolderName.value = SelectedFolderName;
				txtFolderName.focus();
				txtFolderName.select();
				break;
			case "managefolders":
				window.location.href="/folders/ListFolders.aspx";
				break;
			case "expSolidworks":
				ExportFolder(FolderID, 2, null);
				break;
			case "expALGOR":
				ExportFolder(FolderID, 3, null);
				break;
			case "expANSYS":
				ExportFolder(FolderID, 4, null);
				break;
			case "expNEiWorks":
				ExportFolder(FolderID, 5, null);
				break;
			case "expCOMSOL3":
				ExportFolder(FolderID, 6, '3.4 or newer');
				break;
			case "expCOMSOL4":
				ExportFolder(FolderID, 10, null);
				break;
			case "expETBX":
				ExportFolder(FolderID, 8, null);
				break;
			case "expSpaceClaim":
				ExportFolder(FolderID, 9, null);
				break;
			case "0":
				//do nothing
				drpFolderAction.selectedIndex = 0;
				break;
			default: 
				alert("Unhandled Action: " + action);
		}

		//drpFolderAction.selectedIndex=0;
		divUserMessage.innerHTML = msg;
		//alert("End RunFolderAction()");
		
	}
	
	function DeleteFolder_CALLBACK(DeleteWasSuccessfull,context){
		divUserMessage.innerHTML = "Folder ["+gOldFolderName+"] was deleted.";
		//Refresh the folder list and selected checkboxes
		aci.matweb.WebServices.Folders.GetFolderList(GetFolderList_CALLBACK,ErrorHandler,"DeleteFolder_CALLBACK");
	}	

	// rsw
	function ExportFolder(folderid, modeid, comsolversion){
		if(txtFolderMatCount.value.substring(0,1) == '0'){
			alert('Folder is empty, nothing to export.');
			drpFolderAction.selectedIndex = 0;
			return;
		}

		var ExportPath = "/folders/ListFolders.aspx";
		
		//alert(ExportPath);
		location.href = ExportPath;
		//window.open(ExportPath, '', '', '');
	}
	
	
	
	
	//===============================================================
	// This runs on the onclick event for btnApplyFolderName.
	// It handles creating a new folder, or renaming an existing one.
	//===============================================================
	function ApplyFolderName(){
	
		//alert("ApplyFolderName()");
		var action = drpFolderAction.options[drpFolderAction.selectedIndex].value;
		var NewFolderName = txtFolderName.value;
		var FolderID = GetSelectedFolderID();
		var msg = "";

		//alert(FolderID);

		//If we are showing the non-existant "virtual" folder "My Folder",
		//create it first, and then rename it.
		if(action=="rename" && FolderID==0)
			action="add_and_rename";

		switch(action){
			case "new":
				aci.matweb.WebServices.Folders.AddFolder(NewFolderName,AddRenameFolder_CALLBACK,ErrorHandler,action);
				break;
			case "rename":
				gOldFolderName = drpFolderList.options[drpFolderList.selectedIndex].text;
				aci.matweb.WebServices.Folders.RenameFolder(FolderID,NewFolderName,AddRenameFolder_CALLBACK,ErrorHandler,action);
				break;
			case "add_and_rename":
				gOldFolderName = drpFolderList.options[drpFolderList.selectedIndex].text;
				aci.matweb.WebServices.Folders.AddFolder(NewFolderName,AddRenameFolder_CALLBACK,ErrorHandler,action);
				break;
			default: 
				alert("Unhandled Action in ApplyFolderName(): " + action);
		}

		HideFolderNameControls();
		
	}
	
	//=================================
	// CALLBACK FUNCTION
	//=================================
	function AddRenameFolder_CALLBACK(ReturnData,context){
		//alert("AddRenameFolder_CALLBACK()");
		
		var msg;
		//Folder = ReturnData.Folder;
	
		switch(context){
			case "new":
				//gNewFolderID = Folder.FolderID;
				gNewFolderID = ReturnData.NewFolderID;
				msg = "The folder [" + ReturnData.NewFolderName + "] was created.";
				break;
			case "rename":
				msg = "The folder [" + gOldFolderName+ "] was renamed to [" + ReturnData.NewFolderName + "].";
				break;
			case "add_and_rename":
				//gNewFolderID = Folder.FolderID;
				gNewFolderID = ReturnData.NewFolderID;
				msg = "The folder [" + gOldFolderName+ "] was renamed to [" + ReturnData.NewFolderName + "].";
				break;
		}
		
		divUserMessage.innerHTML = msg;
		
		//refresh the folder list, so it includes the new folder
		aci.matweb.WebServices.Folders.GetFolderList(GetFolderList_CALLBACK,ErrorHandler,"AddFolder");

	}
	
	//=================================
	function HideFolderNameControls(){
	//=================================
		//These fields will not exist for anonymous users
		if(drpFolderAction){
			drpFolderAction.selectedIndex=0;
			divFolderName.style.display="none";
			tblFolderName.style.display="none";
		}
	}
	
	//=================================
	function GetSelectedFolderID(){
	//=================================
		var FolderID = 0;
		//if one of the processeses created a new folder id, then use it.
		// alert('gNewFolderID=' + gNewFolderID);
		if(gNewFolderID > 0){
			FolderID = gNewFolderID;
			//gNewFolderID = 0;//clear the new folder id, so we don't use it again.
		}
		else 
			if(drpFolderList != null){
				if(drpFolderList.options.length == 0){
					FolderID = gSelectedFolderID;
				}
				else{
					var SelectedOption = drpFolderList.options[drpFolderList.selectedIndex];  
					FolderID = SelectedOption.value; //this will be 0 if no folders exist yet
				}
			}
		return FolderID;
	}

	//=================================
	function UnregisteredUserMessage(chkBox){
	//=================================
		matweb.alert("This feature is reserved for registered users.<p/><a href='/membership/regstart.aspx'>Click here to register.</a>","Reserved Feature");
		chkBox.checked = false;
	}

	// =================================
	// CALLBACK FUNCTION
	// The folder web service returns a JS array of iBatis data objects of type: aci.matweb.data.folder.Folder 
	// The drop down list of folders is set using this list.
	// =================================
	function GetFolderList_CALLBACK(iFolderList,Context){
	
		//alert(iFolderList);
		//alert('iFolderList.length: ' + iFolderList.length);

		if(drpFolderList != null){
		
		var SelectedFolderID = GetSelectedFolderID();//remember the current selected folder, so we can use it later
		var opt;
		var folder;

		//clear the options and reload them
		drpFolderList.options.length = 0;

		//add a default empty folder...		
		if(iFolderList.length==0){
			opt = document.createElement('OPTION');
			opt.value = "0";
			opt.text = "My Folder";
			drpFolderList.options.add(opt);
			i++;
		}
		
		for(var i = 0;i < iFolderList.length;i++){
			opt = document.createElement('OPTION');
			folder = iFolderList[i];
			//alert(folder.FolderID);
			//alert(folder.FolderName);
			opt.value = folder.FolderID;
			opt.text = folder.FolderName;
			if(folder.FolderID == SelectedFolderID) opt.selected = true;
			drpFolderList.options.add(opt);
		}

		//clear the new folder id, so we don't use it again. 
		gNewFolderID = 0;
		
		//Set checkboxes and folder count fields for the new selected folder
		SetSelectedFolder();
		}

	}
	
	// =================================
	// ON ERROR CALLBACK FUNCTION
	// =================================
	function ErrorHandler(error,context) {
	
			var stackTrace = error.get_stackTrace();
			var message = error.get_message();
			var statusCode = error.get_statusCode();
			var exceptionType = error.get_exceptionType();
			var timedout = error.get_timedOut();
			var Debug = false;

			// Compose the error message...
			var ErrMsg =     
					"<strong>Stack Trace</strong><br/>" +  stackTrace + "<br/>" +
					"<strong>WebService Error</strong><br/>" + message + "<br/>" +
					"<strong>Status Code</strong><br/>" + statusCode + "<br/>" +
					"<strong>Exception Type</strong><br/>" + exceptionType + "<br/>" +
					"<strong>JS Context</strong><br/>" + context + "<br/>" +
					"<strong>Timedout</strong><br/>" + timedout + "<br/>" +
					"<strong>Source Page</strong><br/>" + "/search/MaterialGroupSearch.aspx";
			//matweb.alert(ErrMsg,"Error in WebService",null,"500");
			if(Debug){
				matweb.alert(ErrMsg,"Error in WebService",null,"500");
				//alert(ErrMsg);
			}
			else{
				ErrMsg = escape(ErrMsg);
				//matweb.alert(ErrMsg);
				//window.location.href="/errorJS.aspx?ErrMsg=" + ErrMsg;
				divUserMessage.innerHTML = "Problem retrieving folder data...Some folder features may not work.";
			}
		
	}
	
	function ShowInterpolationMessage(){
		InterpolationMsg = "The material was found via interpolation, so no data points can be displayed. See the material data sheet for actual data points.";
		matweb.alert(InterpolationMsg);
	}
	
</script>

		</td>
	</tr>
	</table>

<hr />
<a id="help"></a>
<table><tr><td>
<p/><b>Instructions:</b> This page allows you to quickly access all of the polymers/plastics, metals, ceramics, fluids, and other engineering materials
in the MatWeb material property database.&nbsp; Just select the material category you would like to find from the tree.  Click on the [+] icon to open subcategory branches in the tree.
Then click the <b>'Find'</b> button next to its box.&nbsp; You can then follow the links to the most complete data sheets on the Web, with information on over 1000 available properties, including dielectric
constant, Poisson's ratio, glass transition temperature, dissipation factor, and Vicat softening point.
</td><td><a href="https://proto3000.com"><img src="/images/assets/proto3000.jpg" border = "0"><br>

Proto3000 - Rapid Prototyping</a>
</td></tr></table>

<p/><span class="hiText">Notes:</span>

<ul>
  <li>Visit MatWeb's <a href="/search/PropertySearch.aspx">property-based search page</a> to limit your results to materials that meet specific property performance criteria.</li>
  <li>Text searches can be done from any page - use the <b>Quick Search</b> box in the top right of the page.</li>
  <li>More <a href="/help/strategy.aspx"><strong>Search Strategies</strong></a> on how to find information in MatWeb.</li>
   <li><a href="/search/GetAllMatls.aspx">Show All Materials</a> - an inefficient drill-down to data sheets.  Registered users who are logged in omit a step in this drill down process.</li>
</ul>

<p/><a href="#Top">Top</a>

<script type="text/javascript">

	var txtMatGroupText = document.getElementById("ctl00_ContentMain_txtMatGroupText");
	var txtMatGroupID = document.getElementById("ctl00_ContentMain_txtMatGroupID");
	var divMatGroupName = document.getElementById("ctl00_ContentMain_divMatGroupName");

	function ucMatGroupFinder1_OnSelected(MatGroupID,MatGroupText){
		txtMatGroupText.value = MatGroupText;
		txtMatGroupID.value = MatGroupID;
		divMatGroupName.innerHTML = MatGroupText;
		//__doPostBack("ctl00$ContentMain$btnSubmit","");
	}

	function ucMatGroupTree_OnNodeClick(NodeText,MatGroupID){
		txtMatGroupText.value = NodeText;
		txtMatGroupID.value = MatGroupID;
		divMatGroupName.innerHTML = NodeText;
		//__doPostBack("ctl00$ContentMain$btnSubmit","");
	}
	
	function validate(){
		if (txtMatGroupID.value == ""){
			matweb.alert("Please select a material category","User Input Error");
			return false;
		}
		
		return true;
		
	}

</script>


</div>
<!-- ===================================== END MAIN CONTENT ========================================== -->

<table class="tabletight" style="width:100%" >
        <tr>
                <td align="center" class="footer" colspan="3">
                        <br /><br />
                
                        <a href="/membership/regupgrade.aspx" class="footlink"><span class="subscribeLink">Subscribe to Premium Services</span></a><br/>
                
                        <span class="footer"><b>Searches:</b>&nbsp;&nbsp;</span>
                        <a href="/search/AdvancedSearch.aspx" class="footlink"><span class="foot">Advanced</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/search/CompositionSearch.aspx" class="footlink"><span class="foot">Composition</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/search/PropertySearch.aspx" class="footlink"><span class="foot">Property</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/search/MaterialGroupSearch.aspx" class="footlink"><span class="foot">Material&nbsp;Type</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/search/SearchManufacturerName.aspx" class="footlink"><span class="foot">Manufacturer</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/search/SearchTradeName.aspx" class="footlink"><span class="foot">Trade&nbsp;Name</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/search/SearchUNS.aspx" class="footlink"><span class="foot">UNS Number</span></a>
                        <br />
                        <span class="footer"><b>Other&nbsp;Links:</b>&nbsp;&nbsp;</span>
                        <a href="/services/advertising.aspx" class="footlink"><span class="foot">Advertising</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/services/submitdata.aspx" class="footlink"><span class="foot">Submit&nbsp;Data</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/services/databaselicense.aspx" class="footlink"><span class="foot">Database&nbsp;Licensing</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/services/webhosting.aspx" class="footlink"><span class="foot">Web&nbsp;Design&nbsp;&amp;&nbsp;Hosting</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/clickthrough.aspx?addataid=277" class="footlink"><span class="foot">Trade&nbsp;Publications</span></a>

                        <br />
                        <a href="/reference/suppliers.aspx" class="footlink"><span class="foot">Supplier&nbsp;List</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/tools/unitconverter.aspx" class="footlink"><span class="foot">Unit&nbsp;Converter</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/tools/contents.aspx#reference" class="footlink"><span class="foot">Reference</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/reference/link.aspx" class="footlink"><span class="foot">Links</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/help/help.aspx" class="footlink"><span class="foot">Help</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/services/contact.aspx" class="footlink"><span class="foot">Contact&nbsp;Us</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/tools/contents.aspx" class="footlink"><span class="foot" style="color:red;">Site&nbsp;Map</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/reference/faq.aspx" class="footlink"><span class="foot">FAQ</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/index.aspx" class="footlink"><span class="foot">Home</span></a>
                        <br />&nbsp;
                        <div id="fb-root"></div>

<table><tr><td><script src="http://connect.facebook.net/en_US/all.js#xfbml=1"></script><fb:like-box href="https://www.facebook.com/MatWeb.LLC" width="175" show_faces="false" stream="false" header="false"></fb:like-box>

</td><td>

<a href="https://twitter.com/MatWeb" class="twitter-follow-button" data-show-count="false" data-show-screen-name="false">Follow @MatWeb</a> <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>

</td></tr></table>
                </td>
        </tr>
        <tr>
                <td colspan="3"><img src="/images/bluebar.gif" width="100%" height="5" alt="" /></td>
        </tr>
        <tr>
                <td style="background-image:url(/images/gray.gif);"><img src="/images/spacer.gif" width="5" height="1" alt=""/></td>
                <td style="background-image:url(/images/gray.gif);">
                        <span class="footer">
                        Please read our <a href="/reference/terms.aspx" class="footlink"><span class="foot">License Agreement</span></a> regarding materials data and our  <a href="/reference/privacy.aspx" class="footlink"><span class="foot">Privacy Policy</span></a>.
                        Questions or comments about MatWeb? Please contact us at
                        <a href="mailto:webmaster@matweb.com" class="footlink"><span class="foot">webmaster@matweb.com</span></a>. We appreciate your input.
                        <br /><br />
                        The contents of this web site, the MatWeb logo, and "MatWeb" are Copyright 1996-2024
                        by MatWeb, LLC. MatWeb is intended for personal, non-commercial use. The contents, results, and technical data from this site
                        may not be reproduced either electronically, photographically or substantively without permission from MatWeb, LLC.
                        </span>
                        <br />
                </td>

                <td style="background-image:url(/images/gray.gif);"><img src="/images/spacer.gif" width="5" height="1" alt=""/></td>
        </tr>
</table>




<script type="text/javascript">
//<![CDATA[
var ctl00_ContentMain_ucMatGroupTree_msTreeView_ImageArray =  new Array('', '', '', '/WebResource.axd?d=zLp3QJeZSj4-fsnL2_raV70Mk3pr-OdjL_rvrZzXUTCnZ6LU5Dg_Tc5XZg4eZpT9O0gxZJrJcxGb23XJofeGD9JhYLr2wl0VkQZnJOx0T0ShBosq0&t=638313619312541215', '/WebResource.axd?d=vFualGN2ZZ4fny_RX_gYiUTY6FBLUapne4VfY6P3sGiyrOYd-Ji2sWg_olT-exaztendUUEOoH87RS2l44bpZTscfmmv4MKRqJK8rgesmkrNfP2O0&t=638313619312541215', '/WebResource.axd?d=4nLCbQ2Yfc2i_msrQXqKzK8pLQD-GrNVZ7DeXU-La2qatczrgeCLazv0RzIkDDHyYfLFalCBO8s71lRS8yifxTChF9dUJvTLxNEeeFlghyuY0dtM0&t=638313619312541215', '/WebResource.axd?d=uEI6Enq15_q4pXO6g0E7vtAqtD5WIokAEQ2Vz8V3Yi36cefediLfyCwENkPd-w1GxTXu-HkNqcDlrYOw8bxgGqeHJtzc-PBlTa9nrZRfdIyj8fQh0&t=638313619312541215', '/WebResource.axd?d=hzeik1gGhZrfDZXUWCMsBOx5b43bIFor8UCTzm_wuh92b9Z-WQU4_fQCSUw6CaP5tnc9LTU0jl0sy4Y6Cf_c8q8HFXRqeHQuscXIqqLNSZkgl2cu0&t=638313619312541215', '/WebResource.axd?d=6v5qM6VKQYD3XNwwXEy7Tix7dzVdqDJ8QTRIlUPrzXQqNsXQgUwIfmn34ZzRORd_IZj0bF4vkZzID1HuSFE5uKyLgaPYeAYE8VT8HYfb3Rk6zsDw0&t=638313619312541215', '/WebResource.axd?d=d9nh8nEM1YCqIfE9jLzNhPyFIZqARvlFExwnFG0vbvskO-A2qORlhTyUVIxDtSwldGrOp8o9ef8lZ9SRhPQ9rv9TkGV-NVYvH9_yHT6CvWpIPF4S0&t=638313619312541215', '/WebResource.axd?d=7_V5n1KYSQsj6PoSqgIxb0egBsAa91jSXPkbmM6FaiF14-JsNnpUDnfSdxZcvfd0OQCLDtC24pMIXigO-41cL2rsq-0xea-SSG7yZUyRcliFFqe70&t=638313619312541215', '/WebResource.axd?d=TinC6igX1vPDgWh7DstWwBTHTyH4uuybDU4NW47jJD06wHdAO3YyJC_eKK1Zn5oIbwqRo4Cri8YwhA82G-LD0p14yEF1EfoIZvKleYMIuQ2K1hkE0&t=638313619312541215', '/WebResource.axd?d=4B0dvJqmzY4ukTboQq7lmxokRL9NTsoWh4yErB11Tb-ucvMw_p_0mxYVn4U1Uz6Jk795umrO0C4-B-8Yq1Kxxz6uyDT15O2BSCb8349f4YWDdIVy0&t=638313619312541215', '/WebResource.axd?d=TUiibr9hjS_MlNf09dKZECe1z56fbaPbfgJfC5eYJi0TtdBviUcA1JQJLV81aqunzpXysgruhL3ihpgd7ziL5p9SQ86xVLNiUqbYpJnW9Rj7sN1E0&t=638313619312541215', '/WebResource.axd?d=vUBrM2nNhECNbFO0AJKq52p2zWrY5XG8nWgCDL4AfsrexStb0HREDV6GHMvdkAGDInxYtTyWXuxFUIiQv3JX2fScVRwQsrVv5srHrjOIeC3ayAG20&t=638313619312541215', '/WebResource.axd?d=Z-LP5lKcsnv2A9UpExXu-5leQYIAZMYp6SXNognZi1r66YT8u24-YjJaxjisE9ruYU8NKnNz72H4shclsk8PM3S5M-VYMk_F_G5fcd3ra6Sj9CaS0&t=638313619312541215', '/WebResource.axd?d=QI4SACqRn7PxBBHPfkR7peFMAEv7lX1_iZ_7jncKwxRTlvhSAncd5K2oRqw8J8WmJjWQcbGgEblhrufy-hUjZMa1YLUC47mjfV1eS4LM3kUqLT960&t=638313619312541215', '/WebResource.axd?d=ayrFqNSzdiTAdowDVbEH-ZzKbvw5bCq7onyCSAcJhCVPVIHlUB9M_fo8BLUTeGpbtcoMiyFy1MoeEIsweMrpVandBKC4vAK-NCBcfn2FIreK8mEJ0&t=638313619312541215', '/WebResource.axd?d=OK_uWiuoGURaJ3CC0fVlQ7v9GUnIAkx03w03rQ2ENqcwdObLiQwK7B9yRFyAWj9tuUqsa0874ksgkaRo8KirB7q1ZL_huh7gCq_XehXfHMEKMiAl0&t=638313619312541215');
//]]>
</script>


<script type="text/javascript">
//<![CDATA[
(function() {var fn = function() {AjaxControlToolkit.ModalPopupBehavior.invokeViaServer('ctl00_ContentMain_ucPopupMessage1_ModalPopupExtender1', false); Sys.Application.remove_load(fn);};Sys.Application.add_load(fn);})();
var callBackFrameUrl='/WebResource.axd?d=Mt7MojvAyTqAEcx0MpKgcF_QVa09tLyMQS6c5JLNO4j2M_F6CMxYnN0HdVYxyrhggOpkTwyNqQ51iS6UoHXgJqHBROE1&t=638313619312541215';
WebForm_InitCallback();var ctl00_ContentMain_ucMatGroupTree_msTreeView_Data = new Object();
ctl00_ContentMain_ucMatGroupTree_msTreeView_Data.images = ctl00_ContentMain_ucMatGroupTree_msTreeView_ImageArray;
ctl00_ContentMain_ucMatGroupTree_msTreeView_Data.collapseToolTip = "Collapse {0}";
ctl00_ContentMain_ucMatGroupTree_msTreeView_Data.expandToolTip = "Expand {0}";
ctl00_ContentMain_ucMatGroupTree_msTreeView_Data.expandState = theForm.elements['ctl00_ContentMain_ucMatGroupTree_msTreeView_ExpandState'];
ctl00_ContentMain_ucMatGroupTree_msTreeView_Data.selectedNodeID = theForm.elements['ctl00_ContentMain_ucMatGroupTree_msTreeView_SelectedNode'];
for (var i=0;i<19;i++) {
var preLoad = new Image();
if (ctl00_ContentMain_ucMatGroupTree_msTreeView_ImageArray[i].length > 0)
preLoad.src = ctl00_ContentMain_ucMatGroupTree_msTreeView_ImageArray[i];
}
ctl00_ContentMain_ucMatGroupTree_msTreeView_Data.lastIndex = 8;
ctl00_ContentMain_ucMatGroupTree_msTreeView_Data.populateLog = theForm.elements['ctl00_ContentMain_ucMatGroupTree_msTreeView_PopulateLog'];
ctl00_ContentMain_ucMatGroupTree_msTreeView_Data.treeViewID = 'ctl00$ContentMain$ucMatGroupTree$msTreeView';
ctl00_ContentMain_ucMatGroupTree_msTreeView_Data.name = 'ctl00_ContentMain_ucMatGroupTree_msTreeView_Data';
Sys.Application.initialize();
Sys.Application.add_init(function() {
    $create(AjaxControlToolkit.ModalPopupBehavior, {"BackgroundCssClass":"modalBackground","OkControlID":"ctl00_ContentMain_ucPopupMessage1_btnOK","PopupControlID":"ctl00_ContentMain_ucPopupMessage1_divPopupMessage","PopupDragHandleControlID":"ctl00_ContentMain_ucPopupMessage1_trTitleRow","dynamicServicePath":"/search/MaterialGroupSearch.aspx","id":"ctl00_ContentMain_ucPopupMessage1_ModalPopupExtender1"}, null, null, $get("ctl00_ContentMain_ucPopupMessage1_hndPopupControl"));
});
Sys.Application.add_init(function() {
    $create(AjaxControlToolkit.TextBoxWatermarkBehavior, {"ClientStateFieldID":"ctl00_ContentMain_UcMatGroupFinder1_TextBoxWatermarkExtender2_ClientState","WatermarkCssClass":"greyOut","WatermarkText":"Type at least 4 characters here...","id":"ctl00_ContentMain_UcMatGroupFinder1_TextBoxWatermarkExtender2"}, null, null, $get("ctl00_ContentMain_UcMatGroupFinder1_txtSearchText"));
});
//]]>
</script>
</form>


<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-15290815-1");
pageTracker._setDomainName(".matweb.com");
pageTracker._trackPageview();
} catch(err) {}</script>

<!-- 20160905 insert SpecialChem javascript -->
<script type="text/javascript" src="//collect.specialchem.com/collect.js"></script>
<script type="text/javascript">
    //<![CDATA[
    var _spc = (_spc || []);
    _spc.push(['init',
    {
        partner: 'MatWeb'
        
        //, iid: '12345'
    }]);
    //]]>
</script>
<script> (function(){ var s = document.createElement('script'); var h = document.querySelector('head') || document.body; s.src = 'https://acsbapp.com/apps/app/dist/js/app.js'; s.async = true; s.onload = function(){ acsbJS.init({ statementLink : '', footerHtml : '', hideMobile : false, hideTrigger : false, disableBgProcess : false, language : 'en', position : 'right', leadColor : '#146FF8', triggerColor : '#146FF8', triggerRadius : '50%', triggerPositionX : 'right', triggerPositionY : 'bottom', triggerIcon : 'people', triggerSize : 'bottom', triggerOffsetX : 20, triggerOffsetY : 20, mobile : { triggerSize : 'small', triggerPositionX : 'right', triggerPositionY : 'bottom', triggerOffsetX : 20, triggerOffsetY : 20, triggerRadius : '20' } }); }; h.appendChild(s); })();</script>
</body>
</html>
