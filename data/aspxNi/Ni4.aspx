

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head id="ctl00_mainHeader"><title>
	Metal, Plastic, and Ceramic Search Index
</title><meta http-equiv="Content-Style-Type" content="text/css" /><meta http-equiv="Content-Language" content="en" />

        <script src="/includes/flash.js" type="text/javascript"></script>

        <script src="/includes/prototype.js" type="text/javascript"></script>
        <!-- script src="/includes/scriptaculous/effects.js" type="text/javascript"></script -->
        <script src="/includes/matweb.js" type="text/javascript"></script>
        <link rel="stylesheet" href="/includes/main.css" />

        <script type="text/javascript">

                function _onload(){
                        matweb.debug = false; // ;
                        //matweb.writeOverlay();
                }

                function _onkeydown(evt){
                        //PREVENT GLOBAL AUTO-SUBMIT OF FORMS
                        return matweb.DisableAutoSubmit(evt);
                }

                //EXAMPLE USAGE:
                //matweb.register_onload(function(){alert("hello from onload event");});
                //matweb.writeOverlay();


        </script>

        <style type="text/css" media="screen">
        @import url("/ad_IES/css/styles.css");
        </style>
<meta name="KEYWORDS" content="carbon steels, low alloy steels, corrosion resistant stainless, high temperature stainless, heat resistant stainless steels, AISI steel alloy specifications, high strength steel alloys, tensile strength of steels, maraging, superalloy, lead alloy, magnesium, tin alloy, tungsten, zinc, precious metal, pure metallic element, TPE elastomer, dielectric constant, plastic dissipation factor, nylon glass transition temperature, ionomer vicat softening point, poisson's ratio, polyetherimide, fluoropolymer, silicone, polyester dielectric constant" /><meta name="DESCRIPTION" content="Searchable list of plastics, metals, and ceramics categorized into a fields like polypropylene, nylon, ABS, aluminum alloys, oxides, etc.  Search results are detailed data sheets with tensile strength, density, dielectric constant, dissipation factor, poisson's ratio, glass transition temperature, and Vicat softening point." /><meta name="ROBOTS" content="NOFOLLOW" /><style type="text/css">
	.ctl00_ContentMain_ucMatGroupTree_msTreeView_0 { text-decoration:none; }
	.ctl00_ContentMain_ucMatGroupTree_msTreeView_1 { color:Black; }
	.ctl00_ContentMain_ucMatGroupTree_msTreeView_2 { padding:0px 3px 0px 3px; }

</style></head>

<body onload="_onload();" onkeydown="return _onkeydown(event);">

<a id="Top"></a>

<div id="divSiteName" style="position:absolute; top:0; left:0 ;">
<a href="/index.aspx"><img src="/images/sitelive.gif" alt="" /></a>
</div>

<div id="divPopupOverlay" style="display:none;"></div>

<div id="divPopupAlert" class="popupAlert" style="display:none;">
        <table style="width:100%; height:100%; border-style:outset; border-width:5px; border-color:blue;" >
                <tr><th style=" height:25px; background-color:Maroon; font-size:14px; color:White; "><span id="spanPopupAlertTitle">System Message</span></th></tr>
                <tr><td style=" vertical-align:top; padding:3px; background-color:White;" id="tdPopupAlertMessage" ></td></tr>
                <tr><td style=" height:25px; text-align:center; background-color:Silver;"><input type="button" id="btnPopupAlert" onclick="matweb.alertClose();" style="" value="OK" /></td></tr>
        </table>
</div>

<form name="aspnetForm" method="post" action="MaterialGroupSearch.aspx" onsubmit="javascript:return WebForm_OnSubmit();" id="aspnetForm">
<div>
<input type="hidden" name="__EVENTTARGET" id="__EVENTTARGET" value="" />
<input type="hidden" name="__EVENTARGUMENT" id="__EVENTARGUMENT" value="" />
<input type="hidden" name="__LASTFOCUS" id="__LASTFOCUS" value="" />
<input type="hidden" name="ctl00_ContentMain_ucMatGroupTree_msTreeView_ExpandState" id="ctl00_ContentMain_ucMatGroupTree_msTreeView_ExpandState" value="ccccccnc" />
<input type="hidden" name="ctl00_ContentMain_ucMatGroupTree_msTreeView_SelectedNode" id="ctl00_ContentMain_ucMatGroupTree_msTreeView_SelectedNode" value="" />
<input type="hidden" name="ctl00_ContentMain_ucMatGroupTree_msTreeView_PopulateLog" id="ctl00_ContentMain_ucMatGroupTree_msTreeView_PopulateLog" value="" />
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="/wEPDwULLTE2Nzc3MDc2MDAPZBYCZg9kFgICAQ9kFioCAQ9kFgJmDxYCHgRUZXh0BWdEYXRhIHNoZWV0cyBmb3Igb3ZlciA8c3BhbiBjbGFzcz0iZ3JlZXRpbmciPjE4MCwwMDA8L3NwYW4+ICBtZXRhbHMsIHBsYXN0aWNzLCBjZXJhbWljcywgYW5kIGNvbXBvc2l0ZXMuZAICD2QWAmYPFgIfAAUESE9NRWQCAw9kFgJmDxYCHwAFBlNFQVJDSGQCBA9kFgJmDxYCHwAFBVRPT0xTZAIFD2QWAmYPFgIfAAUJU1VQUExJRVJTZAIGD2QWAmYPFgIfAAUHRk9MREVSU2QCBw9kFgJmDxYCHwAFCEFCT1VUIFVTZAIID2QWAmYPFgIfAAUDRkFRZAIJD2QWAmYPFgIfAAUHQUNDT1VOVGQCCg9kFgJmDxYCHwAFBkxPRyBJTmQCCw8QZGQWAWZkAgwPZBYCZg8WAh8ABQhTZWFyY2hlc2QCDQ9kFgJmDxYCHwAFCEFkdmFuY2VkZAIOD2QWAmYPFgIfAAUIQ2F0ZWdvcnlkAg8PZBYCZg8WAh8ABQhQcm9wZXJ0eWQCEA9kFgJmDxYCHwAFBk1ldGFsc2QCEQ9kFgJmDxYCHwAFClRyYWRlIE5hbWVkAhIPZBYCZg8WAh8ABQxNYW51ZmFjdHVyZXJkAhMPZBYCAgEPZBYCZg8WAh8ABRlSZWNlbnRseSBWaWV3ZWQgTWF0ZXJpYWxzZAIVD2QWEAIDD2QWAmYPFgIfAAWXATxhIGhyZWY9Ii9jbGlja3Rocm91Z2guYXNweD9hZGRhdGFpZD0xMzUxIj48aW1nIHNyYz0iL2ltYWdlcy9hc3NldHMvbWV0YWxtZW4tTmlja2VsLnBuZyIgYm9yZGVyPTAgYWx0PSJNZXRhbG1lbiBTYWxlcyIgd2lkdGggPSAiNzI4IiBoZWlnaHQgPSAiOTAiPjwvYT5kAgQPZBYCAgIPFgIeBXN0eWxlBRlkaXNwbGF5Om5vbmU7d2lkdGg6MjUwcHg7FgYCAQ9kFgJmD2QWAmYPDxYCHwAFElVzZXIgSW5wdXQgUHJvYmxlbWRkAgMPDxYCHwBlZGQCBQ8WAh4FdmFsdWUFAk9LZAIFD2QWBAIBDw9kFgIfAQUMV2lkdGg6MzA0cHg7ZAICDxAWBB8BBStwb3NpdGlvbjphYnNvbHV0ZTtkaXNwbGF5Om5vbmU7V2lkdGg6MzA4cHg7HgRzaXplBQIxOGRkZAIGDw8WBB4IQ2xpZW50SUQCAR4KTGFuZ3VhZ2VJRAIBZBYCZg8UKwAJDxYOHhdQb3B1bGF0ZU5vZGVzRnJvbUNsaWVudGceEkVuYWJsZUNsaWVudFNjcmlwdGceCVNob3dMaW5lc2ceCE5vZGVXcmFwZx4NTmV2ZXJFeHBhbmRlZGQeDFNlbGVjdGVkTm9kZWQeCUxhc3RJbmRleAIIZBYIHgtOb2RlU3BhY2luZxsAAAAAAAAAAAEAAAAeCUZvcmVDb2xvcgojHhFIb3Jpem9udGFsUGFkZGluZxsAAAAAAAAIQAEAAAAeBF8hU0IChIAYZGRkZGRkFCsACQUjMjowLDA6MCwwOjEsMDoyLDA6MywwOjQsMDo1LDA6NiwwOjcUKwACFgwfAAUSQ2FyYm9uICg4NjYgbWF0bHMpHgVWYWx1ZQUDMjgzHghFeHBhbmRlZGgeDFNlbGVjdEFjdGlvbgsqLlN5c3RlbS5XZWIuVUkuV2ViQ29udHJvbHMuVHJlZU5vZGVTZWxlY3RBY3Rpb24CHgtOYXZpZ2F0ZVVybAVBamF2YXNjcmlwdDp1Y01hdEdyb3VwVHJlZV9Pbk5vZGVDbGljaygnQ2FyYm9uICg4NjYgbWF0bHMpJywnMjgzJykeEFBvcHVsYXRlT25EZW1hbmRnZBQrAAIWDB8ABRVDZXJhbWljICgxMDAwNCBtYXRscykfEQUCMTEfEmgfEwsrBAIfFAVDamF2YXNjcmlwdDp1Y01hdEdyb3VwVHJlZV9Pbk5vZGVDbGljaygnQ2VyYW1pYyAoMTAwMDQgbWF0bHMpJywnMTEnKR8VZ2QUKwACFgwfAAUSRmx1aWQgKDc1NjIgbWF0bHMpHxEFATUfEmgfEwsrBAIfFAU/amF2YXNjcmlwdDp1Y01hdEdyb3VwVHJlZV9Pbk5vZGVDbGljaygnRmx1aWQgKDc1NjIgbWF0bHMpJywnNScpHxVnZBQrAAIWDB8ABRNNZXRhbCAoMTcwNTIgbWF0bHMpHxEFATkfEmgfEwsrBAIfFAVAamF2YXNjcmlwdDp1Y01hdEdyb3VwVHJlZV9Pbk5vZGVDbGljaygnTWV0YWwgKDE3MDUyIG1hdGxzKScsJzknKR8VZ2QUKwACFgwfAAUnT3RoZXIgRW5naW5lZXJpbmcgTWF0ZXJpYWwgKDgwNjMgbWF0bHMpHxEFAzI4NR8SaB8TCysEAh8UBVZqYXZhc2NyaXB0OnVjTWF0R3JvdXBUcmVlX09uTm9kZUNsaWNrKCdPdGhlciBFbmdpbmVlcmluZyBNYXRlcmlhbCAoODA2MyBtYXRscyknLCcyODUnKR8VZ2QUKwACFgwfAAUVUG9seW1lciAoOTc2MzUgbWF0bHMpHxEFAjEwHxJoHxMLKwQCHxQFQ2phdmFzY3JpcHQ6dWNNYXRHcm91cFRyZWVfT25Ob2RlQ2xpY2soJ1BvbHltZXIgKDk3NjM1IG1hdGxzKScsJzEwJykfFWdkFCsAAhYMHwAFGFB1cmUgRWxlbWVudCAoNTA3IG1hdGxzKR8RBQMxODQfEmgfEwsrBAIfFAVHamF2YXNjcmlwdDp1Y01hdEdyb3VwVHJlZV9Pbk5vZGVDbGljaygnUHVyZSBFbGVtZW50ICg1MDcgbWF0bHMpJywnMTg0JykfFWhkFCsAAhYMHwAFJVdvb2QgYW5kIE5hdHVyYWwgUHJvZHVjdHMgKDM5OCBtYXRscykfEQUDMjc3HxJoHxMLKwQCHxQFVGphdmFzY3JpcHQ6dWNNYXRHcm91cFRyZWVfT25Ob2RlQ2xpY2soJ1dvb2QgYW5kIE5hdHVyYWwgUHJvZHVjdHMgKDM5OCBtYXRscyknLCcyNzcnKR8VZ2RkAgcPFgIeCWlubmVyaHRtbAUZTmlja2VsIEFsbG95ICgxMjEwIG1hdGxzKWQCDQ8PFgIeB1Zpc2libGVoZGQCDg8PFgIfF2dkFgQCAQ8PFgIfAAUEMTI0NWRkAgMPDxYCHwAFDE5pY2tlbCBBbGxveWRkAg8PDxYGHgtDdXJyZW50UGFnZQIEHg9DdXJyZW50U2VhcmNoSUQCsfODEx8XZ2QWBAICDw8WAh8AZWRkAgMPDxYCHxdnZBY8AgEPDxYCHwAFBDEyNDVkZAIDDxBkEBUFATEBMgEzATQBNRUFATEBMgEzATQBNRQrAwVnZ2dnZxYBAgNkAgUPDxYCHwAFATdkZAIHDw8WCB8ABQtbUHJldiBQYWdlXR8OCiMeB0VuYWJsZWRnHxACBGRkAgkPDxYGHwAFC1tOZXh0IFBhZ2VdHw4KIx8QAgRkZAILDxBkZBYBAgVkAg8PFgIfF2hkAhEPEA9kFgIeCG9uY2hhbmdlBRNTZXRTZWxlY3RlZEZvbGRlcigpZGRkAhUPFgIfF2gWAgIBDxBkZBYBZmQCFw9kFgQCAQ8PFgQfAGUfGmhkZAICDw8WAh8XaGRkAhkPDxYEHwAFDU1hdGVyaWFsIE5hbWUfGmhkZAIbDw8WAh8XaGRkAh0PDxYCHxdoZGQCHw9kFgYCAQ8PFgQfAAUFUHJvcDEfGmhkZAIDDw8WAh8XaGRkAgUPDxYCHxdoZGQCIQ9kFgYCAQ8PFgQfAAUFUHJvcDIfGmhkZAIDDw8WAh8XaGRkAgUPDxYCHxdoZGQCIw9kFgYCAQ8PFgQfAAUFUHJvcDMfGmhkZAIDDw8WAh8XaGRkAgUPDxYCHxdoZGQCJQ9kFgYCAQ8PFgQfAAUFUHJvcDQfGmhkZAIDDw8WAh8XaGRkAgUPDxYCHxdoZGQCJw9kFgYCAQ8PFgQfAAUFUHJvcDUfGmhkZAIDDw8WAh8XaGRkAgUPDxYCHxdoZGQCKQ9kFgYCAQ8PFgQfAAUFUHJvcDYfGmhkZAIDDw8WAh8XaGRkAgUPDxYCHxdoZGQCKw9kFgYCAQ8PFgQfAAUFUHJvcDcfGmhkZAIDDw8WAh8XaGRkAgUPDxYCHxdoZGQCLQ9kFgYCAQ8PFgQfAAUFUHJvcDgfGmhkZAIDDw8WAh8XaGRkAgUPDxYCHxdoZGQCLw9kFgYCAQ8PFgQfAAUFUHJvcDkfGmhkZAIDDw8WAh8XaGRkAgUPDxYCHxdoZGQCMQ9kFgYCAQ8PFgQfAAUGUHJvcDEwHxpoZGQCAw8PFgIfF2hkZAIFDw8WAh8XaGRkAjUPDxYCHwAFBDEyNDVkZAI3DxBkEBUFATEBMgEzATQBNRUFATEBMgEzATQBNRQrAwVnZ2dnZxYBAgNkAjkPDxYCHwAFATdkZAI7Dw8WCB8ABQtbUHJldiBQYWdlXR8OCiMfGmcfEAIEZGQCPQ8PFgYfAAULW05leHQgUGFnZV0fDgojHxACBGRkAj8PEGRkFgECBWQCQQ8WAh8XZxYCAgEPDxYCHwAFuAQ8YnIgLz4NCk1hdGVyaWFscyBmbGFnZ2VkIGFzIGRpc2NvbnRpbnVlZCAoPGltZyBzcmM9Ii9pbWFnZXMvYnV0dG9ucy9pY29uRGlzY29udGludWVkLmpwZyIgYWx0PSIiIC8+KSBhcmUgbm8gbG9uZ2VyIHBhcnQgb2YgdGhlIG1hbnVmYWN0dXJlcuKAmXMgc3RhbmRhcmQgcHJvZHVjdCBsaW5lIGFjY29yZGluZyB0byBvdXIgbGF0ZXN0IGluZm9ybWF0aW9uLiAgVGhlc2UgbWF0ZXJpYWxzIG1heSBiZSBhdmFpbGFibGUgYnkgc3BlY2lhbCBvcmRlciwgaW4gZGlzdHJpYnV0aW9uIGludmVudG9yeSwgb3IgcmVpbnN0YXRlZCBhcyBhbiBhY3RpdmUgcHJvZHVjdC4gIERhdGEgc2hlZXRzIGZyb20gbWF0ZXJpYWxzIHRoYXQgYXJlIG5vIGxvbmdlciBhdmFpbGFibGUgcmVtYWluIGluIE1hdFdlYiB0byBhc3Npc3QgdXNlcnMgaW4gZmluZGluZyByZXBsYWNlbWVudCBtYXRlcmlhbHMuICANCjxiciAvPjxiciAvPg0KVXNlcnMgb2Ygb3VyIEFkdmFuY2VkIFNlYXJjaCAocmVnaXN0cmF0aW9uIHJlcXVpcmVkKSBtYXkgZXhjbHVkZSBkaXNjb250aW51ZWQgbWF0ZXJpYWxzIGZyb20gc2VhcmNoIHJlc3VsdHMuZGQCFg9kFgJmDxYCHwAFcDxhIGhyZWY9Ii9jbGlja3Rocm91Z2guYXNweD9hZGRhdGFpZD0yNzciIGNsYXNzPSJmb290bGluayI+PHNwYW4gY2xhc3M9ImZvb3QiPlRyYWRlJm5ic3A7UHVibGljYXRpb25zPC9zcGFuPjwvYT5kGAEFHl9fQ29udHJvbHNSZXF1aXJlUG9zdEJhY2tLZXlfXxYEBTZjdGwwMCRDb250ZW50TWFpbiRVY01hdEdyb3VwRmluZGVyMSRzZWxlY3RDYXRlZ29yeUxpc3QFK2N0bDAwJENvbnRlbnRNYWluJHVjTWF0R3JvdXBUcmVlJG1zVHJlZVZpZXcFG2N0bDAwJENvbnRlbnRNYWluJGJ0blN1Ym1pdAUaY3RsMDAkQ29udGVudE1haW4kYnRuUmVzZXTx4FWQeRJBjnX+wpt5F5Gv2Esc9g==" />
</div>

<script type="text/javascript">
//<![CDATA[
var theForm = document.forms['aspnetForm'];
if (!theForm) {
    theForm = document.aspnetForm;
}
function __doPostBack(eventTarget, eventArgument) {
    if (!theForm.onsubmit || (theForm.onsubmit() != false)) {
        theForm.__EVENTTARGET.value = eventTarget;
        theForm.__EVENTARGUMENT.value = eventArgument;
        theForm.submit();
    }
}
//]]>
</script>


<script src="/WebResource.axd?d=M68BSyLvwBMHRYzsSV62mvczr7w5VyKnq2JbgqcAAU1ioThlC6FNEVBncBdaA4oqf8cXPzy7QERzajvFfM67hSR6S0w1&amp;t=638313619312541215" type="text/javascript"></script>


<script src="/WebResource.axd?d=ClzPoKUn1iQnMGy7NeMJw70TvMayZ5w9nNmB7h_dY64i4BFFgjVcppyp69BFqhuzHctzc-rEantAjczzQ6UVMYkaWJw1&amp;t=638313619312541215" type="text/javascript"></script>
<script type="text/javascript">
//<![CDATA[

    function TreeView_PopulateNodeDoCallBack(context,param) {
        WebForm_DoCallback(context.data.treeViewID,param,TreeView_ProcessNodeData,context,TreeView_ProcessNodeData,false);
    }
var ctl00_ContentMain_ucMatGroupTree_msTreeView_Data = null;//]]>
</script>

<script src="/ScriptResource.axd?d=148LWaywfmset9PYVR0m-KYXX9JfyGYGqOtIYBGEAzoJh5VhFBwSI6JxnmoNvgtlAprhvntpc6lTDbRMgdKxckUmc8m2qw_fOCsX0G9LC8ojUpw4cTHWXgfuT_vU6m2Kpv3mt01iCczrTE2-J5X6EeBa2281&amp;t=637769794779625837" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=v_V1UgDrDAchg8Zuc1FL4qkvmjt-bwap7ZxwVGGPleYiJ6zfDOTL6wtCmVvILjbwo_sqb2PMUTNkjO4vp6nd-E7WWXTPgSI5-nvn3YDnHrVk5bgVX4Qv6F0fV4wTSslmaLZ3k98tckojDLWAEI7jm8h0H4d7BoGYxkqsyh8-Iujfrjjm0&amp;t=637769794779625837" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=V7Zma3AISNyGTbHVRPw8svJ4Ub7xUUaeoKwImcvnuDiy-XBR331l3eTjiEP8J_QlX2qgyAHxZFLywNbNyZ50fJLVEOCjx11lIDJOiQxc0oGA3qi7XXiggiwEcIWwzb9j2Is1n1D9sU40Zusk35AtkqRKL3Y1&amp;t=633177911400000000" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=wOguQo_yd3hCG4ajcSXPC6-XdeL0hmYWSMaFjvMXt1CBqR9neJqTXyTfPki4lIz4B29Zz_83btvS12BY6KpBKT-AgP0eaXp7V3h6XRdjiozV5w_n3oQRZg5Yq1QW8UNGj0Cy9jsap_7EcU2M99CapUyVG6I1&amp;t=633177911400000000" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=MAJxSy62Z3EtRwHXuuMc8KC6PFmrPqmkpcwdpcPGeqtqUQyJ3FBQ3cFyYWN1zNID_ngA53aMZdG_dpRu3TRDH8-iqKAjPAcc2BWe63WYGrGbyHc9syJxR6lypTgvRaYmwwEmkLEsaGUniMurHc16yr1VrjmzG3HWOeu9UXNr23Wv2xzh0&amp;t=633177911400000000" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=ukx9eyyt483y_CGgCF1OToQPWzPYyCynhpMvhxN7idzW3WaKjHjpgAOcMgtXZIEcspnbyDEADdRWCSHB-hF39Ycl13KWsqdlOHElw2qgSkGmkL8iNH37htb0BNS0lxw6oHvtqYiVpw-nZeYoQ1NkgX2qXOI1&amp;t=633177911400000000" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=Ovkr-MWDypjnq0C7FVbNgOs-Mt3NaktNVf8SEOX721PoQ5mQqTM6S4igSQL1_PXvh96km3stLiqrorOgm8nnnUU6y6edTkhxmLiwfFtEXQEAAnFFQpUepgsxEd0ahMvWcDfEac0EE7J8iDb2rqSMTxk51ydfY-c-8zcKvo5Mrn83cvfN0&amp;t=633177911400000000" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=pKQF4iQYwkSn5cAwTNDM8ez4TLepowWnai-WD6NVL5h0ay9Qtt0a9vPCaI-KN9OHcv1fL8VnwxEDM6EAVtgZY06V8XJTlVrbmb8WlMvGmhwBeL3_SVtKKOS-QExY8AnVbTFl_wOknplhy8bz-9sIWNCh4XPq1WhRj7ALNAKaeruPhS7d0&amp;t=633177911400000000" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=uM6eUuIuIa_NiCgVrtwgqczDeqTqI37XalY8Ri_W2ZjXEzIKi84afVBjFk8gneIDXcFnw8yjCD1qhE8mzDTzAUSdKnbDte0yC23kMoyvxPyu7vRH9E-6JMZtE8-gELUPmoZdOZJ9nXitHKYBLw2P6skU1IkzFRegK74bETu2FdEtuaeR0&amp;t=633177911400000000" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=ewF5FMb1ckbNCdJ9rpE5xMSXy-WecqVGE8uma1_520KusuW7jTUE2tnPMG7JyR0FDIFqDUMQbsfTZlsQ0sD4YAW8MnN90GKIMPvzaP7eOG3ss8E050_l7Rpnaz7I_U3ITzBXkcJZIOPcI1PxOReT8FSeWmo1&amp;t=633177911400000000" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=3lB_EBCMPyiwzDhzWdoE-i4mLHhkIMbbkaLwMr-U1WcAoDqBHS-oYFn4gz3ZwnpzK_KZp97Ev8ZbiUNo7WMJ45h2xgOH59erMFo1BpNb87-yu3X0mFyWs5rB3ANm4lsVSIu7RZd7XUEwfZXzugJ3CoJqxvRqiTGLpcXquEF24hK3OUH-0&amp;t=633177911400000000" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=Z7-pZSKJ6vCyr9H9S4r9baB7TLiJ-T4EoMOvlH-iQ5-icaO-vWMrjXk3EPctuPrcO5qwJSTaLQtHyguwle7Kty78exrSwHataIriNUEu1DnUkixv4pxU7Dknp_GDVHby5XcMA4lMwazAE0AFG9Cndu0An_gAGACGlYXsGUG9W6okCQsH0&amp;t=633177911400000000" type="text/javascript"></script>
<script src="../WebServices/Materials.asmx/js" type="text/javascript"></script>
<script src="../WebServices/MatGroups.asmx/js" type="text/javascript"></script>
<script src="../WebServices/Folders.asmx/js" type="text/javascript"></script>
<script type="text/javascript">
//<![CDATA[
function WebForm_OnSubmit() {
null;
return true;
}
//]]>
</script>

<div>

	<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="640EFF8C" />
</div>

<!--
The ScriptManager control is required for all ASP AJAX functionality - especially used by search pages.
It generates the javascript required.
Only one script manager is allowed on a page, and since this is the master page, this is the main script manager
However, a ScriptManagerProxy control can be used to set properties and settings on indivual pages.
For an example, see /WebControls/ucSearchResults.ascx
-->
<script type="text/javascript">
//<![CDATA[
Sys.WebForms.PageRequestManager._initialize('ctl00$ajxScriptManager', document.getElementById('aspnetForm'));
Sys.WebForms.PageRequestManager.getInstance()._updateControls([], [], [], 90);
//]]>
</script>


<table class="tabletight" style="width:100%; text-align:center;background-image: url(/images/gray.gif);">
  <tr >
    <td rowspan="2"><a href="/index.aspx"><img src="/images/logo1.gif" width="270" height="42" alt="MatWeb - Material Property Data"  /></a></td>

                
                <td style="text-align:right;vertical-align:middle; width:90%" ><span class='greeting'><span class='hiText'><a href ='/services/advertising.aspx'>Advertise with MatWeb!</a></span>&nbsp;&nbsp;&nbsp;&nbsp;</span></td>
                <td style="text-align:right;vertical-align:middle; padding-right:5px;" ><a href='/membership/regstart.aspx'><img src='/images/buttons/btnRegisterBSF.gif' width="62" height="20" alt="Register Now" /></a></td>

                
  </tr>
  <tr >
    <td  align="left" valign="bottom" colspan="2"><span class="tagline">
    
    Data sheets for over <span class="greeting">180,000</span>  metals, plastics, ceramics, and composites.</span></td>
  </tr>
  <tr style="background-image:url(/images/blue.gif); background-color:#000099; " >
    <td bgcolor="#000099"><a href="/index.aspx"><img src="/images/logo2.gif" width="270" height="23px" alt="MatWeb - Material Property Data" border="0" /></a></td>
    <td class="topnavig8" style=" background-color:#000099;vertical-align:middle; text-align:right; white-space:nowrap;" colspan="2" align="right" valign="middle">
        <a href="/index.aspx" class="topnavlink">HOME</a>&nbsp;&nbsp;&#8226;&nbsp;&nbsp;
                        <a href="/search/search.aspx" class="topnavlink">SEARCH</a>&nbsp;&nbsp;&#8226;&nbsp;&nbsp;
                        <a href="/tools/tools.aspx" class="topnavlink">TOOLS</a>&nbsp;&nbsp;&#8226;&nbsp;&nbsp;
                        <a href="/reference/suppliers.aspx" class="topnavlink">SUPPLIERS</a>&nbsp;&nbsp;&#8226;&nbsp;&nbsp;
                        <a href="/folders/ListFolders.aspx" class="topnavlink">FOLDERS</a>&nbsp;&nbsp;&#8226;&nbsp;&nbsp;
                        <a href="/services/services.aspx" class="topnavlink">ABOUT US</a>&nbsp;&nbsp;&#8226;&nbsp;&nbsp;
                        <a href="/reference/faq.aspx" class="topnavlink">FAQ</a>&nbsp;&nbsp;&#8226;&nbsp;&nbsp;
                        
                        <a href="/membership/login.aspx" class="topnavlink">LOG IN</a>
                        
                        &nbsp;
                        
                        &nbsp;
    </td>
  </tr>
</table>

<div id="divRecentMatls" class="tableborder tight" style=" display:none; position:absolute; top:95px; left:150px; width:400px; height:400px;" onmouseover="clearTimeout(tmrtmp);" onmouseout="tmrtmp=setTimeout('RecentMatls.Hide()', 800)">
        <table class="headerblock tabletight" style="vertical-align:top; font-size:13px; width:100%;"><tr>
                <td style="padding:3px;"> Recently Viewed Materials (most recent at top)</td>
                <td style="padding:3px;text-align:right;"><input type="image" src="/images/buttons/btnCloseWhite.gif" onclick="RecentMatls.Hide();return false;" style="cursor:pointer;" />&nbsp;</td>
        </tr></table>
        <div id="divRecentMatlsBody" style="height:375px; overflow:auto; background-color:White;">

                <table id="tblRecentMatls" class="tabledataformat tableloose" style="background-color:White;">
    
                <tr><td>
                <p />
                <a href="/membership/login.aspx">Login</a> to see your most recently viewed materials here.<p />
                Or if you don't have an account with us yet, then <a href="/membership/regstart.aspx">click here to register.</a>
                </td></tr>
                
                </table>

        </div>
</div>

<script type="text/javascript">

        var RecentMatls = new _RecentMatls();
        var tmrtmp;

        function _RecentMatls(){

                var divRecentMatls;
                var tblRecentMatls;
                var trRecentMatlsRowTemplate;
                var trRecentMatlsNoData;
                var DataIsLoaded=false;

                //Using prototype.js methods...
                divRecentMatls = $("divRecentMatls");
                tblRecentMatls = $("tblRecentMatls");
    

                divRecentMatls.style.display = "none";

                this.Toggle = function(){
                        //alert("Toggle()");
                        if(divRecentMatls.style.display == "block"){this.Hide();}
                        else{this.Show();}
                }

                this.Show = function(){
                        //alert("Show()");
                        divRecentMatls.style.display = "block";
                        matweb.toggleSelect(divRecentMatls,0);
                        if(!DataIsLoaded){
            
                        }
                }

                this.Hide = function(){
                        //alert("Hide()");
                        divRecentMatls.style.display = "none";
                        matweb.toggleSelect(divRecentMatls,1);
                }

    

        }//end class

</script>

<table class="tabletight t_ableborder t_ablegrid" style="width:100%;" >
  <tr>
   <td style="width:100%; height:27px; white-space:nowrap; text-align:left; vertical-align:middle;" class="tagline" >
      <span class="navlink"><b>&nbsp;&nbsp;Searches:</b></span>
      &nbsp;&nbsp;<a href="/search/AdvancedSearch.aspx" class="navlink"><span class="nav"><font color="#CC0000">Advanced</font></span></a>
      &nbsp;|&nbsp;<a href="/search/MaterialGroupSearch.aspx" class="navlink"><span class="nav">Category</span></a>
      &nbsp;|&nbsp;<a href="/search/PropertySearch.aspx" class="navlink"><span class="nav">Property</span></a>
      &nbsp;|&nbsp;<a href="/search/CompositionSearch.aspx" class="navlink"><span class="nav">Metals</span></a>
      &nbsp;|&nbsp;<a href="/search/SearchTradeName.aspx" class="navlink"><span class="nav">Trade Name</span></a>
      &nbsp;|&nbsp;<a href="/search/SearchManufacturerName.aspx" class="navlink"><span class="nav">Manufacturer</span></a>
                &nbsp;|&nbsp;<a href="javascript:RecentMatls.Toggle();" id="ctl00_lnkRecentMatls" class="navlink"><span class="nav"><font color="#CC0000">Recently Viewed Materials</font></span></a>
    </td>

    <td style="text-align:right; vertical-align:bottom; padding-right:5px;">

      <table class="tabletight t_ableborder" ><tr>
        <td style=" white-space:nowrap; vertical-align:bottom; " >
                                        &nbsp;&nbsp;
                                        <input name="ctl00$txtQuickText" type="text" id="ctl00_txtQuickText" style="width:100px;" maxlength="40" class="quicksearchinput" onkeydown="txtQuickText_OnKeyDown(event);" />
                                        <input type="image" id="btnQuickTextSearch" style="vertical-align:top; width:60px; height:25px;" src="/images/buttons/btnSearch.gif" alt="Search" onclick="return btnQuickTextSearch_Click()"  />
                                        <script type="text/javascript">

                                                //SETUP THE AUTOPOST ON ENTER KEY EVENT FOR txtQuickText FIELD
                                                //document.getElementById("ctl00_txtQuickText").onkeydown=txtQuickText_OnKeyDown;

                                                function txtQuickText_OnKeyDown(evt){
                                                        //SUBMIT THE FORM AS IF THE BUTTON WAS PUSHED
                                                        //var UniqueID = "";
                                                        var txtQuickText = document.getElementById('ctl00_txtQuickText');
                                                        if(matweb.IsEnterKey(evt))
                                                                if(checkSearchText(txtQuickText))
                                                                        //matweb.DoPostBack(UniqueID, "");
                                                                        window.location.href="/search/QuickText.aspx?SearchText=" + escape(txtQuickText.value);
                                                        return false;
                                                }

                                                function btnQuickTextSearch_Click(){
                                                        var txtQuickText = document.getElementById('ctl00_txtQuickText');
                                                        if(checkSearchText(txtQuickText))
                                                                window.location.href="/search/QuickText.aspx?SearchText=" + escape(txtQuickText.value);
                                                        return false;
                                                }

                                                function checkSearchText(searchbox){
                                                        searchbox.value = trim(searchbox.value);
                                                        if(searchbox.value == ''){
                                                                //alert('Please enter something to search for');
                                                                matweb.alert('Please enter something to search for', 'Search Error');
                                                                //searchbox.focus();
                                                                return false;
                                                        }
                                                        return true;
                                                }

                                                function trim(s)  { return ltrim(rtrim(s)) }
                                                function ltrim(s) { return s.replace(/^\s+/g, "") }
                                                function rtrim(s) { return s.replace(/\s+$/g, "") }

                                        </script>
                                </td>
                                </tr>
                        </table>

    </td>
  </tr>
  <tr style="background-image:url(/images/shad.gif);height:7px;" ><td colspan="2"></td></tr>
</table>

<!-- ====================================== MAIN CONTENT ============================================= -->
<div style="vertical-align:top; padding-left:10px; padding-right:10px;" >





	<center><a href="/clickthrough.aspx?addataid=1351"><img src="/images/assets/metalmen-Nickel.png" border=0 alt="Metalmen Sales" width = "728" height = "90"></a>
</center>
	
	<h2><a href="#help"><img src="/images/buttons/iconHelp.gif" alt="" style="vertical-align:bottom;" /></a>Material Category Search</h2>
	
	

<div id="ctl00_ContentMain_ucPopupMessage1_divPopupMessage" class="divPopupAlert" style="display:none;width:250px;">
	<table >
		<tr id="ctl00_ContentMain_ucPopupMessage1_trTitleRow" class="divPopupAlert_TitleRow" style="cursor:move;">
	<th><span id="ctl00_ContentMain_ucPopupMessage1_lblTitle">User Input Problem</span></th>
</tr>

		<tr class="divPopupAlert_ContentRow" ><td style="" ><span id="ctl00_ContentMain_ucPopupMessage1_lblMessage"></span></td></tr>
		<tr class="divPopupAlert_ButtonRow"><td><input name="ctl00$ContentMain$ucPopupMessage1$btnOK" type="button" id="ctl00_ContentMain_ucPopupMessage1_btnOK" value="OK" /></td></tr>
	</table>
</div>

<input type="hidden" name="ctl00$ContentMain$ucPopupMessage1$hndPopupControl" id="ctl00_ContentMain_ucPopupMessage1_hndPopupControl" />


	<table class="" style="width:100%;">
	<tr>
		<td style=" vertical-align:top; width:300px;" >
			<strong>Find a Material Category:</strong>
			

<input name="ctl00$ContentMain$UcMatGroupFinder1$txtSearchText" type="text" value="Nickel" id="ctl00_ContentMain_UcMatGroupFinder1_txtSearchText" style="Width:304px;" /><br />
<select name="ctl00$ContentMain$UcMatGroupFinder1$selectCategoryList" id="ctl00_ContentMain_UcMatGroupFinder1_selectCategoryList" style="position:absolute;display:none;Width:308px;" size="18">
</select>

<input type="hidden" name="ctl00$ContentMain$UcMatGroupFinder1$TextBoxWatermarkExtender2_ClientState" id="ctl00_ContentMain_UcMatGroupFinder1_TextBoxWatermarkExtender2_ClientState" />

<script type="text/javascript">

var ucMatGroupFinderManager = new _ucMatGroupFinderManager("ctl00_ContentMain_UcMatGroupFinder1_txtSearchText","ctl00_ContentMain_UcMatGroupFinder1_selectCategoryList");

function _ucMatGroupFinderManager(txtSearchTextID,selectCategoryListID){

	var txtSearchText = document.getElementById(txtSearchTextID);
	var selectCategoryList = document.getElementById(selectCategoryListID);
	var showtime;
	var blurtime;
	var MinTextLength = 4;
	var KeyStrokeActionDelay = 650;
	var FadeInOutDelay = 0.2;
	var LastUsedText;
	
	//The init function is currently run at the bottom of this class
	this.init=function(){

		txtSearchText.onfocus = txtSearchText_onfocus;
		txtSearchText.onkeyup = txtSearchText_checktext;
		txtSearchText.onblur = selectCategoryList_startblur;
		
		selectCategoryList.onfocus = selectCategoryList_clearblur;
		selectCategoryList.onchange = selectCategoryList_onchange;
		selectCategoryList.onblur = selectCategoryList_startblur;

	}

	txtSearchText_onfocus = function(){
		if(txtSearchText.value == "Type at least 4 characters here...")
			txtSearchText.value = "";
		selectCategoryList_clearblur();
		txtSearchText_checktext();
	}

	function txtSearchText_checktext(){
		searchText = txtSearchText.value;
		//alert(searchText);
		clearTimeout(showtime);
		if(searchText.length >= MinTextLength){
			showtime = setTimeout(function(){
				selectCategoryList.style.display = "block";
				if(txtSearchText.value!=LastUsedText){
					LastUsedText = searchText;
					aci.matweb.WebServices.MatGroups.FindMatGroups(searchText,DisplayCatData,matweb.WebServiceErrorHandler,"txtSearchText_checktext");
				}
				//AjaxControlToolkit.Animation.FadeInAnimation.play(selectCategoryList, FadeInOutDelay, 1, 25, 0, 1, true);
			},KeyStrokeActionDelay);
		}
		else{
			LastUsedText = "";
			showtime = setTimeout(function(){
				selectCategoryList.options.length = 0;
				selectCategoryList.style.display = "none";
				//AjaxControlToolkit.Animation.FadeOutAnimation.play(selectCategoryList,FadeInOutDelay);
				//AjaxControlToolkit.Animation.FadeOutAnimation.play(selectCategoryList,FadeInOutDelay, 25, 0, 1, true);
			},KeyStrokeActionDelay);
		}
	}

	function selectCategoryList_clearblur(){
		clearTimeout(blurtime);
	}

	function selectCategoryList_onchange(){
		var MatGroupID = selectCategoryList.options[selectCategoryList.selectedIndex].value;
		var MatGroupText = selectCategoryList.options[selectCategoryList.selectedIndex].text;
		//alert("Selected MatGroupID: " + MatGroupID);
		//alert("Selected MatGroupText: " + MatGroupText);
		//When no data is found, we have a "No Categories Contain Text..." message, but the MatGroupID is set to 0, so we can ignore if MatGroupID == 0.
		if(MatGroupID > 0){
			ucMatGroupFinder1_OnSelected(MatGroupID,MatGroupText)
		}
		selectCategoryList_startblur();
		//txtSearchText.value = "";
	}
	
	function selectCategoryList_startblur(){
		//alert("selectCategoryList_onblur()");
		clearTimeout(blurtime);
		blurtime = setTimeout(function(){
			selectCategoryList.style.display = "none";
			//AjaxControlToolkit.Animation.FadeOutAnimation.play(selectCategoryList,FadeInOutDelay, 25, 0, 1, true);
		},KeyStrokeActionDelay);
	}
	
	function DisplayCatData(MatGroupData){
		//alert("GetCatData()");
		//alert("vwMaterialGroupList: " + vwMaterialGroupList);
		var MatGroupID,GroupName, MatCount;
		selectCategoryList.options.length = 0;//clear any existing options....
		if(MatGroupData.length==0){
				//alert(vwMaterialGroupList[i].GroupName);
				MatGroupID = 0;
				GroupName = "No categories contain the text: " + txtSearchText.value
				selectCategoryList.options[0] = new Option(GroupName,MatGroupID);					
		}
		else{
			for(i=0;i<MatGroupData.length;i++){
				//alert(vwMaterialGroupList[i].GroupName);
				MatCount = MatGroupData[i].MatCount;
				MatGroupID = MatGroupData[i].MatGroupID;
				GroupName = MatGroupData[i].GroupName + " (" + MatCount + " matls)";
				selectCategoryList.options[i] = new Option(GroupName,MatGroupID);					
			}
		}
	}
	
	this.init();

}//end class
	
</script>



			<strong>Select a Material Category:</strong>
			
			<div class="tableborder" style=" height:225px; width:300px; overflow:scroll; ">
			<a href="#ctl00_ContentMain_ucMatGroupTree_msTreeView_SkipLink"><img alt="Skip Navigation Links." src="/WebResource.axd?d=vAkieGj3ugBpu6CKp7dqvCP5iHLzPbUd2XME-h1vQcC6d5hLg7Vc1kNVzB8A45Ui3K9XZ9m3-E2pxsaJQYh2uMkAul41&amp;t=638313619312541215" width="0" height="0" style="border-width:0px;" /></a><div id="ctl00_ContentMain_ucMatGroupTree_msTreeView">
	<table cellpadding="0" cellspacing="0" style="border-width:0;">
		<tr>
			<td><a id="ctl00_ContentMain_ucMatGroupTree_msTreeViewn0" href="javascript:TreeView_PopulateNode(ctl00_ContentMain_ucMatGroupTree_msTreeView_Data,0,document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewn0'),document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewt0'),null,'r','Carbon (866 matls)','283','f','','f')"><img src="/WebResource.axd?d=6v5qM6VKQYD3XNwwXEy7Tix7dzVdqDJ8QTRIlUPrzXQqNsXQgUwIfmn34ZzRORd_IZj0bF4vkZzID1HuSFE5uKyLgaPYeAYE8VT8HYfb3Rk6zsDw0&amp;t=638313619312541215" alt="Expand Carbon (866 matls)" style="border-width:0;" /></a></td><td class="ctl00_ContentMain_ucMatGroupTree_msTreeView_2"><a class="ctl00_ContentMain_ucMatGroupTree_msTreeView_0 ctl00_ContentMain_ucMatGroupTree_msTreeView_1" href="javascript:ucMatGroupTree_OnNodeClick('Carbon (866 matls)','283')" id="ctl00_ContentMain_ucMatGroupTree_msTreeViewt0">Carbon (866 matls)</a></td>
		</tr><tr style="height:0px;">
			<td></td>
		</tr>
	</table><table cellpadding="0" cellspacing="0" style="border-width:0;">
		<tr style="height:0px;">
			<td></td>
		</tr><tr>
			<td><a id="ctl00_ContentMain_ucMatGroupTree_msTreeViewn1" href="javascript:TreeView_PopulateNode(ctl00_ContentMain_ucMatGroupTree_msTreeView_Data,1,document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewn1'),document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewt1'),null,'t','Ceramic (10004 matls)','11','f','','f')"><img src="/WebResource.axd?d=TinC6igX1vPDgWh7DstWwBTHTyH4uuybDU4NW47jJD06wHdAO3YyJC_eKK1Zn5oIbwqRo4Cri8YwhA82G-LD0p14yEF1EfoIZvKleYMIuQ2K1hkE0&amp;t=638313619312541215" alt="Expand Ceramic (10004 matls)" style="border-width:0;" /></a></td><td class="ctl00_ContentMain_ucMatGroupTree_msTreeView_2"><a class="ctl00_ContentMain_ucMatGroupTree_msTreeView_0 ctl00_ContentMain_ucMatGroupTree_msTreeView_1" href="javascript:ucMatGroupTree_OnNodeClick('Ceramic (10004 matls)','11')" id="ctl00_ContentMain_ucMatGroupTree_msTreeViewt1">Ceramic (10004 matls)</a></td>
		</tr><tr style="height:0px;">
			<td></td>
		</tr>
	</table><table cellpadding="0" cellspacing="0" style="border-width:0;">
		<tr style="height:0px;">
			<td></td>
		</tr><tr>
			<td><a id="ctl00_ContentMain_ucMatGroupTree_msTreeViewn2" href="javascript:TreeView_PopulateNode(ctl00_ContentMain_ucMatGroupTree_msTreeView_Data,2,document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewn2'),document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewt2'),null,'t','Fluid (7562 matls)','5','f','','f')"><img src="/WebResource.axd?d=TinC6igX1vPDgWh7DstWwBTHTyH4uuybDU4NW47jJD06wHdAO3YyJC_eKK1Zn5oIbwqRo4Cri8YwhA82G-LD0p14yEF1EfoIZvKleYMIuQ2K1hkE0&amp;t=638313619312541215" alt="Expand Fluid (7562 matls)" style="border-width:0;" /></a></td><td class="ctl00_ContentMain_ucMatGroupTree_msTreeView_2"><a class="ctl00_ContentMain_ucMatGroupTree_msTreeView_0 ctl00_ContentMain_ucMatGroupTree_msTreeView_1" href="javascript:ucMatGroupTree_OnNodeClick('Fluid (7562 matls)','5')" id="ctl00_ContentMain_ucMatGroupTree_msTreeViewt2">Fluid (7562 matls)</a></td>
		</tr><tr style="height:0px;">
			<td></td>
		</tr>
	</table><table cellpadding="0" cellspacing="0" style="border-width:0;">
		<tr style="height:0px;">
			<td></td>
		</tr><tr>
			<td><a id="ctl00_ContentMain_ucMatGroupTree_msTreeViewn3" href="javascript:TreeView_PopulateNode(ctl00_ContentMain_ucMatGroupTree_msTreeView_Data,3,document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewn3'),document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewt3'),null,'t','Metal (17052 matls)','9','f','','f')"><img src="/WebResource.axd?d=TinC6igX1vPDgWh7DstWwBTHTyH4uuybDU4NW47jJD06wHdAO3YyJC_eKK1Zn5oIbwqRo4Cri8YwhA82G-LD0p14yEF1EfoIZvKleYMIuQ2K1hkE0&amp;t=638313619312541215" alt="Expand Metal (17052 matls)" style="border-width:0;" /></a></td><td class="ctl00_ContentMain_ucMatGroupTree_msTreeView_2"><a class="ctl00_ContentMain_ucMatGroupTree_msTreeView_0 ctl00_ContentMain_ucMatGroupTree_msTreeView_1" href="javascript:ucMatGroupTree_OnNodeClick('Metal (17052 matls)','9')" id="ctl00_ContentMain_ucMatGroupTree_msTreeViewt3">Metal (17052 matls)</a></td>
		</tr><tr style="height:0px;">
			<td></td>
		</tr>
	</table><table cellpadding="0" cellspacing="0" style="border-width:0;">
		<tr style="height:0px;">
			<td></td>
		</tr><tr>
			<td><a id="ctl00_ContentMain_ucMatGroupTree_msTreeViewn4" href="javascript:TreeView_PopulateNode(ctl00_ContentMain_ucMatGroupTree_msTreeView_Data,4,document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewn4'),document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewt4'),null,'t','Other Engineering Material (8063 matls)','285','f','','f')"><img src="/WebResource.axd?d=TinC6igX1vPDgWh7DstWwBTHTyH4uuybDU4NW47jJD06wHdAO3YyJC_eKK1Zn5oIbwqRo4Cri8YwhA82G-LD0p14yEF1EfoIZvKleYMIuQ2K1hkE0&amp;t=638313619312541215" alt="Expand Other Engineering Material (8063 matls)" style="border-width:0;" /></a></td><td class="ctl00_ContentMain_ucMatGroupTree_msTreeView_2"><a class="ctl00_ContentMain_ucMatGroupTree_msTreeView_0 ctl00_ContentMain_ucMatGroupTree_msTreeView_1" href="javascript:ucMatGroupTree_OnNodeClick('Other Engineering Material (8063 matls)','285')" id="ctl00_ContentMain_ucMatGroupTree_msTreeViewt4">Other Engineering Material (8063 matls)</a></td>
		</tr><tr style="height:0px;">
			<td></td>
		</tr>
	</table><table cellpadding="0" cellspacing="0" style="border-width:0;">
		<tr style="height:0px;">
			<td></td>
		</tr><tr>
			<td><a id="ctl00_ContentMain_ucMatGroupTree_msTreeViewn5" href="javascript:TreeView_PopulateNode(ctl00_ContentMain_ucMatGroupTree_msTreeView_Data,5,document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewn5'),document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewt5'),null,'t','Polymer (97635 matls)','10','f','','f')"><img src="/WebResource.axd?d=TinC6igX1vPDgWh7DstWwBTHTyH4uuybDU4NW47jJD06wHdAO3YyJC_eKK1Zn5oIbwqRo4Cri8YwhA82G-LD0p14yEF1EfoIZvKleYMIuQ2K1hkE0&amp;t=638313619312541215" alt="Expand Polymer (97635 matls)" style="border-width:0;" /></a></td><td class="ctl00_ContentMain_ucMatGroupTree_msTreeView_2"><a class="ctl00_ContentMain_ucMatGroupTree_msTreeView_0 ctl00_ContentMain_ucMatGroupTree_msTreeView_1" href="javascript:ucMatGroupTree_OnNodeClick('Polymer (97635 matls)','10')" id="ctl00_ContentMain_ucMatGroupTree_msTreeViewt5">Polymer (97635 matls)</a></td>
		</tr><tr style="height:0px;">
			<td></td>
		</tr>
	</table><table cellpadding="0" cellspacing="0" style="border-width:0;">
		<tr style="height:0px;">
			<td></td>
		</tr><tr>
			<td><img src="/WebResource.axd?d=7_V5n1KYSQsj6PoSqgIxb0egBsAa91jSXPkbmM6FaiF14-JsNnpUDnfSdxZcvfd0OQCLDtC24pMIXigO-41cL2rsq-0xea-SSG7yZUyRcliFFqe70&amp;t=638313619312541215" alt="" /></td><td class="ctl00_ContentMain_ucMatGroupTree_msTreeView_2"><a class="ctl00_ContentMain_ucMatGroupTree_msTreeView_0 ctl00_ContentMain_ucMatGroupTree_msTreeView_1" href="javascript:ucMatGroupTree_OnNodeClick('Pure Element (507 matls)','184')" id="ctl00_ContentMain_ucMatGroupTree_msTreeViewt6">Pure Element (507 matls)</a></td>
		</tr><tr style="height:0px;">
			<td></td>
		</tr>
	</table><table cellpadding="0" cellspacing="0" style="border-width:0;">
		<tr style="height:0px;">
			<td></td>
		</tr><tr>
			<td><a id="ctl00_ContentMain_ucMatGroupTree_msTreeViewn7" href="javascript:TreeView_PopulateNode(ctl00_ContentMain_ucMatGroupTree_msTreeView_Data,7,document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewn7'),document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewt7'),null,'l','Wood and Natural Products (398 matls)','277','f','','t')"><img src="/WebResource.axd?d=vUBrM2nNhECNbFO0AJKq52p2zWrY5XG8nWgCDL4AfsrexStb0HREDV6GHMvdkAGDInxYtTyWXuxFUIiQv3JX2fScVRwQsrVv5srHrjOIeC3ayAG20&amp;t=638313619312541215" alt="Expand Wood and Natural Products (398 matls)" style="border-width:0;" /></a></td><td class="ctl00_ContentMain_ucMatGroupTree_msTreeView_2"><a class="ctl00_ContentMain_ucMatGroupTree_msTreeView_0 ctl00_ContentMain_ucMatGroupTree_msTreeView_1" href="javascript:ucMatGroupTree_OnNodeClick('Wood and Natural Products (398 matls)','277')" id="ctl00_ContentMain_ucMatGroupTree_msTreeViewt7">Wood and Natural Products (398 matls)</a></td>
		</tr><tr style="height:0px;">
			<td></td>
		</tr>
	</table>
</div><a id="ctl00_ContentMain_ucMatGroupTree_msTreeView_SkipLink"></a>
<span id="ctl00_ContentMain_ucMatGroupTree_lblDebug"></span>

			</div>
			
			<strong>Selected Material Category: </strong>
			<div id="ctl00_ContentMain_divMatGroupName" class="selectedItem" style="width:306px;">Nickel Alloy (1210 matls)</div>
			<input name="ctl00$ContentMain$txtMatGroupID" type="hidden" id="ctl00_ContentMain_txtMatGroupID" value="193" />
			<input name="ctl00$ContentMain$txtMatGroupText" type="hidden" id="ctl00_ContentMain_txtMatGroupText" value="Nickel Alloy (1210 matls)" />
			
			

			<input type="image" name="ctl00$ContentMain$btnSubmit" id="ctl00_ContentMain_btnSubmit" src="../images/buttons/btnFind.gif" alt="Find" style="border-width:0px;" /> <!-- OnClientClick="return validate()" -->
			<input type="image" name="ctl00$ContentMain$btnReset" id="ctl00_ContentMain_btnReset" title="Clear Search" src="/images/buttons/btnReset.gif" alt="Clear Search" style="border-width:0px;" />

			<span id="ctl00_ContentMain_lblDebug"></span>

		</td>
		<td>&nbsp;</td>
		<td style=" vertical-align:top; width:80%;" >
			<strong>Search Results</strong>
			<div class="tableborder" id="divSearchResults" style="padding:5px;" >
				
				<div id="ctl00_ContentMain_pnlSearchResults">
	
					There are <span id="ctl00_ContentMain_lblMatlCount">1245</span> materials in the category 
					<span id="ctl00_ContentMain_lblMaterialGroupName" class="selectedItem">Nickel Alloy</span>.
					If your material is not listed, please refer to our <a href="/help/strategy.aspx">search strategy</a> page for assistance in limiting your search.
				
</div>
			</div>
			<p />




<div id="ctl00_ContentMain_UcSearchResults1_pnlResultsFound" class="tableborder">
	

	<table style="background-color:Silver; width:100%;" class="" >
		<tr>
			<td><strong>Found <span id="ctl00_ContentMain_UcSearchResults1_lblResultCount">1245</span> Results</strong> -- 
				Page 
				<select name="ctl00$ContentMain$UcSearchResults1$drpPageSelect1" onchange="javascript:setTimeout('__doPostBack(\'ctl00$ContentMain$UcSearchResults1$drpPageSelect1\',\'\')', 0)" id="ctl00_ContentMain_UcSearchResults1_drpPageSelect1">
		<option value="1">1</option>
		<option value="2">2</option>
		<option value="3">3</option>
		<option selected="selected" value="4">4</option>
		<option value="5">5</option>

	</select>
				of <span id="ctl00_ContentMain_UcSearchResults1_lblPageTotal">7</span> 
				-- 
				<a id="ctl00_ContentMain_UcSearchResults1_lnkPrevPage" href="javascript:__doPostBack('ctl00$ContentMain$UcSearchResults1$lnkPrevPage','')" style="color:Black;">[Prev Page]</a>
				<a id="ctl00_ContentMain_UcSearchResults1_lnkNextPage" href="javascript:__doPostBack('ctl00$ContentMain$UcSearchResults1$lnkNextPage','')" style="color:Black;">[Next Page]</a>
				--&nbsp;			
				view
				<select name="ctl00$ContentMain$UcSearchResults1$drpPageSize1" onchange="javascript:setTimeout('__doPostBack(\'ctl00$ContentMain$UcSearchResults1$drpPageSize1\',\'\')', 0)" id="ctl00_ContentMain_UcSearchResults1_drpPageSize1">
		<option value="25">25</option>
		<option value="50">50</option>
		<option value="75">75</option>
		<option value="100">100</option>
		<option value="150">150</option>
		<option selected="selected" value="200">200</option>

	</select> per page
			</td>
		</tr>
		
	</table>
	
	
	
	
	<table class="t_ablegrid" style="width:100%;">
		<tr style="font-size:9px;">
			<td style="font-size:9px;">Use Folder</td>
			<td style="font-size:9px;">Contains</td>
			<td style="font-size:9px;">&nbsp;</td>
			
			<td style="font-size:9px;"><div id="divFolderName" style="display:none;">New Folder Name</div></td>
			<td style="font-size:9px;"></td>
			<td style="width:90%"></td>
		</tr>
		<tr style="vertical-align:top;">
			<td style="vertical-align:top;">
				<select name="ctl00$ContentMain$UcSearchResults1$drpFolderList" id="ctl00_ContentMain_UcSearchResults1_drpFolderList" onchange="SetSelectedFolder()" style="width:125px;">
		<option selected="selected" value="0">My Folder</option>

	</select>
			</td>
			<td style="vertical-align:top;">
				<strong><input name="ctl00$ContentMain$UcSearchResults1$txtFolderMatCount" type="text" id="ctl00_ContentMain_UcSearchResults1_txtFolderMatCount" readonly="readonly" style="border:none 0 white; background-color:White; color:Black; width: 35px; font-weight:bold;" value="0/0" /></strong>
			</td>
			<td>
				
				<input type="image" id="btnCompareFolders" src="/images/buttons/btnCompareMatls.gif" alt="Compare Checked Materials" onclick="CompareMaterials();return false;" />
			</td>
			
			<td style="white-space:nowrap;vertical-align:top;">
			
				<table id="tblFolderName" class="tabletight" style="display:none;"><tr><td><input type="text" id="txtFolderName2" style="display:inline; width:100px;" /><br /></td>
				<td><input type="image" id="btnApplyFolderName2" src="/images/buttons/btnSaveChanges.gif" style="display:inline;" onclick="ApplyFolderName();return false;" />
				<input type="image" id="btnHideFolderNameControls2" src="/images/buttons/btnCancel.gif" style="display:inline;" onclick="HideFolderNameControls();return false;" /></td>
				</tr></table>
				
			</td>
			<td style="white-space:nowrap;">
				<div class="usermessage" id="divUserMessage"></div>
			</td>
		</tr>
	</table>

	<table class="tabledataformat t_ablegrid" id="tblResults" style="table-layout:auto;"  >

		<tr class="">

			
		
			<th style="width:65px; white-space:nowrap;">Select</th>

			<th style="width:10px;">
				<!--Discontinued and Found Via Interpolated Indicators -->
			</th>
			
			<th style="width:auto;">
				<a id="ctl00_ContentMain_UcSearchResults1_lnkSortMatName" disabled="disabled" title="Click to Sort By Material Name">Material Name</a>
				<br />
				
				
			</th>
		
			
			
			

			

			

			

			

			

			

			

			
		</tr>

		<!-- this is where the search results are actually loaded.  See code behind -->		
		<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17300"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;601</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17300' href="/search/DataSheet.aspx?MatGUID=15bf603e76504b8290e8e4dfba6cb2b4" >Haynes Hastelloy® G-30® alloy, all-weld metal, GMAW weld 1.1 mm diameter filler wire</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17350"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;602</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17350' href="/search/DataSheet.aspx?MatGUID=c8c0b158524a446bb953b781dee28efb" >Hoskins Manufacturing Alumel® Thermocouple Wire Type K - Std</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17352"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;603</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17352' href="/search/DataSheet.aspx?MatGUID=e14ca9c83c5f45dd945e655fa4edff8b" >Hoskins Manufacturing Chromel® Thermocouple Wire</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17353"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;604</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17353' href="/search/DataSheet.aspx?MatGUID=c7a3c2c8a4614b0b9bed09a405751e36" >Haynes R-41 alloy, 0.635 mm thick sheet, solution heat treated at 1079°C, water quenched, 30 minutes at 1066°C, aged 16 hours at 760°C (1400°F), air cooled</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17356"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;605</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17356' href="/search/DataSheet.aspx?MatGUID=d1fa65c8caba4185b0d5853ab22c4b8a" >Haynes R-41 alloy, 1.27 mm thick sheet, solution heat treated at 1079°C, water quenched, 30 minutes at 1066°C, aged 16 hours at 760°C (1400°F), air cooled</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17363"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;606</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17363' href="/search/DataSheet.aspx?MatGUID=0c44870be3da48b596925422f96628fe" >Haynes R-41 alloy, <0.762 mm thick sheet, 30 minutes at 1066°C, air cooled, aged 16 hours at 760°C (1400°F), air cooled</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17364"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;607</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17364' href="/search/DataSheet.aspx?MatGUID=1f7b9998900143089ace29889a8326e4" >Haynes R-41 alloy, <0.762 mm thick sheet, 30 minutes at 1121°C, air cooled, aged 16 hours at 899°C, air cooled</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17365"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;608</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17365' href="/search/DataSheet.aspx?MatGUID=3fda8a35f33843f2adc2eea09cbc3228" >Haynes R-41 alloy, sheet 0.762-2.54 mm exclusive, 30 minutes at 1066°C, air cooled, aged 16 hours at 760°C (1400°F)</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17366"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;609</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17366' href="/search/DataSheet.aspx?MatGUID=dee3a175a160413abdc9fd7dd5f5aff2" >Haynes R-41 alloy, sheet 0.762-2.54 mm exclusive, 30 minutes at 1121°C, air cooled, aged 16 hours at 899°C</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17367"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;610</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17367' href="/search/DataSheet.aspx?MatGUID=727890c7552a4b1d8b335b36ba3eb882" >Haynes R-41 alloy, 1.27 mm thick sheet, solution heat treated at 1079°C, water quenched, 30 minutes at 1066°C, air cooled, aged 16 hours at 760°C (1400°F)</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17373"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;611</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17373' href="/search/DataSheet.aspx?MatGUID=6fef806afc1046cd8eef976b495e1860" >Haynes Waspaloy™ alloy, sheet</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_177695"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;612</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_177695' href="/search/DataSheet.aspx?MatGUID=813be78ffa64451ca7e06bc93b201f55" >IBC Advanced Alloys FA230 Beryllium Nickel Casting Alloy</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_177696"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;613</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_177696' href="/search/DataSheet.aspx?MatGUID=805d48f2897f4a85af3e965ea5f49e5a" >IBC Advanced Alloys 41C Beryllium Nickel Casting Alloy</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_177697"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;614</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_177697' href="/search/DataSheet.aspx?MatGUID=1a0d3dd216e74d2d991d26f162c53bc5" >IBC Advanced Alloys 42C Beryllium Nickel Casting Alloy</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_177698"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;615</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_177698' href="/search/DataSheet.aspx?MatGUID=2c8d07b44870405293bf94acdad02d49" >IBC Advanced Alloys 43C Beryllium Nickel Casting Alloy</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_177699"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;616</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_177699' href="/search/DataSheet.aspx?MatGUID=ce03421809f54f2594a54c6fb30622db" >IBC Advanced Alloys 44C Beryllium Nickel Casting Alloy</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_177700"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;617</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_177700' href="/search/DataSheet.aspx?MatGUID=0e35c4d3fa28494c880a2b1c247d693f" >IBC Advanced Alloys 46C Beryllium Nickel Casting Alloy</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_177703"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;618</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_177703' href="/search/DataSheet.aspx?MatGUID=1b8864ab71bd4054b47aa07e0fc8f681" >IBC Advanced Alloys 6% BeNi Beryllium Nickel Master Alloy</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17387"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;619</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17387' href="/search/DataSheet.aspx?MatGUID=f4f8ef7d99d54afc85eb51b2778900ed" >Special Metals Nickel 200</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17388"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;620</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17388' href="/search/DataSheet.aspx?MatGUID=88acc0a0af424892adcea4c0817fcbc1" >Special Metals Nickel 201</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17389"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;621</td><td style="width:auto;"><img src="/images/buttons/iconDiscontinued.jpg" alt="Discontinued" title="Discontinued"  /></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17389' href="/search/DataSheet.aspx?MatGUID=24efe706c16146bebd09c3e8060887d0" >Special Metals Nickel 205</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17390"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;622</td><td style="width:auto;"><img src="/images/buttons/iconDiscontinued.jpg" alt="Discontinued" title="Discontinued"  /></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17390' href="/search/DataSheet.aspx?MatGUID=105336e392234f6395a57c94d3acc022" >Special Metals Nickel 212</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17391"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;623</td><td style="width:auto;"><img src="/images/buttons/iconDiscontinued.jpg" alt="Discontinued" title="Discontinued"  /></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17391' href="/search/DataSheet.aspx?MatGUID=e8fb60f0c50f45a588a7476ee4308fc3" >Special Metals Nickel 222</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17392"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;624</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17392' href="/search/DataSheet.aspx?MatGUID=98906e949d244982a9a8af4998ff58d4" >Special Metals Nickel 270</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17393"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;625</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17393' href="/search/DataSheet.aspx?MatGUID=1364d8231703476b8c466cdd07be71b7" >Special Metals MONEL® Alloy 400</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17394"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;626</td><td style="width:auto;"><img src="/images/buttons/iconDiscontinued.jpg" alt="Discontinued" title="Discontinued"  /></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17394' href="/search/DataSheet.aspx?MatGUID=21a7ed8d1b274695aa21d6b62f2d25b3" >Special Metals MONEL® Alloy 401</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17395"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;627</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17395' href="/search/DataSheet.aspx?MatGUID=c1ceb228f4764f3f99f68f036d49bb1a" >Special Metals MONEL® Alloy R-405</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17396"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;628</td><td style="width:auto;"><img src="/images/buttons/iconDiscontinued.jpg" alt="Discontinued" title="Discontinued"  /></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17396' href="/search/DataSheet.aspx?MatGUID=a2f7dae701be400689c1339112f09948" >Special Metals MONEL® Alloy 450</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17397"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;629</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17397' href="/search/DataSheet.aspx?MatGUID=13ef71e975a046628e1d033bb17b377c" >Special Metals MONEL® Alloy K-500</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17398"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;630</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17398' href="/search/DataSheet.aspx?MatGUID=61e8ffa63dcd4215af41b307246c9038" >Special Metals FERRY® Alloy</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17399"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;631</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17399' href="/search/DataSheet.aspx?MatGUID=029d44b293ee41a1926d8de74e6369bc" >Special Metals INCONEL® Alloy 600</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17400"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;632</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17400' href="/search/DataSheet.aspx?MatGUID=f3fb3ae6ebe54d98ad8fa01c74b6a3e8" >Special Metals INCONEL® Alloy 601</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_150488"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;633</td><td style="width:auto;"><img src="/images/buttons/iconDiscontinued.jpg" alt="Discontinued" title="Discontinued"  /></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_150488' href="/search/DataSheet.aspx?MatGUID=1ffd225fa22742b3bde45d778c4568e8" >Special Metals Inconel® 604 Nickel Alloy Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17401"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;634</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17401' href="/search/DataSheet.aspx?MatGUID=adf2123d8e494e75aef7417989ffea92" >Special Metals INCONEL® Alloy 617</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17402"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;635</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17402' href="/search/DataSheet.aspx?MatGUID=4a194f59f35a427dbc5009f043349cb5" >Special Metals INCONEL® Alloy 625</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17403"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;636</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17403' href="/search/DataSheet.aspx?MatGUID=94950a2d209040a09b89952d45086134" >Special Metals INCONEL® Alloy 718</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17404"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;637</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17404' href="/search/DataSheet.aspx?MatGUID=9612aa3272134531b8b33eb80e61a1af" >Special Metals INCONEL® Alloy X-750</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17405"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;638</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17405' href="/search/DataSheet.aspx?MatGUID=41b31ffaab9f4c89b6969cc65f99d2d3" >Special Metals INCONEL® Alloy 751</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17406"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;639</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17406' href="/search/DataSheet.aspx?MatGUID=54c68277da7146aca1cc80ca15f51c2c" >Special Metals INCONEL® Alloy MA 754</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17407"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;640</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17407' href="/search/DataSheet.aspx?MatGUID=491cc2bcff3540d9bf826797ce221367" >Special Metals INCONEL® Alloy G-3</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17408"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;641</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17408' href="/search/DataSheet.aspx?MatGUID=24441921ff1d43cbbc473ecc672d1b6b" >Special Metals INCONEL® Alloy C-276</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17409"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;642</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17409' href="/search/DataSheet.aspx?MatGUID=8bc5a8a0737a4f2aacf40efc3cb5a0c5" >Special Metals INCONEL® Alloy HX</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17410"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;643</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17410' href="/search/DataSheet.aspx?MatGUID=080d047f5a784bee87e397a3d2b42666" >Special Metals NIMONIC® Alloy 75</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17411"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;644</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17411' href="/search/DataSheet.aspx?MatGUID=7ffc704ef25e4a6f8c2c262c7ff727fd" >Special Metals NIMONIC® Alloy 80A</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17412"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;645</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17412' href="/search/DataSheet.aspx?MatGUID=56bfb1c4cbb64b94ac151ac87a6c4930" >Special Metals NIMONIC® Alloy 81</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17413"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;646</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17413' href="/search/DataSheet.aspx?MatGUID=d595f149290b4713905a35de672f7bf2" >Special Metals NIMONIC® Alloy 86</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17414"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;647</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17414' href="/search/DataSheet.aspx?MatGUID=07348f8a4bea4b01afb6c065ec731e31" >Special Metals NIMONIC® Alloy 90</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_150722"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;648</td><td style="width:auto;"><img src="/images/buttons/iconDiscontinued.jpg" alt="Discontinued" title="Discontinued"  /></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_150722' href="/search/DataSheet.aspx?MatGUID=4b12c36b2af846ffa0775083a9c3e59b" >Special Metals Nimonic® 95 Nickel Alloy Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_150723"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;649</td><td style="width:auto;"><img src="/images/buttons/iconDiscontinued.jpg" alt="Discontinued" title="Discontinued"  /></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_150723' href="/search/DataSheet.aspx?MatGUID=400a36e4a0d44d08baa3ba9ee13616ac" >Special Metals Nimonic® 100 Nickel Alloy Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17415"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;650</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17415' href="/search/DataSheet.aspx?MatGUID=d28cc5c50b924eb18e18cf7599910613" >Special Metals NIMONIC® Alloy 105</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17416"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;651</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17416' href="/search/DataSheet.aspx?MatGUID=0c37688f1b9a472f869823666bd84315" >Special Metals NIMONIC® Alloy 115</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17417"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;652</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17417' href="/search/DataSheet.aspx?MatGUID=fbba09b206da47ef8bc4b83258916409" >Special Metals NIMONIC® Alloy 263</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17418"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;653</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17418' href="/search/DataSheet.aspx?MatGUID=d3781c33baf6454581ac8616175c221c" >Special Metals NIMONIC® Alloy 901</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17419"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;654</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17419' href="/search/DataSheet.aspx?MatGUID=ce4625ab166c480aa633422927ed9b79" >Special Metals NIMONIC® Alloy PE11</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17420"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;655</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17420' href="/search/DataSheet.aspx?MatGUID=c887b0a511424ad19c44c0bd0f50124c" >Special Metals NIMONIC® Alloy PE16</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17421"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;656</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17421' href="/search/DataSheet.aspx?MatGUID=f7dd2c685c0348b2af9a1e86cf1840de" >Special Metals NIMONIC® Alloy PK33</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17422"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;657</td><td style="width:auto;"><img src="/images/buttons/iconDiscontinued.jpg" alt="Discontinued" title="Discontinued"  /></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17422' href="/search/DataSheet.aspx?MatGUID=343dd008d64042bc8f2711dc02bb21d2" >Special Metals NIMONIC® Alloy AP1</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17425"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;658</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17425' href="/search/DataSheet.aspx?MatGUID=cdf4449d2dc4486a94e841be8da2ec29" >Special Metals INCOLOY® Alloy 825</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17430"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;659</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17430' href="/search/DataSheet.aspx?MatGUID=a86eb4a4410944aaa7ae4f567d59b0a1" >Special Metals INCOLOY® Alloy 925</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17435"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;660</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17435' href="/search/DataSheet.aspx?MatGUID=26bedba42b0641aa80fa55c965130460" >Special Metals INCONEL® C276 Nickel Superalloy Tubing, Annealed</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17436"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;661</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17436' href="/search/DataSheet.aspx?MatGUID=e2657cbd4472415e827efc80d3e3a029" >Special Metals INCONEL® C276 Nickel Superalloy Tubing, 24% Cold Work</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17437"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;662</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17437' href="/search/DataSheet.aspx?MatGUID=e86671a0b0cb44e6b842694369845989" >Special Metals INCONEL® C276 Nickel Superalloy Tubing, 27% Cold Work</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17438"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;663</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17438' href="/search/DataSheet.aspx?MatGUID=90607b9e930b492b8d663724b690efc1" >Special Metals INCONEL® C276 Nickel Superalloy Tubing, 30% Cold Work</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17439"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;664</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17439' href="/search/DataSheet.aspx?MatGUID=f449a3ffa0134954b3fca96c952494b2" >Special Metals INCONEL® C276 Nickel Superalloy Plate</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17440"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;665</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17440' href="/search/DataSheet.aspx?MatGUID=c2bea868ff8b499c9461a182ac860ea8" >Special Metals INCONEL® C276 Nickel Superalloy Bar</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17441"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;666</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17441' href="/search/DataSheet.aspx?MatGUID=d7cb6e47376f47ebb15234af42563f1d" >Special Metals INCONEL® C276 Nickel Superalloy Sheet</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17442"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;667</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17442' href="/search/DataSheet.aspx?MatGUID=b8b1f77653c54f4f9038f4b4ecb14e1e" >Special Metals INCONEL® G-3 Nickel Superalloy (UNS N06985)</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17443"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;668</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17443' href="/search/DataSheet.aspx?MatGUID=1fa55846a31c42119e5abd12fde717b8" >Special Metals INCONEL® HX Nickel Superalloy (UNS N06002)</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17444"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;669</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17444' href="/search/DataSheet.aspx?MatGUID=84d2284cc7de4ed788f3e1e04a4df017" >Special Metals INCONEL® 601GC® Nickel Superalloy</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17445"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;670</td><td style="width:auto;"><img src="/images/buttons/iconDiscontinued.jpg" alt="Discontinued" title="Discontinued"  /></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17445' href="/search/DataSheet.aspx?MatGUID=bda98c70332843a8930cd0e66971a47e" >Special Metals INCONEL® 622 Nickel Superalloy (UNS N06022) Plate 0.500 in (12.7 mm)</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17446"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;671</td><td style="width:auto;"><img src="/images/buttons/iconDiscontinued.jpg" alt="Discontinued" title="Discontinued"  /></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17446' href="/search/DataSheet.aspx?MatGUID=87b9d4d5ef9a42089e9019eaefc60ddd" >Special Metals INCONEL® 622 Nickel Superalloy (UNS N06022) Plate 0.250 in (6.4 mm)</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17447"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;672</td><td style="width:auto;"><img src="/images/buttons/iconDiscontinued.jpg" alt="Discontinued" title="Discontinued"  /></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17447' href="/search/DataSheet.aspx?MatGUID=b63d82fd9f8f4a10a6f1d6e8cf84adf2" >Special Metals INCONEL® 622 Nickel Superalloy (UNS N06022) Sheet 0.060 in (1.5 mm)</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17448"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;673</td><td style="width:auto;"><img src="/images/buttons/iconDiscontinued.jpg" alt="Discontinued" title="Discontinued"  /></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17448' href="/search/DataSheet.aspx?MatGUID=5e80f067bdce4af989adeb6179fe9a45" >Special Metals INCONEL® 622 Nickel Superalloy (UNS N06022) Round 1.500 in (38.1 mm)</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17449"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;674</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17449' href="/search/DataSheet.aspx?MatGUID=f4c5f45b2d2242f19cffe04a57e2dd2f" >Special Metals INCONEL® 625LCF® Nickel Superalloy (UNS N06626) Annealed 2150°F (1180°C)</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17450"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;675</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17450' href="/search/DataSheet.aspx?MatGUID=182edb9a5c3d4c659fb64d85a10532df" >Special Metals INCONEL® 625LCF® Nickel Superalloy (UNS N06626) Annealed 2150°F (1180°C) + Aged 1200°F (650°C)</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17451"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;676</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17451' href="/search/DataSheet.aspx?MatGUID=234d1cb43ff24736bc42e35ff1f1a633" >Special Metals INCONEL® 625LCF® Nickel Superalloy (UNS N06626) Annealed 2150°F (1180°C) + Aged 1400°F (760°C)</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17452"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;677</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17452' href="/search/DataSheet.aspx?MatGUID=26c4bbd557554e5f904826dcbd850b75" >Special Metals INCONEL® 625LCF® Nickel Superalloy (UNS N06626) Annealed 1950°F (1070°C)</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17453"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;678</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17453' href="/search/DataSheet.aspx?MatGUID=f8a82c0f36d4452ca332def79cdf3914" >Special Metals INCONEL® 625LCF® Nickel Superalloy (UNS N06626) Annealed 1950°F (1070°C) + Aged 1200°F (650°C)</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17454"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;679</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17454' href="/search/DataSheet.aspx?MatGUID=520b1f8d4b564793b6c587dc206f30de" >Special Metals INCONEL® 625LCF® Nickel Superalloy (UNS N06626) Annealed 1950°F (1070°C) + Aged 1400°F (760°C)</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17455"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;680</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17455' href="/search/DataSheet.aspx?MatGUID=cefba4c5484c46eb86018a78dd3599ef" >Special Metals INCONEL® 625LCF® Nickel Superalloy (UNS N06626) Annealed + 5% Cold Work</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17456"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;681</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17456' href="/search/DataSheet.aspx?MatGUID=e76f115155d34f10948662fe3219fdfd" >Special Metals INCONEL® 625LCF® Nickel Superalloy (UNS N06626) Annealed + 10% Cold Work</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17457"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;682</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17457' href="/search/DataSheet.aspx?MatGUID=fceefc2bde064cf3abcd5f5dd889caea" >Special Metals INCONEL® 625LCF® Nickel Superalloy (UNS N06626) Annealed + 15% Cold Work</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17458"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;683</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17458' href="/search/DataSheet.aspx?MatGUID=061b2e7289374faabecb42ebe7b5a1df" >Special Metals INCONEL® 625LCF® Nickel Superalloy (UNS N06626) Annealed + 20% Cold Work</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17459"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;684</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17459' href="/search/DataSheet.aspx?MatGUID=ffa7635d8c3d46238b3a4a7799d07f99" >Special Metals INCONEL® 625LCF® Nickel Superalloy (UNS N06626) Annealed + 30% Cold Work</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17460"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;685</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17460' href="/search/DataSheet.aspx?MatGUID=a8936d87b82e4069a91647063e02a621" >Special Metals INCONEL® 625LCF® Nickel Superalloy (UNS N06626) Annealed + 40% Cold Work</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17461"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;686</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17461' href="/search/DataSheet.aspx?MatGUID=3f7998a11d6548d09aab97a5ab05297f" >Special Metals INCONEL® 625LCF® Nickel Superalloy (UNS N06626) Annealed + 50% Cold Work</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17462"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;687</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17462' href="/search/DataSheet.aspx?MatGUID=9cc23d709b224e1b936b6e1cb60d1a4f" >Special Metals INCONEL® Alloy 686 Nickel Superalloy Plate (UNS N06686)</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17463"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;688</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17463' href="/search/DataSheet.aspx?MatGUID=66adac285a9d4c4f81b15e02da4f2235" >Special Metals INCONEL® Alloy 686 Nickel Superalloy Sheet (UNS N06686)</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17464"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;689</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17464' href="/search/DataSheet.aspx?MatGUID=c809479b25bd452eadf442455ba17146" >Special Metals INCONEL® Alloy 686 Nickel Superalloy Rod (UNS N06686)</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17465"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;690</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17465' href="/search/DataSheet.aspx?MatGUID=a62ecc8ffc774a39a8f580a68aa3c286" >Special Metals INCONEL® Alloy 686 Nickel Superalloy 10% Cold Worked (UNS N06686)</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17466"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;691</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17466' href="/search/DataSheet.aspx?MatGUID=194399b288894a04a2dbc69809f13136" >Special Metals INCONEL® Alloy 686 Nickel Superalloy 20% Cold Worked (UNS N06686)</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_191596"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;692</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_191596' href="/search/DataSheet.aspx?MatGUID=e60828c915174e7299571c10f27669d0" >Special Metals INCONEL® Alloy 686 Nickel Superalloy 30% Cold Worked (UNS N06686)</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_191597"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;693</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_191597' href="/search/DataSheet.aspx?MatGUID=9240a5e34c3e4b02a49a6f0f7a1b7c34" >Special Metals INCONEL® Alloy 686 Nickel Superalloy 40% Cold Worked (UNS N06686)</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_191598"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;694</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_191598' href="/search/DataSheet.aspx?MatGUID=abb0920038d94010be67f07419ac163a" >Special Metals INCONEL® Alloy 686 Nickel Superalloy 50% Cold Worked (UNS N06686)</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17467"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;695</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17467' href="/search/DataSheet.aspx?MatGUID=3542755925f74f549bec41d8b3162d0c" >Special Metals INCONEL® 690 Nickel Superalloy, Cold Drawn Tube, 0.50 inch (12.7 mm) o.d. and 0.050 inch (1.27 mm) Thickness</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17468"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;696</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17468' href="/search/DataSheet.aspx?MatGUID=3942563a71e24b8ab39ac1d493e01506" >Special Metals INCONEL® 690 Nickel Superalloy, Cold Drawn Tube, 0.75 inch (19.0 mm) o.d. and 0.065 inch (1.65 mm) Thickness</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17469"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;697</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17469' href="/search/DataSheet.aspx?MatGUID=c3730ef57e3545ab9cebe3ec3e3395c4" >Special Metals INCONEL® 690 Nickel Superalloy, Cold Drawn Tube, 3.50 inch (88.9 mm) o.d. and 0.219 inch (5.49 mm) Thickness</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17470"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;698</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17470' href="/search/DataSheet.aspx?MatGUID=188ac191ce26481387c1d3c168902064" >Special Metals INCONEL® 690 Nickel Superalloy, Hot Rolled Flat, 0.5 x 2.0 inch (13 x 51 mm)</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17471"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;699</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17471' href="/search/DataSheet.aspx?MatGUID=f505d1ec95254bda89e342500ce4e29b" >Special Metals INCONEL® 690 Nickel Superalloy, Hot Rolled Flat, 2.0 inch (51 mm) Diameter</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17472"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;700</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17472' href="/search/DataSheet.aspx?MatGUID=4221387e9b1245999e6cfada1c7444e9" >Special Metals INCONEL® 690 Nickel Superalloy, Hot Rolled Flat, 0.62 inch (16 mm) Diameter</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17473"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;701</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17473' href="/search/DataSheet.aspx?MatGUID=45a05b27c2b44e42b7e3560dfaf7c875" >Special Metals INCONEL® 690 Nickel Superalloy, Cold Rolled Strip, 0.150 inch (3.81 mm) Thick</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17474"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;702</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17474' href="/search/DataSheet.aspx?MatGUID=33295430c5c3404a8a2e075554d4529c" >Special Metals INCONEL® 706 Precipitation Hardening Alloy, Cold Rolled Sheet, Solution Treated</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17475"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;703</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17475' href="/search/DataSheet.aspx?MatGUID=021cc944a09146f78a44c16844791ec5" >Special Metals INCONEL® 706 Precipitation Hardening Alloy, Forged Disc, Solution Treated</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17476"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;704</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17476' href="/search/DataSheet.aspx?MatGUID=306cfb3e227946b9b90403055e15c6a4" >Special Metals INCONEL® 706 Precipitation Hardening Alloy, Cold Rolled Sheet, 3 Part Heat Treatment, 0.040 inch (1.02 mm) Thickness</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17477"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;705</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17477' href="/search/DataSheet.aspx?MatGUID=bd147e4c60f04b8ba41a0d3444b1f2e0" >Special Metals INCONEL® 706 Precipitation Hardening Alloy, Cold Rolled Sheet, 2 Part Heat Treatment, 0.040 inch (1.02 mm) Thickness</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17478"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;706</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17478' href="/search/DataSheet.aspx?MatGUID=18493cadc9e34dcd8bd4589c30fa8dd0" >Special Metals INCONEL® 706 Precipitation Hardening Alloy, Cold Rolled Sheet, 3 Part Heat Treatment, 0.062 inch (1.57 mm) Thickness</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17479"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;707</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17479' href="/search/DataSheet.aspx?MatGUID=cc03f6d222da4f728b5afc073c533ea7" >Special Metals INCONEL® 706 Precipitation Hardening Alloy, Cold Rolled Sheet, 2 Part Heat Treatment, 0.062 inch (1.57 mm) Thickness</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17480"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;708</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17480' href="/search/DataSheet.aspx?MatGUID=be822c73ad0b4cbb811985557a682651" >Special Metals INCONEL® 706 Precipitation Hardening Alloy, Hot Finished Rod, 3 Part Heat Treatment, 0.562 inch (14.3 mm) Diameter</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17481"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;709</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17481' href="/search/DataSheet.aspx?MatGUID=ec986b350d4d43cda9f309fb72761663" >Special Metals INCONEL® 706 Precipitation Hardening Alloy, Hot Finished Rod, 3 Part Heat Treatment, 8.0 inch (203 mm) Diameter</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17482"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;710</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17482' href="/search/DataSheet.aspx?MatGUID=287e0f23595a4e09805fb13b2699027f" >Special Metals INCONEL® 706 Precipitation Hardening Alloy, Hot Finished Flat, 2 Part Heat Treatment, 1.250 inch (31.8 mm) Thickness</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17483"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;711</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17483' href="/search/DataSheet.aspx?MatGUID=958dbfab4a7f48a98ab03e1aaefebdba" >Special Metals INCONEL® 706 Precipitation Hardening Alloy, Hot Finished Rod, 2 Part Heat Treatment, 0.562 inch (14.3 mm) Diameter</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17484"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;712</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17484' href="/search/DataSheet.aspx?MatGUID=c3025a03f28d471ca3589db6097eaa7a" >Special Metals INCONEL® 706 Precipitation Hardening Alloy, Forged Disc, 3 Part Heat Treatment</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17485"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;713</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17485' href="/search/DataSheet.aspx?MatGUID=35aa964eb9e443e59eca454c5a11e9da" >Special Metals INCONEL® 706 Precipitation Hardening Alloy, Forged Disc, 2 Part Heat Treatment</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17486"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;714</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17486' href="/search/DataSheet.aspx?MatGUID=b95db0912b2a4b8bb0c39b34d3672c73" >Special Metals INCONEL® 718SPF™ Nickel Superalloy, Annealed</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17487"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;715</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17487' href="/search/DataSheet.aspx?MatGUID=8eabfa5d91954540b3f18c2d34c5dc21" >Special Metals INCONEL® 718SPF™ Nickel Superalloy, Annealed + Aged</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17488"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;716</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17488' href="/search/DataSheet.aspx?MatGUID=d0b7e807e97446d1a4a8dddfa1b20e49" >Special Metals INCONEL® 725 Nickel Superalloy (UNS N07725), Annealed Round</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17489"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;717</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17489' href="/search/DataSheet.aspx?MatGUID=d20a0275b7cc48d79aab53b5e03ed7b2" >Special Metals INCONEL® 725 Nickel Superalloy (UNS N07725), Age Hardened Round</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17490"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;718</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17490' href="/search/DataSheet.aspx?MatGUID=0527521ddb834b109da8a9f26790095b" >Special Metals INCONEL® 725 Nickel Superalloy (UNS N07725), Annealed Tube</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17491"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;719</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17491' href="/search/DataSheet.aspx?MatGUID=4c29eada21a6473fb881f6f0816fcc26" >Special Metals INCONEL® 725 Nickel Superalloy (UNS N07725), Age Hardened Tube</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17492"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;720</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17492' href="/search/DataSheet.aspx?MatGUID=698b8fa169cf4bb3afa812789c6f88e0" >Special Metals INCONEL® 725 Nickel Superalloy (UNS N07725) Age Hardened Strip</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17493"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;721</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17493' href="/search/DataSheet.aspx?MatGUID=988d6f526c764f2984a52921dbf38c08" >Special Metals INCONEL® MA758 Oxide-Dispersion-Strengthened Ni-Cr Superalloy</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17494"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;722</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17494' href="/search/DataSheet.aspx?MatGUID=534050c1f9c44f97aa2f1746bef2fa0e" >Special Metals MONEL® 404 Copper-Nickel Alloy</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17496"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;723</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17496' href="/search/DataSheet.aspx?MatGUID=78c18473d31c4d2e860032c79adf6eab" >Special Metals DURANICKEL® 301 (UNS N03301) Hot-Finished Rod and Bar</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17497"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;724</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17497' href="/search/DataSheet.aspx?MatGUID=bec08455c2eb4b479dfbf55ecf1a9b85" >Special Metals DURANICKEL® 301 (UNS N03301) Hot-Finished and Aged Rod and Bar</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17498"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;725</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17498' href="/search/DataSheet.aspx?MatGUID=833493c463ff4998b4ffe0d58e8dc906" >Special Metals DURANICKEL® 301 (UNS N03301) Cold-Drawn Rod and Bar</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17499"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;726</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17499' href="/search/DataSheet.aspx?MatGUID=2883c32a4862445d821e3339b05cbef8" >Special Metals DURANICKEL® 301 (UNS N03301) Cold-Drawn and Aged Rod and Bar</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17500"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;727</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17500' href="/search/DataSheet.aspx?MatGUID=39a321f2ac364fb28c80f33eec128f74" >Special Metals DURANICKEL® 301 (UNS N03301) Annealed Rod and Bar</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17501"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;728</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17501' href="/search/DataSheet.aspx?MatGUID=3b9bcb47f1104394a24519f96e8e1317" >Special Metals DURANICKEL® 301 (UNS N03301) Annealed and Aged Rod and Bar</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17502"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;729</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17502' href="/search/DataSheet.aspx?MatGUID=e960b7110c5f4ed19c681264d4f48048" >Special Metals DURANICKEL® 301 (UNS N03301) Annealed Strip</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17503"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;730</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17503' href="/search/DataSheet.aspx?MatGUID=36983e1e28174c6ca7e015088b44c928" >Special Metals DURANICKEL® 301 (UNS N03301) Annealed and Aged Strip</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17504"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;731</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17504' href="/search/DataSheet.aspx?MatGUID=fb9849423d6d4b379b76c4c18754b13c" >Special Metals DURANICKEL® 301 (UNS N03301) Half-Hard Strip</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17505"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;732</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17505' href="/search/DataSheet.aspx?MatGUID=e3e7f08ed9804d0c9be0843460334d47" >Special Metals DURANICKEL® 301 (UNS N03301) Half-Hard and Aged Strip</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17506"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;733</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17506' href="/search/DataSheet.aspx?MatGUID=4d729685b6db4da7b98e9101ba14a200" >Special Metals DURANICKEL® 301 (UNS N03301) Spring Strip</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17507"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;734</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17507' href="/search/DataSheet.aspx?MatGUID=0a13bd0dad534bf48f0f21d1dd0a4863" >Special Metals DURANICKEL® 301 (UNS N03301) Spring Strip, Aged</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17508"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;735</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17508' href="/search/DataSheet.aspx?MatGUID=677e8555a2c043bfa0c8892d27874795" >Special Metals DURANICKEL® 301 (UNS N03301) Annealed Wire</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17509"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;736</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17509' href="/search/DataSheet.aspx?MatGUID=872bd106be5d4e5998e407cbcb597999" >Special Metals DURANICKEL® 301 (UNS N03301) Annealed and Aged Wire</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17510"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;737</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17510' href="/search/DataSheet.aspx?MatGUID=db26f06e94b54f79aa40199e8d84a0d3" >Special Metals DURANICKEL® 301 (UNS N03301) Spring Wire</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17511"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;738</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17511' href="/search/DataSheet.aspx?MatGUID=85c9af85f6484621ace9019c945a5a52" >Special Metals DURANICKEL® 301 (UNS N03301) Spring Wire, Aged</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17512"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;739</td><td style="width:auto;"><img src="/images/buttons/iconDiscontinued.jpg" alt="Discontinued" title="Discontinued"  /></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17512' href="/search/DataSheet.aspx?MatGUID=1fbffd20a0fd413f8e5f00158463f7c3" >Special Metals Electroformed Nickel, 6 Micron (0.24 mil) Foil, As-Electroformed</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17513"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;740</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17513' href="/search/DataSheet.aspx?MatGUID=392af4ba1dca4dfdb8e62d76a6cb9bff" >Special Metals Electroformed Nickel Foil, As-Electroformed</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17514"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;741</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17514' href="/search/DataSheet.aspx?MatGUID=2b16dfc8c84e49c886763932937557db" >Special Metals Electroformed Nickel, 13 Micron (0.5 mil) Foil, Electroformed and Annealed</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17515"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;742</td><td style="width:auto;"><img src="/images/buttons/iconDiscontinued.jpg" alt="Discontinued" title="Discontinued"  /></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17515' href="/search/DataSheet.aspx?MatGUID=6d38758d87f24673855417e587c3ff3f" >Special Metals Electroformed Nickel, 200 Micron (8 mil) Foil, Electroformed and Annealed</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17516"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;743</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17516' href="/search/DataSheet.aspx?MatGUID=86c21db95eb94f41ad724561c9da55ca" >California Fine Wire Permanickel® K-300 Nickel Alloy</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17519"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;744</td><td style="width:auto;"><img src="/images/buttons/iconDiscontinued.jpg" alt="Discontinued" title="Discontinued"  /></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17519' href="/search/DataSheet.aspx?MatGUID=b6fa6c2d047f461994715814caa72c57" >Special Metals BRIGHTRAY® Alloy B Electrical Resistance Alloy</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17520"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;745</td><td style="width:auto;"><img src="/images/buttons/iconDiscontinued.jpg" alt="Discontinued" title="Discontinued"  /></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17520' href="/search/DataSheet.aspx?MatGUID=3ad59dd711b146febcf0644b4563ff9a" >Special Metals BRIGHTRAY® Alloy C Electrical Resistance Alloy</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17522"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;746</td><td style="width:auto;"><img src="/images/buttons/iconDiscontinued.jpg" alt="Discontinued" title="Discontinued"  /></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17522' href="/search/DataSheet.aspx?MatGUID=61a207f6e51c4025878a36934788b51d" >Special Metals BRIGHTRAY® Alloy S Electrical Resistance Alloy</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17526"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;747</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17526' href="/search/DataSheet.aspx?MatGUID=d8f2595c011f453eb61023ac1bb8dac6" >Special Metals NILO® Alloy 48 Controlled-Expansion Alloy</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17527"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;748</td><td style="width:auto;"><img src="/images/buttons/iconDiscontinued.jpg" alt="Discontinued" title="Discontinued"  /></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17527' href="/search/DataSheet.aspx?MatGUID=99f438c2c36f4ec5bb5da75a7d2df7bb" >Special Metals NILO® Alloy 475 Controlled-Expansion Alloy</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17529"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;749</td><td style="width:auto;"><img src="/images/buttons/iconDiscontinued.jpg" alt="Discontinued" title="Discontinued"  /></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17529' href="/search/DataSheet.aspx?MatGUID=fd44e54e0904421f806cc85dde46e4f6" >Special Metals NILOMAG® Alloy 77</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17531"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;750</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17531' href="/search/DataSheet.aspx?MatGUID=1e9687d919244e51a7d0a7fc0459b356" >Special Metals Nitinol Superelastic Ni-Ti alloy</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17532"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;751</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17532' href="/search/DataSheet.aspx?MatGUID=7bf10705e0494dfbbc07626e82729b04" >Special Metals Nitinol High-Strength Superelastic Ni-Ti alloy</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17533"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;752</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17533' href="/search/DataSheet.aspx?MatGUID=ba21dac525d44344a68aa93cda906b79" >Special Metals Nitinol High-Temperature Shape Memory Ni-Ti alloy Ribbon</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17534"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;753</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17534' href="/search/DataSheet.aspx?MatGUID=d79a3e1ffa6f4cfa8c3083b7dbdb4f89" >Special Metals Nitinol High-Temperature Shape Memory Ni-Ti alloy</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17535"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;754</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17535' href="/search/DataSheet.aspx?MatGUID=d307e63b36044fec9f039fe95ee0343b" >Special Metals Nitinol Body-Temperature Ni-Ti alloy</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17536"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;755</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17536' href="/search/DataSheet.aspx?MatGUID=9bcbd98b380e40bdaf3b806611a5f276" >Special Metals Nitinol Chrome-Doped Superelastic Ni-Ti alloy</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17537"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;756</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17537' href="/search/DataSheet.aspx?MatGUID=a7c1135b6c454b67b52904d623d33abe" >Special Metals Nitinol High-Strength Superelastic Ni-Ti-Fe alloy</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_98892"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;757</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_98892' href="/search/DataSheet.aspx?MatGUID=73a7c99193164a2b8594d12788e171b3" >Special Metals INCONEL® alloy 693</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_98893"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;758</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_98893' href="/search/DataSheet.aspx?MatGUID=07bf67488fce4a27814ddf74d156299d" >Special Metals INCONEL® alloy 740 Precipitation-Hardenable Ni-Cr-Co Superalloy</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_98894"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;759</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_98894' href="/search/DataSheet.aspx?MatGUID=5496620754534585907fc0745784a78e" >Special Metals INCONEL® alloy MA758 Ni-Cr Superalloy</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_98895"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;760</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_98895' href="/search/DataSheet.aspx?MatGUID=93b4807016ff4758a5993eb3cfd333ec" >Special Metals INCONEL® alloy 783 Ni-Co-Fe Alloy</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_98896"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;761</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_98896' href="/search/DataSheet.aspx?MatGUID=adf57900a3984be09308293f4e3a6c06" >Special Metals INCONEL® Alloy N06230 Ni-Cr-W-Mo Alloy</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_98897"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;762</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_98897' href="/search/DataSheet.aspx?MatGUID=831b36af1ca84e37ab8c1391ec605262" >Special Metals INCONEL® alloy 22 Austenitic Alloy</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_98898"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;763</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_98898' href="/search/DataSheet.aspx?MatGUID=89f560b5e53d41daa751322e4e24219e" >Special Metals INCOLOY® alloy 803 Fe-Ni-Cr Alloy</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_98900"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;764</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_98900' href="/search/DataSheet.aspx?MatGUID=7aa71b2dab1d4b3ca3dfd03d5d6d5d9a" >Special Metals INCOLOY® alloy 864</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_98901"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;765</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_98901' href="/search/DataSheet.aspx?MatGUID=2b7887fb69ca40deb0134671236ac3ea" >Special Metals INCOLOY® alloy 890 Ni-Cr-Fe Superalloy</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_191599"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;766</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_191599' href="/search/DataSheet.aspx?MatGUID=bc9e110388c84e9fb65392692a4958ce" >Special Metals INCOLOY® Alloy 945 Nickel-Iron-Chromium Alloy, Annealed and Aged Rod (UNS N09945)</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_191600"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;767</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_191600' href="/search/DataSheet.aspx?MatGUID=db0cbe91fa424db590912e0bcd1ab2e3" >Special Metals INCOLOY® Alloy 945 Nickel-Iron-Chromium Alloy, Pump Shaft Grade, Cold Worked and Aged Rod (UNS N09945)</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_191601"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;768</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_191601' href="/search/DataSheet.aspx?MatGUID=f32e797201bc4e5bb9dc122cbf0868da" >Special Metals INCOLOY® Alloy 945 Nickel-Iron-Chromium Alloy, Cold Drawn, Annealed and Aged Tubing (UNS N09945)</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_191602"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;769</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_191602' href="/search/DataSheet.aspx?MatGUID=322fc9da673e466e993cc2c4448394b5" >Special Metals INCOLOY® Alloy 945X Nickel-Iron-Chromium Alloy, Annealed and Aged Rod (UNS N09945)</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_191603"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;770</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_191603' href="/search/DataSheet.aspx?MatGUID=cb2a6330dec244f6aebacfca9278a74c" >Special Metals INCOLOY® Alloy 945X Nickel-Iron-Chromium Alloy, Pump Shaft Grade, Cold Worked and Aged Rod (UNS N09945)</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_98906"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;771</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_98906' href="/search/DataSheet.aspx?MatGUID=e6fab274acc6452898ce9cb9351e1adf" >Special Metals NIMONIC® alloy 91 Ni-Cr-Co Alloy</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_150727"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;772</td><td style="width:auto;"><img src="/images/buttons/iconDiscontinued.jpg" alt="Discontinued" title="Discontinued"  /></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_150727' href="/search/DataSheet.aspx?MatGUID=b27569f8b73e43539809cb1066e270fb" >Special Metals Udimet® 500 Nickel Alloy (UNS N07500)</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_98909"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;773</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_98909' href="/search/DataSheet.aspx?MatGUID=349c742356c44f82a4fccb2b84679ad6" >Special Metals UDIMET® alloy 520 Superalloy</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_150728"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;774</td><td style="width:auto;"><img src="/images/buttons/iconDiscontinued.jpg" alt="Discontinued" title="Discontinued"  /></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_150728' href="/search/DataSheet.aspx?MatGUID=a8350417a229451f8f9b7f3d2af37ad5" >Special Metals Udimet® 630 Nickel Alloy Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_150729"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;775</td><td style="width:auto;"><img src="/images/buttons/iconDiscontinued.jpg" alt="Discontinued" title="Discontinued"  /></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_150729' href="/search/DataSheet.aspx?MatGUID=42b0db30b54e4e46aed918c32cab0b26" >Special Metals Udimet® 700 Nickel Alloy</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_150730"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;776</td><td style="width:auto;"><img src="/images/buttons/iconDiscontinued.jpg" alt="Discontinued" title="Discontinued"  /></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_150730' href="/search/DataSheet.aspx?MatGUID=0b198631f0c541b4b1dc2d60f14859a7" >Special Metals Udimet® 710 Nickel Alloy</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_98910"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;777</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_98910' href="/search/DataSheet.aspx?MatGUID=205ba4b2490a481c95908800f21b7bc8" >Special Metals UDIMET® alloy 720 Nickel Base Alloy</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_98911"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;778</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_98911' href="/search/DataSheet.aspx?MatGUID=32c176363f754584a1d82b869d21317b" >Special Metals UDIMET® alloy D-979 Fe-Ni Alloy</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_98912"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;779</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_98912' href="/search/DataSheet.aspx?MatGUID=76a6a1a41cfd4059aa4f68d754eedacd" >Special Metals UDIMET® alloy R41 Ni-Cr Alloy</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_191482"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;780</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_191482' href="/search/DataSheet.aspx?MatGUID=7fd05647d3af48fc8735018afe22d0d3" >Special Metals INCOTHERM® TD Nickel Alloy</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_239853"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;781</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_239853' href="/search/DataSheet.aspx?MatGUID=d84df950229543af9bccc7b4f7288e21" >Special Metals Waspaloy Age Hardenable Nickel Alloy (UNS N07001)</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_186225"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;782</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_186225' href="/search/DataSheet.aspx?MatGUID=7e0054e8204349148736201aef4f5e09" >Johnson Matthey Pallabraze™ 960 Precious Metal Brazing Filler Metal</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_186226"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;783</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_186226' href="/search/DataSheet.aspx?MatGUID=ebe44ddf48d141faadc15e256d5623e6" >Johnson Matthey Pallabraze™ 977 Precious Metal Brazing Filler Metal</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_186278"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;784</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_186278' href="/search/DataSheet.aspx?MatGUID=e6dbd5ca886c4b608d27b5927b35aadf" >Johnson Matthey Nickelbraze™ HTN1 Nickel Based Brazing Filler Metal</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_186279"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;785</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_186279' href="/search/DataSheet.aspx?MatGUID=aeeda5ef385e41028a8ffd9bd9d11c5b" >Johnson Matthey Nickelbraze™ HTN1A Nickel Based Brazing Filler Metal</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_186280"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;786</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_186280' href="/search/DataSheet.aspx?MatGUID=f1d48ad458b84eccad2e703c78183d8a" >Johnson Matthey Nickelbraze™ HTN2 Nickel Based Brazing Filler Metal</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_186281"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;787</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_186281' href="/search/DataSheet.aspx?MatGUID=a7e9745fa41c4ff1be1435a94b063f6e" >Johnson Matthey Nickelbraze™ HTN3 Nickel Based Brazing Filler Metal</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_186282"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;788</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_186282' href="/search/DataSheet.aspx?MatGUID=3b79ffeb0add4222bf7146777753613e" >Johnson Matthey Nickelbraze™ HTN4 Nickel Based Brazing Filler Metal</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_186283"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;789</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_186283' href="/search/DataSheet.aspx?MatGUID=b8f8ef46b5d8488b99b92b17af51f448" >Johnson Matthey Nickelbraze™ HTN5 Nickel Based Brazing Filler Metal</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_186284"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;790</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_186284' href="/search/DataSheet.aspx?MatGUID=2587f5dd1b114da78b514b0af08bfab7" >Johnson Matthey Nickelbraze™ HTN6 Nickel Based Brazing Filler Metal</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_186285"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;791</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_186285' href="/search/DataSheet.aspx?MatGUID=0d002e7e0c7a43418ccbc069b4399893" >Johnson Matthey Nickelbraze™ HTN7 Nickel Based Brazing Filler Metal</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17730"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;792</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17730' href="/search/DataSheet.aspx?MatGUID=a3d50f11ce1344f3a0d56e7e9e042f13" >Kennametal Conforma Clad WC 200 Tungsten Carbide-Nickel Cladding</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17731"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;793</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17731' href="/search/DataSheet.aspx?MatGUID=8cd5b93a7df94c65b630870fe005e3e6" >Kennametal Conforma Clad WC 210 Tungsten Carbide-Nickel Cladding</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17732"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;794</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17732' href="/search/DataSheet.aspx?MatGUID=a2a749f9f9c34e5c8d23a403cbe7266e" >Kennametal Conforma Clad WC 219 Tungsten Carbide-Nickel Cladding</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_138097"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;795</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_138097' href="/search/DataSheet.aspx?MatGUID=5016af0c47ca4ae48f57470f2e5f18ac" >Kinetics MIM Ni as Sintered</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17795"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;796</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17795' href="/search/DataSheet.aspx?MatGUID=587291af6958465a9d9bbb6719ba56a1" >ATI Allegheny Ludlum AL 625HP™ Stainless Steel</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17796"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;797</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17796' href="/search/DataSheet.aspx?MatGUID=ae5e37b8b92647dfb22d8ff0feef0297" >ATI Allegheny Ludlum Allcorr® Nickel-Based, Corrosion Resistant Alloy</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17797"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;798</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17797' href="/search/DataSheet.aspx?MatGUID=3109b6e91cfb4e90b07612098c5ee00e" >ATI Allegheny Ludlum Altemp® 263 Nickel-Based alloy</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17798"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;799</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17798' href="/search/DataSheet.aspx?MatGUID=fd8436b6d2554abbb087734422137420" >ATI Allegheny Ludlum Altemp® HX Nickel-Based superalloy</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17799"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;800</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17799' href="/search/DataSheet.aspx?MatGUID=4d3f77e2c3d2458eb0940ade2d85f26f" >ATI Allegheny Ludlum Altemp® X-750 Nickel-Based superalloy</a></td></tr>

		
	</table>

	<table style="background-color:Silver;" class="tabledataformat" >

		<tr class="">
			<th>&nbsp;</th>
		</tr>

		<tr>
			<td><strong>Found <span id="ctl00_ContentMain_UcSearchResults1_lblResultCount2">1245</span> Results</strong> -- 
				
				Page 
				<select name="ctl00$ContentMain$UcSearchResults1$drpPageSelect2" onchange="javascript:setTimeout('__doPostBack(\'ctl00$ContentMain$UcSearchResults1$drpPageSelect2\',\'\')', 0)" id="ctl00_ContentMain_UcSearchResults1_drpPageSelect2">
		<option value="1">1</option>
		<option value="2">2</option>
		<option value="3">3</option>
		<option selected="selected" value="4">4</option>
		<option value="5">5</option>

	</select>
				of <span id="ctl00_ContentMain_UcSearchResults1_lblPageTotal2">7</span> 
				
				-- 
				<a id="ctl00_ContentMain_UcSearchResults1_lnkPrevPage2" href="javascript:__doPostBack('ctl00$ContentMain$UcSearchResults1$lnkPrevPage2','')" style="color:Black;">[Prev Page]</a>
				<a id="ctl00_ContentMain_UcSearchResults1_lnkNextPage2" href="javascript:__doPostBack('ctl00$ContentMain$UcSearchResults1$lnkNextPage2','')" style="color:Black;">[Next Page]</a>
				--&nbsp;

				view 
				<select name="ctl00$ContentMain$UcSearchResults1$drpPageSize2" onchange="javascript:setTimeout('__doPostBack(\'ctl00$ContentMain$UcSearchResults1$drpPageSize2\',\'\')', 0)" id="ctl00_ContentMain_UcSearchResults1_drpPageSize2">
		<option value="25">25</option>
		<option value="50">50</option>
		<option value="75">75</option>
		<option value="100">100</option>
		<option value="150">150</option>
		<option selected="selected" value="200">200</option>

	</select> per page
			</td>
		</tr>
	</table>
	
	<span id="ctl00_ContentMain_UcSearchResults1_divDiscontinued" style="font-size:11px;color:red">
	<span id="ctl00_ContentMain_UcSearchResults1_lblDisclaimer"><br />
Materials flagged as discontinued (<img src="/images/buttons/iconDiscontinued.jpg" alt="" />) are no longer part of the manufacturer’s standard product line according to our latest information.  These materials may be available by special order, in distribution inventory, or reinstated as an active product.  Data sheets from materials that are no longer available remain in MatWeb to assist users in finding replacement materials.  
<br /><br />
Users of our Advanced Search (registration required) may exclude discontinued materials from search results.</span>
	</span> 


</div>





<script type="text/javascript">

	//GLOBAL VARIABLES
	var gCurrentCheckbox;
	var gNewFolderID;
	var gOldFolderName;
	var gSelectedFolderID = 0;
	var gMaxFolders = 0;

	//GLOBAL FIELD REFERENCES
	var drpFolderList = document.getElementById("ctl00_ContentMain_UcSearchResults1_drpFolderList");
	var drpFolderAction = document.getElementById("ctl00_ContentMain_UcSearchResults1_drpFolderAction");
	var txtFolderMatCount = document.getElementById("ctl00_ContentMain_UcSearchResults1_txtFolderMatCount");
	var divUserMessage = document.getElementById("divUserMessage");
	var divFolderName = document.getElementById("divFolderName");
	var tblFolderName = document.getElementById("tblFolderName");
	var txtFolderName = document.getElementById("txtFolderName2");
	
	// See MS ajax.net client side documentation
	// http://ajax.asp.net/docs/ClientReference/Sys.Net/WebrequestManagerClass/	
	Sys.Net.WebRequestManager.add_invokingRequest(ucSearchResults_OnRequestStart);
	Sys.Net.WebRequestManager.add_completedRequest(ucSearchResults_OnRequestEnd);
	
	var timerID = 0;

	function ucSearchResults_OnRequestStart(){
		document.body.style.cursor="progress";
	}
	
	function ucSearchResults_OnRequestEnd(){
		document.body.style.cursor="auto";
	}

	//drpFolderAction pull down does not exist for anonymous users
	if(drpFolderAction) {
		drpFolderAction.onchange=RunFolderAction;
	}

	//RUN ONCE on page load
	//matweb.register_onload(SetSelectedFolder);
	
	SearchMatIDList = []; //an array of MatIDs
	//matweb.alert(MatIDList);
	
	//======================
	//Test Service 
	//======================
	//TestService("This is a test");
	//aci.matweb.WebServices.Folders.RaiseTestError(null,ErrorHandler,"testerror");
	
	function TestService(arg){
		aci.matweb.WebServices.Folders.HelloWorld(arg, TestService_CALLBACK,ErrorHandler,"TestService");
	}
	
	function TestService_CALLBACK(data,context){
		alert(data);
	}
	
	// =================================
	// This function is the onchange event for the material checkbox. See Code Behind.
	// The first time a material is checked, and there are no folders, the folder web service creates a NEW FOLDER
	// =================================
	function ToggleMat(chkBox,MatID,MaxFolderMats){
	
		HideFolderNameControls();

		var FolderID,Context;
		gCurrentCheckbox = chkBox;
		var matLink = $("lnkMatl_" + MatID);
		var MatName = matLink.innerHTML;
		//alert(MatName);

		var SelectedOption = drpFolderList.options[drpFolderList.selectedIndex];  

		if(gNewFolderID > 0)
			FolderID = gNewFolderID;
		else
			FolderID = SelectedOption.value; //this will be 0 if no folders exist yet
		//alert(FolderID);
		
		// call the folder web service 
		// See: ScriptManagerProxy object at the top of this page
		// Also See: /folders/FolderService.asmx & /App_Code/FolderService.cs
		if(chkBox.checked){
			Context = "AddMat";
			aci.matweb.WebServices.Folders.AddMaterial(FolderID, MatID, MatName, ToggleMat_CALLBACK,ErrorHandler,Context);
		}
		else{
			Context = "RemoveMat";
			aci.matweb.WebServices.Folders.RemoveMaterial(FolderID, MatID, ToggleMat_CALLBACK,ErrorHandler,Context);
		}
			
	}
	
	// =================================
	// CALLBACK FUNCTION
	// this function is called by the AJAX framework after the web service is invoked
	// The folder web service returns a ReturnData type object.
	// =================================
	function ToggleMat_CALLBACK(data,context){

		 //alert(data);
		// alert(context);

		var FolderMatCount = data.FolderMatCount
		var NewFolderID = data.NewFolderID
		var DisplayMessage = data.DisplayMessage
		var MaxFolderMats = data.FolderMaxMatls;
		
		txtFolderMatCount.value = FolderMatCount + "/" + MaxFolderMats;

		// If this is the first time the user checked the checkbox, and the user did not already have any folders, 
		// a new folder may have been created.
		// Save the new folderID to a hidden field, so we can reference it later.
		if(NewFolderID > 0){
			gNewFolderID = NewFolderID;
		}
		
		// alert(gNewFolderID);
		// alert(FolderCount);
		// alert(ReloadFolderList);
		// if(NewFolderID>0) alert(NewFolderID);
		// alert(MaxFolderMats);
		
		switch(context){
		case "AddMat":
			if(DisplayMessage){
				gCurrentCheckbox.checked = false;
				matweb.alert(DisplayMessage);
			}
			break    
		case "RemoveMat":
			break    
		default:
			matweb.Alert("Unhandeled Context in ToggleMat_CALLBACK() function: " + Context);
		}//end switch

	}//end function

	
	//=================================
	function CompareMaterials(){
	//=================================
		var SelectedFolderID = GetSelectedFolderID();
		window.location.href="/folders/Compare.aspx?FolderID=" + SelectedFolderID;
	}

	//=================================
	// Runs on drpFolderList.onchange, and also is called by other methods.
	// Sets the checkbox for each visible material in the folder.
	// Also sets txtFolderMatCount with the MatCount indicator for the selected folder.
	//=================================
	function SetSelectedFolder(){
		//alert("SetSelectedFolder()");
		var SelectedFolderID = GetSelectedFolderID();
		//alert("SelectedFolderID :" + SelectedFolderID)
		
		aci.matweb.WebServices.Folders.SetSelectedFolder(SelectedFolderID,SetSelectedFolder_CALLBACK,ErrorHandler,"update_txtFolderMatCount");
		
		HideFolderNameControls();
	}

	// =================================
	// CALLBACK FUNCTION
	// Returns a list of MatID's in the selected folder, and sets the checkbox for each material.
	// Also sets txtFolderMatCount with the MatCount indicator for the selected folder.
	// =================================
	function SetSelectedFolder_CALLBACK(data,context){

		// alert(data);
		// alert(context);

		// the folder web service returns an object with 4 fields
		//alert("data.FolderMatCount: " + data.FolderMatCount);
		//alert("data.FolderMaxMatls: " + data.FolderMaxMatls);
		//alert("data.DisplayMessage: " + data.DisplayMessage);
		//alert("data.MatIDList: " + data.FolderMatIDList);

		if(data.DisplayMessage){
			matweb.alert(data.DisplayMessage);
		}

		txtFolderMatCount.value = data.FolderMatCount + "/" + data.FolderMaxMatls;

		//alert(SearchMatIDList);
		var chkFolder;

		// Set checkboxes for any visible materials in the folder
		// loop over all materials in the search results for the current page...
		//alert('lenOfArray=' + SearchMatIDList.length);
		for(s=0;s<SearchMatIDList.length;s++){
			chkFolder = document.getElementById("chkFolder_" + SearchMatIDList[s]);
			//alert('index=' + SearchMatIDList[s]);
			chkFolder.checked = false; //clear any currently checked boxes...
			//loop over all materials in the folder...
			//If there is a match, make it checked.
			for(f=0;f<data.FolderMatIDList.length;f++){
				if(SearchMatIDList[s]==data.FolderMatIDList[f]){
					//alert("Match found: " + data.FolderMatIDList[f]);
					chkFolder.checked = true;
				}
			}
		}

	}//end function
	
	//=================================
	function RunFolderAction(){
	//=================================
	
		//alert("RunFolderAction()");
		var action = drpFolderAction.options[drpFolderAction.selectedIndex].value;
		var SelectedFolderName = drpFolderList.options[drpFolderList.selectedIndex].text;
		var FolderID = GetSelectedFolderID();
		var msg = "";
		
		switch(action){
			case "empty":
				aci.matweb.WebServices.Folders.EmptyFolder(FolderID,SetSelectedFolder_CALLBACK,ErrorHandler,action);
				msg = "Folder ["+SelectedFolderName+"] was cleared of materials";
				HideFolderNameControls();
				break;
			case "delete":
				var answer = confirm("Delete folder ["+SelectedFolderName+"] ?");
				if(answer){
					gOldFolderName = SelectedFolderName;
					aci.matweb.WebServices.Folders.DeleteFolder(FolderID,DeleteFolder_CALLBACK,ErrorHandler,action);
				}
				HideFolderNameControls();
				break;
			case "new":
				//setting up new folder controls
				if(drpFolderList.options.length >= gMaxFolders){
					matweb.alert("You have already added as many folders as you are currently allowed by the system (" + gMaxFolders + ").<p/> You may view our <a href=\"/membership/benefits.aspx\">benefits</a> page for the features that are allowed for your current user level.");
				}
				else{
				divFolderName.style.display="block";
				divFolderName.innerHTML="New Folder Name";
				tblFolderName.style.display="block";
				txtFolderName.value = "New Folder Name";
				txtFolderName.focus();
				txtFolderName.select();
				}
				break;
			case "rename":
				//setting up rename folder controls
				divFolderName.style.display="block";
				divFolderName.innerHTML="Rename Folder";
				tblFolderName.style.display="block";
				txtFolderName.value = SelectedFolderName;
				txtFolderName.focus();
				txtFolderName.select();
				break;
			case "managefolders":
				window.location.href="/folders/ListFolders.aspx";
				break;
			case "expSolidworks":
				ExportFolder(FolderID, 2, null);
				break;
			case "expALGOR":
				ExportFolder(FolderID, 3, null);
				break;
			case "expANSYS":
				ExportFolder(FolderID, 4, null);
				break;
			case "expNEiWorks":
				ExportFolder(FolderID, 5, null);
				break;
			case "expCOMSOL3":
				ExportFolder(FolderID, 6, '3.4 or newer');
				break;
			case "expCOMSOL4":
				ExportFolder(FolderID, 10, null);
				break;
			case "expETBX":
				ExportFolder(FolderID, 8, null);
				break;
			case "expSpaceClaim":
				ExportFolder(FolderID, 9, null);
				break;
			case "0":
				//do nothing
				drpFolderAction.selectedIndex = 0;
				break;
			default: 
				alert("Unhandled Action: " + action);
		}

		//drpFolderAction.selectedIndex=0;
		divUserMessage.innerHTML = msg;
		//alert("End RunFolderAction()");
		
	}
	
	function DeleteFolder_CALLBACK(DeleteWasSuccessfull,context){
		divUserMessage.innerHTML = "Folder ["+gOldFolderName+"] was deleted.";
		//Refresh the folder list and selected checkboxes
		aci.matweb.WebServices.Folders.GetFolderList(GetFolderList_CALLBACK,ErrorHandler,"DeleteFolder_CALLBACK");
	}	

	// rsw
	function ExportFolder(folderid, modeid, comsolversion){
		if(txtFolderMatCount.value.substring(0,1) == '0'){
			alert('Folder is empty, nothing to export.');
			drpFolderAction.selectedIndex = 0;
			return;
		}

		var ExportPath = "/folders/ListFolders.aspx";
		
		//alert(ExportPath);
		location.href = ExportPath;
		//window.open(ExportPath, '', '', '');
	}
	
	
	
	
	//===============================================================
	// This runs on the onclick event for btnApplyFolderName.
	// It handles creating a new folder, or renaming an existing one.
	//===============================================================
	function ApplyFolderName(){
	
		//alert("ApplyFolderName()");
		var action = drpFolderAction.options[drpFolderAction.selectedIndex].value;
		var NewFolderName = txtFolderName.value;
		var FolderID = GetSelectedFolderID();
		var msg = "";

		//alert(FolderID);

		//If we are showing the non-existant "virtual" folder "My Folder",
		//create it first, and then rename it.
		if(action=="rename" && FolderID==0)
			action="add_and_rename";

		switch(action){
			case "new":
				aci.matweb.WebServices.Folders.AddFolder(NewFolderName,AddRenameFolder_CALLBACK,ErrorHandler,action);
				break;
			case "rename":
				gOldFolderName = drpFolderList.options[drpFolderList.selectedIndex].text;
				aci.matweb.WebServices.Folders.RenameFolder(FolderID,NewFolderName,AddRenameFolder_CALLBACK,ErrorHandler,action);
				break;
			case "add_and_rename":
				gOldFolderName = drpFolderList.options[drpFolderList.selectedIndex].text;
				aci.matweb.WebServices.Folders.AddFolder(NewFolderName,AddRenameFolder_CALLBACK,ErrorHandler,action);
				break;
			default: 
				alert("Unhandled Action in ApplyFolderName(): " + action);
		}

		HideFolderNameControls();
		
	}
	
	//=================================
	// CALLBACK FUNCTION
	//=================================
	function AddRenameFolder_CALLBACK(ReturnData,context){
		//alert("AddRenameFolder_CALLBACK()");
		
		var msg;
		//Folder = ReturnData.Folder;
	
		switch(context){
			case "new":
				//gNewFolderID = Folder.FolderID;
				gNewFolderID = ReturnData.NewFolderID;
				msg = "The folder [" + ReturnData.NewFolderName + "] was created.";
				break;
			case "rename":
				msg = "The folder [" + gOldFolderName+ "] was renamed to [" + ReturnData.NewFolderName + "].";
				break;
			case "add_and_rename":
				//gNewFolderID = Folder.FolderID;
				gNewFolderID = ReturnData.NewFolderID;
				msg = "The folder [" + gOldFolderName+ "] was renamed to [" + ReturnData.NewFolderName + "].";
				break;
		}
		
		divUserMessage.innerHTML = msg;
		
		//refresh the folder list, so it includes the new folder
		aci.matweb.WebServices.Folders.GetFolderList(GetFolderList_CALLBACK,ErrorHandler,"AddFolder");

	}
	
	//=================================
	function HideFolderNameControls(){
	//=================================
		//These fields will not exist for anonymous users
		if(drpFolderAction){
			drpFolderAction.selectedIndex=0;
			divFolderName.style.display="none";
			tblFolderName.style.display="none";
		}
	}
	
	//=================================
	function GetSelectedFolderID(){
	//=================================
		var FolderID = 0;
		//if one of the processeses created a new folder id, then use it.
		// alert('gNewFolderID=' + gNewFolderID);
		if(gNewFolderID > 0){
			FolderID = gNewFolderID;
			//gNewFolderID = 0;//clear the new folder id, so we don't use it again.
		}
		else 
			if(drpFolderList != null){
				if(drpFolderList.options.length == 0){
					FolderID = gSelectedFolderID;
				}
				else{
					var SelectedOption = drpFolderList.options[drpFolderList.selectedIndex];  
					FolderID = SelectedOption.value; //this will be 0 if no folders exist yet
				}
			}
		return FolderID;
	}

	//=================================
	function UnregisteredUserMessage(chkBox){
	//=================================
		matweb.alert("This feature is reserved for registered users.<p/><a href='/membership/regstart.aspx'>Click here to register.</a>","Reserved Feature");
		chkBox.checked = false;
	}

	// =================================
	// CALLBACK FUNCTION
	// The folder web service returns a JS array of iBatis data objects of type: aci.matweb.data.folder.Folder 
	// The drop down list of folders is set using this list.
	// =================================
	function GetFolderList_CALLBACK(iFolderList,Context){
	
		//alert(iFolderList);
		//alert('iFolderList.length: ' + iFolderList.length);

		if(drpFolderList != null){
		
		var SelectedFolderID = GetSelectedFolderID();//remember the current selected folder, so we can use it later
		var opt;
		var folder;

		//clear the options and reload them
		drpFolderList.options.length = 0;

		//add a default empty folder...		
		if(iFolderList.length==0){
			opt = document.createElement('OPTION');
			opt.value = "0";
			opt.text = "My Folder";
			drpFolderList.options.add(opt);
			i++;
		}
		
		for(var i = 0;i < iFolderList.length;i++){
			opt = document.createElement('OPTION');
			folder = iFolderList[i];
			//alert(folder.FolderID);
			//alert(folder.FolderName);
			opt.value = folder.FolderID;
			opt.text = folder.FolderName;
			if(folder.FolderID == SelectedFolderID) opt.selected = true;
			drpFolderList.options.add(opt);
		}

		//clear the new folder id, so we don't use it again. 
		gNewFolderID = 0;
		
		//Set checkboxes and folder count fields for the new selected folder
		SetSelectedFolder();
		}

	}
	
	// =================================
	// ON ERROR CALLBACK FUNCTION
	// =================================
	function ErrorHandler(error,context) {
	
			var stackTrace = error.get_stackTrace();
			var message = error.get_message();
			var statusCode = error.get_statusCode();
			var exceptionType = error.get_exceptionType();
			var timedout = error.get_timedOut();
			var Debug = false;

			// Compose the error message...
			var ErrMsg =     
					"<strong>Stack Trace</strong><br/>" +  stackTrace + "<br/>" +
					"<strong>WebService Error</strong><br/>" + message + "<br/>" +
					"<strong>Status Code</strong><br/>" + statusCode + "<br/>" +
					"<strong>Exception Type</strong><br/>" + exceptionType + "<br/>" +
					"<strong>JS Context</strong><br/>" + context + "<br/>" +
					"<strong>Timedout</strong><br/>" + timedout + "<br/>" +
					"<strong>Source Page</strong><br/>" + "/search/MaterialGroupSearch.aspx";
			//matweb.alert(ErrMsg,"Error in WebService",null,"500");
			if(Debug){
				matweb.alert(ErrMsg,"Error in WebService",null,"500");
				//alert(ErrMsg);
			}
			else{
				ErrMsg = escape(ErrMsg);
				//matweb.alert(ErrMsg);
				//window.location.href="/errorJS.aspx?ErrMsg=" + ErrMsg;
				divUserMessage.innerHTML = "Problem retrieving folder data...Some folder features may not work.";
			}
		
	}
	
	function ShowInterpolationMessage(){
		InterpolationMsg = "The material was found via interpolation, so no data points can be displayed. See the material data sheet for actual data points.";
		matweb.alert(InterpolationMsg);
	}
	
</script>

		</td>
	</tr>
	</table>

<hr />
<a id="help"></a>
<table><tr><td>
<p/><b>Instructions:</b> This page allows you to quickly access all of the polymers/plastics, metals, ceramics, fluids, and other engineering materials
in the MatWeb material property database.&nbsp; Just select the material category you would like to find from the tree.  Click on the [+] icon to open subcategory branches in the tree.
Then click the <b>'Find'</b> button next to its box.&nbsp; You can then follow the links to the most complete data sheets on the Web, with information on over 1000 available properties, including dielectric
constant, Poisson's ratio, glass transition temperature, dissipation factor, and Vicat softening point.
</td><td><a href="https://proto3000.com"><img src="/images/assets/proto3000.jpg" border = "0"><br>

Proto3000 - Rapid Prototyping</a>
</td></tr></table>

<p/><span class="hiText">Notes:</span>

<ul>
  <li>Visit MatWeb's <a href="/search/PropertySearch.aspx">property-based search page</a> to limit your results to materials that meet specific property performance criteria.</li>
  <li>Text searches can be done from any page - use the <b>Quick Search</b> box in the top right of the page.</li>
  <li>More <a href="/help/strategy.aspx"><strong>Search Strategies</strong></a> on how to find information in MatWeb.</li>
   <li><a href="/search/GetAllMatls.aspx">Show All Materials</a> - an inefficient drill-down to data sheets.  Registered users who are logged in omit a step in this drill down process.</li>
</ul>

<p/><a href="#Top">Top</a>

<script type="text/javascript">

	var txtMatGroupText = document.getElementById("ctl00_ContentMain_txtMatGroupText");
	var txtMatGroupID = document.getElementById("ctl00_ContentMain_txtMatGroupID");
	var divMatGroupName = document.getElementById("ctl00_ContentMain_divMatGroupName");

	function ucMatGroupFinder1_OnSelected(MatGroupID,MatGroupText){
		txtMatGroupText.value = MatGroupText;
		txtMatGroupID.value = MatGroupID;
		divMatGroupName.innerHTML = MatGroupText;
		//__doPostBack("ctl00$ContentMain$btnSubmit","");
	}

	function ucMatGroupTree_OnNodeClick(NodeText,MatGroupID){
		txtMatGroupText.value = NodeText;
		txtMatGroupID.value = MatGroupID;
		divMatGroupName.innerHTML = NodeText;
		//__doPostBack("ctl00$ContentMain$btnSubmit","");
	}
	
	function validate(){
		if (txtMatGroupID.value == ""){
			matweb.alert("Please select a material category","User Input Error");
			return false;
		}
		
		return true;
		
	}

</script>


</div>
<!-- ===================================== END MAIN CONTENT ========================================== -->

<table class="tabletight" style="width:100%" >
        <tr>
                <td align="center" class="footer" colspan="3">
                        <br /><br />
                
                        <a href="/membership/regupgrade.aspx" class="footlink"><span class="subscribeLink">Subscribe to Premium Services</span></a><br/>
                
                        <span class="footer"><b>Searches:</b>&nbsp;&nbsp;</span>
                        <a href="/search/AdvancedSearch.aspx" class="footlink"><span class="foot">Advanced</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/search/CompositionSearch.aspx" class="footlink"><span class="foot">Composition</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/search/PropertySearch.aspx" class="footlink"><span class="foot">Property</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/search/MaterialGroupSearch.aspx" class="footlink"><span class="foot">Material&nbsp;Type</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/search/SearchManufacturerName.aspx" class="footlink"><span class="foot">Manufacturer</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/search/SearchTradeName.aspx" class="footlink"><span class="foot">Trade&nbsp;Name</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/search/SearchUNS.aspx" class="footlink"><span class="foot">UNS Number</span></a>
                        <br />
                        <span class="footer"><b>Other&nbsp;Links:</b>&nbsp;&nbsp;</span>
                        <a href="/services/advertising.aspx" class="footlink"><span class="foot">Advertising</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/services/submitdata.aspx" class="footlink"><span class="foot">Submit&nbsp;Data</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/services/databaselicense.aspx" class="footlink"><span class="foot">Database&nbsp;Licensing</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/services/webhosting.aspx" class="footlink"><span class="foot">Web&nbsp;Design&nbsp;&amp;&nbsp;Hosting</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/clickthrough.aspx?addataid=277" class="footlink"><span class="foot">Trade&nbsp;Publications</span></a>

                        <br />
                        <a href="/reference/suppliers.aspx" class="footlink"><span class="foot">Supplier&nbsp;List</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/tools/unitconverter.aspx" class="footlink"><span class="foot">Unit&nbsp;Converter</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/tools/contents.aspx#reference" class="footlink"><span class="foot">Reference</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/reference/link.aspx" class="footlink"><span class="foot">Links</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/help/help.aspx" class="footlink"><span class="foot">Help</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/services/contact.aspx" class="footlink"><span class="foot">Contact&nbsp;Us</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/tools/contents.aspx" class="footlink"><span class="foot" style="color:red;">Site&nbsp;Map</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/reference/faq.aspx" class="footlink"><span class="foot">FAQ</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/index.aspx" class="footlink"><span class="foot">Home</span></a>
                        <br />&nbsp;
                        <div id="fb-root"></div>

<table><tr><td><script src="http://connect.facebook.net/en_US/all.js#xfbml=1"></script><fb:like-box href="https://www.facebook.com/MatWeb.LLC" width="175" show_faces="false" stream="false" header="false"></fb:like-box>

</td><td>

<a href="https://twitter.com/MatWeb" class="twitter-follow-button" data-show-count="false" data-show-screen-name="false">Follow @MatWeb</a> <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>

</td></tr></table>
                </td>
        </tr>
        <tr>
                <td colspan="3"><img src="/images/bluebar.gif" width="100%" height="5" alt="" /></td>
        </tr>
        <tr>
                <td style="background-image:url(/images/gray.gif);"><img src="/images/spacer.gif" width="5" height="1" alt=""/></td>
                <td style="background-image:url(/images/gray.gif);">
                        <span class="footer">
                        Please read our <a href="/reference/terms.aspx" class="footlink"><span class="foot">License Agreement</span></a> regarding materials data and our  <a href="/reference/privacy.aspx" class="footlink"><span class="foot">Privacy Policy</span></a>.
                        Questions or comments about MatWeb? Please contact us at
                        <a href="mailto:webmaster@matweb.com" class="footlink"><span class="foot">webmaster@matweb.com</span></a>. We appreciate your input.
                        <br /><br />
                        The contents of this web site, the MatWeb logo, and "MatWeb" are Copyright 1996-2024
                        by MatWeb, LLC. MatWeb is intended for personal, non-commercial use. The contents, results, and technical data from this site
                        may not be reproduced either electronically, photographically or substantively without permission from MatWeb, LLC.
                        </span>
                        <br />
                </td>

                <td style="background-image:url(/images/gray.gif);"><img src="/images/spacer.gif" width="5" height="1" alt=""/></td>
        </tr>
</table>




<script type="text/javascript">
//<![CDATA[
var ctl00_ContentMain_ucMatGroupTree_msTreeView_ImageArray =  new Array('', '', '', '/WebResource.axd?d=zLp3QJeZSj4-fsnL2_raV70Mk3pr-OdjL_rvrZzXUTCnZ6LU5Dg_Tc5XZg4eZpT9O0gxZJrJcxGb23XJofeGD9JhYLr2wl0VkQZnJOx0T0ShBosq0&t=638313619312541215', '/WebResource.axd?d=vFualGN2ZZ4fny_RX_gYiUTY6FBLUapne4VfY6P3sGiyrOYd-Ji2sWg_olT-exaztendUUEOoH87RS2l44bpZTscfmmv4MKRqJK8rgesmkrNfP2O0&t=638313619312541215', '/WebResource.axd?d=4nLCbQ2Yfc2i_msrQXqKzK8pLQD-GrNVZ7DeXU-La2qatczrgeCLazv0RzIkDDHyYfLFalCBO8s71lRS8yifxTChF9dUJvTLxNEeeFlghyuY0dtM0&t=638313619312541215', '/WebResource.axd?d=uEI6Enq15_q4pXO6g0E7vtAqtD5WIokAEQ2Vz8V3Yi36cefediLfyCwENkPd-w1GxTXu-HkNqcDlrYOw8bxgGqeHJtzc-PBlTa9nrZRfdIyj8fQh0&t=638313619312541215', '/WebResource.axd?d=hzeik1gGhZrfDZXUWCMsBOx5b43bIFor8UCTzm_wuh92b9Z-WQU4_fQCSUw6CaP5tnc9LTU0jl0sy4Y6Cf_c8q8HFXRqeHQuscXIqqLNSZkgl2cu0&t=638313619312541215', '/WebResource.axd?d=6v5qM6VKQYD3XNwwXEy7Tix7dzVdqDJ8QTRIlUPrzXQqNsXQgUwIfmn34ZzRORd_IZj0bF4vkZzID1HuSFE5uKyLgaPYeAYE8VT8HYfb3Rk6zsDw0&t=638313619312541215', '/WebResource.axd?d=d9nh8nEM1YCqIfE9jLzNhPyFIZqARvlFExwnFG0vbvskO-A2qORlhTyUVIxDtSwldGrOp8o9ef8lZ9SRhPQ9rv9TkGV-NVYvH9_yHT6CvWpIPF4S0&t=638313619312541215', '/WebResource.axd?d=7_V5n1KYSQsj6PoSqgIxb0egBsAa91jSXPkbmM6FaiF14-JsNnpUDnfSdxZcvfd0OQCLDtC24pMIXigO-41cL2rsq-0xea-SSG7yZUyRcliFFqe70&t=638313619312541215', '/WebResource.axd?d=TinC6igX1vPDgWh7DstWwBTHTyH4uuybDU4NW47jJD06wHdAO3YyJC_eKK1Zn5oIbwqRo4Cri8YwhA82G-LD0p14yEF1EfoIZvKleYMIuQ2K1hkE0&t=638313619312541215', '/WebResource.axd?d=4B0dvJqmzY4ukTboQq7lmxokRL9NTsoWh4yErB11Tb-ucvMw_p_0mxYVn4U1Uz6Jk795umrO0C4-B-8Yq1Kxxz6uyDT15O2BSCb8349f4YWDdIVy0&t=638313619312541215', '/WebResource.axd?d=TUiibr9hjS_MlNf09dKZECe1z56fbaPbfgJfC5eYJi0TtdBviUcA1JQJLV81aqunzpXysgruhL3ihpgd7ziL5p9SQ86xVLNiUqbYpJnW9Rj7sN1E0&t=638313619312541215', '/WebResource.axd?d=vUBrM2nNhECNbFO0AJKq52p2zWrY5XG8nWgCDL4AfsrexStb0HREDV6GHMvdkAGDInxYtTyWXuxFUIiQv3JX2fScVRwQsrVv5srHrjOIeC3ayAG20&t=638313619312541215', '/WebResource.axd?d=Z-LP5lKcsnv2A9UpExXu-5leQYIAZMYp6SXNognZi1r66YT8u24-YjJaxjisE9ruYU8NKnNz72H4shclsk8PM3S5M-VYMk_F_G5fcd3ra6Sj9CaS0&t=638313619312541215', '/WebResource.axd?d=QI4SACqRn7PxBBHPfkR7peFMAEv7lX1_iZ_7jncKwxRTlvhSAncd5K2oRqw8J8WmJjWQcbGgEblhrufy-hUjZMa1YLUC47mjfV1eS4LM3kUqLT960&t=638313619312541215', '/WebResource.axd?d=ayrFqNSzdiTAdowDVbEH-ZzKbvw5bCq7onyCSAcJhCVPVIHlUB9M_fo8BLUTeGpbtcoMiyFy1MoeEIsweMrpVandBKC4vAK-NCBcfn2FIreK8mEJ0&t=638313619312541215', '/WebResource.axd?d=OK_uWiuoGURaJ3CC0fVlQ7v9GUnIAkx03w03rQ2ENqcwdObLiQwK7B9yRFyAWj9tuUqsa0874ksgkaRo8KirB7q1ZL_huh7gCq_XehXfHMEKMiAl0&t=638313619312541215');
//]]>
</script>


<script type="text/javascript">
//<![CDATA[
(function() {var fn = function() {AjaxControlToolkit.ModalPopupBehavior.invokeViaServer('ctl00_ContentMain_ucPopupMessage1_ModalPopupExtender1', false); Sys.Application.remove_load(fn);};Sys.Application.add_load(fn);})();
var callBackFrameUrl='/WebResource.axd?d=Mt7MojvAyTqAEcx0MpKgcF_QVa09tLyMQS6c5JLNO4j2M_F6CMxYnN0HdVYxyrhggOpkTwyNqQ51iS6UoHXgJqHBROE1&t=638313619312541215';
WebForm_InitCallback();var ctl00_ContentMain_ucMatGroupTree_msTreeView_Data = new Object();
ctl00_ContentMain_ucMatGroupTree_msTreeView_Data.images = ctl00_ContentMain_ucMatGroupTree_msTreeView_ImageArray;
ctl00_ContentMain_ucMatGroupTree_msTreeView_Data.collapseToolTip = "Collapse {0}";
ctl00_ContentMain_ucMatGroupTree_msTreeView_Data.expandToolTip = "Expand {0}";
ctl00_ContentMain_ucMatGroupTree_msTreeView_Data.expandState = theForm.elements['ctl00_ContentMain_ucMatGroupTree_msTreeView_ExpandState'];
ctl00_ContentMain_ucMatGroupTree_msTreeView_Data.selectedNodeID = theForm.elements['ctl00_ContentMain_ucMatGroupTree_msTreeView_SelectedNode'];
for (var i=0;i<19;i++) {
var preLoad = new Image();
if (ctl00_ContentMain_ucMatGroupTree_msTreeView_ImageArray[i].length > 0)
preLoad.src = ctl00_ContentMain_ucMatGroupTree_msTreeView_ImageArray[i];
}
ctl00_ContentMain_ucMatGroupTree_msTreeView_Data.lastIndex = 8;
ctl00_ContentMain_ucMatGroupTree_msTreeView_Data.populateLog = theForm.elements['ctl00_ContentMain_ucMatGroupTree_msTreeView_PopulateLog'];
ctl00_ContentMain_ucMatGroupTree_msTreeView_Data.treeViewID = 'ctl00$ContentMain$ucMatGroupTree$msTreeView';
ctl00_ContentMain_ucMatGroupTree_msTreeView_Data.name = 'ctl00_ContentMain_ucMatGroupTree_msTreeView_Data';
Sys.Application.initialize();
Sys.Application.add_init(function() {
    $create(AjaxControlToolkit.ModalPopupBehavior, {"BackgroundCssClass":"modalBackground","OkControlID":"ctl00_ContentMain_ucPopupMessage1_btnOK","PopupControlID":"ctl00_ContentMain_ucPopupMessage1_divPopupMessage","PopupDragHandleControlID":"ctl00_ContentMain_ucPopupMessage1_trTitleRow","dynamicServicePath":"/search/MaterialGroupSearch.aspx","id":"ctl00_ContentMain_ucPopupMessage1_ModalPopupExtender1"}, null, null, $get("ctl00_ContentMain_ucPopupMessage1_hndPopupControl"));
});
Sys.Application.add_init(function() {
    $create(AjaxControlToolkit.TextBoxWatermarkBehavior, {"ClientStateFieldID":"ctl00_ContentMain_UcMatGroupFinder1_TextBoxWatermarkExtender2_ClientState","WatermarkCssClass":"greyOut","WatermarkText":"Type at least 4 characters here...","id":"ctl00_ContentMain_UcMatGroupFinder1_TextBoxWatermarkExtender2"}, null, null, $get("ctl00_ContentMain_UcMatGroupFinder1_txtSearchText"));
});
//]]>
</script>
</form>


<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-15290815-1");
pageTracker._setDomainName(".matweb.com");
pageTracker._trackPageview();
} catch(err) {}</script>

<!-- 20160905 insert SpecialChem javascript -->
<script type="text/javascript" src="//collect.specialchem.com/collect.js"></script>
<script type="text/javascript">
    //<![CDATA[
    var _spc = (_spc || []);
    _spc.push(['init',
    {
        partner: 'MatWeb'
        
        //, iid: '12345'
    }]);
    //]]>
</script>
<script> (function(){ var s = document.createElement('script'); var h = document.querySelector('head') || document.body; s.src = 'https://acsbapp.com/apps/app/dist/js/app.js'; s.async = true; s.onload = function(){ acsbJS.init({ statementLink : '', footerHtml : '', hideMobile : false, hideTrigger : false, disableBgProcess : false, language : 'en', position : 'right', leadColor : '#146FF8', triggerColor : '#146FF8', triggerRadius : '50%', triggerPositionX : 'right', triggerPositionY : 'bottom', triggerIcon : 'people', triggerSize : 'bottom', triggerOffsetX : 20, triggerOffsetY : 20, mobile : { triggerSize : 'small', triggerPositionX : 'right', triggerPositionY : 'bottom', triggerOffsetX : 20, triggerOffsetY : 20, triggerRadius : '20' } }); }; h.appendChild(s); })();</script>
</body>
</html>
