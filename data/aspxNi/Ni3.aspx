

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head id="ctl00_mainHeader"><title>
	Metal, Plastic, and Ceramic Search Index
</title><meta http-equiv="Content-Style-Type" content="text/css" /><meta http-equiv="Content-Language" content="en" />

        <script src="/includes/flash.js" type="text/javascript"></script>

        <script src="/includes/prototype.js" type="text/javascript"></script>
        <!-- script src="/includes/scriptaculous/effects.js" type="text/javascript"></script -->
        <script src="/includes/matweb.js" type="text/javascript"></script>
        <link rel="stylesheet" href="/includes/main.css" />

        <script type="text/javascript">

                function _onload(){
                        matweb.debug = false; // ;
                        //matweb.writeOverlay();
                }

                function _onkeydown(evt){
                        //PREVENT GLOBAL AUTO-SUBMIT OF FORMS
                        return matweb.DisableAutoSubmit(evt);
                }

                //EXAMPLE USAGE:
                //matweb.register_onload(function(){alert("hello from onload event");});
                //matweb.writeOverlay();


        </script>

        <style type="text/css" media="screen">
        @import url("/ad_IES/css/styles.css");
        </style>
<meta name="KEYWORDS" content="carbon steels, low alloy steels, corrosion resistant stainless, high temperature stainless, heat resistant stainless steels, AISI steel alloy specifications, high strength steel alloys, tensile strength of steels, maraging, superalloy, lead alloy, magnesium, tin alloy, tungsten, zinc, precious metal, pure metallic element, TPE elastomer, dielectric constant, plastic dissipation factor, nylon glass transition temperature, ionomer vicat softening point, poisson's ratio, polyetherimide, fluoropolymer, silicone, polyester dielectric constant" /><meta name="DESCRIPTION" content="Searchable list of plastics, metals, and ceramics categorized into a fields like polypropylene, nylon, ABS, aluminum alloys, oxides, etc.  Search results are detailed data sheets with tensile strength, density, dielectric constant, dissipation factor, poisson's ratio, glass transition temperature, and Vicat softening point." /><meta name="ROBOTS" content="NOFOLLOW" /><style type="text/css">
	.ctl00_ContentMain_ucMatGroupTree_msTreeView_0 { text-decoration:none; }
	.ctl00_ContentMain_ucMatGroupTree_msTreeView_1 { color:Black; }
	.ctl00_ContentMain_ucMatGroupTree_msTreeView_2 { padding:0px 3px 0px 3px; }

</style></head>

<body onload="_onload();" onkeydown="return _onkeydown(event);">

<a id="Top"></a>

<div id="divSiteName" style="position:absolute; top:0; left:0 ;">
<a href="/index.aspx"><img src="/images/sitelive.gif" alt="" /></a>
</div>

<div id="divPopupOverlay" style="display:none;"></div>

<div id="divPopupAlert" class="popupAlert" style="display:none;">
        <table style="width:100%; height:100%; border-style:outset; border-width:5px; border-color:blue;" >
                <tr><th style=" height:25px; background-color:Maroon; font-size:14px; color:White; "><span id="spanPopupAlertTitle">System Message</span></th></tr>
                <tr><td style=" vertical-align:top; padding:3px; background-color:White;" id="tdPopupAlertMessage" ></td></tr>
                <tr><td style=" height:25px; text-align:center; background-color:Silver;"><input type="button" id="btnPopupAlert" onclick="matweb.alertClose();" style="" value="OK" /></td></tr>
        </table>
</div>

<form name="aspnetForm" method="post" action="MaterialGroupSearch.aspx" onsubmit="javascript:return WebForm_OnSubmit();" id="aspnetForm">
<div>
<input type="hidden" name="__EVENTTARGET" id="__EVENTTARGET" value="" />
<input type="hidden" name="__EVENTARGUMENT" id="__EVENTARGUMENT" value="" />
<input type="hidden" name="__LASTFOCUS" id="__LASTFOCUS" value="" />
<input type="hidden" name="ctl00_ContentMain_ucMatGroupTree_msTreeView_ExpandState" id="ctl00_ContentMain_ucMatGroupTree_msTreeView_ExpandState" value="ccccccnc" />
<input type="hidden" name="ctl00_ContentMain_ucMatGroupTree_msTreeView_SelectedNode" id="ctl00_ContentMain_ucMatGroupTree_msTreeView_SelectedNode" value="" />
<input type="hidden" name="ctl00_ContentMain_ucMatGroupTree_msTreeView_PopulateLog" id="ctl00_ContentMain_ucMatGroupTree_msTreeView_PopulateLog" value="" />
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="/wEPDwULLTE2Nzc3MDc2MDAPZBYCZg9kFgICAQ9kFioCAQ9kFgJmDxYCHgRUZXh0BWdEYXRhIHNoZWV0cyBmb3Igb3ZlciA8c3BhbiBjbGFzcz0iZ3JlZXRpbmciPjE4MCwwMDA8L3NwYW4+ICBtZXRhbHMsIHBsYXN0aWNzLCBjZXJhbWljcywgYW5kIGNvbXBvc2l0ZXMuZAICD2QWAmYPFgIfAAUESE9NRWQCAw9kFgJmDxYCHwAFBlNFQVJDSGQCBA9kFgJmDxYCHwAFBVRPT0xTZAIFD2QWAmYPFgIfAAUJU1VQUExJRVJTZAIGD2QWAmYPFgIfAAUHRk9MREVSU2QCBw9kFgJmDxYCHwAFCEFCT1VUIFVTZAIID2QWAmYPFgIfAAUDRkFRZAIJD2QWAmYPFgIfAAUHQUNDT1VOVGQCCg9kFgJmDxYCHwAFBkxPRyBJTmQCCw8QZGQWAWZkAgwPZBYCZg8WAh8ABQhTZWFyY2hlc2QCDQ9kFgJmDxYCHwAFCEFkdmFuY2VkZAIOD2QWAmYPFgIfAAUIQ2F0ZWdvcnlkAg8PZBYCZg8WAh8ABQhQcm9wZXJ0eWQCEA9kFgJmDxYCHwAFBk1ldGFsc2QCEQ9kFgJmDxYCHwAFClRyYWRlIE5hbWVkAhIPZBYCZg8WAh8ABQxNYW51ZmFjdHVyZXJkAhMPZBYCAgEPZBYCZg8WAh8ABRlSZWNlbnRseSBWaWV3ZWQgTWF0ZXJpYWxzZAIVD2QWEAIDD2QWAmYPFgIfAAWXATxhIGhyZWY9Ii9jbGlja3Rocm91Z2guYXNweD9hZGRhdGFpZD0xMzUxIj48aW1nIHNyYz0iL2ltYWdlcy9hc3NldHMvbWV0YWxtZW4tTmlja2VsLnBuZyIgYm9yZGVyPTAgYWx0PSJNZXRhbG1lbiBTYWxlcyIgd2lkdGggPSAiNzI4IiBoZWlnaHQgPSAiOTAiPjwvYT5kAgQPZBYCAgIPFgIeBXN0eWxlBRlkaXNwbGF5Om5vbmU7d2lkdGg6MjUwcHg7FgYCAQ9kFgJmD2QWAmYPDxYCHwAFElVzZXIgSW5wdXQgUHJvYmxlbWRkAgMPDxYCHwBlZGQCBQ8WAh4FdmFsdWUFAk9LZAIFD2QWBAIBDw9kFgIfAQUMV2lkdGg6MzA0cHg7ZAICDxAWBB8BBStwb3NpdGlvbjphYnNvbHV0ZTtkaXNwbGF5Om5vbmU7V2lkdGg6MzA4cHg7HgRzaXplBQIxOGRkZAIGDw8WBB4IQ2xpZW50SUQCAR4KTGFuZ3VhZ2VJRAIBZBYCZg8UKwAJDxYOHhdQb3B1bGF0ZU5vZGVzRnJvbUNsaWVudGceEkVuYWJsZUNsaWVudFNjcmlwdGceCVNob3dMaW5lc2ceCE5vZGVXcmFwZx4NTmV2ZXJFeHBhbmRlZGQeDFNlbGVjdGVkTm9kZWQeCUxhc3RJbmRleAIIZBYIHgtOb2RlU3BhY2luZxsAAAAAAAAAAAEAAAAeCUZvcmVDb2xvcgojHhFIb3Jpem9udGFsUGFkZGluZxsAAAAAAAAIQAEAAAAeBF8hU0IChIAYZGRkZGRkFCsACQUjMjowLDA6MCwwOjEsMDoyLDA6MywwOjQsMDo1LDA6NiwwOjcUKwACFgwfAAUSQ2FyYm9uICg4NjYgbWF0bHMpHgVWYWx1ZQUDMjgzHghFeHBhbmRlZGgeDFNlbGVjdEFjdGlvbgsqLlN5c3RlbS5XZWIuVUkuV2ViQ29udHJvbHMuVHJlZU5vZGVTZWxlY3RBY3Rpb24CHgtOYXZpZ2F0ZVVybAVBamF2YXNjcmlwdDp1Y01hdEdyb3VwVHJlZV9Pbk5vZGVDbGljaygnQ2FyYm9uICg4NjYgbWF0bHMpJywnMjgzJykeEFBvcHVsYXRlT25EZW1hbmRnZBQrAAIWDB8ABRVDZXJhbWljICgxMDAwNCBtYXRscykfEQUCMTEfEmgfEwsrBAIfFAVDamF2YXNjcmlwdDp1Y01hdEdyb3VwVHJlZV9Pbk5vZGVDbGljaygnQ2VyYW1pYyAoMTAwMDQgbWF0bHMpJywnMTEnKR8VZ2QUKwACFgwfAAUSRmx1aWQgKDc1NjIgbWF0bHMpHxEFATUfEmgfEwsrBAIfFAU/amF2YXNjcmlwdDp1Y01hdEdyb3VwVHJlZV9Pbk5vZGVDbGljaygnRmx1aWQgKDc1NjIgbWF0bHMpJywnNScpHxVnZBQrAAIWDB8ABRNNZXRhbCAoMTcwNTIgbWF0bHMpHxEFATkfEmgfEwsrBAIfFAVAamF2YXNjcmlwdDp1Y01hdEdyb3VwVHJlZV9Pbk5vZGVDbGljaygnTWV0YWwgKDE3MDUyIG1hdGxzKScsJzknKR8VZ2QUKwACFgwfAAUnT3RoZXIgRW5naW5lZXJpbmcgTWF0ZXJpYWwgKDgwNjMgbWF0bHMpHxEFAzI4NR8SaB8TCysEAh8UBVZqYXZhc2NyaXB0OnVjTWF0R3JvdXBUcmVlX09uTm9kZUNsaWNrKCdPdGhlciBFbmdpbmVlcmluZyBNYXRlcmlhbCAoODA2MyBtYXRscyknLCcyODUnKR8VZ2QUKwACFgwfAAUVUG9seW1lciAoOTc2MzUgbWF0bHMpHxEFAjEwHxJoHxMLKwQCHxQFQ2phdmFzY3JpcHQ6dWNNYXRHcm91cFRyZWVfT25Ob2RlQ2xpY2soJ1BvbHltZXIgKDk3NjM1IG1hdGxzKScsJzEwJykfFWdkFCsAAhYMHwAFGFB1cmUgRWxlbWVudCAoNTA3IG1hdGxzKR8RBQMxODQfEmgfEwsrBAIfFAVHamF2YXNjcmlwdDp1Y01hdEdyb3VwVHJlZV9Pbk5vZGVDbGljaygnUHVyZSBFbGVtZW50ICg1MDcgbWF0bHMpJywnMTg0JykfFWhkFCsAAhYMHwAFJVdvb2QgYW5kIE5hdHVyYWwgUHJvZHVjdHMgKDM5OCBtYXRscykfEQUDMjc3HxJoHxMLKwQCHxQFVGphdmFzY3JpcHQ6dWNNYXRHcm91cFRyZWVfT25Ob2RlQ2xpY2soJ1dvb2QgYW5kIE5hdHVyYWwgUHJvZHVjdHMgKDM5OCBtYXRscyknLCcyNzcnKR8VZ2RkAgcPFgIeCWlubmVyaHRtbAUZTmlja2VsIEFsbG95ICgxMjEwIG1hdGxzKWQCDQ8PFgIeB1Zpc2libGVoZGQCDg8PFgIfF2dkFgQCAQ8PFgIfAAUEMTI0NWRkAgMPDxYCHwAFDE5pY2tlbCBBbGxveWRkAg8PDxYGHgtDdXJyZW50UGFnZQIDHg9DdXJyZW50U2VhcmNoSUQCsfODEx8XZ2QWBAICDw8WAh8AZWRkAgMPDxYCHxdnZBY8AgEPDxYCHwAFBDEyNDVkZAIDDxBkEBUFATEBMgEzATQBNRUFATEBMgEzATQBNRQrAwVnZ2dnZxYBAgJkAgUPDxYCHwAFATdkZAIHDw8WCB8ABQtbUHJldiBQYWdlXR8OCiMeB0VuYWJsZWRnHxACBGRkAgkPDxYGHwAFC1tOZXh0IFBhZ2VdHw4KIx8QAgRkZAILDxBkZBYBAgVkAg8PFgIfF2hkAhEPEA9kFgIeCG9uY2hhbmdlBRNTZXRTZWxlY3RlZEZvbGRlcigpZGRkAhUPFgIfF2gWAgIBDxBkZBYBZmQCFw9kFgQCAQ8PFgQfAGUfGmhkZAICDw8WAh8XaGRkAhkPDxYEHwAFDU1hdGVyaWFsIE5hbWUfGmhkZAIbDw8WAh8XaGRkAh0PDxYCHxdoZGQCHw9kFgYCAQ8PFgQfAAUFUHJvcDEfGmhkZAIDDw8WAh8XaGRkAgUPDxYCHxdoZGQCIQ9kFgYCAQ8PFgQfAAUFUHJvcDIfGmhkZAIDDw8WAh8XaGRkAgUPDxYCHxdoZGQCIw9kFgYCAQ8PFgQfAAUFUHJvcDMfGmhkZAIDDw8WAh8XaGRkAgUPDxYCHxdoZGQCJQ9kFgYCAQ8PFgQfAAUFUHJvcDQfGmhkZAIDDw8WAh8XaGRkAgUPDxYCHxdoZGQCJw9kFgYCAQ8PFgQfAAUFUHJvcDUfGmhkZAIDDw8WAh8XaGRkAgUPDxYCHxdoZGQCKQ9kFgYCAQ8PFgQfAAUFUHJvcDYfGmhkZAIDDw8WAh8XaGRkAgUPDxYCHxdoZGQCKw9kFgYCAQ8PFgQfAAUFUHJvcDcfGmhkZAIDDw8WAh8XaGRkAgUPDxYCHxdoZGQCLQ9kFgYCAQ8PFgQfAAUFUHJvcDgfGmhkZAIDDw8WAh8XaGRkAgUPDxYCHxdoZGQCLw9kFgYCAQ8PFgQfAAUFUHJvcDkfGmhkZAIDDw8WAh8XaGRkAgUPDxYCHxdoZGQCMQ9kFgYCAQ8PFgQfAAUGUHJvcDEwHxpoZGQCAw8PFgIfF2hkZAIFDw8WAh8XaGRkAjUPDxYCHwAFBDEyNDVkZAI3DxBkEBUFATEBMgEzATQBNRUFATEBMgEzATQBNRQrAwVnZ2dnZxYBAgJkAjkPDxYCHwAFATdkZAI7Dw8WCB8ABQtbUHJldiBQYWdlXR8OCiMfGmcfEAIEZGQCPQ8PFgYfAAULW05leHQgUGFnZV0fDgojHxACBGRkAj8PEGRkFgECBWQCQQ8WAh8XaBYCAgEPDxYCHwAFuAQ8YnIgLz4NCk1hdGVyaWFscyBmbGFnZ2VkIGFzIGRpc2NvbnRpbnVlZCAoPGltZyBzcmM9Ii9pbWFnZXMvYnV0dG9ucy9pY29uRGlzY29udGludWVkLmpwZyIgYWx0PSIiIC8+KSBhcmUgbm8gbG9uZ2VyIHBhcnQgb2YgdGhlIG1hbnVmYWN0dXJlcuKAmXMgc3RhbmRhcmQgcHJvZHVjdCBsaW5lIGFjY29yZGluZyB0byBvdXIgbGF0ZXN0IGluZm9ybWF0aW9uLiAgVGhlc2UgbWF0ZXJpYWxzIG1heSBiZSBhdmFpbGFibGUgYnkgc3BlY2lhbCBvcmRlciwgaW4gZGlzdHJpYnV0aW9uIGludmVudG9yeSwgb3IgcmVpbnN0YXRlZCBhcyBhbiBhY3RpdmUgcHJvZHVjdC4gIERhdGEgc2hlZXRzIGZyb20gbWF0ZXJpYWxzIHRoYXQgYXJlIG5vIGxvbmdlciBhdmFpbGFibGUgcmVtYWluIGluIE1hdFdlYiB0byBhc3Npc3QgdXNlcnMgaW4gZmluZGluZyByZXBsYWNlbWVudCBtYXRlcmlhbHMuICANCjxiciAvPjxiciAvPg0KVXNlcnMgb2Ygb3VyIEFkdmFuY2VkIFNlYXJjaCAocmVnaXN0cmF0aW9uIHJlcXVpcmVkKSBtYXkgZXhjbHVkZSBkaXNjb250aW51ZWQgbWF0ZXJpYWxzIGZyb20gc2VhcmNoIHJlc3VsdHMuZGQCFg9kFgJmDxYCHwAFcDxhIGhyZWY9Ii9jbGlja3Rocm91Z2guYXNweD9hZGRhdGFpZD0yNzciIGNsYXNzPSJmb290bGluayI+PHNwYW4gY2xhc3M9ImZvb3QiPlRyYWRlJm5ic3A7UHVibGljYXRpb25zPC9zcGFuPjwvYT5kGAEFHl9fQ29udHJvbHNSZXF1aXJlUG9zdEJhY2tLZXlfXxYEBTZjdGwwMCRDb250ZW50TWFpbiRVY01hdEdyb3VwRmluZGVyMSRzZWxlY3RDYXRlZ29yeUxpc3QFK2N0bDAwJENvbnRlbnRNYWluJHVjTWF0R3JvdXBUcmVlJG1zVHJlZVZpZXcFG2N0bDAwJENvbnRlbnRNYWluJGJ0blN1Ym1pdAUaY3RsMDAkQ29udGVudE1haW4kYnRuUmVzZXRTubOza3EcTTN/qijGbE/vlalNRg==" />
</div>

<script type="text/javascript">
//<![CDATA[
var theForm = document.forms['aspnetForm'];
if (!theForm) {
    theForm = document.aspnetForm;
}
function __doPostBack(eventTarget, eventArgument) {
    if (!theForm.onsubmit || (theForm.onsubmit() != false)) {
        theForm.__EVENTTARGET.value = eventTarget;
        theForm.__EVENTARGUMENT.value = eventArgument;
        theForm.submit();
    }
}
//]]>
</script>


<script src="/WebResource.axd?d=M68BSyLvwBMHRYzsSV62mvczr7w5VyKnq2JbgqcAAU1ioThlC6FNEVBncBdaA4oqf8cXPzy7QERzajvFfM67hSR6S0w1&amp;t=638313619312541215" type="text/javascript"></script>


<script src="/WebResource.axd?d=ClzPoKUn1iQnMGy7NeMJw70TvMayZ5w9nNmB7h_dY64i4BFFgjVcppyp69BFqhuzHctzc-rEantAjczzQ6UVMYkaWJw1&amp;t=638313619312541215" type="text/javascript"></script>
<script type="text/javascript">
//<![CDATA[

    function TreeView_PopulateNodeDoCallBack(context,param) {
        WebForm_DoCallback(context.data.treeViewID,param,TreeView_ProcessNodeData,context,TreeView_ProcessNodeData,false);
    }
var ctl00_ContentMain_ucMatGroupTree_msTreeView_Data = null;//]]>
</script>

<script src="/ScriptResource.axd?d=148LWaywfmset9PYVR0m-KYXX9JfyGYGqOtIYBGEAzoJh5VhFBwSI6JxnmoNvgtlAprhvntpc6lTDbRMgdKxckUmc8m2qw_fOCsX0G9LC8ojUpw4cTHWXgfuT_vU6m2Kpv3mt01iCczrTE2-J5X6EeBa2281&amp;t=637769794779625837" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=v_V1UgDrDAchg8Zuc1FL4qkvmjt-bwap7ZxwVGGPleYiJ6zfDOTL6wtCmVvILjbwo_sqb2PMUTNkjO4vp6nd-E7WWXTPgSI5-nvn3YDnHrVk5bgVX4Qv6F0fV4wTSslmaLZ3k98tckojDLWAEI7jm8h0H4d7BoGYxkqsyh8-Iujfrjjm0&amp;t=637769794779625837" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=V7Zma3AISNyGTbHVRPw8svJ4Ub7xUUaeoKwImcvnuDiy-XBR331l3eTjiEP8J_QlX2qgyAHxZFLywNbNyZ50fJLVEOCjx11lIDJOiQxc0oGA3qi7XXiggiwEcIWwzb9j2Is1n1D9sU40Zusk35AtkqRKL3Y1&amp;t=633177911400000000" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=wOguQo_yd3hCG4ajcSXPC6-XdeL0hmYWSMaFjvMXt1CBqR9neJqTXyTfPki4lIz4B29Zz_83btvS12BY6KpBKT-AgP0eaXp7V3h6XRdjiozV5w_n3oQRZg5Yq1QW8UNGj0Cy9jsap_7EcU2M99CapUyVG6I1&amp;t=633177911400000000" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=MAJxSy62Z3EtRwHXuuMc8KC6PFmrPqmkpcwdpcPGeqtqUQyJ3FBQ3cFyYWN1zNID_ngA53aMZdG_dpRu3TRDH8-iqKAjPAcc2BWe63WYGrGbyHc9syJxR6lypTgvRaYmwwEmkLEsaGUniMurHc16yr1VrjmzG3HWOeu9UXNr23Wv2xzh0&amp;t=633177911400000000" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=ukx9eyyt483y_CGgCF1OToQPWzPYyCynhpMvhxN7idzW3WaKjHjpgAOcMgtXZIEcspnbyDEADdRWCSHB-hF39Ycl13KWsqdlOHElw2qgSkGmkL8iNH37htb0BNS0lxw6oHvtqYiVpw-nZeYoQ1NkgX2qXOI1&amp;t=633177911400000000" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=Ovkr-MWDypjnq0C7FVbNgOs-Mt3NaktNVf8SEOX721PoQ5mQqTM6S4igSQL1_PXvh96km3stLiqrorOgm8nnnUU6y6edTkhxmLiwfFtEXQEAAnFFQpUepgsxEd0ahMvWcDfEac0EE7J8iDb2rqSMTxk51ydfY-c-8zcKvo5Mrn83cvfN0&amp;t=633177911400000000" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=pKQF4iQYwkSn5cAwTNDM8ez4TLepowWnai-WD6NVL5h0ay9Qtt0a9vPCaI-KN9OHcv1fL8VnwxEDM6EAVtgZY06V8XJTlVrbmb8WlMvGmhwBeL3_SVtKKOS-QExY8AnVbTFl_wOknplhy8bz-9sIWNCh4XPq1WhRj7ALNAKaeruPhS7d0&amp;t=633177911400000000" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=uM6eUuIuIa_NiCgVrtwgqczDeqTqI37XalY8Ri_W2ZjXEzIKi84afVBjFk8gneIDXcFnw8yjCD1qhE8mzDTzAUSdKnbDte0yC23kMoyvxPyu7vRH9E-6JMZtE8-gELUPmoZdOZJ9nXitHKYBLw2P6skU1IkzFRegK74bETu2FdEtuaeR0&amp;t=633177911400000000" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=ewF5FMb1ckbNCdJ9rpE5xMSXy-WecqVGE8uma1_520KusuW7jTUE2tnPMG7JyR0FDIFqDUMQbsfTZlsQ0sD4YAW8MnN90GKIMPvzaP7eOG3ss8E050_l7Rpnaz7I_U3ITzBXkcJZIOPcI1PxOReT8FSeWmo1&amp;t=633177911400000000" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=3lB_EBCMPyiwzDhzWdoE-i4mLHhkIMbbkaLwMr-U1WcAoDqBHS-oYFn4gz3ZwnpzK_KZp97Ev8ZbiUNo7WMJ45h2xgOH59erMFo1BpNb87-yu3X0mFyWs5rB3ANm4lsVSIu7RZd7XUEwfZXzugJ3CoJqxvRqiTGLpcXquEF24hK3OUH-0&amp;t=633177911400000000" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=Z7-pZSKJ6vCyr9H9S4r9baB7TLiJ-T4EoMOvlH-iQ5-icaO-vWMrjXk3EPctuPrcO5qwJSTaLQtHyguwle7Kty78exrSwHataIriNUEu1DnUkixv4pxU7Dknp_GDVHby5XcMA4lMwazAE0AFG9Cndu0An_gAGACGlYXsGUG9W6okCQsH0&amp;t=633177911400000000" type="text/javascript"></script>
<script src="../WebServices/Materials.asmx/js" type="text/javascript"></script>
<script src="../WebServices/MatGroups.asmx/js" type="text/javascript"></script>
<script src="../WebServices/Folders.asmx/js" type="text/javascript"></script>
<script type="text/javascript">
//<![CDATA[
function WebForm_OnSubmit() {
null;
return true;
}
//]]>
</script>

<div>

	<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="640EFF8C" />
</div>

<!--
The ScriptManager control is required for all ASP AJAX functionality - especially used by search pages.
It generates the javascript required.
Only one script manager is allowed on a page, and since this is the master page, this is the main script manager
However, a ScriptManagerProxy control can be used to set properties and settings on indivual pages.
For an example, see /WebControls/ucSearchResults.ascx
-->
<script type="text/javascript">
//<![CDATA[
Sys.WebForms.PageRequestManager._initialize('ctl00$ajxScriptManager', document.getElementById('aspnetForm'));
Sys.WebForms.PageRequestManager.getInstance()._updateControls([], [], [], 90);
//]]>
</script>


<table class="tabletight" style="width:100%; text-align:center;background-image: url(/images/gray.gif);">
  <tr >
    <td rowspan="2"><a href="/index.aspx"><img src="/images/logo1.gif" width="270" height="42" alt="MatWeb - Material Property Data"  /></a></td>

                
                <td style="text-align:right;vertical-align:middle; width:90%" ><span class='greeting'><span class='hiText'><a href ='/services/advertising.aspx'>Advertise with MatWeb!</a></span>&nbsp;&nbsp;&nbsp;&nbsp;</span></td>
                <td style="text-align:right;vertical-align:middle; padding-right:5px;" ><a href='/membership/regstart.aspx'><img src='/images/buttons/btnRegisterBSF.gif' width="62" height="20" alt="Register Now" /></a></td>

                
  </tr>
  <tr >
    <td  align="left" valign="bottom" colspan="2"><span class="tagline">
    
    Data sheets for over <span class="greeting">180,000</span>  metals, plastics, ceramics, and composites.</span></td>
  </tr>
  <tr style="background-image:url(/images/blue.gif); background-color:#000099; " >
    <td bgcolor="#000099"><a href="/index.aspx"><img src="/images/logo2.gif" width="270" height="23px" alt="MatWeb - Material Property Data" border="0" /></a></td>
    <td class="topnavig8" style=" background-color:#000099;vertical-align:middle; text-align:right; white-space:nowrap;" colspan="2" align="right" valign="middle">
        <a href="/index.aspx" class="topnavlink">HOME</a>&nbsp;&nbsp;&#8226;&nbsp;&nbsp;
                        <a href="/search/search.aspx" class="topnavlink">SEARCH</a>&nbsp;&nbsp;&#8226;&nbsp;&nbsp;
                        <a href="/tools/tools.aspx" class="topnavlink">TOOLS</a>&nbsp;&nbsp;&#8226;&nbsp;&nbsp;
                        <a href="/reference/suppliers.aspx" class="topnavlink">SUPPLIERS</a>&nbsp;&nbsp;&#8226;&nbsp;&nbsp;
                        <a href="/folders/ListFolders.aspx" class="topnavlink">FOLDERS</a>&nbsp;&nbsp;&#8226;&nbsp;&nbsp;
                        <a href="/services/services.aspx" class="topnavlink">ABOUT US</a>&nbsp;&nbsp;&#8226;&nbsp;&nbsp;
                        <a href="/reference/faq.aspx" class="topnavlink">FAQ</a>&nbsp;&nbsp;&#8226;&nbsp;&nbsp;
                        
                        <a href="/membership/login.aspx" class="topnavlink">LOG IN</a>
                        
                        &nbsp;
                        
                        &nbsp;
    </td>
  </tr>
</table>

<div id="divRecentMatls" class="tableborder tight" style=" display:none; position:absolute; top:95px; left:150px; width:400px; height:400px;" onmouseover="clearTimeout(tmrtmp);" onmouseout="tmrtmp=setTimeout('RecentMatls.Hide()', 800)">
        <table class="headerblock tabletight" style="vertical-align:top; font-size:13px; width:100%;"><tr>
                <td style="padding:3px;"> Recently Viewed Materials (most recent at top)</td>
                <td style="padding:3px;text-align:right;"><input type="image" src="/images/buttons/btnCloseWhite.gif" onclick="RecentMatls.Hide();return false;" style="cursor:pointer;" />&nbsp;</td>
        </tr></table>
        <div id="divRecentMatlsBody" style="height:375px; overflow:auto; background-color:White;">

                <table id="tblRecentMatls" class="tabledataformat tableloose" style="background-color:White;">
    
                <tr><td>
                <p />
                <a href="/membership/login.aspx">Login</a> to see your most recently viewed materials here.<p />
                Or if you don't have an account with us yet, then <a href="/membership/regstart.aspx">click here to register.</a>
                </td></tr>
                
                </table>

        </div>
</div>

<script type="text/javascript">

        var RecentMatls = new _RecentMatls();
        var tmrtmp;

        function _RecentMatls(){

                var divRecentMatls;
                var tblRecentMatls;
                var trRecentMatlsRowTemplate;
                var trRecentMatlsNoData;
                var DataIsLoaded=false;

                //Using prototype.js methods...
                divRecentMatls = $("divRecentMatls");
                tblRecentMatls = $("tblRecentMatls");
    

                divRecentMatls.style.display = "none";

                this.Toggle = function(){
                        //alert("Toggle()");
                        if(divRecentMatls.style.display == "block"){this.Hide();}
                        else{this.Show();}
                }

                this.Show = function(){
                        //alert("Show()");
                        divRecentMatls.style.display = "block";
                        matweb.toggleSelect(divRecentMatls,0);
                        if(!DataIsLoaded){
            
                        }
                }

                this.Hide = function(){
                        //alert("Hide()");
                        divRecentMatls.style.display = "none";
                        matweb.toggleSelect(divRecentMatls,1);
                }

    

        }//end class

</script>

<table class="tabletight t_ableborder t_ablegrid" style="width:100%;" >
  <tr>
   <td style="width:100%; height:27px; white-space:nowrap; text-align:left; vertical-align:middle;" class="tagline" >
      <span class="navlink"><b>&nbsp;&nbsp;Searches:</b></span>
      &nbsp;&nbsp;<a href="/search/AdvancedSearch.aspx" class="navlink"><span class="nav"><font color="#CC0000">Advanced</font></span></a>
      &nbsp;|&nbsp;<a href="/search/MaterialGroupSearch.aspx" class="navlink"><span class="nav">Category</span></a>
      &nbsp;|&nbsp;<a href="/search/PropertySearch.aspx" class="navlink"><span class="nav">Property</span></a>
      &nbsp;|&nbsp;<a href="/search/CompositionSearch.aspx" class="navlink"><span class="nav">Metals</span></a>
      &nbsp;|&nbsp;<a href="/search/SearchTradeName.aspx" class="navlink"><span class="nav">Trade Name</span></a>
      &nbsp;|&nbsp;<a href="/search/SearchManufacturerName.aspx" class="navlink"><span class="nav">Manufacturer</span></a>
                &nbsp;|&nbsp;<a href="javascript:RecentMatls.Toggle();" id="ctl00_lnkRecentMatls" class="navlink"><span class="nav"><font color="#CC0000">Recently Viewed Materials</font></span></a>
    </td>

    <td style="text-align:right; vertical-align:bottom; padding-right:5px;">

      <table class="tabletight t_ableborder" ><tr>
        <td style=" white-space:nowrap; vertical-align:bottom; " >
                                        &nbsp;&nbsp;
                                        <input name="ctl00$txtQuickText" type="text" id="ctl00_txtQuickText" style="width:100px;" maxlength="40" class="quicksearchinput" onkeydown="txtQuickText_OnKeyDown(event);" />
                                        <input type="image" id="btnQuickTextSearch" style="vertical-align:top; width:60px; height:25px;" src="/images/buttons/btnSearch.gif" alt="Search" onclick="return btnQuickTextSearch_Click()"  />
                                        <script type="text/javascript">

                                                //SETUP THE AUTOPOST ON ENTER KEY EVENT FOR txtQuickText FIELD
                                                //document.getElementById("ctl00_txtQuickText").onkeydown=txtQuickText_OnKeyDown;

                                                function txtQuickText_OnKeyDown(evt){
                                                        //SUBMIT THE FORM AS IF THE BUTTON WAS PUSHED
                                                        //var UniqueID = "";
                                                        var txtQuickText = document.getElementById('ctl00_txtQuickText');
                                                        if(matweb.IsEnterKey(evt))
                                                                if(checkSearchText(txtQuickText))
                                                                        //matweb.DoPostBack(UniqueID, "");
                                                                        window.location.href="/search/QuickText.aspx?SearchText=" + escape(txtQuickText.value);
                                                        return false;
                                                }

                                                function btnQuickTextSearch_Click(){
                                                        var txtQuickText = document.getElementById('ctl00_txtQuickText');
                                                        if(checkSearchText(txtQuickText))
                                                                window.location.href="/search/QuickText.aspx?SearchText=" + escape(txtQuickText.value);
                                                        return false;
                                                }

                                                function checkSearchText(searchbox){
                                                        searchbox.value = trim(searchbox.value);
                                                        if(searchbox.value == ''){
                                                                //alert('Please enter something to search for');
                                                                matweb.alert('Please enter something to search for', 'Search Error');
                                                                //searchbox.focus();
                                                                return false;
                                                        }
                                                        return true;
                                                }

                                                function trim(s)  { return ltrim(rtrim(s)) }
                                                function ltrim(s) { return s.replace(/^\s+/g, "") }
                                                function rtrim(s) { return s.replace(/\s+$/g, "") }

                                        </script>
                                </td>
                                </tr>
                        </table>

    </td>
  </tr>
  <tr style="background-image:url(/images/shad.gif);height:7px;" ><td colspan="2"></td></tr>
</table>

<!-- ====================================== MAIN CONTENT ============================================= -->
<div style="vertical-align:top; padding-left:10px; padding-right:10px;" >





	<center><a href="/clickthrough.aspx?addataid=1351"><img src="/images/assets/metalmen-Nickel.png" border=0 alt="Metalmen Sales" width = "728" height = "90"></a>
</center>
	
	<h2><a href="#help"><img src="/images/buttons/iconHelp.gif" alt="" style="vertical-align:bottom;" /></a>Material Category Search</h2>
	
	

<div id="ctl00_ContentMain_ucPopupMessage1_divPopupMessage" class="divPopupAlert" style="display:none;width:250px;">
	<table >
		<tr id="ctl00_ContentMain_ucPopupMessage1_trTitleRow" class="divPopupAlert_TitleRow" style="cursor:move;">
	<th><span id="ctl00_ContentMain_ucPopupMessage1_lblTitle">User Input Problem</span></th>
</tr>

		<tr class="divPopupAlert_ContentRow" ><td style="" ><span id="ctl00_ContentMain_ucPopupMessage1_lblMessage"></span></td></tr>
		<tr class="divPopupAlert_ButtonRow"><td><input name="ctl00$ContentMain$ucPopupMessage1$btnOK" type="button" id="ctl00_ContentMain_ucPopupMessage1_btnOK" value="OK" /></td></tr>
	</table>
</div>

<input type="hidden" name="ctl00$ContentMain$ucPopupMessage1$hndPopupControl" id="ctl00_ContentMain_ucPopupMessage1_hndPopupControl" />


	<table class="" style="width:100%;">
	<tr>
		<td style=" vertical-align:top; width:300px;" >
			<strong>Find a Material Category:</strong>
			

<input name="ctl00$ContentMain$UcMatGroupFinder1$txtSearchText" type="text" value="Nickel" id="ctl00_ContentMain_UcMatGroupFinder1_txtSearchText" style="Width:304px;" /><br />
<select name="ctl00$ContentMain$UcMatGroupFinder1$selectCategoryList" id="ctl00_ContentMain_UcMatGroupFinder1_selectCategoryList" style="position:absolute;display:none;Width:308px;" size="18">
</select>

<input type="hidden" name="ctl00$ContentMain$UcMatGroupFinder1$TextBoxWatermarkExtender2_ClientState" id="ctl00_ContentMain_UcMatGroupFinder1_TextBoxWatermarkExtender2_ClientState" />

<script type="text/javascript">

var ucMatGroupFinderManager = new _ucMatGroupFinderManager("ctl00_ContentMain_UcMatGroupFinder1_txtSearchText","ctl00_ContentMain_UcMatGroupFinder1_selectCategoryList");

function _ucMatGroupFinderManager(txtSearchTextID,selectCategoryListID){

	var txtSearchText = document.getElementById(txtSearchTextID);
	var selectCategoryList = document.getElementById(selectCategoryListID);
	var showtime;
	var blurtime;
	var MinTextLength = 4;
	var KeyStrokeActionDelay = 650;
	var FadeInOutDelay = 0.2;
	var LastUsedText;
	
	//The init function is currently run at the bottom of this class
	this.init=function(){

		txtSearchText.onfocus = txtSearchText_onfocus;
		txtSearchText.onkeyup = txtSearchText_checktext;
		txtSearchText.onblur = selectCategoryList_startblur;
		
		selectCategoryList.onfocus = selectCategoryList_clearblur;
		selectCategoryList.onchange = selectCategoryList_onchange;
		selectCategoryList.onblur = selectCategoryList_startblur;

	}

	txtSearchText_onfocus = function(){
		if(txtSearchText.value == "Type at least 4 characters here...")
			txtSearchText.value = "";
		selectCategoryList_clearblur();
		txtSearchText_checktext();
	}

	function txtSearchText_checktext(){
		searchText = txtSearchText.value;
		//alert(searchText);
		clearTimeout(showtime);
		if(searchText.length >= MinTextLength){
			showtime = setTimeout(function(){
				selectCategoryList.style.display = "block";
				if(txtSearchText.value!=LastUsedText){
					LastUsedText = searchText;
					aci.matweb.WebServices.MatGroups.FindMatGroups(searchText,DisplayCatData,matweb.WebServiceErrorHandler,"txtSearchText_checktext");
				}
				//AjaxControlToolkit.Animation.FadeInAnimation.play(selectCategoryList, FadeInOutDelay, 1, 25, 0, 1, true);
			},KeyStrokeActionDelay);
		}
		else{
			LastUsedText = "";
			showtime = setTimeout(function(){
				selectCategoryList.options.length = 0;
				selectCategoryList.style.display = "none";
				//AjaxControlToolkit.Animation.FadeOutAnimation.play(selectCategoryList,FadeInOutDelay);
				//AjaxControlToolkit.Animation.FadeOutAnimation.play(selectCategoryList,FadeInOutDelay, 25, 0, 1, true);
			},KeyStrokeActionDelay);
		}
	}

	function selectCategoryList_clearblur(){
		clearTimeout(blurtime);
	}

	function selectCategoryList_onchange(){
		var MatGroupID = selectCategoryList.options[selectCategoryList.selectedIndex].value;
		var MatGroupText = selectCategoryList.options[selectCategoryList.selectedIndex].text;
		//alert("Selected MatGroupID: " + MatGroupID);
		//alert("Selected MatGroupText: " + MatGroupText);
		//When no data is found, we have a "No Categories Contain Text..." message, but the MatGroupID is set to 0, so we can ignore if MatGroupID == 0.
		if(MatGroupID > 0){
			ucMatGroupFinder1_OnSelected(MatGroupID,MatGroupText)
		}
		selectCategoryList_startblur();
		//txtSearchText.value = "";
	}
	
	function selectCategoryList_startblur(){
		//alert("selectCategoryList_onblur()");
		clearTimeout(blurtime);
		blurtime = setTimeout(function(){
			selectCategoryList.style.display = "none";
			//AjaxControlToolkit.Animation.FadeOutAnimation.play(selectCategoryList,FadeInOutDelay, 25, 0, 1, true);
		},KeyStrokeActionDelay);
	}
	
	function DisplayCatData(MatGroupData){
		//alert("GetCatData()");
		//alert("vwMaterialGroupList: " + vwMaterialGroupList);
		var MatGroupID,GroupName, MatCount;
		selectCategoryList.options.length = 0;//clear any existing options....
		if(MatGroupData.length==0){
				//alert(vwMaterialGroupList[i].GroupName);
				MatGroupID = 0;
				GroupName = "No categories contain the text: " + txtSearchText.value
				selectCategoryList.options[0] = new Option(GroupName,MatGroupID);					
		}
		else{
			for(i=0;i<MatGroupData.length;i++){
				//alert(vwMaterialGroupList[i].GroupName);
				MatCount = MatGroupData[i].MatCount;
				MatGroupID = MatGroupData[i].MatGroupID;
				GroupName = MatGroupData[i].GroupName + " (" + MatCount + " matls)";
				selectCategoryList.options[i] = new Option(GroupName,MatGroupID);					
			}
		}
	}
	
	this.init();

}//end class
	
</script>



			<strong>Select a Material Category:</strong>
			
			<div class="tableborder" style=" height:225px; width:300px; overflow:scroll; ">
			<a href="#ctl00_ContentMain_ucMatGroupTree_msTreeView_SkipLink"><img alt="Skip Navigation Links." src="/WebResource.axd?d=vAkieGj3ugBpu6CKp7dqvCP5iHLzPbUd2XME-h1vQcC6d5hLg7Vc1kNVzB8A45Ui3K9XZ9m3-E2pxsaJQYh2uMkAul41&amp;t=638313619312541215" width="0" height="0" style="border-width:0px;" /></a><div id="ctl00_ContentMain_ucMatGroupTree_msTreeView">
	<table cellpadding="0" cellspacing="0" style="border-width:0;">
		<tr>
			<td><a id="ctl00_ContentMain_ucMatGroupTree_msTreeViewn0" href="javascript:TreeView_PopulateNode(ctl00_ContentMain_ucMatGroupTree_msTreeView_Data,0,document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewn0'),document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewt0'),null,'r','Carbon (866 matls)','283','f','','f')"><img src="/WebResource.axd?d=6v5qM6VKQYD3XNwwXEy7Tix7dzVdqDJ8QTRIlUPrzXQqNsXQgUwIfmn34ZzRORd_IZj0bF4vkZzID1HuSFE5uKyLgaPYeAYE8VT8HYfb3Rk6zsDw0&amp;t=638313619312541215" alt="Expand Carbon (866 matls)" style="border-width:0;" /></a></td><td class="ctl00_ContentMain_ucMatGroupTree_msTreeView_2"><a class="ctl00_ContentMain_ucMatGroupTree_msTreeView_0 ctl00_ContentMain_ucMatGroupTree_msTreeView_1" href="javascript:ucMatGroupTree_OnNodeClick('Carbon (866 matls)','283')" id="ctl00_ContentMain_ucMatGroupTree_msTreeViewt0">Carbon (866 matls)</a></td>
		</tr><tr style="height:0px;">
			<td></td>
		</tr>
	</table><table cellpadding="0" cellspacing="0" style="border-width:0;">
		<tr style="height:0px;">
			<td></td>
		</tr><tr>
			<td><a id="ctl00_ContentMain_ucMatGroupTree_msTreeViewn1" href="javascript:TreeView_PopulateNode(ctl00_ContentMain_ucMatGroupTree_msTreeView_Data,1,document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewn1'),document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewt1'),null,'t','Ceramic (10004 matls)','11','f','','f')"><img src="/WebResource.axd?d=TinC6igX1vPDgWh7DstWwBTHTyH4uuybDU4NW47jJD06wHdAO3YyJC_eKK1Zn5oIbwqRo4Cri8YwhA82G-LD0p14yEF1EfoIZvKleYMIuQ2K1hkE0&amp;t=638313619312541215" alt="Expand Ceramic (10004 matls)" style="border-width:0;" /></a></td><td class="ctl00_ContentMain_ucMatGroupTree_msTreeView_2"><a class="ctl00_ContentMain_ucMatGroupTree_msTreeView_0 ctl00_ContentMain_ucMatGroupTree_msTreeView_1" href="javascript:ucMatGroupTree_OnNodeClick('Ceramic (10004 matls)','11')" id="ctl00_ContentMain_ucMatGroupTree_msTreeViewt1">Ceramic (10004 matls)</a></td>
		</tr><tr style="height:0px;">
			<td></td>
		</tr>
	</table><table cellpadding="0" cellspacing="0" style="border-width:0;">
		<tr style="height:0px;">
			<td></td>
		</tr><tr>
			<td><a id="ctl00_ContentMain_ucMatGroupTree_msTreeViewn2" href="javascript:TreeView_PopulateNode(ctl00_ContentMain_ucMatGroupTree_msTreeView_Data,2,document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewn2'),document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewt2'),null,'t','Fluid (7562 matls)','5','f','','f')"><img src="/WebResource.axd?d=TinC6igX1vPDgWh7DstWwBTHTyH4uuybDU4NW47jJD06wHdAO3YyJC_eKK1Zn5oIbwqRo4Cri8YwhA82G-LD0p14yEF1EfoIZvKleYMIuQ2K1hkE0&amp;t=638313619312541215" alt="Expand Fluid (7562 matls)" style="border-width:0;" /></a></td><td class="ctl00_ContentMain_ucMatGroupTree_msTreeView_2"><a class="ctl00_ContentMain_ucMatGroupTree_msTreeView_0 ctl00_ContentMain_ucMatGroupTree_msTreeView_1" href="javascript:ucMatGroupTree_OnNodeClick('Fluid (7562 matls)','5')" id="ctl00_ContentMain_ucMatGroupTree_msTreeViewt2">Fluid (7562 matls)</a></td>
		</tr><tr style="height:0px;">
			<td></td>
		</tr>
	</table><table cellpadding="0" cellspacing="0" style="border-width:0;">
		<tr style="height:0px;">
			<td></td>
		</tr><tr>
			<td><a id="ctl00_ContentMain_ucMatGroupTree_msTreeViewn3" href="javascript:TreeView_PopulateNode(ctl00_ContentMain_ucMatGroupTree_msTreeView_Data,3,document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewn3'),document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewt3'),null,'t','Metal (17052 matls)','9','f','','f')"><img src="/WebResource.axd?d=TinC6igX1vPDgWh7DstWwBTHTyH4uuybDU4NW47jJD06wHdAO3YyJC_eKK1Zn5oIbwqRo4Cri8YwhA82G-LD0p14yEF1EfoIZvKleYMIuQ2K1hkE0&amp;t=638313619312541215" alt="Expand Metal (17052 matls)" style="border-width:0;" /></a></td><td class="ctl00_ContentMain_ucMatGroupTree_msTreeView_2"><a class="ctl00_ContentMain_ucMatGroupTree_msTreeView_0 ctl00_ContentMain_ucMatGroupTree_msTreeView_1" href="javascript:ucMatGroupTree_OnNodeClick('Metal (17052 matls)','9')" id="ctl00_ContentMain_ucMatGroupTree_msTreeViewt3">Metal (17052 matls)</a></td>
		</tr><tr style="height:0px;">
			<td></td>
		</tr>
	</table><table cellpadding="0" cellspacing="0" style="border-width:0;">
		<tr style="height:0px;">
			<td></td>
		</tr><tr>
			<td><a id="ctl00_ContentMain_ucMatGroupTree_msTreeViewn4" href="javascript:TreeView_PopulateNode(ctl00_ContentMain_ucMatGroupTree_msTreeView_Data,4,document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewn4'),document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewt4'),null,'t','Other Engineering Material (8063 matls)','285','f','','f')"><img src="/WebResource.axd?d=TinC6igX1vPDgWh7DstWwBTHTyH4uuybDU4NW47jJD06wHdAO3YyJC_eKK1Zn5oIbwqRo4Cri8YwhA82G-LD0p14yEF1EfoIZvKleYMIuQ2K1hkE0&amp;t=638313619312541215" alt="Expand Other Engineering Material (8063 matls)" style="border-width:0;" /></a></td><td class="ctl00_ContentMain_ucMatGroupTree_msTreeView_2"><a class="ctl00_ContentMain_ucMatGroupTree_msTreeView_0 ctl00_ContentMain_ucMatGroupTree_msTreeView_1" href="javascript:ucMatGroupTree_OnNodeClick('Other Engineering Material (8063 matls)','285')" id="ctl00_ContentMain_ucMatGroupTree_msTreeViewt4">Other Engineering Material (8063 matls)</a></td>
		</tr><tr style="height:0px;">
			<td></td>
		</tr>
	</table><table cellpadding="0" cellspacing="0" style="border-width:0;">
		<tr style="height:0px;">
			<td></td>
		</tr><tr>
			<td><a id="ctl00_ContentMain_ucMatGroupTree_msTreeViewn5" href="javascript:TreeView_PopulateNode(ctl00_ContentMain_ucMatGroupTree_msTreeView_Data,5,document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewn5'),document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewt5'),null,'t','Polymer (97635 matls)','10','f','','f')"><img src="/WebResource.axd?d=TinC6igX1vPDgWh7DstWwBTHTyH4uuybDU4NW47jJD06wHdAO3YyJC_eKK1Zn5oIbwqRo4Cri8YwhA82G-LD0p14yEF1EfoIZvKleYMIuQ2K1hkE0&amp;t=638313619312541215" alt="Expand Polymer (97635 matls)" style="border-width:0;" /></a></td><td class="ctl00_ContentMain_ucMatGroupTree_msTreeView_2"><a class="ctl00_ContentMain_ucMatGroupTree_msTreeView_0 ctl00_ContentMain_ucMatGroupTree_msTreeView_1" href="javascript:ucMatGroupTree_OnNodeClick('Polymer (97635 matls)','10')" id="ctl00_ContentMain_ucMatGroupTree_msTreeViewt5">Polymer (97635 matls)</a></td>
		</tr><tr style="height:0px;">
			<td></td>
		</tr>
	</table><table cellpadding="0" cellspacing="0" style="border-width:0;">
		<tr style="height:0px;">
			<td></td>
		</tr><tr>
			<td><img src="/WebResource.axd?d=7_V5n1KYSQsj6PoSqgIxb0egBsAa91jSXPkbmM6FaiF14-JsNnpUDnfSdxZcvfd0OQCLDtC24pMIXigO-41cL2rsq-0xea-SSG7yZUyRcliFFqe70&amp;t=638313619312541215" alt="" /></td><td class="ctl00_ContentMain_ucMatGroupTree_msTreeView_2"><a class="ctl00_ContentMain_ucMatGroupTree_msTreeView_0 ctl00_ContentMain_ucMatGroupTree_msTreeView_1" href="javascript:ucMatGroupTree_OnNodeClick('Pure Element (507 matls)','184')" id="ctl00_ContentMain_ucMatGroupTree_msTreeViewt6">Pure Element (507 matls)</a></td>
		</tr><tr style="height:0px;">
			<td></td>
		</tr>
	</table><table cellpadding="0" cellspacing="0" style="border-width:0;">
		<tr style="height:0px;">
			<td></td>
		</tr><tr>
			<td><a id="ctl00_ContentMain_ucMatGroupTree_msTreeViewn7" href="javascript:TreeView_PopulateNode(ctl00_ContentMain_ucMatGroupTree_msTreeView_Data,7,document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewn7'),document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewt7'),null,'l','Wood and Natural Products (398 matls)','277','f','','t')"><img src="/WebResource.axd?d=vUBrM2nNhECNbFO0AJKq52p2zWrY5XG8nWgCDL4AfsrexStb0HREDV6GHMvdkAGDInxYtTyWXuxFUIiQv3JX2fScVRwQsrVv5srHrjOIeC3ayAG20&amp;t=638313619312541215" alt="Expand Wood and Natural Products (398 matls)" style="border-width:0;" /></a></td><td class="ctl00_ContentMain_ucMatGroupTree_msTreeView_2"><a class="ctl00_ContentMain_ucMatGroupTree_msTreeView_0 ctl00_ContentMain_ucMatGroupTree_msTreeView_1" href="javascript:ucMatGroupTree_OnNodeClick('Wood and Natural Products (398 matls)','277')" id="ctl00_ContentMain_ucMatGroupTree_msTreeViewt7">Wood and Natural Products (398 matls)</a></td>
		</tr><tr style="height:0px;">
			<td></td>
		</tr>
	</table>
</div><a id="ctl00_ContentMain_ucMatGroupTree_msTreeView_SkipLink"></a>
<span id="ctl00_ContentMain_ucMatGroupTree_lblDebug"></span>

			</div>
			
			<strong>Selected Material Category: </strong>
			<div id="ctl00_ContentMain_divMatGroupName" class="selectedItem" style="width:306px;">Nickel Alloy (1210 matls)</div>
			<input name="ctl00$ContentMain$txtMatGroupID" type="hidden" id="ctl00_ContentMain_txtMatGroupID" value="193" />
			<input name="ctl00$ContentMain$txtMatGroupText" type="hidden" id="ctl00_ContentMain_txtMatGroupText" value="Nickel Alloy (1210 matls)" />
			
			

			<input type="image" name="ctl00$ContentMain$btnSubmit" id="ctl00_ContentMain_btnSubmit" src="../images/buttons/btnFind.gif" alt="Find" style="border-width:0px;" /> <!-- OnClientClick="return validate()" -->
			<input type="image" name="ctl00$ContentMain$btnReset" id="ctl00_ContentMain_btnReset" title="Clear Search" src="/images/buttons/btnReset.gif" alt="Clear Search" style="border-width:0px;" />

			<span id="ctl00_ContentMain_lblDebug"></span>

		</td>
		<td>&nbsp;</td>
		<td style=" vertical-align:top; width:80%;" >
			<strong>Search Results</strong>
			<div class="tableborder" id="divSearchResults" style="padding:5px;" >
				
				<div id="ctl00_ContentMain_pnlSearchResults">
	
					There are <span id="ctl00_ContentMain_lblMatlCount">1245</span> materials in the category 
					<span id="ctl00_ContentMain_lblMaterialGroupName" class="selectedItem">Nickel Alloy</span>.
					If your material is not listed, please refer to our <a href="/help/strategy.aspx">search strategy</a> page for assistance in limiting your search.
				
</div>
			</div>
			<p />




<div id="ctl00_ContentMain_UcSearchResults1_pnlResultsFound" class="tableborder">
	

	<table style="background-color:Silver; width:100%;" class="" >
		<tr>
			<td><strong>Found <span id="ctl00_ContentMain_UcSearchResults1_lblResultCount">1245</span> Results</strong> -- 
				Page 
				<select name="ctl00$ContentMain$UcSearchResults1$drpPageSelect1" onchange="javascript:setTimeout('__doPostBack(\'ctl00$ContentMain$UcSearchResults1$drpPageSelect1\',\'\')', 0)" id="ctl00_ContentMain_UcSearchResults1_drpPageSelect1">
		<option value="1">1</option>
		<option value="2">2</option>
		<option selected="selected" value="3">3</option>
		<option value="4">4</option>
		<option value="5">5</option>

	</select>
				of <span id="ctl00_ContentMain_UcSearchResults1_lblPageTotal">7</span> 
				-- 
				<a id="ctl00_ContentMain_UcSearchResults1_lnkPrevPage" href="javascript:__doPostBack('ctl00$ContentMain$UcSearchResults1$lnkPrevPage','')" style="color:Black;">[Prev Page]</a>
				<a id="ctl00_ContentMain_UcSearchResults1_lnkNextPage" href="javascript:__doPostBack('ctl00$ContentMain$UcSearchResults1$lnkNextPage','')" style="color:Black;">[Next Page]</a>
				--&nbsp;			
				view
				<select name="ctl00$ContentMain$UcSearchResults1$drpPageSize1" onchange="javascript:setTimeout('__doPostBack(\'ctl00$ContentMain$UcSearchResults1$drpPageSize1\',\'\')', 0)" id="ctl00_ContentMain_UcSearchResults1_drpPageSize1">
		<option value="25">25</option>
		<option value="50">50</option>
		<option value="75">75</option>
		<option value="100">100</option>
		<option value="150">150</option>
		<option selected="selected" value="200">200</option>

	</select> per page
			</td>
		</tr>
		
	</table>
	
	
	
	
	<table class="t_ablegrid" style="width:100%;">
		<tr style="font-size:9px;">
			<td style="font-size:9px;">Use Folder</td>
			<td style="font-size:9px;">Contains</td>
			<td style="font-size:9px;">&nbsp;</td>
			
			<td style="font-size:9px;"><div id="divFolderName" style="display:none;">New Folder Name</div></td>
			<td style="font-size:9px;"></td>
			<td style="width:90%"></td>
		</tr>
		<tr style="vertical-align:top;">
			<td style="vertical-align:top;">
				<select name="ctl00$ContentMain$UcSearchResults1$drpFolderList" id="ctl00_ContentMain_UcSearchResults1_drpFolderList" onchange="SetSelectedFolder()" style="width:125px;">
		<option selected="selected" value="0">My Folder</option>

	</select>
			</td>
			<td style="vertical-align:top;">
				<strong><input name="ctl00$ContentMain$UcSearchResults1$txtFolderMatCount" type="text" id="ctl00_ContentMain_UcSearchResults1_txtFolderMatCount" readonly="readonly" style="border:none 0 white; background-color:White; color:Black; width: 35px; font-weight:bold;" value="0/0" /></strong>
			</td>
			<td>
				
				<input type="image" id="btnCompareFolders" src="/images/buttons/btnCompareMatls.gif" alt="Compare Checked Materials" onclick="CompareMaterials();return false;" />
			</td>
			
			<td style="white-space:nowrap;vertical-align:top;">
			
				<table id="tblFolderName" class="tabletight" style="display:none;"><tr><td><input type="text" id="txtFolderName2" style="display:inline; width:100px;" /><br /></td>
				<td><input type="image" id="btnApplyFolderName2" src="/images/buttons/btnSaveChanges.gif" style="display:inline;" onclick="ApplyFolderName();return false;" />
				<input type="image" id="btnHideFolderNameControls2" src="/images/buttons/btnCancel.gif" style="display:inline;" onclick="HideFolderNameControls();return false;" /></td>
				</tr></table>
				
			</td>
			<td style="white-space:nowrap;">
				<div class="usermessage" id="divUserMessage"></div>
			</td>
		</tr>
	</table>

	<table class="tabledataformat t_ablegrid" id="tblResults" style="table-layout:auto;"  >

		<tr class="">

			
		
			<th style="width:65px; white-space:nowrap;">Select</th>

			<th style="width:10px;">
				<!--Discontinued and Found Via Interpolated Indicators -->
			</th>
			
			<th style="width:auto;">
				<a id="ctl00_ContentMain_UcSearchResults1_lnkSortMatName" disabled="disabled" title="Click to Sort By Material Name">Material Name</a>
				<br />
				
				
			</th>
		
			
			
			

			

			

			

			

			

			

			

			
		</tr>

		<!-- this is where the search results are actually loaded.  See code behind -->		
		<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_16855"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;401</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_16855' href="/search/DataSheet.aspx?MatGUID=90f48295bce04501bedd4dd515cc19c2" >Haynes Hastelloy® N, 1.6 mm sheet, heat treated at 1175°C, welded and tested as welded</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_16859"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;402</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_16859' href="/search/DataSheet.aspx?MatGUID=1ee79cbff83c4edcaf5782b507c8c4c9" >Haynes Hastelloy® N, 1.14 mm sheet, heat treated at 1149°C (2100°F)</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_16865"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;403</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_16865' href="/search/DataSheet.aspx?MatGUID=da8279f62c4a445080201a1a4b0abf44" >Haynes Hastelloy® N, 1.14 mm sheet, aged 10000 hours at 538°C (1000°F)</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_16866"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;404</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_16866' href="/search/DataSheet.aspx?MatGUID=883b3e5c0cb549aabda6b4ff4430b232" >Haynes Hastelloy® N, 1.14 mm sheet, aged 10000 hours at 593°C (1100°F)</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_16867"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;405</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_16867' href="/search/DataSheet.aspx?MatGUID=385f54d2657c42a1a80a2bbae7b450e4" >Haynes Hastelloy® N, 1.14 mm sheet, aged 10000 hours at 649°C (1200°F)</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_16868"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;406</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_16868' href="/search/DataSheet.aspx?MatGUID=d829266e4c314ce19a8095da6390c5f9" >Haynes Hastelloy® N, 1.14 mm sheet, aged 10000 hours at 704°C (1300°F)</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_16869"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;407</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_16869' href="/search/DataSheet.aspx?MatGUID=c1b4b85f87b54851884f99c7eda6b5bf" >Haynes Hastelloy® N, 1.14 mm sheet, aged 10000 hours at 760°C (1400°F)</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_16875"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;408</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_16875' href="/search/DataSheet.aspx?MatGUID=051747faa6e34dddb4906430946dcc4a" >Haynes Hastelloy® N, weld metal as welded</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_16877"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;409</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_16877' href="/search/DataSheet.aspx?MatGUID=1c956fffe58e4181b10cc505ebceb324" >Haynes Hastelloy® N, weld metal, aged 500 hours at 649°C (1200°F)</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_16879"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;410</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_16879' href="/search/DataSheet.aspx?MatGUID=1e96b9fbab53481a87685d9bcdeeb16a" >Haynes Hastelloy® S alloy, flat products</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_16898"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;411</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_16898' href="/search/DataSheet.aspx?MatGUID=06470746c8534b4cbf36f69393b46999" >Haynes Hastelloy® S alloy, flat products, solution heat-treated at 538°C (1000°F), aged 1000 hours</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_16899"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;412</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_16899' href="/search/DataSheet.aspx?MatGUID=057207bd7b9d4fc2b794bcbd658a39f3" >Haynes Hastelloy® S alloy, flat products, solution heat-treated at 538°C (1000°F), aged 4000 hours</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_16900"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;413</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_16900' href="/search/DataSheet.aspx?MatGUID=916cd3b3feae4f1390056d7e5433394d" >Haynes Hastelloy® S alloy, flat products, solution heat-treated at 538°C (1000°F), aged 8000 hours</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_16901"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;414</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_16901' href="/search/DataSheet.aspx?MatGUID=ee8d9cfd0d924a6f9f3182951c24625d" >Haynes Hastelloy® S alloy, flat products, solution heat-treated at 538°C (1000°F), aged 16000 hours</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_16902"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;415</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_16902' href="/search/DataSheet.aspx?MatGUID=da5e5669ea17437d8aa536c4c85b98b8" >Haynes Hastelloy® S alloy, flat products, solution heat-treated at 649°C (1200°F), aged 1000 hours</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_16903"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;416</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_16903' href="/search/DataSheet.aspx?MatGUID=b9f116316660407d9312011531621072" >Haynes Hastelloy® S alloy, flat products, solution heat-treated at 649°C (1200°F), aged 4000 hours</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_16904"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;417</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_16904' href="/search/DataSheet.aspx?MatGUID=7a66201b155a4886837822972ee933f8" >Haynes Hastelloy® S alloy, flat products, solution heat-treated at 649°C (1200°F), aged 8000 hours</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_16905"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;418</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_16905' href="/search/DataSheet.aspx?MatGUID=40e97ed5282f4d2280f5709de4837cb1" >Haynes Hastelloy® S alloy, flat products, solution heat-treated at 649°C (1200°F), aged 16000 hours</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_16906"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;419</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_16906' href="/search/DataSheet.aspx?MatGUID=2d46596965e240ae9e54b3ff5b7e0972" >Haynes Hastelloy® S alloy, flat products, solution heat-treated at 760°C (1400°F), aged 1000 hours</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_16907"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;420</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_16907' href="/search/DataSheet.aspx?MatGUID=f15b650d7a2649aa80ff5659dfdbc465" >Haynes Hastelloy® S alloy, flat products, solution heat-treated at 760°C (1400°F), aged 4000 hours</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_16908"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;421</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_16908' href="/search/DataSheet.aspx?MatGUID=9c0615ec95cb497d98025d2c5bc2c1ef" >Haynes Hastelloy® S alloy, flat products, solution heat-treated at 760°C (1400°F), aged 8000 hours</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_16909"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;422</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_16909' href="/search/DataSheet.aspx?MatGUID=69367753f7c14d8ab11853c7a39a5260" >Haynes Hastelloy® S alloy, flat products, solution heat-treated at 760°C (1400°F), aged 16000 hours</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_16911"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;423</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_16911' href="/search/DataSheet.aspx?MatGUID=ace54ed309d94b999930929f3fd55378" >Haynes Hastelloy® S alloy, plate 12.7 mm (1/2 in.) thick, solution heat-treated at 427°C (800°F), aged 1000 hours</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_16912"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;424</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_16912' href="/search/DataSheet.aspx?MatGUID=4a3c5da8cb8d44839cf4eed73b5189ba" >Haynes Hastelloy® S alloy, plate 12.7 mm (1/2 in.) thick, solution heat-treated at 427°C (800°F), aged 4000 hours</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_16913"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;425</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_16913' href="/search/DataSheet.aspx?MatGUID=40e5a45e6e8c4a1aaf92b64d7738c42f" >Haynes Hastelloy® S alloy, plate 12.7 mm (1/2 in.) thick, solution heat-treated at 427°C (800°F), aged 8000 hours</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_16914"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;426</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_16914' href="/search/DataSheet.aspx?MatGUID=ff68652c85bd4f70b0011ecc668e4d7f" >Haynes Hastelloy® S alloy, plate 12.7 mm (1/2 in.) thick, solution heat-treated at 427°C (800°F), aged 16000 hours</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_16927"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;427</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_16927' href="/search/DataSheet.aspx?MatGUID=8b79f1fe6dcd438891eadff2952f8995" >Haynes Hastelloy® S alloy, plate 12.7 mm (1/2 in.) thick, solution heat-treated at 871°C (1600°F), aged 1000 hours</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_16928"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;428</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_16928' href="/search/DataSheet.aspx?MatGUID=16e79c483bed4f558bfe5219a452011a" >Haynes Hastelloy® S alloy, plate 12.7 mm (1/2 in.) thick, solution heat-treated at 871°C (1600°F), aged 4000 hours</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_16929"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;429</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_16929' href="/search/DataSheet.aspx?MatGUID=6d573aa11c464963ad27a1f5bd2272cb" >Haynes Hastelloy® S alloy, plate 12.7 mm (1/2 in.) thick, solution heat-treated at 871°C (1600°F), aged 8000 hours</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_16930"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;430</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_16930' href="/search/DataSheet.aspx?MatGUID=8906624d1e0840319c92aeeedb58f516" >Haynes Hastelloy® S alloy, plate 12.7 mm (1/2 in.) thick, solution heat-treated at 871°C (1600°F), aged 16000 hours</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_16931"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;431</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_16931' href="/search/DataSheet.aspx?MatGUID=d8521aa5e8a84f39ad96fe6c4cf2f277" >Haynes Hastelloy® S alloy, gas tungsten arc welded plate 12.7 mm (1/2 in.) thick</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_16932"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;432</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_16932' href="/search/DataSheet.aspx?MatGUID=bda7960a8ffa441cb29b12df9acf4bd6" >Haynes Hastelloy® S alloy, gas tungsten arc welded plate 12.7 mm (1/2 in.) thick, aged 1000 hours</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_16933"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;433</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_16933' href="/search/DataSheet.aspx?MatGUID=286122e938fb4e20ae27096405bb133d" >Haynes Hastelloy® S alloy, gas tungsten arc welded plate 12.7 mm (1/2 in.) thick, aged 4000 hours</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_16934"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;434</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_16934' href="/search/DataSheet.aspx?MatGUID=34cda926f4ca47ee89fc075fcc6ec487" >Haynes Hastelloy® S alloy, gas tungsten arc welded plate 12.7 mm (1/2 in.) thick, aged at 649°C (1200°F) for 8000 hours</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_16935"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;435</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_16935' href="/search/DataSheet.aspx?MatGUID=2ddc728f5395432382405ba9c2ff831d" >Haynes Hastelloy® S alloy, gas tungsten arc welded plate 12.7 mm (1/2 in.) thick, aged 16000 hours</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_16936"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;436</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_16936' href="/search/DataSheet.aspx?MatGUID=70c868c24b2c4955a6d747519a278342" >Haynes Hastelloy® S alloy, gas tungsten arc welded</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_16937"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;437</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_16937' href="/search/DataSheet.aspx?MatGUID=a9f912ab7cac4c55be884ad7d3726083" >Haynes Hastelloy® S alloy, gas tungsten arc welded, aged 1300 hours</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_16938"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;438</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_16938' href="/search/DataSheet.aspx?MatGUID=d754afc084b44785b6517ff6f1a2982f" >Haynes Hastelloy® S alloy, gas tungsten arc welded, aged at 538°C (1000°F) for 4000 hours</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_16939"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;439</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_16939' href="/search/DataSheet.aspx?MatGUID=40ffd8df1deb42668e0a95295889f60b" >Haynes Hastelloy® S alloy, weld metal, aged 1000 hours</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_16940"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;440</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_16940' href="/search/DataSheet.aspx?MatGUID=a03df68437e04cd9ab9bb2c453a16ff6" >Haynes Hastelloy® S alloy, weld metal, aged 4000 hours</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_16941"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;441</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_16941' href="/search/DataSheet.aspx?MatGUID=6e33a5f20cb94fdaa101485c6f362515" >Haynes Hastelloy® S alloy, weld metal, aged at 649°C (1200°F) for 1000 hours</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_16942"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;442</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_16942' href="/search/DataSheet.aspx?MatGUID=4d2b288d82ba47ce8c61257548a65cdd" >Haynes Hastelloy® S alloy, weld metal, aged 16000 hours</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_16994"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;443</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_16994' href="/search/DataSheet.aspx?MatGUID=1c49609319b84d7dbd3d7876ac1afd7f" >Haynes Hastelloy® W alloy, gas tungsten arc welds, as-welded</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_16996"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;444</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_16996' href="/search/DataSheet.aspx?MatGUID=45a7a4c001ef45919c41263fcc0635a2" >Haynes Hastelloy® W alloy, gas tungsten arc welds, aged 1000 hours at 650°C (1200°F)</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_16998"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;445</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_16998' href="/search/DataSheet.aspx?MatGUID=ae25b328f8ad4881aa7fa32b8a647c40" >Haynes Hastelloy® W alloy, gas metal arc welds, as-welded</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17000"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;446</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17000' href="/search/DataSheet.aspx?MatGUID=8e2d91c27f9c4a45ae5601114d41ae52" >Haynes Hastelloy® W alloy, gas metal arc welds, aged 1000 hours at 650°C (1200°F)</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17002"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;447</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17002' href="/search/DataSheet.aspx?MatGUID=8289e10a00a84c08b8781a745eca9471" >Haynes Hastelloy® W alloy, shielded metal arc welds, as-welded</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17004"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;448</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17004' href="/search/DataSheet.aspx?MatGUID=318686ccce1242638d0658bb25d3f7dc" >Haynes Hastelloy® W alloy, shielded metal arc welds, aged 1000 hours at 650°C (1200°F)</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17006"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;449</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17006' href="/search/DataSheet.aspx?MatGUID=2d5589de935a4ae288691987cf75a703" >Haynes Hastelloy® alloy X 12.7 mm plate weldments</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17008"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;450</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17008' href="/search/DataSheet.aspx?MatGUID=b2806fc180204ef3b85ce3f84010afd6" >Haynes Hastelloy® alloy 188 12.7 mm plate weldments</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17010"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;451</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17010' href="/search/DataSheet.aspx?MatGUID=d7234236c13146d8a75c0b0b367cce72" >Haynes Hastelloy® W alloy as a weld filler, base metal Multimet alloy 12.7 mm plate weldment</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17012"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;452</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17012' href="/search/DataSheet.aspx?MatGUID=63c4c99bae454c56b7da9854a7f7bf64" >Haynes Hastelloy® alloy 625 12.7 mm plate weldments</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17014"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;453</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17014' href="/search/DataSheet.aspx?MatGUID=186c0b12e9c34d56a45dd78d285b4520" >Haynes Hastelloy® alloy 718 12.7 mm plate weldments</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17015"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;454</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17015' href="/search/DataSheet.aspx?MatGUID=19e2086d71954011907597570bf33a95" >Haynes Hastelloy® W alloy as a weld filler, base metal Type 304 Stainless steel 12.7 mm plate weldments</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17016"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;455</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17016' href="/search/DataSheet.aspx?MatGUID=20a9ddd495fa4c13a8b1353b6a74030e" >Haynes Hastelloy® W alloy as a weld filler, base metal Carbon steel 12.7 mm plate weldments</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17017"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;456</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17017' href="/search/DataSheet.aspx?MatGUID=d63994ca69c84e77aac8ea045ac15e83" >Haynes Hastelloy® W alloy as a weld filler, base metal Alloy 188/ Multimet alloy 12.7 mm plate weldments</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17019"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;457</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17019' href="/search/DataSheet.aspx?MatGUID=c184a286a2bb41349e5662333c68e2d5" >Haynes Hastelloy® W alloy as a weld filler, base metal Alloy 625/ Alloy 718 12.7 mm plate weldments</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17021"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;458</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17021' href="/search/DataSheet.aspx?MatGUID=b1d997557d5340babc7256d59d6367fb" >Haynes Hastelloy® W alloy as a weld filler, base metal Type 304 Carbon steel 12.7 mm plate weldments</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17022"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;459</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17022' href="/search/DataSheet.aspx?MatGUID=677e376bc75f495ea4c83a05502a5c0c" >Haynes Hastelloy® X alloy, sheet</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_162620"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;460</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_162620' href="/search/DataSheet.aspx?MatGUID=2f32d96f589140569991166b71f47b08" >Haynes Hastelloy® Hybrid-BC1® Nickel Alloy Sheet, Cold Rolled and Solution Annealed</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_162621"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;461</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_162621' href="/search/DataSheet.aspx?MatGUID=10b57bb0d97f4e70863c62e825f2020c" >Haynes Hastelloy® Hybrid-BC1® Nickel Alloy Plate, Hot Rolled and Solution Annealed</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_162622"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;462</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_162622' href="/search/DataSheet.aspx?MatGUID=fe078a082c98472ba797a6758d34ea77" >Haynes Hastelloy® Hybrid-BC1® Nickel Alloy Bar, Hot Rolled and Solution Annealed</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_162623"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;463</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_162623' href="/search/DataSheet.aspx?MatGUID=797c60fb64054209928134fe3fe52f96" >Haynes Hastelloy® Hybrid-BC1® Nickel Alloy Gas Tungsten Arc Welded (GTAW) Plate</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_162624"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;464</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_162624' href="/search/DataSheet.aspx?MatGUID=6aa9d03f5d114e6e98596123686a23fa" >Haynes Hastelloy® Hybrid-BC1® Nickel Alloy Synergic Gas Metal Arc Welded (GMAW) Plate</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_162625"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;465</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_162625' href="/search/DataSheet.aspx?MatGUID=cd33541dd30840e7a863d55521b18078" >Haynes Hastelloy® Hybrid-BC1® Nickel Alloy Shielded Metal Arc Welded (SMAW) Plate</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_162626"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;466</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_162626' href="/search/DataSheet.aspx?MatGUID=79c99e5242a7464bbcc27c6f945a08b8" >Haynes Hastelloy® Hybrid-BC1® Nickel Alloy Synergic Gas Metal Arc Welded (GMAW) All Weld Cruciform</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_162627"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;467</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_162627' href="/search/DataSheet.aspx?MatGUID=d8e7c1ef5db24cd6ab79723bd457d012" >Haynes Hastelloy® C-22HS® Nickel Alloy Sheet, Cold-rolled, Solution Annealed</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_162628"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;468</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_162628' href="/search/DataSheet.aspx?MatGUID=60d15d777df94631af20d9ed15efe1c2" >Haynes Hastelloy® C-22HS® Nickel Alloy Plate and Bar, Hot-rolled, Solution Annealed</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_162629"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;469</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_162629' href="/search/DataSheet.aspx?MatGUID=5b6ec0a935af46d3a972d89c222cce16" >Haynes Hastelloy® C-22HS® Nickel Alloy Sheet, Cold-rolled, Solution Annealed, Age-hardened</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_162630"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;470</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_162630' href="/search/DataSheet.aspx?MatGUID=7ba6fd79f30e41bb89aca9c95e9dbf80" >Haynes Hastelloy® C-22HS® Nickel Alloy Plate and Bar, Hot-rolled, Solution Annealed, Age-hardened</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_162631"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;471</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_162631' href="/search/DataSheet.aspx?MatGUID=3f824a91e8754e55a22a2105da019889" >Haynes Hastelloy® C-22HS® Nickel Alloy Plate, Annealed</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_162632"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;472</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_162632' href="/search/DataSheet.aspx?MatGUID=a2dbc408fa9e41c79137f52b006d3983" >Haynes Hastelloy® C-22HS® Nickel Alloy Plate, Age-hardened</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_162633"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;473</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_162633' href="/search/DataSheet.aspx?MatGUID=6fdefe170fb74436a396def9651fb376" >Haynes Hastelloy® C-22HS® Nickel Alloy Gas Tungsten Arc Welded (GTAW) Sheet</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_162634"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;474</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_162634' href="/search/DataSheet.aspx?MatGUID=288137972a4547a6b60258c636a14411" >Haynes Hastelloy® C-22HS® Nickel Alloy Gas Tungsten Arc Welded (GTAW) Plate</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_162635"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;475</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_162635' href="/search/DataSheet.aspx?MatGUID=9f23f6d6f0144641b78748753c15b986" >Haynes Hastelloy® C-22HS® Nickel Alloy Gas Tungsten Arc Welded (GTAW) All Weld Cruciform</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_162636"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;476</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_162636' href="/search/DataSheet.aspx?MatGUID=8a84e29a40884175abd6540c0da01f8e" >Haynes Hastelloy® C-22HS® Nickel Alloy Synergic Gas Metal Arc Welded (GMAW) Plate</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_162637"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;477</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_162637' href="/search/DataSheet.aspx?MatGUID=e214ac0a6f2b433d9387f013b890d2a1" >Haynes Hastelloy® C-22HS® Nickel Alloy Synergic Gas Metal Arc Welded (GMAW) All Weld Cruciform</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_162638"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;478</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_162638' href="/search/DataSheet.aspx?MatGUID=7b35ead2bf9d4c83864db1c92b2b14c0" >Haynes Hastelloy® G-35® Nickel Alloy Sheet, Annealed at 2075°F (1135°C)</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_162639"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;479</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_162639' href="/search/DataSheet.aspx?MatGUID=885d3ac60a0b4a448f59950fa8979cc1" >Haynes Hastelloy® G-35® Nickel Alloy Plate, Annealed at 2075°F (1135°C)</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_162640"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;480</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_162640' href="/search/DataSheet.aspx?MatGUID=582b4c91934c482a99e42a1a33afcb70" >Haynes Hastelloy® G-35® Nickel Alloy Bar, Annealed at 2075°F (1135°C)</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_162641"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;481</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_162641' href="/search/DataSheet.aspx?MatGUID=7922027870514ecaa22f619d1a0806c4" >Haynes Hastelloy® G-35® Nickel Alloy Plate, Annealed at 2025°F (1107°C)</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_162642"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;482</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_162642' href="/search/DataSheet.aspx?MatGUID=7ab4533f96b44856822993d9366ded2c" >Haynes Hastelloy® G-35® Nickel Alloy Gas Tungsten Arc Welded (GTAW) Plate</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_162643"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;483</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_162643' href="/search/DataSheet.aspx?MatGUID=ac66d329f9504edda170afd5822627c9" >Haynes Hastelloy® G-35® Nickel Alloy Synergic Gas Metal Arc Welded (GMAW) Plate</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_162644"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;484</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_162644' href="/search/DataSheet.aspx?MatGUID=faddcd0daddc4d27a3f9a7ef2e1fdccd" >Haynes Hastelloy® G-35® Nickel Alloy Synergic Gas Metal Arc Welded (GMAW) All Weld Cruciform</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_162645"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;485</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_162645' href="/search/DataSheet.aspx?MatGUID=33f983226edd4ef995e180e214f61897" >Haynes 75 Nickel Alloy Sheet, Annealed at 1925°F (1050°C)</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_162646"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;486</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_162646' href="/search/DataSheet.aspx?MatGUID=65e60a736fc241e5b9adb13499a523c2" >Haynes HR-224® Nickel Alloy</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_162648"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;487</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_162648' href="/search/DataSheet.aspx?MatGUID=148bd330b8714feaa39c43114a1bb749" >Haynes 282® Nickel Alloy Sheet, Solution Annealed and Age-hardened</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_162649"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;488</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_162649' href="/search/DataSheet.aspx?MatGUID=cec3df755b074b99ab3d99f984bee511" >Haynes 282® Nickel Alloy Sheet, Plate and Bar, Solution Annealed</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_162650"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;489</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_162650' href="/search/DataSheet.aspx?MatGUID=e8492496693b4369a4f28a8f68703ed8" >Haynes 282® Nickel Alloy Sheet, Solution Annealed and Cold Worked</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_162651"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;490</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_162651' href="/search/DataSheet.aspx?MatGUID=980f8a08c4d34452a63d55431dc74e3a" >Haynes 282® Nickel Alloy Plate, Solution Annealed and Age-Hardened</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_162652"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;491</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_162652' href="/search/DataSheet.aspx?MatGUID=87c3f2f18592463fbd858f48e56296ac" >Haynes 282® Nickel Alloy Plate, Solution Annealed and Age-Hardened, after long term high temperature exposure</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_162653"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;492</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_162653' href="/search/DataSheet.aspx?MatGUID=9281b51bbbcc40328aa14d8a2e69728b" >Haynes 282® Nickel Alloy Sheet, Gamma-Prime Strengthened</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_162654"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;493</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_162654' href="/search/DataSheet.aspx?MatGUID=194c0b2d274447d1b4f0a1bdc5d6359b" >Haynes 282® Nickel Alloy Autogenously Welded Sheet</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_162655"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;494</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_162655' href="/search/DataSheet.aspx?MatGUID=016a3c9938184ba98b1de8f21de0a1fa" >Haynes 282® Nickel Alloy Gas Tungsten Arc Welded (GTAW) Plate</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_162656"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;495</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_162656' href="/search/DataSheet.aspx?MatGUID=0c7f77a226794479ab7a369cb81f9984" >Haynes 282® Nickel Alloy Synergic Gas Metal Arc Welded (GMAW) Plate</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_162657"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;496</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_162657' href="/search/DataSheet.aspx?MatGUID=f9ae1bbd799b4f2795792f2fa91b53f7" >Haynes 282® Nickel Alloy Synergic Gas Metal Arc Welded (GMAW) All Weld Cruciform</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_162658"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;497</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_162658' href="/search/DataSheet.aspx?MatGUID=b998cbd37f1d44849e7ff0049d0fd447" >Haynes 617 Nickel Alloy Plate</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_162659"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;498</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_162659' href="/search/DataSheet.aspx?MatGUID=97a3bd57e49e43919ee4707c4c03b212" >Haynes 625SQ® Nickel Alloy Sheet</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_162661"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;499</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_162661' href="/search/DataSheet.aspx?MatGUID=025e0200430848a9ad0890fc9f401f89" >Haynes X-750 Nickel Alloy Sheet and Strip (AMS 5598, UNS N07750)</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17029"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;500</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17029' href="/search/DataSheet.aspx?MatGUID=1a08517e8fdf4d20b2d73bc87f1f53ad" >Haynes Hastelloy® B-3® alloy, 0% cold work</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17030"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;501</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17030' href="/search/DataSheet.aspx?MatGUID=fc35429dbb3647e6bc399d4afa45fc23" >Haynes Hastelloy® B-3® alloy, 10% cold work</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17031"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;502</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17031' href="/search/DataSheet.aspx?MatGUID=36615b095ae54dc88c35773e0c51b039" >Haynes Hastelloy® B-3® alloy, 20% cold work</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17032"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;503</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17032' href="/search/DataSheet.aspx?MatGUID=17915b3614514b93aa65596a49e1383e" >Haynes Hastelloy® B-3® alloy, 30% cold work</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17033"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;504</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17033' href="/search/DataSheet.aspx?MatGUID=100fa525e1224ad6847980a7a1dea074" >Haynes Hastelloy® B-3® alloy, 40% cold work</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17034"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;505</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17034' href="/search/DataSheet.aspx?MatGUID=aa02f68a2a684d02842bc033e9bff6c9" >Haynes Hastelloy® B-3® alloy, 50% cold work</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17035"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;506</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17035' href="/search/DataSheet.aspx?MatGUID=f6e10626d22d415b883494560d086ce4" >Haynes Hastelloy® B-3® alloy, heat treated at 1065°C, rapid quenched</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17036"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;507</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17036' href="/search/DataSheet.aspx?MatGUID=d975fefda20e43ce8648a43c5bdb159d" >Haynes Hastelloy® B-3® alloy, after elevated temperature exposure</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17056"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;508</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17056' href="/search/DataSheet.aspx?MatGUID=c3f2b4ed47514e4d80d55195ee85e947" >Haynes Hastelloy® B-3® alloy, 3.2 mm bright annealed sheet</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17063"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;509</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17063' href="/search/DataSheet.aspx?MatGUID=5b4f78ca1990472183864d395335c987" >Haynes Hastelloy® B-3® alloy, 6.3 mm plate</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17070"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;510</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17070' href="/search/DataSheet.aspx?MatGUID=58dba2e12908477fa24ddd43be5c790b" >Haynes Hastelloy® C-22® alloy, 0.71-3.2 mm thick sheet, solution heat treated</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17078"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;511</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17078' href="/search/DataSheet.aspx?MatGUID=9bce2cf6485e409db4eddd56b141367e" >Haynes Hastelloy® C-22® alloy, 6.4-19.1 mm thick plate, solution heat treated</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17086"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;512</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17086' href="/search/DataSheet.aspx?MatGUID=1f5f612f32de49b9b6522bf959410ac8" >Haynes Hastelloy® C-22® alloy, 12.7-50.8 mm thick bar, solution heat treated</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17094"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;513</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17094' href="/search/DataSheet.aspx?MatGUID=4ef3e5b550d249858e1e539f0369d8db" >Haynes Hastelloy® C-22® alloy, 0% cold worked sheet</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17095"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;514</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17095' href="/search/DataSheet.aspx?MatGUID=879a02b2e11247e5a10a975dc3682417" >Haynes Hastelloy® C-22® alloy, 10% cold worked sheet</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17096"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;515</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17096' href="/search/DataSheet.aspx?MatGUID=c734a598c5b84fc785d05c2ff3471cf6" >Haynes Hastelloy® C-22® alloy, 20% cold worked sheet</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17097"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;516</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17097' href="/search/DataSheet.aspx?MatGUID=647c8f68c22b4083998cdfede4d1753c" >Haynes Hastelloy® C-22® alloy, 30% cold worked sheet</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17098"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;517</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17098' href="/search/DataSheet.aspx?MatGUID=ecbdbd8b129747d79220c0beab7b40e9" >Haynes Hastelloy® C-22® alloy, 40% cold worked sheet</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17099"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;518</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17099' href="/search/DataSheet.aspx?MatGUID=e6497f1c34244d16b0e93902e0bb9d2d" >Haynes Hastelloy® C-22® alloy, 50% cold worked sheet</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17100"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;519</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17100' href="/search/DataSheet.aspx?MatGUID=c4c9eccb87564007a3fe92ee6ceb1fd1" >Haynes Hastelloy® C-22® alloy, 60% cold worked sheet</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17101"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;520</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17101' href="/search/DataSheet.aspx?MatGUID=f4a58ad81c884537a7dfd01aa16caabc" >Haynes Hastelloy® C-22® alloy, 0% cold worked sheet, aged 100 hours at 500°C</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17102"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;521</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17102' href="/search/DataSheet.aspx?MatGUID=990ba9f934f348fd9fe951c6f0931810" >Haynes Hastelloy® C-22® alloy, 10% cold worked sheet, aged 100 hours at 500°C</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17103"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;522</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17103' href="/search/DataSheet.aspx?MatGUID=70c262446533484dacf01c17e93d14d6" >Haynes Hastelloy® C-22® alloy, 20% cold worked sheet, aged 100 hours at 500°C</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17104"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;523</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17104' href="/search/DataSheet.aspx?MatGUID=372190b2410042219989ab3153b1f5ab" >Haynes Hastelloy® C-22® alloy, 40% cold worked sheet, aged 100 hours at 500°C</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17105"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;524</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17105' href="/search/DataSheet.aspx?MatGUID=3817022860364099b325c109f3803231" >Haynes Hastelloy® C-22® alloy, 60% cold worked sheet, aged 100 hours at 500°C</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17106"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;525</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17106' href="/search/DataSheet.aspx?MatGUID=d7e5b4580c2e4a1082a4956c1f290f7a" >Haynes Hastelloy® C-22® alloy, transverse, GTAW weldment, sheet - plate</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17110"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;526</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17110' href="/search/DataSheet.aspx?MatGUID=6c331515f9a644b5a878a1e546b66b4f" >Haynes Hastelloy® C-22® alloy, transverse, GTAW short arc weldment, plate</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17118"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;527</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17118' href="/search/DataSheet.aspx?MatGUID=c1dae52ca5434f6faf786b75106cb94a" >Haynes Hastelloy® C-22® alloy, transverse, GTAW spray weldment, 12.7 mm (1/2 in.) thick plate</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17121"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;528</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17121' href="/search/DataSheet.aspx?MatGUID=bf669f607c8f41fa9b590f72b8c988a5" >Haynes Hastelloy® C-22® alloy, transverse, SMAW weldment, 19.1 mm thick plate</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17122"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;529</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17122' href="/search/DataSheet.aspx?MatGUID=5c8f4a568f16436e88bd254949452d75" >Haynes Hastelloy® C-22® alloy, transverse, GMAW short arc weldment, plate</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17124"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;530</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17124' href="/search/DataSheet.aspx?MatGUID=584087d5ef70420c8901f509efdc2c08" >Haynes Hastelloy® C-22® alloy, transverse, GMAW spray weldment, 23.4 mm thick plate</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17128"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;531</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17128' href="/search/DataSheet.aspx?MatGUID=c946b6de91994e51a6cba4aa0194fed9" >Haynes Hastelloy® C-22® alloy, all-weld metal, GTAW</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17131"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;532</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17131' href="/search/DataSheet.aspx?MatGUID=f263d813d1074414825244959e7a0a84" >Haynes Hastelloy® C-22® alloy, all-weld metal, GMAW short arc</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17134"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;533</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17134' href="/search/DataSheet.aspx?MatGUID=c352dc5819704d6bac9acd3fd704cd29" >Haynes Hastelloy® C-22® alloy, all-weld metal, SMAW</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17136"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;534</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17136' href="/search/DataSheet.aspx?MatGUID=ccc0ae6f29684628a85fb3c4a01b11ab" >Haynes Hastelloy® C-22® alloy, GTAW, as welded, unnotched</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17137"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;535</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17137' href="/search/DataSheet.aspx?MatGUID=5475964a87f643a7ace7347189aa71e5" >Haynes Hastelloy® C-22® alloy, GTAW, aged 4000 hours at 450°C (800°F), unnotched</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17138"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;536</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17138' href="/search/DataSheet.aspx?MatGUID=41ba9ea4483c4d6799a24aff1ea7dd69" >Haynes Hastelloy® C-22® alloy, GTAW, as welded, notched</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17139"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;537</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17139' href="/search/DataSheet.aspx?MatGUID=eb4a9da566fc45e3b8bc91263b337ede" >Haynes Hastelloy® C-22® alloy, GTAW, aged 4000 hours at 450°C (800°F), notched</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17140"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;538</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17140' href="/search/DataSheet.aspx?MatGUID=1bba8c43ae9b4f90b1028a6460f44e09" >Haynes Hastelloy® C-22® alloy, GMAW, as welded, unnotched</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17141"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;539</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17141' href="/search/DataSheet.aspx?MatGUID=18f7e7526e4c4b0283c8fbda1d2a6d9c" >Haynes Hastelloy® C-22® alloy, GMAW, aged 4000 hours at 450°C (800°F), unnotched</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17142"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;540</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17142' href="/search/DataSheet.aspx?MatGUID=72239f5246b1413880655988e65ad936" >Haynes Hastelloy® C-22® alloy, GMAW, as welded, notched</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17143"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;541</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17143' href="/search/DataSheet.aspx?MatGUID=782e19c40a534459a1bf2dc20c18902b" >Haynes Hastelloy® C-22® alloy, GMAW, aged 4000 hours at 450°C (800°F), notched</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17144"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;542</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17144' href="/search/DataSheet.aspx?MatGUID=f40e65f0620948b3bf7a2614067338be" >Haynes Hastelloy® C-22® alloy, GTAW weld, 316L base metal, C-22 weld metal</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17145"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;543</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17145' href="/search/DataSheet.aspx?MatGUID=add44ffe14fb4f7eb8df3da0ff95899e" >Haynes Hastelloy® C-22® alloy, SMAW weld, 316L base metal, C-22 weld metal</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17146"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;544</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17146' href="/search/DataSheet.aspx?MatGUID=677596c2717b467cb0d869f037e4f155" >Haynes Hastelloy® C-22® alloy, GTAW weld, 904L base metal, C-22 weld metal</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17147"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;545</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17147' href="/search/DataSheet.aspx?MatGUID=6912eb47d0704a18a4cebb62409a23ca" >Haynes Hastelloy® C-22® alloy, SMAW weld, 904L base metal, C-22 weld metal</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17148"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;546</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17148' href="/search/DataSheet.aspx?MatGUID=514a0f38a9c34cb7989d9c748010b8db" >Haynes Hastelloy® C-22® alloy, GTAW weld, C-22 base and weld metal</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17149"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;547</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17149' href="/search/DataSheet.aspx?MatGUID=c97d9d80170e41aeb6476fb515118a8f" >Haynes Hastelloy® C-22® alloy, SMAW weld, C-22 base and weld metal</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17150"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;548</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17150' href="/search/DataSheet.aspx?MatGUID=068ba26130fc49fe89d51482c15602c5" >Haynes Hastelloy® C-4 alloy, sheet, heat treated at 1066°C, rapid quenched</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17165"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;549</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17165' href="/search/DataSheet.aspx?MatGUID=247febc2fdf04025a497c40296e7eec6" >Haynes Hastelloy® C-4 alloy, sheet, aged 100 hours at 899°C</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17179"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;550</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17179' href="/search/DataSheet.aspx?MatGUID=f207b28c6d614a2d92c774f6b9f235e7" >Haynes Hastelloy® C-4 alloy, plate, heat treated at 1066°C, rapid quenched</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17196"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;551</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17196' href="/search/DataSheet.aspx?MatGUID=684b452fb7b440ff9cfa1667f2fe4bee" >Haynes Hastelloy® C-4 alloy, welded plate 12.7 mm (1/2 in.) thick, GTAW</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17199"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;552</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17199' href="/search/DataSheet.aspx?MatGUID=9ff329587c2244aab5634a1ecea8761a" >Haynes Hastelloy® C-4 alloy, welded plate 12.7 mm (1/2 in.) thick, aged 10 hours at 649°C (1200°F)</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17200"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;553</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17200' href="/search/DataSheet.aspx?MatGUID=0bbf1dab4b6141d29ee7ef1c83747955" >Haynes Hastelloy® C-4 alloy, welded plate 12.7 mm (1/2 in.) thick, aged 1000 hours at 649°C (1200°F)</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17201"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;554</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17201' href="/search/DataSheet.aspx?MatGUID=f02936d1ce35460683f4ddd5a5a4cf46" >Haynes Hastelloy® C-4 alloy, all-weld metal, as welded, GTAW</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_130829"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;555</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_130829' href="/search/DataSheet.aspx?MatGUID=c8ed9883fce84b4c8e8ac26fe8dc0b2b" >H.C. Starck NiV Tube Targets</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_146233"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;556</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_146233' href="/search/DataSheet.aspx?MatGUID=bf5886b231ca4f63ac78e0149552bbaf" >Elmet-Starck MP35N® Nickel/Cobalt/Chromium/Molybdenum Alloy</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17214"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;557</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17214' href="/search/DataSheet.aspx?MatGUID=be2ad7581c924bbfbfcd7e958a93e87e" >H.C. Starck Ampersint® MAP FeNi 50/50 For Hardmetals</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17215"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;558</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17215' href="/search/DataSheet.aspx?MatGUID=fa3c2ac2d5914f61a7f9aab6b8eb5d9e" >H.C. Starck Ampersint® MAP FeNi 50/50 For Soft Magnets</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17217"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;559</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17217' href="/search/DataSheet.aspx?MatGUID=0aac9cea131f4abcb4d7e8fd0c4fab9b" >H.C. Starck Ni-SA 625 Nickel Super Alloy</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17218"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;560</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17218' href="/search/DataSheet.aspx?MatGUID=d41d5832949748f58797ac7b0b89f524" >H.C. Starck Ni-SA U700 Nickel Super Alloy</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17219"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;561</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17219' href="/search/DataSheet.aspx?MatGUID=7ec3a525cf2b40febcc6750dfab81a1d" >H.C. Starck Ni-SA 718 Nickel Super Alloy</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17220"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;562</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17220' href="/search/DataSheet.aspx?MatGUID=79e6896f8290430684505f9527f268a1" >H.C. Starck Ni-SA 939 Nickel Super Alloy</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17223"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;563</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17223' href="/search/DataSheet.aspx?MatGUID=d3f4d16b564a4f10971ad40bc30129a5" >H.C. Starck NiAl 97/3 Atomized Powder</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17242"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;564</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17242' href="/search/DataSheet.aspx?MatGUID=dbe6a35caaab4d6f97c9b019e48700f6" >H.C. Starck Amperit® 175.1 Nickel (Ni)</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17243"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;565</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17243' href="/search/DataSheet.aspx?MatGUID=2cb2c9670dd847c78d52ea1ba6ae8c04" >H.C. Starck Amperit® 175.2 Nickel (Ni)</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17244"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;566</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17244' href="/search/DataSheet.aspx?MatGUID=4f0dfcdf2d764325be6c8e778aef6f37" >H.C. Starck Amperit® 250.071 Water Atomized (NiCr 80-20)</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17245"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;567</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17245' href="/search/DataSheet.aspx?MatGUID=156b7554595847c59f1de889e11b1b5d" >H.C. Starck Amperit® 250.1 Water Atomized (NiCr 80-20)</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17246"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;568</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17246' href="/search/DataSheet.aspx?MatGUID=2f0f8cc9b7f6496ab4377fc191f13966" >H.C. Starck Amperit® 250.2 Water Atomized (NiCr 80-20)</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17247"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;569</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17247' href="/search/DataSheet.aspx?MatGUID=07bea4e3f8a0495697d06f8dd88ff7ca" >H.C. Starck Amperit® 250.3 Water Atomized (NiCr 80-20)</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17248"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;570</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17248' href="/search/DataSheet.aspx?MatGUID=926136277467435fb64db279caf40d77" >H.C. Starck Amperit® 251 Gas Atomized (NiCr 80-20)</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17249"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;571</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17249' href="/search/DataSheet.aspx?MatGUID=f2e87c9dee31472c9212805a4fdff56f" >H.C. Starck Amperit® 251.1 Gas Atomized (NiCr 80-20)</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17250"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;572</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17250' href="/search/DataSheet.aspx?MatGUID=afac2ec7e54f4e2b8857ab279a7f1e15" >H.C. Starck Amperit® 251.2 Gas Atomized (NiCr 80-20)</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17251"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;573</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17251' href="/search/DataSheet.aspx?MatGUID=3ad45840f8d4442687c15bee66a33044" >H.C. Starck Amperit® 280.2 Water Atomized (NiAl 95-5)</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17252"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;574</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17252' href="/search/DataSheet.aspx?MatGUID=40d9ae48c56e458d9ed3109be4769b43" >H.C. Starck Amperit® 281 Gas Atomized (NiAl 95-5)</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17253"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;575</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17253' href="/search/DataSheet.aspx?MatGUID=3dda03e0c8584bdb9bc89617f103806f" >H.C. Starck Amperit® 281.2 Gas Atomized (NiAl 95-5)</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17254"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;576</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17254' href="/search/DataSheet.aspx?MatGUID=edb1c51fe39744588d0542bd5f3cb7f8" >H.C. Starck Amperit® 281.3 Gas Atomized (NiAl 95-5)</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17255"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;577</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17255' href="/search/DataSheet.aspx?MatGUID=106ef59718634d7cb9348489fdab94a0" >H.C. Starck Amperit® 290.3 Water Atomized Nickel-Aluminum 69-31 (NiAl)</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17256"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;578</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17256' href="/search/DataSheet.aspx?MatGUID=336c22034b2040e58844cb70cbdd912f" >H.C. Starck Amperit® 290.8 Water Atomized Nickel-Aluminum 69-31 (NiAl)</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17257"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;579</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17257' href="/search/DataSheet.aspx?MatGUID=cd47664cfeee406baf5093e8eeef573b" >H.C. Starck Amperit® 335.063 Nickel Self-fluxing Alloy HRC 60 (NiCrBSi)</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17258"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;580</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17258' href="/search/DataSheet.aspx?MatGUID=8c9e97371ae441cc86263be07ad73d86" >H.C. Starck Amperit® 335.066 Nickel Self-fluxing Alloy HRC 60 (NiCrBSi)</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17265"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;581</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17265' href="/search/DataSheet.aspx?MatGUID=233d3a94a53742f4ab6222f0c7c49bd9" >Haynes Hastelloy® G-30® alloy, 0.71 mm thick sheet</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17266"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;582</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17266' href="/search/DataSheet.aspx?MatGUID=8c9def186fa74d8197493e73705e3c3a" >Haynes Hastelloy® G-30® alloy, 3.2 mm thick sheet</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17267"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;583</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17267' href="/search/DataSheet.aspx?MatGUID=fab3fd24088b4c239dce9d81b520a3eb" >Haynes Hastelloy® G-30® alloy, 6.4 mm thick plate</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17268"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;584</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17268' href="/search/DataSheet.aspx?MatGUID=7125d81919b047afa0f8fc47ad570560" >Haynes Hastelloy® G-30® alloy, 9.5 mm thick plate</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17269"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;585</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17269' href="/search/DataSheet.aspx?MatGUID=98c7c90abe1f4f4ebe7c66fa5bda7bbb" >Haynes Hastelloy® G-30® alloy, 12.7 mm (1/2 in.) thick plate</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17270"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;586</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17270' href="/search/DataSheet.aspx?MatGUID=649867f0b3ea4fd389880dae1091c66f" >Haynes Hastelloy® G-30® alloy, 19.1 mm thick plate</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17271"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;587</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17271' href="/search/DataSheet.aspx?MatGUID=aca00e6a99a24558b00b21e8ff2f86b2" >Haynes Hastelloy® G-30® alloy, 31.8 mm thick plate</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17272"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;588</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17272' href="/search/DataSheet.aspx?MatGUID=c91538ba6c16499e951e75da8717292b" >Haynes Hastelloy® G-30® alloy, 25.4 mm thick bar</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17273"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;589</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17273' href="/search/DataSheet.aspx?MatGUID=46f578592f4944e9aa89608f48915d16" >Haynes Hastelloy® G-30® alloy, plate and bar</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17279"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;590</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17279' href="/search/DataSheet.aspx?MatGUID=c0021659f1664e889e0b2f9109ba6100" >Haynes Hastelloy® G-30® alloy, mill annealed</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17280"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;591</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17280' href="/search/DataSheet.aspx?MatGUID=ba28ac6d2d874f28a27d9ed083213320" >Haynes Hastelloy® G-30® alloy, 10% cold rolled</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17281"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;592</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17281' href="/search/DataSheet.aspx?MatGUID=94dcabdcc9ce42f9a8e3f025a8e08920" >Haynes Hastelloy® G-30® alloy, 30% cold rolled</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17282"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;593</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17282' href="/search/DataSheet.aspx?MatGUID=225cc5ce531e4b85817b8b615dcc0864" >Haynes Hastelloy® G-30® alloy, 50% cold rolled</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17283"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;594</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17283' href="/search/DataSheet.aspx?MatGUID=7c5e8b6069fb4070bd30b191e8c09d4d" >Haynes Hastelloy® G-30® alloy, 50% cold rolled and 1 hour at 500°C, air cooled</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17284"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;595</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17284' href="/search/DataSheet.aspx?MatGUID=26185bccc0144c2aac05efb63d676f38" >Haynes Hastelloy® G-30® alloy, 50% cold rolled and 5000 hours at 500°C, air cooled</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17285"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;596</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17285' href="/search/DataSheet.aspx?MatGUID=5dd3e0e706554bddbce136070e581819" >Haynes Hastelloy® G-30® alloy, 3.2 mm thick sheet, GTAW weld</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17288"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;597</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17288' href="/search/DataSheet.aspx?MatGUID=885b4ec7a7f04130bedddf0de45208fd" >Haynes Hastelloy® G-30® alloy, 12.7 mm (1/2 in.) thick plate, GTAW weld 3.2 mm diameter filler wire</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17291"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;598</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17291' href="/search/DataSheet.aspx?MatGUID=5d6a05b9310e4a249a13ff107a5ae3c8" >Haynes Hastelloy® G-30® alloy, 12.7 mm (1/2 in.) thick plate, GTAW short arc weld 1.1 mm diameter filler wire</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17294"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;599</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17294' href="/search/DataSheet.aspx?MatGUID=de58d8099c76427b8815969399ad7970" >Haynes Hastelloy® G-30® alloy, 12.7 mm (1/2 in.) thick plate, GMAW spray weld 1.1 mm diameter filler wire</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17297"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;600</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17297' href="/search/DataSheet.aspx?MatGUID=aa2b321b063348b6acda320ea9de01ea" >Haynes Hastelloy® G-30® alloy, all-weld metal, GTAW weld 3.2 mm diameter filler wire</a></td></tr>

		
	</table>

	<table style="background-color:Silver;" class="tabledataformat" >

		<tr class="">
			<th>&nbsp;</th>
		</tr>

		<tr>
			<td><strong>Found <span id="ctl00_ContentMain_UcSearchResults1_lblResultCount2">1245</span> Results</strong> -- 
				
				Page 
				<select name="ctl00$ContentMain$UcSearchResults1$drpPageSelect2" onchange="javascript:setTimeout('__doPostBack(\'ctl00$ContentMain$UcSearchResults1$drpPageSelect2\',\'\')', 0)" id="ctl00_ContentMain_UcSearchResults1_drpPageSelect2">
		<option value="1">1</option>
		<option value="2">2</option>
		<option selected="selected" value="3">3</option>
		<option value="4">4</option>
		<option value="5">5</option>

	</select>
				of <span id="ctl00_ContentMain_UcSearchResults1_lblPageTotal2">7</span> 
				
				-- 
				<a id="ctl00_ContentMain_UcSearchResults1_lnkPrevPage2" href="javascript:__doPostBack('ctl00$ContentMain$UcSearchResults1$lnkPrevPage2','')" style="color:Black;">[Prev Page]</a>
				<a id="ctl00_ContentMain_UcSearchResults1_lnkNextPage2" href="javascript:__doPostBack('ctl00$ContentMain$UcSearchResults1$lnkNextPage2','')" style="color:Black;">[Next Page]</a>
				--&nbsp;

				view 
				<select name="ctl00$ContentMain$UcSearchResults1$drpPageSize2" onchange="javascript:setTimeout('__doPostBack(\'ctl00$ContentMain$UcSearchResults1$drpPageSize2\',\'\')', 0)" id="ctl00_ContentMain_UcSearchResults1_drpPageSize2">
		<option value="25">25</option>
		<option value="50">50</option>
		<option value="75">75</option>
		<option value="100">100</option>
		<option value="150">150</option>
		<option selected="selected" value="200">200</option>

	</select> per page
			</td>
		</tr>
	</table>
	
	 


</div>





<script type="text/javascript">

	//GLOBAL VARIABLES
	var gCurrentCheckbox;
	var gNewFolderID;
	var gOldFolderName;
	var gSelectedFolderID = 0;
	var gMaxFolders = 0;

	//GLOBAL FIELD REFERENCES
	var drpFolderList = document.getElementById("ctl00_ContentMain_UcSearchResults1_drpFolderList");
	var drpFolderAction = document.getElementById("ctl00_ContentMain_UcSearchResults1_drpFolderAction");
	var txtFolderMatCount = document.getElementById("ctl00_ContentMain_UcSearchResults1_txtFolderMatCount");
	var divUserMessage = document.getElementById("divUserMessage");
	var divFolderName = document.getElementById("divFolderName");
	var tblFolderName = document.getElementById("tblFolderName");
	var txtFolderName = document.getElementById("txtFolderName2");
	
	// See MS ajax.net client side documentation
	// http://ajax.asp.net/docs/ClientReference/Sys.Net/WebrequestManagerClass/	
	Sys.Net.WebRequestManager.add_invokingRequest(ucSearchResults_OnRequestStart);
	Sys.Net.WebRequestManager.add_completedRequest(ucSearchResults_OnRequestEnd);
	
	var timerID = 0;

	function ucSearchResults_OnRequestStart(){
		document.body.style.cursor="progress";
	}
	
	function ucSearchResults_OnRequestEnd(){
		document.body.style.cursor="auto";
	}

	//drpFolderAction pull down does not exist for anonymous users
	if(drpFolderAction) {
		drpFolderAction.onchange=RunFolderAction;
	}

	//RUN ONCE on page load
	//matweb.register_onload(SetSelectedFolder);
	
	SearchMatIDList = []; //an array of MatIDs
	//matweb.alert(MatIDList);
	
	//======================
	//Test Service 
	//======================
	//TestService("This is a test");
	//aci.matweb.WebServices.Folders.RaiseTestError(null,ErrorHandler,"testerror");
	
	function TestService(arg){
		aci.matweb.WebServices.Folders.HelloWorld(arg, TestService_CALLBACK,ErrorHandler,"TestService");
	}
	
	function TestService_CALLBACK(data,context){
		alert(data);
	}
	
	// =================================
	// This function is the onchange event for the material checkbox. See Code Behind.
	// The first time a material is checked, and there are no folders, the folder web service creates a NEW FOLDER
	// =================================
	function ToggleMat(chkBox,MatID,MaxFolderMats){
	
		HideFolderNameControls();

		var FolderID,Context;
		gCurrentCheckbox = chkBox;
		var matLink = $("lnkMatl_" + MatID);
		var MatName = matLink.innerHTML;
		//alert(MatName);

		var SelectedOption = drpFolderList.options[drpFolderList.selectedIndex];  

		if(gNewFolderID > 0)
			FolderID = gNewFolderID;
		else
			FolderID = SelectedOption.value; //this will be 0 if no folders exist yet
		//alert(FolderID);
		
		// call the folder web service 
		// See: ScriptManagerProxy object at the top of this page
		// Also See: /folders/FolderService.asmx & /App_Code/FolderService.cs
		if(chkBox.checked){
			Context = "AddMat";
			aci.matweb.WebServices.Folders.AddMaterial(FolderID, MatID, MatName, ToggleMat_CALLBACK,ErrorHandler,Context);
		}
		else{
			Context = "RemoveMat";
			aci.matweb.WebServices.Folders.RemoveMaterial(FolderID, MatID, ToggleMat_CALLBACK,ErrorHandler,Context);
		}
			
	}
	
	// =================================
	// CALLBACK FUNCTION
	// this function is called by the AJAX framework after the web service is invoked
	// The folder web service returns a ReturnData type object.
	// =================================
	function ToggleMat_CALLBACK(data,context){

		 //alert(data);
		// alert(context);

		var FolderMatCount = data.FolderMatCount
		var NewFolderID = data.NewFolderID
		var DisplayMessage = data.DisplayMessage
		var MaxFolderMats = data.FolderMaxMatls;
		
		txtFolderMatCount.value = FolderMatCount + "/" + MaxFolderMats;

		// If this is the first time the user checked the checkbox, and the user did not already have any folders, 
		// a new folder may have been created.
		// Save the new folderID to a hidden field, so we can reference it later.
		if(NewFolderID > 0){
			gNewFolderID = NewFolderID;
		}
		
		// alert(gNewFolderID);
		// alert(FolderCount);
		// alert(ReloadFolderList);
		// if(NewFolderID>0) alert(NewFolderID);
		// alert(MaxFolderMats);
		
		switch(context){
		case "AddMat":
			if(DisplayMessage){
				gCurrentCheckbox.checked = false;
				matweb.alert(DisplayMessage);
			}
			break    
		case "RemoveMat":
			break    
		default:
			matweb.Alert("Unhandeled Context in ToggleMat_CALLBACK() function: " + Context);
		}//end switch

	}//end function

	
	//=================================
	function CompareMaterials(){
	//=================================
		var SelectedFolderID = GetSelectedFolderID();
		window.location.href="/folders/Compare.aspx?FolderID=" + SelectedFolderID;
	}

	//=================================
	// Runs on drpFolderList.onchange, and also is called by other methods.
	// Sets the checkbox for each visible material in the folder.
	// Also sets txtFolderMatCount with the MatCount indicator for the selected folder.
	//=================================
	function SetSelectedFolder(){
		//alert("SetSelectedFolder()");
		var SelectedFolderID = GetSelectedFolderID();
		//alert("SelectedFolderID :" + SelectedFolderID)
		
		aci.matweb.WebServices.Folders.SetSelectedFolder(SelectedFolderID,SetSelectedFolder_CALLBACK,ErrorHandler,"update_txtFolderMatCount");
		
		HideFolderNameControls();
	}

	// =================================
	// CALLBACK FUNCTION
	// Returns a list of MatID's in the selected folder, and sets the checkbox for each material.
	// Also sets txtFolderMatCount with the MatCount indicator for the selected folder.
	// =================================
	function SetSelectedFolder_CALLBACK(data,context){

		// alert(data);
		// alert(context);

		// the folder web service returns an object with 4 fields
		//alert("data.FolderMatCount: " + data.FolderMatCount);
		//alert("data.FolderMaxMatls: " + data.FolderMaxMatls);
		//alert("data.DisplayMessage: " + data.DisplayMessage);
		//alert("data.MatIDList: " + data.FolderMatIDList);

		if(data.DisplayMessage){
			matweb.alert(data.DisplayMessage);
		}

		txtFolderMatCount.value = data.FolderMatCount + "/" + data.FolderMaxMatls;

		//alert(SearchMatIDList);
		var chkFolder;

		// Set checkboxes for any visible materials in the folder
		// loop over all materials in the search results for the current page...
		//alert('lenOfArray=' + SearchMatIDList.length);
		for(s=0;s<SearchMatIDList.length;s++){
			chkFolder = document.getElementById("chkFolder_" + SearchMatIDList[s]);
			//alert('index=' + SearchMatIDList[s]);
			chkFolder.checked = false; //clear any currently checked boxes...
			//loop over all materials in the folder...
			//If there is a match, make it checked.
			for(f=0;f<data.FolderMatIDList.length;f++){
				if(SearchMatIDList[s]==data.FolderMatIDList[f]){
					//alert("Match found: " + data.FolderMatIDList[f]);
					chkFolder.checked = true;
				}
			}
		}

	}//end function
	
	//=================================
	function RunFolderAction(){
	//=================================
	
		//alert("RunFolderAction()");
		var action = drpFolderAction.options[drpFolderAction.selectedIndex].value;
		var SelectedFolderName = drpFolderList.options[drpFolderList.selectedIndex].text;
		var FolderID = GetSelectedFolderID();
		var msg = "";
		
		switch(action){
			case "empty":
				aci.matweb.WebServices.Folders.EmptyFolder(FolderID,SetSelectedFolder_CALLBACK,ErrorHandler,action);
				msg = "Folder ["+SelectedFolderName+"] was cleared of materials";
				HideFolderNameControls();
				break;
			case "delete":
				var answer = confirm("Delete folder ["+SelectedFolderName+"] ?");
				if(answer){
					gOldFolderName = SelectedFolderName;
					aci.matweb.WebServices.Folders.DeleteFolder(FolderID,DeleteFolder_CALLBACK,ErrorHandler,action);
				}
				HideFolderNameControls();
				break;
			case "new":
				//setting up new folder controls
				if(drpFolderList.options.length >= gMaxFolders){
					matweb.alert("You have already added as many folders as you are currently allowed by the system (" + gMaxFolders + ").<p/> You may view our <a href=\"/membership/benefits.aspx\">benefits</a> page for the features that are allowed for your current user level.");
				}
				else{
				divFolderName.style.display="block";
				divFolderName.innerHTML="New Folder Name";
				tblFolderName.style.display="block";
				txtFolderName.value = "New Folder Name";
				txtFolderName.focus();
				txtFolderName.select();
				}
				break;
			case "rename":
				//setting up rename folder controls
				divFolderName.style.display="block";
				divFolderName.innerHTML="Rename Folder";
				tblFolderName.style.display="block";
				txtFolderName.value = SelectedFolderName;
				txtFolderName.focus();
				txtFolderName.select();
				break;
			case "managefolders":
				window.location.href="/folders/ListFolders.aspx";
				break;
			case "expSolidworks":
				ExportFolder(FolderID, 2, null);
				break;
			case "expALGOR":
				ExportFolder(FolderID, 3, null);
				break;
			case "expANSYS":
				ExportFolder(FolderID, 4, null);
				break;
			case "expNEiWorks":
				ExportFolder(FolderID, 5, null);
				break;
			case "expCOMSOL3":
				ExportFolder(FolderID, 6, '3.4 or newer');
				break;
			case "expCOMSOL4":
				ExportFolder(FolderID, 10, null);
				break;
			case "expETBX":
				ExportFolder(FolderID, 8, null);
				break;
			case "expSpaceClaim":
				ExportFolder(FolderID, 9, null);
				break;
			case "0":
				//do nothing
				drpFolderAction.selectedIndex = 0;
				break;
			default: 
				alert("Unhandled Action: " + action);
		}

		//drpFolderAction.selectedIndex=0;
		divUserMessage.innerHTML = msg;
		//alert("End RunFolderAction()");
		
	}
	
	function DeleteFolder_CALLBACK(DeleteWasSuccessfull,context){
		divUserMessage.innerHTML = "Folder ["+gOldFolderName+"] was deleted.";
		//Refresh the folder list and selected checkboxes
		aci.matweb.WebServices.Folders.GetFolderList(GetFolderList_CALLBACK,ErrorHandler,"DeleteFolder_CALLBACK");
	}	

	// rsw
	function ExportFolder(folderid, modeid, comsolversion){
		if(txtFolderMatCount.value.substring(0,1) == '0'){
			alert('Folder is empty, nothing to export.');
			drpFolderAction.selectedIndex = 0;
			return;
		}

		var ExportPath = "/folders/ListFolders.aspx";
		
		//alert(ExportPath);
		location.href = ExportPath;
		//window.open(ExportPath, '', '', '');
	}
	
	
	
	
	//===============================================================
	// This runs on the onclick event for btnApplyFolderName.
	// It handles creating a new folder, or renaming an existing one.
	//===============================================================
	function ApplyFolderName(){
	
		//alert("ApplyFolderName()");
		var action = drpFolderAction.options[drpFolderAction.selectedIndex].value;
		var NewFolderName = txtFolderName.value;
		var FolderID = GetSelectedFolderID();
		var msg = "";

		//alert(FolderID);

		//If we are showing the non-existant "virtual" folder "My Folder",
		//create it first, and then rename it.
		if(action=="rename" && FolderID==0)
			action="add_and_rename";

		switch(action){
			case "new":
				aci.matweb.WebServices.Folders.AddFolder(NewFolderName,AddRenameFolder_CALLBACK,ErrorHandler,action);
				break;
			case "rename":
				gOldFolderName = drpFolderList.options[drpFolderList.selectedIndex].text;
				aci.matweb.WebServices.Folders.RenameFolder(FolderID,NewFolderName,AddRenameFolder_CALLBACK,ErrorHandler,action);
				break;
			case "add_and_rename":
				gOldFolderName = drpFolderList.options[drpFolderList.selectedIndex].text;
				aci.matweb.WebServices.Folders.AddFolder(NewFolderName,AddRenameFolder_CALLBACK,ErrorHandler,action);
				break;
			default: 
				alert("Unhandled Action in ApplyFolderName(): " + action);
		}

		HideFolderNameControls();
		
	}
	
	//=================================
	// CALLBACK FUNCTION
	//=================================
	function AddRenameFolder_CALLBACK(ReturnData,context){
		//alert("AddRenameFolder_CALLBACK()");
		
		var msg;
		//Folder = ReturnData.Folder;
	
		switch(context){
			case "new":
				//gNewFolderID = Folder.FolderID;
				gNewFolderID = ReturnData.NewFolderID;
				msg = "The folder [" + ReturnData.NewFolderName + "] was created.";
				break;
			case "rename":
				msg = "The folder [" + gOldFolderName+ "] was renamed to [" + ReturnData.NewFolderName + "].";
				break;
			case "add_and_rename":
				//gNewFolderID = Folder.FolderID;
				gNewFolderID = ReturnData.NewFolderID;
				msg = "The folder [" + gOldFolderName+ "] was renamed to [" + ReturnData.NewFolderName + "].";
				break;
		}
		
		divUserMessage.innerHTML = msg;
		
		//refresh the folder list, so it includes the new folder
		aci.matweb.WebServices.Folders.GetFolderList(GetFolderList_CALLBACK,ErrorHandler,"AddFolder");

	}
	
	//=================================
	function HideFolderNameControls(){
	//=================================
		//These fields will not exist for anonymous users
		if(drpFolderAction){
			drpFolderAction.selectedIndex=0;
			divFolderName.style.display="none";
			tblFolderName.style.display="none";
		}
	}
	
	//=================================
	function GetSelectedFolderID(){
	//=================================
		var FolderID = 0;
		//if one of the processeses created a new folder id, then use it.
		// alert('gNewFolderID=' + gNewFolderID);
		if(gNewFolderID > 0){
			FolderID = gNewFolderID;
			//gNewFolderID = 0;//clear the new folder id, so we don't use it again.
		}
		else 
			if(drpFolderList != null){
				if(drpFolderList.options.length == 0){
					FolderID = gSelectedFolderID;
				}
				else{
					var SelectedOption = drpFolderList.options[drpFolderList.selectedIndex];  
					FolderID = SelectedOption.value; //this will be 0 if no folders exist yet
				}
			}
		return FolderID;
	}

	//=================================
	function UnregisteredUserMessage(chkBox){
	//=================================
		matweb.alert("This feature is reserved for registered users.<p/><a href='/membership/regstart.aspx'>Click here to register.</a>","Reserved Feature");
		chkBox.checked = false;
	}

	// =================================
	// CALLBACK FUNCTION
	// The folder web service returns a JS array of iBatis data objects of type: aci.matweb.data.folder.Folder 
	// The drop down list of folders is set using this list.
	// =================================
	function GetFolderList_CALLBACK(iFolderList,Context){
	
		//alert(iFolderList);
		//alert('iFolderList.length: ' + iFolderList.length);

		if(drpFolderList != null){
		
		var SelectedFolderID = GetSelectedFolderID();//remember the current selected folder, so we can use it later
		var opt;
		var folder;

		//clear the options and reload them
		drpFolderList.options.length = 0;

		//add a default empty folder...		
		if(iFolderList.length==0){
			opt = document.createElement('OPTION');
			opt.value = "0";
			opt.text = "My Folder";
			drpFolderList.options.add(opt);
			i++;
		}
		
		for(var i = 0;i < iFolderList.length;i++){
			opt = document.createElement('OPTION');
			folder = iFolderList[i];
			//alert(folder.FolderID);
			//alert(folder.FolderName);
			opt.value = folder.FolderID;
			opt.text = folder.FolderName;
			if(folder.FolderID == SelectedFolderID) opt.selected = true;
			drpFolderList.options.add(opt);
		}

		//clear the new folder id, so we don't use it again. 
		gNewFolderID = 0;
		
		//Set checkboxes and folder count fields for the new selected folder
		SetSelectedFolder();
		}

	}
	
	// =================================
	// ON ERROR CALLBACK FUNCTION
	// =================================
	function ErrorHandler(error,context) {
	
			var stackTrace = error.get_stackTrace();
			var message = error.get_message();
			var statusCode = error.get_statusCode();
			var exceptionType = error.get_exceptionType();
			var timedout = error.get_timedOut();
			var Debug = false;

			// Compose the error message...
			var ErrMsg =     
					"<strong>Stack Trace</strong><br/>" +  stackTrace + "<br/>" +
					"<strong>WebService Error</strong><br/>" + message + "<br/>" +
					"<strong>Status Code</strong><br/>" + statusCode + "<br/>" +
					"<strong>Exception Type</strong><br/>" + exceptionType + "<br/>" +
					"<strong>JS Context</strong><br/>" + context + "<br/>" +
					"<strong>Timedout</strong><br/>" + timedout + "<br/>" +
					"<strong>Source Page</strong><br/>" + "/search/MaterialGroupSearch.aspx";
			//matweb.alert(ErrMsg,"Error in WebService",null,"500");
			if(Debug){
				matweb.alert(ErrMsg,"Error in WebService",null,"500");
				//alert(ErrMsg);
			}
			else{
				ErrMsg = escape(ErrMsg);
				//matweb.alert(ErrMsg);
				//window.location.href="/errorJS.aspx?ErrMsg=" + ErrMsg;
				divUserMessage.innerHTML = "Problem retrieving folder data...Some folder features may not work.";
			}
		
	}
	
	function ShowInterpolationMessage(){
		InterpolationMsg = "The material was found via interpolation, so no data points can be displayed. See the material data sheet for actual data points.";
		matweb.alert(InterpolationMsg);
	}
	
</script>

		</td>
	</tr>
	</table>

<hr />
<a id="help"></a>
<table><tr><td>
<p/><b>Instructions:</b> This page allows you to quickly access all of the polymers/plastics, metals, ceramics, fluids, and other engineering materials
in the MatWeb material property database.&nbsp; Just select the material category you would like to find from the tree.  Click on the [+] icon to open subcategory branches in the tree.
Then click the <b>'Find'</b> button next to its box.&nbsp; You can then follow the links to the most complete data sheets on the Web, with information on over 1000 available properties, including dielectric
constant, Poisson's ratio, glass transition temperature, dissipation factor, and Vicat softening point.
</td><td><a href="https://proto3000.com"><img src="/images/assets/proto3000.jpg" border = "0"><br>

Proto3000 - Rapid Prototyping</a>
</td></tr></table>

<p/><span class="hiText">Notes:</span>

<ul>
  <li>Visit MatWeb's <a href="/search/PropertySearch.aspx">property-based search page</a> to limit your results to materials that meet specific property performance criteria.</li>
  <li>Text searches can be done from any page - use the <b>Quick Search</b> box in the top right of the page.</li>
  <li>More <a href="/help/strategy.aspx"><strong>Search Strategies</strong></a> on how to find information in MatWeb.</li>
   <li><a href="/search/GetAllMatls.aspx">Show All Materials</a> - an inefficient drill-down to data sheets.  Registered users who are logged in omit a step in this drill down process.</li>
</ul>

<p/><a href="#Top">Top</a>

<script type="text/javascript">

	var txtMatGroupText = document.getElementById("ctl00_ContentMain_txtMatGroupText");
	var txtMatGroupID = document.getElementById("ctl00_ContentMain_txtMatGroupID");
	var divMatGroupName = document.getElementById("ctl00_ContentMain_divMatGroupName");

	function ucMatGroupFinder1_OnSelected(MatGroupID,MatGroupText){
		txtMatGroupText.value = MatGroupText;
		txtMatGroupID.value = MatGroupID;
		divMatGroupName.innerHTML = MatGroupText;
		//__doPostBack("ctl00$ContentMain$btnSubmit","");
	}

	function ucMatGroupTree_OnNodeClick(NodeText,MatGroupID){
		txtMatGroupText.value = NodeText;
		txtMatGroupID.value = MatGroupID;
		divMatGroupName.innerHTML = NodeText;
		//__doPostBack("ctl00$ContentMain$btnSubmit","");
	}
	
	function validate(){
		if (txtMatGroupID.value == ""){
			matweb.alert("Please select a material category","User Input Error");
			return false;
		}
		
		return true;
		
	}

</script>


</div>
<!-- ===================================== END MAIN CONTENT ========================================== -->

<table class="tabletight" style="width:100%" >
        <tr>
                <td align="center" class="footer" colspan="3">
                        <br /><br />
                
                        <a href="/membership/regupgrade.aspx" class="footlink"><span class="subscribeLink">Subscribe to Premium Services</span></a><br/>
                
                        <span class="footer"><b>Searches:</b>&nbsp;&nbsp;</span>
                        <a href="/search/AdvancedSearch.aspx" class="footlink"><span class="foot">Advanced</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/search/CompositionSearch.aspx" class="footlink"><span class="foot">Composition</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/search/PropertySearch.aspx" class="footlink"><span class="foot">Property</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/search/MaterialGroupSearch.aspx" class="footlink"><span class="foot">Material&nbsp;Type</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/search/SearchManufacturerName.aspx" class="footlink"><span class="foot">Manufacturer</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/search/SearchTradeName.aspx" class="footlink"><span class="foot">Trade&nbsp;Name</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/search/SearchUNS.aspx" class="footlink"><span class="foot">UNS Number</span></a>
                        <br />
                        <span class="footer"><b>Other&nbsp;Links:</b>&nbsp;&nbsp;</span>
                        <a href="/services/advertising.aspx" class="footlink"><span class="foot">Advertising</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/services/submitdata.aspx" class="footlink"><span class="foot">Submit&nbsp;Data</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/services/databaselicense.aspx" class="footlink"><span class="foot">Database&nbsp;Licensing</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/services/webhosting.aspx" class="footlink"><span class="foot">Web&nbsp;Design&nbsp;&amp;&nbsp;Hosting</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/clickthrough.aspx?addataid=277" class="footlink"><span class="foot">Trade&nbsp;Publications</span></a>

                        <br />
                        <a href="/reference/suppliers.aspx" class="footlink"><span class="foot">Supplier&nbsp;List</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/tools/unitconverter.aspx" class="footlink"><span class="foot">Unit&nbsp;Converter</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/tools/contents.aspx#reference" class="footlink"><span class="foot">Reference</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/reference/link.aspx" class="footlink"><span class="foot">Links</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/help/help.aspx" class="footlink"><span class="foot">Help</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/services/contact.aspx" class="footlink"><span class="foot">Contact&nbsp;Us</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/tools/contents.aspx" class="footlink"><span class="foot" style="color:red;">Site&nbsp;Map</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/reference/faq.aspx" class="footlink"><span class="foot">FAQ</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/index.aspx" class="footlink"><span class="foot">Home</span></a>
                        <br />&nbsp;
                        <div id="fb-root"></div>

<table><tr><td><script src="http://connect.facebook.net/en_US/all.js#xfbml=1"></script><fb:like-box href="https://www.facebook.com/MatWeb.LLC" width="175" show_faces="false" stream="false" header="false"></fb:like-box>

</td><td>

<a href="https://twitter.com/MatWeb" class="twitter-follow-button" data-show-count="false" data-show-screen-name="false">Follow @MatWeb</a> <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>

</td></tr></table>
                </td>
        </tr>
        <tr>
                <td colspan="3"><img src="/images/bluebar.gif" width="100%" height="5" alt="" /></td>
        </tr>
        <tr>
                <td style="background-image:url(/images/gray.gif);"><img src="/images/spacer.gif" width="5" height="1" alt=""/></td>
                <td style="background-image:url(/images/gray.gif);">
                        <span class="footer">
                        Please read our <a href="/reference/terms.aspx" class="footlink"><span class="foot">License Agreement</span></a> regarding materials data and our  <a href="/reference/privacy.aspx" class="footlink"><span class="foot">Privacy Policy</span></a>.
                        Questions or comments about MatWeb? Please contact us at
                        <a href="mailto:webmaster@matweb.com" class="footlink"><span class="foot">webmaster@matweb.com</span></a>. We appreciate your input.
                        <br /><br />
                        The contents of this web site, the MatWeb logo, and "MatWeb" are Copyright 1996-2024
                        by MatWeb, LLC. MatWeb is intended for personal, non-commercial use. The contents, results, and technical data from this site
                        may not be reproduced either electronically, photographically or substantively without permission from MatWeb, LLC.
                        </span>
                        <br />
                </td>

                <td style="background-image:url(/images/gray.gif);"><img src="/images/spacer.gif" width="5" height="1" alt=""/></td>
        </tr>
</table>




<script type="text/javascript">
//<![CDATA[
var ctl00_ContentMain_ucMatGroupTree_msTreeView_ImageArray =  new Array('', '', '', '/WebResource.axd?d=zLp3QJeZSj4-fsnL2_raV70Mk3pr-OdjL_rvrZzXUTCnZ6LU5Dg_Tc5XZg4eZpT9O0gxZJrJcxGb23XJofeGD9JhYLr2wl0VkQZnJOx0T0ShBosq0&t=638313619312541215', '/WebResource.axd?d=vFualGN2ZZ4fny_RX_gYiUTY6FBLUapne4VfY6P3sGiyrOYd-Ji2sWg_olT-exaztendUUEOoH87RS2l44bpZTscfmmv4MKRqJK8rgesmkrNfP2O0&t=638313619312541215', '/WebResource.axd?d=4nLCbQ2Yfc2i_msrQXqKzK8pLQD-GrNVZ7DeXU-La2qatczrgeCLazv0RzIkDDHyYfLFalCBO8s71lRS8yifxTChF9dUJvTLxNEeeFlghyuY0dtM0&t=638313619312541215', '/WebResource.axd?d=uEI6Enq15_q4pXO6g0E7vtAqtD5WIokAEQ2Vz8V3Yi36cefediLfyCwENkPd-w1GxTXu-HkNqcDlrYOw8bxgGqeHJtzc-PBlTa9nrZRfdIyj8fQh0&t=638313619312541215', '/WebResource.axd?d=hzeik1gGhZrfDZXUWCMsBOx5b43bIFor8UCTzm_wuh92b9Z-WQU4_fQCSUw6CaP5tnc9LTU0jl0sy4Y6Cf_c8q8HFXRqeHQuscXIqqLNSZkgl2cu0&t=638313619312541215', '/WebResource.axd?d=6v5qM6VKQYD3XNwwXEy7Tix7dzVdqDJ8QTRIlUPrzXQqNsXQgUwIfmn34ZzRORd_IZj0bF4vkZzID1HuSFE5uKyLgaPYeAYE8VT8HYfb3Rk6zsDw0&t=638313619312541215', '/WebResource.axd?d=d9nh8nEM1YCqIfE9jLzNhPyFIZqARvlFExwnFG0vbvskO-A2qORlhTyUVIxDtSwldGrOp8o9ef8lZ9SRhPQ9rv9TkGV-NVYvH9_yHT6CvWpIPF4S0&t=638313619312541215', '/WebResource.axd?d=7_V5n1KYSQsj6PoSqgIxb0egBsAa91jSXPkbmM6FaiF14-JsNnpUDnfSdxZcvfd0OQCLDtC24pMIXigO-41cL2rsq-0xea-SSG7yZUyRcliFFqe70&t=638313619312541215', '/WebResource.axd?d=TinC6igX1vPDgWh7DstWwBTHTyH4uuybDU4NW47jJD06wHdAO3YyJC_eKK1Zn5oIbwqRo4Cri8YwhA82G-LD0p14yEF1EfoIZvKleYMIuQ2K1hkE0&t=638313619312541215', '/WebResource.axd?d=4B0dvJqmzY4ukTboQq7lmxokRL9NTsoWh4yErB11Tb-ucvMw_p_0mxYVn4U1Uz6Jk795umrO0C4-B-8Yq1Kxxz6uyDT15O2BSCb8349f4YWDdIVy0&t=638313619312541215', '/WebResource.axd?d=TUiibr9hjS_MlNf09dKZECe1z56fbaPbfgJfC5eYJi0TtdBviUcA1JQJLV81aqunzpXysgruhL3ihpgd7ziL5p9SQ86xVLNiUqbYpJnW9Rj7sN1E0&t=638313619312541215', '/WebResource.axd?d=vUBrM2nNhECNbFO0AJKq52p2zWrY5XG8nWgCDL4AfsrexStb0HREDV6GHMvdkAGDInxYtTyWXuxFUIiQv3JX2fScVRwQsrVv5srHrjOIeC3ayAG20&t=638313619312541215', '/WebResource.axd?d=Z-LP5lKcsnv2A9UpExXu-5leQYIAZMYp6SXNognZi1r66YT8u24-YjJaxjisE9ruYU8NKnNz72H4shclsk8PM3S5M-VYMk_F_G5fcd3ra6Sj9CaS0&t=638313619312541215', '/WebResource.axd?d=QI4SACqRn7PxBBHPfkR7peFMAEv7lX1_iZ_7jncKwxRTlvhSAncd5K2oRqw8J8WmJjWQcbGgEblhrufy-hUjZMa1YLUC47mjfV1eS4LM3kUqLT960&t=638313619312541215', '/WebResource.axd?d=ayrFqNSzdiTAdowDVbEH-ZzKbvw5bCq7onyCSAcJhCVPVIHlUB9M_fo8BLUTeGpbtcoMiyFy1MoeEIsweMrpVandBKC4vAK-NCBcfn2FIreK8mEJ0&t=638313619312541215', '/WebResource.axd?d=OK_uWiuoGURaJ3CC0fVlQ7v9GUnIAkx03w03rQ2ENqcwdObLiQwK7B9yRFyAWj9tuUqsa0874ksgkaRo8KirB7q1ZL_huh7gCq_XehXfHMEKMiAl0&t=638313619312541215');
//]]>
</script>


<script type="text/javascript">
//<![CDATA[
(function() {var fn = function() {AjaxControlToolkit.ModalPopupBehavior.invokeViaServer('ctl00_ContentMain_ucPopupMessage1_ModalPopupExtender1', false); Sys.Application.remove_load(fn);};Sys.Application.add_load(fn);})();
var callBackFrameUrl='/WebResource.axd?d=Mt7MojvAyTqAEcx0MpKgcF_QVa09tLyMQS6c5JLNO4j2M_F6CMxYnN0HdVYxyrhggOpkTwyNqQ51iS6UoHXgJqHBROE1&t=638313619312541215';
WebForm_InitCallback();var ctl00_ContentMain_ucMatGroupTree_msTreeView_Data = new Object();
ctl00_ContentMain_ucMatGroupTree_msTreeView_Data.images = ctl00_ContentMain_ucMatGroupTree_msTreeView_ImageArray;
ctl00_ContentMain_ucMatGroupTree_msTreeView_Data.collapseToolTip = "Collapse {0}";
ctl00_ContentMain_ucMatGroupTree_msTreeView_Data.expandToolTip = "Expand {0}";
ctl00_ContentMain_ucMatGroupTree_msTreeView_Data.expandState = theForm.elements['ctl00_ContentMain_ucMatGroupTree_msTreeView_ExpandState'];
ctl00_ContentMain_ucMatGroupTree_msTreeView_Data.selectedNodeID = theForm.elements['ctl00_ContentMain_ucMatGroupTree_msTreeView_SelectedNode'];
for (var i=0;i<19;i++) {
var preLoad = new Image();
if (ctl00_ContentMain_ucMatGroupTree_msTreeView_ImageArray[i].length > 0)
preLoad.src = ctl00_ContentMain_ucMatGroupTree_msTreeView_ImageArray[i];
}
ctl00_ContentMain_ucMatGroupTree_msTreeView_Data.lastIndex = 8;
ctl00_ContentMain_ucMatGroupTree_msTreeView_Data.populateLog = theForm.elements['ctl00_ContentMain_ucMatGroupTree_msTreeView_PopulateLog'];
ctl00_ContentMain_ucMatGroupTree_msTreeView_Data.treeViewID = 'ctl00$ContentMain$ucMatGroupTree$msTreeView';
ctl00_ContentMain_ucMatGroupTree_msTreeView_Data.name = 'ctl00_ContentMain_ucMatGroupTree_msTreeView_Data';
Sys.Application.initialize();
Sys.Application.add_init(function() {
    $create(AjaxControlToolkit.ModalPopupBehavior, {"BackgroundCssClass":"modalBackground","OkControlID":"ctl00_ContentMain_ucPopupMessage1_btnOK","PopupControlID":"ctl00_ContentMain_ucPopupMessage1_divPopupMessage","PopupDragHandleControlID":"ctl00_ContentMain_ucPopupMessage1_trTitleRow","dynamicServicePath":"/search/MaterialGroupSearch.aspx","id":"ctl00_ContentMain_ucPopupMessage1_ModalPopupExtender1"}, null, null, $get("ctl00_ContentMain_ucPopupMessage1_hndPopupControl"));
});
Sys.Application.add_init(function() {
    $create(AjaxControlToolkit.TextBoxWatermarkBehavior, {"ClientStateFieldID":"ctl00_ContentMain_UcMatGroupFinder1_TextBoxWatermarkExtender2_ClientState","WatermarkCssClass":"greyOut","WatermarkText":"Type at least 4 characters here...","id":"ctl00_ContentMain_UcMatGroupFinder1_TextBoxWatermarkExtender2"}, null, null, $get("ctl00_ContentMain_UcMatGroupFinder1_txtSearchText"));
});
//]]>
</script>
</form>


<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-15290815-1");
pageTracker._setDomainName(".matweb.com");
pageTracker._trackPageview();
} catch(err) {}</script>

<!-- 20160905 insert SpecialChem javascript -->
<script type="text/javascript" src="//collect.specialchem.com/collect.js"></script>
<script type="text/javascript">
    //<![CDATA[
    var _spc = (_spc || []);
    _spc.push(['init',
    {
        partner: 'MatWeb'
        
        //, iid: '12345'
    }]);
    //]]>
</script>
<script> (function(){ var s = document.createElement('script'); var h = document.querySelector('head') || document.body; s.src = 'https://acsbapp.com/apps/app/dist/js/app.js'; s.async = true; s.onload = function(){ acsbJS.init({ statementLink : '', footerHtml : '', hideMobile : false, hideTrigger : false, disableBgProcess : false, language : 'en', position : 'right', leadColor : '#146FF8', triggerColor : '#146FF8', triggerRadius : '50%', triggerPositionX : 'right', triggerPositionY : 'bottom', triggerIcon : 'people', triggerSize : 'bottom', triggerOffsetX : 20, triggerOffsetY : 20, mobile : { triggerSize : 'small', triggerPositionX : 'right', triggerPositionY : 'bottom', triggerOffsetX : 20, triggerOffsetY : 20, triggerRadius : '20' } }); }; h.appendChild(s); })();</script>
</body>
</html>
