

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head id="ctl00_mainHeader"><title>
	Metal, Plastic, and Ceramic Search Index
</title><meta http-equiv="Content-Style-Type" content="text/css" /><meta http-equiv="Content-Language" content="en" />

        <script src="/includes/flash.js" type="text/javascript"></script>

        <script src="/includes/prototype.js" type="text/javascript"></script>
        <!-- script src="/includes/scriptaculous/effects.js" type="text/javascript"></script -->
        <script src="/includes/matweb.js" type="text/javascript"></script>
        <link rel="stylesheet" href="/includes/main.css" />

        <script type="text/javascript">

                function _onload(){
                        matweb.debug = false; // ;
                        //matweb.writeOverlay();
                }

                function _onkeydown(evt){
                        //PREVENT GLOBAL AUTO-SUBMIT OF FORMS
                        return matweb.DisableAutoSubmit(evt);
                }

                //EXAMPLE USAGE:
                //matweb.register_onload(function(){alert("hello from onload event");});
                //matweb.writeOverlay();


        </script>

        <style type="text/css" media="screen">
        @import url("/ad_IES/css/styles.css");
        </style>
<meta name="KEYWORDS" content="carbon steels, low alloy steels, corrosion resistant stainless, high temperature stainless, heat resistant stainless steels, AISI steel alloy specifications, high strength steel alloys, tensile strength of steels, maraging, superalloy, lead alloy, magnesium, tin alloy, tungsten, zinc, precious metal, pure metallic element, TPE elastomer, dielectric constant, plastic dissipation factor, nylon glass transition temperature, ionomer vicat softening point, poisson's ratio, polyetherimide, fluoropolymer, silicone, polyester dielectric constant" /><meta name="DESCRIPTION" content="Searchable list of plastics, metals, and ceramics categorized into a fields like polypropylene, nylon, ABS, aluminum alloys, oxides, etc.  Search results are detailed data sheets with tensile strength, density, dielectric constant, dissipation factor, poisson's ratio, glass transition temperature, and Vicat softening point." /><meta name="ROBOTS" content="NOFOLLOW" /><style type="text/css">
	.ctl00_ContentMain_ucMatGroupTree_msTreeView_0 { text-decoration:none; }
	.ctl00_ContentMain_ucMatGroupTree_msTreeView_1 { color:Black; }
	.ctl00_ContentMain_ucMatGroupTree_msTreeView_2 { padding:0px 3px 0px 3px; }

</style></head>

<body onload="_onload();" onkeydown="return _onkeydown(event);">

<a id="Top"></a>

<div id="divSiteName" style="position:absolute; top:0; left:0 ;">
<a href="/index.aspx"><img src="/images/sitelive.gif" alt="" /></a>
</div>

<div id="divPopupOverlay" style="display:none;"></div>

<div id="divPopupAlert" class="popupAlert" style="display:none;">
        <table style="width:100%; height:100%; border-style:outset; border-width:5px; border-color:blue;" >
                <tr><th style=" height:25px; background-color:Maroon; font-size:14px; color:White; "><span id="spanPopupAlertTitle">System Message</span></th></tr>
                <tr><td style=" vertical-align:top; padding:3px; background-color:White;" id="tdPopupAlertMessage" ></td></tr>
                <tr><td style=" height:25px; text-align:center; background-color:Silver;"><input type="button" id="btnPopupAlert" onclick="matweb.alertClose();" style="" value="OK" /></td></tr>
        </table>
</div>

<form name="aspnetForm" method="post" action="MaterialGroupSearch.aspx" onsubmit="javascript:return WebForm_OnSubmit();" id="aspnetForm">
<div>
<input type="hidden" name="__EVENTTARGET" id="__EVENTTARGET" value="" />
<input type="hidden" name="__EVENTARGUMENT" id="__EVENTARGUMENT" value="" />
<input type="hidden" name="__LASTFOCUS" id="__LASTFOCUS" value="" />
<input type="hidden" name="ctl00_ContentMain_ucMatGroupTree_msTreeView_ExpandState" id="ctl00_ContentMain_ucMatGroupTree_msTreeView_ExpandState" value="ccccccnc" />
<input type="hidden" name="ctl00_ContentMain_ucMatGroupTree_msTreeView_SelectedNode" id="ctl00_ContentMain_ucMatGroupTree_msTreeView_SelectedNode" value="" />
<input type="hidden" name="ctl00_ContentMain_ucMatGroupTree_msTreeView_PopulateLog" id="ctl00_ContentMain_ucMatGroupTree_msTreeView_PopulateLog" value="" />
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="/wEPDwULLTE2Nzc3MDc2MDAPZBYCZg9kFgICAQ9kFioCAQ9kFgJmDxYCHgRUZXh0BWdEYXRhIHNoZWV0cyBmb3Igb3ZlciA8c3BhbiBjbGFzcz0iZ3JlZXRpbmciPjE4MCwwMDA8L3NwYW4+ICBtZXRhbHMsIHBsYXN0aWNzLCBjZXJhbWljcywgYW5kIGNvbXBvc2l0ZXMuZAICD2QWAmYPFgIfAAUESE9NRWQCAw9kFgJmDxYCHwAFBlNFQVJDSGQCBA9kFgJmDxYCHwAFBVRPT0xTZAIFD2QWAmYPFgIfAAUJU1VQUExJRVJTZAIGD2QWAmYPFgIfAAUHRk9MREVSU2QCBw9kFgJmDxYCHwAFCEFCT1VUIFVTZAIID2QWAmYPFgIfAAUDRkFRZAIJD2QWAmYPFgIfAAUHQUNDT1VOVGQCCg9kFgJmDxYCHwAFBkxPRyBJTmQCCw8QZGQWAWZkAgwPZBYCZg8WAh8ABQhTZWFyY2hlc2QCDQ9kFgJmDxYCHwAFCEFkdmFuY2VkZAIOD2QWAmYPFgIfAAUIQ2F0ZWdvcnlkAg8PZBYCZg8WAh8ABQhQcm9wZXJ0eWQCEA9kFgJmDxYCHwAFBk1ldGFsc2QCEQ9kFgJmDxYCHwAFClRyYWRlIE5hbWVkAhIPZBYCZg8WAh8ABQxNYW51ZmFjdHVyZXJkAhMPZBYCAgEPZBYCZg8WAh8ABRlSZWNlbnRseSBWaWV3ZWQgTWF0ZXJpYWxzZAIVD2QWEAIDD2QWAmYPFgIfAAWXATxhIGhyZWY9Ii9jbGlja3Rocm91Z2guYXNweD9hZGRhdGFpZD0xMzUxIj48aW1nIHNyYz0iL2ltYWdlcy9hc3NldHMvbWV0YWxtZW4tTmlja2VsLnBuZyIgYm9yZGVyPTAgYWx0PSJNZXRhbG1lbiBTYWxlcyIgd2lkdGggPSAiNzI4IiBoZWlnaHQgPSAiOTAiPjwvYT5kAgQPZBYCAgIPFgIeBXN0eWxlBRlkaXNwbGF5Om5vbmU7d2lkdGg6MjUwcHg7FgYCAQ9kFgJmD2QWAmYPDxYCHwAFElVzZXIgSW5wdXQgUHJvYmxlbWRkAgMPDxYCHwBlZGQCBQ8WAh4FdmFsdWUFAk9LZAIFD2QWBAIBDw9kFgIfAQUMV2lkdGg6MzA0cHg7ZAICDxAWBB8BBStwb3NpdGlvbjphYnNvbHV0ZTtkaXNwbGF5Om5vbmU7V2lkdGg6MzA4cHg7HgRzaXplBQIxOGRkZAIGDw8WBB4IQ2xpZW50SUQCAR4KTGFuZ3VhZ2VJRAIBZBYCZg8UKwAJDxYOHhdQb3B1bGF0ZU5vZGVzRnJvbUNsaWVudGceEkVuYWJsZUNsaWVudFNjcmlwdGceCVNob3dMaW5lc2ceCE5vZGVXcmFwZx4NTmV2ZXJFeHBhbmRlZGQeDFNlbGVjdGVkTm9kZWQeCUxhc3RJbmRleAIIZBYIHgtOb2RlU3BhY2luZxsAAAAAAAAAAAEAAAAeCUZvcmVDb2xvcgojHhFIb3Jpem9udGFsUGFkZGluZxsAAAAAAAAIQAEAAAAeBF8hU0IChIAYZGRkZGRkFCsACQUjMjowLDA6MCwwOjEsMDoyLDA6MywwOjQsMDo1LDA6NiwwOjcUKwACFgwfAAUSQ2FyYm9uICg4NjYgbWF0bHMpHgVWYWx1ZQUDMjgzHghFeHBhbmRlZGgeDFNlbGVjdEFjdGlvbgsqLlN5c3RlbS5XZWIuVUkuV2ViQ29udHJvbHMuVHJlZU5vZGVTZWxlY3RBY3Rpb24CHgtOYXZpZ2F0ZVVybAVBamF2YXNjcmlwdDp1Y01hdEdyb3VwVHJlZV9Pbk5vZGVDbGljaygnQ2FyYm9uICg4NjYgbWF0bHMpJywnMjgzJykeEFBvcHVsYXRlT25EZW1hbmRnZBQrAAIWDB8ABRVDZXJhbWljICgxMDAwNCBtYXRscykfEQUCMTEfEmgfEwsrBAIfFAVDamF2YXNjcmlwdDp1Y01hdEdyb3VwVHJlZV9Pbk5vZGVDbGljaygnQ2VyYW1pYyAoMTAwMDQgbWF0bHMpJywnMTEnKR8VZ2QUKwACFgwfAAUSRmx1aWQgKDc1NjIgbWF0bHMpHxEFATUfEmgfEwsrBAIfFAU/amF2YXNjcmlwdDp1Y01hdEdyb3VwVHJlZV9Pbk5vZGVDbGljaygnRmx1aWQgKDc1NjIgbWF0bHMpJywnNScpHxVnZBQrAAIWDB8ABRNNZXRhbCAoMTcwNTIgbWF0bHMpHxEFATkfEmgfEwsrBAIfFAVAamF2YXNjcmlwdDp1Y01hdEdyb3VwVHJlZV9Pbk5vZGVDbGljaygnTWV0YWwgKDE3MDUyIG1hdGxzKScsJzknKR8VZ2QUKwACFgwfAAUnT3RoZXIgRW5naW5lZXJpbmcgTWF0ZXJpYWwgKDgwNjMgbWF0bHMpHxEFAzI4NR8SaB8TCysEAh8UBVZqYXZhc2NyaXB0OnVjTWF0R3JvdXBUcmVlX09uTm9kZUNsaWNrKCdPdGhlciBFbmdpbmVlcmluZyBNYXRlcmlhbCAoODA2MyBtYXRscyknLCcyODUnKR8VZ2QUKwACFgwfAAUVUG9seW1lciAoOTc2MzUgbWF0bHMpHxEFAjEwHxJoHxMLKwQCHxQFQ2phdmFzY3JpcHQ6dWNNYXRHcm91cFRyZWVfT25Ob2RlQ2xpY2soJ1BvbHltZXIgKDk3NjM1IG1hdGxzKScsJzEwJykfFWdkFCsAAhYMHwAFGFB1cmUgRWxlbWVudCAoNTA3IG1hdGxzKR8RBQMxODQfEmgfEwsrBAIfFAVHamF2YXNjcmlwdDp1Y01hdEdyb3VwVHJlZV9Pbk5vZGVDbGljaygnUHVyZSBFbGVtZW50ICg1MDcgbWF0bHMpJywnMTg0JykfFWhkFCsAAhYMHwAFJVdvb2QgYW5kIE5hdHVyYWwgUHJvZHVjdHMgKDM5OCBtYXRscykfEQUDMjc3HxJoHxMLKwQCHxQFVGphdmFzY3JpcHQ6dWNNYXRHcm91cFRyZWVfT25Ob2RlQ2xpY2soJ1dvb2QgYW5kIE5hdHVyYWwgUHJvZHVjdHMgKDM5OCBtYXRscyknLCcyNzcnKR8VZ2RkAgcPFgIeCWlubmVyaHRtbAUZTmlja2VsIEFsbG95ICgxMjEwIG1hdGxzKWQCDQ8PFgIeB1Zpc2libGVoZGQCDg8PFgIfF2dkFgQCAQ8PFgIfAAUEMTI0NWRkAgMPDxYCHwAFDE5pY2tlbCBBbGxveWRkAg8PDxYGHgtDdXJyZW50UGFnZQIBHg9DdXJyZW50U2VhcmNoSUQCsfODEx8XZ2QWBAICDw8WAh8AZWRkAgMPDxYCHxdnZBY8AgEPDxYCHwAFBDEyNDVkZAIDDxBkEBUFATEBMgEzATQBNRUFATEBMgEzATQBNRQrAwVnZ2dnZxYBZmQCBQ8PFgIfAAUBN2RkAgcPDxYGHw4KTh4HRW5hYmxlZGgfEAIEZGQCCQ8PFgQfDgojHxACBGRkAgsPEGRkFgECBWQCDw8WAh8XaGQCEQ8QD2QWAh4Ib25jaGFuZ2UFE1NldFNlbGVjdGVkRm9sZGVyKClkZGQCFQ8WAh8XaBYCAgEPEGRkFgFmZAIXD2QWBAIBDw8WBB8AZR8aaGRkAgIPDxYCHxdoZGQCGQ8PFgQfAAUNTWF0ZXJpYWwgTmFtZR8aaGRkAhsPDxYCHxdoZGQCHQ8PFgIfF2hkZAIfD2QWBgIBDw8WBB8ABQVQcm9wMR8aaGRkAgMPDxYCHxdoZGQCBQ8PFgIfF2hkZAIhD2QWBgIBDw8WBB8ABQVQcm9wMh8aaGRkAgMPDxYCHxdoZGQCBQ8PFgIfF2hkZAIjD2QWBgIBDw8WBB8ABQVQcm9wMx8aaGRkAgMPDxYCHxdoZGQCBQ8PFgIfF2hkZAIlD2QWBgIBDw8WBB8ABQVQcm9wNB8aaGRkAgMPDxYCHxdoZGQCBQ8PFgIfF2hkZAInD2QWBgIBDw8WBB8ABQVQcm9wNR8aaGRkAgMPDxYCHxdoZGQCBQ8PFgIfF2hkZAIpD2QWBgIBDw8WBB8ABQVQcm9wNh8aaGRkAgMPDxYCHxdoZGQCBQ8PFgIfF2hkZAIrD2QWBgIBDw8WBB8ABQVQcm9wNx8aaGRkAgMPDxYCHxdoZGQCBQ8PFgIfF2hkZAItD2QWBgIBDw8WBB8ABQVQcm9wOB8aaGRkAgMPDxYCHxdoZGQCBQ8PFgIfF2hkZAIvD2QWBgIBDw8WBB8ABQVQcm9wOR8aaGRkAgMPDxYCHxdoZGQCBQ8PFgIfF2hkZAIxD2QWBgIBDw8WBB8ABQZQcm9wMTAfGmhkZAIDDw8WAh8XaGRkAgUPDxYCHxdoZGQCNQ8PFgIfAAUEMTI0NWRkAjcPEGQQFQUBMQEyATMBNAE1FQUBMQEyATMBNAE1FCsDBWdnZ2dnFgFmZAI5Dw8WAh8ABQE3ZGQCOw8PFgYfDgpOHxpoHxACBGRkAj0PDxYEHw4KIx8QAgRkZAI/DxBkZBYBAgVkAkEPFgIfF2cWAgIBDw8WAh8ABbgEPGJyIC8+DQpNYXRlcmlhbHMgZmxhZ2dlZCBhcyBkaXNjb250aW51ZWQgKDxpbWcgc3JjPSIvaW1hZ2VzL2J1dHRvbnMvaWNvbkRpc2NvbnRpbnVlZC5qcGciIGFsdD0iIiAvPikgYXJlIG5vIGxvbmdlciBwYXJ0IG9mIHRoZSBtYW51ZmFjdHVyZXLigJlzIHN0YW5kYXJkIHByb2R1Y3QgbGluZSBhY2NvcmRpbmcgdG8gb3VyIGxhdGVzdCBpbmZvcm1hdGlvbi4gIFRoZXNlIG1hdGVyaWFscyBtYXkgYmUgYXZhaWxhYmxlIGJ5IHNwZWNpYWwgb3JkZXIsIGluIGRpc3RyaWJ1dGlvbiBpbnZlbnRvcnksIG9yIHJlaW5zdGF0ZWQgYXMgYW4gYWN0aXZlIHByb2R1Y3QuICBEYXRhIHNoZWV0cyBmcm9tIG1hdGVyaWFscyB0aGF0IGFyZSBubyBsb25nZXIgYXZhaWxhYmxlIHJlbWFpbiBpbiBNYXRXZWIgdG8gYXNzaXN0IHVzZXJzIGluIGZpbmRpbmcgcmVwbGFjZW1lbnQgbWF0ZXJpYWxzLiAgDQo8YnIgLz48YnIgLz4NClVzZXJzIG9mIG91ciBBZHZhbmNlZCBTZWFyY2ggKHJlZ2lzdHJhdGlvbiByZXF1aXJlZCkgbWF5IGV4Y2x1ZGUgZGlzY29udGludWVkIG1hdGVyaWFscyBmcm9tIHNlYXJjaCByZXN1bHRzLmRkAhYPZBYCZg8WAh8ABXA8YSBocmVmPSIvY2xpY2t0aHJvdWdoLmFzcHg/YWRkYXRhaWQ9Mjc3IiBjbGFzcz0iZm9vdGxpbmsiPjxzcGFuIGNsYXNzPSJmb290Ij5UcmFkZSZuYnNwO1B1YmxpY2F0aW9uczwvc3Bhbj48L2E+ZBgBBR5fX0NvbnRyb2xzUmVxdWlyZVBvc3RCYWNrS2V5X18WBAU2Y3RsMDAkQ29udGVudE1haW4kVWNNYXRHcm91cEZpbmRlcjEkc2VsZWN0Q2F0ZWdvcnlMaXN0BStjdGwwMCRDb250ZW50TWFpbiR1Y01hdEdyb3VwVHJlZSRtc1RyZWVWaWV3BRtjdGwwMCRDb250ZW50TWFpbiRidG5TdWJtaXQFGmN0bDAwJENvbnRlbnRNYWluJGJ0blJlc2V0cQ2cbAcd3U3+TkG9a+Sgd/sm/CM=" />
</div>

<script type="text/javascript">
//<![CDATA[
var theForm = document.forms['aspnetForm'];
if (!theForm) {
    theForm = document.aspnetForm;
}
function __doPostBack(eventTarget, eventArgument) {
    if (!theForm.onsubmit || (theForm.onsubmit() != false)) {
        theForm.__EVENTTARGET.value = eventTarget;
        theForm.__EVENTARGUMENT.value = eventArgument;
        theForm.submit();
    }
}
//]]>
</script>


<script src="/WebResource.axd?d=M68BSyLvwBMHRYzsSV62mvczr7w5VyKnq2JbgqcAAU1ioThlC6FNEVBncBdaA4oqf8cXPzy7QERzajvFfM67hSR6S0w1&amp;t=638313619312541215" type="text/javascript"></script>


<script src="/WebResource.axd?d=ClzPoKUn1iQnMGy7NeMJw70TvMayZ5w9nNmB7h_dY64i4BFFgjVcppyp69BFqhuzHctzc-rEantAjczzQ6UVMYkaWJw1&amp;t=638313619312541215" type="text/javascript"></script>
<script type="text/javascript">
//<![CDATA[

    function TreeView_PopulateNodeDoCallBack(context,param) {
        WebForm_DoCallback(context.data.treeViewID,param,TreeView_ProcessNodeData,context,TreeView_ProcessNodeData,false);
    }
var ctl00_ContentMain_ucMatGroupTree_msTreeView_Data = null;//]]>
</script>

<script src="/ScriptResource.axd?d=148LWaywfmset9PYVR0m-KYXX9JfyGYGqOtIYBGEAzoJh5VhFBwSI6JxnmoNvgtlAprhvntpc6lTDbRMgdKxckUmc8m2qw_fOCsX0G9LC8ojUpw4cTHWXgfuT_vU6m2Kpv3mt01iCczrTE2-J5X6EeBa2281&amp;t=637769794779625837" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=v_V1UgDrDAchg8Zuc1FL4qkvmjt-bwap7ZxwVGGPleYiJ6zfDOTL6wtCmVvILjbwo_sqb2PMUTNkjO4vp6nd-E7WWXTPgSI5-nvn3YDnHrVk5bgVX4Qv6F0fV4wTSslmaLZ3k98tckojDLWAEI7jm8h0H4d7BoGYxkqsyh8-Iujfrjjm0&amp;t=637769794779625837" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=V7Zma3AISNyGTbHVRPw8svJ4Ub7xUUaeoKwImcvnuDiy-XBR331l3eTjiEP8J_QlX2qgyAHxZFLywNbNyZ50fJLVEOCjx11lIDJOiQxc0oGA3qi7XXiggiwEcIWwzb9j2Is1n1D9sU40Zusk35AtkqRKL3Y1&amp;t=633177911400000000" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=wOguQo_yd3hCG4ajcSXPC6-XdeL0hmYWSMaFjvMXt1CBqR9neJqTXyTfPki4lIz4B29Zz_83btvS12BY6KpBKT-AgP0eaXp7V3h6XRdjiozV5w_n3oQRZg5Yq1QW8UNGj0Cy9jsap_7EcU2M99CapUyVG6I1&amp;t=633177911400000000" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=MAJxSy62Z3EtRwHXuuMc8KC6PFmrPqmkpcwdpcPGeqtqUQyJ3FBQ3cFyYWN1zNID_ngA53aMZdG_dpRu3TRDH8-iqKAjPAcc2BWe63WYGrGbyHc9syJxR6lypTgvRaYmwwEmkLEsaGUniMurHc16yr1VrjmzG3HWOeu9UXNr23Wv2xzh0&amp;t=633177911400000000" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=ukx9eyyt483y_CGgCF1OToQPWzPYyCynhpMvhxN7idzW3WaKjHjpgAOcMgtXZIEcspnbyDEADdRWCSHB-hF39Ycl13KWsqdlOHElw2qgSkGmkL8iNH37htb0BNS0lxw6oHvtqYiVpw-nZeYoQ1NkgX2qXOI1&amp;t=633177911400000000" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=Ovkr-MWDypjnq0C7FVbNgOs-Mt3NaktNVf8SEOX721PoQ5mQqTM6S4igSQL1_PXvh96km3stLiqrorOgm8nnnUU6y6edTkhxmLiwfFtEXQEAAnFFQpUepgsxEd0ahMvWcDfEac0EE7J8iDb2rqSMTxk51ydfY-c-8zcKvo5Mrn83cvfN0&amp;t=633177911400000000" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=pKQF4iQYwkSn5cAwTNDM8ez4TLepowWnai-WD6NVL5h0ay9Qtt0a9vPCaI-KN9OHcv1fL8VnwxEDM6EAVtgZY06V8XJTlVrbmb8WlMvGmhwBeL3_SVtKKOS-QExY8AnVbTFl_wOknplhy8bz-9sIWNCh4XPq1WhRj7ALNAKaeruPhS7d0&amp;t=633177911400000000" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=uM6eUuIuIa_NiCgVrtwgqczDeqTqI37XalY8Ri_W2ZjXEzIKi84afVBjFk8gneIDXcFnw8yjCD1qhE8mzDTzAUSdKnbDte0yC23kMoyvxPyu7vRH9E-6JMZtE8-gELUPmoZdOZJ9nXitHKYBLw2P6skU1IkzFRegK74bETu2FdEtuaeR0&amp;t=633177911400000000" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=ewF5FMb1ckbNCdJ9rpE5xMSXy-WecqVGE8uma1_520KusuW7jTUE2tnPMG7JyR0FDIFqDUMQbsfTZlsQ0sD4YAW8MnN90GKIMPvzaP7eOG3ss8E050_l7Rpnaz7I_U3ITzBXkcJZIOPcI1PxOReT8FSeWmo1&amp;t=633177911400000000" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=3lB_EBCMPyiwzDhzWdoE-i4mLHhkIMbbkaLwMr-U1WcAoDqBHS-oYFn4gz3ZwnpzK_KZp97Ev8ZbiUNo7WMJ45h2xgOH59erMFo1BpNb87-yu3X0mFyWs5rB3ANm4lsVSIu7RZd7XUEwfZXzugJ3CoJqxvRqiTGLpcXquEF24hK3OUH-0&amp;t=633177911400000000" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=Z7-pZSKJ6vCyr9H9S4r9baB7TLiJ-T4EoMOvlH-iQ5-icaO-vWMrjXk3EPctuPrcO5qwJSTaLQtHyguwle7Kty78exrSwHataIriNUEu1DnUkixv4pxU7Dknp_GDVHby5XcMA4lMwazAE0AFG9Cndu0An_gAGACGlYXsGUG9W6okCQsH0&amp;t=633177911400000000" type="text/javascript"></script>
<script src="../WebServices/Materials.asmx/js" type="text/javascript"></script>
<script src="../WebServices/MatGroups.asmx/js" type="text/javascript"></script>
<script src="../WebServices/Folders.asmx/js" type="text/javascript"></script>
<script type="text/javascript">
//<![CDATA[
function WebForm_OnSubmit() {
null;
return true;
}
//]]>
</script>

<div>

	<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="640EFF8C" />
</div>

<!--
The ScriptManager control is required for all ASP AJAX functionality - especially used by search pages.
It generates the javascript required.
Only one script manager is allowed on a page, and since this is the master page, this is the main script manager
However, a ScriptManagerProxy control can be used to set properties and settings on indivual pages.
For an example, see /WebControls/ucSearchResults.ascx
-->
<script type="text/javascript">
//<![CDATA[
Sys.WebForms.PageRequestManager._initialize('ctl00$ajxScriptManager', document.getElementById('aspnetForm'));
Sys.WebForms.PageRequestManager.getInstance()._updateControls([], [], [], 90);
//]]>
</script>


<table class="tabletight" style="width:100%; text-align:center;background-image: url(/images/gray.gif);">
  <tr >
    <td rowspan="2"><a href="/index.aspx"><img src="/images/logo1.gif" width="270" height="42" alt="MatWeb - Material Property Data"  /></a></td>

                
                <td style="text-align:right;vertical-align:middle; width:90%" ><span class='greeting'><span class='hiText'><a href ='/services/advertising.aspx'>Advertise with MatWeb!</a></span>&nbsp;&nbsp;&nbsp;&nbsp;</span></td>
                <td style="text-align:right;vertical-align:middle; padding-right:5px;" ><a href='/membership/regstart.aspx'><img src='/images/buttons/btnRegisterBSF.gif' width="62" height="20" alt="Register Now" /></a></td>

                
  </tr>
  <tr >
    <td  align="left" valign="bottom" colspan="2"><span class="tagline">
    
    Data sheets for over <span class="greeting">180,000</span>  metals, plastics, ceramics, and composites.</span></td>
  </tr>
  <tr style="background-image:url(/images/blue.gif); background-color:#000099; " >
    <td bgcolor="#000099"><a href="/index.aspx"><img src="/images/logo2.gif" width="270" height="23px" alt="MatWeb - Material Property Data" border="0" /></a></td>
    <td class="topnavig8" style=" background-color:#000099;vertical-align:middle; text-align:right; white-space:nowrap;" colspan="2" align="right" valign="middle">
        <a href="/index.aspx" class="topnavlink">HOME</a>&nbsp;&nbsp;&#8226;&nbsp;&nbsp;
                        <a href="/search/search.aspx" class="topnavlink">SEARCH</a>&nbsp;&nbsp;&#8226;&nbsp;&nbsp;
                        <a href="/tools/tools.aspx" class="topnavlink">TOOLS</a>&nbsp;&nbsp;&#8226;&nbsp;&nbsp;
                        <a href="/reference/suppliers.aspx" class="topnavlink">SUPPLIERS</a>&nbsp;&nbsp;&#8226;&nbsp;&nbsp;
                        <a href="/folders/ListFolders.aspx" class="topnavlink">FOLDERS</a>&nbsp;&nbsp;&#8226;&nbsp;&nbsp;
                        <a href="/services/services.aspx" class="topnavlink">ABOUT US</a>&nbsp;&nbsp;&#8226;&nbsp;&nbsp;
                        <a href="/reference/faq.aspx" class="topnavlink">FAQ</a>&nbsp;&nbsp;&#8226;&nbsp;&nbsp;
                        
                        <a href="/membership/login.aspx" class="topnavlink">LOG IN</a>
                        
                        &nbsp;
                        
                        &nbsp;
    </td>
  </tr>
</table>

<div id="divRecentMatls" class="tableborder tight" style=" display:none; position:absolute; top:95px; left:150px; width:400px; height:400px;" onmouseover="clearTimeout(tmrtmp);" onmouseout="tmrtmp=setTimeout('RecentMatls.Hide()', 800)">
        <table class="headerblock tabletight" style="vertical-align:top; font-size:13px; width:100%;"><tr>
                <td style="padding:3px;"> Recently Viewed Materials (most recent at top)</td>
                <td style="padding:3px;text-align:right;"><input type="image" src="/images/buttons/btnCloseWhite.gif" onclick="RecentMatls.Hide();return false;" style="cursor:pointer;" />&nbsp;</td>
        </tr></table>
        <div id="divRecentMatlsBody" style="height:375px; overflow:auto; background-color:White;">

                <table id="tblRecentMatls" class="tabledataformat tableloose" style="background-color:White;">
    
                <tr><td>
                <p />
                <a href="/membership/login.aspx">Login</a> to see your most recently viewed materials here.<p />
                Or if you don't have an account with us yet, then <a href="/membership/regstart.aspx">click here to register.</a>
                </td></tr>
                
                </table>

        </div>
</div>

<script type="text/javascript">

        var RecentMatls = new _RecentMatls();
        var tmrtmp;

        function _RecentMatls(){

                var divRecentMatls;
                var tblRecentMatls;
                var trRecentMatlsRowTemplate;
                var trRecentMatlsNoData;
                var DataIsLoaded=false;

                //Using prototype.js methods...
                divRecentMatls = $("divRecentMatls");
                tblRecentMatls = $("tblRecentMatls");
    

                divRecentMatls.style.display = "none";

                this.Toggle = function(){
                        //alert("Toggle()");
                        if(divRecentMatls.style.display == "block"){this.Hide();}
                        else{this.Show();}
                }

                this.Show = function(){
                        //alert("Show()");
                        divRecentMatls.style.display = "block";
                        matweb.toggleSelect(divRecentMatls,0);
                        if(!DataIsLoaded){
            
                        }
                }

                this.Hide = function(){
                        //alert("Hide()");
                        divRecentMatls.style.display = "none";
                        matweb.toggleSelect(divRecentMatls,1);
                }

    

        }//end class

</script>

<table class="tabletight t_ableborder t_ablegrid" style="width:100%;" >
  <tr>
   <td style="width:100%; height:27px; white-space:nowrap; text-align:left; vertical-align:middle;" class="tagline" >
      <span class="navlink"><b>&nbsp;&nbsp;Searches:</b></span>
      &nbsp;&nbsp;<a href="/search/AdvancedSearch.aspx" class="navlink"><span class="nav"><font color="#CC0000">Advanced</font></span></a>
      &nbsp;|&nbsp;<a href="/search/MaterialGroupSearch.aspx" class="navlink"><span class="nav">Category</span></a>
      &nbsp;|&nbsp;<a href="/search/PropertySearch.aspx" class="navlink"><span class="nav">Property</span></a>
      &nbsp;|&nbsp;<a href="/search/CompositionSearch.aspx" class="navlink"><span class="nav">Metals</span></a>
      &nbsp;|&nbsp;<a href="/search/SearchTradeName.aspx" class="navlink"><span class="nav">Trade Name</span></a>
      &nbsp;|&nbsp;<a href="/search/SearchManufacturerName.aspx" class="navlink"><span class="nav">Manufacturer</span></a>
                &nbsp;|&nbsp;<a href="javascript:RecentMatls.Toggle();" id="ctl00_lnkRecentMatls" class="navlink"><span class="nav"><font color="#CC0000">Recently Viewed Materials</font></span></a>
    </td>

    <td style="text-align:right; vertical-align:bottom; padding-right:5px;">

      <table class="tabletight t_ableborder" ><tr>
        <td style=" white-space:nowrap; vertical-align:bottom; " >
                                        &nbsp;&nbsp;
                                        <input name="ctl00$txtQuickText" type="text" id="ctl00_txtQuickText" style="width:100px;" maxlength="40" class="quicksearchinput" onkeydown="txtQuickText_OnKeyDown(event);" />
                                        <input type="image" id="btnQuickTextSearch" style="vertical-align:top; width:60px; height:25px;" src="/images/buttons/btnSearch.gif" alt="Search" onclick="return btnQuickTextSearch_Click()"  />
                                        <script type="text/javascript">

                                                //SETUP THE AUTOPOST ON ENTER KEY EVENT FOR txtQuickText FIELD
                                                //document.getElementById("ctl00_txtQuickText").onkeydown=txtQuickText_OnKeyDown;

                                                function txtQuickText_OnKeyDown(evt){
                                                        //SUBMIT THE FORM AS IF THE BUTTON WAS PUSHED
                                                        //var UniqueID = "";
                                                        var txtQuickText = document.getElementById('ctl00_txtQuickText');
                                                        if(matweb.IsEnterKey(evt))
                                                                if(checkSearchText(txtQuickText))
                                                                        //matweb.DoPostBack(UniqueID, "");
                                                                        window.location.href="/search/QuickText.aspx?SearchText=" + escape(txtQuickText.value);
                                                        return false;
                                                }

                                                function btnQuickTextSearch_Click(){
                                                        var txtQuickText = document.getElementById('ctl00_txtQuickText');
                                                        if(checkSearchText(txtQuickText))
                                                                window.location.href="/search/QuickText.aspx?SearchText=" + escape(txtQuickText.value);
                                                        return false;
                                                }

                                                function checkSearchText(searchbox){
                                                        searchbox.value = trim(searchbox.value);
                                                        if(searchbox.value == ''){
                                                                //alert('Please enter something to search for');
                                                                matweb.alert('Please enter something to search for', 'Search Error');
                                                                //searchbox.focus();
                                                                return false;
                                                        }
                                                        return true;
                                                }

                                                function trim(s)  { return ltrim(rtrim(s)) }
                                                function ltrim(s) { return s.replace(/^\s+/g, "") }
                                                function rtrim(s) { return s.replace(/\s+$/g, "") }

                                        </script>
                                </td>
                                </tr>
                        </table>

    </td>
  </tr>
  <tr style="background-image:url(/images/shad.gif);height:7px;" ><td colspan="2"></td></tr>
</table>

<!-- ====================================== MAIN CONTENT ============================================= -->
<div style="vertical-align:top; padding-left:10px; padding-right:10px;" >





	<center><a href="/clickthrough.aspx?addataid=1351"><img src="/images/assets/metalmen-Nickel.png" border=0 alt="Metalmen Sales" width = "728" height = "90"></a>
</center>
	
	<h2><a href="#help"><img src="/images/buttons/iconHelp.gif" alt="" style="vertical-align:bottom;" /></a>Material Category Search</h2>
	
	

<div id="ctl00_ContentMain_ucPopupMessage1_divPopupMessage" class="divPopupAlert" style="display:none;width:250px;">
	<table >
		<tr id="ctl00_ContentMain_ucPopupMessage1_trTitleRow" class="divPopupAlert_TitleRow" style="cursor:move;">
	<th><span id="ctl00_ContentMain_ucPopupMessage1_lblTitle">User Input Problem</span></th>
</tr>

		<tr class="divPopupAlert_ContentRow" ><td style="" ><span id="ctl00_ContentMain_ucPopupMessage1_lblMessage"></span></td></tr>
		<tr class="divPopupAlert_ButtonRow"><td><input name="ctl00$ContentMain$ucPopupMessage1$btnOK" type="button" id="ctl00_ContentMain_ucPopupMessage1_btnOK" value="OK" /></td></tr>
	</table>
</div>

<input type="hidden" name="ctl00$ContentMain$ucPopupMessage1$hndPopupControl" id="ctl00_ContentMain_ucPopupMessage1_hndPopupControl" />


	<table class="" style="width:100%;">
	<tr>
		<td style=" vertical-align:top; width:300px;" >
			<strong>Find a Material Category:</strong>
			

<input name="ctl00$ContentMain$UcMatGroupFinder1$txtSearchText" type="text" value="Nickel" id="ctl00_ContentMain_UcMatGroupFinder1_txtSearchText" style="Width:304px;" /><br />
<select name="ctl00$ContentMain$UcMatGroupFinder1$selectCategoryList" id="ctl00_ContentMain_UcMatGroupFinder1_selectCategoryList" style="position:absolute;display:none;Width:308px;" size="18">
</select>

<input type="hidden" name="ctl00$ContentMain$UcMatGroupFinder1$TextBoxWatermarkExtender2_ClientState" id="ctl00_ContentMain_UcMatGroupFinder1_TextBoxWatermarkExtender2_ClientState" />

<script type="text/javascript">

var ucMatGroupFinderManager = new _ucMatGroupFinderManager("ctl00_ContentMain_UcMatGroupFinder1_txtSearchText","ctl00_ContentMain_UcMatGroupFinder1_selectCategoryList");

function _ucMatGroupFinderManager(txtSearchTextID,selectCategoryListID){

	var txtSearchText = document.getElementById(txtSearchTextID);
	var selectCategoryList = document.getElementById(selectCategoryListID);
	var showtime;
	var blurtime;
	var MinTextLength = 4;
	var KeyStrokeActionDelay = 650;
	var FadeInOutDelay = 0.2;
	var LastUsedText;
	
	//The init function is currently run at the bottom of this class
	this.init=function(){

		txtSearchText.onfocus = txtSearchText_onfocus;
		txtSearchText.onkeyup = txtSearchText_checktext;
		txtSearchText.onblur = selectCategoryList_startblur;
		
		selectCategoryList.onfocus = selectCategoryList_clearblur;
		selectCategoryList.onchange = selectCategoryList_onchange;
		selectCategoryList.onblur = selectCategoryList_startblur;

	}

	txtSearchText_onfocus = function(){
		if(txtSearchText.value == "Type at least 4 characters here...")
			txtSearchText.value = "";
		selectCategoryList_clearblur();
		txtSearchText_checktext();
	}

	function txtSearchText_checktext(){
		searchText = txtSearchText.value;
		//alert(searchText);
		clearTimeout(showtime);
		if(searchText.length >= MinTextLength){
			showtime = setTimeout(function(){
				selectCategoryList.style.display = "block";
				if(txtSearchText.value!=LastUsedText){
					LastUsedText = searchText;
					aci.matweb.WebServices.MatGroups.FindMatGroups(searchText,DisplayCatData,matweb.WebServiceErrorHandler,"txtSearchText_checktext");
				}
				//AjaxControlToolkit.Animation.FadeInAnimation.play(selectCategoryList, FadeInOutDelay, 1, 25, 0, 1, true);
			},KeyStrokeActionDelay);
		}
		else{
			LastUsedText = "";
			showtime = setTimeout(function(){
				selectCategoryList.options.length = 0;
				selectCategoryList.style.display = "none";
				//AjaxControlToolkit.Animation.FadeOutAnimation.play(selectCategoryList,FadeInOutDelay);
				//AjaxControlToolkit.Animation.FadeOutAnimation.play(selectCategoryList,FadeInOutDelay, 25, 0, 1, true);
			},KeyStrokeActionDelay);
		}
	}

	function selectCategoryList_clearblur(){
		clearTimeout(blurtime);
	}

	function selectCategoryList_onchange(){
		var MatGroupID = selectCategoryList.options[selectCategoryList.selectedIndex].value;
		var MatGroupText = selectCategoryList.options[selectCategoryList.selectedIndex].text;
		//alert("Selected MatGroupID: " + MatGroupID);
		//alert("Selected MatGroupText: " + MatGroupText);
		//When no data is found, we have a "No Categories Contain Text..." message, but the MatGroupID is set to 0, so we can ignore if MatGroupID == 0.
		if(MatGroupID > 0){
			ucMatGroupFinder1_OnSelected(MatGroupID,MatGroupText)
		}
		selectCategoryList_startblur();
		//txtSearchText.value = "";
	}
	
	function selectCategoryList_startblur(){
		//alert("selectCategoryList_onblur()");
		clearTimeout(blurtime);
		blurtime = setTimeout(function(){
			selectCategoryList.style.display = "none";
			//AjaxControlToolkit.Animation.FadeOutAnimation.play(selectCategoryList,FadeInOutDelay, 25, 0, 1, true);
		},KeyStrokeActionDelay);
	}
	
	function DisplayCatData(MatGroupData){
		//alert("GetCatData()");
		//alert("vwMaterialGroupList: " + vwMaterialGroupList);
		var MatGroupID,GroupName, MatCount;
		selectCategoryList.options.length = 0;//clear any existing options....
		if(MatGroupData.length==0){
				//alert(vwMaterialGroupList[i].GroupName);
				MatGroupID = 0;
				GroupName = "No categories contain the text: " + txtSearchText.value
				selectCategoryList.options[0] = new Option(GroupName,MatGroupID);					
		}
		else{
			for(i=0;i<MatGroupData.length;i++){
				//alert(vwMaterialGroupList[i].GroupName);
				MatCount = MatGroupData[i].MatCount;
				MatGroupID = MatGroupData[i].MatGroupID;
				GroupName = MatGroupData[i].GroupName + " (" + MatCount + " matls)";
				selectCategoryList.options[i] = new Option(GroupName,MatGroupID);					
			}
		}
	}
	
	this.init();

}//end class
	
</script>



			<strong>Select a Material Category:</strong>
			
			<div class="tableborder" style=" height:225px; width:300px; overflow:scroll; ">
			<a href="#ctl00_ContentMain_ucMatGroupTree_msTreeView_SkipLink"><img alt="Skip Navigation Links." src="/WebResource.axd?d=vAkieGj3ugBpu6CKp7dqvCP5iHLzPbUd2XME-h1vQcC6d5hLg7Vc1kNVzB8A45Ui3K9XZ9m3-E2pxsaJQYh2uMkAul41&amp;t=638313619312541215" width="0" height="0" style="border-width:0px;" /></a><div id="ctl00_ContentMain_ucMatGroupTree_msTreeView">
	<table cellpadding="0" cellspacing="0" style="border-width:0;">
		<tr>
			<td><a id="ctl00_ContentMain_ucMatGroupTree_msTreeViewn0" href="javascript:TreeView_PopulateNode(ctl00_ContentMain_ucMatGroupTree_msTreeView_Data,0,document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewn0'),document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewt0'),null,'r','Carbon (866 matls)','283','f','','f')"><img src="/WebResource.axd?d=6v5qM6VKQYD3XNwwXEy7Tix7dzVdqDJ8QTRIlUPrzXQqNsXQgUwIfmn34ZzRORd_IZj0bF4vkZzID1HuSFE5uKyLgaPYeAYE8VT8HYfb3Rk6zsDw0&amp;t=638313619312541215" alt="Expand Carbon (866 matls)" style="border-width:0;" /></a></td><td class="ctl00_ContentMain_ucMatGroupTree_msTreeView_2"><a class="ctl00_ContentMain_ucMatGroupTree_msTreeView_0 ctl00_ContentMain_ucMatGroupTree_msTreeView_1" href="javascript:ucMatGroupTree_OnNodeClick('Carbon (866 matls)','283')" id="ctl00_ContentMain_ucMatGroupTree_msTreeViewt0">Carbon (866 matls)</a></td>
		</tr><tr style="height:0px;">
			<td></td>
		</tr>
	</table><table cellpadding="0" cellspacing="0" style="border-width:0;">
		<tr style="height:0px;">
			<td></td>
		</tr><tr>
			<td><a id="ctl00_ContentMain_ucMatGroupTree_msTreeViewn1" href="javascript:TreeView_PopulateNode(ctl00_ContentMain_ucMatGroupTree_msTreeView_Data,1,document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewn1'),document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewt1'),null,'t','Ceramic (10004 matls)','11','f','','f')"><img src="/WebResource.axd?d=TinC6igX1vPDgWh7DstWwBTHTyH4uuybDU4NW47jJD06wHdAO3YyJC_eKK1Zn5oIbwqRo4Cri8YwhA82G-LD0p14yEF1EfoIZvKleYMIuQ2K1hkE0&amp;t=638313619312541215" alt="Expand Ceramic (10004 matls)" style="border-width:0;" /></a></td><td class="ctl00_ContentMain_ucMatGroupTree_msTreeView_2"><a class="ctl00_ContentMain_ucMatGroupTree_msTreeView_0 ctl00_ContentMain_ucMatGroupTree_msTreeView_1" href="javascript:ucMatGroupTree_OnNodeClick('Ceramic (10004 matls)','11')" id="ctl00_ContentMain_ucMatGroupTree_msTreeViewt1">Ceramic (10004 matls)</a></td>
		</tr><tr style="height:0px;">
			<td></td>
		</tr>
	</table><table cellpadding="0" cellspacing="0" style="border-width:0;">
		<tr style="height:0px;">
			<td></td>
		</tr><tr>
			<td><a id="ctl00_ContentMain_ucMatGroupTree_msTreeViewn2" href="javascript:TreeView_PopulateNode(ctl00_ContentMain_ucMatGroupTree_msTreeView_Data,2,document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewn2'),document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewt2'),null,'t','Fluid (7562 matls)','5','f','','f')"><img src="/WebResource.axd?d=TinC6igX1vPDgWh7DstWwBTHTyH4uuybDU4NW47jJD06wHdAO3YyJC_eKK1Zn5oIbwqRo4Cri8YwhA82G-LD0p14yEF1EfoIZvKleYMIuQ2K1hkE0&amp;t=638313619312541215" alt="Expand Fluid (7562 matls)" style="border-width:0;" /></a></td><td class="ctl00_ContentMain_ucMatGroupTree_msTreeView_2"><a class="ctl00_ContentMain_ucMatGroupTree_msTreeView_0 ctl00_ContentMain_ucMatGroupTree_msTreeView_1" href="javascript:ucMatGroupTree_OnNodeClick('Fluid (7562 matls)','5')" id="ctl00_ContentMain_ucMatGroupTree_msTreeViewt2">Fluid (7562 matls)</a></td>
		</tr><tr style="height:0px;">
			<td></td>
		</tr>
	</table><table cellpadding="0" cellspacing="0" style="border-width:0;">
		<tr style="height:0px;">
			<td></td>
		</tr><tr>
			<td><a id="ctl00_ContentMain_ucMatGroupTree_msTreeViewn3" href="javascript:TreeView_PopulateNode(ctl00_ContentMain_ucMatGroupTree_msTreeView_Data,3,document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewn3'),document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewt3'),null,'t','Metal (17052 matls)','9','f','','f')"><img src="/WebResource.axd?d=TinC6igX1vPDgWh7DstWwBTHTyH4uuybDU4NW47jJD06wHdAO3YyJC_eKK1Zn5oIbwqRo4Cri8YwhA82G-LD0p14yEF1EfoIZvKleYMIuQ2K1hkE0&amp;t=638313619312541215" alt="Expand Metal (17052 matls)" style="border-width:0;" /></a></td><td class="ctl00_ContentMain_ucMatGroupTree_msTreeView_2"><a class="ctl00_ContentMain_ucMatGroupTree_msTreeView_0 ctl00_ContentMain_ucMatGroupTree_msTreeView_1" href="javascript:ucMatGroupTree_OnNodeClick('Metal (17052 matls)','9')" id="ctl00_ContentMain_ucMatGroupTree_msTreeViewt3">Metal (17052 matls)</a></td>
		</tr><tr style="height:0px;">
			<td></td>
		</tr>
	</table><table cellpadding="0" cellspacing="0" style="border-width:0;">
		<tr style="height:0px;">
			<td></td>
		</tr><tr>
			<td><a id="ctl00_ContentMain_ucMatGroupTree_msTreeViewn4" href="javascript:TreeView_PopulateNode(ctl00_ContentMain_ucMatGroupTree_msTreeView_Data,4,document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewn4'),document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewt4'),null,'t','Other Engineering Material (8063 matls)','285','f','','f')"><img src="/WebResource.axd?d=TinC6igX1vPDgWh7DstWwBTHTyH4uuybDU4NW47jJD06wHdAO3YyJC_eKK1Zn5oIbwqRo4Cri8YwhA82G-LD0p14yEF1EfoIZvKleYMIuQ2K1hkE0&amp;t=638313619312541215" alt="Expand Other Engineering Material (8063 matls)" style="border-width:0;" /></a></td><td class="ctl00_ContentMain_ucMatGroupTree_msTreeView_2"><a class="ctl00_ContentMain_ucMatGroupTree_msTreeView_0 ctl00_ContentMain_ucMatGroupTree_msTreeView_1" href="javascript:ucMatGroupTree_OnNodeClick('Other Engineering Material (8063 matls)','285')" id="ctl00_ContentMain_ucMatGroupTree_msTreeViewt4">Other Engineering Material (8063 matls)</a></td>
		</tr><tr style="height:0px;">
			<td></td>
		</tr>
	</table><table cellpadding="0" cellspacing="0" style="border-width:0;">
		<tr style="height:0px;">
			<td></td>
		</tr><tr>
			<td><a id="ctl00_ContentMain_ucMatGroupTree_msTreeViewn5" href="javascript:TreeView_PopulateNode(ctl00_ContentMain_ucMatGroupTree_msTreeView_Data,5,document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewn5'),document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewt5'),null,'t','Polymer (97635 matls)','10','f','','f')"><img src="/WebResource.axd?d=TinC6igX1vPDgWh7DstWwBTHTyH4uuybDU4NW47jJD06wHdAO3YyJC_eKK1Zn5oIbwqRo4Cri8YwhA82G-LD0p14yEF1EfoIZvKleYMIuQ2K1hkE0&amp;t=638313619312541215" alt="Expand Polymer (97635 matls)" style="border-width:0;" /></a></td><td class="ctl00_ContentMain_ucMatGroupTree_msTreeView_2"><a class="ctl00_ContentMain_ucMatGroupTree_msTreeView_0 ctl00_ContentMain_ucMatGroupTree_msTreeView_1" href="javascript:ucMatGroupTree_OnNodeClick('Polymer (97635 matls)','10')" id="ctl00_ContentMain_ucMatGroupTree_msTreeViewt5">Polymer (97635 matls)</a></td>
		</tr><tr style="height:0px;">
			<td></td>
		</tr>
	</table><table cellpadding="0" cellspacing="0" style="border-width:0;">
		<tr style="height:0px;">
			<td></td>
		</tr><tr>
			<td><img src="/WebResource.axd?d=7_V5n1KYSQsj6PoSqgIxb0egBsAa91jSXPkbmM6FaiF14-JsNnpUDnfSdxZcvfd0OQCLDtC24pMIXigO-41cL2rsq-0xea-SSG7yZUyRcliFFqe70&amp;t=638313619312541215" alt="" /></td><td class="ctl00_ContentMain_ucMatGroupTree_msTreeView_2"><a class="ctl00_ContentMain_ucMatGroupTree_msTreeView_0 ctl00_ContentMain_ucMatGroupTree_msTreeView_1" href="javascript:ucMatGroupTree_OnNodeClick('Pure Element (507 matls)','184')" id="ctl00_ContentMain_ucMatGroupTree_msTreeViewt6">Pure Element (507 matls)</a></td>
		</tr><tr style="height:0px;">
			<td></td>
		</tr>
	</table><table cellpadding="0" cellspacing="0" style="border-width:0;">
		<tr style="height:0px;">
			<td></td>
		</tr><tr>
			<td><a id="ctl00_ContentMain_ucMatGroupTree_msTreeViewn7" href="javascript:TreeView_PopulateNode(ctl00_ContentMain_ucMatGroupTree_msTreeView_Data,7,document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewn7'),document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewt7'),null,'l','Wood and Natural Products (398 matls)','277','f','','t')"><img src="/WebResource.axd?d=vUBrM2nNhECNbFO0AJKq52p2zWrY5XG8nWgCDL4AfsrexStb0HREDV6GHMvdkAGDInxYtTyWXuxFUIiQv3JX2fScVRwQsrVv5srHrjOIeC3ayAG20&amp;t=638313619312541215" alt="Expand Wood and Natural Products (398 matls)" style="border-width:0;" /></a></td><td class="ctl00_ContentMain_ucMatGroupTree_msTreeView_2"><a class="ctl00_ContentMain_ucMatGroupTree_msTreeView_0 ctl00_ContentMain_ucMatGroupTree_msTreeView_1" href="javascript:ucMatGroupTree_OnNodeClick('Wood and Natural Products (398 matls)','277')" id="ctl00_ContentMain_ucMatGroupTree_msTreeViewt7">Wood and Natural Products (398 matls)</a></td>
		</tr><tr style="height:0px;">
			<td></td>
		</tr>
	</table>
</div><a id="ctl00_ContentMain_ucMatGroupTree_msTreeView_SkipLink"></a>
<span id="ctl00_ContentMain_ucMatGroupTree_lblDebug"></span>

			</div>
			
			<strong>Selected Material Category: </strong>
			<div id="ctl00_ContentMain_divMatGroupName" class="selectedItem" style="width:306px;">Nickel Alloy (1210 matls)</div>
			<input name="ctl00$ContentMain$txtMatGroupID" type="hidden" id="ctl00_ContentMain_txtMatGroupID" value="193" />
			<input name="ctl00$ContentMain$txtMatGroupText" type="hidden" id="ctl00_ContentMain_txtMatGroupText" value="Nickel Alloy (1210 matls)" />
			
			

			<input type="image" name="ctl00$ContentMain$btnSubmit" id="ctl00_ContentMain_btnSubmit" src="../images/buttons/btnFind.gif" alt="Find" style="border-width:0px;" /> <!-- OnClientClick="return validate()" -->
			<input type="image" name="ctl00$ContentMain$btnReset" id="ctl00_ContentMain_btnReset" title="Clear Search" src="/images/buttons/btnReset.gif" alt="Clear Search" style="border-width:0px;" />

			<span id="ctl00_ContentMain_lblDebug"></span>

		</td>
		<td>&nbsp;</td>
		<td style=" vertical-align:top; width:80%;" >
			<strong>Search Results</strong>
			<div class="tableborder" id="divSearchResults" style="padding:5px;" >
				
				<div id="ctl00_ContentMain_pnlSearchResults">
	
					There are <span id="ctl00_ContentMain_lblMatlCount">1245</span> materials in the category 
					<span id="ctl00_ContentMain_lblMaterialGroupName" class="selectedItem">Nickel Alloy</span>.
					If your material is not listed, please refer to our <a href="/help/strategy.aspx">search strategy</a> page for assistance in limiting your search.
				
</div>
			</div>
			<p />




<div id="ctl00_ContentMain_UcSearchResults1_pnlResultsFound" class="tableborder">
	

	<table style="background-color:Silver; width:100%;" class="" >
		<tr>
			<td><strong>Found <span id="ctl00_ContentMain_UcSearchResults1_lblResultCount">1245</span> Results</strong> -- 
				Page 
				<select name="ctl00$ContentMain$UcSearchResults1$drpPageSelect1" onchange="javascript:setTimeout('__doPostBack(\'ctl00$ContentMain$UcSearchResults1$drpPageSelect1\',\'\')', 0)" id="ctl00_ContentMain_UcSearchResults1_drpPageSelect1">
		<option selected="selected" value="1">1</option>
		<option value="2">2</option>
		<option value="3">3</option>
		<option value="4">4</option>
		<option value="5">5</option>

	</select>
				of <span id="ctl00_ContentMain_UcSearchResults1_lblPageTotal">7</span> 
				-- 
				<a id="ctl00_ContentMain_UcSearchResults1_lnkPrevPage" disabled="disabled" style="color:Gray;">[Prev Page]</a>
				<a id="ctl00_ContentMain_UcSearchResults1_lnkNextPage" href="javascript:__doPostBack('ctl00$ContentMain$UcSearchResults1$lnkNextPage','')" style="color:Black;">[Next Page]</a>
				--&nbsp;			
				view
				<select name="ctl00$ContentMain$UcSearchResults1$drpPageSize1" onchange="javascript:setTimeout('__doPostBack(\'ctl00$ContentMain$UcSearchResults1$drpPageSize1\',\'\')', 0)" id="ctl00_ContentMain_UcSearchResults1_drpPageSize1">
		<option value="25">25</option>
		<option value="50">50</option>
		<option value="75">75</option>
		<option value="100">100</option>
		<option value="150">150</option>
		<option selected="selected" value="200">200</option>

	</select> per page
			</td>
		</tr>
		
	</table>
	
	
	
	
	<table class="t_ablegrid" style="width:100%;">
		<tr style="font-size:9px;">
			<td style="font-size:9px;">Use Folder</td>
			<td style="font-size:9px;">Contains</td>
			<td style="font-size:9px;">&nbsp;</td>
			
			<td style="font-size:9px;"><div id="divFolderName" style="display:none;">New Folder Name</div></td>
			<td style="font-size:9px;"></td>
			<td style="width:90%"></td>
		</tr>
		<tr style="vertical-align:top;">
			<td style="vertical-align:top;">
				<select name="ctl00$ContentMain$UcSearchResults1$drpFolderList" id="ctl00_ContentMain_UcSearchResults1_drpFolderList" onchange="SetSelectedFolder()" style="width:125px;">
		<option selected="selected" value="0">My Folder</option>

	</select>
			</td>
			<td style="vertical-align:top;">
				<strong><input name="ctl00$ContentMain$UcSearchResults1$txtFolderMatCount" type="text" id="ctl00_ContentMain_UcSearchResults1_txtFolderMatCount" readonly="readonly" style="border:none 0 white; background-color:White; color:Black; width: 35px; font-weight:bold;" value="0/0" /></strong>
			</td>
			<td>
				
				<input type="image" id="btnCompareFolders" src="/images/buttons/btnCompareMatls.gif" alt="Compare Checked Materials" onclick="CompareMaterials();return false;" />
			</td>
			
			<td style="white-space:nowrap;vertical-align:top;">
			
				<table id="tblFolderName" class="tabletight" style="display:none;"><tr><td><input type="text" id="txtFolderName2" style="display:inline; width:100px;" /><br /></td>
				<td><input type="image" id="btnApplyFolderName2" src="/images/buttons/btnSaveChanges.gif" style="display:inline;" onclick="ApplyFolderName();return false;" />
				<input type="image" id="btnHideFolderNameControls2" src="/images/buttons/btnCancel.gif" style="display:inline;" onclick="HideFolderNameControls();return false;" /></td>
				</tr></table>
				
			</td>
			<td style="white-space:nowrap;">
				<div class="usermessage" id="divUserMessage"></div>
			</td>
		</tr>
	</table>

	<table class="tabledataformat t_ablegrid" id="tblResults" style="table-layout:auto;"  >

		<tr class="">

			
		
			<th style="width:65px; white-space:nowrap;">Select</th>

			<th style="width:10px;">
				<!--Discontinued and Found Via Interpolated Indicators -->
			</th>
			
			<th style="width:auto;">
				<a id="ctl00_ContentMain_UcSearchResults1_lnkSortMatName" disabled="disabled" title="Click to Sort By Material Name">Material Name</a>
				<br />
				
				
			</th>
		
			
			
			

			

			

			

			

			

			

			

			
		</tr>

		<!-- this is where the search results are actually loaded.  See code behind -->		
		<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_71"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;1</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_71' href="/search/DataSheet.aspx?MatGUID=e6eb83327e534850a062dbca3bc758dc" >Nickel, Ni</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_183297"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;2</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_183297' href="/search/DataSheet.aspx?MatGUID=70ace0c8e25c45e78e1025e3998384e0" >3M Techical Ceramics EKagrip® 10 Friction-enhancing electroless nickel diamond coating</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_183298"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;3</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_183298' href="/search/DataSheet.aspx?MatGUID=4b16b515be924531a115863bff3dbd80" >3M Techical Ceramics EKagrip® 25 Friction-enhancing electroless nickel diamond coating</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_183299"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;4</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_183299' href="/search/DataSheet.aspx?MatGUID=5d888c4d53a24809852851ff6a580c77" >3M Techical Ceramics EKagrip® 35 Friction-enhancing electroless nickel diamond coating</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_1246"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;5</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_1246' href="/search/DataSheet.aspx?MatGUID=0e159f5bce6642438cea66bdc796fb94" >H.C. Starck Amperit® 205.5 Dense Coated Nickel Graphite 75-25 (Ni-C)</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_1249"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;6</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_1249' href="/search/DataSheet.aspx?MatGUID=f478aaf45191400b90bf274edac091f7" >H.C. Starck Amperit® 410.1 Gas Atomized (NiCoCrAlY)</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_1250"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;7</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_1250' href="/search/DataSheet.aspx?MatGUID=3c960c75a3614e51962480874e24c82c" >H.C. Starck Amperit® 413.3 Gas Atomized (NiCoCrAlY)</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_1251"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;8</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_1251' href="/search/DataSheet.aspx?MatGUID=56bfa91ddf574222bedf452a6fcfd6d9" >H.C. Starck Amperit® 413.6 Gas Atomized (NiCoCrAlY)</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_1252"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;9</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_1252' href="/search/DataSheet.aspx?MatGUID=f41c52ed32444ae29bd14a53f51a008b" >H.C. Starck Amperit® 415.072 CoNiCrAlY (MCrAlY)</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_1253"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;10</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_1253' href="/search/DataSheet.aspx?MatGUID=320df897002849c39edb8439360410e4" >H.C. Starck Amperit® 415.079 CoNiCrAlY (MCrAlY)</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_1254"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;11</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_1254' href="/search/DataSheet.aspx?MatGUID=fcef0f27ba4343caa6f0528097393cec" >H.C. Starck Amperit® 415.1 CoNiCrAlY (MCrAlY)</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_1255"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;12</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_1255' href="/search/DataSheet.aspx?MatGUID=1877401ec2b1483bb0a5feb3f016ea8f" >H.C. Starck Amperit® 415.2 CoNiCrAlY (MCrAlY)</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_1256"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;13</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_1256' href="/search/DataSheet.aspx?MatGUID=9ba7504a1dff42adbebf766e79a736dd" >H.C. Starck Amperit® 415.6 CoNiCrAlY (MCrAlY)</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_1257"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;14</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_1257' href="/search/DataSheet.aspx?MatGUID=f237e05c0b8d4b90ae5da7aafbd70c46" >H.C. Starck Amperit® 421.087 NiCoCrAlTaReY (MCrAlY)</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_1258"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;15</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_1258' href="/search/DataSheet.aspx?MatGUID=4b31612b18324c90b6b93edfd89f8f07" >H.C. Starck Amperit® 421.1 NiCoCrAlTaReY (MCrAlY)</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_203487"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;16</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_203487' href="/search/DataSheet.aspx?MatGUID=8651187bfff84e5ab091a5e5b82760c1" >Morgan Advanced Ceramics Incronibsi™ Mac-Incronibsi™14-WM Braze Alloy</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_203488"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;17</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_203488' href="/search/DataSheet.aspx?MatGUID=4f36c7ae35244658a4f6889b27ecb222" >Morgan Advanced Ceramics Incronibsi™ Mac-Incronibsi™7-WM Braze Alloy</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_203492"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;18</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_203492' href="/search/DataSheet.aspx?MatGUID=8075d5e1d6c84d09b21f4846c50ba0f3" >Morgan Advanced Ceramics Palnicro™ Mac-Palnicro™ 30-WM Braze Alloy</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_81674"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;19</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_81674' href="/search/DataSheet.aspx?MatGUID=8808b026f7c14d2f8d61f2d476aaeb26" >Overview of materials for Nickel Alloy</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_100439"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;20</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_100439' href="/search/DataSheet.aspx?MatGUID=cdff081df0a64c8bb3f6b2aeecf9fb1c" >Tophel® Type E Thermocouple Alloy</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_100441"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;21</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_100441' href="/search/DataSheet.aspx?MatGUID=55e18abf590a4b45b495111ba9a4ccf3" >Alloy 19® Thermocouple Alloy</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_100442"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;22</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_100442' href="/search/DataSheet.aspx?MatGUID=61be65f7212d42ba81ee0dca3b4cf21a" >Alloy 20® Thermocouple Alloy</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_100454"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;23</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_100454' href="/search/DataSheet.aspx?MatGUID=df719248b9ee458f91c64286dd745dc2" >Kanthal® 52 Nickel-Iron Resistor Alloy</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_100455"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;24</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_100455' href="/search/DataSheet.aspx?MatGUID=19a379f76f4742d19e289ea558558439" >Kanthal® 70 Resistor Material</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_100461"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;25</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_100461' href="/search/DataSheet.aspx?MatGUID=d497619a3a7646b98f79f6dc26753544" >Nichrome 35-20 Medium Temperature Resistor Material</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_100462"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;26</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_100462' href="/search/DataSheet.aspx?MatGUID=29709d152e1142f5b05db70440d62c2a" >Nichrome 40-20 Medium Temperature Resistor Material</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_100463"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;27</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_100463' href="/search/DataSheet.aspx?MatGUID=014093642976472984e91c7392e67b55" >Nichrome 60-15 Medium Temperature Resistor Material</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_100464"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;28</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_100464' href="/search/DataSheet.aspx?MatGUID=70c6833687d54cb4a000bd49ffd8e86c" >Nichrome 70-30 Medium Temperature Resistor Material</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_88082"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;29</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_88082' href="/search/DataSheet.aspx?MatGUID=ded215b6b3e742c5acf4f941ef8934be" >Nickel Iron Alloy 52 Sealing Wire, UNS N14052</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_150725"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;30</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_150725' href="/search/DataSheet.aspx?MatGUID=e5a6dbf006ab45c2a928e3b0aeb6cb54" >Rene 100 Nickel Alloy (UNS N13100)</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_150724"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;31</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_150724' href="/search/DataSheet.aspx?MatGUID=bf03bf1ab500491e8bc1b26d0443ba63" >Refractory 26 Nickel Alloy Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_150615"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;32</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_150615' href="/search/DataSheet.aspx?MatGUID=bcf0ef0b5cf143aaaacaae339830fc9e" >RA-333 Nickel Alloy (UNS N06333)</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_150614"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;33</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_150614' href="/search/DataSheet.aspx?MatGUID=c2a04308d3ce4910a595d79bc821288e" >NA-224 Nickel Alloy Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_150221"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;34</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_150221' href="/search/DataSheet.aspx?MatGUID=3e5af421ec084323bbe3b0a9bb9ee640" >IN-102 Nickel Alloy, UNS N06102</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_14219"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;35</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_14219' href="/search/DataSheet.aspx?MatGUID=eba7962e4bcf4f8db32d6d9a6afcc760" >Constantan Thermocouple Wire</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_14223"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;36</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_14223' href="/search/DataSheet.aspx?MatGUID=de9dd08433714f698d513766dccea437" >Nitinol - NiTi Shape Memory Alloy; High-Temperature Phase</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_14224"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;37</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_14224' href="/search/DataSheet.aspx?MatGUID=44afc7d3c6eb4829bc2df27884fd2d6c" >Nitinol - NiTi Shape Memory Alloy; Low-Temperature Phase</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_14580"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;38</td><td style="width:auto;"><img src="/images/buttons/iconDiscontinued.jpg" alt="Discontinued" title="Discontinued"  /></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_14580' href="/search/DataSheet.aspx?MatGUID=cc64257d727e426c92bc096b89c1fda5" >Latitude Manufacturing PowderFlo® X Metal Injection Molding Compound</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_14767"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;39</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_14767' href="/search/DataSheet.aspx?MatGUID=3177fa2c18604ac5b136cd9c8f017fee" >ATI Allvac® Nickelvac® H-X Nickel Superalloy, Heat Treatment: 1177°C (2150°F) Anneal</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_14768"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;40</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_14768' href="/search/DataSheet.aspx?MatGUID=f287775db5d54b52b83589418f32786f" >ATI Allvac® Nickelvac® 600 Nickel Superalloy, Heat Treatment: 927°C (1700°F) Anneal</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_14769"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;41</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_14769' href="/search/DataSheet.aspx?MatGUID=0878b78b1bb84432b66a243575121427" >ATI Allvac® Nickelvac® 625 Nickel Superalloy, Heat Treatment: 927°C (1700°F) Anneal</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_14770"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;42</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_14770' href="/search/DataSheet.aspx?MatGUID=e200da3a4f0d47cfae0438dc71d801c2" >ATI Allvac® WASPALOY® Nickel Superalloy, Heat Treatment: 1010°C (1850°F) + Age</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_14771"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;43</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_14771' href="/search/DataSheet.aspx?MatGUID=995f2fd4c1b24bb6afa83acc3935391c" >ATI Allvac® Rene 41® Nickel Superalloy, Heat Treatment: 1079°C (1975°F) + Age</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_14772"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;44</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_14772' href="/search/DataSheet.aspx?MatGUID=5ba7d06a82e5486e830364c62dac9dba" >ATI Allvac® Nickelvac® N-90 Nickel Superalloy, Heat Treatment: 1066°C (1950°F) + Age</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_14773"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;45</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_14773' href="/search/DataSheet.aspx?MatGUID=e725134bd52644fca14863917456a811" >ATI Allvac® M-252 Nickel Superalloy, Heat Treatment: 1177°C (2150°F) Anneal</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_14774"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;46</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_14774' href="/search/DataSheet.aspx?MatGUID=1e4c894dc25847aa9ab1a83c6a2dbcca" >ATI Allvac® Nickelvac® C-263 Nickel Superalloy, Heat Treatment: 927°C (1700°F) Anneal</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_14775"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;47</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_14775' href="/search/DataSheet.aspx?MatGUID=fda195b1d3c944988fa271af5c21a595" >ATI Allvac® 718 Nickel Superalloy, Heat Treatment: 982°C (1800°F) + Age</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_14776"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;48</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_14776' href="/search/DataSheet.aspx?MatGUID=d780eaa45777430887a7184f2380f297" >ATI Allvac® 718Plus™ Nickel Superalloy</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_14777"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;49</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_14777' href="/search/DataSheet.aspx?MatGUID=84bf1ca6ac074918998cbd902526dc5f" >ATI Allvac® Nickelvac® W-722 Nickel Superalloy, Heat Treatment: 982°C (1800°F) + Age</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_14778"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;50</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_14778' href="/search/DataSheet.aspx?MatGUID=75f015fca4fe4e61a0c21aa9691bad01" >ATI Allvac® Nickelvac® X-750 Nickel Superalloy, Heat Treatment: 982°C (1800°F) + Age</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_14779"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;51</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_14779' href="/search/DataSheet.aspx?MatGUID=db31868104da49769722f64022ebdc1e" >ATI Allvac® Nickelvac® X-750 Nickel Superalloy, Heat Treatment: 1149°C (2100°F) + Age</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_14780"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;52</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_14780' href="/search/DataSheet.aspx?MatGUID=712e0954c304453e9116217e64043835" >ATI Allvac® 706 Nickel Superalloy, Heat Treatment: 954°C (1750°F) + Age</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_14781"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;53</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_14781' href="/search/DataSheet.aspx?MatGUID=a084b8c3ebe444898b6e9c8cd59db810" >ATI Allvac® Nickelvac® 901 Nickel Superalloy, Heat Treatment: 1010°C (1850°F) + Age</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_14782"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;54</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_14782' href="/search/DataSheet.aspx?MatGUID=f64926edc4664becbeb8e52a5d47f4f7" >ATI Allvac® Nickelvac® H-N Nickel Superalloy, Heat Treatment: 1177°C (2150°F) Anneal</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_14783"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;55</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_14783' href="/search/DataSheet.aspx?MatGUID=435df23541a34cada345d9355d3135cd" >ATI Allvac® Nickelvac® H-W Nickel Superalloy, Heat Treatment: 1185°C (2165°F) Anneal</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_14784"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;56</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_14784' href="/search/DataSheet.aspx?MatGUID=257f0a2e482f47eb938d5a02f9e86187" >ATI Allvac® Nickelvac® HC-276 Nickel Superalloy, Heat Treatment: 1121°C (2050°F) Anneal</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_14785"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;57</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_14785' href="/search/DataSheet.aspx?MatGUID=be3e0f56f37b42d9a2d2e33923a960be" >ATI Allvac® Astroloy Nickel Superalloy, Heat Treatment: 1163°C (2125°F) + Age</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_14786"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;58</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_14786' href="/search/DataSheet.aspx?MatGUID=5ea3d713ec9541988430497331422b21" >ATI Allvac® 520 Nickel Superalloy, Heat Treatment: 1121°C (2050°F) + Age</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_14787"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;59</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_14787' href="/search/DataSheet.aspx?MatGUID=e5868e2f0dc0449ea6b7e5799828db30" >ATI Allvac® 720 Nickel Superalloy, Heat Treatment: 1093°C (2000°F) + Age</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_14788"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;60</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_14788' href="/search/DataSheet.aspx?MatGUID=e8e373d77a344f3b993106cd87819e18" >ATI Allvac® 35N Nickel Superalloy, Heat Treatment: 1052°C (1925°F) + Age</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_14865"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;61</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_14865' href="/search/DataSheet.aspx?MatGUID=762b4eed34d94ec7bbc1f69768e3d192" >Alliance A-2 Alnico Magnetic Material</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_14866"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;62</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_14866' href="/search/DataSheet.aspx?MatGUID=65b854b1ca104dafb745cf271d1e848b" >Alliance A-3 Alnico Magnetic Material</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_14867"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;63</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_14867' href="/search/DataSheet.aspx?MatGUID=d3e328e647c241289d1ec26fdcf47899" >Alliance A-5 Alnico Magnetic Material</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_268377"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;64</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_268377' href="/search/DataSheet.aspx?MatGUID=8ee4d23da9fe4169a810cb87676bd8bd" >Alliance A-57 Cast Alnico Magnet</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_14868"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;65</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_14868' href="/search/DataSheet.aspx?MatGUID=b03ed6701a654fea948a64e179747932" >Alliance A-5DG Alnico Magnetic Material</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_14869"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;66</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_14869' href="/search/DataSheet.aspx?MatGUID=fd4fb7e596014b1cb2125c34bc968704" >Alliance A-5-7 Alnico Magnetic Material</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_14870"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;67</td><td style="width:auto;"><img src="/images/buttons/iconDiscontinued.jpg" alt="Discontinued" title="Discontinued"  /></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_14870' href="/search/DataSheet.aspx?MatGUID=177ceb737f8848bcb8399a86a03f7dc3" >Alliance A-6 Alnico Magnetic Material</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_14871"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;68</td><td style="width:auto;"><img src="/images/buttons/iconDiscontinued.jpg" alt="Discontinued" title="Discontinued"  /></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_14871' href="/search/DataSheet.aspx?MatGUID=5a7597d4b5f54e589513b8aabe27422d" >Alliance A-8 Alnico Magnetic Material</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_14872"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;69</td><td style="width:auto;"><img src="/images/buttons/iconDiscontinued.jpg" alt="Discontinued" title="Discontinued"  /></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_14872' href="/search/DataSheet.aspx?MatGUID=43e09fcb5e664d1abe182834c980b3b2" >Alliance A-2S Alnico Magnetic Material</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_14873"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;70</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_14873' href="/search/DataSheet.aspx?MatGUID=e04eeb728b924388b3ac244c4b25aebd" >Alliance A-5S Alnico Magnetic Material</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_14874"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;71</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_14874' href="/search/DataSheet.aspx?MatGUID=7750ed3f941845888253f3a375d4588b" >Alliance A-6S Alnico Magnetic Material</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_14875"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;72</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_14875' href="/search/DataSheet.aspx?MatGUID=be88ec594a7a4af3b894f7ba911a2680" >Alliance A-8S Alnico Magnetic Material</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_14916"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;73</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_14916' href="/search/DataSheet.aspx?MatGUID=a820ad05dbfc457f82d7751ea08ac84f" >ATI Allvac® Nickelvac® 400 UNS N04400 Nickel Superalloy</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_14917"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;74</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_14917' href="/search/DataSheet.aspx?MatGUID=09a955d664a74798a12a1d2b122d991c" >ATI Allvac® Nickelvac® K-500 UNS N05500 Nickel Superalloy</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_14918"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;75</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_14918' href="/search/DataSheet.aspx?MatGUID=580dbaf52ae848d28e81c898da467e37" >ATI Allvac® Allcorr® UNS N06110 Nickel Superalloy</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_14919"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;76</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_14919' href="/search/DataSheet.aspx?MatGUID=54a7399d35a44cd2b23b96f3dc0086e2" >ATI Allvac® Nickelvac® C 22 UNS N06022 Nickel Superalloy</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_14920"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;77</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_14920' href="/search/DataSheet.aspx?MatGUID=602fe5681f93485f966c9393b080a5d1" >ATI Allvac® Nickelvac® 601 UNS N06601 Nickel Superalloy</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_14921"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;78</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_14921' href="/search/DataSheet.aspx?MatGUID=0bd922e0247e478ca52f98b003570526" >ATI Allvac® Nickelvac® 690 UNS N06690 Nickel Superalloy</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_14922"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;79</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_14922' href="/search/DataSheet.aspx?MatGUID=4bd812e9ba0442dba06c212205e0533a" >ATI Allvac® Nickelvac® 80 A UNS N07080 Nickel Superalloy</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_14923"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;80</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_14923' href="/search/DataSheet.aspx?MatGUID=8ea7e24f8eec452e9ceaa928f2c7b6b9" >ATI Allvac® 718-OP® UNS N07718 Nickel Superalloy</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_14924"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;81</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_14924' href="/search/DataSheet.aspx?MatGUID=cfd554d9f98145b2a4fa397dc7186d57" >ATI Allvac® Nickelvac® X- 751 UNS N07751 Nickel Superalloy</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_14926"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;82</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_14926' href="/search/DataSheet.aspx?MatGUID=05656f4ad3194d64814ad03c37ec2a24" >ATI Allvac® Nickelvac® 606 Nickel Superalloy</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_14928"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;83</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_14928' href="/search/DataSheet.aspx?MatGUID=b729719854724478adc8beb71b9079f2" >ATI Allvac® Nickelvac® C-276 UNS N10276 Nickel Superalloy</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_14929"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;84</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_14929' href="/search/DataSheet.aspx?MatGUID=90d87723e9c549b199ff77983b58f987" >ATI Allvac® Nickelvac® H-B-2 UNS N10665 Nickel Superalloy</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_121063"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;85</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_121063' href="/search/DataSheet.aspx?MatGUID=73f2b93724434ddf8d6e4a9a85b8dfde" >Alloy Wire International RW 80 Ni-Cr Resistance Wire</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_15081"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;86</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_15081' href="/search/DataSheet.aspx?MatGUID=620552cd881b491b874c2b3bcb449534" >Bales Mold Service Ni-Hard® (Nickel-Cobalt) Mold Coating</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_15083"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;87</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_15083' href="/search/DataSheet.aspx?MatGUID=d5a7cba1fff04c68b89e62a75962c1c0" >Bales Mold Service Nicklon® Nickel-P.T.F.E. Mold Coating</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_15084"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;88</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_15084' href="/search/DataSheet.aspx?MatGUID=a92525523d68489f9d0faa7f361091a4" >Bales Mold Service QQ-N-290 Sulfamate Nickel Mold Coating</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_15085"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;89</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_15085' href="/search/DataSheet.aspx?MatGUID=11fcb2489eda4ffc988d38b551d273c1" >Bales Mold Service Nibore® Electroless Nickel - Boron Nitride Mold Coating</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_15090"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;90</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_15090' href="/search/DataSheet.aspx?MatGUID=eaf707994d5d4ec2993b3f25828b05d2" >Materion Beryllium Nickel Strip - Alloy 360 Annealed</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_15091"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;91</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_15091' href="/search/DataSheet.aspx?MatGUID=b0935a8f61854e26b3401e3fc97a47e5" >Materion Beryllium Nickel Strip - Alloy 360 1/4 Hard</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_15092"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;92</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_15092' href="/search/DataSheet.aspx?MatGUID=1f43b165461b49dbaeb053e8c5a2fb41" >Materion Beryllium Nickel Strip - Alloy 360 1/2 Hard</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_15093"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;93</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_15093' href="/search/DataSheet.aspx?MatGUID=669fda5caa484732b9f3d5242136b802" >Materion Beryllium Nickel Strip - Alloy 360 Hard</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_15094"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;94</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_15094' href="/search/DataSheet.aspx?MatGUID=ef1209afb33542419cebf2ecf7a6d340" >Materion Beryllium Nickel Strip - Alloy 360 Annealed, 2.5 hr at 950°F</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_15095"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;95</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_15095' href="/search/DataSheet.aspx?MatGUID=29a3175671114352aa204d53c75e7f57" >Materion Beryllium Nickel Strip - Alloy 360 1/4 Hard, 2.5 hr at 950°F</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_15096"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;96</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_15096' href="/search/DataSheet.aspx?MatGUID=89e1e47f43ca405499db58f4f444f4e5" >Materion Beryllium Nickel Strip - Alloy 360 1/2 Hard, 1.5 hr at 950°F</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_15097"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;97</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_15097' href="/search/DataSheet.aspx?MatGUID=2c16617e7c684669ae579026ef806e3d" >Materion Beryllium Nickel Strip - Alloy 360 Hard, 1.5 hr at 950°F</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_15098"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;98</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_15098' href="/search/DataSheet.aspx?MatGUID=ad882ad84b8748a097ca5e2b37ef18af" >Materion Beryllium Nickel Strip - Alloy 360 MH2, Mill Hardened</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_15099"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;99</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_15099' href="/search/DataSheet.aspx?MatGUID=f8cd92e6156a46aa8d39115fa75b22b5" >Materion Beryllium Nickel Strip - Alloy 360 MH4, Mill Hardened</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_15100"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;100</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_15100' href="/search/DataSheet.aspx?MatGUID=980bf1f39c0740d292bae9c6cbb8662e" >Materion Beryllium Nickel Strip - Alloy 360 MH6, Mill Hardened</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_15101"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;101</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_15101' href="/search/DataSheet.aspx?MatGUID=b026c7448fbd4a87a1bdb1f2c5c369d1" >Materion Beryllium Nickel Strip - Alloy 360 MH8, Mill Hardened</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_15102"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;102</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_15102' href="/search/DataSheet.aspx?MatGUID=09dbc7d488a74513a4b2a60ed4aad707" >Materion Beryllium Nickel Strip - Alloy 360 MH10, Mill Hardened</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_15103"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;103</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_15103' href="/search/DataSheet.aspx?MatGUID=5cfb86af2d224cbcb66eeb868fe6ba06" >Materion Beryllium Nickel Strip - Alloy 360 MH12, Mill Hardened</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_15131"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;104</td><td style="width:auto;"><img src="/images/buttons/iconDiscontinued.jpg" alt="Discontinued" title="Discontinued"  /></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_15131' href="/search/DataSheet.aspx?MatGUID=ad618fdd4f0d42c492b3af4990da9b3f" >Materion Beryllium Nickel M220C</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_15306"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;105</td><td style="width:auto;"><img src="/images/buttons/iconDiscontinued.jpg" alt="Discontinued" title="Discontinued"  /></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_15306' href="/search/DataSheet.aspx?MatGUID=27cb4a7ca5844a70beba33d5c7dc5ac0" >Materion Nibryl Beryllium Nickel Alloy (Nibryl)-360 Rod and Bar; A (A) Temper; All Sizes (UNS N03360)</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_15307"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;106</td><td style="width:auto;"><img src="/images/buttons/iconDiscontinued.jpg" alt="Discontinued" title="Discontinued"  /></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_15307' href="/search/DataSheet.aspx?MatGUID=09d87d3bfd6045ebbcfdca6c0f31bf2d" >Materion Nibryl Beryllium Nickel Alloy (Nibryl)-360 Rod and Bar; H775 (H775) Temper; All Sizes (UNS N03360)</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_15308"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;107</td><td style="width:auto;"><img src="/images/buttons/iconDiscontinued.jpg" alt="Discontinued" title="Discontinued"  /></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_15308' href="/search/DataSheet.aspx?MatGUID=c0637eb3c0a846d4b1d274e7e985c59b" >Materion Nibryl Beryllium Nickel Alloy (Nibryl)-360 Rod and Bar; H925 (H925) Temper; All Sizes (UNS N03360)</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_15309"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;108</td><td style="width:auto;"><img src="/images/buttons/iconDiscontinued.jpg" alt="Discontinued" title="Discontinued"  /></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_15309' href="/search/DataSheet.aspx?MatGUID=8e0be27f891541a78d7a9bbecd5e61d9" >Materion Nibryl Beryllium Nickel Alloy (Nibryl)-360 Rod and Bar; H1050 (H1050) Temper; All Sizes (UNS N03360)</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_323522"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;109</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_323522' href="/search/DataSheet.aspx?MatGUID=559a4dce424444908bef27c2f6ebccff" >Materion Nickel-Chromium (NiCr) Sputtering Target for Large Area Coating Applications</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_15543"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;110</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_15543' href="/search/DataSheet.aspx?MatGUID=f62c5ff49e524cb3a4c78f15303b0baa" >California Fine Wire Alloy 52 Nickel Iron Alloy</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_15545"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;111</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_15545' href="/search/DataSheet.aspx?MatGUID=5253a6e3f31d4fdf81e9c6b280c9578a" >California Fine Wire Nickel Alloy 99</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_15546"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;112</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_15546' href="/search/DataSheet.aspx?MatGUID=1c4144dd28d34b62b46e15cd1153271f" >California Fine Wire Nickel 211 (Nickel Manganese Alloy)</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_15547"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;113</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_15547' href="/search/DataSheet.aspx?MatGUID=3f5e0a897b1344f1b1f2bad4cee0a5f9" >California Fine Wire Nickel 220 Super Alloy</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_15548"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;114</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_15548' href="/search/DataSheet.aspx?MatGUID=75e7ff29c9e045b4aae6a35543d2a18e" >California Fine Wire Nickel 271 Super Alloy</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_15549"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;115</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_15549' href="/search/DataSheet.aspx?MatGUID=687a378920f34f46a53bbc587af5044b" >California Fine Wire Nickel 272 Super Alloy</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_15550"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;116</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_15550' href="/search/DataSheet.aspx?MatGUID=66c84bcf379d4a2987f096cab74efafd" >California Fine Wire Nickel 5900 Nickel Alloy</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_15551"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;117</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_15551' href="/search/DataSheet.aspx?MatGUID=50538ac6e41a4439b0725f7a4c61e566" >California Fine Wire Nickel A 200 Nickel Alloy</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_15552"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;118</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_15552' href="/search/DataSheet.aspx?MatGUID=27fb383c6be748f9adb9c19238b213fa" >California Fine Wire Nickel Alloy 99 Nickel Alloy</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_15565"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;119</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_15565' href="/search/DataSheet.aspx?MatGUID=4e95c89e6267417cb10465059594d4a7" >California Fine Wire Nickel Alloy 120 Nickel Iron Resistance Wire</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_15570"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;120</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_15570' href="/search/DataSheet.aspx?MatGUID=e1fa0799ce134ed5a826d28ce3639499" >California Fine Wire Nico Copper Nickel Resistance Wire</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_15571"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;121</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_15571' href="/search/DataSheet.aspx?MatGUID=5f631f8229e14906a96666e12c226ec2" >California Fine Wire NIFE 5200 Nickel Iron Resistance Wire</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_15573"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;122</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_15573' href="/search/DataSheet.aspx?MatGUID=9c35474400904e6dac821acdb361f905" >California Fine Wire Stablohm 650 Nickel Chromium Resistance Wire</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_15574"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;123</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_15574' href="/search/DataSheet.aspx?MatGUID=7e790d6fbbb94034abf4edb853101401" >California Fine Wire Stablohm 675 Nickel, Chromium and Iron Resistance Wire</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_15575"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;124</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_15575' href="/search/DataSheet.aspx?MatGUID=820cb9d7a0bb4ab0bfe32ebb89ea8777" >California Fine Wire Stablohm 710 Nickel Chromium Resistance Wire</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_15577"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;125</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_15577' href="/search/DataSheet.aspx?MatGUID=57f58de667714574b449ade331a70b72" >California Fine Wire Stablohm 800 Nickel, Chromium, Aluminum, and Copper Resistance Wire</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_15579"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;126</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_15579' href="/search/DataSheet.aspx?MatGUID=bae505647bd141dfbd93e384acf4b723" >California Fine Wire Stablohm 825 Nickel Chromium Resistance Wire</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_15698"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;127</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_15698' href="/search/DataSheet.aspx?MatGUID=383b0bbe0360447e8ed21d2b1d2666f7" >Carpenter Custom Age 625 PLUS® Nickel-Base Alloy, Aged 732°C (1350°F)/4hr/AC</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_15699"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;128</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_15699' href="/search/DataSheet.aspx?MatGUID=75a77285ea224903b34c042af1900225" >Carpenter Custom Age 625 PLUS® Nickel-Base Alloy, Aged 732°C (1350°F)/8hr/AC</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_15700"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;129</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_15700' href="/search/DataSheet.aspx?MatGUID=9161fb1a41d44e3398fd42723df81812" >Carpenter Custom Age 625 PLUS® Nickel-Base Alloy, Double Aged 718°C (1325°F)/8hr/AC to 621°C (1150°F)/8hr/AC</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_15701"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;130</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_15701' href="/search/DataSheet.aspx?MatGUID=2dcbdda903d6400a81b8ea555b9b279d" >Carpenter Custom Age 625 PLUS® Nickel-Base Alloy, Double Aged 732°C (1350°F)/8hr/AC to 621°C (1150°F)/8hr/AC</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_15702"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;131</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_15702' href="/search/DataSheet.aspx?MatGUID=1ec07c295bf94631972d06225bd27287" >Carpenter Custom Age 625 PLUS® Nickel-Base Alloy, Double Aged 746°C (1375°F)/8hr/AC to 621°C (1150°F)/8hr/AC</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_15703"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;132</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_15703' href="/search/DataSheet.aspx?MatGUID=93459b72858c4ff58fc76c461c71b778" >Carpenter Custom Age 625 PLUS® Nickel-Base Alloy, Cold Drawn 37.5%</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_15704"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;133</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_15704' href="/search/DataSheet.aspx?MatGUID=3b620111d046492a8595fe395a779983" >Carpenter Custom Age 625 PLUS® Nickel-Base Alloy, Cold Drawn 37.5% Plus Aging 649°C (1200°F)/4hr/FC to 579°C/4hr/AC</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_15705"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;134</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_15705' href="/search/DataSheet.aspx?MatGUID=b8b5da487b2e47cbbeef849ff1436b21" >Carpenter Custom Age 625 PLUS® Nickel-Base Alloy, Double Aged 732°C/8hr/FC to 621°C/8hr/AC</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_15720"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;135</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_15720' href="/search/DataSheet.aspx?MatGUID=0f21cd16882444e2a2d2403968dea3b7" >Carpenter MP35N* Ni-Co-Cr-Mo Alloy, 0% Cold Reduction</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_15721"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;136</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_15721' href="/search/DataSheet.aspx?MatGUID=66d74913c0d0411f923df53cd60a7ffc" >Carpenter MP35N* Ni-Co-Cr-Mo Alloy, 15% Cold Reduction</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_15722"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;137</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_15722' href="/search/DataSheet.aspx?MatGUID=b78f5c9066924c5eb6c16d949f27d928" >Carpenter MP35N* Ni-Co-Cr-Mo Alloy, 25% Cold Reduction</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_15723"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;138</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_15723' href="/search/DataSheet.aspx?MatGUID=5cfcde790c67439ca6ca2d86d256dd64" >Carpenter MP35N* Ni-Co-Cr-Mo Alloy, 35% Cold Reduction</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_15724"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;139</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_15724' href="/search/DataSheet.aspx?MatGUID=18e053f77fcf42b19a5865ea4801d032" >Carpenter MP35N* Ni-Co-Cr-Mo Alloy, 45% Cold Reduction</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_15725"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;140</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_15725' href="/search/DataSheet.aspx?MatGUID=89488d61a1594cc68da45f9b6cd79080" >Carpenter MP35N* Ni-Co-Cr-Mo Alloy, 55% Cold Reduction</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_15726"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;141</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_15726' href="/search/DataSheet.aspx?MatGUID=64f260df11094625b18d000211deedb8" >Carpenter MP35N* Ni-Co-Cr-Mo Alloy, 65% Cold Reduction</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_15727"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;142</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_15727' href="/search/DataSheet.aspx?MatGUID=626c6055f32c438784d657458232b752" >Carpenter MP35N* Ni-Co-Cr-Mo Alloy, 0% Cold Reduction + Aged 538°C (1000°F)/4hr/AC</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_15728"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;143</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_15728' href="/search/DataSheet.aspx?MatGUID=8966619838bf4d23885e6b59ca2fb2b9" >Carpenter MP35N* Ni-Co-Cr-Mo Alloy, 15% Cold Reduction + Aged 538°C (1000°F)/4hr/AC</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_15729"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;144</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_15729' href="/search/DataSheet.aspx?MatGUID=08c265e65b114a57a458c3f561bba195" >Carpenter MP35N* Ni-Co-Cr-Mo Alloy, 25% Cold Reduction + Aged 538°C (1000°F)/4hr/AC</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_15730"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;145</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_15730' href="/search/DataSheet.aspx?MatGUID=073eb647bc5c4ba4bc15f707f04d8a68" >Carpenter MP35N* Ni-Co-Cr-Mo Alloy, 35% Cold Reduction + Aged 538°C (1000°F)/4hr/AC</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_15731"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;146</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_15731' href="/search/DataSheet.aspx?MatGUID=6ddf8a337973406cb5c11eb285e470a1" >Carpenter MP35N* Ni-Co-Cr-Mo Alloy, 45% Cold Reduction + Aged 538°C (1000°F)/4hr/AC</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_15732"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;147</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_15732' href="/search/DataSheet.aspx?MatGUID=206cb304a78b4edba724c545596e28d4" >Carpenter MP35N* Ni-Co-Cr-Mo Alloy, 53% Cold Reduction + Aged 538°C (1000°F)/4hr/AC</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_15733"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;148</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_15733' href="/search/DataSheet.aspx?MatGUID=39e171d024fe4d4baabdd16c397f4c5d" >Carpenter MP35N* Ni-Co-Cr-Mo Alloy, Cold Drawn 33%, Aged 565°C/4hr/AC</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_15740"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;149</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_15740' href="/search/DataSheet.aspx?MatGUID=f00df5c9f3f244d4b8c1f282f01383f7" >Carpenter MP35N* Ni-Co-Cr-Mo Alloy, Cold Drawn 53%, Aged 565°C/4hr/AC</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_15750"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;150</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_15750' href="/search/DataSheet.aspx?MatGUID=badf8bf8dee94ffca20e2162bdc61d96" >Carpenter HyMu "80"® Electronic Alloy, Cold Drawn Bar</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_15751"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;151</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_15751' href="/search/DataSheet.aspx?MatGUID=8371100e80e7448aaa3b30a843cbe362" >Carpenter HyMu "80"® Electronic Alloy, Hydrogen Annealed Bar</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_15752"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;152</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_15752' href="/search/DataSheet.aspx?MatGUID=ec35e8da57eb418d8b773b6822031451" >Carpenter HyMu "80"® Electronic Alloy, Bar, After Process Anneal</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_15753"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;153</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_15753' href="/search/DataSheet.aspx?MatGUID=3fb89640c7d644ab9be5cebe647dc549" >Carpenter HyMu "80"® Electronic Alloy, Cold Rolled Strip</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_15754"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;154</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_15754' href="/search/DataSheet.aspx?MatGUID=524a6d3c7862413e9c47a347a3d45369" >Carpenter HyMu "80"® Electronic Alloy, Hydrogen Annealed Strip</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_15755"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;155</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_15755' href="/search/DataSheet.aspx?MatGUID=bc7e80d593764d5eaebac2124b546b3c" >Carpenter HyMu "80"® Electronic Alloy, Strip, After Process Anneal</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_15819"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;156</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_15819' href="/search/DataSheet.aspx?MatGUID=ba7840f7785046df8364e7e2ac63f09b" >Carlson 330 Austenitic Alloy</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_15820"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;157</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_15820' href="/search/DataSheet.aspx?MatGUID=efdf010f4bf84c379ba8d927f762acd4" >Carlson 20 Plus Alloy</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_15821"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;158</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_15821' href="/search/DataSheet.aspx?MatGUID=b5affb48a236453f9abd1b8acc120d22" >Carlson C 200 Commercially Pure Nickel</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_15822"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;159</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_15822' href="/search/DataSheet.aspx?MatGUID=3868bbda19b5467d9b60d6feab4d1db1" >Carlson C 201 Commercially Pure Nickel</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_15823"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;160</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_15823' href="/search/DataSheet.aspx?MatGUID=04bc31c2b0244ccf9d6d2c86fc05b38a" >Carlson C 400 Nickel-Copper Alloy</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_15824"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;161</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_15824' href="/search/DataSheet.aspx?MatGUID=c1fab9eb8c2a4c66a320eac1cc349228" >Carlson C 600 Nickel-Chromium-Iron Alloy</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_15825"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;162</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_15825' href="/search/DataSheet.aspx?MatGUID=c8f46d214a714978be367968f74d04e8" >Carlson C 600ESR Nickel-Chromium-Iron Alloy</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_15826"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;163</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_15826' href="/search/DataSheet.aspx?MatGUID=5bdf5ed4c6a44b1ca35416de78af6eef" >Carlson C 601 Nickel-Chromium-Iron Alloy</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_15827"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;164</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_15827' href="/search/DataSheet.aspx?MatGUID=a0574ea982f14fb6998b52ff30641abe" >Carlson C 625 Nickel-Chromium-Molybdenum Alloy</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_15828"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;165</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_15828' href="/search/DataSheet.aspx?MatGUID=c281ea6dbcb6435d8fb9bc7184ba63b1" >Carlson C 800 Nickel-Iron-Chromium Alloy</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_15829"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;166</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_15829' href="/search/DataSheet.aspx?MatGUID=69de04916d774adc9a0024c8ea2d9b44" >Carlson C 800H Nickel-Iron-Chromium Alloy</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_15830"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;167</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_15830' href="/search/DataSheet.aspx?MatGUID=ed914041d16644c591886d0705875611" >Carlson C 800AT Nickel-Iron-Chromium Alloy</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_15831"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;168</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_15831' href="/search/DataSheet.aspx?MatGUID=eb6452f894cb462988cdf15d4ac0a1de" >Carlson C 825 Nickel-Iron-Chromium Alloy</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_15832"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;169</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_15832' href="/search/DataSheet.aspx?MatGUID=7b3295ae31df46069e07bcf1fa389a3d" >Carlson GOC 276 Nickel-Molybdenum-Chromium</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_15833"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;170</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_15833' href="/search/DataSheet.aspx?MatGUID=8a4893b00fce48cbbed05bd0e0696e26" >Carlson 70 30 Cu Ni Copper-Nickel Alloy</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_15987"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;171</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_15987' href="/search/DataSheet.aspx?MatGUID=a4a5a73637994f27a83f64ca698dcae7" >AFE Technologies Cronite 37Ni/18Cr No. HR5</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_15989"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;172</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_15989' href="/search/DataSheet.aspx?MatGUID=65bd09a1bff8478f9eca593ad71b9387" >AFE Technologies Cronite Standard Nickel Alloy Castings, No. HR4</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_15990"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;173</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_15990' href="/search/DataSheet.aspx?MatGUID=cd915752167a45919457854ae26965f9" >AFE Technologies Cronite 34Ni/25Cr/Nb No. HR 33 Nb</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_15991"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;174</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_15991' href="/search/DataSheet.aspx?MatGUID=358dfba90aeb40b58805d05dbc657498" >AFE Technologies Cronite 657 Nickel Alloy Castings</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_16064"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;175</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_16064' href="/search/DataSheet.aspx?MatGUID=e1bac255c1964e19a43b296f3d581516" >Crucible Compaction Metals P/M Low Carbon Astroloy, Supersolvus</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_16065"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;176</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_16065' href="/search/DataSheet.aspx?MatGUID=1f2df80d79834fae9b930dec30f22132" >Crucible Compaction Metals Rene 95 Nickel Based Superalloy</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_16066"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;177</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_16066' href="/search/DataSheet.aspx?MatGUID=ed0a2e8ba3034f8bb6b0c774e02fdd35" >Crucible Compaction Metals Rene Supersolvus Rene 95 Nickel Based Superalloy</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_16067"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;178</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_16067' href="/search/DataSheet.aspx?MatGUID=417c2b72672d45e89472732b32911e8f" >Crucible Compaction Metals P/M 625 Corrosion Resistant Alloy</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_16068"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;179</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_16068' href="/search/DataSheet.aspx?MatGUID=0a3515b26de041f78ade90e87baad585" >Crucible Compaction Metals P/M 625M Corrosion Resistant Alloy</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_16069"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;180</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_16069' href="/search/DataSheet.aspx?MatGUID=4bb96077ff464b96adf702a6cd46e0eb" >Crucible Compaction Metals P/M N625 Corrosion Resistant Alloy</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_16070"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;181</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_16070' href="/search/DataSheet.aspx?MatGUID=5a9f8bd03af14853a5efc77baa278af7" >Crucible Compaction Metals P/M N690 Corrosion Resistant Alloy</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_16075"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;182</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_16075' href="/search/DataSheet.aspx?MatGUID=85d1131d0db34364b965c0570fae84d2" >Kennametal Stellite Deloro® 208 PM after P/M Processing</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_16076"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;183</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_16076' href="/search/DataSheet.aspx?MatGUID=6d746ebc422d4702bd38ef36c443e6e6" >Kennametal Stellite Deloro® 208 PM cast</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_16083"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;184</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_16083' href="/search/DataSheet.aspx?MatGUID=db36451623fe43e28daa9f524e74ea65" >Kennametal Stellite Deloro® 711 PM after P/M Processing</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_16086"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;185</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_16086' href="/search/DataSheet.aspx?MatGUID=ba8e8b51d2004679bfa281a1933a62ad" >Kennametal Stellite Deloro® 711 PM cast</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_16087"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;186</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_16087' href="/search/DataSheet.aspx?MatGUID=801e90c4d7ca475c82cf1e8c7fa520fe" >Kennametal Stellite Deloro® 716 PM with P/M Processing</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_16088"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;187</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_16088' href="/search/DataSheet.aspx?MatGUID=0fcd6d39925942b9b9b5c89653e57795" >Kennametal Stellite Deloro® 716 PM cast</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_16095"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;188</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_16095' href="/search/DataSheet.aspx?MatGUID=d5f623b6a7da42baab96f63465ed7bb9" >Kennametal Stellite Deloro® 40G</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_198187"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;189</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_198187' href="/search/DataSheet.aspx?MatGUID=07a184a4383944eaa9c11d766c2d3906" >Datum Alloys Nickel 200/201 Strip and Tags</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_16112"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;190</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_16112' href="/search/DataSheet.aspx?MatGUID=16e4303581224c758427c22313500e22" >Kennametal Stellite Deloro® N-6 PM with P/M Processing</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_16113"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;191</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_16113' href="/search/DataSheet.aspx?MatGUID=58bb6bd4f5f14f45a0fb7f1dc4962993" >Kennametal Stellite Deloro® N-6 PM cast</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_147481"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;192</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_147481' href="/search/DataSheet.aspx?MatGUID=63ad9436bb2541068f60a68bd87e9280" >Eagle Brass 200/201 Nickel Alloy, Annealed</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_147482"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;193</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_147482' href="/search/DataSheet.aspx?MatGUID=ea3d4c0059f44e94a0b20d414cee302a" >Eagle Brass 200/201 Nickel Alloy, 1/8 Hardened</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_147483"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;194</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_147483' href="/search/DataSheet.aspx?MatGUID=5d8ae4d2296d4635817bcd714bb6019b" >Eagle Brass 200/201 Nickel Alloy, 1/4 Hardened</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_147484"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;195</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_147484' href="/search/DataSheet.aspx?MatGUID=ab24ff5f64334275a6b1158250b26794" >Eagle Brass 200/201 Nickel Alloy, 1/2 Hardened</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_147485"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;196</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_147485' href="/search/DataSheet.aspx?MatGUID=6dbd608985fc4e3ca023ae57f7e959e4" >Eagle Brass 200/201 Nickel Alloy, 3/4 Hardened</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_147486"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;197</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_147486' href="/search/DataSheet.aspx?MatGUID=ba654a64a72643d980d4b744f05db48d" >Eagle Brass 200/201 Nickel Alloy, Full Hardened</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_194941"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;198</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_194941' href="/search/DataSheet.aspx?MatGUID=03ab1b3ed0af4c6a9033ef7ac90b85b5" >Ed Fagan EFI Alloy 79 Soft Magnetic Alloy</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_280590"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;199</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_280590' href="/search/DataSheet.aspx?MatGUID=ca263157b8304da9a93758bbdf15bd78" >EOS NickelAlloy IN625 DMSL Powder</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_280591"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;200</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_280591' href="/search/DataSheet.aspx?MatGUID=c86056b671ac48a1a0f9d595c8a41761" >EOS NickelAlloy IN718 DMSL on EOS M 290</a></td></tr>

		
	</table>

	<table style="background-color:Silver;" class="tabledataformat" >

		<tr class="">
			<th>&nbsp;</th>
		</tr>

		<tr>
			<td><strong>Found <span id="ctl00_ContentMain_UcSearchResults1_lblResultCount2">1245</span> Results</strong> -- 
				
				Page 
				<select name="ctl00$ContentMain$UcSearchResults1$drpPageSelect2" onchange="javascript:setTimeout('__doPostBack(\'ctl00$ContentMain$UcSearchResults1$drpPageSelect2\',\'\')', 0)" id="ctl00_ContentMain_UcSearchResults1_drpPageSelect2">
		<option selected="selected" value="1">1</option>
		<option value="2">2</option>
		<option value="3">3</option>
		<option value="4">4</option>
		<option value="5">5</option>

	</select>
				of <span id="ctl00_ContentMain_UcSearchResults1_lblPageTotal2">7</span> 
				
				-- 
				<a id="ctl00_ContentMain_UcSearchResults1_lnkPrevPage2" disabled="disabled" style="color:Gray;">[Prev Page]</a>
				<a id="ctl00_ContentMain_UcSearchResults1_lnkNextPage2" href="javascript:__doPostBack('ctl00$ContentMain$UcSearchResults1$lnkNextPage2','')" style="color:Black;">[Next Page]</a>
				--&nbsp;

				view 
				<select name="ctl00$ContentMain$UcSearchResults1$drpPageSize2" onchange="javascript:setTimeout('__doPostBack(\'ctl00$ContentMain$UcSearchResults1$drpPageSize2\',\'\')', 0)" id="ctl00_ContentMain_UcSearchResults1_drpPageSize2">
		<option value="25">25</option>
		<option value="50">50</option>
		<option value="75">75</option>
		<option value="100">100</option>
		<option value="150">150</option>
		<option selected="selected" value="200">200</option>

	</select> per page
			</td>
		</tr>
	</table>
	
	<span id="ctl00_ContentMain_UcSearchResults1_divDiscontinued" style="font-size:11px;color:red">
	<span id="ctl00_ContentMain_UcSearchResults1_lblDisclaimer"><br />
Materials flagged as discontinued (<img src="/images/buttons/iconDiscontinued.jpg" alt="" />) are no longer part of the manufacturer’s standard product line according to our latest information.  These materials may be available by special order, in distribution inventory, or reinstated as an active product.  Data sheets from materials that are no longer available remain in MatWeb to assist users in finding replacement materials.  
<br /><br />
Users of our Advanced Search (registration required) may exclude discontinued materials from search results.</span>
	</span> 


</div>





<script type="text/javascript">

	//GLOBAL VARIABLES
	var gCurrentCheckbox;
	var gNewFolderID;
	var gOldFolderName;
	var gSelectedFolderID = 0;
	var gMaxFolders = 0;

	//GLOBAL FIELD REFERENCES
	var drpFolderList = document.getElementById("ctl00_ContentMain_UcSearchResults1_drpFolderList");
	var drpFolderAction = document.getElementById("ctl00_ContentMain_UcSearchResults1_drpFolderAction");
	var txtFolderMatCount = document.getElementById("ctl00_ContentMain_UcSearchResults1_txtFolderMatCount");
	var divUserMessage = document.getElementById("divUserMessage");
	var divFolderName = document.getElementById("divFolderName");
	var tblFolderName = document.getElementById("tblFolderName");
	var txtFolderName = document.getElementById("txtFolderName2");
	
	// See MS ajax.net client side documentation
	// http://ajax.asp.net/docs/ClientReference/Sys.Net/WebrequestManagerClass/	
	Sys.Net.WebRequestManager.add_invokingRequest(ucSearchResults_OnRequestStart);
	Sys.Net.WebRequestManager.add_completedRequest(ucSearchResults_OnRequestEnd);
	
	var timerID = 0;

	function ucSearchResults_OnRequestStart(){
		document.body.style.cursor="progress";
	}
	
	function ucSearchResults_OnRequestEnd(){
		document.body.style.cursor="auto";
	}

	//drpFolderAction pull down does not exist for anonymous users
	if(drpFolderAction) {
		drpFolderAction.onchange=RunFolderAction;
	}

	//RUN ONCE on page load
	//matweb.register_onload(SetSelectedFolder);
	
	SearchMatIDList = []; //an array of MatIDs
	//matweb.alert(MatIDList);
	
	//======================
	//Test Service 
	//======================
	//TestService("This is a test");
	//aci.matweb.WebServices.Folders.RaiseTestError(null,ErrorHandler,"testerror");
	
	function TestService(arg){
		aci.matweb.WebServices.Folders.HelloWorld(arg, TestService_CALLBACK,ErrorHandler,"TestService");
	}
	
	function TestService_CALLBACK(data,context){
		alert(data);
	}
	
	// =================================
	// This function is the onchange event for the material checkbox. See Code Behind.
	// The first time a material is checked, and there are no folders, the folder web service creates a NEW FOLDER
	// =================================
	function ToggleMat(chkBox,MatID,MaxFolderMats){
	
		HideFolderNameControls();

		var FolderID,Context;
		gCurrentCheckbox = chkBox;
		var matLink = $("lnkMatl_" + MatID);
		var MatName = matLink.innerHTML;
		//alert(MatName);

		var SelectedOption = drpFolderList.options[drpFolderList.selectedIndex];  

		if(gNewFolderID > 0)
			FolderID = gNewFolderID;
		else
			FolderID = SelectedOption.value; //this will be 0 if no folders exist yet
		//alert(FolderID);
		
		// call the folder web service 
		// See: ScriptManagerProxy object at the top of this page
		// Also See: /folders/FolderService.asmx & /App_Code/FolderService.cs
		if(chkBox.checked){
			Context = "AddMat";
			aci.matweb.WebServices.Folders.AddMaterial(FolderID, MatID, MatName, ToggleMat_CALLBACK,ErrorHandler,Context);
		}
		else{
			Context = "RemoveMat";
			aci.matweb.WebServices.Folders.RemoveMaterial(FolderID, MatID, ToggleMat_CALLBACK,ErrorHandler,Context);
		}
			
	}
	
	// =================================
	// CALLBACK FUNCTION
	// this function is called by the AJAX framework after the web service is invoked
	// The folder web service returns a ReturnData type object.
	// =================================
	function ToggleMat_CALLBACK(data,context){

		 //alert(data);
		// alert(context);

		var FolderMatCount = data.FolderMatCount
		var NewFolderID = data.NewFolderID
		var DisplayMessage = data.DisplayMessage
		var MaxFolderMats = data.FolderMaxMatls;
		
		txtFolderMatCount.value = FolderMatCount + "/" + MaxFolderMats;

		// If this is the first time the user checked the checkbox, and the user did not already have any folders, 
		// a new folder may have been created.
		// Save the new folderID to a hidden field, so we can reference it later.
		if(NewFolderID > 0){
			gNewFolderID = NewFolderID;
		}
		
		// alert(gNewFolderID);
		// alert(FolderCount);
		// alert(ReloadFolderList);
		// if(NewFolderID>0) alert(NewFolderID);
		// alert(MaxFolderMats);
		
		switch(context){
		case "AddMat":
			if(DisplayMessage){
				gCurrentCheckbox.checked = false;
				matweb.alert(DisplayMessage);
			}
			break    
		case "RemoveMat":
			break    
		default:
			matweb.Alert("Unhandeled Context in ToggleMat_CALLBACK() function: " + Context);
		}//end switch

	}//end function

	
	//=================================
	function CompareMaterials(){
	//=================================
		var SelectedFolderID = GetSelectedFolderID();
		window.location.href="/folders/Compare.aspx?FolderID=" + SelectedFolderID;
	}

	//=================================
	// Runs on drpFolderList.onchange, and also is called by other methods.
	// Sets the checkbox for each visible material in the folder.
	// Also sets txtFolderMatCount with the MatCount indicator for the selected folder.
	//=================================
	function SetSelectedFolder(){
		//alert("SetSelectedFolder()");
		var SelectedFolderID = GetSelectedFolderID();
		//alert("SelectedFolderID :" + SelectedFolderID)
		
		aci.matweb.WebServices.Folders.SetSelectedFolder(SelectedFolderID,SetSelectedFolder_CALLBACK,ErrorHandler,"update_txtFolderMatCount");
		
		HideFolderNameControls();
	}

	// =================================
	// CALLBACK FUNCTION
	// Returns a list of MatID's in the selected folder, and sets the checkbox for each material.
	// Also sets txtFolderMatCount with the MatCount indicator for the selected folder.
	// =================================
	function SetSelectedFolder_CALLBACK(data,context){

		// alert(data);
		// alert(context);

		// the folder web service returns an object with 4 fields
		//alert("data.FolderMatCount: " + data.FolderMatCount);
		//alert("data.FolderMaxMatls: " + data.FolderMaxMatls);
		//alert("data.DisplayMessage: " + data.DisplayMessage);
		//alert("data.MatIDList: " + data.FolderMatIDList);

		if(data.DisplayMessage){
			matweb.alert(data.DisplayMessage);
		}

		txtFolderMatCount.value = data.FolderMatCount + "/" + data.FolderMaxMatls;

		//alert(SearchMatIDList);
		var chkFolder;

		// Set checkboxes for any visible materials in the folder
		// loop over all materials in the search results for the current page...
		//alert('lenOfArray=' + SearchMatIDList.length);
		for(s=0;s<SearchMatIDList.length;s++){
			chkFolder = document.getElementById("chkFolder_" + SearchMatIDList[s]);
			//alert('index=' + SearchMatIDList[s]);
			chkFolder.checked = false; //clear any currently checked boxes...
			//loop over all materials in the folder...
			//If there is a match, make it checked.
			for(f=0;f<data.FolderMatIDList.length;f++){
				if(SearchMatIDList[s]==data.FolderMatIDList[f]){
					//alert("Match found: " + data.FolderMatIDList[f]);
					chkFolder.checked = true;
				}
			}
		}

	}//end function
	
	//=================================
	function RunFolderAction(){
	//=================================
	
		//alert("RunFolderAction()");
		var action = drpFolderAction.options[drpFolderAction.selectedIndex].value;
		var SelectedFolderName = drpFolderList.options[drpFolderList.selectedIndex].text;
		var FolderID = GetSelectedFolderID();
		var msg = "";
		
		switch(action){
			case "empty":
				aci.matweb.WebServices.Folders.EmptyFolder(FolderID,SetSelectedFolder_CALLBACK,ErrorHandler,action);
				msg = "Folder ["+SelectedFolderName+"] was cleared of materials";
				HideFolderNameControls();
				break;
			case "delete":
				var answer = confirm("Delete folder ["+SelectedFolderName+"] ?");
				if(answer){
					gOldFolderName = SelectedFolderName;
					aci.matweb.WebServices.Folders.DeleteFolder(FolderID,DeleteFolder_CALLBACK,ErrorHandler,action);
				}
				HideFolderNameControls();
				break;
			case "new":
				//setting up new folder controls
				if(drpFolderList.options.length >= gMaxFolders){
					matweb.alert("You have already added as many folders as you are currently allowed by the system (" + gMaxFolders + ").<p/> You may view our <a href=\"/membership/benefits.aspx\">benefits</a> page for the features that are allowed for your current user level.");
				}
				else{
				divFolderName.style.display="block";
				divFolderName.innerHTML="New Folder Name";
				tblFolderName.style.display="block";
				txtFolderName.value = "New Folder Name";
				txtFolderName.focus();
				txtFolderName.select();
				}
				break;
			case "rename":
				//setting up rename folder controls
				divFolderName.style.display="block";
				divFolderName.innerHTML="Rename Folder";
				tblFolderName.style.display="block";
				txtFolderName.value = SelectedFolderName;
				txtFolderName.focus();
				txtFolderName.select();
				break;
			case "managefolders":
				window.location.href="/folders/ListFolders.aspx";
				break;
			case "expSolidworks":
				ExportFolder(FolderID, 2, null);
				break;
			case "expALGOR":
				ExportFolder(FolderID, 3, null);
				break;
			case "expANSYS":
				ExportFolder(FolderID, 4, null);
				break;
			case "expNEiWorks":
				ExportFolder(FolderID, 5, null);
				break;
			case "expCOMSOL3":
				ExportFolder(FolderID, 6, '3.4 or newer');
				break;
			case "expCOMSOL4":
				ExportFolder(FolderID, 10, null);
				break;
			case "expETBX":
				ExportFolder(FolderID, 8, null);
				break;
			case "expSpaceClaim":
				ExportFolder(FolderID, 9, null);
				break;
			case "0":
				//do nothing
				drpFolderAction.selectedIndex = 0;
				break;
			default: 
				alert("Unhandled Action: " + action);
		}

		//drpFolderAction.selectedIndex=0;
		divUserMessage.innerHTML = msg;
		//alert("End RunFolderAction()");
		
	}
	
	function DeleteFolder_CALLBACK(DeleteWasSuccessfull,context){
		divUserMessage.innerHTML = "Folder ["+gOldFolderName+"] was deleted.";
		//Refresh the folder list and selected checkboxes
		aci.matweb.WebServices.Folders.GetFolderList(GetFolderList_CALLBACK,ErrorHandler,"DeleteFolder_CALLBACK");
	}	

	// rsw
	function ExportFolder(folderid, modeid, comsolversion){
		if(txtFolderMatCount.value.substring(0,1) == '0'){
			alert('Folder is empty, nothing to export.');
			drpFolderAction.selectedIndex = 0;
			return;
		}

		var ExportPath = "/folders/ListFolders.aspx";
		
		//alert(ExportPath);
		location.href = ExportPath;
		//window.open(ExportPath, '', '', '');
	}
	
	
	
	
	//===============================================================
	// This runs on the onclick event for btnApplyFolderName.
	// It handles creating a new folder, or renaming an existing one.
	//===============================================================
	function ApplyFolderName(){
	
		//alert("ApplyFolderName()");
		var action = drpFolderAction.options[drpFolderAction.selectedIndex].value;
		var NewFolderName = txtFolderName.value;
		var FolderID = GetSelectedFolderID();
		var msg = "";

		//alert(FolderID);

		//If we are showing the non-existant "virtual" folder "My Folder",
		//create it first, and then rename it.
		if(action=="rename" && FolderID==0)
			action="add_and_rename";

		switch(action){
			case "new":
				aci.matweb.WebServices.Folders.AddFolder(NewFolderName,AddRenameFolder_CALLBACK,ErrorHandler,action);
				break;
			case "rename":
				gOldFolderName = drpFolderList.options[drpFolderList.selectedIndex].text;
				aci.matweb.WebServices.Folders.RenameFolder(FolderID,NewFolderName,AddRenameFolder_CALLBACK,ErrorHandler,action);
				break;
			case "add_and_rename":
				gOldFolderName = drpFolderList.options[drpFolderList.selectedIndex].text;
				aci.matweb.WebServices.Folders.AddFolder(NewFolderName,AddRenameFolder_CALLBACK,ErrorHandler,action);
				break;
			default: 
				alert("Unhandled Action in ApplyFolderName(): " + action);
		}

		HideFolderNameControls();
		
	}
	
	//=================================
	// CALLBACK FUNCTION
	//=================================
	function AddRenameFolder_CALLBACK(ReturnData,context){
		//alert("AddRenameFolder_CALLBACK()");
		
		var msg;
		//Folder = ReturnData.Folder;
	
		switch(context){
			case "new":
				//gNewFolderID = Folder.FolderID;
				gNewFolderID = ReturnData.NewFolderID;
				msg = "The folder [" + ReturnData.NewFolderName + "] was created.";
				break;
			case "rename":
				msg = "The folder [" + gOldFolderName+ "] was renamed to [" + ReturnData.NewFolderName + "].";
				break;
			case "add_and_rename":
				//gNewFolderID = Folder.FolderID;
				gNewFolderID = ReturnData.NewFolderID;
				msg = "The folder [" + gOldFolderName+ "] was renamed to [" + ReturnData.NewFolderName + "].";
				break;
		}
		
		divUserMessage.innerHTML = msg;
		
		//refresh the folder list, so it includes the new folder
		aci.matweb.WebServices.Folders.GetFolderList(GetFolderList_CALLBACK,ErrorHandler,"AddFolder");

	}
	
	//=================================
	function HideFolderNameControls(){
	//=================================
		//These fields will not exist for anonymous users
		if(drpFolderAction){
			drpFolderAction.selectedIndex=0;
			divFolderName.style.display="none";
			tblFolderName.style.display="none";
		}
	}
	
	//=================================
	function GetSelectedFolderID(){
	//=================================
		var FolderID = 0;
		//if one of the processeses created a new folder id, then use it.
		// alert('gNewFolderID=' + gNewFolderID);
		if(gNewFolderID > 0){
			FolderID = gNewFolderID;
			//gNewFolderID = 0;//clear the new folder id, so we don't use it again.
		}
		else 
			if(drpFolderList != null){
				if(drpFolderList.options.length == 0){
					FolderID = gSelectedFolderID;
				}
				else{
					var SelectedOption = drpFolderList.options[drpFolderList.selectedIndex];  
					FolderID = SelectedOption.value; //this will be 0 if no folders exist yet
				}
			}
		return FolderID;
	}

	//=================================
	function UnregisteredUserMessage(chkBox){
	//=================================
		matweb.alert("This feature is reserved for registered users.<p/><a href='/membership/regstart.aspx'>Click here to register.</a>","Reserved Feature");
		chkBox.checked = false;
	}

	// =================================
	// CALLBACK FUNCTION
	// The folder web service returns a JS array of iBatis data objects of type: aci.matweb.data.folder.Folder 
	// The drop down list of folders is set using this list.
	// =================================
	function GetFolderList_CALLBACK(iFolderList,Context){
	
		//alert(iFolderList);
		//alert('iFolderList.length: ' + iFolderList.length);

		if(drpFolderList != null){
		
		var SelectedFolderID = GetSelectedFolderID();//remember the current selected folder, so we can use it later
		var opt;
		var folder;

		//clear the options and reload them
		drpFolderList.options.length = 0;

		//add a default empty folder...		
		if(iFolderList.length==0){
			opt = document.createElement('OPTION');
			opt.value = "0";
			opt.text = "My Folder";
			drpFolderList.options.add(opt);
			i++;
		}
		
		for(var i = 0;i < iFolderList.length;i++){
			opt = document.createElement('OPTION');
			folder = iFolderList[i];
			//alert(folder.FolderID);
			//alert(folder.FolderName);
			opt.value = folder.FolderID;
			opt.text = folder.FolderName;
			if(folder.FolderID == SelectedFolderID) opt.selected = true;
			drpFolderList.options.add(opt);
		}

		//clear the new folder id, so we don't use it again. 
		gNewFolderID = 0;
		
		//Set checkboxes and folder count fields for the new selected folder
		SetSelectedFolder();
		}

	}
	
	// =================================
	// ON ERROR CALLBACK FUNCTION
	// =================================
	function ErrorHandler(error,context) {
	
			var stackTrace = error.get_stackTrace();
			var message = error.get_message();
			var statusCode = error.get_statusCode();
			var exceptionType = error.get_exceptionType();
			var timedout = error.get_timedOut();
			var Debug = false;

			// Compose the error message...
			var ErrMsg =     
					"<strong>Stack Trace</strong><br/>" +  stackTrace + "<br/>" +
					"<strong>WebService Error</strong><br/>" + message + "<br/>" +
					"<strong>Status Code</strong><br/>" + statusCode + "<br/>" +
					"<strong>Exception Type</strong><br/>" + exceptionType + "<br/>" +
					"<strong>JS Context</strong><br/>" + context + "<br/>" +
					"<strong>Timedout</strong><br/>" + timedout + "<br/>" +
					"<strong>Source Page</strong><br/>" + "/search/MaterialGroupSearch.aspx";
			//matweb.alert(ErrMsg,"Error in WebService",null,"500");
			if(Debug){
				matweb.alert(ErrMsg,"Error in WebService",null,"500");
				//alert(ErrMsg);
			}
			else{
				ErrMsg = escape(ErrMsg);
				//matweb.alert(ErrMsg);
				//window.location.href="/errorJS.aspx?ErrMsg=" + ErrMsg;
				divUserMessage.innerHTML = "Problem retrieving folder data...Some folder features may not work.";
			}
		
	}
	
	function ShowInterpolationMessage(){
		InterpolationMsg = "The material was found via interpolation, so no data points can be displayed. See the material data sheet for actual data points.";
		matweb.alert(InterpolationMsg);
	}
	
</script>

		</td>
	</tr>
	</table>

<hr />
<a id="help"></a>
<table><tr><td>
<p/><b>Instructions:</b> This page allows you to quickly access all of the polymers/plastics, metals, ceramics, fluids, and other engineering materials
in the MatWeb material property database.&nbsp; Just select the material category you would like to find from the tree.  Click on the [+] icon to open subcategory branches in the tree.
Then click the <b>'Find'</b> button next to its box.&nbsp; You can then follow the links to the most complete data sheets on the Web, with information on over 1000 available properties, including dielectric
constant, Poisson's ratio, glass transition temperature, dissipation factor, and Vicat softening point.
</td><td><a href="https://proto3000.com"><img src="/images/assets/proto3000.jpg" border = "0"><br>

Proto3000 - Rapid Prototyping</a>
</td></tr></table>

<p/><span class="hiText">Notes:</span>

<ul>
  <li>Visit MatWeb's <a href="/search/PropertySearch.aspx">property-based search page</a> to limit your results to materials that meet specific property performance criteria.</li>
  <li>Text searches can be done from any page - use the <b>Quick Search</b> box in the top right of the page.</li>
  <li>More <a href="/help/strategy.aspx"><strong>Search Strategies</strong></a> on how to find information in MatWeb.</li>
   <li><a href="/search/GetAllMatls.aspx">Show All Materials</a> - an inefficient drill-down to data sheets.  Registered users who are logged in omit a step in this drill down process.</li>
</ul>

<p/><a href="#Top">Top</a>

<script type="text/javascript">

	var txtMatGroupText = document.getElementById("ctl00_ContentMain_txtMatGroupText");
	var txtMatGroupID = document.getElementById("ctl00_ContentMain_txtMatGroupID");
	var divMatGroupName = document.getElementById("ctl00_ContentMain_divMatGroupName");

	function ucMatGroupFinder1_OnSelected(MatGroupID,MatGroupText){
		txtMatGroupText.value = MatGroupText;
		txtMatGroupID.value = MatGroupID;
		divMatGroupName.innerHTML = MatGroupText;
		//__doPostBack("ctl00$ContentMain$btnSubmit","");
	}

	function ucMatGroupTree_OnNodeClick(NodeText,MatGroupID){
		txtMatGroupText.value = NodeText;
		txtMatGroupID.value = MatGroupID;
		divMatGroupName.innerHTML = NodeText;
		//__doPostBack("ctl00$ContentMain$btnSubmit","");
	}
	
	function validate(){
		if (txtMatGroupID.value == ""){
			matweb.alert("Please select a material category","User Input Error");
			return false;
		}
		
		return true;
		
	}

</script>


</div>
<!-- ===================================== END MAIN CONTENT ========================================== -->

<table class="tabletight" style="width:100%" >
        <tr>
                <td align="center" class="footer" colspan="3">
                        <br /><br />
                
                        <a href="/membership/regupgrade.aspx" class="footlink"><span class="subscribeLink">Subscribe to Premium Services</span></a><br/>
                
                        <span class="footer"><b>Searches:</b>&nbsp;&nbsp;</span>
                        <a href="/search/AdvancedSearch.aspx" class="footlink"><span class="foot">Advanced</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/search/CompositionSearch.aspx" class="footlink"><span class="foot">Composition</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/search/PropertySearch.aspx" class="footlink"><span class="foot">Property</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/search/MaterialGroupSearch.aspx" class="footlink"><span class="foot">Material&nbsp;Type</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/search/SearchManufacturerName.aspx" class="footlink"><span class="foot">Manufacturer</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/search/SearchTradeName.aspx" class="footlink"><span class="foot">Trade&nbsp;Name</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/search/SearchUNS.aspx" class="footlink"><span class="foot">UNS Number</span></a>
                        <br />
                        <span class="footer"><b>Other&nbsp;Links:</b>&nbsp;&nbsp;</span>
                        <a href="/services/advertising.aspx" class="footlink"><span class="foot">Advertising</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/services/submitdata.aspx" class="footlink"><span class="foot">Submit&nbsp;Data</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/services/databaselicense.aspx" class="footlink"><span class="foot">Database&nbsp;Licensing</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/services/webhosting.aspx" class="footlink"><span class="foot">Web&nbsp;Design&nbsp;&amp;&nbsp;Hosting</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/clickthrough.aspx?addataid=277" class="footlink"><span class="foot">Trade&nbsp;Publications</span></a>

                        <br />
                        <a href="/reference/suppliers.aspx" class="footlink"><span class="foot">Supplier&nbsp;List</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/tools/unitconverter.aspx" class="footlink"><span class="foot">Unit&nbsp;Converter</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/tools/contents.aspx#reference" class="footlink"><span class="foot">Reference</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/reference/link.aspx" class="footlink"><span class="foot">Links</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/help/help.aspx" class="footlink"><span class="foot">Help</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/services/contact.aspx" class="footlink"><span class="foot">Contact&nbsp;Us</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/tools/contents.aspx" class="footlink"><span class="foot" style="color:red;">Site&nbsp;Map</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/reference/faq.aspx" class="footlink"><span class="foot">FAQ</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/index.aspx" class="footlink"><span class="foot">Home</span></a>
                        <br />&nbsp;
                        <div id="fb-root"></div>

<table><tr><td><script src="http://connect.facebook.net/en_US/all.js#xfbml=1"></script><fb:like-box href="https://www.facebook.com/MatWeb.LLC" width="175" show_faces="false" stream="false" header="false"></fb:like-box>

</td><td>

<a href="https://twitter.com/MatWeb" class="twitter-follow-button" data-show-count="false" data-show-screen-name="false">Follow @MatWeb</a> <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>

</td></tr></table>
                </td>
        </tr>
        <tr>
                <td colspan="3"><img src="/images/bluebar.gif" width="100%" height="5" alt="" /></td>
        </tr>
        <tr>
                <td style="background-image:url(/images/gray.gif);"><img src="/images/spacer.gif" width="5" height="1" alt=""/></td>
                <td style="background-image:url(/images/gray.gif);">
                        <span class="footer">
                        Please read our <a href="/reference/terms.aspx" class="footlink"><span class="foot">License Agreement</span></a> regarding materials data and our  <a href="/reference/privacy.aspx" class="footlink"><span class="foot">Privacy Policy</span></a>.
                        Questions or comments about MatWeb? Please contact us at
                        <a href="mailto:webmaster@matweb.com" class="footlink"><span class="foot">webmaster@matweb.com</span></a>. We appreciate your input.
                        <br /><br />
                        The contents of this web site, the MatWeb logo, and "MatWeb" are Copyright 1996-2024
                        by MatWeb, LLC. MatWeb is intended for personal, non-commercial use. The contents, results, and technical data from this site
                        may not be reproduced either electronically, photographically or substantively without permission from MatWeb, LLC.
                        </span>
                        <br />
                </td>

                <td style="background-image:url(/images/gray.gif);"><img src="/images/spacer.gif" width="5" height="1" alt=""/></td>
        </tr>
</table>




<script type="text/javascript">
//<![CDATA[
var ctl00_ContentMain_ucMatGroupTree_msTreeView_ImageArray =  new Array('', '', '', '/WebResource.axd?d=zLp3QJeZSj4-fsnL2_raV70Mk3pr-OdjL_rvrZzXUTCnZ6LU5Dg_Tc5XZg4eZpT9O0gxZJrJcxGb23XJofeGD9JhYLr2wl0VkQZnJOx0T0ShBosq0&t=638313619312541215', '/WebResource.axd?d=vFualGN2ZZ4fny_RX_gYiUTY6FBLUapne4VfY6P3sGiyrOYd-Ji2sWg_olT-exaztendUUEOoH87RS2l44bpZTscfmmv4MKRqJK8rgesmkrNfP2O0&t=638313619312541215', '/WebResource.axd?d=4nLCbQ2Yfc2i_msrQXqKzK8pLQD-GrNVZ7DeXU-La2qatczrgeCLazv0RzIkDDHyYfLFalCBO8s71lRS8yifxTChF9dUJvTLxNEeeFlghyuY0dtM0&t=638313619312541215', '/WebResource.axd?d=uEI6Enq15_q4pXO6g0E7vtAqtD5WIokAEQ2Vz8V3Yi36cefediLfyCwENkPd-w1GxTXu-HkNqcDlrYOw8bxgGqeHJtzc-PBlTa9nrZRfdIyj8fQh0&t=638313619312541215', '/WebResource.axd?d=hzeik1gGhZrfDZXUWCMsBOx5b43bIFor8UCTzm_wuh92b9Z-WQU4_fQCSUw6CaP5tnc9LTU0jl0sy4Y6Cf_c8q8HFXRqeHQuscXIqqLNSZkgl2cu0&t=638313619312541215', '/WebResource.axd?d=6v5qM6VKQYD3XNwwXEy7Tix7dzVdqDJ8QTRIlUPrzXQqNsXQgUwIfmn34ZzRORd_IZj0bF4vkZzID1HuSFE5uKyLgaPYeAYE8VT8HYfb3Rk6zsDw0&t=638313619312541215', '/WebResource.axd?d=d9nh8nEM1YCqIfE9jLzNhPyFIZqARvlFExwnFG0vbvskO-A2qORlhTyUVIxDtSwldGrOp8o9ef8lZ9SRhPQ9rv9TkGV-NVYvH9_yHT6CvWpIPF4S0&t=638313619312541215', '/WebResource.axd?d=7_V5n1KYSQsj6PoSqgIxb0egBsAa91jSXPkbmM6FaiF14-JsNnpUDnfSdxZcvfd0OQCLDtC24pMIXigO-41cL2rsq-0xea-SSG7yZUyRcliFFqe70&t=638313619312541215', '/WebResource.axd?d=TinC6igX1vPDgWh7DstWwBTHTyH4uuybDU4NW47jJD06wHdAO3YyJC_eKK1Zn5oIbwqRo4Cri8YwhA82G-LD0p14yEF1EfoIZvKleYMIuQ2K1hkE0&t=638313619312541215', '/WebResource.axd?d=4B0dvJqmzY4ukTboQq7lmxokRL9NTsoWh4yErB11Tb-ucvMw_p_0mxYVn4U1Uz6Jk795umrO0C4-B-8Yq1Kxxz6uyDT15O2BSCb8349f4YWDdIVy0&t=638313619312541215', '/WebResource.axd?d=TUiibr9hjS_MlNf09dKZECe1z56fbaPbfgJfC5eYJi0TtdBviUcA1JQJLV81aqunzpXysgruhL3ihpgd7ziL5p9SQ86xVLNiUqbYpJnW9Rj7sN1E0&t=638313619312541215', '/WebResource.axd?d=vUBrM2nNhECNbFO0AJKq52p2zWrY5XG8nWgCDL4AfsrexStb0HREDV6GHMvdkAGDInxYtTyWXuxFUIiQv3JX2fScVRwQsrVv5srHrjOIeC3ayAG20&t=638313619312541215', '/WebResource.axd?d=Z-LP5lKcsnv2A9UpExXu-5leQYIAZMYp6SXNognZi1r66YT8u24-YjJaxjisE9ruYU8NKnNz72H4shclsk8PM3S5M-VYMk_F_G5fcd3ra6Sj9CaS0&t=638313619312541215', '/WebResource.axd?d=QI4SACqRn7PxBBHPfkR7peFMAEv7lX1_iZ_7jncKwxRTlvhSAncd5K2oRqw8J8WmJjWQcbGgEblhrufy-hUjZMa1YLUC47mjfV1eS4LM3kUqLT960&t=638313619312541215', '/WebResource.axd?d=ayrFqNSzdiTAdowDVbEH-ZzKbvw5bCq7onyCSAcJhCVPVIHlUB9M_fo8BLUTeGpbtcoMiyFy1MoeEIsweMrpVandBKC4vAK-NCBcfn2FIreK8mEJ0&t=638313619312541215', '/WebResource.axd?d=OK_uWiuoGURaJ3CC0fVlQ7v9GUnIAkx03w03rQ2ENqcwdObLiQwK7B9yRFyAWj9tuUqsa0874ksgkaRo8KirB7q1ZL_huh7gCq_XehXfHMEKMiAl0&t=638313619312541215');
//]]>
</script>


<script type="text/javascript">
//<![CDATA[
(function() {var fn = function() {AjaxControlToolkit.ModalPopupBehavior.invokeViaServer('ctl00_ContentMain_ucPopupMessage1_ModalPopupExtender1', false); Sys.Application.remove_load(fn);};Sys.Application.add_load(fn);})();
var callBackFrameUrl='/WebResource.axd?d=Mt7MojvAyTqAEcx0MpKgcF_QVa09tLyMQS6c5JLNO4j2M_F6CMxYnN0HdVYxyrhggOpkTwyNqQ51iS6UoHXgJqHBROE1&t=638313619312541215';
WebForm_InitCallback();var ctl00_ContentMain_ucMatGroupTree_msTreeView_Data = new Object();
ctl00_ContentMain_ucMatGroupTree_msTreeView_Data.images = ctl00_ContentMain_ucMatGroupTree_msTreeView_ImageArray;
ctl00_ContentMain_ucMatGroupTree_msTreeView_Data.collapseToolTip = "Collapse {0}";
ctl00_ContentMain_ucMatGroupTree_msTreeView_Data.expandToolTip = "Expand {0}";
ctl00_ContentMain_ucMatGroupTree_msTreeView_Data.expandState = theForm.elements['ctl00_ContentMain_ucMatGroupTree_msTreeView_ExpandState'];
ctl00_ContentMain_ucMatGroupTree_msTreeView_Data.selectedNodeID = theForm.elements['ctl00_ContentMain_ucMatGroupTree_msTreeView_SelectedNode'];
for (var i=0;i<19;i++) {
var preLoad = new Image();
if (ctl00_ContentMain_ucMatGroupTree_msTreeView_ImageArray[i].length > 0)
preLoad.src = ctl00_ContentMain_ucMatGroupTree_msTreeView_ImageArray[i];
}
ctl00_ContentMain_ucMatGroupTree_msTreeView_Data.lastIndex = 8;
ctl00_ContentMain_ucMatGroupTree_msTreeView_Data.populateLog = theForm.elements['ctl00_ContentMain_ucMatGroupTree_msTreeView_PopulateLog'];
ctl00_ContentMain_ucMatGroupTree_msTreeView_Data.treeViewID = 'ctl00$ContentMain$ucMatGroupTree$msTreeView';
ctl00_ContentMain_ucMatGroupTree_msTreeView_Data.name = 'ctl00_ContentMain_ucMatGroupTree_msTreeView_Data';
Sys.Application.initialize();
Sys.Application.add_init(function() {
    $create(AjaxControlToolkit.ModalPopupBehavior, {"BackgroundCssClass":"modalBackground","OkControlID":"ctl00_ContentMain_ucPopupMessage1_btnOK","PopupControlID":"ctl00_ContentMain_ucPopupMessage1_divPopupMessage","PopupDragHandleControlID":"ctl00_ContentMain_ucPopupMessage1_trTitleRow","dynamicServicePath":"/search/MaterialGroupSearch.aspx","id":"ctl00_ContentMain_ucPopupMessage1_ModalPopupExtender1"}, null, null, $get("ctl00_ContentMain_ucPopupMessage1_hndPopupControl"));
});
Sys.Application.add_init(function() {
    $create(AjaxControlToolkit.TextBoxWatermarkBehavior, {"ClientStateFieldID":"ctl00_ContentMain_UcMatGroupFinder1_TextBoxWatermarkExtender2_ClientState","WatermarkCssClass":"greyOut","WatermarkText":"Type at least 4 characters here...","id":"ctl00_ContentMain_UcMatGroupFinder1_TextBoxWatermarkExtender2"}, null, null, $get("ctl00_ContentMain_UcMatGroupFinder1_txtSearchText"));
});
//]]>
</script>
</form>


<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-15290815-1");
pageTracker._setDomainName(".matweb.com");
pageTracker._trackPageview();
} catch(err) {}</script>

<!-- 20160905 insert SpecialChem javascript -->
<script type="text/javascript" src="//collect.specialchem.com/collect.js"></script>
<script type="text/javascript">
    //<![CDATA[
    var _spc = (_spc || []);
    _spc.push(['init',
    {
        partner: 'MatWeb'
        
        //, iid: '12345'
    }]);
    //]]>
</script>
<script> (function(){ var s = document.createElement('script'); var h = document.querySelector('head') || document.body; s.src = 'https://acsbapp.com/apps/app/dist/js/app.js'; s.async = true; s.onload = function(){ acsbJS.init({ statementLink : '', footerHtml : '', hideMobile : false, hideTrigger : false, disableBgProcess : false, language : 'en', position : 'right', leadColor : '#146FF8', triggerColor : '#146FF8', triggerRadius : '50%', triggerPositionX : 'right', triggerPositionY : 'bottom', triggerIcon : 'people', triggerSize : 'bottom', triggerOffsetX : 20, triggerOffsetY : 20, mobile : { triggerSize : 'small', triggerPositionX : 'right', triggerPositionY : 'bottom', triggerOffsetX : 20, triggerOffsetY : 20, triggerRadius : '20' } }); }; h.appendChild(s); })();</script>
</body>
</html>
