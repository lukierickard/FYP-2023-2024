

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head id="ctl00_mainHeader"><title>
	Metal, Plastic, and Ceramic Search Index
</title><meta http-equiv="Content-Style-Type" content="text/css" /><meta http-equiv="Content-Language" content="en" />

        <script src="/includes/flash.js" type="text/javascript"></script>

        <script src="/includes/prototype.js" type="text/javascript"></script>
        <!-- script src="/includes/scriptaculous/effects.js" type="text/javascript"></script -->
        <script src="/includes/matweb.js" type="text/javascript"></script>
        <link rel="stylesheet" href="/includes/main.css" />

        <script type="text/javascript">

                function _onload(){
                        matweb.debug = false; // ;
                        //matweb.writeOverlay();
                }

                function _onkeydown(evt){
                        //PREVENT GLOBAL AUTO-SUBMIT OF FORMS
                        return matweb.DisableAutoSubmit(evt);
                }

                //EXAMPLE USAGE:
                //matweb.register_onload(function(){alert("hello from onload event");});
                //matweb.writeOverlay();


        </script>

        <style type="text/css" media="screen">
        @import url("/ad_IES/css/styles.css");
        </style>
<meta name="KEYWORDS" content="carbon steels, low alloy steels, corrosion resistant stainless, high temperature stainless, heat resistant stainless steels, AISI steel alloy specifications, high strength steel alloys, tensile strength of steels, maraging, superalloy, lead alloy, magnesium, tin alloy, tungsten, zinc, precious metal, pure metallic element, TPE elastomer, dielectric constant, plastic dissipation factor, nylon glass transition temperature, ionomer vicat softening point, poisson's ratio, polyetherimide, fluoropolymer, silicone, polyester dielectric constant" /><meta name="DESCRIPTION" content="Searchable list of plastics, metals, and ceramics categorized into a fields like polypropylene, nylon, ABS, aluminum alloys, oxides, etc.  Search results are detailed data sheets with tensile strength, density, dielectric constant, dissipation factor, poisson's ratio, glass transition temperature, and Vicat softening point." /><meta name="ROBOTS" content="NOFOLLOW" /><style type="text/css">
	.ctl00_ContentMain_ucMatGroupTree_msTreeView_0 { text-decoration:none; }
	.ctl00_ContentMain_ucMatGroupTree_msTreeView_1 { color:Black; }
	.ctl00_ContentMain_ucMatGroupTree_msTreeView_2 { padding:0px 3px 0px 3px; }

</style></head>

<body onload="_onload();" onkeydown="return _onkeydown(event);">

<a id="Top"></a>

<div id="divSiteName" style="position:absolute; top:0; left:0 ;">
<a href="/index.aspx"><img src="/images/sitelive.gif" alt="" /></a>
</div>

<div id="divPopupOverlay" style="display:none;"></div>

<div id="divPopupAlert" class="popupAlert" style="display:none;">
        <table style="width:100%; height:100%; border-style:outset; border-width:5px; border-color:blue;" >
                <tr><th style=" height:25px; background-color:Maroon; font-size:14px; color:White; "><span id="spanPopupAlertTitle">System Message</span></th></tr>
                <tr><td style=" vertical-align:top; padding:3px; background-color:White;" id="tdPopupAlertMessage" ></td></tr>
                <tr><td style=" height:25px; text-align:center; background-color:Silver;"><input type="button" id="btnPopupAlert" onclick="matweb.alertClose();" style="" value="OK" /></td></tr>
        </table>
</div>

<form name="aspnetForm" method="post" action="MaterialGroupSearch.aspx" onsubmit="javascript:return WebForm_OnSubmit();" id="aspnetForm">
<div>
<input type="hidden" name="__EVENTTARGET" id="__EVENTTARGET" value="" />
<input type="hidden" name="__EVENTARGUMENT" id="__EVENTARGUMENT" value="" />
<input type="hidden" name="__LASTFOCUS" id="__LASTFOCUS" value="" />
<input type="hidden" name="ctl00_ContentMain_ucMatGroupTree_msTreeView_ExpandState" id="ctl00_ContentMain_ucMatGroupTree_msTreeView_ExpandState" value="ccccccnc" />
<input type="hidden" name="ctl00_ContentMain_ucMatGroupTree_msTreeView_SelectedNode" id="ctl00_ContentMain_ucMatGroupTree_msTreeView_SelectedNode" value="" />
<input type="hidden" name="ctl00_ContentMain_ucMatGroupTree_msTreeView_PopulateLog" id="ctl00_ContentMain_ucMatGroupTree_msTreeView_PopulateLog" value="" />
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="/wEPDwULLTE2Nzc3MDc2MDAPZBYCZg9kFgICAQ9kFioCAQ9kFgJmDxYCHgRUZXh0BWdEYXRhIHNoZWV0cyBmb3Igb3ZlciA8c3BhbiBjbGFzcz0iZ3JlZXRpbmciPjE4MCwwMDA8L3NwYW4+ICBtZXRhbHMsIHBsYXN0aWNzLCBjZXJhbWljcywgYW5kIGNvbXBvc2l0ZXMuZAICD2QWAmYPFgIfAAUESE9NRWQCAw9kFgJmDxYCHwAFBlNFQVJDSGQCBA9kFgJmDxYCHwAFBVRPT0xTZAIFD2QWAmYPFgIfAAUJU1VQUExJRVJTZAIGD2QWAmYPFgIfAAUHRk9MREVSU2QCBw9kFgJmDxYCHwAFCEFCT1VUIFVTZAIID2QWAmYPFgIfAAUDRkFRZAIJD2QWAmYPFgIfAAUHQUNDT1VOVGQCCg9kFgJmDxYCHwAFBkxPRyBJTmQCCw8QZGQWAWZkAgwPZBYCZg8WAh8ABQhTZWFyY2hlc2QCDQ9kFgJmDxYCHwAFCEFkdmFuY2VkZAIOD2QWAmYPFgIfAAUIQ2F0ZWdvcnlkAg8PZBYCZg8WAh8ABQhQcm9wZXJ0eWQCEA9kFgJmDxYCHwAFBk1ldGFsc2QCEQ9kFgJmDxYCHwAFClRyYWRlIE5hbWVkAhIPZBYCZg8WAh8ABQxNYW51ZmFjdHVyZXJkAhMPZBYCAgEPZBYCZg8WAh8ABRlSZWNlbnRseSBWaWV3ZWQgTWF0ZXJpYWxzZAIVD2QWEAIDD2QWAmYPFgIfAAWZATxhIGhyZWY9Ii9jbGlja3Rocm91Z2guYXNweD9hZGRhdGFpZD0xMzQ5Ij48aW1nIHNyYz0iL2ltYWdlcy9hc3NldHMvbWV0YWxtZW4tQWx1bWludW0ucG5nIiBib3JkZXI9MCBhbHQ9Ik1ldGFsbWVuIFNhbGVzIiB3aWR0aCA9ICI3MjgiIGhlaWdodCA9ICI5MCI+PC9hPmQCBA9kFgICAg8WAh4Fc3R5bGUFGWRpc3BsYXk6bm9uZTt3aWR0aDoyNTBweDsWBgIBD2QWAmYPZBYCZg8PFgIfAAUSVXNlciBJbnB1dCBQcm9ibGVtZGQCAw8PFgIfAGVkZAIFDxYCHgV2YWx1ZQUCT0tkAgUPZBYEAgEPD2QWAh8BBQxXaWR0aDozMDRweDtkAgIPEBYEHwEFK3Bvc2l0aW9uOmFic29sdXRlO2Rpc3BsYXk6bm9uZTtXaWR0aDozMDhweDseBHNpemUFAjE4ZGRkAgYPDxYEHghDbGllbnRJRAIBHgpMYW5ndWFnZUlEAgFkFgJmDxQrAAkPFg4eF1BvcHVsYXRlTm9kZXNGcm9tQ2xpZW50Zx4SRW5hYmxlQ2xpZW50U2NyaXB0Zx4JU2hvd0xpbmVzZx4ITm9kZVdyYXBnHg1OZXZlckV4cGFuZGVkZB4MU2VsZWN0ZWROb2RlZB4JTGFzdEluZGV4AghkFggeC05vZGVTcGFjaW5nGwAAAAAAAAAAAQAAAB4JRm9yZUNvbG9yCiMeEUhvcml6b250YWxQYWRkaW5nGwAAAAAAAAhAAQAAAB4EXyFTQgKEgBhkZGRkZGQUKwAJBSMyOjAsMDowLDA6MSwwOjIsMDozLDA6NCwwOjUsMDo2LDA6NxQrAAIWDB8ABRJDYXJib24gKDg2NiBtYXRscykeBVZhbHVlBQMyODMeCEV4cGFuZGVkaB4MU2VsZWN0QWN0aW9uCyouU3lzdGVtLldlYi5VSS5XZWJDb250cm9scy5UcmVlTm9kZVNlbGVjdEFjdGlvbgIeC05hdmlnYXRlVXJsBUFqYXZhc2NyaXB0OnVjTWF0R3JvdXBUcmVlX09uTm9kZUNsaWNrKCdDYXJib24gKDg2NiBtYXRscyknLCcyODMnKR4QUG9wdWxhdGVPbkRlbWFuZGdkFCsAAhYMHwAFFUNlcmFtaWMgKDEwMDA0IG1hdGxzKR8RBQIxMR8SaB8TCysEAh8UBUNqYXZhc2NyaXB0OnVjTWF0R3JvdXBUcmVlX09uTm9kZUNsaWNrKCdDZXJhbWljICgxMDAwNCBtYXRscyknLCcxMScpHxVnZBQrAAIWDB8ABRJGbHVpZCAoNzU2MiBtYXRscykfEQUBNR8SaB8TCysEAh8UBT9qYXZhc2NyaXB0OnVjTWF0R3JvdXBUcmVlX09uTm9kZUNsaWNrKCdGbHVpZCAoNzU2MiBtYXRscyknLCc1JykfFWdkFCsAAhYMHwAFE01ldGFsICgxNzA1MiBtYXRscykfEQUBOR8SaB8TCysEAh8UBUBqYXZhc2NyaXB0OnVjTWF0R3JvdXBUcmVlX09uTm9kZUNsaWNrKCdNZXRhbCAoMTcwNTIgbWF0bHMpJywnOScpHxVnZBQrAAIWDB8ABSdPdGhlciBFbmdpbmVlcmluZyBNYXRlcmlhbCAoODA2MyBtYXRscykfEQUDMjg1HxJoHxMLKwQCHxQFVmphdmFzY3JpcHQ6dWNNYXRHcm91cFRyZWVfT25Ob2RlQ2xpY2soJ090aGVyIEVuZ2luZWVyaW5nIE1hdGVyaWFsICg4MDYzIG1hdGxzKScsJzI4NScpHxVnZBQrAAIWDB8ABRVQb2x5bWVyICg5NzYzNSBtYXRscykfEQUCMTAfEmgfEwsrBAIfFAVDamF2YXNjcmlwdDp1Y01hdEdyb3VwVHJlZV9Pbk5vZGVDbGljaygnUG9seW1lciAoOTc2MzUgbWF0bHMpJywnMTAnKR8VZ2QUKwACFgwfAAUYUHVyZSBFbGVtZW50ICg1MDcgbWF0bHMpHxEFAzE4NB8SaB8TCysEAh8UBUdqYXZhc2NyaXB0OnVjTWF0R3JvdXBUcmVlX09uTm9kZUNsaWNrKCdQdXJlIEVsZW1lbnQgKDUwNyBtYXRscyknLCcxODQnKR8VaGQUKwACFgwfAAUlV29vZCBhbmQgTmF0dXJhbCBQcm9kdWN0cyAoMzk4IG1hdGxzKR8RBQMyNzcfEmgfEwsrBAIfFAVUamF2YXNjcmlwdDp1Y01hdEdyb3VwVHJlZV9Pbk5vZGVDbGljaygnV29vZCBhbmQgTmF0dXJhbCBQcm9kdWN0cyAoMzk4IG1hdGxzKScsJzI3NycpHxVnZGQCBw8WAh4JaW5uZXJodG1sBSY3MDAwIFNlcmllcyBBbHVtaW51bSBBbGxveSAoMTUyIG1hdGxzKWQCDQ8PFgIeB1Zpc2libGVoZGQCDg8PFgIfF2dkFgQCAQ8PFgIfAAUDMTU2ZGQCAw8PFgIfAAUaNzAwMCBTZXJpZXMgQWx1bWludW0gQWxsb3lkZAIPDw8WBh4LQ3VycmVudFBhZ2UCAR4PQ3VycmVudFNlYXJjaElEAr7G/BIfF2dkFgQCAg8PFgIfAGVkZAIDDw8WAh8XZ2QWPgIBDw8WAh8ABQMxNTZkZAIDDxBkEBUBATEVAQExFCsDAWcWAWZkAgUPDxYCHwAFATFkZAIHDw8WCB8ABQtbUHJldiBQYWdlXR8OCk4eB0VuYWJsZWRoHxACBGRkAgkPDxYIHwAFC1tOZXh0IFBhZ2VdHw4KTh8QAgQfGmhkZAILDxBkZBYBAgVkAg0PFgIfF2hkAg8PFgIfF2hkAhEPEA9kFgIeCG9uY2hhbmdlBRNTZXRTZWxlY3RlZEZvbGRlcigpZGRkAhUPFgIfF2gWAgIBDxBkZBYBZmQCFw9kFgQCAQ8PFgQfAGUfGmhkZAICDw8WAh8XaGRkAhkPDxYEHwAFDU1hdGVyaWFsIE5hbWUfGmhkZAIbDw8WAh8XaGRkAh0PDxYCHxdoZGQCHw9kFgYCAQ8PFgQfAAUFUHJvcDEfGmhkZAIDDw8WAh8XaGRkAgUPDxYCHxdoZGQCIQ9kFgYCAQ8PFgQfAAUFUHJvcDIfGmhkZAIDDw8WAh8XaGRkAgUPDxYCHxdoZGQCIw9kFgYCAQ8PFgQfAAUFUHJvcDMfGmhkZAIDDw8WAh8XaGRkAgUPDxYCHxdoZGQCJQ9kFgYCAQ8PFgQfAAUFUHJvcDQfGmhkZAIDDw8WAh8XaGRkAgUPDxYCHxdoZGQCJw9kFgYCAQ8PFgQfAAUFUHJvcDUfGmhkZAIDDw8WAh8XaGRkAgUPDxYCHxdoZGQCKQ9kFgYCAQ8PFgQfAAUFUHJvcDYfGmhkZAIDDw8WAh8XaGRkAgUPDxYCHxdoZGQCKw9kFgYCAQ8PFgQfAAUFUHJvcDcfGmhkZAIDDw8WAh8XaGRkAgUPDxYCHxdoZGQCLQ9kFgYCAQ8PFgQfAAUFUHJvcDgfGmhkZAIDDw8WAh8XaGRkAgUPDxYCHxdoZGQCLw9kFgYCAQ8PFgQfAAUFUHJvcDkfGmhkZAIDDw8WAh8XaGRkAgUPDxYCHxdoZGQCMQ9kFgYCAQ8PFgQfAAUGUHJvcDEwHxpoZGQCAw8PFgIfF2hkZAIFDw8WAh8XaGRkAjUPDxYCHwAFAzE1NmRkAjcPEGQQFQEBMRUBATEUKwMBZxYBZmQCOQ8PFgIfAAUBMWRkAjsPDxYIHwAFC1tQcmV2IFBhZ2VdHw4KTh8aaB8QAgRkZAI9Dw8WCB8ABQtbTmV4dCBQYWdlXR8OCk4fEAIEHxpoZGQCPw8QZGQWAQIFZAJBDxYCHxdoFgICAQ8PFgIfAAW4BDxiciAvPg0KTWF0ZXJpYWxzIGZsYWdnZWQgYXMgZGlzY29udGludWVkICg8aW1nIHNyYz0iL2ltYWdlcy9idXR0b25zL2ljb25EaXNjb250aW51ZWQuanBnIiBhbHQ9IiIgLz4pIGFyZSBubyBsb25nZXIgcGFydCBvZiB0aGUgbWFudWZhY3R1cmVy4oCZcyBzdGFuZGFyZCBwcm9kdWN0IGxpbmUgYWNjb3JkaW5nIHRvIG91ciBsYXRlc3QgaW5mb3JtYXRpb24uICBUaGVzZSBtYXRlcmlhbHMgbWF5IGJlIGF2YWlsYWJsZSBieSBzcGVjaWFsIG9yZGVyLCBpbiBkaXN0cmlidXRpb24gaW52ZW50b3J5LCBvciByZWluc3RhdGVkIGFzIGFuIGFjdGl2ZSBwcm9kdWN0LiAgRGF0YSBzaGVldHMgZnJvbSBtYXRlcmlhbHMgdGhhdCBhcmUgbm8gbG9uZ2VyIGF2YWlsYWJsZSByZW1haW4gaW4gTWF0V2ViIHRvIGFzc2lzdCB1c2VycyBpbiBmaW5kaW5nIHJlcGxhY2VtZW50IG1hdGVyaWFscy4gIA0KPGJyIC8+PGJyIC8+DQpVc2VycyBvZiBvdXIgQWR2YW5jZWQgU2VhcmNoIChyZWdpc3RyYXRpb24gcmVxdWlyZWQpIG1heSBleGNsdWRlIGRpc2NvbnRpbnVlZCBtYXRlcmlhbHMgZnJvbSBzZWFyY2ggcmVzdWx0cy5kZAIWD2QWAmYPFgIfAAVwPGEgaHJlZj0iL2NsaWNrdGhyb3VnaC5hc3B4P2FkZGF0YWlkPTI3NyIgY2xhc3M9ImZvb3RsaW5rIj48c3BhbiBjbGFzcz0iZm9vdCI+VHJhZGUmbmJzcDtQdWJsaWNhdGlvbnM8L3NwYW4+PC9hPmQYAQUeX19Db250cm9sc1JlcXVpcmVQb3N0QmFja0tleV9fFgQFNmN0bDAwJENvbnRlbnRNYWluJFVjTWF0R3JvdXBGaW5kZXIxJHNlbGVjdENhdGVnb3J5TGlzdAUrY3RsMDAkQ29udGVudE1haW4kdWNNYXRHcm91cFRyZWUkbXNUcmVlVmlldwUbY3RsMDAkQ29udGVudE1haW4kYnRuU3VibWl0BRpjdGwwMCRDb250ZW50TWFpbiRidG5SZXNldLeulTpJp3BpKKoU6eQDwMxQbuh4" />
</div>

<script type="text/javascript">
//<![CDATA[
var theForm = document.forms['aspnetForm'];
if (!theForm) {
    theForm = document.aspnetForm;
}
function __doPostBack(eventTarget, eventArgument) {
    if (!theForm.onsubmit || (theForm.onsubmit() != false)) {
        theForm.__EVENTTARGET.value = eventTarget;
        theForm.__EVENTARGUMENT.value = eventArgument;
        theForm.submit();
    }
}
//]]>
</script>


<script src="/WebResource.axd?d=M68BSyLvwBMHRYzsSV62mvczr7w5VyKnq2JbgqcAAU1ioThlC6FNEVBncBdaA4oqf8cXPzy7QERzajvFfM67hSR6S0w1&amp;t=638313619312541215" type="text/javascript"></script>


<script src="/WebResource.axd?d=ClzPoKUn1iQnMGy7NeMJw70TvMayZ5w9nNmB7h_dY64i4BFFgjVcppyp69BFqhuzHctzc-rEantAjczzQ6UVMYkaWJw1&amp;t=638313619312541215" type="text/javascript"></script>
<script type="text/javascript">
//<![CDATA[

    function TreeView_PopulateNodeDoCallBack(context,param) {
        WebForm_DoCallback(context.data.treeViewID,param,TreeView_ProcessNodeData,context,TreeView_ProcessNodeData,false);
    }
var ctl00_ContentMain_ucMatGroupTree_msTreeView_Data = null;//]]>
</script>

<script src="/ScriptResource.axd?d=148LWaywfmset9PYVR0m-KYXX9JfyGYGqOtIYBGEAzoJh5VhFBwSI6JxnmoNvgtlAprhvntpc6lTDbRMgdKxckUmc8m2qw_fOCsX0G9LC8ojUpw4cTHWXgfuT_vU6m2Kpv3mt01iCczrTE2-J5X6EeBa2281&amp;t=637769794779625837" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=v_V1UgDrDAchg8Zuc1FL4qkvmjt-bwap7ZxwVGGPleYiJ6zfDOTL6wtCmVvILjbwo_sqb2PMUTNkjO4vp6nd-E7WWXTPgSI5-nvn3YDnHrVk5bgVX4Qv6F0fV4wTSslmaLZ3k98tckojDLWAEI7jm8h0H4d7BoGYxkqsyh8-Iujfrjjm0&amp;t=637769794779625837" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=V7Zma3AISNyGTbHVRPw8svJ4Ub7xUUaeoKwImcvnuDiy-XBR331l3eTjiEP8J_QlX2qgyAHxZFLywNbNyZ50fJLVEOCjx11lIDJOiQxc0oGA3qi7XXiggiwEcIWwzb9j2Is1n1D9sU40Zusk35AtkqRKL3Y1&amp;t=633177911400000000" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=wOguQo_yd3hCG4ajcSXPC6-XdeL0hmYWSMaFjvMXt1CBqR9neJqTXyTfPki4lIz4B29Zz_83btvS12BY6KpBKT-AgP0eaXp7V3h6XRdjiozV5w_n3oQRZg5Yq1QW8UNGj0Cy9jsap_7EcU2M99CapUyVG6I1&amp;t=633177911400000000" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=MAJxSy62Z3EtRwHXuuMc8KC6PFmrPqmkpcwdpcPGeqtqUQyJ3FBQ3cFyYWN1zNID_ngA53aMZdG_dpRu3TRDH8-iqKAjPAcc2BWe63WYGrGbyHc9syJxR6lypTgvRaYmwwEmkLEsaGUniMurHc16yr1VrjmzG3HWOeu9UXNr23Wv2xzh0&amp;t=633177911400000000" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=ukx9eyyt483y_CGgCF1OToQPWzPYyCynhpMvhxN7idzW3WaKjHjpgAOcMgtXZIEcspnbyDEADdRWCSHB-hF39Ycl13KWsqdlOHElw2qgSkGmkL8iNH37htb0BNS0lxw6oHvtqYiVpw-nZeYoQ1NkgX2qXOI1&amp;t=633177911400000000" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=Ovkr-MWDypjnq0C7FVbNgOs-Mt3NaktNVf8SEOX721PoQ5mQqTM6S4igSQL1_PXvh96km3stLiqrorOgm8nnnUU6y6edTkhxmLiwfFtEXQEAAnFFQpUepgsxEd0ahMvWcDfEac0EE7J8iDb2rqSMTxk51ydfY-c-8zcKvo5Mrn83cvfN0&amp;t=633177911400000000" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=pKQF4iQYwkSn5cAwTNDM8ez4TLepowWnai-WD6NVL5h0ay9Qtt0a9vPCaI-KN9OHcv1fL8VnwxEDM6EAVtgZY06V8XJTlVrbmb8WlMvGmhwBeL3_SVtKKOS-QExY8AnVbTFl_wOknplhy8bz-9sIWNCh4XPq1WhRj7ALNAKaeruPhS7d0&amp;t=633177911400000000" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=uM6eUuIuIa_NiCgVrtwgqczDeqTqI37XalY8Ri_W2ZjXEzIKi84afVBjFk8gneIDXcFnw8yjCD1qhE8mzDTzAUSdKnbDte0yC23kMoyvxPyu7vRH9E-6JMZtE8-gELUPmoZdOZJ9nXitHKYBLw2P6skU1IkzFRegK74bETu2FdEtuaeR0&amp;t=633177911400000000" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=ewF5FMb1ckbNCdJ9rpE5xMSXy-WecqVGE8uma1_520KusuW7jTUE2tnPMG7JyR0FDIFqDUMQbsfTZlsQ0sD4YAW8MnN90GKIMPvzaP7eOG3ss8E050_l7Rpnaz7I_U3ITzBXkcJZIOPcI1PxOReT8FSeWmo1&amp;t=633177911400000000" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=3lB_EBCMPyiwzDhzWdoE-i4mLHhkIMbbkaLwMr-U1WcAoDqBHS-oYFn4gz3ZwnpzK_KZp97Ev8ZbiUNo7WMJ45h2xgOH59erMFo1BpNb87-yu3X0mFyWs5rB3ANm4lsVSIu7RZd7XUEwfZXzugJ3CoJqxvRqiTGLpcXquEF24hK3OUH-0&amp;t=633177911400000000" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=Z7-pZSKJ6vCyr9H9S4r9baB7TLiJ-T4EoMOvlH-iQ5-icaO-vWMrjXk3EPctuPrcO5qwJSTaLQtHyguwle7Kty78exrSwHataIriNUEu1DnUkixv4pxU7Dknp_GDVHby5XcMA4lMwazAE0AFG9Cndu0An_gAGACGlYXsGUG9W6okCQsH0&amp;t=633177911400000000" type="text/javascript"></script>
<script src="../WebServices/Materials.asmx/js" type="text/javascript"></script>
<script src="../WebServices/MatGroups.asmx/js" type="text/javascript"></script>
<script src="../WebServices/Folders.asmx/js" type="text/javascript"></script>
<script type="text/javascript">
//<![CDATA[
function WebForm_OnSubmit() {
null;
return true;
}
//]]>
</script>

<div>

	<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="640EFF8C" />
</div>

<!--
The ScriptManager control is required for all ASP AJAX functionality - especially used by search pages.
It generates the javascript required.
Only one script manager is allowed on a page, and since this is the master page, this is the main script manager
However, a ScriptManagerProxy control can be used to set properties and settings on indivual pages.
For an example, see /WebControls/ucSearchResults.ascx
-->
<script type="text/javascript">
//<![CDATA[
Sys.WebForms.PageRequestManager._initialize('ctl00$ajxScriptManager', document.getElementById('aspnetForm'));
Sys.WebForms.PageRequestManager.getInstance()._updateControls([], [], [], 90);
//]]>
</script>


<table class="tabletight" style="width:100%; text-align:center;background-image: url(/images/gray.gif);">
  <tr >
    <td rowspan="2"><a href="/index.aspx"><img src="/images/logo1.gif" width="270" height="42" alt="MatWeb - Material Property Data"  /></a></td>

                
                <td style="text-align:right;vertical-align:middle; width:90%" ><span class='greeting'><span class='hiText'><a href ='/services/advertising.aspx'>Advertise with MatWeb!</a></span>&nbsp;&nbsp;&nbsp;&nbsp;</span></td>
                <td style="text-align:right;vertical-align:middle; padding-right:5px;" ><a href='/membership/regstart.aspx'><img src='/images/buttons/btnRegisterBSF.gif' width="62" height="20" alt="Register Now" /></a></td>

                
  </tr>
  <tr >
    <td  align="left" valign="bottom" colspan="2"><span class="tagline">
    
    Data sheets for over <span class="greeting">180,000</span>  metals, plastics, ceramics, and composites.</span></td>
  </tr>
  <tr style="background-image:url(/images/blue.gif); background-color:#000099; " >
    <td bgcolor="#000099"><a href="/index.aspx"><img src="/images/logo2.gif" width="270" height="23px" alt="MatWeb - Material Property Data" border="0" /></a></td>
    <td class="topnavig8" style=" background-color:#000099;vertical-align:middle; text-align:right; white-space:nowrap;" colspan="2" align="right" valign="middle">
        <a href="/index.aspx" class="topnavlink">HOME</a>&nbsp;&nbsp;&#8226;&nbsp;&nbsp;
                        <a href="/search/search.aspx" class="topnavlink">SEARCH</a>&nbsp;&nbsp;&#8226;&nbsp;&nbsp;
                        <a href="/tools/tools.aspx" class="topnavlink">TOOLS</a>&nbsp;&nbsp;&#8226;&nbsp;&nbsp;
                        <a href="/reference/suppliers.aspx" class="topnavlink">SUPPLIERS</a>&nbsp;&nbsp;&#8226;&nbsp;&nbsp;
                        <a href="/folders/ListFolders.aspx" class="topnavlink">FOLDERS</a>&nbsp;&nbsp;&#8226;&nbsp;&nbsp;
                        <a href="/services/services.aspx" class="topnavlink">ABOUT US</a>&nbsp;&nbsp;&#8226;&nbsp;&nbsp;
                        <a href="/reference/faq.aspx" class="topnavlink">FAQ</a>&nbsp;&nbsp;&#8226;&nbsp;&nbsp;
                        
                        <a href="/membership/login.aspx" class="topnavlink">LOG IN</a>
                        
                        &nbsp;
                        
                        &nbsp;
    </td>
  </tr>
</table>

<div id="divRecentMatls" class="tableborder tight" style=" display:none; position:absolute; top:95px; left:150px; width:400px; height:400px;" onmouseover="clearTimeout(tmrtmp);" onmouseout="tmrtmp=setTimeout('RecentMatls.Hide()', 800)">
        <table class="headerblock tabletight" style="vertical-align:top; font-size:13px; width:100%;"><tr>
                <td style="padding:3px;"> Recently Viewed Materials (most recent at top)</td>
                <td style="padding:3px;text-align:right;"><input type="image" src="/images/buttons/btnCloseWhite.gif" onclick="RecentMatls.Hide();return false;" style="cursor:pointer;" />&nbsp;</td>
        </tr></table>
        <div id="divRecentMatlsBody" style="height:375px; overflow:auto; background-color:White;">

                <table id="tblRecentMatls" class="tabledataformat tableloose" style="background-color:White;">
    
                <tr><td>
                <p />
                <a href="/membership/login.aspx">Login</a> to see your most recently viewed materials here.<p />
                Or if you don't have an account with us yet, then <a href="/membership/regstart.aspx">click here to register.</a>
                </td></tr>
                
                </table>

        </div>
</div>

<script type="text/javascript">

        var RecentMatls = new _RecentMatls();
        var tmrtmp;

        function _RecentMatls(){

                var divRecentMatls;
                var tblRecentMatls;
                var trRecentMatlsRowTemplate;
                var trRecentMatlsNoData;
                var DataIsLoaded=false;

                //Using prototype.js methods...
                divRecentMatls = $("divRecentMatls");
                tblRecentMatls = $("tblRecentMatls");
    

                divRecentMatls.style.display = "none";

                this.Toggle = function(){
                        //alert("Toggle()");
                        if(divRecentMatls.style.display == "block"){this.Hide();}
                        else{this.Show();}
                }

                this.Show = function(){
                        //alert("Show()");
                        divRecentMatls.style.display = "block";
                        matweb.toggleSelect(divRecentMatls,0);
                        if(!DataIsLoaded){
            
                        }
                }

                this.Hide = function(){
                        //alert("Hide()");
                        divRecentMatls.style.display = "none";
                        matweb.toggleSelect(divRecentMatls,1);
                }

    

        }//end class

</script>

<table class="tabletight t_ableborder t_ablegrid" style="width:100%;" >
  <tr>
   <td style="width:100%; height:27px; white-space:nowrap; text-align:left; vertical-align:middle;" class="tagline" >
      <span class="navlink"><b>&nbsp;&nbsp;Searches:</b></span>
      &nbsp;&nbsp;<a href="/search/AdvancedSearch.aspx" class="navlink"><span class="nav"><font color="#CC0000">Advanced</font></span></a>
      &nbsp;|&nbsp;<a href="/search/MaterialGroupSearch.aspx" class="navlink"><span class="nav">Category</span></a>
      &nbsp;|&nbsp;<a href="/search/PropertySearch.aspx" class="navlink"><span class="nav">Property</span></a>
      &nbsp;|&nbsp;<a href="/search/CompositionSearch.aspx" class="navlink"><span class="nav">Metals</span></a>
      &nbsp;|&nbsp;<a href="/search/SearchTradeName.aspx" class="navlink"><span class="nav">Trade Name</span></a>
      &nbsp;|&nbsp;<a href="/search/SearchManufacturerName.aspx" class="navlink"><span class="nav">Manufacturer</span></a>
                &nbsp;|&nbsp;<a href="javascript:RecentMatls.Toggle();" id="ctl00_lnkRecentMatls" class="navlink"><span class="nav"><font color="#CC0000">Recently Viewed Materials</font></span></a>
    </td>

    <td style="text-align:right; vertical-align:bottom; padding-right:5px;">

      <table class="tabletight t_ableborder" ><tr>
        <td style=" white-space:nowrap; vertical-align:bottom; " >
                                        &nbsp;&nbsp;
                                        <input name="ctl00$txtQuickText" type="text" id="ctl00_txtQuickText" style="width:100px;" maxlength="40" class="quicksearchinput" onkeydown="txtQuickText_OnKeyDown(event);" />
                                        <input type="image" id="btnQuickTextSearch" style="vertical-align:top; width:60px; height:25px;" src="/images/buttons/btnSearch.gif" alt="Search" onclick="return btnQuickTextSearch_Click()"  />
                                        <script type="text/javascript">

                                                //SETUP THE AUTOPOST ON ENTER KEY EVENT FOR txtQuickText FIELD
                                                //document.getElementById("ctl00_txtQuickText").onkeydown=txtQuickText_OnKeyDown;

                                                function txtQuickText_OnKeyDown(evt){
                                                        //SUBMIT THE FORM AS IF THE BUTTON WAS PUSHED
                                                        //var UniqueID = "";
                                                        var txtQuickText = document.getElementById('ctl00_txtQuickText');
                                                        if(matweb.IsEnterKey(evt))
                                                                if(checkSearchText(txtQuickText))
                                                                        //matweb.DoPostBack(UniqueID, "");
                                                                        window.location.href="/search/QuickText.aspx?SearchText=" + escape(txtQuickText.value);
                                                        return false;
                                                }

                                                function btnQuickTextSearch_Click(){
                                                        var txtQuickText = document.getElementById('ctl00_txtQuickText');
                                                        if(checkSearchText(txtQuickText))
                                                                window.location.href="/search/QuickText.aspx?SearchText=" + escape(txtQuickText.value);
                                                        return false;
                                                }

                                                function checkSearchText(searchbox){
                                                        searchbox.value = trim(searchbox.value);
                                                        if(searchbox.value == ''){
                                                                //alert('Please enter something to search for');
                                                                matweb.alert('Please enter something to search for', 'Search Error');
                                                                //searchbox.focus();
                                                                return false;
                                                        }
                                                        return true;
                                                }

                                                function trim(s)  { return ltrim(rtrim(s)) }
                                                function ltrim(s) { return s.replace(/^\s+/g, "") }
                                                function rtrim(s) { return s.replace(/\s+$/g, "") }

                                        </script>
                                </td>
                                </tr>
                        </table>

    </td>
  </tr>
  <tr style="background-image:url(/images/shad.gif);height:7px;" ><td colspan="2"></td></tr>
</table>

<!-- ====================================== MAIN CONTENT ============================================= -->
<div style="vertical-align:top; padding-left:10px; padding-right:10px;" >





	<center><a href="/clickthrough.aspx?addataid=1349"><img src="/images/assets/metalmen-Aluminum.png" border=0 alt="Metalmen Sales" width = "728" height = "90"></a>
</center>
	
	<h2><a href="#help"><img src="/images/buttons/iconHelp.gif" alt="" style="vertical-align:bottom;" /></a>Material Category Search</h2>
	
	

<div id="ctl00_ContentMain_ucPopupMessage1_divPopupMessage" class="divPopupAlert" style="display:none;width:250px;">
	<table >
		<tr id="ctl00_ContentMain_ucPopupMessage1_trTitleRow" class="divPopupAlert_TitleRow" style="cursor:move;">
	<th><span id="ctl00_ContentMain_ucPopupMessage1_lblTitle">User Input Problem</span></th>
</tr>

		<tr class="divPopupAlert_ContentRow" ><td style="" ><span id="ctl00_ContentMain_ucPopupMessage1_lblMessage"></span></td></tr>
		<tr class="divPopupAlert_ButtonRow"><td><input name="ctl00$ContentMain$ucPopupMessage1$btnOK" type="button" id="ctl00_ContentMain_ucPopupMessage1_btnOK" value="OK" /></td></tr>
	</table>
</div>

<input type="hidden" name="ctl00$ContentMain$ucPopupMessage1$hndPopupControl" id="ctl00_ContentMain_ucPopupMessage1_hndPopupControl" />


	<table class="" style="width:100%;">
	<tr>
		<td style=" vertical-align:top; width:300px;" >
			<strong>Find a Material Category:</strong>
			

<input name="ctl00$ContentMain$UcMatGroupFinder1$txtSearchText" type="text" value="alum" id="ctl00_ContentMain_UcMatGroupFinder1_txtSearchText" style="Width:304px;" /><br />
<select name="ctl00$ContentMain$UcMatGroupFinder1$selectCategoryList" id="ctl00_ContentMain_UcMatGroupFinder1_selectCategoryList" style="position:absolute;display:none;Width:308px;" size="18">
</select>

<input type="hidden" name="ctl00$ContentMain$UcMatGroupFinder1$TextBoxWatermarkExtender2_ClientState" id="ctl00_ContentMain_UcMatGroupFinder1_TextBoxWatermarkExtender2_ClientState" />

<script type="text/javascript">

var ucMatGroupFinderManager = new _ucMatGroupFinderManager("ctl00_ContentMain_UcMatGroupFinder1_txtSearchText","ctl00_ContentMain_UcMatGroupFinder1_selectCategoryList");

function _ucMatGroupFinderManager(txtSearchTextID,selectCategoryListID){

	var txtSearchText = document.getElementById(txtSearchTextID);
	var selectCategoryList = document.getElementById(selectCategoryListID);
	var showtime;
	var blurtime;
	var MinTextLength = 4;
	var KeyStrokeActionDelay = 650;
	var FadeInOutDelay = 0.2;
	var LastUsedText;
	
	//The init function is currently run at the bottom of this class
	this.init=function(){

		txtSearchText.onfocus = txtSearchText_onfocus;
		txtSearchText.onkeyup = txtSearchText_checktext;
		txtSearchText.onblur = selectCategoryList_startblur;
		
		selectCategoryList.onfocus = selectCategoryList_clearblur;
		selectCategoryList.onchange = selectCategoryList_onchange;
		selectCategoryList.onblur = selectCategoryList_startblur;

	}

	txtSearchText_onfocus = function(){
		if(txtSearchText.value == "Type at least 4 characters here...")
			txtSearchText.value = "";
		selectCategoryList_clearblur();
		txtSearchText_checktext();
	}

	function txtSearchText_checktext(){
		searchText = txtSearchText.value;
		//alert(searchText);
		clearTimeout(showtime);
		if(searchText.length >= MinTextLength){
			showtime = setTimeout(function(){
				selectCategoryList.style.display = "block";
				if(txtSearchText.value!=LastUsedText){
					LastUsedText = searchText;
					aci.matweb.WebServices.MatGroups.FindMatGroups(searchText,DisplayCatData,matweb.WebServiceErrorHandler,"txtSearchText_checktext");
				}
				//AjaxControlToolkit.Animation.FadeInAnimation.play(selectCategoryList, FadeInOutDelay, 1, 25, 0, 1, true);
			},KeyStrokeActionDelay);
		}
		else{
			LastUsedText = "";
			showtime = setTimeout(function(){
				selectCategoryList.options.length = 0;
				selectCategoryList.style.display = "none";
				//AjaxControlToolkit.Animation.FadeOutAnimation.play(selectCategoryList,FadeInOutDelay);
				//AjaxControlToolkit.Animation.FadeOutAnimation.play(selectCategoryList,FadeInOutDelay, 25, 0, 1, true);
			},KeyStrokeActionDelay);
		}
	}

	function selectCategoryList_clearblur(){
		clearTimeout(blurtime);
	}

	function selectCategoryList_onchange(){
		var MatGroupID = selectCategoryList.options[selectCategoryList.selectedIndex].value;
		var MatGroupText = selectCategoryList.options[selectCategoryList.selectedIndex].text;
		//alert("Selected MatGroupID: " + MatGroupID);
		//alert("Selected MatGroupText: " + MatGroupText);
		//When no data is found, we have a "No Categories Contain Text..." message, but the MatGroupID is set to 0, so we can ignore if MatGroupID == 0.
		if(MatGroupID > 0){
			ucMatGroupFinder1_OnSelected(MatGroupID,MatGroupText)
		}
		selectCategoryList_startblur();
		//txtSearchText.value = "";
	}
	
	function selectCategoryList_startblur(){
		//alert("selectCategoryList_onblur()");
		clearTimeout(blurtime);
		blurtime = setTimeout(function(){
			selectCategoryList.style.display = "none";
			//AjaxControlToolkit.Animation.FadeOutAnimation.play(selectCategoryList,FadeInOutDelay, 25, 0, 1, true);
		},KeyStrokeActionDelay);
	}
	
	function DisplayCatData(MatGroupData){
		//alert("GetCatData()");
		//alert("vwMaterialGroupList: " + vwMaterialGroupList);
		var MatGroupID,GroupName, MatCount;
		selectCategoryList.options.length = 0;//clear any existing options....
		if(MatGroupData.length==0){
				//alert(vwMaterialGroupList[i].GroupName);
				MatGroupID = 0;
				GroupName = "No categories contain the text: " + txtSearchText.value
				selectCategoryList.options[0] = new Option(GroupName,MatGroupID);					
		}
		else{
			for(i=0;i<MatGroupData.length;i++){
				//alert(vwMaterialGroupList[i].GroupName);
				MatCount = MatGroupData[i].MatCount;
				MatGroupID = MatGroupData[i].MatGroupID;
				GroupName = MatGroupData[i].GroupName + " (" + MatCount + " matls)";
				selectCategoryList.options[i] = new Option(GroupName,MatGroupID);					
			}
		}
	}
	
	this.init();

}//end class
	
</script>



			<strong>Select a Material Category:</strong>
			
			<div class="tableborder" style=" height:225px; width:300px; overflow:scroll; ">
			<a href="#ctl00_ContentMain_ucMatGroupTree_msTreeView_SkipLink"><img alt="Skip Navigation Links." src="/WebResource.axd?d=vAkieGj3ugBpu6CKp7dqvCP5iHLzPbUd2XME-h1vQcC6d5hLg7Vc1kNVzB8A45Ui3K9XZ9m3-E2pxsaJQYh2uMkAul41&amp;t=638313619312541215" width="0" height="0" style="border-width:0px;" /></a><div id="ctl00_ContentMain_ucMatGroupTree_msTreeView">
	<table cellpadding="0" cellspacing="0" style="border-width:0;">
		<tr>
			<td><a id="ctl00_ContentMain_ucMatGroupTree_msTreeViewn0" href="javascript:TreeView_PopulateNode(ctl00_ContentMain_ucMatGroupTree_msTreeView_Data,0,document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewn0'),document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewt0'),null,'r','Carbon (866 matls)','283','f','','f')"><img src="/WebResource.axd?d=6v5qM6VKQYD3XNwwXEy7Tix7dzVdqDJ8QTRIlUPrzXQqNsXQgUwIfmn34ZzRORd_IZj0bF4vkZzID1HuSFE5uKyLgaPYeAYE8VT8HYfb3Rk6zsDw0&amp;t=638313619312541215" alt="Expand Carbon (866 matls)" style="border-width:0;" /></a></td><td class="ctl00_ContentMain_ucMatGroupTree_msTreeView_2"><a class="ctl00_ContentMain_ucMatGroupTree_msTreeView_0 ctl00_ContentMain_ucMatGroupTree_msTreeView_1" href="javascript:ucMatGroupTree_OnNodeClick('Carbon (866 matls)','283')" id="ctl00_ContentMain_ucMatGroupTree_msTreeViewt0">Carbon (866 matls)</a></td>
		</tr><tr style="height:0px;">
			<td></td>
		</tr>
	</table><table cellpadding="0" cellspacing="0" style="border-width:0;">
		<tr style="height:0px;">
			<td></td>
		</tr><tr>
			<td><a id="ctl00_ContentMain_ucMatGroupTree_msTreeViewn1" href="javascript:TreeView_PopulateNode(ctl00_ContentMain_ucMatGroupTree_msTreeView_Data,1,document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewn1'),document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewt1'),null,'t','Ceramic (10004 matls)','11','f','','f')"><img src="/WebResource.axd?d=TinC6igX1vPDgWh7DstWwBTHTyH4uuybDU4NW47jJD06wHdAO3YyJC_eKK1Zn5oIbwqRo4Cri8YwhA82G-LD0p14yEF1EfoIZvKleYMIuQ2K1hkE0&amp;t=638313619312541215" alt="Expand Ceramic (10004 matls)" style="border-width:0;" /></a></td><td class="ctl00_ContentMain_ucMatGroupTree_msTreeView_2"><a class="ctl00_ContentMain_ucMatGroupTree_msTreeView_0 ctl00_ContentMain_ucMatGroupTree_msTreeView_1" href="javascript:ucMatGroupTree_OnNodeClick('Ceramic (10004 matls)','11')" id="ctl00_ContentMain_ucMatGroupTree_msTreeViewt1">Ceramic (10004 matls)</a></td>
		</tr><tr style="height:0px;">
			<td></td>
		</tr>
	</table><table cellpadding="0" cellspacing="0" style="border-width:0;">
		<tr style="height:0px;">
			<td></td>
		</tr><tr>
			<td><a id="ctl00_ContentMain_ucMatGroupTree_msTreeViewn2" href="javascript:TreeView_PopulateNode(ctl00_ContentMain_ucMatGroupTree_msTreeView_Data,2,document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewn2'),document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewt2'),null,'t','Fluid (7562 matls)','5','f','','f')"><img src="/WebResource.axd?d=TinC6igX1vPDgWh7DstWwBTHTyH4uuybDU4NW47jJD06wHdAO3YyJC_eKK1Zn5oIbwqRo4Cri8YwhA82G-LD0p14yEF1EfoIZvKleYMIuQ2K1hkE0&amp;t=638313619312541215" alt="Expand Fluid (7562 matls)" style="border-width:0;" /></a></td><td class="ctl00_ContentMain_ucMatGroupTree_msTreeView_2"><a class="ctl00_ContentMain_ucMatGroupTree_msTreeView_0 ctl00_ContentMain_ucMatGroupTree_msTreeView_1" href="javascript:ucMatGroupTree_OnNodeClick('Fluid (7562 matls)','5')" id="ctl00_ContentMain_ucMatGroupTree_msTreeViewt2">Fluid (7562 matls)</a></td>
		</tr><tr style="height:0px;">
			<td></td>
		</tr>
	</table><table cellpadding="0" cellspacing="0" style="border-width:0;">
		<tr style="height:0px;">
			<td></td>
		</tr><tr>
			<td><a id="ctl00_ContentMain_ucMatGroupTree_msTreeViewn3" href="javascript:TreeView_PopulateNode(ctl00_ContentMain_ucMatGroupTree_msTreeView_Data,3,document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewn3'),document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewt3'),null,'t','Metal (17052 matls)','9','f','','f')"><img src="/WebResource.axd?d=TinC6igX1vPDgWh7DstWwBTHTyH4uuybDU4NW47jJD06wHdAO3YyJC_eKK1Zn5oIbwqRo4Cri8YwhA82G-LD0p14yEF1EfoIZvKleYMIuQ2K1hkE0&amp;t=638313619312541215" alt="Expand Metal (17052 matls)" style="border-width:0;" /></a></td><td class="ctl00_ContentMain_ucMatGroupTree_msTreeView_2"><a class="ctl00_ContentMain_ucMatGroupTree_msTreeView_0 ctl00_ContentMain_ucMatGroupTree_msTreeView_1" href="javascript:ucMatGroupTree_OnNodeClick('Metal (17052 matls)','9')" id="ctl00_ContentMain_ucMatGroupTree_msTreeViewt3">Metal (17052 matls)</a></td>
		</tr><tr style="height:0px;">
			<td></td>
		</tr>
	</table><table cellpadding="0" cellspacing="0" style="border-width:0;">
		<tr style="height:0px;">
			<td></td>
		</tr><tr>
			<td><a id="ctl00_ContentMain_ucMatGroupTree_msTreeViewn4" href="javascript:TreeView_PopulateNode(ctl00_ContentMain_ucMatGroupTree_msTreeView_Data,4,document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewn4'),document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewt4'),null,'t','Other Engineering Material (8063 matls)','285','f','','f')"><img src="/WebResource.axd?d=TinC6igX1vPDgWh7DstWwBTHTyH4uuybDU4NW47jJD06wHdAO3YyJC_eKK1Zn5oIbwqRo4Cri8YwhA82G-LD0p14yEF1EfoIZvKleYMIuQ2K1hkE0&amp;t=638313619312541215" alt="Expand Other Engineering Material (8063 matls)" style="border-width:0;" /></a></td><td class="ctl00_ContentMain_ucMatGroupTree_msTreeView_2"><a class="ctl00_ContentMain_ucMatGroupTree_msTreeView_0 ctl00_ContentMain_ucMatGroupTree_msTreeView_1" href="javascript:ucMatGroupTree_OnNodeClick('Other Engineering Material (8063 matls)','285')" id="ctl00_ContentMain_ucMatGroupTree_msTreeViewt4">Other Engineering Material (8063 matls)</a></td>
		</tr><tr style="height:0px;">
			<td></td>
		</tr>
	</table><table cellpadding="0" cellspacing="0" style="border-width:0;">
		<tr style="height:0px;">
			<td></td>
		</tr><tr>
			<td><a id="ctl00_ContentMain_ucMatGroupTree_msTreeViewn5" href="javascript:TreeView_PopulateNode(ctl00_ContentMain_ucMatGroupTree_msTreeView_Data,5,document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewn5'),document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewt5'),null,'t','Polymer (97635 matls)','10','f','','f')"><img src="/WebResource.axd?d=TinC6igX1vPDgWh7DstWwBTHTyH4uuybDU4NW47jJD06wHdAO3YyJC_eKK1Zn5oIbwqRo4Cri8YwhA82G-LD0p14yEF1EfoIZvKleYMIuQ2K1hkE0&amp;t=638313619312541215" alt="Expand Polymer (97635 matls)" style="border-width:0;" /></a></td><td class="ctl00_ContentMain_ucMatGroupTree_msTreeView_2"><a class="ctl00_ContentMain_ucMatGroupTree_msTreeView_0 ctl00_ContentMain_ucMatGroupTree_msTreeView_1" href="javascript:ucMatGroupTree_OnNodeClick('Polymer (97635 matls)','10')" id="ctl00_ContentMain_ucMatGroupTree_msTreeViewt5">Polymer (97635 matls)</a></td>
		</tr><tr style="height:0px;">
			<td></td>
		</tr>
	</table><table cellpadding="0" cellspacing="0" style="border-width:0;">
		<tr style="height:0px;">
			<td></td>
		</tr><tr>
			<td><img src="/WebResource.axd?d=7_V5n1KYSQsj6PoSqgIxb0egBsAa91jSXPkbmM6FaiF14-JsNnpUDnfSdxZcvfd0OQCLDtC24pMIXigO-41cL2rsq-0xea-SSG7yZUyRcliFFqe70&amp;t=638313619312541215" alt="" /></td><td class="ctl00_ContentMain_ucMatGroupTree_msTreeView_2"><a class="ctl00_ContentMain_ucMatGroupTree_msTreeView_0 ctl00_ContentMain_ucMatGroupTree_msTreeView_1" href="javascript:ucMatGroupTree_OnNodeClick('Pure Element (507 matls)','184')" id="ctl00_ContentMain_ucMatGroupTree_msTreeViewt6">Pure Element (507 matls)</a></td>
		</tr><tr style="height:0px;">
			<td></td>
		</tr>
	</table><table cellpadding="0" cellspacing="0" style="border-width:0;">
		<tr style="height:0px;">
			<td></td>
		</tr><tr>
			<td><a id="ctl00_ContentMain_ucMatGroupTree_msTreeViewn7" href="javascript:TreeView_PopulateNode(ctl00_ContentMain_ucMatGroupTree_msTreeView_Data,7,document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewn7'),document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewt7'),null,'l','Wood and Natural Products (398 matls)','277','f','','t')"><img src="/WebResource.axd?d=vUBrM2nNhECNbFO0AJKq52p2zWrY5XG8nWgCDL4AfsrexStb0HREDV6GHMvdkAGDInxYtTyWXuxFUIiQv3JX2fScVRwQsrVv5srHrjOIeC3ayAG20&amp;t=638313619312541215" alt="Expand Wood and Natural Products (398 matls)" style="border-width:0;" /></a></td><td class="ctl00_ContentMain_ucMatGroupTree_msTreeView_2"><a class="ctl00_ContentMain_ucMatGroupTree_msTreeView_0 ctl00_ContentMain_ucMatGroupTree_msTreeView_1" href="javascript:ucMatGroupTree_OnNodeClick('Wood and Natural Products (398 matls)','277')" id="ctl00_ContentMain_ucMatGroupTree_msTreeViewt7">Wood and Natural Products (398 matls)</a></td>
		</tr><tr style="height:0px;">
			<td></td>
		</tr>
	</table>
</div><a id="ctl00_ContentMain_ucMatGroupTree_msTreeView_SkipLink"></a>
<span id="ctl00_ContentMain_ucMatGroupTree_lblDebug"></span>

			</div>
			
			<strong>Selected Material Category: </strong>
			<div id="ctl00_ContentMain_divMatGroupName" class="selectedItem" style="width:306px;">7000 Series Aluminum Alloy (152 matls)</div>
			<input name="ctl00$ContentMain$txtMatGroupID" type="hidden" id="ctl00_ContentMain_txtMatGroupID" value="207" />
			<input name="ctl00$ContentMain$txtMatGroupText" type="hidden" id="ctl00_ContentMain_txtMatGroupText" value="7000 Series Aluminum Alloy (152 matls)" />
			
			

			<input type="image" name="ctl00$ContentMain$btnSubmit" id="ctl00_ContentMain_btnSubmit" src="../images/buttons/btnFind.gif" alt="Find" style="border-width:0px;" /> <!-- OnClientClick="return validate()" -->
			<input type="image" name="ctl00$ContentMain$btnReset" id="ctl00_ContentMain_btnReset" title="Clear Search" src="/images/buttons/btnReset.gif" alt="Clear Search" style="border-width:0px;" />

			<span id="ctl00_ContentMain_lblDebug"></span>

		</td>
		<td>&nbsp;</td>
		<td style=" vertical-align:top; width:80%;" >
			<strong>Search Results</strong>
			<div class="tableborder" id="divSearchResults" style="padding:5px;" >
				
				<div id="ctl00_ContentMain_pnlSearchResults">
	
					There are <span id="ctl00_ContentMain_lblMatlCount">156</span> materials in the category 
					<span id="ctl00_ContentMain_lblMaterialGroupName" class="selectedItem">7000 Series Aluminum Alloy</span>.
					If your material is not listed, please refer to our <a href="/help/strategy.aspx">search strategy</a> page for assistance in limiting your search.
				
</div>
			</div>
			<p />




<div id="ctl00_ContentMain_UcSearchResults1_pnlResultsFound" class="tableborder">
	

	<table style="background-color:Silver; width:100%;" class="" >
		<tr>
			<td><strong>Found <span id="ctl00_ContentMain_UcSearchResults1_lblResultCount">156</span> Results</strong> -- 
				Page 
				<select name="ctl00$ContentMain$UcSearchResults1$drpPageSelect1" onchange="javascript:setTimeout('__doPostBack(\'ctl00$ContentMain$UcSearchResults1$drpPageSelect1\',\'\')', 0)" id="ctl00_ContentMain_UcSearchResults1_drpPageSelect1">
		<option selected="selected" value="1">1</option>

	</select>
				of <span id="ctl00_ContentMain_UcSearchResults1_lblPageTotal">1</span> 
				-- 
				<a id="ctl00_ContentMain_UcSearchResults1_lnkPrevPage" disabled="disabled" style="color:Gray;">[Prev Page]</a>
				<a id="ctl00_ContentMain_UcSearchResults1_lnkNextPage" disabled="disabled" style="color:Gray;">[Next Page]</a>
				--&nbsp;			
				view
				<select name="ctl00$ContentMain$UcSearchResults1$drpPageSize1" onchange="javascript:setTimeout('__doPostBack(\'ctl00$ContentMain$UcSearchResults1$drpPageSize1\',\'\')', 0)" id="ctl00_ContentMain_UcSearchResults1_drpPageSize1">
		<option value="25">25</option>
		<option value="50">50</option>
		<option value="75">75</option>
		<option value="100">100</option>
		<option value="150">150</option>
		<option selected="selected" value="200">200</option>

	</select> per page
			</td>
		</tr>
		
	</table>
	
	
	
	
	<table class="t_ablegrid" style="width:100%;">
		<tr style="font-size:9px;">
			<td style="font-size:9px;">Use Folder</td>
			<td style="font-size:9px;">Contains</td>
			<td style="font-size:9px;">&nbsp;</td>
			
			<td style="font-size:9px;"><div id="divFolderName" style="display:none;">New Folder Name</div></td>
			<td style="font-size:9px;"></td>
			<td style="width:90%"></td>
		</tr>
		<tr style="vertical-align:top;">
			<td style="vertical-align:top;">
				<select name="ctl00$ContentMain$UcSearchResults1$drpFolderList" id="ctl00_ContentMain_UcSearchResults1_drpFolderList" onchange="SetSelectedFolder()" style="width:125px;">
		<option selected="selected" value="0">My Folder</option>

	</select>
			</td>
			<td style="vertical-align:top;">
				<strong><input name="ctl00$ContentMain$UcSearchResults1$txtFolderMatCount" type="text" id="ctl00_ContentMain_UcSearchResults1_txtFolderMatCount" readonly="readonly" style="border:none 0 white; background-color:White; color:Black; width: 35px; font-weight:bold;" value="0/0" /></strong>
			</td>
			<td>
				
				<input type="image" id="btnCompareFolders" src="/images/buttons/btnCompareMatls.gif" alt="Compare Checked Materials" onclick="CompareMaterials();return false;" />
			</td>
			
			<td style="white-space:nowrap;vertical-align:top;">
			
				<table id="tblFolderName" class="tabletight" style="display:none;"><tr><td><input type="text" id="txtFolderName2" style="display:inline; width:100px;" /><br /></td>
				<td><input type="image" id="btnApplyFolderName2" src="/images/buttons/btnSaveChanges.gif" style="display:inline;" onclick="ApplyFolderName();return false;" />
				<input type="image" id="btnHideFolderNameControls2" src="/images/buttons/btnCancel.gif" style="display:inline;" onclick="HideFolderNameControls();return false;" /></td>
				</tr></table>
				
			</td>
			<td style="white-space:nowrap;">
				<div class="usermessage" id="divUserMessage"></div>
			</td>
		</tr>
	</table>

	<table class="tabledataformat t_ablegrid" id="tblResults" style="table-layout:auto;"  >

		<tr class="">

			
		
			<th style="width:65px; white-space:nowrap;">Select</th>

			<th style="width:10px;">
				<!--Discontinued and Found Via Interpolated Indicators -->
			</th>
			
			<th style="width:auto;">
				<a id="ctl00_ContentMain_UcSearchResults1_lnkSortMatName" disabled="disabled" title="Click to Sort By Material Name">Material Name</a>
				<br />
				
				
			</th>
		
			
			
			

			

			

			

			

			

			

			

			
		</tr>

		<!-- this is where the search results are actually loaded.  See code behind -->		
		<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_81688"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;1</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_81688' href="/search/DataSheet.aspx?MatGUID=ab9706916818406b80c22b7f39db0c78" >Overview of materials for 7000 Series Aluminum Alloy</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9520"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;2</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9520' href="/search/DataSheet.aspx?MatGUID=1242b1b6e433453c9e263baff562aac7" >Aluminum 7001-O</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9521"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;3</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9521' href="/search/DataSheet.aspx?MatGUID=cee3abca3386446fb9eee07f5140604b" >Aluminum 7001-T6; 7001-T651</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9522"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;4</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9522' href="/search/DataSheet.aspx?MatGUID=a109eb23a2414d80a5bf23bb18b33742" >Aluminum 7001-T75</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9525"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;5</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9525' href="/search/DataSheet.aspx?MatGUID=d41667bc11964819929805540b29bc58" >7003 Aluminum Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9526"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;6</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9526' href="/search/DataSheet.aspx?MatGUID=3a29279aa5184592ab69f7abe5f25269" >7004 Aluminum Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9528"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;7</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9528' href="/search/DataSheet.aspx?MatGUID=418587e416114026aae056b2695c5bde" >Aluminum 7005-O</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9529"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;8</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9529' href="/search/DataSheet.aspx?MatGUID=0d7139f8ea0243cfbdbc53b68a75b906" >Aluminum 7005-T53</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9530"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;9</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9530' href="/search/DataSheet.aspx?MatGUID=34c308934f7a4be589a80ecbee94406e" >Aluminum 7005-T6, 7005-T63, and 7005-T6351</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9531"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;10</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9531' href="/search/DataSheet.aspx?MatGUID=3a8cecb869344078aa4e37ec1e9d9d44" >Aluminum 7005-W</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9532"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;11</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9532' href="/search/DataSheet.aspx?MatGUID=42fbfd1a4bf3439bba4c25e3b270fefd" >7008 Aluminum Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9533"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;12</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9533' href="/search/DataSheet.aspx?MatGUID=a0391b00251a4246b50794d03d07620d" >7009 Aluminum Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9535"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;13</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9535' href="/search/DataSheet.aspx?MatGUID=bed879fd995c4c8e98f8d24d644b5db4" >7010 Aluminum Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9536"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;14</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9536' href="/search/DataSheet.aspx?MatGUID=9921d4f9bde04b3e8fa0f2f9cf7053ff" >7012 Aluminum Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9537"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;15</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9537' href="/search/DataSheet.aspx?MatGUID=f7a63d76ca0749a4921ce7318e21fde3" >7014 Aluminum Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9538"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;16</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9538' href="/search/DataSheet.aspx?MatGUID=c567d381737641eeb13a9444708c0700" >7015 Aluminum Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9540"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;17</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9540' href="/search/DataSheet.aspx?MatGUID=b9971b73dd5e4587aabaf06a9e8194c2" >Aluminum 7016-T5</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9542"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;18</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9542' href="/search/DataSheet.aspx?MatGUID=75b699e7a2764b5aa153a38c059bf4ad" >7017 Aluminum Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9543"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;19</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9543' href="/search/DataSheet.aspx?MatGUID=c5aa43d992cb48fc8044617ae3108fa0" >7018 Aluminum Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9544"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;20</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9544' href="/search/DataSheet.aspx?MatGUID=1a1d7879455d4a749147cb69cbfa3ea3" >7019 Aluminum Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9545"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;21</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9545' href="/search/DataSheet.aspx?MatGUID=5babff9b0e3b445e8a82d8c166fb14fb" >Aluminum 7019A Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9547"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;22</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9547' href="/search/DataSheet.aspx?MatGUID=c66e13c1d36445c29bb9f852ccf2da17" >7020-T6 Aluminum</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_130348"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;23</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_130348' href="/search/DataSheet.aspx?MatGUID=379f1db808944eb5a73b65bf3fdbb501" >7020-T652 Aluminum</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9548"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;24</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9548' href="/search/DataSheet.aspx?MatGUID=9cbd28d89cf04ae285b7cd53501828c4" >Aluminum 7021-T62</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9550"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;25</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9550' href="/search/DataSheet.aspx?MatGUID=45cc3d2c44544b45ac61cc3c01ab28e4" >7022 Aluminum Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9552"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;26</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9552' href="/search/DataSheet.aspx?MatGUID=ec24bc3de76749f4a3fc1f75c199af0c" >7023 Aluminum Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9554"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;27</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9554' href="/search/DataSheet.aspx?MatGUID=ba1f5dcc559b4a3fb06d7b9c8b9b114f" >7024 Aluminum Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9555"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;28</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9555' href="/search/DataSheet.aspx?MatGUID=8496d2d315da412bb7aef90de5b9310c" >7025 Aluminum Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9556"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;29</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9556' href="/search/DataSheet.aspx?MatGUID=7fae2c6272444dacb8d5252c77732ec3" >7026 Aluminum Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9557"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;30</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9557' href="/search/DataSheet.aspx?MatGUID=377b80b48304465e9c36faffb11ae3c3" >7028 Aluminum Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9558"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;31</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9558' href="/search/DataSheet.aspx?MatGUID=de9018905a6d4a998188e8ce1ad4485a" >Aluminum 7029-T5</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9559"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;32</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9559' href="/search/DataSheet.aspx?MatGUID=47e6b2593b074b2fafae9ef98810aa7d" >7030 Aluminum Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9560"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;33</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9560' href="/search/DataSheet.aspx?MatGUID=75c0a33007de43bead45d21bf13734b2" >7031 Aluminum Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9561"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;34</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9561' href="/search/DataSheet.aspx?MatGUID=50f917083b2a4bf89fec36ac48e6cf1b" >7032 Aluminum Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9562"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;35</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9562' href="/search/DataSheet.aspx?MatGUID=21fb5d81fb66459b8721d8099f2e2cc0" >7033 Aluminum Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9563"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;36</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9563' href="/search/DataSheet.aspx?MatGUID=844471e12b9e4d389b5983f26876b835" >Aluminum 7034-T6</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_160216"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;37</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_160216' href="/search/DataSheet.aspx?MatGUID=efc64d5b32ac49d2ba76e17182fad4df" >Aluminum 7035 Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_160217"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;38</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_160217' href="/search/DataSheet.aspx?MatGUID=f9236eff53cb4fbfa80945630950359a" >Aluminum 7035A Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_160218"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;39</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_160218' href="/search/DataSheet.aspx?MatGUID=058f792aed0649f89c73f12f85dc1b57" >Aluminum 7036 Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_160220"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;40</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_160220' href="/search/DataSheet.aspx?MatGUID=f3eba75b0c674641bba14ee30a1a94d3" >Aluminum 7037 Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9564"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;41</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9564' href="/search/DataSheet.aspx?MatGUID=5ccdef84a8fd457d88cf8f1b70edeb92" >Aluminum 7039-O</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9565"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;42</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9565' href="/search/DataSheet.aspx?MatGUID=67426fbb5e0d4ee6be7c0fb99cc4c756" >Aluminum 7039-T61</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9566"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;43</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9566' href="/search/DataSheet.aspx?MatGUID=e4e262e692284ac994651fe1e268322c" >Aluminum 7039-T64</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9567"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;44</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9567' href="/search/DataSheet.aspx?MatGUID=5f53ad37fd87494996a07d492d1b51fb" >7040 Aluminum Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_160222"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;45</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_160222' href="/search/DataSheet.aspx?MatGUID=8ec417a825bb4c5cb555422e2d0b78fc" >Aluminum 7041 Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_160258"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;46</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_160258' href="/search/DataSheet.aspx?MatGUID=0476f18a9ad54e3aade30780e7acd125" >Aluminum 7042 Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9568"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;47</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9568' href="/search/DataSheet.aspx?MatGUID=932439e4822d4d5f9969f5f032b14d8b" >7046 Aluminum Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9569"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;48</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9569' href="/search/DataSheet.aspx?MatGUID=6a9425963fa147e28f7c2d843f5a63b3" >Aluminum 7046A Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_160249"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;49</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_160249' href="/search/DataSheet.aspx?MatGUID=42e3ac5e90624c44876a9f3d01ff8ed7" >Aluminum 7047 Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9570"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;50</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9570' href="/search/DataSheet.aspx?MatGUID=a52eecdd412b446685702d5de49f1ed9" >Aluminum 7049A Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9571"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;51</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9571' href="/search/DataSheet.aspx?MatGUID=897ef3c3a1f842ef9e4a20f88103a727" >Aluminum 7049-T352</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9572"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;52</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9572' href="/search/DataSheet.aspx?MatGUID=516378206bb94306a4d186087f5bbc0b" >Aluminum 7049-T73; 7049-T7352</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9574"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;53</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9574' href="/search/DataSheet.aspx?MatGUID=2567090450aa4b668c3e1d4a718cf664" >Aluminum 7050A Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9575"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;54</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9575' href="/search/DataSheet.aspx?MatGUID=5b0b9a74f1f44599b248d84361328214" >Aluminum 7050-T73511; 7050-T73510</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_156322"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;55</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_156322' href="/search/DataSheet.aspx?MatGUID=c242c65b8b60449786b08ec45e9ae757" >Aluminum 7050-T73652 forgings</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_156323"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;56</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_156323' href="/search/DataSheet.aspx?MatGUID=21bd5ee1587b44d29e9574f73895faf9" >Aluminum 7050-T73651 plate</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9576"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;57</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9576' href="/search/DataSheet.aspx?MatGUID=1dc19fb0f19341a298c24528cf73ead8" >Aluminum 7050-T74</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9577"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;58</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9577' href="/search/DataSheet.aspx?MatGUID=142262cf7fbc4c83917ca5c3d17df1ed" >Aluminum 7050-T7451 (7050-T73651)</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9578"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;59</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9578' href="/search/DataSheet.aspx?MatGUID=a8298bba8d02486c97c06990a3c215d9" >Aluminum 7050-T7651</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9581"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;60</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9581' href="/search/DataSheet.aspx?MatGUID=de18146a946e46cc9fa90369ee0923e4" >Aluminum 7055-T7751 Plate</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_176074"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;61</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_176074' href="/search/DataSheet.aspx?MatGUID=adcb6ede1656447281625939e9ad149f" >Aluminum 7055-T77511 Extrusions</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_160225"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;62</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_160225' href="/search/DataSheet.aspx?MatGUID=5ef9834ed0fc42bb8df77014af289240" >Aluminum 7056 Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9583"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;63</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9583' href="/search/DataSheet.aspx?MatGUID=733715d5285d42f49e0b512008d1271d" >7060 Aluminum Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9584"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;64</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9584' href="/search/DataSheet.aspx?MatGUID=8a51d5b546174dec823f23630c8f18c1" >7064 Aluminum Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_160238"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;65</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_160238' href="/search/DataSheet.aspx?MatGUID=2fd6a9e2a71f44639819721c0abc2825" >Aluminum 7065 Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_122001"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;66</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_122001' href="/search/DataSheet.aspx?MatGUID=c31454fe42574158b23efcc6abb0a6b9" >Aluminum 7068-T6; 7068-T651 Rod & Bar</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_122002"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;67</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_122002' href="/search/DataSheet.aspx?MatGUID=1ee9c9628f9d455b97fb4606daa84bd9" >Aluminum 7068-T6; 7068-T6511 Rod & Bar</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_122003"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;68</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_122003' href="/search/DataSheet.aspx?MatGUID=c4cfe2141d1d493c98a7c036f45a7461" >Aluminum 7068-T76; 7068-T76511 Rod & Bar</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9588"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;69</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9588' href="/search/DataSheet.aspx?MatGUID=f755b2226d7f40a2ac2d33b5cb1e5dd1" >Aluminum 7072-H113</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9589"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;70</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9589' href="/search/DataSheet.aspx?MatGUID=4a4568af1a16449da3bae92dc57fcfd0" >Aluminum 7072-H12</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9590"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;71</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9590' href="/search/DataSheet.aspx?MatGUID=8bf3e1d54b764c82b4225e392c6d0a72" >Aluminum 7072-H14</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9591"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;72</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9591' href="/search/DataSheet.aspx?MatGUID=dbe327a39d7347bd917d9f10ca577e1d" >Aluminum 7072-O</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9592"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;73</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9592' href="/search/DataSheet.aspx?MatGUID=7d7d6e4e0bf74145800cae70b5e96314" >Alclad Aluminum 7075-O</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9593"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;74</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9593' href="/search/DataSheet.aspx?MatGUID=9852e9cdc3d4466ea9f111f3f0025c7d" >Alclad Aluminum 7075-T6, T651</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9594"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;75</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9594' href="/search/DataSheet.aspx?MatGUID=da98aea5e9de44138a7d28782f60a836" >Aluminum 7075-O</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9595"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;76</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9595' href="/search/DataSheet.aspx?MatGUID=4f19a42be94546b686bbf43f79c51b7d" >Aluminum 7075-T6; 7075-T651</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9596"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;77</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9596' href="/search/DataSheet.aspx?MatGUID=6653b72914864cc0a0ff7adf5b720167" >Aluminum 7075-T73; 7075-T735x</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_109186"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;78</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_109186' href="/search/DataSheet.aspx?MatGUID=e1adabad6c1b4fed99133c4b135303a8" >Aluminum 7075-T76; 7075-T7651</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9598"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;79</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9598' href="/search/DataSheet.aspx?MatGUID=8916a21e7d604d3499f7637fde053930" >Aluminum 7076-T61</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_160227"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;80</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_160227' href="/search/DataSheet.aspx?MatGUID=55798e93a5804a7da63bc0e65852458f" >Aluminum 7081 Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_160228"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;81</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_160228' href="/search/DataSheet.aspx?MatGUID=a3890996b4af4ea29687b0dd39ecfbdd" >Aluminum 7085 Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9600"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;82</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9600' href="/search/DataSheet.aspx?MatGUID=2a910577a63549439f076255350f03d4" >7090 Aluminum Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9601"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;83</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9601' href="/search/DataSheet.aspx?MatGUID=104899d400b84416b672899b9d820b8e" >7093 Aluminum Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_160229"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;84</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_160229' href="/search/DataSheet.aspx?MatGUID=ab42259d7a6d4fc3a1b8cca209b34458" >Aluminum 7095 Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_160241"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;85</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_160241' href="/search/DataSheet.aspx?MatGUID=f09c7739e44c4dc69493040a1ce4fdcf" >Aluminum 7099 Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9603"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;86</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9603' href="/search/DataSheet.aspx?MatGUID=7affc819e7714c6fa109c09b3fc68100" >7108 Aluminum Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9604"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;87</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9604' href="/search/DataSheet.aspx?MatGUID=c54753eb162a4606a442e5b0fb6ea738" >Aluminum 7108A Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9605"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;88</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9605' href="/search/DataSheet.aspx?MatGUID=2af5ada51f114d9e9d627346391bc301" >7116 Aluminum Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9606"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;89</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9606' href="/search/DataSheet.aspx?MatGUID=7f96ed4fe9424771a5008d8211cb4630" >7122 Aluminum Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9607"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;90</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9607' href="/search/DataSheet.aspx?MatGUID=d62fafde9ffe4ef68614c5e2ab5869e5" >7129 Aluminum Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_160219"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;91</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_160219' href="/search/DataSheet.aspx?MatGUID=2a45b764bdb74b3b9f9e841be7c876bd" >Aluminum 7136 Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_160221"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;92</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_160221' href="/search/DataSheet.aspx?MatGUID=8f816f23c26e489b92bd3db112751c24" >Aluminum 7140 Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9608"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;93</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9608' href="/search/DataSheet.aspx?MatGUID=7e4fa2a0c2b54eccad3370fedceb3248" >7149 Aluminum Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9609"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;94</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9609' href="/search/DataSheet.aspx?MatGUID=3fec163aba814c979b30103abbb72b99" >Aluminum 7150-T7751 Plate</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_176076"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;95</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_176076' href="/search/DataSheet.aspx?MatGUID=dc93c2f2da44422b8e5ca259731c45bc" >Aluminum 7150-T77511 Extrusions</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_160224"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;96</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_160224' href="/search/DataSheet.aspx?MatGUID=173f047427fe4b25a632f8ca59e7f35c" >Aluminum 7155 Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_160226"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;97</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_160226' href="/search/DataSheet.aspx?MatGUID=aad98a67516e4d149e89e019b5f0cb5f" >Aluminum 7168 Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9613"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;98</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9613' href="/search/DataSheet.aspx?MatGUID=6985d7b839b64b3a87f632bc5450f51b" >Aluminum 7175-T66</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9614"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;99</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9614' href="/search/DataSheet.aspx?MatGUID=13ca3d21315b4632b4330965f0e01c31" >Aluminum 7175-T7351</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9615"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;100</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9615' href="/search/DataSheet.aspx?MatGUID=f845f4cbd7d04dafb1a43eca72378be2" >Aluminum 7175-T736; 7175-T7365x</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_170634"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;101</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_170634' href="/search/DataSheet.aspx?MatGUID=171ecd391a264bf5bb63dd6b4234835c" >Aluminum 7175-T736; 7175-T7365x (extended High Temperature Data)</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9616"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;102</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9616' href="/search/DataSheet.aspx?MatGUID=b021540967e6482e96d408c2103329dc" >Aluminum 7175-T74</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9617"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;103</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9617' href="/search/DataSheet.aspx?MatGUID=9a169e5a5a384428b6157e381470ba2d" >Alclad Aluminum 7178-O</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9618"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;104</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9618' href="/search/DataSheet.aspx?MatGUID=89c7df2d0d82406c9a173c5ea25f62cf" >Alclad Aluminum 7178-T6, T651</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9619"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;105</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9619' href="/search/DataSheet.aspx?MatGUID=5e778650fba346b59b51e908341e6383" >Aluminum 7178-O</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9620"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;106</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9620' href="/search/DataSheet.aspx?MatGUID=606e61c4801648c59d6d3215fa6824a5" >Aluminum 7178-T6; 7178-T651</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_170636"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;107</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_170636' href="/search/DataSheet.aspx?MatGUID=2e15ce33ee204be1a26982bd6e77ade3" >Aluminum 7178-T6; 7178-T651 (Included Creep/Rupture Data)</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9621"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;108</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9621' href="/search/DataSheet.aspx?MatGUID=19a50d7eaf564054bddd5cfcbaf1eb6e" >Aluminum 7178-T76; 7178-T7651</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_160257"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;109</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_160257' href="/search/DataSheet.aspx?MatGUID=30cab0d8eebe4a7b8f2da9cc15df6bd4" >Aluminum 7181 Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_160250"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;110</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_160250' href="/search/DataSheet.aspx?MatGUID=64329cfc157140cca6234e6d5fc64269" >Aluminum 7185 Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_160215"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;111</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_160215' href="/search/DataSheet.aspx?MatGUID=4365242399b44833b9e8ac53c6376ce1" >Aluminum 7204 Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9626"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;112</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9626' href="/search/DataSheet.aspx?MatGUID=7e0fd21437904bf4afedb66e2f223190" >7229 Aluminum Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9627"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;113</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9627' href="/search/DataSheet.aspx?MatGUID=4a884a58c3bb4814b2ed1d25fda9b7d0" >7249 Aluminum Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_160223"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;114</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_160223' href="/search/DataSheet.aspx?MatGUID=445f58c4761d4b27a7275eb131da8634" >Aluminum 7250 Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_160256"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;115</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_160256' href="/search/DataSheet.aspx?MatGUID=bba08145b51d490e9b5c159708ed4e2a" >Aluminum 7255 Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9628"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;116</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9628' href="/search/DataSheet.aspx?MatGUID=46fa9531d4c4477a931f76869032f6c7" >7277 Aluminum Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9629"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;117</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9629' href="/search/DataSheet.aspx?MatGUID=701b94bc489d4b7caac8b55a73791992" >7278 Aluminum Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9630"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;118</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9630' href="/search/DataSheet.aspx?MatGUID=752ef90b35d4414e92b3f7c61539d173" >Aluminum 7278A Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9632"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;119</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9632' href="/search/DataSheet.aspx?MatGUID=0d55769c6f1740f8a5124451dd4c436f" >7349 Aluminum Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9636"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;120</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9636' href="/search/DataSheet.aspx?MatGUID=3c1a95bbb1c748c283a29486d22bb975" >7449 Aluminum Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9645"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;121</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9645' href="/search/DataSheet.aspx?MatGUID=63eb4fb672fc4036a3112bd639a163d7" >Alclad Aluminum 7475-T61</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9646"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;122</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9646' href="/search/DataSheet.aspx?MatGUID=aa2fab25253d49749d28e4163fac5401" >Alclad Aluminum 7475-T761</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9647"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;123</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9647' href="/search/DataSheet.aspx?MatGUID=9301e602923c4bca973c5856a82249ca" >Aluminum 7475-T61</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_170631"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;124</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_170631' href="/search/DataSheet.aspx?MatGUID=289bf484b5b14d78bf144eb4487ac552" >Aluminum 7475-T61 (Extended High Temperature Data)</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9648"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;125</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9648' href="/search/DataSheet.aspx?MatGUID=6810de82af5b4b0b9803fd7c75361c19" >Aluminum 7475-T651</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9649"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;126</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9649' href="/search/DataSheet.aspx?MatGUID=92620a0b7b9a44fcb34799eda01d2b18" >Aluminum 7475-T7351</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9650"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;127</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9650' href="/search/DataSheet.aspx?MatGUID=ed81af9cda46489faca9ee659ed533ba" >Aluminum 7475-T76</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9651"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;128</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9651' href="/search/DataSheet.aspx?MatGUID=4ca4377e3600476eb81e78e1289affd2" >Aluminum 7475-T761</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_170632"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;129</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_170632' href="/search/DataSheet.aspx?MatGUID=c33d2fe863fb4318af54315c5d02b50c" >Aluminum 7475-T761 (Extended High Temperature Data)</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9652"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;130</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9652' href="/search/DataSheet.aspx?MatGUID=49085217327c4bc1b52c27527e1698e5" >Aluminum 7475-T7651</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_14432"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;131</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_14432' href="/search/DataSheet.aspx?MatGUID=a8692eef20f348d8b1b75586d0171724" >Composition Spec for Aluminum 7129H</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_14434"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;132</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_14434' href="/search/DataSheet.aspx?MatGUID=a67930a556e94eab843a544f78226267" >Composition Spec for Aluminum 7146H</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_176072"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;133</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_176072' href="/search/DataSheet.aspx?MatGUID=029e3f401e484e9cacb160e24eebb907" >Alcoa 7055-T77511 Aluminum Extrusions</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_176073"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;134</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_176073' href="/search/DataSheet.aspx?MatGUID=8755e9a9df8e4ebf8148593d1d3c4545" >Alcoa 7055-T7751 Aluminum Plate</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_14758"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;135</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_14758' href="/search/DataSheet.aspx?MatGUID=f5d582dd5f0247068bbc74d3218d6995" >ALIMEX ACP 6000 Aluminum Cast Plate</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_14761"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;136</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_14761' href="/search/DataSheet.aspx?MatGUID=83c6b0129acc44649379c9b6433bf114" >ALIMEX PLANAL 7075 T651/T652 Aluminum Alloy</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_14762"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;137</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_14762' href="/search/DataSheet.aspx?MatGUID=2b752c0339034c4e9d1aeb8855c195e9" >ALIMEX AMP 8000 Aluminum Plate</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_14766"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;138</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_14766' href="/search/DataSheet.aspx?MatGUID=89976ff1a73a4e72871b623ea4418f49" >ALIMEX 7075 T651/T652 Aluminum Alloy Rolled</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_160273"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;139</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_160273' href="/search/DataSheet.aspx?MatGUID=e27862b6dd924ed8b916ac955775f35c" >Constellium ALPLAN® 7075 High-Strength Rolled Precision Aluminum Plate, Milled Both Sides</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_160279"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;140</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_160279' href="/search/DataSheet.aspx?MatGUID=3cf9d67aba064b0390df12460578b0df" >Constellium Certal® Thick Aluminum Plate</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_160280"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;141</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_160280' href="/search/DataSheet.aspx?MatGUID=584c2793893c49758444f9ae5baf044b" >Constellium Certal® SPC Aluminum</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_160281"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;142</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_160281' href="/search/DataSheet.aspx?MatGUID=c22dae400b804c6487b8e5b9dfbf4e29" >Constellium Certal® SPC Forged Aluminum</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_160288"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;143</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_160288' href="/search/DataSheet.aspx?MatGUID=099646bddf6545b7be1d9700589f60cb" >Constellium Unidal® High-Strength Rolled Precision Aluminum Plate, Milled Both Sides</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17344"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;144</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17344' href="/search/DataSheet.aspx?MatGUID=b5abe85929e84f00bb53acf82a12ced3" >Aleris Hokotol Aluminum Alloy, 100 mm Thickness</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17345"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;145</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17345' href="/search/DataSheet.aspx?MatGUID=522526891469433a80156883a73adbbe" >Aleris Hokotol Aluminum Alloy, 200 mm Thickness</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17346"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;146</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17346' href="/search/DataSheet.aspx?MatGUID=900b3e9f4f3e4557918d2e03c01deb9b" >Aleris Hokotol Aluminum Alloy, 300 mm Thickness</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_159724"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;147</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_159724' href="/search/DataSheet.aspx?MatGUID=3029a79d1e04449ca97c2e43834a6932" >Aleris Hokotol Aluminum Alloy, 325 mm Thickness</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17660"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;148</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17660' href="/search/DataSheet.aspx?MatGUID=72e595303f7d43fea0831c4ae268aa2d" >Kaiser 7068 T6, T6511 Rod & Bar</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17661"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;149</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17661' href="/search/DataSheet.aspx?MatGUID=5cb8993e0f9440dda476d9a6ea051140" >Kaiser 7068 T6, T651 Rod & Bar</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17662"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;150</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17662' href="/search/DataSheet.aspx?MatGUID=dc4c391ec39b4b65b09ac01838cedcf4" >Kaiser 7068 T76, T76511 Rod & Bar</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_178118"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;151</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_178118' href="/search/DataSheet.aspx?MatGUID=4b3f9e2be3d0451e8d839d3e027b6bff" >Misumi ANP79 Aluminum Alloy</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_100846"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;152</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_100846' href="/search/DataSheet.aspx?MatGUID=61ecbebda6074bd78ea1ce141613b66e" >RSP Technology RSA-7034 T6 Aluminum Super Alloy</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_318913"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;153</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_318913' href="/search/DataSheet.aspx?MatGUID=8a804cf352524012a38ae85f677cbb80" >Traid Villarroya TVH® TV 7020-T6 Extruded (AA 7020) Aluminum alloy</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_318926"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;154</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_318926' href="/search/DataSheet.aspx?MatGUID=eefd5280161244f3af1477ded65e7a37" >Traid Villarroya TVH® TV 7020-T6 Drawn (AA 7020) Aluminum alloy</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_318914"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;155</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_318914' href="/search/DataSheet.aspx?MatGUID=526bf218f0b248a0b775af55d531d145" >Traid Villarroya TVH® TV 7075-T6 Extruded (AA 7075) Aluminum alloy</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_318927"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;156</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_318927' href="/search/DataSheet.aspx?MatGUID=49e670e3740946c099aaa123aa404a6e" >Traid Villarroya TVH® TV 7075-T6 Drawn (AA 7075) Aluminum alloy</a></td></tr>

		
	</table>

	<table style="background-color:Silver;" class="tabledataformat" >

		<tr class="">
			<th>&nbsp;</th>
		</tr>

		<tr>
			<td><strong>Found <span id="ctl00_ContentMain_UcSearchResults1_lblResultCount2">156</span> Results</strong> -- 
				
				Page 
				<select name="ctl00$ContentMain$UcSearchResults1$drpPageSelect2" onchange="javascript:setTimeout('__doPostBack(\'ctl00$ContentMain$UcSearchResults1$drpPageSelect2\',\'\')', 0)" id="ctl00_ContentMain_UcSearchResults1_drpPageSelect2">
		<option selected="selected" value="1">1</option>

	</select>
				of <span id="ctl00_ContentMain_UcSearchResults1_lblPageTotal2">1</span> 
				
				-- 
				<a id="ctl00_ContentMain_UcSearchResults1_lnkPrevPage2" disabled="disabled" style="color:Gray;">[Prev Page]</a>
				<a id="ctl00_ContentMain_UcSearchResults1_lnkNextPage2" disabled="disabled" style="color:Gray;">[Next Page]</a>
				--&nbsp;

				view 
				<select name="ctl00$ContentMain$UcSearchResults1$drpPageSize2" onchange="javascript:setTimeout('__doPostBack(\'ctl00$ContentMain$UcSearchResults1$drpPageSize2\',\'\')', 0)" id="ctl00_ContentMain_UcSearchResults1_drpPageSize2">
		<option value="25">25</option>
		<option value="50">50</option>
		<option value="75">75</option>
		<option value="100">100</option>
		<option value="150">150</option>
		<option selected="selected" value="200">200</option>

	</select> per page
			</td>
		</tr>
	</table>
	
	 


</div>





<script type="text/javascript">

	//GLOBAL VARIABLES
	var gCurrentCheckbox;
	var gNewFolderID;
	var gOldFolderName;
	var gSelectedFolderID = 0;
	var gMaxFolders = 0;

	//GLOBAL FIELD REFERENCES
	var drpFolderList = document.getElementById("ctl00_ContentMain_UcSearchResults1_drpFolderList");
	var drpFolderAction = document.getElementById("ctl00_ContentMain_UcSearchResults1_drpFolderAction");
	var txtFolderMatCount = document.getElementById("ctl00_ContentMain_UcSearchResults1_txtFolderMatCount");
	var divUserMessage = document.getElementById("divUserMessage");
	var divFolderName = document.getElementById("divFolderName");
	var tblFolderName = document.getElementById("tblFolderName");
	var txtFolderName = document.getElementById("txtFolderName2");
	
	// See MS ajax.net client side documentation
	// http://ajax.asp.net/docs/ClientReference/Sys.Net/WebrequestManagerClass/	
	Sys.Net.WebRequestManager.add_invokingRequest(ucSearchResults_OnRequestStart);
	Sys.Net.WebRequestManager.add_completedRequest(ucSearchResults_OnRequestEnd);
	
	var timerID = 0;

	function ucSearchResults_OnRequestStart(){
		document.body.style.cursor="progress";
	}
	
	function ucSearchResults_OnRequestEnd(){
		document.body.style.cursor="auto";
	}

	//drpFolderAction pull down does not exist for anonymous users
	if(drpFolderAction) {
		drpFolderAction.onchange=RunFolderAction;
	}

	//RUN ONCE on page load
	//matweb.register_onload(SetSelectedFolder);
	
	SearchMatIDList = []; //an array of MatIDs
	//matweb.alert(MatIDList);
	
	//======================
	//Test Service 
	//======================
	//TestService("This is a test");
	//aci.matweb.WebServices.Folders.RaiseTestError(null,ErrorHandler,"testerror");
	
	function TestService(arg){
		aci.matweb.WebServices.Folders.HelloWorld(arg, TestService_CALLBACK,ErrorHandler,"TestService");
	}
	
	function TestService_CALLBACK(data,context){
		alert(data);
	}
	
	// =================================
	// This function is the onchange event for the material checkbox. See Code Behind.
	// The first time a material is checked, and there are no folders, the folder web service creates a NEW FOLDER
	// =================================
	function ToggleMat(chkBox,MatID,MaxFolderMats){
	
		HideFolderNameControls();

		var FolderID,Context;
		gCurrentCheckbox = chkBox;
		var matLink = $("lnkMatl_" + MatID);
		var MatName = matLink.innerHTML;
		//alert(MatName);

		var SelectedOption = drpFolderList.options[drpFolderList.selectedIndex];  

		if(gNewFolderID > 0)
			FolderID = gNewFolderID;
		else
			FolderID = SelectedOption.value; //this will be 0 if no folders exist yet
		//alert(FolderID);
		
		// call the folder web service 
		// See: ScriptManagerProxy object at the top of this page
		// Also See: /folders/FolderService.asmx & /App_Code/FolderService.cs
		if(chkBox.checked){
			Context = "AddMat";
			aci.matweb.WebServices.Folders.AddMaterial(FolderID, MatID, MatName, ToggleMat_CALLBACK,ErrorHandler,Context);
		}
		else{
			Context = "RemoveMat";
			aci.matweb.WebServices.Folders.RemoveMaterial(FolderID, MatID, ToggleMat_CALLBACK,ErrorHandler,Context);
		}
			
	}
	
	// =================================
	// CALLBACK FUNCTION
	// this function is called by the AJAX framework after the web service is invoked
	// The folder web service returns a ReturnData type object.
	// =================================
	function ToggleMat_CALLBACK(data,context){

		 //alert(data);
		// alert(context);

		var FolderMatCount = data.FolderMatCount
		var NewFolderID = data.NewFolderID
		var DisplayMessage = data.DisplayMessage
		var MaxFolderMats = data.FolderMaxMatls;
		
		txtFolderMatCount.value = FolderMatCount + "/" + MaxFolderMats;

		// If this is the first time the user checked the checkbox, and the user did not already have any folders, 
		// a new folder may have been created.
		// Save the new folderID to a hidden field, so we can reference it later.
		if(NewFolderID > 0){
			gNewFolderID = NewFolderID;
		}
		
		// alert(gNewFolderID);
		// alert(FolderCount);
		// alert(ReloadFolderList);
		// if(NewFolderID>0) alert(NewFolderID);
		// alert(MaxFolderMats);
		
		switch(context){
		case "AddMat":
			if(DisplayMessage){
				gCurrentCheckbox.checked = false;
				matweb.alert(DisplayMessage);
			}
			break    
		case "RemoveMat":
			break    
		default:
			matweb.Alert("Unhandeled Context in ToggleMat_CALLBACK() function: " + Context);
		}//end switch

	}//end function

	
	//=================================
	function CompareMaterials(){
	//=================================
		var SelectedFolderID = GetSelectedFolderID();
		window.location.href="/folders/Compare.aspx?FolderID=" + SelectedFolderID;
	}

	//=================================
	// Runs on drpFolderList.onchange, and also is called by other methods.
	// Sets the checkbox for each visible material in the folder.
	// Also sets txtFolderMatCount with the MatCount indicator for the selected folder.
	//=================================
	function SetSelectedFolder(){
		//alert("SetSelectedFolder()");
		var SelectedFolderID = GetSelectedFolderID();
		//alert("SelectedFolderID :" + SelectedFolderID)
		
		aci.matweb.WebServices.Folders.SetSelectedFolder(SelectedFolderID,SetSelectedFolder_CALLBACK,ErrorHandler,"update_txtFolderMatCount");
		
		HideFolderNameControls();
	}

	// =================================
	// CALLBACK FUNCTION
	// Returns a list of MatID's in the selected folder, and sets the checkbox for each material.
	// Also sets txtFolderMatCount with the MatCount indicator for the selected folder.
	// =================================
	function SetSelectedFolder_CALLBACK(data,context){

		// alert(data);
		// alert(context);

		// the folder web service returns an object with 4 fields
		//alert("data.FolderMatCount: " + data.FolderMatCount);
		//alert("data.FolderMaxMatls: " + data.FolderMaxMatls);
		//alert("data.DisplayMessage: " + data.DisplayMessage);
		//alert("data.MatIDList: " + data.FolderMatIDList);

		if(data.DisplayMessage){
			matweb.alert(data.DisplayMessage);
		}

		txtFolderMatCount.value = data.FolderMatCount + "/" + data.FolderMaxMatls;

		//alert(SearchMatIDList);
		var chkFolder;

		// Set checkboxes for any visible materials in the folder
		// loop over all materials in the search results for the current page...
		//alert('lenOfArray=' + SearchMatIDList.length);
		for(s=0;s<SearchMatIDList.length;s++){
			chkFolder = document.getElementById("chkFolder_" + SearchMatIDList[s]);
			//alert('index=' + SearchMatIDList[s]);
			chkFolder.checked = false; //clear any currently checked boxes...
			//loop over all materials in the folder...
			//If there is a match, make it checked.
			for(f=0;f<data.FolderMatIDList.length;f++){
				if(SearchMatIDList[s]==data.FolderMatIDList[f]){
					//alert("Match found: " + data.FolderMatIDList[f]);
					chkFolder.checked = true;
				}
			}
		}

	}//end function
	
	//=================================
	function RunFolderAction(){
	//=================================
	
		//alert("RunFolderAction()");
		var action = drpFolderAction.options[drpFolderAction.selectedIndex].value;
		var SelectedFolderName = drpFolderList.options[drpFolderList.selectedIndex].text;
		var FolderID = GetSelectedFolderID();
		var msg = "";
		
		switch(action){
			case "empty":
				aci.matweb.WebServices.Folders.EmptyFolder(FolderID,SetSelectedFolder_CALLBACK,ErrorHandler,action);
				msg = "Folder ["+SelectedFolderName+"] was cleared of materials";
				HideFolderNameControls();
				break;
			case "delete":
				var answer = confirm("Delete folder ["+SelectedFolderName+"] ?");
				if(answer){
					gOldFolderName = SelectedFolderName;
					aci.matweb.WebServices.Folders.DeleteFolder(FolderID,DeleteFolder_CALLBACK,ErrorHandler,action);
				}
				HideFolderNameControls();
				break;
			case "new":
				//setting up new folder controls
				if(drpFolderList.options.length >= gMaxFolders){
					matweb.alert("You have already added as many folders as you are currently allowed by the system (" + gMaxFolders + ").<p/> You may view our <a href=\"/membership/benefits.aspx\">benefits</a> page for the features that are allowed for your current user level.");
				}
				else{
				divFolderName.style.display="block";
				divFolderName.innerHTML="New Folder Name";
				tblFolderName.style.display="block";
				txtFolderName.value = "New Folder Name";
				txtFolderName.focus();
				txtFolderName.select();
				}
				break;
			case "rename":
				//setting up rename folder controls
				divFolderName.style.display="block";
				divFolderName.innerHTML="Rename Folder";
				tblFolderName.style.display="block";
				txtFolderName.value = SelectedFolderName;
				txtFolderName.focus();
				txtFolderName.select();
				break;
			case "managefolders":
				window.location.href="/folders/ListFolders.aspx";
				break;
			case "expSolidworks":
				ExportFolder(FolderID, 2, null);
				break;
			case "expALGOR":
				ExportFolder(FolderID, 3, null);
				break;
			case "expANSYS":
				ExportFolder(FolderID, 4, null);
				break;
			case "expNEiWorks":
				ExportFolder(FolderID, 5, null);
				break;
			case "expCOMSOL3":
				ExportFolder(FolderID, 6, '3.4 or newer');
				break;
			case "expCOMSOL4":
				ExportFolder(FolderID, 10, null);
				break;
			case "expETBX":
				ExportFolder(FolderID, 8, null);
				break;
			case "expSpaceClaim":
				ExportFolder(FolderID, 9, null);
				break;
			case "0":
				//do nothing
				drpFolderAction.selectedIndex = 0;
				break;
			default: 
				alert("Unhandled Action: " + action);
		}

		//drpFolderAction.selectedIndex=0;
		divUserMessage.innerHTML = msg;
		//alert("End RunFolderAction()");
		
	}
	
	function DeleteFolder_CALLBACK(DeleteWasSuccessfull,context){
		divUserMessage.innerHTML = "Folder ["+gOldFolderName+"] was deleted.";
		//Refresh the folder list and selected checkboxes
		aci.matweb.WebServices.Folders.GetFolderList(GetFolderList_CALLBACK,ErrorHandler,"DeleteFolder_CALLBACK");
	}	

	// rsw
	function ExportFolder(folderid, modeid, comsolversion){
		if(txtFolderMatCount.value.substring(0,1) == '0'){
			alert('Folder is empty, nothing to export.');
			drpFolderAction.selectedIndex = 0;
			return;
		}

		var ExportPath = "/folders/ListFolders.aspx";
		
		//alert(ExportPath);
		location.href = ExportPath;
		//window.open(ExportPath, '', '', '');
	}
	
	
	
	
	//===============================================================
	// This runs on the onclick event for btnApplyFolderName.
	// It handles creating a new folder, or renaming an existing one.
	//===============================================================
	function ApplyFolderName(){
	
		//alert("ApplyFolderName()");
		var action = drpFolderAction.options[drpFolderAction.selectedIndex].value;
		var NewFolderName = txtFolderName.value;
		var FolderID = GetSelectedFolderID();
		var msg = "";

		//alert(FolderID);

		//If we are showing the non-existant "virtual" folder "My Folder",
		//create it first, and then rename it.
		if(action=="rename" && FolderID==0)
			action="add_and_rename";

		switch(action){
			case "new":
				aci.matweb.WebServices.Folders.AddFolder(NewFolderName,AddRenameFolder_CALLBACK,ErrorHandler,action);
				break;
			case "rename":
				gOldFolderName = drpFolderList.options[drpFolderList.selectedIndex].text;
				aci.matweb.WebServices.Folders.RenameFolder(FolderID,NewFolderName,AddRenameFolder_CALLBACK,ErrorHandler,action);
				break;
			case "add_and_rename":
				gOldFolderName = drpFolderList.options[drpFolderList.selectedIndex].text;
				aci.matweb.WebServices.Folders.AddFolder(NewFolderName,AddRenameFolder_CALLBACK,ErrorHandler,action);
				break;
			default: 
				alert("Unhandled Action in ApplyFolderName(): " + action);
		}

		HideFolderNameControls();
		
	}
	
	//=================================
	// CALLBACK FUNCTION
	//=================================
	function AddRenameFolder_CALLBACK(ReturnData,context){
		//alert("AddRenameFolder_CALLBACK()");
		
		var msg;
		//Folder = ReturnData.Folder;
	
		switch(context){
			case "new":
				//gNewFolderID = Folder.FolderID;
				gNewFolderID = ReturnData.NewFolderID;
				msg = "The folder [" + ReturnData.NewFolderName + "] was created.";
				break;
			case "rename":
				msg = "The folder [" + gOldFolderName+ "] was renamed to [" + ReturnData.NewFolderName + "].";
				break;
			case "add_and_rename":
				//gNewFolderID = Folder.FolderID;
				gNewFolderID = ReturnData.NewFolderID;
				msg = "The folder [" + gOldFolderName+ "] was renamed to [" + ReturnData.NewFolderName + "].";
				break;
		}
		
		divUserMessage.innerHTML = msg;
		
		//refresh the folder list, so it includes the new folder
		aci.matweb.WebServices.Folders.GetFolderList(GetFolderList_CALLBACK,ErrorHandler,"AddFolder");

	}
	
	//=================================
	function HideFolderNameControls(){
	//=================================
		//These fields will not exist for anonymous users
		if(drpFolderAction){
			drpFolderAction.selectedIndex=0;
			divFolderName.style.display="none";
			tblFolderName.style.display="none";
		}
	}
	
	//=================================
	function GetSelectedFolderID(){
	//=================================
		var FolderID = 0;
		//if one of the processeses created a new folder id, then use it.
		// alert('gNewFolderID=' + gNewFolderID);
		if(gNewFolderID > 0){
			FolderID = gNewFolderID;
			//gNewFolderID = 0;//clear the new folder id, so we don't use it again.
		}
		else 
			if(drpFolderList != null){
				if(drpFolderList.options.length == 0){
					FolderID = gSelectedFolderID;
				}
				else{
					var SelectedOption = drpFolderList.options[drpFolderList.selectedIndex];  
					FolderID = SelectedOption.value; //this will be 0 if no folders exist yet
				}
			}
		return FolderID;
	}

	//=================================
	function UnregisteredUserMessage(chkBox){
	//=================================
		matweb.alert("This feature is reserved for registered users.<p/><a href='/membership/regstart.aspx'>Click here to register.</a>","Reserved Feature");
		chkBox.checked = false;
	}

	// =================================
	// CALLBACK FUNCTION
	// The folder web service returns a JS array of iBatis data objects of type: aci.matweb.data.folder.Folder 
	// The drop down list of folders is set using this list.
	// =================================
	function GetFolderList_CALLBACK(iFolderList,Context){
	
		//alert(iFolderList);
		//alert('iFolderList.length: ' + iFolderList.length);

		if(drpFolderList != null){
		
		var SelectedFolderID = GetSelectedFolderID();//remember the current selected folder, so we can use it later
		var opt;
		var folder;

		//clear the options and reload them
		drpFolderList.options.length = 0;

		//add a default empty folder...		
		if(iFolderList.length==0){
			opt = document.createElement('OPTION');
			opt.value = "0";
			opt.text = "My Folder";
			drpFolderList.options.add(opt);
			i++;
		}
		
		for(var i = 0;i < iFolderList.length;i++){
			opt = document.createElement('OPTION');
			folder = iFolderList[i];
			//alert(folder.FolderID);
			//alert(folder.FolderName);
			opt.value = folder.FolderID;
			opt.text = folder.FolderName;
			if(folder.FolderID == SelectedFolderID) opt.selected = true;
			drpFolderList.options.add(opt);
		}

		//clear the new folder id, so we don't use it again. 
		gNewFolderID = 0;
		
		//Set checkboxes and folder count fields for the new selected folder
		SetSelectedFolder();
		}

	}
	
	// =================================
	// ON ERROR CALLBACK FUNCTION
	// =================================
	function ErrorHandler(error,context) {
	
			var stackTrace = error.get_stackTrace();
			var message = error.get_message();
			var statusCode = error.get_statusCode();
			var exceptionType = error.get_exceptionType();
			var timedout = error.get_timedOut();
			var Debug = false;

			// Compose the error message...
			var ErrMsg =     
					"<strong>Stack Trace</strong><br/>" +  stackTrace + "<br/>" +
					"<strong>WebService Error</strong><br/>" + message + "<br/>" +
					"<strong>Status Code</strong><br/>" + statusCode + "<br/>" +
					"<strong>Exception Type</strong><br/>" + exceptionType + "<br/>" +
					"<strong>JS Context</strong><br/>" + context + "<br/>" +
					"<strong>Timedout</strong><br/>" + timedout + "<br/>" +
					"<strong>Source Page</strong><br/>" + "/search/MaterialGroupSearch.aspx";
			//matweb.alert(ErrMsg,"Error in WebService",null,"500");
			if(Debug){
				matweb.alert(ErrMsg,"Error in WebService",null,"500");
				//alert(ErrMsg);
			}
			else{
				ErrMsg = escape(ErrMsg);
				//matweb.alert(ErrMsg);
				//window.location.href="/errorJS.aspx?ErrMsg=" + ErrMsg;
				divUserMessage.innerHTML = "Problem retrieving folder data...Some folder features may not work.";
			}
		
	}
	
	function ShowInterpolationMessage(){
		InterpolationMsg = "The material was found via interpolation, so no data points can be displayed. See the material data sheet for actual data points.";
		matweb.alert(InterpolationMsg);
	}
	
</script>

		</td>
	</tr>
	</table>

<hr />
<a id="help"></a>
<table><tr><td>
<p/><b>Instructions:</b> This page allows you to quickly access all of the polymers/plastics, metals, ceramics, fluids, and other engineering materials
in the MatWeb material property database.&nbsp; Just select the material category you would like to find from the tree.  Click on the [+] icon to open subcategory branches in the tree.
Then click the <b>'Find'</b> button next to its box.&nbsp; You can then follow the links to the most complete data sheets on the Web, with information on over 1000 available properties, including dielectric
constant, Poisson's ratio, glass transition temperature, dissipation factor, and Vicat softening point.
</td><td><a href="https://proto3000.com"><img src="/images/assets/proto3000.jpg" border = "0"><br>

Proto3000 - Rapid Prototyping</a>
</td></tr></table>

<p/><span class="hiText">Notes:</span>

<ul>
  <li>Visit MatWeb's <a href="/search/PropertySearch.aspx">property-based search page</a> to limit your results to materials that meet specific property performance criteria.</li>
  <li>Text searches can be done from any page - use the <b>Quick Search</b> box in the top right of the page.</li>
  <li>More <a href="/help/strategy.aspx"><strong>Search Strategies</strong></a> on how to find information in MatWeb.</li>
   <li><a href="/search/GetAllMatls.aspx">Show All Materials</a> - an inefficient drill-down to data sheets.  Registered users who are logged in omit a step in this drill down process.</li>
</ul>

<p/><a href="#Top">Top</a>

<script type="text/javascript">

	var txtMatGroupText = document.getElementById("ctl00_ContentMain_txtMatGroupText");
	var txtMatGroupID = document.getElementById("ctl00_ContentMain_txtMatGroupID");
	var divMatGroupName = document.getElementById("ctl00_ContentMain_divMatGroupName");

	function ucMatGroupFinder1_OnSelected(MatGroupID,MatGroupText){
		txtMatGroupText.value = MatGroupText;
		txtMatGroupID.value = MatGroupID;
		divMatGroupName.innerHTML = MatGroupText;
		//__doPostBack("ctl00$ContentMain$btnSubmit","");
	}

	function ucMatGroupTree_OnNodeClick(NodeText,MatGroupID){
		txtMatGroupText.value = NodeText;
		txtMatGroupID.value = MatGroupID;
		divMatGroupName.innerHTML = NodeText;
		//__doPostBack("ctl00$ContentMain$btnSubmit","");
	}
	
	function validate(){
		if (txtMatGroupID.value == ""){
			matweb.alert("Please select a material category","User Input Error");
			return false;
		}
		
		return true;
		
	}

</script>


</div>
<!-- ===================================== END MAIN CONTENT ========================================== -->

<table class="tabletight" style="width:100%" >
        <tr>
                <td align="center" class="footer" colspan="3">
                        <br /><br />
                
                        <a href="/membership/regupgrade.aspx" class="footlink"><span class="subscribeLink">Subscribe to Premium Services</span></a><br/>
                
                        <span class="footer"><b>Searches:</b>&nbsp;&nbsp;</span>
                        <a href="/search/AdvancedSearch.aspx" class="footlink"><span class="foot">Advanced</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/search/CompositionSearch.aspx" class="footlink"><span class="foot">Composition</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/search/PropertySearch.aspx" class="footlink"><span class="foot">Property</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/search/MaterialGroupSearch.aspx" class="footlink"><span class="foot">Material&nbsp;Type</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/search/SearchManufacturerName.aspx" class="footlink"><span class="foot">Manufacturer</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/search/SearchTradeName.aspx" class="footlink"><span class="foot">Trade&nbsp;Name</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/search/SearchUNS.aspx" class="footlink"><span class="foot">UNS Number</span></a>
                        <br />
                        <span class="footer"><b>Other&nbsp;Links:</b>&nbsp;&nbsp;</span>
                        <a href="/services/advertising.aspx" class="footlink"><span class="foot">Advertising</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/services/submitdata.aspx" class="footlink"><span class="foot">Submit&nbsp;Data</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/services/databaselicense.aspx" class="footlink"><span class="foot">Database&nbsp;Licensing</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/services/webhosting.aspx" class="footlink"><span class="foot">Web&nbsp;Design&nbsp;&amp;&nbsp;Hosting</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/clickthrough.aspx?addataid=277" class="footlink"><span class="foot">Trade&nbsp;Publications</span></a>

                        <br />
                        <a href="/reference/suppliers.aspx" class="footlink"><span class="foot">Supplier&nbsp;List</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/tools/unitconverter.aspx" class="footlink"><span class="foot">Unit&nbsp;Converter</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/tools/contents.aspx#reference" class="footlink"><span class="foot">Reference</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/reference/link.aspx" class="footlink"><span class="foot">Links</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/help/help.aspx" class="footlink"><span class="foot">Help</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/services/contact.aspx" class="footlink"><span class="foot">Contact&nbsp;Us</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/tools/contents.aspx" class="footlink"><span class="foot" style="color:red;">Site&nbsp;Map</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/reference/faq.aspx" class="footlink"><span class="foot">FAQ</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/index.aspx" class="footlink"><span class="foot">Home</span></a>
                        <br />&nbsp;
                        <div id="fb-root"></div>

<table><tr><td><script src="http://connect.facebook.net/en_US/all.js#xfbml=1"></script><fb:like-box href="https://www.facebook.com/MatWeb.LLC" width="175" show_faces="false" stream="false" header="false"></fb:like-box>

</td><td>

<a href="https://twitter.com/MatWeb" class="twitter-follow-button" data-show-count="false" data-show-screen-name="false">Follow @MatWeb</a> <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>

</td></tr></table>
                </td>
        </tr>
        <tr>
                <td colspan="3"><img src="/images/bluebar.gif" width="100%" height="5" alt="" /></td>
        </tr>
        <tr>
                <td style="background-image:url(/images/gray.gif);"><img src="/images/spacer.gif" width="5" height="1" alt=""/></td>
                <td style="background-image:url(/images/gray.gif);">
                        <span class="footer">
                        Please read our <a href="/reference/terms.aspx" class="footlink"><span class="foot">License Agreement</span></a> regarding materials data and our  <a href="/reference/privacy.aspx" class="footlink"><span class="foot">Privacy Policy</span></a>.
                        Questions or comments about MatWeb? Please contact us at
                        <a href="mailto:webmaster@matweb.com" class="footlink"><span class="foot">webmaster@matweb.com</span></a>. We appreciate your input.
                        <br /><br />
                        The contents of this web site, the MatWeb logo, and "MatWeb" are Copyright 1996-2024
                        by MatWeb, LLC. MatWeb is intended for personal, non-commercial use. The contents, results, and technical data from this site
                        may not be reproduced either electronically, photographically or substantively without permission from MatWeb, LLC.
                        </span>
                        <br />
                </td>

                <td style="background-image:url(/images/gray.gif);"><img src="/images/spacer.gif" width="5" height="1" alt=""/></td>
        </tr>
</table>




<script type="text/javascript">
//<![CDATA[
var ctl00_ContentMain_ucMatGroupTree_msTreeView_ImageArray =  new Array('', '', '', '/WebResource.axd?d=zLp3QJeZSj4-fsnL2_raV70Mk3pr-OdjL_rvrZzXUTCnZ6LU5Dg_Tc5XZg4eZpT9O0gxZJrJcxGb23XJofeGD9JhYLr2wl0VkQZnJOx0T0ShBosq0&t=638313619312541215', '/WebResource.axd?d=vFualGN2ZZ4fny_RX_gYiUTY6FBLUapne4VfY6P3sGiyrOYd-Ji2sWg_olT-exaztendUUEOoH87RS2l44bpZTscfmmv4MKRqJK8rgesmkrNfP2O0&t=638313619312541215', '/WebResource.axd?d=4nLCbQ2Yfc2i_msrQXqKzK8pLQD-GrNVZ7DeXU-La2qatczrgeCLazv0RzIkDDHyYfLFalCBO8s71lRS8yifxTChF9dUJvTLxNEeeFlghyuY0dtM0&t=638313619312541215', '/WebResource.axd?d=uEI6Enq15_q4pXO6g0E7vtAqtD5WIokAEQ2Vz8V3Yi36cefediLfyCwENkPd-w1GxTXu-HkNqcDlrYOw8bxgGqeHJtzc-PBlTa9nrZRfdIyj8fQh0&t=638313619312541215', '/WebResource.axd?d=hzeik1gGhZrfDZXUWCMsBOx5b43bIFor8UCTzm_wuh92b9Z-WQU4_fQCSUw6CaP5tnc9LTU0jl0sy4Y6Cf_c8q8HFXRqeHQuscXIqqLNSZkgl2cu0&t=638313619312541215', '/WebResource.axd?d=6v5qM6VKQYD3XNwwXEy7Tix7dzVdqDJ8QTRIlUPrzXQqNsXQgUwIfmn34ZzRORd_IZj0bF4vkZzID1HuSFE5uKyLgaPYeAYE8VT8HYfb3Rk6zsDw0&t=638313619312541215', '/WebResource.axd?d=d9nh8nEM1YCqIfE9jLzNhPyFIZqARvlFExwnFG0vbvskO-A2qORlhTyUVIxDtSwldGrOp8o9ef8lZ9SRhPQ9rv9TkGV-NVYvH9_yHT6CvWpIPF4S0&t=638313619312541215', '/WebResource.axd?d=7_V5n1KYSQsj6PoSqgIxb0egBsAa91jSXPkbmM6FaiF14-JsNnpUDnfSdxZcvfd0OQCLDtC24pMIXigO-41cL2rsq-0xea-SSG7yZUyRcliFFqe70&t=638313619312541215', '/WebResource.axd?d=TinC6igX1vPDgWh7DstWwBTHTyH4uuybDU4NW47jJD06wHdAO3YyJC_eKK1Zn5oIbwqRo4Cri8YwhA82G-LD0p14yEF1EfoIZvKleYMIuQ2K1hkE0&t=638313619312541215', '/WebResource.axd?d=4B0dvJqmzY4ukTboQq7lmxokRL9NTsoWh4yErB11Tb-ucvMw_p_0mxYVn4U1Uz6Jk795umrO0C4-B-8Yq1Kxxz6uyDT15O2BSCb8349f4YWDdIVy0&t=638313619312541215', '/WebResource.axd?d=TUiibr9hjS_MlNf09dKZECe1z56fbaPbfgJfC5eYJi0TtdBviUcA1JQJLV81aqunzpXysgruhL3ihpgd7ziL5p9SQ86xVLNiUqbYpJnW9Rj7sN1E0&t=638313619312541215', '/WebResource.axd?d=vUBrM2nNhECNbFO0AJKq52p2zWrY5XG8nWgCDL4AfsrexStb0HREDV6GHMvdkAGDInxYtTyWXuxFUIiQv3JX2fScVRwQsrVv5srHrjOIeC3ayAG20&t=638313619312541215', '/WebResource.axd?d=Z-LP5lKcsnv2A9UpExXu-5leQYIAZMYp6SXNognZi1r66YT8u24-YjJaxjisE9ruYU8NKnNz72H4shclsk8PM3S5M-VYMk_F_G5fcd3ra6Sj9CaS0&t=638313619312541215', '/WebResource.axd?d=QI4SACqRn7PxBBHPfkR7peFMAEv7lX1_iZ_7jncKwxRTlvhSAncd5K2oRqw8J8WmJjWQcbGgEblhrufy-hUjZMa1YLUC47mjfV1eS4LM3kUqLT960&t=638313619312541215', '/WebResource.axd?d=ayrFqNSzdiTAdowDVbEH-ZzKbvw5bCq7onyCSAcJhCVPVIHlUB9M_fo8BLUTeGpbtcoMiyFy1MoeEIsweMrpVandBKC4vAK-NCBcfn2FIreK8mEJ0&t=638313619312541215', '/WebResource.axd?d=OK_uWiuoGURaJ3CC0fVlQ7v9GUnIAkx03w03rQ2ENqcwdObLiQwK7B9yRFyAWj9tuUqsa0874ksgkaRo8KirB7q1ZL_huh7gCq_XehXfHMEKMiAl0&t=638313619312541215');
//]]>
</script>


<script type="text/javascript">
//<![CDATA[
(function() {var fn = function() {AjaxControlToolkit.ModalPopupBehavior.invokeViaServer('ctl00_ContentMain_ucPopupMessage1_ModalPopupExtender1', false); Sys.Application.remove_load(fn);};Sys.Application.add_load(fn);})();
var callBackFrameUrl='/WebResource.axd?d=Mt7MojvAyTqAEcx0MpKgcF_QVa09tLyMQS6c5JLNO4j2M_F6CMxYnN0HdVYxyrhggOpkTwyNqQ51iS6UoHXgJqHBROE1&t=638313619312541215';
WebForm_InitCallback();var ctl00_ContentMain_ucMatGroupTree_msTreeView_Data = new Object();
ctl00_ContentMain_ucMatGroupTree_msTreeView_Data.images = ctl00_ContentMain_ucMatGroupTree_msTreeView_ImageArray;
ctl00_ContentMain_ucMatGroupTree_msTreeView_Data.collapseToolTip = "Collapse {0}";
ctl00_ContentMain_ucMatGroupTree_msTreeView_Data.expandToolTip = "Expand {0}";
ctl00_ContentMain_ucMatGroupTree_msTreeView_Data.expandState = theForm.elements['ctl00_ContentMain_ucMatGroupTree_msTreeView_ExpandState'];
ctl00_ContentMain_ucMatGroupTree_msTreeView_Data.selectedNodeID = theForm.elements['ctl00_ContentMain_ucMatGroupTree_msTreeView_SelectedNode'];
for (var i=0;i<19;i++) {
var preLoad = new Image();
if (ctl00_ContentMain_ucMatGroupTree_msTreeView_ImageArray[i].length > 0)
preLoad.src = ctl00_ContentMain_ucMatGroupTree_msTreeView_ImageArray[i];
}
ctl00_ContentMain_ucMatGroupTree_msTreeView_Data.lastIndex = 8;
ctl00_ContentMain_ucMatGroupTree_msTreeView_Data.populateLog = theForm.elements['ctl00_ContentMain_ucMatGroupTree_msTreeView_PopulateLog'];
ctl00_ContentMain_ucMatGroupTree_msTreeView_Data.treeViewID = 'ctl00$ContentMain$ucMatGroupTree$msTreeView';
ctl00_ContentMain_ucMatGroupTree_msTreeView_Data.name = 'ctl00_ContentMain_ucMatGroupTree_msTreeView_Data';
Sys.Application.initialize();
Sys.Application.add_init(function() {
    $create(AjaxControlToolkit.ModalPopupBehavior, {"BackgroundCssClass":"modalBackground","OkControlID":"ctl00_ContentMain_ucPopupMessage1_btnOK","PopupControlID":"ctl00_ContentMain_ucPopupMessage1_divPopupMessage","PopupDragHandleControlID":"ctl00_ContentMain_ucPopupMessage1_trTitleRow","dynamicServicePath":"/search/MaterialGroupSearch.aspx","id":"ctl00_ContentMain_ucPopupMessage1_ModalPopupExtender1"}, null, null, $get("ctl00_ContentMain_ucPopupMessage1_hndPopupControl"));
});
Sys.Application.add_init(function() {
    $create(AjaxControlToolkit.TextBoxWatermarkBehavior, {"ClientStateFieldID":"ctl00_ContentMain_UcMatGroupFinder1_TextBoxWatermarkExtender2_ClientState","WatermarkCssClass":"greyOut","WatermarkText":"Type at least 4 characters here...","id":"ctl00_ContentMain_UcMatGroupFinder1_TextBoxWatermarkExtender2"}, null, null, $get("ctl00_ContentMain_UcMatGroupFinder1_txtSearchText"));
});
//]]>
</script>
</form>


<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-15290815-1");
pageTracker._setDomainName(".matweb.com");
pageTracker._trackPageview();
} catch(err) {}</script>

<!-- 20160905 insert SpecialChem javascript -->
<script type="text/javascript" src="//collect.specialchem.com/collect.js"></script>
<script type="text/javascript">
    //<![CDATA[
    var _spc = (_spc || []);
    _spc.push(['init',
    {
        partner: 'MatWeb'
        
        //, iid: '12345'
    }]);
    //]]>
</script>
<script> (function(){ var s = document.createElement('script'); var h = document.querySelector('head') || document.body; s.src = 'https://acsbapp.com/apps/app/dist/js/app.js'; s.async = true; s.onload = function(){ acsbJS.init({ statementLink : '', footerHtml : '', hideMobile : false, hideTrigger : false, disableBgProcess : false, language : 'en', position : 'right', leadColor : '#146FF8', triggerColor : '#146FF8', triggerRadius : '50%', triggerPositionX : 'right', triggerPositionY : 'bottom', triggerIcon : 'people', triggerSize : 'bottom', triggerOffsetX : 20, triggerOffsetY : 20, mobile : { triggerSize : 'small', triggerPositionX : 'right', triggerPositionY : 'bottom', triggerOffsetX : 20, triggerOffsetY : 20, triggerRadius : '20' } }); }; h.appendChild(s); })();</script>
</body>
</html>
