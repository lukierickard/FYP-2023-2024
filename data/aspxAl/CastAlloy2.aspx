

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head id="ctl00_mainHeader"><title>
	Metal, Plastic, and Ceramic Search Index
</title><meta http-equiv="Content-Style-Type" content="text/css" /><meta http-equiv="Content-Language" content="en" />

        <script src="/includes/flash.js" type="text/javascript"></script>

        <script src="/includes/prototype.js" type="text/javascript"></script>
        <!-- script src="/includes/scriptaculous/effects.js" type="text/javascript"></script -->
        <script src="/includes/matweb.js" type="text/javascript"></script>
        <link rel="stylesheet" href="/includes/main.css" />

        <script type="text/javascript">

                function _onload(){
                        matweb.debug = false; // ;
                        //matweb.writeOverlay();
                }

                function _onkeydown(evt){
                        //PREVENT GLOBAL AUTO-SUBMIT OF FORMS
                        return matweb.DisableAutoSubmit(evt);
                }

                //EXAMPLE USAGE:
                //matweb.register_onload(function(){alert("hello from onload event");});
                //matweb.writeOverlay();


        </script>

        <style type="text/css" media="screen">
        @import url("/ad_IES/css/styles.css");
        </style>
<meta name="KEYWORDS" content="carbon steels, low alloy steels, corrosion resistant stainless, high temperature stainless, heat resistant stainless steels, AISI steel alloy specifications, high strength steel alloys, tensile strength of steels, maraging, superalloy, lead alloy, magnesium, tin alloy, tungsten, zinc, precious metal, pure metallic element, TPE elastomer, dielectric constant, plastic dissipation factor, nylon glass transition temperature, ionomer vicat softening point, poisson's ratio, polyetherimide, fluoropolymer, silicone, polyester dielectric constant" /><meta name="DESCRIPTION" content="Searchable list of plastics, metals, and ceramics categorized into a fields like polypropylene, nylon, ABS, aluminum alloys, oxides, etc.  Search results are detailed data sheets with tensile strength, density, dielectric constant, dissipation factor, poisson's ratio, glass transition temperature, and Vicat softening point." /><meta name="ROBOTS" content="NOFOLLOW" /><style type="text/css">
	.ctl00_ContentMain_ucMatGroupTree_msTreeView_0 { text-decoration:none; }
	.ctl00_ContentMain_ucMatGroupTree_msTreeView_1 { color:Black; }
	.ctl00_ContentMain_ucMatGroupTree_msTreeView_2 { padding:0px 3px 0px 3px; }

</style></head>

<body onload="_onload();" onkeydown="return _onkeydown(event);">

<a id="Top"></a>

<div id="divSiteName" style="position:absolute; top:0; left:0 ;">
<a href="/index.aspx"><img src="/images/sitelive.gif" alt="" /></a>
</div>

<div id="divPopupOverlay" style="display:none;"></div>

<div id="divPopupAlert" class="popupAlert" style="display:none;">
        <table style="width:100%; height:100%; border-style:outset; border-width:5px; border-color:blue;" >
                <tr><th style=" height:25px; background-color:Maroon; font-size:14px; color:White; "><span id="spanPopupAlertTitle">System Message</span></th></tr>
                <tr><td style=" vertical-align:top; padding:3px; background-color:White;" id="tdPopupAlertMessage" ></td></tr>
                <tr><td style=" height:25px; text-align:center; background-color:Silver;"><input type="button" id="btnPopupAlert" onclick="matweb.alertClose();" style="" value="OK" /></td></tr>
        </table>
</div>

<form name="aspnetForm" method="post" action="MaterialGroupSearch.aspx" onsubmit="javascript:return WebForm_OnSubmit();" id="aspnetForm">
<div>
<input type="hidden" name="__EVENTTARGET" id="__EVENTTARGET" value="" />
<input type="hidden" name="__EVENTARGUMENT" id="__EVENTARGUMENT" value="" />
<input type="hidden" name="__LASTFOCUS" id="__LASTFOCUS" value="" />
<input type="hidden" name="ctl00_ContentMain_ucMatGroupTree_msTreeView_ExpandState" id="ctl00_ContentMain_ucMatGroupTree_msTreeView_ExpandState" value="ccccccnc" />
<input type="hidden" name="ctl00_ContentMain_ucMatGroupTree_msTreeView_SelectedNode" id="ctl00_ContentMain_ucMatGroupTree_msTreeView_SelectedNode" value="" />
<input type="hidden" name="ctl00_ContentMain_ucMatGroupTree_msTreeView_PopulateLog" id="ctl00_ContentMain_ucMatGroupTree_msTreeView_PopulateLog" value="" />
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="/wEPDwULLTE2Nzc3MDc2MDAPZBYCZg9kFgICAQ9kFioCAQ9kFgJmDxYCHgRUZXh0BWdEYXRhIHNoZWV0cyBmb3Igb3ZlciA8c3BhbiBjbGFzcz0iZ3JlZXRpbmciPjE4MCwwMDA8L3NwYW4+ICBtZXRhbHMsIHBsYXN0aWNzLCBjZXJhbWljcywgYW5kIGNvbXBvc2l0ZXMuZAICD2QWAmYPFgIfAAUESE9NRWQCAw9kFgJmDxYCHwAFBlNFQVJDSGQCBA9kFgJmDxYCHwAFBVRPT0xTZAIFD2QWAmYPFgIfAAUJU1VQUExJRVJTZAIGD2QWAmYPFgIfAAUHRk9MREVSU2QCBw9kFgJmDxYCHwAFCEFCT1VUIFVTZAIID2QWAmYPFgIfAAUDRkFRZAIJD2QWAmYPFgIfAAUHQUNDT1VOVGQCCg9kFgJmDxYCHwAFBkxPRyBJTmQCCw8QZGQWAWZkAgwPZBYCZg8WAh8ABQhTZWFyY2hlc2QCDQ9kFgJmDxYCHwAFCEFkdmFuY2VkZAIOD2QWAmYPFgIfAAUIQ2F0ZWdvcnlkAg8PZBYCZg8WAh8ABQhQcm9wZXJ0eWQCEA9kFgJmDxYCHwAFBk1ldGFsc2QCEQ9kFgJmDxYCHwAFClRyYWRlIE5hbWVkAhIPZBYCZg8WAh8ABQxNYW51ZmFjdHVyZXJkAhMPZBYCAgEPZBYCZg8WAh8ABRlSZWNlbnRseSBWaWV3ZWQgTWF0ZXJpYWxzZAIVD2QWEAIDD2QWAmYPFgIfAAWZATxhIGhyZWY9Ii9jbGlja3Rocm91Z2guYXNweD9hZGRhdGFpZD0xMzQ5Ij48aW1nIHNyYz0iL2ltYWdlcy9hc3NldHMvbWV0YWxtZW4tQWx1bWludW0ucG5nIiBib3JkZXI9MCBhbHQ9Ik1ldGFsbWVuIFNhbGVzIiB3aWR0aCA9ICI3MjgiIGhlaWdodCA9ICI5MCI+PC9hPmQCBA9kFgICAg8WAh4Fc3R5bGUFGWRpc3BsYXk6bm9uZTt3aWR0aDoyNTBweDsWBgIBD2QWAmYPZBYCZg8PFgIfAAUSVXNlciBJbnB1dCBQcm9ibGVtZGQCAw8PFgIfAGVkZAIFDxYCHgV2YWx1ZQUCT0tkAgUPZBYEAgEPD2QWAh8BBQxXaWR0aDozMDRweDtkAgIPEBYEHwEFK3Bvc2l0aW9uOmFic29sdXRlO2Rpc3BsYXk6bm9uZTtXaWR0aDozMDhweDseBHNpemUFAjE4ZGRkAgYPDxYEHghDbGllbnRJRAIBHgpMYW5ndWFnZUlEAgFkFgJmDxQrAAkPFg4eF1BvcHVsYXRlTm9kZXNGcm9tQ2xpZW50Zx4SRW5hYmxlQ2xpZW50U2NyaXB0Zx4JU2hvd0xpbmVzZx4ITm9kZVdyYXBnHg1OZXZlckV4cGFuZGVkZB4MU2VsZWN0ZWROb2RlZB4JTGFzdEluZGV4AghkFggeC05vZGVTcGFjaW5nGwAAAAAAAAAAAQAAAB4JRm9yZUNvbG9yCiMeEUhvcml6b250YWxQYWRkaW5nGwAAAAAAAAhAAQAAAB4EXyFTQgKEgBhkZGRkZGQUKwAJBSMyOjAsMDowLDA6MSwwOjIsMDozLDA6NCwwOjUsMDo2LDA6NxQrAAIWDB8ABRJDYXJib24gKDg2NiBtYXRscykeBVZhbHVlBQMyODMeCEV4cGFuZGVkaB4MU2VsZWN0QWN0aW9uCyouU3lzdGVtLldlYi5VSS5XZWJDb250cm9scy5UcmVlTm9kZVNlbGVjdEFjdGlvbgIeC05hdmlnYXRlVXJsBUFqYXZhc2NyaXB0OnVjTWF0R3JvdXBUcmVlX09uTm9kZUNsaWNrKCdDYXJib24gKDg2NiBtYXRscyknLCcyODMnKR4QUG9wdWxhdGVPbkRlbWFuZGdkFCsAAhYMHwAFFUNlcmFtaWMgKDEwMDA0IG1hdGxzKR8RBQIxMR8SaB8TCysEAh8UBUNqYXZhc2NyaXB0OnVjTWF0R3JvdXBUcmVlX09uTm9kZUNsaWNrKCdDZXJhbWljICgxMDAwNCBtYXRscyknLCcxMScpHxVnZBQrAAIWDB8ABRJGbHVpZCAoNzU2MiBtYXRscykfEQUBNR8SaB8TCysEAh8UBT9qYXZhc2NyaXB0OnVjTWF0R3JvdXBUcmVlX09uTm9kZUNsaWNrKCdGbHVpZCAoNzU2MiBtYXRscyknLCc1JykfFWdkFCsAAhYMHwAFE01ldGFsICgxNzA1MiBtYXRscykfEQUBOR8SaB8TCysEAh8UBUBqYXZhc2NyaXB0OnVjTWF0R3JvdXBUcmVlX09uTm9kZUNsaWNrKCdNZXRhbCAoMTcwNTIgbWF0bHMpJywnOScpHxVnZBQrAAIWDB8ABSdPdGhlciBFbmdpbmVlcmluZyBNYXRlcmlhbCAoODA2MyBtYXRscykfEQUDMjg1HxJoHxMLKwQCHxQFVmphdmFzY3JpcHQ6dWNNYXRHcm91cFRyZWVfT25Ob2RlQ2xpY2soJ090aGVyIEVuZ2luZWVyaW5nIE1hdGVyaWFsICg4MDYzIG1hdGxzKScsJzI4NScpHxVnZBQrAAIWDB8ABRVQb2x5bWVyICg5NzYzNSBtYXRscykfEQUCMTAfEmgfEwsrBAIfFAVDamF2YXNjcmlwdDp1Y01hdEdyb3VwVHJlZV9Pbk5vZGVDbGljaygnUG9seW1lciAoOTc2MzUgbWF0bHMpJywnMTAnKR8VZ2QUKwACFgwfAAUYUHVyZSBFbGVtZW50ICg1MDcgbWF0bHMpHxEFAzE4NB8SaB8TCysEAh8UBUdqYXZhc2NyaXB0OnVjTWF0R3JvdXBUcmVlX09uTm9kZUNsaWNrKCdQdXJlIEVsZW1lbnQgKDUwNyBtYXRscyknLCcxODQnKR8VaGQUKwACFgwfAAUlV29vZCBhbmQgTmF0dXJhbCBQcm9kdWN0cyAoMzk4IG1hdGxzKR8RBQMyNzcfEmgfEwsrBAIfFAVUamF2YXNjcmlwdDp1Y01hdEdyb3VwVHJlZV9Pbk5vZGVDbGljaygnV29vZCBhbmQgTmF0dXJhbCBQcm9kdWN0cyAoMzk4IG1hdGxzKScsJzI3NycpHxVnZGQCBw8WAh4JaW5uZXJodG1sBSJBbHVtaW51bSBDYXN0aW5nIEFsbG95ICg0MDEgbWF0bHMpZAINDw8WAh4HVmlzaWJsZWhkZAIODw8WAh8XZ2QWBAIBDw8WAh8ABQM0MDFkZAIDDw8WAh8ABRZBbHVtaW51bSBDYXN0aW5nIEFsbG95ZGQCDw8PFgYeC0N1cnJlbnRQYWdlAgIeD0N1cnJlbnRTZWFyY2hJRALR94QTHxdnZBYEAgIPDxYCHwBlZGQCAw8PFgIfF2dkFjwCAQ8PFgIfAAUDNDAxZGQCAw8QZBAVAwExATIBMxUDATEBMgEzFCsDA2dnZxYBAgFkAgUPDxYCHwAFATNkZAIHDw8WCB8ABQtbUHJldiBQYWdlXR8OCiMeB0VuYWJsZWRnHxACBGRkAgkPDxYGHwAFC1tOZXh0IFBhZ2VdHw4KIx8QAgRkZAILDxBkZBYBAgVkAg8PFgIfF2hkAhEPEA9kFgIeCG9uY2hhbmdlBRNTZXRTZWxlY3RlZEZvbGRlcigpZGRkAhUPFgIfF2gWAgIBDxBkZBYBZmQCFw9kFgQCAQ8PFgQfAGUfGmhkZAICDw8WAh8XaGRkAhkPDxYEHwAFDU1hdGVyaWFsIE5hbWUfGmhkZAIbDw8WAh8XaGRkAh0PDxYCHxdoZGQCHw9kFgYCAQ8PFgQfAAUFUHJvcDEfGmhkZAIDDw8WAh8XaGRkAgUPDxYCHxdoZGQCIQ9kFgYCAQ8PFgQfAAUFUHJvcDIfGmhkZAIDDw8WAh8XaGRkAgUPDxYCHxdoZGQCIw9kFgYCAQ8PFgQfAAUFUHJvcDMfGmhkZAIDDw8WAh8XaGRkAgUPDxYCHxdoZGQCJQ9kFgYCAQ8PFgQfAAUFUHJvcDQfGmhkZAIDDw8WAh8XaGRkAgUPDxYCHxdoZGQCJw9kFgYCAQ8PFgQfAAUFUHJvcDUfGmhkZAIDDw8WAh8XaGRkAgUPDxYCHxdoZGQCKQ9kFgYCAQ8PFgQfAAUFUHJvcDYfGmhkZAIDDw8WAh8XaGRkAgUPDxYCHxdoZGQCKw9kFgYCAQ8PFgQfAAUFUHJvcDcfGmhkZAIDDw8WAh8XaGRkAgUPDxYCHxdoZGQCLQ9kFgYCAQ8PFgQfAAUFUHJvcDgfGmhkZAIDDw8WAh8XaGRkAgUPDxYCHxdoZGQCLw9kFgYCAQ8PFgQfAAUFUHJvcDkfGmhkZAIDDw8WAh8XaGRkAgUPDxYCHxdoZGQCMQ9kFgYCAQ8PFgQfAAUGUHJvcDEwHxpoZGQCAw8PFgIfF2hkZAIFDw8WAh8XaGRkAjUPDxYCHwAFAzQwMWRkAjcPEGQQFQMBMQEyATMVAwExATIBMxQrAwNnZ2cWAQIBZAI5Dw8WAh8ABQEzZGQCOw8PFggfAAULW1ByZXYgUGFnZV0fDgojHxpnHxACBGRkAj0PDxYGHwAFC1tOZXh0IFBhZ2VdHw4KIx8QAgRkZAI/DxBkZBYBAgVkAkEPFgIfF2cWAgIBDw8WAh8ABbgEPGJyIC8+DQpNYXRlcmlhbHMgZmxhZ2dlZCBhcyBkaXNjb250aW51ZWQgKDxpbWcgc3JjPSIvaW1hZ2VzL2J1dHRvbnMvaWNvbkRpc2NvbnRpbnVlZC5qcGciIGFsdD0iIiAvPikgYXJlIG5vIGxvbmdlciBwYXJ0IG9mIHRoZSBtYW51ZmFjdHVyZXLigJlzIHN0YW5kYXJkIHByb2R1Y3QgbGluZSBhY2NvcmRpbmcgdG8gb3VyIGxhdGVzdCBpbmZvcm1hdGlvbi4gIFRoZXNlIG1hdGVyaWFscyBtYXkgYmUgYXZhaWxhYmxlIGJ5IHNwZWNpYWwgb3JkZXIsIGluIGRpc3RyaWJ1dGlvbiBpbnZlbnRvcnksIG9yIHJlaW5zdGF0ZWQgYXMgYW4gYWN0aXZlIHByb2R1Y3QuICBEYXRhIHNoZWV0cyBmcm9tIG1hdGVyaWFscyB0aGF0IGFyZSBubyBsb25nZXIgYXZhaWxhYmxlIHJlbWFpbiBpbiBNYXRXZWIgdG8gYXNzaXN0IHVzZXJzIGluIGZpbmRpbmcgcmVwbGFjZW1lbnQgbWF0ZXJpYWxzLiAgDQo8YnIgLz48YnIgLz4NClVzZXJzIG9mIG91ciBBZHZhbmNlZCBTZWFyY2ggKHJlZ2lzdHJhdGlvbiByZXF1aXJlZCkgbWF5IGV4Y2x1ZGUgZGlzY29udGludWVkIG1hdGVyaWFscyBmcm9tIHNlYXJjaCByZXN1bHRzLmRkAhYPZBYCZg8WAh8ABXA8YSBocmVmPSIvY2xpY2t0aHJvdWdoLmFzcHg/YWRkYXRhaWQ9Mjc3IiBjbGFzcz0iZm9vdGxpbmsiPjxzcGFuIGNsYXNzPSJmb290Ij5UcmFkZSZuYnNwO1B1YmxpY2F0aW9uczwvc3Bhbj48L2E+ZBgBBR5fX0NvbnRyb2xzUmVxdWlyZVBvc3RCYWNrS2V5X18WBAU2Y3RsMDAkQ29udGVudE1haW4kVWNNYXRHcm91cEZpbmRlcjEkc2VsZWN0Q2F0ZWdvcnlMaXN0BStjdGwwMCRDb250ZW50TWFpbiR1Y01hdEdyb3VwVHJlZSRtc1RyZWVWaWV3BRtjdGwwMCRDb250ZW50TWFpbiRidG5TdWJtaXQFGmN0bDAwJENvbnRlbnRNYWluJGJ0blJlc2V0nD70PkTxiSsyg0+oaNSb7zEsg3k=" />
</div>

<script type="text/javascript">
//<![CDATA[
var theForm = document.forms['aspnetForm'];
if (!theForm) {
    theForm = document.aspnetForm;
}
function __doPostBack(eventTarget, eventArgument) {
    if (!theForm.onsubmit || (theForm.onsubmit() != false)) {
        theForm.__EVENTTARGET.value = eventTarget;
        theForm.__EVENTARGUMENT.value = eventArgument;
        theForm.submit();
    }
}
//]]>
</script>


<script src="/WebResource.axd?d=M68BSyLvwBMHRYzsSV62mvczr7w5VyKnq2JbgqcAAU1ioThlC6FNEVBncBdaA4oqf8cXPzy7QERzajvFfM67hSR6S0w1&amp;t=638313619312541215" type="text/javascript"></script>


<script src="/WebResource.axd?d=ClzPoKUn1iQnMGy7NeMJw70TvMayZ5w9nNmB7h_dY64i4BFFgjVcppyp69BFqhuzHctzc-rEantAjczzQ6UVMYkaWJw1&amp;t=638313619312541215" type="text/javascript"></script>
<script type="text/javascript">
//<![CDATA[

    function TreeView_PopulateNodeDoCallBack(context,param) {
        WebForm_DoCallback(context.data.treeViewID,param,TreeView_ProcessNodeData,context,TreeView_ProcessNodeData,false);
    }
var ctl00_ContentMain_ucMatGroupTree_msTreeView_Data = null;//]]>
</script>

<script src="/ScriptResource.axd?d=148LWaywfmset9PYVR0m-KYXX9JfyGYGqOtIYBGEAzoJh5VhFBwSI6JxnmoNvgtlAprhvntpc6lTDbRMgdKxckUmc8m2qw_fOCsX0G9LC8ojUpw4cTHWXgfuT_vU6m2Kpv3mt01iCczrTE2-J5X6EeBa2281&amp;t=637769794779625837" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=v_V1UgDrDAchg8Zuc1FL4qkvmjt-bwap7ZxwVGGPleYiJ6zfDOTL6wtCmVvILjbwo_sqb2PMUTNkjO4vp6nd-E7WWXTPgSI5-nvn3YDnHrVk5bgVX4Qv6F0fV4wTSslmaLZ3k98tckojDLWAEI7jm8h0H4d7BoGYxkqsyh8-Iujfrjjm0&amp;t=637769794779625837" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=V7Zma3AISNyGTbHVRPw8svJ4Ub7xUUaeoKwImcvnuDiy-XBR331l3eTjiEP8J_QlX2qgyAHxZFLywNbNyZ50fJLVEOCjx11lIDJOiQxc0oGA3qi7XXiggiwEcIWwzb9j2Is1n1D9sU40Zusk35AtkqRKL3Y1&amp;t=633177911400000000" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=wOguQo_yd3hCG4ajcSXPC6-XdeL0hmYWSMaFjvMXt1CBqR9neJqTXyTfPki4lIz4B29Zz_83btvS12BY6KpBKT-AgP0eaXp7V3h6XRdjiozV5w_n3oQRZg5Yq1QW8UNGj0Cy9jsap_7EcU2M99CapUyVG6I1&amp;t=633177911400000000" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=MAJxSy62Z3EtRwHXuuMc8KC6PFmrPqmkpcwdpcPGeqtqUQyJ3FBQ3cFyYWN1zNID_ngA53aMZdG_dpRu3TRDH8-iqKAjPAcc2BWe63WYGrGbyHc9syJxR6lypTgvRaYmwwEmkLEsaGUniMurHc16yr1VrjmzG3HWOeu9UXNr23Wv2xzh0&amp;t=633177911400000000" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=ukx9eyyt483y_CGgCF1OToQPWzPYyCynhpMvhxN7idzW3WaKjHjpgAOcMgtXZIEcspnbyDEADdRWCSHB-hF39Ycl13KWsqdlOHElw2qgSkGmkL8iNH37htb0BNS0lxw6oHvtqYiVpw-nZeYoQ1NkgX2qXOI1&amp;t=633177911400000000" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=Ovkr-MWDypjnq0C7FVbNgOs-Mt3NaktNVf8SEOX721PoQ5mQqTM6S4igSQL1_PXvh96km3stLiqrorOgm8nnnUU6y6edTkhxmLiwfFtEXQEAAnFFQpUepgsxEd0ahMvWcDfEac0EE7J8iDb2rqSMTxk51ydfY-c-8zcKvo5Mrn83cvfN0&amp;t=633177911400000000" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=pKQF4iQYwkSn5cAwTNDM8ez4TLepowWnai-WD6NVL5h0ay9Qtt0a9vPCaI-KN9OHcv1fL8VnwxEDM6EAVtgZY06V8XJTlVrbmb8WlMvGmhwBeL3_SVtKKOS-QExY8AnVbTFl_wOknplhy8bz-9sIWNCh4XPq1WhRj7ALNAKaeruPhS7d0&amp;t=633177911400000000" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=uM6eUuIuIa_NiCgVrtwgqczDeqTqI37XalY8Ri_W2ZjXEzIKi84afVBjFk8gneIDXcFnw8yjCD1qhE8mzDTzAUSdKnbDte0yC23kMoyvxPyu7vRH9E-6JMZtE8-gELUPmoZdOZJ9nXitHKYBLw2P6skU1IkzFRegK74bETu2FdEtuaeR0&amp;t=633177911400000000" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=ewF5FMb1ckbNCdJ9rpE5xMSXy-WecqVGE8uma1_520KusuW7jTUE2tnPMG7JyR0FDIFqDUMQbsfTZlsQ0sD4YAW8MnN90GKIMPvzaP7eOG3ss8E050_l7Rpnaz7I_U3ITzBXkcJZIOPcI1PxOReT8FSeWmo1&amp;t=633177911400000000" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=3lB_EBCMPyiwzDhzWdoE-i4mLHhkIMbbkaLwMr-U1WcAoDqBHS-oYFn4gz3ZwnpzK_KZp97Ev8ZbiUNo7WMJ45h2xgOH59erMFo1BpNb87-yu3X0mFyWs5rB3ANm4lsVSIu7RZd7XUEwfZXzugJ3CoJqxvRqiTGLpcXquEF24hK3OUH-0&amp;t=633177911400000000" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=Z7-pZSKJ6vCyr9H9S4r9baB7TLiJ-T4EoMOvlH-iQ5-icaO-vWMrjXk3EPctuPrcO5qwJSTaLQtHyguwle7Kty78exrSwHataIriNUEu1DnUkixv4pxU7Dknp_GDVHby5XcMA4lMwazAE0AFG9Cndu0An_gAGACGlYXsGUG9W6okCQsH0&amp;t=633177911400000000" type="text/javascript"></script>
<script src="../WebServices/Materials.asmx/js" type="text/javascript"></script>
<script src="../WebServices/MatGroups.asmx/js" type="text/javascript"></script>
<script src="../WebServices/Folders.asmx/js" type="text/javascript"></script>
<script type="text/javascript">
//<![CDATA[
function WebForm_OnSubmit() {
null;
return true;
}
//]]>
</script>

<div>

	<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="640EFF8C" />
</div>

<!--
The ScriptManager control is required for all ASP AJAX functionality - especially used by search pages.
It generates the javascript required.
Only one script manager is allowed on a page, and since this is the master page, this is the main script manager
However, a ScriptManagerProxy control can be used to set properties and settings on indivual pages.
For an example, see /WebControls/ucSearchResults.ascx
-->
<script type="text/javascript">
//<![CDATA[
Sys.WebForms.PageRequestManager._initialize('ctl00$ajxScriptManager', document.getElementById('aspnetForm'));
Sys.WebForms.PageRequestManager.getInstance()._updateControls([], [], [], 90);
//]]>
</script>


<table class="tabletight" style="width:100%; text-align:center;background-image: url(/images/gray.gif);">
  <tr >
    <td rowspan="2"><a href="/index.aspx"><img src="/images/logo1.gif" width="270" height="42" alt="MatWeb - Material Property Data"  /></a></td>

                
                <td style="text-align:right;vertical-align:middle; width:90%" ><span class='greeting'><span class='hiText'><a href ='/services/advertising.aspx'>Advertise with MatWeb!</a></span>&nbsp;&nbsp;&nbsp;&nbsp;</span></td>
                <td style="text-align:right;vertical-align:middle; padding-right:5px;" ><a href='/membership/regstart.aspx'><img src='/images/buttons/btnRegisterBSF.gif' width="62" height="20" alt="Register Now" /></a></td>

                
  </tr>
  <tr >
    <td  align="left" valign="bottom" colspan="2"><span class="tagline">
    
    Data sheets for over <span class="greeting">180,000</span>  metals, plastics, ceramics, and composites.</span></td>
  </tr>
  <tr style="background-image:url(/images/blue.gif); background-color:#000099; " >
    <td bgcolor="#000099"><a href="/index.aspx"><img src="/images/logo2.gif" width="270" height="23px" alt="MatWeb - Material Property Data" border="0" /></a></td>
    <td class="topnavig8" style=" background-color:#000099;vertical-align:middle; text-align:right; white-space:nowrap;" colspan="2" align="right" valign="middle">
        <a href="/index.aspx" class="topnavlink">HOME</a>&nbsp;&nbsp;&#8226;&nbsp;&nbsp;
                        <a href="/search/search.aspx" class="topnavlink">SEARCH</a>&nbsp;&nbsp;&#8226;&nbsp;&nbsp;
                        <a href="/tools/tools.aspx" class="topnavlink">TOOLS</a>&nbsp;&nbsp;&#8226;&nbsp;&nbsp;
                        <a href="/reference/suppliers.aspx" class="topnavlink">SUPPLIERS</a>&nbsp;&nbsp;&#8226;&nbsp;&nbsp;
                        <a href="/folders/ListFolders.aspx" class="topnavlink">FOLDERS</a>&nbsp;&nbsp;&#8226;&nbsp;&nbsp;
                        <a href="/services/services.aspx" class="topnavlink">ABOUT US</a>&nbsp;&nbsp;&#8226;&nbsp;&nbsp;
                        <a href="/reference/faq.aspx" class="topnavlink">FAQ</a>&nbsp;&nbsp;&#8226;&nbsp;&nbsp;
                        
                        <a href="/membership/login.aspx" class="topnavlink">LOG IN</a>
                        
                        &nbsp;
                        
                        &nbsp;
    </td>
  </tr>
</table>

<div id="divRecentMatls" class="tableborder tight" style=" display:none; position:absolute; top:95px; left:150px; width:400px; height:400px;" onmouseover="clearTimeout(tmrtmp);" onmouseout="tmrtmp=setTimeout('RecentMatls.Hide()', 800)">
        <table class="headerblock tabletight" style="vertical-align:top; font-size:13px; width:100%;"><tr>
                <td style="padding:3px;"> Recently Viewed Materials (most recent at top)</td>
                <td style="padding:3px;text-align:right;"><input type="image" src="/images/buttons/btnCloseWhite.gif" onclick="RecentMatls.Hide();return false;" style="cursor:pointer;" />&nbsp;</td>
        </tr></table>
        <div id="divRecentMatlsBody" style="height:375px; overflow:auto; background-color:White;">

                <table id="tblRecentMatls" class="tabledataformat tableloose" style="background-color:White;">
    
                <tr><td>
                <p />
                <a href="/membership/login.aspx">Login</a> to see your most recently viewed materials here.<p />
                Or if you don't have an account with us yet, then <a href="/membership/regstart.aspx">click here to register.</a>
                </td></tr>
                
                </table>

        </div>
</div>

<script type="text/javascript">

        var RecentMatls = new _RecentMatls();
        var tmrtmp;

        function _RecentMatls(){

                var divRecentMatls;
                var tblRecentMatls;
                var trRecentMatlsRowTemplate;
                var trRecentMatlsNoData;
                var DataIsLoaded=false;

                //Using prototype.js methods...
                divRecentMatls = $("divRecentMatls");
                tblRecentMatls = $("tblRecentMatls");
    

                divRecentMatls.style.display = "none";

                this.Toggle = function(){
                        //alert("Toggle()");
                        if(divRecentMatls.style.display == "block"){this.Hide();}
                        else{this.Show();}
                }

                this.Show = function(){
                        //alert("Show()");
                        divRecentMatls.style.display = "block";
                        matweb.toggleSelect(divRecentMatls,0);
                        if(!DataIsLoaded){
            
                        }
                }

                this.Hide = function(){
                        //alert("Hide()");
                        divRecentMatls.style.display = "none";
                        matweb.toggleSelect(divRecentMatls,1);
                }

    

        }//end class

</script>

<table class="tabletight t_ableborder t_ablegrid" style="width:100%;" >
  <tr>
   <td style="width:100%; height:27px; white-space:nowrap; text-align:left; vertical-align:middle;" class="tagline" >
      <span class="navlink"><b>&nbsp;&nbsp;Searches:</b></span>
      &nbsp;&nbsp;<a href="/search/AdvancedSearch.aspx" class="navlink"><span class="nav"><font color="#CC0000">Advanced</font></span></a>
      &nbsp;|&nbsp;<a href="/search/MaterialGroupSearch.aspx" class="navlink"><span class="nav">Category</span></a>
      &nbsp;|&nbsp;<a href="/search/PropertySearch.aspx" class="navlink"><span class="nav">Property</span></a>
      &nbsp;|&nbsp;<a href="/search/CompositionSearch.aspx" class="navlink"><span class="nav">Metals</span></a>
      &nbsp;|&nbsp;<a href="/search/SearchTradeName.aspx" class="navlink"><span class="nav">Trade Name</span></a>
      &nbsp;|&nbsp;<a href="/search/SearchManufacturerName.aspx" class="navlink"><span class="nav">Manufacturer</span></a>
                &nbsp;|&nbsp;<a href="javascript:RecentMatls.Toggle();" id="ctl00_lnkRecentMatls" class="navlink"><span class="nav"><font color="#CC0000">Recently Viewed Materials</font></span></a>
    </td>

    <td style="text-align:right; vertical-align:bottom; padding-right:5px;">

      <table class="tabletight t_ableborder" ><tr>
        <td style=" white-space:nowrap; vertical-align:bottom; " >
                                        &nbsp;&nbsp;
                                        <input name="ctl00$txtQuickText" type="text" id="ctl00_txtQuickText" style="width:100px;" maxlength="40" class="quicksearchinput" onkeydown="txtQuickText_OnKeyDown(event);" />
                                        <input type="image" id="btnQuickTextSearch" style="vertical-align:top; width:60px; height:25px;" src="/images/buttons/btnSearch.gif" alt="Search" onclick="return btnQuickTextSearch_Click()"  />
                                        <script type="text/javascript">

                                                //SETUP THE AUTOPOST ON ENTER KEY EVENT FOR txtQuickText FIELD
                                                //document.getElementById("ctl00_txtQuickText").onkeydown=txtQuickText_OnKeyDown;

                                                function txtQuickText_OnKeyDown(evt){
                                                        //SUBMIT THE FORM AS IF THE BUTTON WAS PUSHED
                                                        //var UniqueID = "";
                                                        var txtQuickText = document.getElementById('ctl00_txtQuickText');
                                                        if(matweb.IsEnterKey(evt))
                                                                if(checkSearchText(txtQuickText))
                                                                        //matweb.DoPostBack(UniqueID, "");
                                                                        window.location.href="/search/QuickText.aspx?SearchText=" + escape(txtQuickText.value);
                                                        return false;
                                                }

                                                function btnQuickTextSearch_Click(){
                                                        var txtQuickText = document.getElementById('ctl00_txtQuickText');
                                                        if(checkSearchText(txtQuickText))
                                                                window.location.href="/search/QuickText.aspx?SearchText=" + escape(txtQuickText.value);
                                                        return false;
                                                }

                                                function checkSearchText(searchbox){
                                                        searchbox.value = trim(searchbox.value);
                                                        if(searchbox.value == ''){
                                                                //alert('Please enter something to search for');
                                                                matweb.alert('Please enter something to search for', 'Search Error');
                                                                //searchbox.focus();
                                                                return false;
                                                        }
                                                        return true;
                                                }

                                                function trim(s)  { return ltrim(rtrim(s)) }
                                                function ltrim(s) { return s.replace(/^\s+/g, "") }
                                                function rtrim(s) { return s.replace(/\s+$/g, "") }

                                        </script>
                                </td>
                                </tr>
                        </table>

    </td>
  </tr>
  <tr style="background-image:url(/images/shad.gif);height:7px;" ><td colspan="2"></td></tr>
</table>

<!-- ====================================== MAIN CONTENT ============================================= -->
<div style="vertical-align:top; padding-left:10px; padding-right:10px;" >





	<center><a href="/clickthrough.aspx?addataid=1349"><img src="/images/assets/metalmen-Aluminum.png" border=0 alt="Metalmen Sales" width = "728" height = "90"></a>
</center>
	
	<h2><a href="#help"><img src="/images/buttons/iconHelp.gif" alt="" style="vertical-align:bottom;" /></a>Material Category Search</h2>
	
	

<div id="ctl00_ContentMain_ucPopupMessage1_divPopupMessage" class="divPopupAlert" style="display:none;width:250px;">
	<table >
		<tr id="ctl00_ContentMain_ucPopupMessage1_trTitleRow" class="divPopupAlert_TitleRow" style="cursor:move;">
	<th><span id="ctl00_ContentMain_ucPopupMessage1_lblTitle">User Input Problem</span></th>
</tr>

		<tr class="divPopupAlert_ContentRow" ><td style="" ><span id="ctl00_ContentMain_ucPopupMessage1_lblMessage"></span></td></tr>
		<tr class="divPopupAlert_ButtonRow"><td><input name="ctl00$ContentMain$ucPopupMessage1$btnOK" type="button" id="ctl00_ContentMain_ucPopupMessage1_btnOK" value="OK" /></td></tr>
	</table>
</div>

<input type="hidden" name="ctl00$ContentMain$ucPopupMessage1$hndPopupControl" id="ctl00_ContentMain_ucPopupMessage1_hndPopupControl" />


	<table class="" style="width:100%;">
	<tr>
		<td style=" vertical-align:top; width:300px;" >
			<strong>Find a Material Category:</strong>
			

<input name="ctl00$ContentMain$UcMatGroupFinder1$txtSearchText" type="text" value="alumi" id="ctl00_ContentMain_UcMatGroupFinder1_txtSearchText" style="Width:304px;" /><br />
<select name="ctl00$ContentMain$UcMatGroupFinder1$selectCategoryList" id="ctl00_ContentMain_UcMatGroupFinder1_selectCategoryList" style="position:absolute;display:none;Width:308px;" size="18">
</select>

<input type="hidden" name="ctl00$ContentMain$UcMatGroupFinder1$TextBoxWatermarkExtender2_ClientState" id="ctl00_ContentMain_UcMatGroupFinder1_TextBoxWatermarkExtender2_ClientState" />

<script type="text/javascript">

var ucMatGroupFinderManager = new _ucMatGroupFinderManager("ctl00_ContentMain_UcMatGroupFinder1_txtSearchText","ctl00_ContentMain_UcMatGroupFinder1_selectCategoryList");

function _ucMatGroupFinderManager(txtSearchTextID,selectCategoryListID){

	var txtSearchText = document.getElementById(txtSearchTextID);
	var selectCategoryList = document.getElementById(selectCategoryListID);
	var showtime;
	var blurtime;
	var MinTextLength = 4;
	var KeyStrokeActionDelay = 650;
	var FadeInOutDelay = 0.2;
	var LastUsedText;
	
	//The init function is currently run at the bottom of this class
	this.init=function(){

		txtSearchText.onfocus = txtSearchText_onfocus;
		txtSearchText.onkeyup = txtSearchText_checktext;
		txtSearchText.onblur = selectCategoryList_startblur;
		
		selectCategoryList.onfocus = selectCategoryList_clearblur;
		selectCategoryList.onchange = selectCategoryList_onchange;
		selectCategoryList.onblur = selectCategoryList_startblur;

	}

	txtSearchText_onfocus = function(){
		if(txtSearchText.value == "Type at least 4 characters here...")
			txtSearchText.value = "";
		selectCategoryList_clearblur();
		txtSearchText_checktext();
	}

	function txtSearchText_checktext(){
		searchText = txtSearchText.value;
		//alert(searchText);
		clearTimeout(showtime);
		if(searchText.length >= MinTextLength){
			showtime = setTimeout(function(){
				selectCategoryList.style.display = "block";
				if(txtSearchText.value!=LastUsedText){
					LastUsedText = searchText;
					aci.matweb.WebServices.MatGroups.FindMatGroups(searchText,DisplayCatData,matweb.WebServiceErrorHandler,"txtSearchText_checktext");
				}
				//AjaxControlToolkit.Animation.FadeInAnimation.play(selectCategoryList, FadeInOutDelay, 1, 25, 0, 1, true);
			},KeyStrokeActionDelay);
		}
		else{
			LastUsedText = "";
			showtime = setTimeout(function(){
				selectCategoryList.options.length = 0;
				selectCategoryList.style.display = "none";
				//AjaxControlToolkit.Animation.FadeOutAnimation.play(selectCategoryList,FadeInOutDelay);
				//AjaxControlToolkit.Animation.FadeOutAnimation.play(selectCategoryList,FadeInOutDelay, 25, 0, 1, true);
			},KeyStrokeActionDelay);
		}
	}

	function selectCategoryList_clearblur(){
		clearTimeout(blurtime);
	}

	function selectCategoryList_onchange(){
		var MatGroupID = selectCategoryList.options[selectCategoryList.selectedIndex].value;
		var MatGroupText = selectCategoryList.options[selectCategoryList.selectedIndex].text;
		//alert("Selected MatGroupID: " + MatGroupID);
		//alert("Selected MatGroupText: " + MatGroupText);
		//When no data is found, we have a "No Categories Contain Text..." message, but the MatGroupID is set to 0, so we can ignore if MatGroupID == 0.
		if(MatGroupID > 0){
			ucMatGroupFinder1_OnSelected(MatGroupID,MatGroupText)
		}
		selectCategoryList_startblur();
		//txtSearchText.value = "";
	}
	
	function selectCategoryList_startblur(){
		//alert("selectCategoryList_onblur()");
		clearTimeout(blurtime);
		blurtime = setTimeout(function(){
			selectCategoryList.style.display = "none";
			//AjaxControlToolkit.Animation.FadeOutAnimation.play(selectCategoryList,FadeInOutDelay, 25, 0, 1, true);
		},KeyStrokeActionDelay);
	}
	
	function DisplayCatData(MatGroupData){
		//alert("GetCatData()");
		//alert("vwMaterialGroupList: " + vwMaterialGroupList);
		var MatGroupID,GroupName, MatCount;
		selectCategoryList.options.length = 0;//clear any existing options....
		if(MatGroupData.length==0){
				//alert(vwMaterialGroupList[i].GroupName);
				MatGroupID = 0;
				GroupName = "No categories contain the text: " + txtSearchText.value
				selectCategoryList.options[0] = new Option(GroupName,MatGroupID);					
		}
		else{
			for(i=0;i<MatGroupData.length;i++){
				//alert(vwMaterialGroupList[i].GroupName);
				MatCount = MatGroupData[i].MatCount;
				MatGroupID = MatGroupData[i].MatGroupID;
				GroupName = MatGroupData[i].GroupName + " (" + MatCount + " matls)";
				selectCategoryList.options[i] = new Option(GroupName,MatGroupID);					
			}
		}
	}
	
	this.init();

}//end class
	
</script>



			<strong>Select a Material Category:</strong>
			
			<div class="tableborder" style=" height:225px; width:300px; overflow:scroll; ">
			<a href="#ctl00_ContentMain_ucMatGroupTree_msTreeView_SkipLink"><img alt="Skip Navigation Links." src="/WebResource.axd?d=vAkieGj3ugBpu6CKp7dqvCP5iHLzPbUd2XME-h1vQcC6d5hLg7Vc1kNVzB8A45Ui3K9XZ9m3-E2pxsaJQYh2uMkAul41&amp;t=638313619312541215" width="0" height="0" style="border-width:0px;" /></a><div id="ctl00_ContentMain_ucMatGroupTree_msTreeView">
	<table cellpadding="0" cellspacing="0" style="border-width:0;">
		<tr>
			<td><a id="ctl00_ContentMain_ucMatGroupTree_msTreeViewn0" href="javascript:TreeView_PopulateNode(ctl00_ContentMain_ucMatGroupTree_msTreeView_Data,0,document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewn0'),document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewt0'),null,'r','Carbon (866 matls)','283','f','','f')"><img src="/WebResource.axd?d=6v5qM6VKQYD3XNwwXEy7Tix7dzVdqDJ8QTRIlUPrzXQqNsXQgUwIfmn34ZzRORd_IZj0bF4vkZzID1HuSFE5uKyLgaPYeAYE8VT8HYfb3Rk6zsDw0&amp;t=638313619312541215" alt="Expand Carbon (866 matls)" style="border-width:0;" /></a></td><td class="ctl00_ContentMain_ucMatGroupTree_msTreeView_2"><a class="ctl00_ContentMain_ucMatGroupTree_msTreeView_0 ctl00_ContentMain_ucMatGroupTree_msTreeView_1" href="javascript:ucMatGroupTree_OnNodeClick('Carbon (866 matls)','283')" id="ctl00_ContentMain_ucMatGroupTree_msTreeViewt0">Carbon (866 matls)</a></td>
		</tr><tr style="height:0px;">
			<td></td>
		</tr>
	</table><table cellpadding="0" cellspacing="0" style="border-width:0;">
		<tr style="height:0px;">
			<td></td>
		</tr><tr>
			<td><a id="ctl00_ContentMain_ucMatGroupTree_msTreeViewn1" href="javascript:TreeView_PopulateNode(ctl00_ContentMain_ucMatGroupTree_msTreeView_Data,1,document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewn1'),document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewt1'),null,'t','Ceramic (10004 matls)','11','f','','f')"><img src="/WebResource.axd?d=TinC6igX1vPDgWh7DstWwBTHTyH4uuybDU4NW47jJD06wHdAO3YyJC_eKK1Zn5oIbwqRo4Cri8YwhA82G-LD0p14yEF1EfoIZvKleYMIuQ2K1hkE0&amp;t=638313619312541215" alt="Expand Ceramic (10004 matls)" style="border-width:0;" /></a></td><td class="ctl00_ContentMain_ucMatGroupTree_msTreeView_2"><a class="ctl00_ContentMain_ucMatGroupTree_msTreeView_0 ctl00_ContentMain_ucMatGroupTree_msTreeView_1" href="javascript:ucMatGroupTree_OnNodeClick('Ceramic (10004 matls)','11')" id="ctl00_ContentMain_ucMatGroupTree_msTreeViewt1">Ceramic (10004 matls)</a></td>
		</tr><tr style="height:0px;">
			<td></td>
		</tr>
	</table><table cellpadding="0" cellspacing="0" style="border-width:0;">
		<tr style="height:0px;">
			<td></td>
		</tr><tr>
			<td><a id="ctl00_ContentMain_ucMatGroupTree_msTreeViewn2" href="javascript:TreeView_PopulateNode(ctl00_ContentMain_ucMatGroupTree_msTreeView_Data,2,document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewn2'),document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewt2'),null,'t','Fluid (7562 matls)','5','f','','f')"><img src="/WebResource.axd?d=TinC6igX1vPDgWh7DstWwBTHTyH4uuybDU4NW47jJD06wHdAO3YyJC_eKK1Zn5oIbwqRo4Cri8YwhA82G-LD0p14yEF1EfoIZvKleYMIuQ2K1hkE0&amp;t=638313619312541215" alt="Expand Fluid (7562 matls)" style="border-width:0;" /></a></td><td class="ctl00_ContentMain_ucMatGroupTree_msTreeView_2"><a class="ctl00_ContentMain_ucMatGroupTree_msTreeView_0 ctl00_ContentMain_ucMatGroupTree_msTreeView_1" href="javascript:ucMatGroupTree_OnNodeClick('Fluid (7562 matls)','5')" id="ctl00_ContentMain_ucMatGroupTree_msTreeViewt2">Fluid (7562 matls)</a></td>
		</tr><tr style="height:0px;">
			<td></td>
		</tr>
	</table><table cellpadding="0" cellspacing="0" style="border-width:0;">
		<tr style="height:0px;">
			<td></td>
		</tr><tr>
			<td><a id="ctl00_ContentMain_ucMatGroupTree_msTreeViewn3" href="javascript:TreeView_PopulateNode(ctl00_ContentMain_ucMatGroupTree_msTreeView_Data,3,document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewn3'),document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewt3'),null,'t','Metal (17052 matls)','9','f','','f')"><img src="/WebResource.axd?d=TinC6igX1vPDgWh7DstWwBTHTyH4uuybDU4NW47jJD06wHdAO3YyJC_eKK1Zn5oIbwqRo4Cri8YwhA82G-LD0p14yEF1EfoIZvKleYMIuQ2K1hkE0&amp;t=638313619312541215" alt="Expand Metal (17052 matls)" style="border-width:0;" /></a></td><td class="ctl00_ContentMain_ucMatGroupTree_msTreeView_2"><a class="ctl00_ContentMain_ucMatGroupTree_msTreeView_0 ctl00_ContentMain_ucMatGroupTree_msTreeView_1" href="javascript:ucMatGroupTree_OnNodeClick('Metal (17052 matls)','9')" id="ctl00_ContentMain_ucMatGroupTree_msTreeViewt3">Metal (17052 matls)</a></td>
		</tr><tr style="height:0px;">
			<td></td>
		</tr>
	</table><table cellpadding="0" cellspacing="0" style="border-width:0;">
		<tr style="height:0px;">
			<td></td>
		</tr><tr>
			<td><a id="ctl00_ContentMain_ucMatGroupTree_msTreeViewn4" href="javascript:TreeView_PopulateNode(ctl00_ContentMain_ucMatGroupTree_msTreeView_Data,4,document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewn4'),document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewt4'),null,'t','Other Engineering Material (8063 matls)','285','f','','f')"><img src="/WebResource.axd?d=TinC6igX1vPDgWh7DstWwBTHTyH4uuybDU4NW47jJD06wHdAO3YyJC_eKK1Zn5oIbwqRo4Cri8YwhA82G-LD0p14yEF1EfoIZvKleYMIuQ2K1hkE0&amp;t=638313619312541215" alt="Expand Other Engineering Material (8063 matls)" style="border-width:0;" /></a></td><td class="ctl00_ContentMain_ucMatGroupTree_msTreeView_2"><a class="ctl00_ContentMain_ucMatGroupTree_msTreeView_0 ctl00_ContentMain_ucMatGroupTree_msTreeView_1" href="javascript:ucMatGroupTree_OnNodeClick('Other Engineering Material (8063 matls)','285')" id="ctl00_ContentMain_ucMatGroupTree_msTreeViewt4">Other Engineering Material (8063 matls)</a></td>
		</tr><tr style="height:0px;">
			<td></td>
		</tr>
	</table><table cellpadding="0" cellspacing="0" style="border-width:0;">
		<tr style="height:0px;">
			<td></td>
		</tr><tr>
			<td><a id="ctl00_ContentMain_ucMatGroupTree_msTreeViewn5" href="javascript:TreeView_PopulateNode(ctl00_ContentMain_ucMatGroupTree_msTreeView_Data,5,document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewn5'),document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewt5'),null,'t','Polymer (97635 matls)','10','f','','f')"><img src="/WebResource.axd?d=TinC6igX1vPDgWh7DstWwBTHTyH4uuybDU4NW47jJD06wHdAO3YyJC_eKK1Zn5oIbwqRo4Cri8YwhA82G-LD0p14yEF1EfoIZvKleYMIuQ2K1hkE0&amp;t=638313619312541215" alt="Expand Polymer (97635 matls)" style="border-width:0;" /></a></td><td class="ctl00_ContentMain_ucMatGroupTree_msTreeView_2"><a class="ctl00_ContentMain_ucMatGroupTree_msTreeView_0 ctl00_ContentMain_ucMatGroupTree_msTreeView_1" href="javascript:ucMatGroupTree_OnNodeClick('Polymer (97635 matls)','10')" id="ctl00_ContentMain_ucMatGroupTree_msTreeViewt5">Polymer (97635 matls)</a></td>
		</tr><tr style="height:0px;">
			<td></td>
		</tr>
	</table><table cellpadding="0" cellspacing="0" style="border-width:0;">
		<tr style="height:0px;">
			<td></td>
		</tr><tr>
			<td><img src="/WebResource.axd?d=7_V5n1KYSQsj6PoSqgIxb0egBsAa91jSXPkbmM6FaiF14-JsNnpUDnfSdxZcvfd0OQCLDtC24pMIXigO-41cL2rsq-0xea-SSG7yZUyRcliFFqe70&amp;t=638313619312541215" alt="" /></td><td class="ctl00_ContentMain_ucMatGroupTree_msTreeView_2"><a class="ctl00_ContentMain_ucMatGroupTree_msTreeView_0 ctl00_ContentMain_ucMatGroupTree_msTreeView_1" href="javascript:ucMatGroupTree_OnNodeClick('Pure Element (507 matls)','184')" id="ctl00_ContentMain_ucMatGroupTree_msTreeViewt6">Pure Element (507 matls)</a></td>
		</tr><tr style="height:0px;">
			<td></td>
		</tr>
	</table><table cellpadding="0" cellspacing="0" style="border-width:0;">
		<tr style="height:0px;">
			<td></td>
		</tr><tr>
			<td><a id="ctl00_ContentMain_ucMatGroupTree_msTreeViewn7" href="javascript:TreeView_PopulateNode(ctl00_ContentMain_ucMatGroupTree_msTreeView_Data,7,document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewn7'),document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewt7'),null,'l','Wood and Natural Products (398 matls)','277','f','','t')"><img src="/WebResource.axd?d=vUBrM2nNhECNbFO0AJKq52p2zWrY5XG8nWgCDL4AfsrexStb0HREDV6GHMvdkAGDInxYtTyWXuxFUIiQv3JX2fScVRwQsrVv5srHrjOIeC3ayAG20&amp;t=638313619312541215" alt="Expand Wood and Natural Products (398 matls)" style="border-width:0;" /></a></td><td class="ctl00_ContentMain_ucMatGroupTree_msTreeView_2"><a class="ctl00_ContentMain_ucMatGroupTree_msTreeView_0 ctl00_ContentMain_ucMatGroupTree_msTreeView_1" href="javascript:ucMatGroupTree_OnNodeClick('Wood and Natural Products (398 matls)','277')" id="ctl00_ContentMain_ucMatGroupTree_msTreeViewt7">Wood and Natural Products (398 matls)</a></td>
		</tr><tr style="height:0px;">
			<td></td>
		</tr>
	</table>
</div><a id="ctl00_ContentMain_ucMatGroupTree_msTreeView_SkipLink"></a>
<span id="ctl00_ContentMain_ucMatGroupTree_lblDebug"></span>

			</div>
			
			<strong>Selected Material Category: </strong>
			<div id="ctl00_ContentMain_divMatGroupName" class="selectedItem" style="width:306px;">Aluminum Casting Alloy (401 matls)</div>
			<input name="ctl00$ContentMain$txtMatGroupID" type="hidden" id="ctl00_ContentMain_txtMatGroupID" value="208" />
			<input name="ctl00$ContentMain$txtMatGroupText" type="hidden" id="ctl00_ContentMain_txtMatGroupText" value="Aluminum Casting Alloy (401 matls)" />
			
			

			<input type="image" name="ctl00$ContentMain$btnSubmit" id="ctl00_ContentMain_btnSubmit" src="../images/buttons/btnFind.gif" alt="Find" style="border-width:0px;" /> <!-- OnClientClick="return validate()" -->
			<input type="image" name="ctl00$ContentMain$btnReset" id="ctl00_ContentMain_btnReset" title="Clear Search" src="/images/buttons/btnReset.gif" alt="Clear Search" style="border-width:0px;" />

			<span id="ctl00_ContentMain_lblDebug"></span>

		</td>
		<td>&nbsp;</td>
		<td style=" vertical-align:top; width:80%;" >
			<strong>Search Results</strong>
			<div class="tableborder" id="divSearchResults" style="padding:5px;" >
				
				<div id="ctl00_ContentMain_pnlSearchResults">
	
					There are <span id="ctl00_ContentMain_lblMatlCount">401</span> materials in the category 
					<span id="ctl00_ContentMain_lblMaterialGroupName" class="selectedItem">Aluminum Casting Alloy</span>.
					If your material is not listed, please refer to our <a href="/help/strategy.aspx">search strategy</a> page for assistance in limiting your search.
				
</div>
			</div>
			<p />




<div id="ctl00_ContentMain_UcSearchResults1_pnlResultsFound" class="tableborder">
	

	<table style="background-color:Silver; width:100%;" class="" >
		<tr>
			<td><strong>Found <span id="ctl00_ContentMain_UcSearchResults1_lblResultCount">401</span> Results</strong> -- 
				Page 
				<select name="ctl00$ContentMain$UcSearchResults1$drpPageSelect1" onchange="javascript:setTimeout('__doPostBack(\'ctl00$ContentMain$UcSearchResults1$drpPageSelect1\',\'\')', 0)" id="ctl00_ContentMain_UcSearchResults1_drpPageSelect1">
		<option value="1">1</option>
		<option selected="selected" value="2">2</option>
		<option value="3">3</option>

	</select>
				of <span id="ctl00_ContentMain_UcSearchResults1_lblPageTotal">3</span> 
				-- 
				<a id="ctl00_ContentMain_UcSearchResults1_lnkPrevPage" href="javascript:__doPostBack('ctl00$ContentMain$UcSearchResults1$lnkPrevPage','')" style="color:Black;">[Prev Page]</a>
				<a id="ctl00_ContentMain_UcSearchResults1_lnkNextPage" href="javascript:__doPostBack('ctl00$ContentMain$UcSearchResults1$lnkNextPage','')" style="color:Black;">[Next Page]</a>
				--&nbsp;			
				view
				<select name="ctl00$ContentMain$UcSearchResults1$drpPageSize1" onchange="javascript:setTimeout('__doPostBack(\'ctl00$ContentMain$UcSearchResults1$drpPageSize1\',\'\')', 0)" id="ctl00_ContentMain_UcSearchResults1_drpPageSize1">
		<option value="25">25</option>
		<option value="50">50</option>
		<option value="75">75</option>
		<option value="100">100</option>
		<option value="150">150</option>
		<option selected="selected" value="200">200</option>

	</select> per page
			</td>
		</tr>
		
	</table>
	
	
	
	
	<table class="t_ablegrid" style="width:100%;">
		<tr style="font-size:9px;">
			<td style="font-size:9px;">Use Folder</td>
			<td style="font-size:9px;">Contains</td>
			<td style="font-size:9px;">&nbsp;</td>
			
			<td style="font-size:9px;"><div id="divFolderName" style="display:none;">New Folder Name</div></td>
			<td style="font-size:9px;"></td>
			<td style="width:90%"></td>
		</tr>
		<tr style="vertical-align:top;">
			<td style="vertical-align:top;">
				<select name="ctl00$ContentMain$UcSearchResults1$drpFolderList" id="ctl00_ContentMain_UcSearchResults1_drpFolderList" onchange="SetSelectedFolder()" style="width:125px;">
		<option selected="selected" value="0">My Folder</option>

	</select>
			</td>
			<td style="vertical-align:top;">
				<strong><input name="ctl00$ContentMain$UcSearchResults1$txtFolderMatCount" type="text" id="ctl00_ContentMain_UcSearchResults1_txtFolderMatCount" readonly="readonly" style="border:none 0 white; background-color:White; color:Black; width: 35px; font-weight:bold;" value="0/0" /></strong>
			</td>
			<td>
				
				<input type="image" id="btnCompareFolders" src="/images/buttons/btnCompareMatls.gif" alt="Compare Checked Materials" onclick="CompareMaterials();return false;" />
			</td>
			
			<td style="white-space:nowrap;vertical-align:top;">
			
				<table id="tblFolderName" class="tabletight" style="display:none;"><tr><td><input type="text" id="txtFolderName2" style="display:inline; width:100px;" /><br /></td>
				<td><input type="image" id="btnApplyFolderName2" src="/images/buttons/btnSaveChanges.gif" style="display:inline;" onclick="ApplyFolderName();return false;" />
				<input type="image" id="btnHideFolderNameControls2" src="/images/buttons/btnCancel.gif" style="display:inline;" onclick="HideFolderNameControls();return false;" /></td>
				</tr></table>
				
			</td>
			<td style="white-space:nowrap;">
				<div class="usermessage" id="divUserMessage"></div>
			</td>
		</tr>
	</table>

	<table class="tabledataformat t_ablegrid" id="tblResults" style="table-layout:auto;"  >

		<tr class="">

			
		
			<th style="width:65px; white-space:nowrap;">Select</th>

			<th style="width:10px;">
				<!--Discontinued and Found Via Interpolated Indicators -->
			</th>
			
			<th style="width:auto;">
				<a id="ctl00_ContentMain_UcSearchResults1_lnkSortMatName" disabled="disabled" title="Click to Sort By Material Name">Material Name</a>
				<br />
				
				
			</th>
		
			
			
			

			

			

			

			

			

			

			

			
		</tr>

		<!-- this is where the search results are actually loaded.  See code behind -->		
		<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10031"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;201</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10031' href="/search/DataSheet.aspx?MatGUID=cc8676499d4945d6ae14d00f19e3f4d3" >Aluminum B384.0 Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10032"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;202</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10032' href="/search/DataSheet.aspx?MatGUID=eafce0f594e644bf84c23edccfc0ec56" >Aluminum C384.0 Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10033"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;203</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10033' href="/search/DataSheet.aspx?MatGUID=392ac894c0d642c0947f8d6b975d55de" >Aluminum 384.0-F Die Casting Alloy</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10034"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;204</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10034' href="/search/DataSheet.aspx?MatGUID=f16e02a3d98c45ee8704207675971e1c" >384.1 Aluminum Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10035"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;205</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10035' href="/search/DataSheet.aspx?MatGUID=5c276977cb1c46c4930f381ebb482d1c" >Aluminum A384.1 Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10036"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;206</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10036' href="/search/DataSheet.aspx?MatGUID=aff13293299c45a780095b8431f03fc6" >Aluminum B384.1 Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10037"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;207</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10037' href="/search/DataSheet.aspx?MatGUID=dc0a8c30d19548229163094e6d615f74" >Aluminum C384.1 Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10038"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;208</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10038' href="/search/DataSheet.aspx?MatGUID=300306d2f22f41be861128a623db30cc" >384.2 Aluminum Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10040"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;209</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10040' href="/search/DataSheet.aspx?MatGUID=9f8f1b7b0aa94729a3a5f1c689a4d1ee" >385.0 Aluminum Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10041"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;210</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10041' href="/search/DataSheet.aspx?MatGUID=a472527cad924ab0b3af13441fc649c5" >385.1 Aluminum Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10053"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;211</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10053' href="/search/DataSheet.aspx?MatGUID=2bb0732f70724d9787d277fec128cc29" >Aluminum A390.0-T6, Permanent Mold Cast</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10054"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;212</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10054' href="/search/DataSheet.aspx?MatGUID=fabe13fb93c943cc96d7ad7f09707b16" >Aluminum A390.0-T6 Sand Cast</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10055"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;213</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10055' href="/search/DataSheet.aspx?MatGUID=c1131e4a194349adadfe012da35e0202" >Aluminum A390.0-T7, Permanent Mold Cast</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10056"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;214</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10056' href="/search/DataSheet.aspx?MatGUID=b12db70000504be99ab0d4fe6a9dbe98" >Aluminum A390.0-T7 Sand Cast</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10057"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;215</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10057' href="/search/DataSheet.aspx?MatGUID=545e06d0038b490fa221a923b5973a0f" >Aluminum A390.0-F, -T5, Permanent Mold Cast</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10058"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;216</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10058' href="/search/DataSheet.aspx?MatGUID=749e09f1cc2b45fc98213c6ca968f72c" >Aluminum A390.0-F, -T5 Sand Cast</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10059"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;217</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10059' href="/search/DataSheet.aspx?MatGUID=5b9cd03099334810b4b6a5dcf01e200f" >Aluminum B390.0-F Die Casting Alloy</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10060"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;218</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10060' href="/search/DataSheet.aspx?MatGUID=24292258c5a54077bf749db43cad1037" >Aluminum 390.0-F Acurad Castings</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10061"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;219</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10061' href="/search/DataSheet.aspx?MatGUID=ac02d06cfe3e4737a9e48d18f73a27fe" >Aluminum 390.0-F Conventional Die Cast</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10062"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;220</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10062' href="/search/DataSheet.aspx?MatGUID=12640bd4108b4ca6819d2269d84e38ef" >Aluminum 390.0-T5 Acurad Castings</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10063"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;221</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10063' href="/search/DataSheet.aspx?MatGUID=7a90eee6fc7340a385424e494e2f748a" >Aluminum 390.0-T5 Conventional Die Cast</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10064"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;222</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10064' href="/search/DataSheet.aspx?MatGUID=7708a2d147414eb39db5960a7bd5e19c" >Aluminum 390.0-T6 Acurad Castings</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10065"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;223</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10065' href="/search/DataSheet.aspx?MatGUID=4355392e90fc40c9b3f61e0538c898cc" >Aluminum 390.0-T7 Acurad Castings</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10066"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;224</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10066' href="/search/DataSheet.aspx?MatGUID=7f2298239219415ba5eeac39c4a5ef61" >Aluminum A390.1 Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10067"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;225</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10067' href="/search/DataSheet.aspx?MatGUID=f8fce046f8dd49aaae73ba026f03f531" >Aluminum B390.1 Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10068"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;226</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10068' href="/search/DataSheet.aspx?MatGUID=1f2527fd611147819a758bdb9ba9d784" >Aluminum 390.2</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10071"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;227</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10071' href="/search/DataSheet.aspx?MatGUID=5df76fd2d4524f858a0de70c8969be1e" >Aluminum 392.0-F Die Casting Alloy</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10072"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;228</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10072' href="/search/DataSheet.aspx?MatGUID=31bbd5751fb64cd4995e66a1996f7d2d" >392.1 Aluminum Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10074"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;229</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10074' href="/search/DataSheet.aspx?MatGUID=2498fbed004343e9b12eafe6469e6d3a" >393.0 Aluminum Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10075"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;230</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10075' href="/search/DataSheet.aspx?MatGUID=d28cd78717cb43bea964d6fd7f518506" >393.1 Aluminum Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10076"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;231</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10076' href="/search/DataSheet.aspx?MatGUID=f01910e94d3c481186307b5dc79983ce" >393.2 Aluminum Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10081"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;232</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10081' href="/search/DataSheet.aspx?MatGUID=ea654db2e9e94fee98366873a66909a5" >408.2 Aluminum Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10082"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;233</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10082' href="/search/DataSheet.aspx?MatGUID=e709cbd8ffc3438ab40171e5d5eca967" >409.2 Aluminum Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10084"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;234</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10084' href="/search/DataSheet.aspx?MatGUID=011b6667d62447d180e1d22f3f124b81" >411.2 Aluminum Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10086"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;235</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10086' href="/search/DataSheet.aspx?MatGUID=641c7123204a4c6bb81190f8685cf60d" >Aluminum A413.0-F Die Casting Alloy</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10087"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;236</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10087' href="/search/DataSheet.aspx?MatGUID=086c55c9c59c41c58cba47b7a0c1e225" >Aluminum B413.0 Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10088"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;237</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10088' href="/search/DataSheet.aspx?MatGUID=6b59b683d2c748dcaf764398d740c8cf" >Aluminum 413.0-F Die Casting Alloy</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10089"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;238</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10089' href="/search/DataSheet.aspx?MatGUID=6185967f0a074363a27c0313637f1e84" >Aluminum A413.1 Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10090"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;239</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10090' href="/search/DataSheet.aspx?MatGUID=5516d8d8dcd24302b8e78bc2cc54f60c" >Aluminum B413.1 Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10091"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;240</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10091' href="/search/DataSheet.aspx?MatGUID=53f4bee02a7f453998c30d6cbc15dad9" >413.2 Aluminum Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10092"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;241</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10092' href="/search/DataSheet.aspx?MatGUID=325d0d26ce6d47468d18a3b583c1e270" >Aluminum A413.2 Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10094"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;242</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10094' href="/search/DataSheet.aspx?MatGUID=77b4cf07c19341079d82205d6d84ec84" >435.2 Aluminum Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10098"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;243</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10098' href="/search/DataSheet.aspx?MatGUID=a1b0e27238e440dfb31ca5bba21da949" >Aluminum A443.0</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10099"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;244</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10099' href="/search/DataSheet.aspx?MatGUID=fedddddf04da46d6843b3c767bf9972b" >Aluminum B443.0-F, Permanent Mold Cast</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10100"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;245</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10100' href="/search/DataSheet.aspx?MatGUID=480e3c56ae0248f990fb697e2ca1e487" >Aluminum B443.0-F, Sand Cast</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10101"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;246</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10101' href="/search/DataSheet.aspx?MatGUID=5e4513e34d3d40938caea361011e2726" >Aluminum C443.0-F Die Casting Alloy</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10102"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;247</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10102' href="/search/DataSheet.aspx?MatGUID=da3c637d80e448e48c5450e4ff3ca7e0" >Aluminum 443.0-F, Permanent Mold Cast</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10103"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;248</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10103' href="/search/DataSheet.aspx?MatGUID=6fa4b95d592d48dd818f8db131c1e4cc" >Aluminum 443.0-F, Sand Cast</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10104"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;249</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10104' href="/search/DataSheet.aspx?MatGUID=51dff3a3827c47049a00a6ace317c132" >Aluminum 443.1 Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10105"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;250</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10105' href="/search/DataSheet.aspx?MatGUID=0f1e02089570411cabe98b71dac14926" >Aluminum A443.1 Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10106"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;251</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10106' href="/search/DataSheet.aspx?MatGUID=570ea5eb3181464a9b9e1f6c30c3d0b5" >Aluminum B443.1 Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10107"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;252</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10107' href="/search/DataSheet.aspx?MatGUID=b393f0cc667b4c57bfdfa68dfcbd42ed" >Aluminum C443.1 Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10108"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;253</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10108' href="/search/DataSheet.aspx?MatGUID=a982279139634bb4abedfadcdbc90d4c" >443.2 Aluminum Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10109"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;254</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10109' href="/search/DataSheet.aspx?MatGUID=ab1b20600ac64355b0e698c416d11477" >Aluminum C443.2 Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10110"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;255</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10110' href="/search/DataSheet.aspx?MatGUID=c0d840321b654aa29d6e5de0d47368dc" >444.0 Aluminum Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10111"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;256</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10111' href="/search/DataSheet.aspx?MatGUID=1e3fa29e2abd41138ca41fa2dfc7e0fc" >Aluminum A444.0-T4, Permanent Mold Cast</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10112"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;257</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10112' href="/search/DataSheet.aspx?MatGUID=9cc1e830761f434c85e6021f45eb2798" >Aluminum A444.0-F Casting Alloy</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10113"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;258</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10113' href="/search/DataSheet.aspx?MatGUID=cf767dcc314b4f61a7dc3f73fbb8c050" >Aluminum A444.1 Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10114"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;259</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10114' href="/search/DataSheet.aspx?MatGUID=2d813c81038c4cff9fa313414729dd51" >444.2 Aluminum Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10115"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;260</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10115' href="/search/DataSheet.aspx?MatGUID=d5472dda45334b74a53c98b368d91917" >Aluminum A444.2 Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10117"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;261</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10117' href="/search/DataSheet.aspx?MatGUID=0d76a9d218274dcb82c746f3e3e4d486" >445.2 Aluminum Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10123"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;262</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10123' href="/search/DataSheet.aspx?MatGUID=d85d6467a4b5453fa61adbec1cbcad50" >511.0 Aluminum Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10124"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;263</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10124' href="/search/DataSheet.aspx?MatGUID=0e4a215854b34df681036e4a10e3aa06" >511.1 Aluminum Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10125"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;264</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10125' href="/search/DataSheet.aspx?MatGUID=262c25e437a745209fd5ab290ff7ee7b" >511.2 Aluminum Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10126"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;265</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10126' href="/search/DataSheet.aspx?MatGUID=a74ff4e83ba64e35b0477ea325fbb4ee" >Aluminum 512.0-F, Sand Cast</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10127"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;266</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10127' href="/search/DataSheet.aspx?MatGUID=a8b66f7e2114408c809b5617197e322c" >512.2 Aluminum Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10128"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;267</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10128' href="/search/DataSheet.aspx?MatGUID=ff3b736420c4414f9fc8e1cceaac143e" >Aluminum 513.0-F, Permanent Mold Cast</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10129"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;268</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10129' href="/search/DataSheet.aspx?MatGUID=38c1cc2b1a6f4605b8b239e74c538c10" >513.2 Aluminum Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10130"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;269</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10130' href="/search/DataSheet.aspx?MatGUID=c6b470dc64ab453f871af5ace03557e5" >Aluminum 514.0-F, Sand Cast</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10131"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;270</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10131' href="/search/DataSheet.aspx?MatGUID=ff123014aa23466089df63a60b4d9913" >514.1 Aluminum Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10132"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;271</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10132' href="/search/DataSheet.aspx?MatGUID=2ccddb67bdcb421eae7c669ea9ccabb7" >514.2 Aluminum Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10134"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;272</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10134' href="/search/DataSheet.aspx?MatGUID=9f1f4b2ffd6843b4be8193ca4444f5bb" >515.0 Aluminum Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10135"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;273</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10135' href="/search/DataSheet.aspx?MatGUID=dc879ce5d2a4425aa24daf9d601c42f3" >515.2 Aluminum Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10136"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;274</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10136' href="/search/DataSheet.aspx?MatGUID=6c0ffff5bb134945b047c4d67a8fc1b7" >516.0 Aluminum Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10137"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;275</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10137' href="/search/DataSheet.aspx?MatGUID=af46a1696d4546288b68aa18f806e642" >516.1 Aluminum Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10138"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;276</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10138' href="/search/DataSheet.aspx?MatGUID=63910549ca814e668c358d731d3925cd" >Aluminum 518.0-F Die Casting Alloy</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10139"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;277</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10139' href="/search/DataSheet.aspx?MatGUID=84384961df244ad2a75bb5c42706a4f1" >518.1 Aluminum Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10140"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;278</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10140' href="/search/DataSheet.aspx?MatGUID=9ee3f74644c3492e9c87488cd083c454" >518.2 Aluminum Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10144"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;279</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10144' href="/search/DataSheet.aspx?MatGUID=2eb3b8960b55457cb705cca8355b1c3a" >Aluminum 520.0-T4, Sand Cast</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10145"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;280</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10145' href="/search/DataSheet.aspx?MatGUID=2459ee61041b44f19343c88e1601839a" >520.2 Aluminum Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10147"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;281</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10147' href="/search/DataSheet.aspx?MatGUID=60b23f8c4fde40e6b59a773b221999b4" >Aluminum A535.0-F or Aluminum 535.0-T5, Cast</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10148"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;282</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10148' href="/search/DataSheet.aspx?MatGUID=71f3ea55c970420b8609830e466e956d" >Aluminum B535.0-F or Aluminum 535.0-T5, Cast</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10149"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;283</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10149' href="/search/DataSheet.aspx?MatGUID=47d4413f49084c91bd4e8cd85b87428d" >Aluminum 535.0-F or Aluminum 535.0-T5, Sand Cast</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10150"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;284</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10150' href="/search/DataSheet.aspx?MatGUID=8dc215fa298340d586117a2b6f02a912" >Aluminum A535.1 Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10151"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;285</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10151' href="/search/DataSheet.aspx?MatGUID=9347211fc1e1476988dfee51d8241c3c" >535.2 Aluminum Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10152"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;286</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10152' href="/search/DataSheet.aspx?MatGUID=0f9249221cf34e78891f1d0d809d5b55" >Aluminum B535.2 Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10159"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;287</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10159' href="/search/DataSheet.aspx?MatGUID=4e517ee4c14b43a2ba984a9c1f122cf7" >Aluminum 705.0-F, Sand Cast</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10160"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;288</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10160' href="/search/DataSheet.aspx?MatGUID=3fe0134b0ee54f5f86c86a5c6ef25c1f" >Aluminum 705.0-T5, Permanent Mold Cast</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10161"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;289</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10161' href="/search/DataSheet.aspx?MatGUID=1d2b0da75dfb4df88a2864953643a712" >Aluminum 705.0-T5, Sand Cast</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10162"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;290</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10162' href="/search/DataSheet.aspx?MatGUID=366898aa22404e6ba99e94e130fde6fd" >705.1 Aluminum Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10165"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;291</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10165' href="/search/DataSheet.aspx?MatGUID=a4f6ab2c4b574ca899307a868f039936" >Aluminum 707.0-F Casting Alloy</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10166"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;292</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10166' href="/search/DataSheet.aspx?MatGUID=5b011498d646405582c65104ceb01b80" >Aluminum 707.0-T5, Sand Cast</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10167"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;293</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10167' href="/search/DataSheet.aspx?MatGUID=cfce98401b7d449e911ca539ef72b312" >Aluminum 707.0-T7, Permanent Mold Cast</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10168"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;294</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10168' href="/search/DataSheet.aspx?MatGUID=a72fe1b3e0224cb49fac7e514823888a" >Aluminum 707.0-T7, Sand Cast</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10169"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;295</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10169' href="/search/DataSheet.aspx?MatGUID=74ef277ca12f48f3ab8e56e728c74cd3" >707.1 Aluminum Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10172"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;296</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10172' href="/search/DataSheet.aspx?MatGUID=3693c9f8e9c949c6bc75483d236bd9a5" >Aluminum 710.0-F Casting Alloy</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10173"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;297</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10173' href="/search/DataSheet.aspx?MatGUID=649bbaeb38b44d5d928d488ab8285416" >Aluminum 710.0-F, Sand Cast</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10174"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;298</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10174' href="/search/DataSheet.aspx?MatGUID=80b024eb598b4a41b1fdbbf79c669dfe" >Aluminum 710.0-T5, Sand Cast</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10175"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;299</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10175' href="/search/DataSheet.aspx?MatGUID=090d43f020224c04b4b219a7c43cde2e" >710.1 Aluminum Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10177"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;300</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10177' href="/search/DataSheet.aspx?MatGUID=b7d3565b76f84e4bb907f8993a3c5150" >Aluminum 711.0-F Casting Alloy</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10178"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;301</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10178' href="/search/DataSheet.aspx?MatGUID=258c4336585a477e8e30a1d77ce53444" >Aluminum 711.0-T1, Permanent Mold Cast</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10179"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;302</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10179' href="/search/DataSheet.aspx?MatGUID=1400e71f3f0c44e3ab087b18f797ed58" >711.1 Aluminum Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10180"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;303</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10180' href="/search/DataSheet.aspx?MatGUID=22b018936254495a8de4f53dd88bc94b" >Aluminum 712.0-F, Sand Cast</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10181"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;304</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10181' href="/search/DataSheet.aspx?MatGUID=eb66155e697d4219abe7768aa7bbf12c" >Aluminum 712.0-T5, Sand Cast</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10182"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;305</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10182' href="/search/DataSheet.aspx?MatGUID=231039305a204284b79821c1485fe2af" >712.2 Aluminum Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10183"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;306</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10183' href="/search/DataSheet.aspx?MatGUID=f4a7aed5e0264ae7a2a8bc098f60fc11" >Aluminum 713.0-F, Sand Cast</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10184"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;307</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10184' href="/search/DataSheet.aspx?MatGUID=2f9bc4e09b91418aa667d22267b7055c" >Aluminum 713.0-T5, Permanent Mold Cast</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10185"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;308</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10185' href="/search/DataSheet.aspx?MatGUID=33c00dd43cb94acdb1fe40f2d1f8f5f5" >Aluminum 713.0-T5, Sand Cast</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10186"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;309</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10186' href="/search/DataSheet.aspx?MatGUID=271da8021db1469ea194ec3fda2863b9" >713.1 Aluminum Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10196"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;310</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10196' href="/search/DataSheet.aspx?MatGUID=fee93c73b47743bbad494bea05a9702d" >Aluminum 771.0-F Casting Alloy</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10197"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;311</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10197' href="/search/DataSheet.aspx?MatGUID=328125766de448b68c220cdadc3e562a" >Aluminum 771.0-T5, Sand Cast</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10198"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;312</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10198' href="/search/DataSheet.aspx?MatGUID=82bd6a5ad258459ea998a22ea78880b6" >Aluminum 771.0-T51, Sand Cast</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10199"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;313</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10199' href="/search/DataSheet.aspx?MatGUID=e373de054a08484bbad31c91e6d46648" >Aluminum 771.0-T52, Sand Cast</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10200"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;314</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10200' href="/search/DataSheet.aspx?MatGUID=08d3ad37504a44b393e7075d7dfef771" >Aluminum 771.0-T53, Sand Cast</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10201"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;315</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10201' href="/search/DataSheet.aspx?MatGUID=b07e4a27878b4bb6bad6385946b6fed1" >Aluminum 771.0-T6, Sand Cast</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10202"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;316</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10202' href="/search/DataSheet.aspx?MatGUID=814f7bf8c527403ba970cdd1fba7f040" >Aluminum 771.0-T71, Sand Cast</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10203"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;317</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10203' href="/search/DataSheet.aspx?MatGUID=751b5f4e84aa4f5b8fe1461966413279" >771.2 Aluminum Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10205"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;318</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10205' href="/search/DataSheet.aspx?MatGUID=f45953262fa8479387d230aea2d419f6" >772.0 Aluminum Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10206"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;319</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10206' href="/search/DataSheet.aspx?MatGUID=5f21a8cb872246ed8c08de5c46b69e6e" >772.2 Aluminum Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10212"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;320</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10212' href="/search/DataSheet.aspx?MatGUID=3d0886e6d3a147a6894604c8b7dc7009" >Aluminum 850.0-T5, Permanent Mold Cast</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10213"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;321</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10213' href="/search/DataSheet.aspx?MatGUID=1b95a239e76e46a9bc91ed6ebe650d17" >Aluminum 850.0-T5, Sand Cast</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10214"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;322</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10214' href="/search/DataSheet.aspx?MatGUID=4369efbf5ae649e19fefc9ea82d8450b" >850.1 Aluminum Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10216"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;323</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10216' href="/search/DataSheet.aspx?MatGUID=c7fbcb731bb0416c93b4a38037623f4e" >Aluminum 851.0-T5, Permanent Mold Cast</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10217"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;324</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10217' href="/search/DataSheet.aspx?MatGUID=e7606e1335b74c578440e2dd37e6588d" >Aluminum 851.0-T5, Sand Cast</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10218"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;325</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10218' href="/search/DataSheet.aspx?MatGUID=2f875fb0f7954257a8583294fee83889" >Aluminum 851.0-T6, Permanent Mold Cast</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10219"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;326</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10219' href="/search/DataSheet.aspx?MatGUID=a1d1dbbc9f014d8fa74b71b1c58e5088" >851.1 Aluminum Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10220"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;327</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10220' href="/search/DataSheet.aspx?MatGUID=6150bc0c75b14a6793ac359afe662c93" >Aluminum 852.0-T5, Permanent Mold Cast</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10221"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;328</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10221' href="/search/DataSheet.aspx?MatGUID=66f4c52f6697491dac6a557db690764a" >Aluminum 852.0-T5, Sand Cast</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10222"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;329</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10222' href="/search/DataSheet.aspx?MatGUID=78ea63d6d69146938afb44ba026fd7fb" >852.1 Aluminum Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10223"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;330</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10223' href="/search/DataSheet.aspx?MatGUID=cfc75ae17f8e4d39af9d12324547210a" >853.0 Aluminum Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10224"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;331</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10224' href="/search/DataSheet.aspx?MatGUID=dacf42da796343798540e9e4c9699c33" >853.2 Aluminum Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10245"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;332</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10245' href="/search/DataSheet.aspx?MatGUID=95d58aafb0724ab1aeef30bdbe3580cb" >Aluminum LM2 As Manufactured, Gravity Die Cast</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10246"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;333</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10246' href="/search/DataSheet.aspx?MatGUID=71d6b153f9a14dc881ce8df246700989" >Aluminum LM2 As Manufactured, Pressure Die Cast</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10247"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;334</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10247' href="/search/DataSheet.aspx?MatGUID=7463b851e88e46da82ad98729cb98f6e" >Aluminum LM4 As Manufactured, Sand Cast</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10248"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;335</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10248' href="/search/DataSheet.aspx?MatGUID=bc172037c0ce4d99998545a19d9c17e1" >Aluminum LM4 As Manufactured, Gravity Die Cast</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10249"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;336</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10249' href="/search/DataSheet.aspx?MatGUID=ff37872029d848128a6f820614cde147" >Aluminum LM4 TF (T6) Temper, Sand Cast</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10250"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;337</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10250' href="/search/DataSheet.aspx?MatGUID=571162c9c47b4f028a56bb9e5856f397" >Aluminum LM4 TF (T6) Temper, Gravity Die Cast</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10251"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;338</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10251' href="/search/DataSheet.aspx?MatGUID=c87a2f68cee540aa8051a50c62e75db0" >Aluminum LM5 As Manufactured, Sand Cast</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10252"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;339</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10252' href="/search/DataSheet.aspx?MatGUID=46bdb5fc25504434944f3848d79ea29e" >Aluminum LM5 As Manufactured, Gravity Die Cast</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10253"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;340</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10253' href="/search/DataSheet.aspx?MatGUID=ba0748bdf368466092f22f6f11c8c1c1" >Aluminum LM6 As Manufactured, Sand Cast</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10254"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;341</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10254' href="/search/DataSheet.aspx?MatGUID=cbe623e17da0496e9a9d44bb84a54ff4" >Aluminum LM6 As Manufactured, Gravity Die Cast</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10255"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;342</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10255' href="/search/DataSheet.aspx?MatGUID=94206dc412364e43ab489c3407d1a5a8" >Aluminum LM25 As Manufactured, Sand Cast</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10256"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;343</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10256' href="/search/DataSheet.aspx?MatGUID=f8eff7adafdc4c44ab396c6c537f3a9c" >Aluminum LM25 As Manufactured, Gravity Die Cast</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10257"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;344</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10257' href="/search/DataSheet.aspx?MatGUID=001104f73389442f98ed9ca10cd0a385" >Aluminum LM25 TE (T5) Temper, Sand Cast</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10258"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;345</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10258' href="/search/DataSheet.aspx?MatGUID=d3039b1b1f4146438f3223edb045f632" >Aluminum LM25 TE (T5) Temper, Gravity Die Cast</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10259"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;346</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10259' href="/search/DataSheet.aspx?MatGUID=94b5bd3f1fdd4f4aa7b3088a257b884a" >Aluminum LM25 TB7 Temper, Sand Cast</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10260"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;347</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10260' href="/search/DataSheet.aspx?MatGUID=c7ea20e832aa481ea9542f5e3bdb6ccd" >Aluminum LM25 TB7 Temper, Gravity Die Cast</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10261"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;348</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10261' href="/search/DataSheet.aspx?MatGUID=0c88435b40c740ed92c1edd22feed920" >Aluminum LM25 TF (T6) Temper, Sand Cast</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10262"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;349</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10262' href="/search/DataSheet.aspx?MatGUID=38d73a5cdae940c28b0f486a438f90b6" >Aluminum LM25 TF (T6) Temper, Gravity Die Cast</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_131141"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;350</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_131141' href="/search/DataSheet.aspx?MatGUID=00fb97ab02dc42e8bfa06108f56682b5" >Alcoa MIC-6 Aluminum Mold Alloy</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_14313"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;351</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_14313' href="/search/DataSheet.aspx?MatGUID=cdfeb4bcdd7e4a7cba0355b6825f67c4" >Composition Spec for Aluminum 1435H</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_14322"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;352</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_14322' href="/search/DataSheet.aspx?MatGUID=9560a603354743cd93bbedd22067bb9e" >Composition Spec for Aluminum 208.1</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_14323"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;353</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_14323' href="/search/DataSheet.aspx?MatGUID=94028bb61eff40df8c955ccc3bc49627" >Composition Spec for Aluminum 208.2</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_14325"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;354</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_14325' href="/search/DataSheet.aspx?MatGUID=4584a0fb24cb4e3f80f2f44b751d82c0" >Composition Spec for Aluminum 213.0</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_14326"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;355</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_14326' href="/search/DataSheet.aspx?MatGUID=8ec00d89c9e24a15b51930766c55cd64" >Composition Spec for Aluminum 213.1</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_14327"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;356</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_14327' href="/search/DataSheet.aspx?MatGUID=1b950770615a46328f584f1c2d9cf45c" >Composition Spec for Aluminum 222.1</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_14328"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;357</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_14328' href="/search/DataSheet.aspx?MatGUID=9f56a5d4e14a41e49bf67ff1032e67d3" >Composition Spec for Aluminum 224.0</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_14329"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;358</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_14329' href="/search/DataSheet.aspx?MatGUID=21fc967e4c2b49b28b1214aa08df48ef" >Composition Spec for Aluminum 224.2</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_14335"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;359</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_14335' href="/search/DataSheet.aspx?MatGUID=e5d5e8e4140d4e3ea4488d804becd8c1" >Composition Spec for Aluminum 243.0</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_14336"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;360</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_14336' href="/search/DataSheet.aspx?MatGUID=49c624c1fc0d4cd2b254a9096ce13ba1" >Composition Spec for Aluminum 243.1</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_14341"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;361</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_14341' href="/search/DataSheet.aspx?MatGUID=b422bc96ee2648199ed90a59d1fd89f9" >Composition Spec for Aluminum 305.0</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_14342"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;362</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_14342' href="/search/DataSheet.aspx?MatGUID=3acd8bff5f894cbca99a61cfe5302256" >Composition Spec for Aluminum 305.2</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_14349"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;363</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_14349' href="/search/DataSheet.aspx?MatGUID=fa9cb42a962041358c53713ed9b1c851" >Composition Spec for Aluminum 324.0</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_14350"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;364</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_14350' href="/search/DataSheet.aspx?MatGUID=68ae5fc0b89545aba07359c04220fef0" >Composition Spec for Aluminum 324.1</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_14351"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;365</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_14351' href="/search/DataSheet.aspx?MatGUID=e5e0ab961917409db88a332039f9755c" >Composition Spec for Aluminum 324.2</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_14352"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;366</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_14352' href="/search/DataSheet.aspx?MatGUID=9ebc4b5863334119b124c2ab5efd2d4a" >Composition Spec for Aluminum 328.1</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_14360"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;367</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_14360' href="/search/DataSheet.aspx?MatGUID=92f2a5d252a54c2dac210ecbeefcaa18" >Composition Spec for Aluminum 343.0</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_14361"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;368</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_14361' href="/search/DataSheet.aspx?MatGUID=1379b48db6af48e69b5fe02c20e063e6" >Composition Spec for Aluminum 343.1</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_14367"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;369</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_14367' href="/search/DataSheet.aspx?MatGUID=1cd421e593e4466ab9ac2de9c4ee020f" >Composition Spec for Aluminum 356.2M</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_14369"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;370</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_14369' href="/search/DataSheet.aspx?MatGUID=d6f49f8ce09e44d0b360b661c6c2b340" >Composition Spec for Aluminum 357P</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_14394"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;371</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_14394' href="/search/DataSheet.aspx?MatGUID=47af04a0fce94a858bb7176438576cfa" >Composition Spec for Aluminum 400A</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_14395"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;372</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_14395' href="/search/DataSheet.aspx?MatGUID=b2605c2040ff4aa18ebfbf8008cca75c" >Composition Spec for Aluminum 400B</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_14396"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;373</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_14396' href="/search/DataSheet.aspx?MatGUID=b1e9da33cf4e4b5f8a9d457430feef41" >Composition Spec for Aluminum 400C</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_14397"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;374</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_14397' href="/search/DataSheet.aspx?MatGUID=eb79345dc64f45188d4fd40b0c111cac" >Composition Spec for Aluminum 400D</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_14398"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;375</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_14398' href="/search/DataSheet.aspx?MatGUID=a571a0d660bc44fead65429c08c94084" >Composition Spec for Aluminum 400S</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_14450"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;376</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_14450' href="/search/DataSheet.aspx?MatGUID=38e31514fb76450c9a26b72b8071af5b" >Composition Spec for Aluminum A305.0</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_14451"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;377</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_14451' href="/search/DataSheet.aspx?MatGUID=a63f61049a644b0e85f6a83ab8f3eb86" >Composition Spec for Aluminum A305.1</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_14452"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;378</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_14452' href="/search/DataSheet.aspx?MatGUID=5753d2a087d94b4aa71d3c735aef9a0c" >Composition Spec for Aluminum A305.2</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_14461"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;379</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_14461' href="/search/DataSheet.aspx?MatGUID=a7765cfb04cb4047b91a547725c602ed" >Composition Spec for Aluminum A356.2M</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_14462"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;380</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_14462' href="/search/DataSheet.aspx?MatGUID=83a9b15058a447378df2d97aaba30eeb" >Composition Spec for Aluminum A356.2R</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_10263"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;381</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_10263' href="/search/DataSheet.aspx?MatGUID=860cf573619c45b9ad43051420a0cec5" >Alcoa QC-10® Aluminum Mold Alloy</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_14756"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;382</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_14756' href="/search/DataSheet.aspx?MatGUID=e1d6d9ccf97d46258d3187438acd8c3a" >ALIMEX ACP 5080R Aluminum Cast Material</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_14757"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;383</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_14757' href="/search/DataSheet.aspx?MatGUID=ee4ac64e03a54e1984ef551bc9b6e9d9" >ALIMEX ACP 5080 Aluminum Cast Plate</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_14758"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;384</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_14758' href="/search/DataSheet.aspx?MatGUID=f5d582dd5f0247068bbc74d3218d6995" >ALIMEX ACP 6000 Aluminum Cast Plate</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_14949"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;385</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_14949' href="/search/DataSheet.aspx?MatGUID=6fc708b1112e4757a0a0579000ff321a" >Alpase K100-S™ Aluminum Plate</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_14950"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;386</td><td style="width:auto;"><img src="/images/buttons/iconDiscontinued.jpg" alt="Discontinued" title="Discontinued"  /></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_14950' href="/search/DataSheet.aspx?MatGUID=eb56a04060634090ae8e53aa7b4e0246" >Alpase K100™ Aluminum Plate</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_14951"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;387</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_14951' href="/search/DataSheet.aspx?MatGUID=c674eae07e6544acaeaab3815d066801" >Alpase M-1™ Aluminum Mold Plate</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_131319"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;388</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_131319' href="/search/DataSheet.aspx?MatGUID=893837079bda413483c723c25514a270" >Alpase M-5™ Aluminum Mold Plate</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_160274"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;389</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_160274' href="/search/DataSheet.aspx?MatGUID=ea2c96f526f6470aa768a254556ca3c2" >Constellium Alumold® 110 Aluminum Cast Sliced Plate</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_160283"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;390</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_160283' href="/search/DataSheet.aspx?MatGUID=efaefb2fa1b5425baca2f72396711880" >Constellium Fibral® Cast Aluminum Sliced Plate</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_185651"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;391</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_185651' href="/search/DataSheet.aspx?MatGUID=6a41c9f935be463f800b5be13b64b21a" >Delphi K-Alloy A304 Casting Alloy</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_133288"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;392</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_133288' href="/search/DataSheet.aspx?MatGUID=5d9406e0d5f4485095cc3b5034072216" >PCP-Canada Max 5® 5083 Cast Aluminum Mold Block</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_133289"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;393</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_133289' href="/search/DataSheet.aspx?MatGUID=9cc81cdd99604246873d6cbf8a96cac8" >PCP-Canada ALCA 5® 5083 Cast Aluminum Mold Plate</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_133290"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;394</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_133290' href="/search/DataSheet.aspx?MatGUID=f6335099ea284aa38591b702bda01e4f" >PCP-Canada ALCA MAX® 2XXX Cast Aluminum Mold Blocks/Plate</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_175998"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;395</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_175998' href="/search/DataSheet.aspx?MatGUID=5770e28ff73d4aec801047462e2f5259" >PCP-Canada BUS PLATE® AA1370-50 Cast Precision Sawed Blocks/Plates</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_19675"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;396</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_19675' href="/search/DataSheet.aspx?MatGUID=f703dcddfb164afbb65ab3e3416c13e1" >Universal Wire Works A356 (AMS 4181) Aluminum Alloy Filler Metal</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_19684"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;397</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_19684' href="/search/DataSheet.aspx?MatGUID=32172087857c4a879fda1327c4efbb33" >Universal Wire Works C355.0 (AMS 4245) Aluminum Alloy Filler Metal</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_19685"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;398</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_19685' href="/search/DataSheet.aspx?MatGUID=ae2e8ac26a554c85833d05e1f4628052" >Universal Wire Works 357 (AMS 4246) Aluminum Alloy Filler Metal</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_131143"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;399</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_131143' href="/search/DataSheet.aspx?MatGUID=055a29e62c8149fe89309481d96a30b6" >Vista Metals Duramold-2™ Cast Aluminum Mold Plate</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_131144"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;400</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_131144' href="/search/DataSheet.aspx?MatGUID=47bcf8d5578349f58a87da61429b3596" >Vista Metals Duramold-5™ Cast Aluminum Mold Plate</a></td></tr>

		
	</table>

	<table style="background-color:Silver;" class="tabledataformat" >

		<tr class="">
			<th>&nbsp;</th>
		</tr>

		<tr>
			<td><strong>Found <span id="ctl00_ContentMain_UcSearchResults1_lblResultCount2">401</span> Results</strong> -- 
				
				Page 
				<select name="ctl00$ContentMain$UcSearchResults1$drpPageSelect2" onchange="javascript:setTimeout('__doPostBack(\'ctl00$ContentMain$UcSearchResults1$drpPageSelect2\',\'\')', 0)" id="ctl00_ContentMain_UcSearchResults1_drpPageSelect2">
		<option value="1">1</option>
		<option selected="selected" value="2">2</option>
		<option value="3">3</option>

	</select>
				of <span id="ctl00_ContentMain_UcSearchResults1_lblPageTotal2">3</span> 
				
				-- 
				<a id="ctl00_ContentMain_UcSearchResults1_lnkPrevPage2" href="javascript:__doPostBack('ctl00$ContentMain$UcSearchResults1$lnkPrevPage2','')" style="color:Black;">[Prev Page]</a>
				<a id="ctl00_ContentMain_UcSearchResults1_lnkNextPage2" href="javascript:__doPostBack('ctl00$ContentMain$UcSearchResults1$lnkNextPage2','')" style="color:Black;">[Next Page]</a>
				--&nbsp;

				view 
				<select name="ctl00$ContentMain$UcSearchResults1$drpPageSize2" onchange="javascript:setTimeout('__doPostBack(\'ctl00$ContentMain$UcSearchResults1$drpPageSize2\',\'\')', 0)" id="ctl00_ContentMain_UcSearchResults1_drpPageSize2">
		<option value="25">25</option>
		<option value="50">50</option>
		<option value="75">75</option>
		<option value="100">100</option>
		<option value="150">150</option>
		<option selected="selected" value="200">200</option>

	</select> per page
			</td>
		</tr>
	</table>
	
	<span id="ctl00_ContentMain_UcSearchResults1_divDiscontinued" style="font-size:11px;color:red">
	<span id="ctl00_ContentMain_UcSearchResults1_lblDisclaimer"><br />
Materials flagged as discontinued (<img src="/images/buttons/iconDiscontinued.jpg" alt="" />) are no longer part of the manufacturer’s standard product line according to our latest information.  These materials may be available by special order, in distribution inventory, or reinstated as an active product.  Data sheets from materials that are no longer available remain in MatWeb to assist users in finding replacement materials.  
<br /><br />
Users of our Advanced Search (registration required) may exclude discontinued materials from search results.</span>
	</span> 


</div>





<script type="text/javascript">

	//GLOBAL VARIABLES
	var gCurrentCheckbox;
	var gNewFolderID;
	var gOldFolderName;
	var gSelectedFolderID = 0;
	var gMaxFolders = 0;

	//GLOBAL FIELD REFERENCES
	var drpFolderList = document.getElementById("ctl00_ContentMain_UcSearchResults1_drpFolderList");
	var drpFolderAction = document.getElementById("ctl00_ContentMain_UcSearchResults1_drpFolderAction");
	var txtFolderMatCount = document.getElementById("ctl00_ContentMain_UcSearchResults1_txtFolderMatCount");
	var divUserMessage = document.getElementById("divUserMessage");
	var divFolderName = document.getElementById("divFolderName");
	var tblFolderName = document.getElementById("tblFolderName");
	var txtFolderName = document.getElementById("txtFolderName2");
	
	// See MS ajax.net client side documentation
	// http://ajax.asp.net/docs/ClientReference/Sys.Net/WebrequestManagerClass/	
	Sys.Net.WebRequestManager.add_invokingRequest(ucSearchResults_OnRequestStart);
	Sys.Net.WebRequestManager.add_completedRequest(ucSearchResults_OnRequestEnd);
	
	var timerID = 0;

	function ucSearchResults_OnRequestStart(){
		document.body.style.cursor="progress";
	}
	
	function ucSearchResults_OnRequestEnd(){
		document.body.style.cursor="auto";
	}

	//drpFolderAction pull down does not exist for anonymous users
	if(drpFolderAction) {
		drpFolderAction.onchange=RunFolderAction;
	}

	//RUN ONCE on page load
	//matweb.register_onload(SetSelectedFolder);
	
	SearchMatIDList = []; //an array of MatIDs
	//matweb.alert(MatIDList);
	
	//======================
	//Test Service 
	//======================
	//TestService("This is a test");
	//aci.matweb.WebServices.Folders.RaiseTestError(null,ErrorHandler,"testerror");
	
	function TestService(arg){
		aci.matweb.WebServices.Folders.HelloWorld(arg, TestService_CALLBACK,ErrorHandler,"TestService");
	}
	
	function TestService_CALLBACK(data,context){
		alert(data);
	}
	
	// =================================
	// This function is the onchange event for the material checkbox. See Code Behind.
	// The first time a material is checked, and there are no folders, the folder web service creates a NEW FOLDER
	// =================================
	function ToggleMat(chkBox,MatID,MaxFolderMats){
	
		HideFolderNameControls();

		var FolderID,Context;
		gCurrentCheckbox = chkBox;
		var matLink = $("lnkMatl_" + MatID);
		var MatName = matLink.innerHTML;
		//alert(MatName);

		var SelectedOption = drpFolderList.options[drpFolderList.selectedIndex];  

		if(gNewFolderID > 0)
			FolderID = gNewFolderID;
		else
			FolderID = SelectedOption.value; //this will be 0 if no folders exist yet
		//alert(FolderID);
		
		// call the folder web service 
		// See: ScriptManagerProxy object at the top of this page
		// Also See: /folders/FolderService.asmx & /App_Code/FolderService.cs
		if(chkBox.checked){
			Context = "AddMat";
			aci.matweb.WebServices.Folders.AddMaterial(FolderID, MatID, MatName, ToggleMat_CALLBACK,ErrorHandler,Context);
		}
		else{
			Context = "RemoveMat";
			aci.matweb.WebServices.Folders.RemoveMaterial(FolderID, MatID, ToggleMat_CALLBACK,ErrorHandler,Context);
		}
			
	}
	
	// =================================
	// CALLBACK FUNCTION
	// this function is called by the AJAX framework after the web service is invoked
	// The folder web service returns a ReturnData type object.
	// =================================
	function ToggleMat_CALLBACK(data,context){

		 //alert(data);
		// alert(context);

		var FolderMatCount = data.FolderMatCount
		var NewFolderID = data.NewFolderID
		var DisplayMessage = data.DisplayMessage
		var MaxFolderMats = data.FolderMaxMatls;
		
		txtFolderMatCount.value = FolderMatCount + "/" + MaxFolderMats;

		// If this is the first time the user checked the checkbox, and the user did not already have any folders, 
		// a new folder may have been created.
		// Save the new folderID to a hidden field, so we can reference it later.
		if(NewFolderID > 0){
			gNewFolderID = NewFolderID;
		}
		
		// alert(gNewFolderID);
		// alert(FolderCount);
		// alert(ReloadFolderList);
		// if(NewFolderID>0) alert(NewFolderID);
		// alert(MaxFolderMats);
		
		switch(context){
		case "AddMat":
			if(DisplayMessage){
				gCurrentCheckbox.checked = false;
				matweb.alert(DisplayMessage);
			}
			break    
		case "RemoveMat":
			break    
		default:
			matweb.Alert("Unhandeled Context in ToggleMat_CALLBACK() function: " + Context);
		}//end switch

	}//end function

	
	//=================================
	function CompareMaterials(){
	//=================================
		var SelectedFolderID = GetSelectedFolderID();
		window.location.href="/folders/Compare.aspx?FolderID=" + SelectedFolderID;
	}

	//=================================
	// Runs on drpFolderList.onchange, and also is called by other methods.
	// Sets the checkbox for each visible material in the folder.
	// Also sets txtFolderMatCount with the MatCount indicator for the selected folder.
	//=================================
	function SetSelectedFolder(){
		//alert("SetSelectedFolder()");
		var SelectedFolderID = GetSelectedFolderID();
		//alert("SelectedFolderID :" + SelectedFolderID)
		
		aci.matweb.WebServices.Folders.SetSelectedFolder(SelectedFolderID,SetSelectedFolder_CALLBACK,ErrorHandler,"update_txtFolderMatCount");
		
		HideFolderNameControls();
	}

	// =================================
	// CALLBACK FUNCTION
	// Returns a list of MatID's in the selected folder, and sets the checkbox for each material.
	// Also sets txtFolderMatCount with the MatCount indicator for the selected folder.
	// =================================
	function SetSelectedFolder_CALLBACK(data,context){

		// alert(data);
		// alert(context);

		// the folder web service returns an object with 4 fields
		//alert("data.FolderMatCount: " + data.FolderMatCount);
		//alert("data.FolderMaxMatls: " + data.FolderMaxMatls);
		//alert("data.DisplayMessage: " + data.DisplayMessage);
		//alert("data.MatIDList: " + data.FolderMatIDList);

		if(data.DisplayMessage){
			matweb.alert(data.DisplayMessage);
		}

		txtFolderMatCount.value = data.FolderMatCount + "/" + data.FolderMaxMatls;

		//alert(SearchMatIDList);
		var chkFolder;

		// Set checkboxes for any visible materials in the folder
		// loop over all materials in the search results for the current page...
		//alert('lenOfArray=' + SearchMatIDList.length);
		for(s=0;s<SearchMatIDList.length;s++){
			chkFolder = document.getElementById("chkFolder_" + SearchMatIDList[s]);
			//alert('index=' + SearchMatIDList[s]);
			chkFolder.checked = false; //clear any currently checked boxes...
			//loop over all materials in the folder...
			//If there is a match, make it checked.
			for(f=0;f<data.FolderMatIDList.length;f++){
				if(SearchMatIDList[s]==data.FolderMatIDList[f]){
					//alert("Match found: " + data.FolderMatIDList[f]);
					chkFolder.checked = true;
				}
			}
		}

	}//end function
	
	//=================================
	function RunFolderAction(){
	//=================================
	
		//alert("RunFolderAction()");
		var action = drpFolderAction.options[drpFolderAction.selectedIndex].value;
		var SelectedFolderName = drpFolderList.options[drpFolderList.selectedIndex].text;
		var FolderID = GetSelectedFolderID();
		var msg = "";
		
		switch(action){
			case "empty":
				aci.matweb.WebServices.Folders.EmptyFolder(FolderID,SetSelectedFolder_CALLBACK,ErrorHandler,action);
				msg = "Folder ["+SelectedFolderName+"] was cleared of materials";
				HideFolderNameControls();
				break;
			case "delete":
				var answer = confirm("Delete folder ["+SelectedFolderName+"] ?");
				if(answer){
					gOldFolderName = SelectedFolderName;
					aci.matweb.WebServices.Folders.DeleteFolder(FolderID,DeleteFolder_CALLBACK,ErrorHandler,action);
				}
				HideFolderNameControls();
				break;
			case "new":
				//setting up new folder controls
				if(drpFolderList.options.length >= gMaxFolders){
					matweb.alert("You have already added as many folders as you are currently allowed by the system (" + gMaxFolders + ").<p/> You may view our <a href=\"/membership/benefits.aspx\">benefits</a> page for the features that are allowed for your current user level.");
				}
				else{
				divFolderName.style.display="block";
				divFolderName.innerHTML="New Folder Name";
				tblFolderName.style.display="block";
				txtFolderName.value = "New Folder Name";
				txtFolderName.focus();
				txtFolderName.select();
				}
				break;
			case "rename":
				//setting up rename folder controls
				divFolderName.style.display="block";
				divFolderName.innerHTML="Rename Folder";
				tblFolderName.style.display="block";
				txtFolderName.value = SelectedFolderName;
				txtFolderName.focus();
				txtFolderName.select();
				break;
			case "managefolders":
				window.location.href="/folders/ListFolders.aspx";
				break;
			case "expSolidworks":
				ExportFolder(FolderID, 2, null);
				break;
			case "expALGOR":
				ExportFolder(FolderID, 3, null);
				break;
			case "expANSYS":
				ExportFolder(FolderID, 4, null);
				break;
			case "expNEiWorks":
				ExportFolder(FolderID, 5, null);
				break;
			case "expCOMSOL3":
				ExportFolder(FolderID, 6, '3.4 or newer');
				break;
			case "expCOMSOL4":
				ExportFolder(FolderID, 10, null);
				break;
			case "expETBX":
				ExportFolder(FolderID, 8, null);
				break;
			case "expSpaceClaim":
				ExportFolder(FolderID, 9, null);
				break;
			case "0":
				//do nothing
				drpFolderAction.selectedIndex = 0;
				break;
			default: 
				alert("Unhandled Action: " + action);
		}

		//drpFolderAction.selectedIndex=0;
		divUserMessage.innerHTML = msg;
		//alert("End RunFolderAction()");
		
	}
	
	function DeleteFolder_CALLBACK(DeleteWasSuccessfull,context){
		divUserMessage.innerHTML = "Folder ["+gOldFolderName+"] was deleted.";
		//Refresh the folder list and selected checkboxes
		aci.matweb.WebServices.Folders.GetFolderList(GetFolderList_CALLBACK,ErrorHandler,"DeleteFolder_CALLBACK");
	}	

	// rsw
	function ExportFolder(folderid, modeid, comsolversion){
		if(txtFolderMatCount.value.substring(0,1) == '0'){
			alert('Folder is empty, nothing to export.');
			drpFolderAction.selectedIndex = 0;
			return;
		}

		var ExportPath = "/folders/ListFolders.aspx";
		
		//alert(ExportPath);
		location.href = ExportPath;
		//window.open(ExportPath, '', '', '');
	}
	
	
	
	
	//===============================================================
	// This runs on the onclick event for btnApplyFolderName.
	// It handles creating a new folder, or renaming an existing one.
	//===============================================================
	function ApplyFolderName(){
	
		//alert("ApplyFolderName()");
		var action = drpFolderAction.options[drpFolderAction.selectedIndex].value;
		var NewFolderName = txtFolderName.value;
		var FolderID = GetSelectedFolderID();
		var msg = "";

		//alert(FolderID);

		//If we are showing the non-existant "virtual" folder "My Folder",
		//create it first, and then rename it.
		if(action=="rename" && FolderID==0)
			action="add_and_rename";

		switch(action){
			case "new":
				aci.matweb.WebServices.Folders.AddFolder(NewFolderName,AddRenameFolder_CALLBACK,ErrorHandler,action);
				break;
			case "rename":
				gOldFolderName = drpFolderList.options[drpFolderList.selectedIndex].text;
				aci.matweb.WebServices.Folders.RenameFolder(FolderID,NewFolderName,AddRenameFolder_CALLBACK,ErrorHandler,action);
				break;
			case "add_and_rename":
				gOldFolderName = drpFolderList.options[drpFolderList.selectedIndex].text;
				aci.matweb.WebServices.Folders.AddFolder(NewFolderName,AddRenameFolder_CALLBACK,ErrorHandler,action);
				break;
			default: 
				alert("Unhandled Action in ApplyFolderName(): " + action);
		}

		HideFolderNameControls();
		
	}
	
	//=================================
	// CALLBACK FUNCTION
	//=================================
	function AddRenameFolder_CALLBACK(ReturnData,context){
		//alert("AddRenameFolder_CALLBACK()");
		
		var msg;
		//Folder = ReturnData.Folder;
	
		switch(context){
			case "new":
				//gNewFolderID = Folder.FolderID;
				gNewFolderID = ReturnData.NewFolderID;
				msg = "The folder [" + ReturnData.NewFolderName + "] was created.";
				break;
			case "rename":
				msg = "The folder [" + gOldFolderName+ "] was renamed to [" + ReturnData.NewFolderName + "].";
				break;
			case "add_and_rename":
				//gNewFolderID = Folder.FolderID;
				gNewFolderID = ReturnData.NewFolderID;
				msg = "The folder [" + gOldFolderName+ "] was renamed to [" + ReturnData.NewFolderName + "].";
				break;
		}
		
		divUserMessage.innerHTML = msg;
		
		//refresh the folder list, so it includes the new folder
		aci.matweb.WebServices.Folders.GetFolderList(GetFolderList_CALLBACK,ErrorHandler,"AddFolder");

	}
	
	//=================================
	function HideFolderNameControls(){
	//=================================
		//These fields will not exist for anonymous users
		if(drpFolderAction){
			drpFolderAction.selectedIndex=0;
			divFolderName.style.display="none";
			tblFolderName.style.display="none";
		}
	}
	
	//=================================
	function GetSelectedFolderID(){
	//=================================
		var FolderID = 0;
		//if one of the processeses created a new folder id, then use it.
		// alert('gNewFolderID=' + gNewFolderID);
		if(gNewFolderID > 0){
			FolderID = gNewFolderID;
			//gNewFolderID = 0;//clear the new folder id, so we don't use it again.
		}
		else 
			if(drpFolderList != null){
				if(drpFolderList.options.length == 0){
					FolderID = gSelectedFolderID;
				}
				else{
					var SelectedOption = drpFolderList.options[drpFolderList.selectedIndex];  
					FolderID = SelectedOption.value; //this will be 0 if no folders exist yet
				}
			}
		return FolderID;
	}

	//=================================
	function UnregisteredUserMessage(chkBox){
	//=================================
		matweb.alert("This feature is reserved for registered users.<p/><a href='/membership/regstart.aspx'>Click here to register.</a>","Reserved Feature");
		chkBox.checked = false;
	}

	// =================================
	// CALLBACK FUNCTION
	// The folder web service returns a JS array of iBatis data objects of type: aci.matweb.data.folder.Folder 
	// The drop down list of folders is set using this list.
	// =================================
	function GetFolderList_CALLBACK(iFolderList,Context){
	
		//alert(iFolderList);
		//alert('iFolderList.length: ' + iFolderList.length);

		if(drpFolderList != null){
		
		var SelectedFolderID = GetSelectedFolderID();//remember the current selected folder, so we can use it later
		var opt;
		var folder;

		//clear the options and reload them
		drpFolderList.options.length = 0;

		//add a default empty folder...		
		if(iFolderList.length==0){
			opt = document.createElement('OPTION');
			opt.value = "0";
			opt.text = "My Folder";
			drpFolderList.options.add(opt);
			i++;
		}
		
		for(var i = 0;i < iFolderList.length;i++){
			opt = document.createElement('OPTION');
			folder = iFolderList[i];
			//alert(folder.FolderID);
			//alert(folder.FolderName);
			opt.value = folder.FolderID;
			opt.text = folder.FolderName;
			if(folder.FolderID == SelectedFolderID) opt.selected = true;
			drpFolderList.options.add(opt);
		}

		//clear the new folder id, so we don't use it again. 
		gNewFolderID = 0;
		
		//Set checkboxes and folder count fields for the new selected folder
		SetSelectedFolder();
		}

	}
	
	// =================================
	// ON ERROR CALLBACK FUNCTION
	// =================================
	function ErrorHandler(error,context) {
	
			var stackTrace = error.get_stackTrace();
			var message = error.get_message();
			var statusCode = error.get_statusCode();
			var exceptionType = error.get_exceptionType();
			var timedout = error.get_timedOut();
			var Debug = false;

			// Compose the error message...
			var ErrMsg =     
					"<strong>Stack Trace</strong><br/>" +  stackTrace + "<br/>" +
					"<strong>WebService Error</strong><br/>" + message + "<br/>" +
					"<strong>Status Code</strong><br/>" + statusCode + "<br/>" +
					"<strong>Exception Type</strong><br/>" + exceptionType + "<br/>" +
					"<strong>JS Context</strong><br/>" + context + "<br/>" +
					"<strong>Timedout</strong><br/>" + timedout + "<br/>" +
					"<strong>Source Page</strong><br/>" + "/search/MaterialGroupSearch.aspx";
			//matweb.alert(ErrMsg,"Error in WebService",null,"500");
			if(Debug){
				matweb.alert(ErrMsg,"Error in WebService",null,"500");
				//alert(ErrMsg);
			}
			else{
				ErrMsg = escape(ErrMsg);
				//matweb.alert(ErrMsg);
				//window.location.href="/errorJS.aspx?ErrMsg=" + ErrMsg;
				divUserMessage.innerHTML = "Problem retrieving folder data...Some folder features may not work.";
			}
		
	}
	
	function ShowInterpolationMessage(){
		InterpolationMsg = "The material was found via interpolation, so no data points can be displayed. See the material data sheet for actual data points.";
		matweb.alert(InterpolationMsg);
	}
	
</script>

		</td>
	</tr>
	</table>

<hr />
<a id="help"></a>
<table><tr><td>
<p/><b>Instructions:</b> This page allows you to quickly access all of the polymers/plastics, metals, ceramics, fluids, and other engineering materials
in the MatWeb material property database.&nbsp; Just select the material category you would like to find from the tree.  Click on the [+] icon to open subcategory branches in the tree.
Then click the <b>'Find'</b> button next to its box.&nbsp; You can then follow the links to the most complete data sheets on the Web, with information on over 1000 available properties, including dielectric
constant, Poisson's ratio, glass transition temperature, dissipation factor, and Vicat softening point.
</td><td><a href="https://proto3000.com"><img src="/images/assets/proto3000.jpg" border = "0"><br>

Proto3000 - Rapid Prototyping</a>
</td></tr></table>

<p/><span class="hiText">Notes:</span>

<ul>
  <li>Visit MatWeb's <a href="/search/PropertySearch.aspx">property-based search page</a> to limit your results to materials that meet specific property performance criteria.</li>
  <li>Text searches can be done from any page - use the <b>Quick Search</b> box in the top right of the page.</li>
  <li>More <a href="/help/strategy.aspx"><strong>Search Strategies</strong></a> on how to find information in MatWeb.</li>
   <li><a href="/search/GetAllMatls.aspx">Show All Materials</a> - an inefficient drill-down to data sheets.  Registered users who are logged in omit a step in this drill down process.</li>
</ul>

<p/><a href="#Top">Top</a>

<script type="text/javascript">

	var txtMatGroupText = document.getElementById("ctl00_ContentMain_txtMatGroupText");
	var txtMatGroupID = document.getElementById("ctl00_ContentMain_txtMatGroupID");
	var divMatGroupName = document.getElementById("ctl00_ContentMain_divMatGroupName");

	function ucMatGroupFinder1_OnSelected(MatGroupID,MatGroupText){
		txtMatGroupText.value = MatGroupText;
		txtMatGroupID.value = MatGroupID;
		divMatGroupName.innerHTML = MatGroupText;
		//__doPostBack("ctl00$ContentMain$btnSubmit","");
	}

	function ucMatGroupTree_OnNodeClick(NodeText,MatGroupID){
		txtMatGroupText.value = NodeText;
		txtMatGroupID.value = MatGroupID;
		divMatGroupName.innerHTML = NodeText;
		//__doPostBack("ctl00$ContentMain$btnSubmit","");
	}
	
	function validate(){
		if (txtMatGroupID.value == ""){
			matweb.alert("Please select a material category","User Input Error");
			return false;
		}
		
		return true;
		
	}

</script>


</div>
<!-- ===================================== END MAIN CONTENT ========================================== -->

<table class="tabletight" style="width:100%" >
        <tr>
                <td align="center" class="footer" colspan="3">
                        <br /><br />
                
                        <a href="/membership/regupgrade.aspx" class="footlink"><span class="subscribeLink">Subscribe to Premium Services</span></a><br/>
                
                        <span class="footer"><b>Searches:</b>&nbsp;&nbsp;</span>
                        <a href="/search/AdvancedSearch.aspx" class="footlink"><span class="foot">Advanced</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/search/CompositionSearch.aspx" class="footlink"><span class="foot">Composition</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/search/PropertySearch.aspx" class="footlink"><span class="foot">Property</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/search/MaterialGroupSearch.aspx" class="footlink"><span class="foot">Material&nbsp;Type</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/search/SearchManufacturerName.aspx" class="footlink"><span class="foot">Manufacturer</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/search/SearchTradeName.aspx" class="footlink"><span class="foot">Trade&nbsp;Name</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/search/SearchUNS.aspx" class="footlink"><span class="foot">UNS Number</span></a>
                        <br />
                        <span class="footer"><b>Other&nbsp;Links:</b>&nbsp;&nbsp;</span>
                        <a href="/services/advertising.aspx" class="footlink"><span class="foot">Advertising</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/services/submitdata.aspx" class="footlink"><span class="foot">Submit&nbsp;Data</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/services/databaselicense.aspx" class="footlink"><span class="foot">Database&nbsp;Licensing</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/services/webhosting.aspx" class="footlink"><span class="foot">Web&nbsp;Design&nbsp;&amp;&nbsp;Hosting</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/clickthrough.aspx?addataid=277" class="footlink"><span class="foot">Trade&nbsp;Publications</span></a>

                        <br />
                        <a href="/reference/suppliers.aspx" class="footlink"><span class="foot">Supplier&nbsp;List</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/tools/unitconverter.aspx" class="footlink"><span class="foot">Unit&nbsp;Converter</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/tools/contents.aspx#reference" class="footlink"><span class="foot">Reference</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/reference/link.aspx" class="footlink"><span class="foot">Links</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/help/help.aspx" class="footlink"><span class="foot">Help</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/services/contact.aspx" class="footlink"><span class="foot">Contact&nbsp;Us</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/tools/contents.aspx" class="footlink"><span class="foot" style="color:red;">Site&nbsp;Map</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/reference/faq.aspx" class="footlink"><span class="foot">FAQ</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/index.aspx" class="footlink"><span class="foot">Home</span></a>
                        <br />&nbsp;
                        <div id="fb-root"></div>

<table><tr><td><script src="http://connect.facebook.net/en_US/all.js#xfbml=1"></script><fb:like-box href="https://www.facebook.com/MatWeb.LLC" width="175" show_faces="false" stream="false" header="false"></fb:like-box>

</td><td>

<a href="https://twitter.com/MatWeb" class="twitter-follow-button" data-show-count="false" data-show-screen-name="false">Follow @MatWeb</a> <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>

</td></tr></table>
                </td>
        </tr>
        <tr>
                <td colspan="3"><img src="/images/bluebar.gif" width="100%" height="5" alt="" /></td>
        </tr>
        <tr>
                <td style="background-image:url(/images/gray.gif);"><img src="/images/spacer.gif" width="5" height="1" alt=""/></td>
                <td style="background-image:url(/images/gray.gif);">
                        <span class="footer">
                        Please read our <a href="/reference/terms.aspx" class="footlink"><span class="foot">License Agreement</span></a> regarding materials data and our  <a href="/reference/privacy.aspx" class="footlink"><span class="foot">Privacy Policy</span></a>.
                        Questions or comments about MatWeb? Please contact us at
                        <a href="mailto:webmaster@matweb.com" class="footlink"><span class="foot">webmaster@matweb.com</span></a>. We appreciate your input.
                        <br /><br />
                        The contents of this web site, the MatWeb logo, and "MatWeb" are Copyright 1996-2024
                        by MatWeb, LLC. MatWeb is intended for personal, non-commercial use. The contents, results, and technical data from this site
                        may not be reproduced either electronically, photographically or substantively without permission from MatWeb, LLC.
                        </span>
                        <br />
                </td>

                <td style="background-image:url(/images/gray.gif);"><img src="/images/spacer.gif" width="5" height="1" alt=""/></td>
        </tr>
</table>




<script type="text/javascript">
//<![CDATA[
var ctl00_ContentMain_ucMatGroupTree_msTreeView_ImageArray =  new Array('', '', '', '/WebResource.axd?d=zLp3QJeZSj4-fsnL2_raV70Mk3pr-OdjL_rvrZzXUTCnZ6LU5Dg_Tc5XZg4eZpT9O0gxZJrJcxGb23XJofeGD9JhYLr2wl0VkQZnJOx0T0ShBosq0&t=638313619312541215', '/WebResource.axd?d=vFualGN2ZZ4fny_RX_gYiUTY6FBLUapne4VfY6P3sGiyrOYd-Ji2sWg_olT-exaztendUUEOoH87RS2l44bpZTscfmmv4MKRqJK8rgesmkrNfP2O0&t=638313619312541215', '/WebResource.axd?d=4nLCbQ2Yfc2i_msrQXqKzK8pLQD-GrNVZ7DeXU-La2qatczrgeCLazv0RzIkDDHyYfLFalCBO8s71lRS8yifxTChF9dUJvTLxNEeeFlghyuY0dtM0&t=638313619312541215', '/WebResource.axd?d=uEI6Enq15_q4pXO6g0E7vtAqtD5WIokAEQ2Vz8V3Yi36cefediLfyCwENkPd-w1GxTXu-HkNqcDlrYOw8bxgGqeHJtzc-PBlTa9nrZRfdIyj8fQh0&t=638313619312541215', '/WebResource.axd?d=hzeik1gGhZrfDZXUWCMsBOx5b43bIFor8UCTzm_wuh92b9Z-WQU4_fQCSUw6CaP5tnc9LTU0jl0sy4Y6Cf_c8q8HFXRqeHQuscXIqqLNSZkgl2cu0&t=638313619312541215', '/WebResource.axd?d=6v5qM6VKQYD3XNwwXEy7Tix7dzVdqDJ8QTRIlUPrzXQqNsXQgUwIfmn34ZzRORd_IZj0bF4vkZzID1HuSFE5uKyLgaPYeAYE8VT8HYfb3Rk6zsDw0&t=638313619312541215', '/WebResource.axd?d=d9nh8nEM1YCqIfE9jLzNhPyFIZqARvlFExwnFG0vbvskO-A2qORlhTyUVIxDtSwldGrOp8o9ef8lZ9SRhPQ9rv9TkGV-NVYvH9_yHT6CvWpIPF4S0&t=638313619312541215', '/WebResource.axd?d=7_V5n1KYSQsj6PoSqgIxb0egBsAa91jSXPkbmM6FaiF14-JsNnpUDnfSdxZcvfd0OQCLDtC24pMIXigO-41cL2rsq-0xea-SSG7yZUyRcliFFqe70&t=638313619312541215', '/WebResource.axd?d=TinC6igX1vPDgWh7DstWwBTHTyH4uuybDU4NW47jJD06wHdAO3YyJC_eKK1Zn5oIbwqRo4Cri8YwhA82G-LD0p14yEF1EfoIZvKleYMIuQ2K1hkE0&t=638313619312541215', '/WebResource.axd?d=4B0dvJqmzY4ukTboQq7lmxokRL9NTsoWh4yErB11Tb-ucvMw_p_0mxYVn4U1Uz6Jk795umrO0C4-B-8Yq1Kxxz6uyDT15O2BSCb8349f4YWDdIVy0&t=638313619312541215', '/WebResource.axd?d=TUiibr9hjS_MlNf09dKZECe1z56fbaPbfgJfC5eYJi0TtdBviUcA1JQJLV81aqunzpXysgruhL3ihpgd7ziL5p9SQ86xVLNiUqbYpJnW9Rj7sN1E0&t=638313619312541215', '/WebResource.axd?d=vUBrM2nNhECNbFO0AJKq52p2zWrY5XG8nWgCDL4AfsrexStb0HREDV6GHMvdkAGDInxYtTyWXuxFUIiQv3JX2fScVRwQsrVv5srHrjOIeC3ayAG20&t=638313619312541215', '/WebResource.axd?d=Z-LP5lKcsnv2A9UpExXu-5leQYIAZMYp6SXNognZi1r66YT8u24-YjJaxjisE9ruYU8NKnNz72H4shclsk8PM3S5M-VYMk_F_G5fcd3ra6Sj9CaS0&t=638313619312541215', '/WebResource.axd?d=QI4SACqRn7PxBBHPfkR7peFMAEv7lX1_iZ_7jncKwxRTlvhSAncd5K2oRqw8J8WmJjWQcbGgEblhrufy-hUjZMa1YLUC47mjfV1eS4LM3kUqLT960&t=638313619312541215', '/WebResource.axd?d=ayrFqNSzdiTAdowDVbEH-ZzKbvw5bCq7onyCSAcJhCVPVIHlUB9M_fo8BLUTeGpbtcoMiyFy1MoeEIsweMrpVandBKC4vAK-NCBcfn2FIreK8mEJ0&t=638313619312541215', '/WebResource.axd?d=OK_uWiuoGURaJ3CC0fVlQ7v9GUnIAkx03w03rQ2ENqcwdObLiQwK7B9yRFyAWj9tuUqsa0874ksgkaRo8KirB7q1ZL_huh7gCq_XehXfHMEKMiAl0&t=638313619312541215');
//]]>
</script>


<script type="text/javascript">
//<![CDATA[
(function() {var fn = function() {AjaxControlToolkit.ModalPopupBehavior.invokeViaServer('ctl00_ContentMain_ucPopupMessage1_ModalPopupExtender1', false); Sys.Application.remove_load(fn);};Sys.Application.add_load(fn);})();
var callBackFrameUrl='/WebResource.axd?d=Mt7MojvAyTqAEcx0MpKgcF_QVa09tLyMQS6c5JLNO4j2M_F6CMxYnN0HdVYxyrhggOpkTwyNqQ51iS6UoHXgJqHBROE1&t=638313619312541215';
WebForm_InitCallback();var ctl00_ContentMain_ucMatGroupTree_msTreeView_Data = new Object();
ctl00_ContentMain_ucMatGroupTree_msTreeView_Data.images = ctl00_ContentMain_ucMatGroupTree_msTreeView_ImageArray;
ctl00_ContentMain_ucMatGroupTree_msTreeView_Data.collapseToolTip = "Collapse {0}";
ctl00_ContentMain_ucMatGroupTree_msTreeView_Data.expandToolTip = "Expand {0}";
ctl00_ContentMain_ucMatGroupTree_msTreeView_Data.expandState = theForm.elements['ctl00_ContentMain_ucMatGroupTree_msTreeView_ExpandState'];
ctl00_ContentMain_ucMatGroupTree_msTreeView_Data.selectedNodeID = theForm.elements['ctl00_ContentMain_ucMatGroupTree_msTreeView_SelectedNode'];
for (var i=0;i<19;i++) {
var preLoad = new Image();
if (ctl00_ContentMain_ucMatGroupTree_msTreeView_ImageArray[i].length > 0)
preLoad.src = ctl00_ContentMain_ucMatGroupTree_msTreeView_ImageArray[i];
}
ctl00_ContentMain_ucMatGroupTree_msTreeView_Data.lastIndex = 8;
ctl00_ContentMain_ucMatGroupTree_msTreeView_Data.populateLog = theForm.elements['ctl00_ContentMain_ucMatGroupTree_msTreeView_PopulateLog'];
ctl00_ContentMain_ucMatGroupTree_msTreeView_Data.treeViewID = 'ctl00$ContentMain$ucMatGroupTree$msTreeView';
ctl00_ContentMain_ucMatGroupTree_msTreeView_Data.name = 'ctl00_ContentMain_ucMatGroupTree_msTreeView_Data';
Sys.Application.initialize();
Sys.Application.add_init(function() {
    $create(AjaxControlToolkit.ModalPopupBehavior, {"BackgroundCssClass":"modalBackground","OkControlID":"ctl00_ContentMain_ucPopupMessage1_btnOK","PopupControlID":"ctl00_ContentMain_ucPopupMessage1_divPopupMessage","PopupDragHandleControlID":"ctl00_ContentMain_ucPopupMessage1_trTitleRow","dynamicServicePath":"/search/MaterialGroupSearch.aspx","id":"ctl00_ContentMain_ucPopupMessage1_ModalPopupExtender1"}, null, null, $get("ctl00_ContentMain_ucPopupMessage1_hndPopupControl"));
});
Sys.Application.add_init(function() {
    $create(AjaxControlToolkit.TextBoxWatermarkBehavior, {"ClientStateFieldID":"ctl00_ContentMain_UcMatGroupFinder1_TextBoxWatermarkExtender2_ClientState","WatermarkCssClass":"greyOut","WatermarkText":"Type at least 4 characters here...","id":"ctl00_ContentMain_UcMatGroupFinder1_TextBoxWatermarkExtender2"}, null, null, $get("ctl00_ContentMain_UcMatGroupFinder1_txtSearchText"));
});
//]]>
</script>
</form>


<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-15290815-1");
pageTracker._setDomainName(".matweb.com");
pageTracker._trackPageview();
} catch(err) {}</script>

<!-- 20160905 insert SpecialChem javascript -->
<script type="text/javascript" src="//collect.specialchem.com/collect.js"></script>
<script type="text/javascript">
    //<![CDATA[
    var _spc = (_spc || []);
    _spc.push(['init',
    {
        partner: 'MatWeb'
        
        //, iid: '12345'
    }]);
    //]]>
</script>
<script> (function(){ var s = document.createElement('script'); var h = document.querySelector('head') || document.body; s.src = 'https://acsbapp.com/apps/app/dist/js/app.js'; s.async = true; s.onload = function(){ acsbJS.init({ statementLink : '', footerHtml : '', hideMobile : false, hideTrigger : false, disableBgProcess : false, language : 'en', position : 'right', leadColor : '#146FF8', triggerColor : '#146FF8', triggerRadius : '50%', triggerPositionX : 'right', triggerPositionY : 'bottom', triggerIcon : 'people', triggerSize : 'bottom', triggerOffsetX : 20, triggerOffsetY : 20, mobile : { triggerSize : 'small', triggerPositionX : 'right', triggerPositionY : 'bottom', triggerOffsetX : 20, triggerOffsetY : 20, triggerRadius : '20' } }); }; h.appendChild(s); })();</script>
</body>
</html>
