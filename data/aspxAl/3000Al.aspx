

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head id="ctl00_mainHeader"><title>
	Metal, Plastic, and Ceramic Search Index
</title><meta http-equiv="Content-Style-Type" content="text/css" /><meta http-equiv="Content-Language" content="en" />

        <script src="/includes/flash.js" type="text/javascript"></script>

        <script src="/includes/prototype.js" type="text/javascript"></script>
        <!-- script src="/includes/scriptaculous/effects.js" type="text/javascript"></script -->
        <script src="/includes/matweb.js" type="text/javascript"></script>
        <link rel="stylesheet" href="/includes/main.css" />

        <script type="text/javascript">

                function _onload(){
                        matweb.debug = false; // ;
                        //matweb.writeOverlay();
                }

                function _onkeydown(evt){
                        //PREVENT GLOBAL AUTO-SUBMIT OF FORMS
                        return matweb.DisableAutoSubmit(evt);
                }

                //EXAMPLE USAGE:
                //matweb.register_onload(function(){alert("hello from onload event");});
                //matweb.writeOverlay();


        </script>

        <style type="text/css" media="screen">
        @import url("/ad_IES/css/styles.css");
        </style>
<meta name="KEYWORDS" content="carbon steels, low alloy steels, corrosion resistant stainless, high temperature stainless, heat resistant stainless steels, AISI steel alloy specifications, high strength steel alloys, tensile strength of steels, maraging, superalloy, lead alloy, magnesium, tin alloy, tungsten, zinc, precious metal, pure metallic element, TPE elastomer, dielectric constant, plastic dissipation factor, nylon glass transition temperature, ionomer vicat softening point, poisson's ratio, polyetherimide, fluoropolymer, silicone, polyester dielectric constant" /><meta name="DESCRIPTION" content="Searchable list of plastics, metals, and ceramics categorized into a fields like polypropylene, nylon, ABS, aluminum alloys, oxides, etc.  Search results are detailed data sheets with tensile strength, density, dielectric constant, dissipation factor, poisson's ratio, glass transition temperature, and Vicat softening point." /><meta name="ROBOTS" content="NOFOLLOW" /><style type="text/css">
	.ctl00_ContentMain_ucMatGroupTree_msTreeView_0 { text-decoration:none; }
	.ctl00_ContentMain_ucMatGroupTree_msTreeView_1 { color:Black; }
	.ctl00_ContentMain_ucMatGroupTree_msTreeView_2 { padding:0px 3px 0px 3px; }

</style></head>

<body onload="_onload();" onkeydown="return _onkeydown(event);">

<a id="Top"></a>

<div id="divSiteName" style="position:absolute; top:0; left:0 ;">
<a href="/index.aspx"><img src="/images/sitelive.gif" alt="" /></a>
</div>

<div id="divPopupOverlay" style="display:none;"></div>

<div id="divPopupAlert" class="popupAlert" style="display:none;">
        <table style="width:100%; height:100%; border-style:outset; border-width:5px; border-color:blue;" >
                <tr><th style=" height:25px; background-color:Maroon; font-size:14px; color:White; "><span id="spanPopupAlertTitle">System Message</span></th></tr>
                <tr><td style=" vertical-align:top; padding:3px; background-color:White;" id="tdPopupAlertMessage" ></td></tr>
                <tr><td style=" height:25px; text-align:center; background-color:Silver;"><input type="button" id="btnPopupAlert" onclick="matweb.alertClose();" style="" value="OK" /></td></tr>
        </table>
</div>

<form name="aspnetForm" method="post" action="MaterialGroupSearch.aspx" onsubmit="javascript:return WebForm_OnSubmit();" id="aspnetForm">
<div>
<input type="hidden" name="__EVENTTARGET" id="__EVENTTARGET" value="" />
<input type="hidden" name="__EVENTARGUMENT" id="__EVENTARGUMENT" value="" />
<input type="hidden" name="__LASTFOCUS" id="__LASTFOCUS" value="" />
<input type="hidden" name="ctl00_ContentMain_ucMatGroupTree_msTreeView_ExpandState" id="ctl00_ContentMain_ucMatGroupTree_msTreeView_ExpandState" value="ccccccnc" />
<input type="hidden" name="ctl00_ContentMain_ucMatGroupTree_msTreeView_SelectedNode" id="ctl00_ContentMain_ucMatGroupTree_msTreeView_SelectedNode" value="" />
<input type="hidden" name="ctl00_ContentMain_ucMatGroupTree_msTreeView_PopulateLog" id="ctl00_ContentMain_ucMatGroupTree_msTreeView_PopulateLog" value="" />
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="/wEPDwULLTE2Nzc3MDc2MDAPZBYCZg9kFgICAQ9kFioCAQ9kFgJmDxYCHgRUZXh0BWdEYXRhIHNoZWV0cyBmb3Igb3ZlciA8c3BhbiBjbGFzcz0iZ3JlZXRpbmciPjE4MCwwMDA8L3NwYW4+ICBtZXRhbHMsIHBsYXN0aWNzLCBjZXJhbWljcywgYW5kIGNvbXBvc2l0ZXMuZAICD2QWAmYPFgIfAAUESE9NRWQCAw9kFgJmDxYCHwAFBlNFQVJDSGQCBA9kFgJmDxYCHwAFBVRPT0xTZAIFD2QWAmYPFgIfAAUJU1VQUExJRVJTZAIGD2QWAmYPFgIfAAUHRk9MREVSU2QCBw9kFgJmDxYCHwAFCEFCT1VUIFVTZAIID2QWAmYPFgIfAAUDRkFRZAIJD2QWAmYPFgIfAAUHQUNDT1VOVGQCCg9kFgJmDxYCHwAFBkxPRyBJTmQCCw8QZGQWAWZkAgwPZBYCZg8WAh8ABQhTZWFyY2hlc2QCDQ9kFgJmDxYCHwAFCEFkdmFuY2VkZAIOD2QWAmYPFgIfAAUIQ2F0ZWdvcnlkAg8PZBYCZg8WAh8ABQhQcm9wZXJ0eWQCEA9kFgJmDxYCHwAFBk1ldGFsc2QCEQ9kFgJmDxYCHwAFClRyYWRlIE5hbWVkAhIPZBYCZg8WAh8ABQxNYW51ZmFjdHVyZXJkAhMPZBYCAgEPZBYCZg8WAh8ABRlSZWNlbnRseSBWaWV3ZWQgTWF0ZXJpYWxzZAIVD2QWEAIDD2QWAmYPFgIfAAWZATxhIGhyZWY9Ii9jbGlja3Rocm91Z2guYXNweD9hZGRhdGFpZD0xMzQ5Ij48aW1nIHNyYz0iL2ltYWdlcy9hc3NldHMvbWV0YWxtZW4tQWx1bWludW0ucG5nIiBib3JkZXI9MCBhbHQ9Ik1ldGFsbWVuIFNhbGVzIiB3aWR0aCA9ICI3MjgiIGhlaWdodCA9ICI5MCI+PC9hPmQCBA9kFgICAg8WAh4Fc3R5bGUFGWRpc3BsYXk6bm9uZTt3aWR0aDoyNTBweDsWBgIBD2QWAmYPZBYCZg8PFgIfAAUSVXNlciBJbnB1dCBQcm9ibGVtZGQCAw8PFgIfAGVkZAIFDxYCHgV2YWx1ZQUCT0tkAgUPZBYEAgEPD2QWAh8BBQxXaWR0aDozMDRweDtkAgIPEBYEHwEFK3Bvc2l0aW9uOmFic29sdXRlO2Rpc3BsYXk6bm9uZTtXaWR0aDozMDhweDseBHNpemUFAjE4ZGRkAgYPDxYEHghDbGllbnRJRAIBHgpMYW5ndWFnZUlEAgFkFgJmDxQrAAkPFg4eF1BvcHVsYXRlTm9kZXNGcm9tQ2xpZW50Zx4SRW5hYmxlQ2xpZW50U2NyaXB0Zx4JU2hvd0xpbmVzZx4ITm9kZVdyYXBnHg1OZXZlckV4cGFuZGVkZB4MU2VsZWN0ZWROb2RlZB4JTGFzdEluZGV4AghkFggeC05vZGVTcGFjaW5nGwAAAAAAAAAAAQAAAB4JRm9yZUNvbG9yCiMeEUhvcml6b250YWxQYWRkaW5nGwAAAAAAAAhAAQAAAB4EXyFTQgKEgBhkZGRkZGQUKwAJBSMyOjAsMDowLDA6MSwwOjIsMDozLDA6NCwwOjUsMDo2LDA6NxQrAAIWDB8ABRJDYXJib24gKDg2NiBtYXRscykeBVZhbHVlBQMyODMeCEV4cGFuZGVkaB4MU2VsZWN0QWN0aW9uCyouU3lzdGVtLldlYi5VSS5XZWJDb250cm9scy5UcmVlTm9kZVNlbGVjdEFjdGlvbgIeC05hdmlnYXRlVXJsBUFqYXZhc2NyaXB0OnVjTWF0R3JvdXBUcmVlX09uTm9kZUNsaWNrKCdDYXJib24gKDg2NiBtYXRscyknLCcyODMnKR4QUG9wdWxhdGVPbkRlbWFuZGdkFCsAAhYMHwAFFUNlcmFtaWMgKDEwMDA0IG1hdGxzKR8RBQIxMR8SaB8TCysEAh8UBUNqYXZhc2NyaXB0OnVjTWF0R3JvdXBUcmVlX09uTm9kZUNsaWNrKCdDZXJhbWljICgxMDAwNCBtYXRscyknLCcxMScpHxVnZBQrAAIWDB8ABRJGbHVpZCAoNzU2MiBtYXRscykfEQUBNR8SaB8TCysEAh8UBT9qYXZhc2NyaXB0OnVjTWF0R3JvdXBUcmVlX09uTm9kZUNsaWNrKCdGbHVpZCAoNzU2MiBtYXRscyknLCc1JykfFWdkFCsAAhYMHwAFE01ldGFsICgxNzA1MiBtYXRscykfEQUBOR8SaB8TCysEAh8UBUBqYXZhc2NyaXB0OnVjTWF0R3JvdXBUcmVlX09uTm9kZUNsaWNrKCdNZXRhbCAoMTcwNTIgbWF0bHMpJywnOScpHxVnZBQrAAIWDB8ABSdPdGhlciBFbmdpbmVlcmluZyBNYXRlcmlhbCAoODA2MyBtYXRscykfEQUDMjg1HxJoHxMLKwQCHxQFVmphdmFzY3JpcHQ6dWNNYXRHcm91cFRyZWVfT25Ob2RlQ2xpY2soJ090aGVyIEVuZ2luZWVyaW5nIE1hdGVyaWFsICg4MDYzIG1hdGxzKScsJzI4NScpHxVnZBQrAAIWDB8ABRVQb2x5bWVyICg5NzYzNSBtYXRscykfEQUCMTAfEmgfEwsrBAIfFAVDamF2YXNjcmlwdDp1Y01hdEdyb3VwVHJlZV9Pbk5vZGVDbGljaygnUG9seW1lciAoOTc2MzUgbWF0bHMpJywnMTAnKR8VZ2QUKwACFgwfAAUYUHVyZSBFbGVtZW50ICg1MDcgbWF0bHMpHxEFAzE4NB8SaB8TCysEAh8UBUdqYXZhc2NyaXB0OnVjTWF0R3JvdXBUcmVlX09uTm9kZUNsaWNrKCdQdXJlIEVsZW1lbnQgKDUwNyBtYXRscyknLCcxODQnKR8VaGQUKwACFgwfAAUlV29vZCBhbmQgTmF0dXJhbCBQcm9kdWN0cyAoMzk4IG1hdGxzKR8RBQMyNzcfEmgfEwsrBAIfFAVUamF2YXNjcmlwdDp1Y01hdEdyb3VwVHJlZV9Pbk5vZGVDbGljaygnV29vZCBhbmQgTmF0dXJhbCBQcm9kdWN0cyAoMzk4IG1hdGxzKScsJzI3NycpHxVnZGQCBw8WAh4JaW5uZXJodG1sBSUzMDAwIFNlcmllcyBBbHVtaW51bSBBbGxveSAoOTIgbWF0bHMpZAINDw8WAh4HVmlzaWJsZWhkZAIODw8WAh8XZ2QWBAIBDw8WAh8ABQI5MmRkAgMPDxYCHwAFGjMwMDAgU2VyaWVzIEFsdW1pbnVtIEFsbG95ZGQCDw8PFgYeC0N1cnJlbnRQYWdlAgEeD0N1cnJlbnRTZWFyY2hJRAK+xvwSHxdnZBYEAgIPDxYCHwBlZGQCAw8PFgIfF2dkFj4CAQ8PFgIfAAUCOTJkZAIDDxBkEBUBATEVAQExFCsDAWcWAWZkAgUPDxYCHwAFATFkZAIHDw8WCB8ABQtbUHJldiBQYWdlXR8OCk4eB0VuYWJsZWRoHxACBGRkAgkPDxYIHwAFC1tOZXh0IFBhZ2VdHw4KTh8QAgQfGmhkZAILDxBkZBYBAgVkAg0PFgIfF2hkAg8PFgIfF2hkAhEPEA9kFgIeCG9uY2hhbmdlBRNTZXRTZWxlY3RlZEZvbGRlcigpZGRkAhUPFgIfF2gWAgIBDxBkZBYBZmQCFw9kFgQCAQ8PFgQfAGUfGmhkZAICDw8WAh8XaGRkAhkPDxYEHwAFDU1hdGVyaWFsIE5hbWUfGmhkZAIbDw8WAh8XaGRkAh0PDxYCHxdoZGQCHw9kFgYCAQ8PFgQfAAUFUHJvcDEfGmhkZAIDDw8WAh8XaGRkAgUPDxYCHxdoZGQCIQ9kFgYCAQ8PFgQfAAUFUHJvcDIfGmhkZAIDDw8WAh8XaGRkAgUPDxYCHxdoZGQCIw9kFgYCAQ8PFgQfAAUFUHJvcDMfGmhkZAIDDw8WAh8XaGRkAgUPDxYCHxdoZGQCJQ9kFgYCAQ8PFgQfAAUFUHJvcDQfGmhkZAIDDw8WAh8XaGRkAgUPDxYCHxdoZGQCJw9kFgYCAQ8PFgQfAAUFUHJvcDUfGmhkZAIDDw8WAh8XaGRkAgUPDxYCHxdoZGQCKQ9kFgYCAQ8PFgQfAAUFUHJvcDYfGmhkZAIDDw8WAh8XaGRkAgUPDxYCHxdoZGQCKw9kFgYCAQ8PFgQfAAUFUHJvcDcfGmhkZAIDDw8WAh8XaGRkAgUPDxYCHxdoZGQCLQ9kFgYCAQ8PFgQfAAUFUHJvcDgfGmhkZAIDDw8WAh8XaGRkAgUPDxYCHxdoZGQCLw9kFgYCAQ8PFgQfAAUFUHJvcDkfGmhkZAIDDw8WAh8XaGRkAgUPDxYCHxdoZGQCMQ9kFgYCAQ8PFgQfAAUGUHJvcDEwHxpoZGQCAw8PFgIfF2hkZAIFDw8WAh8XaGRkAjUPDxYCHwAFAjkyZGQCNw8QZBAVAQExFQEBMRQrAwFnFgFmZAI5Dw8WAh8ABQExZGQCOw8PFggfAAULW1ByZXYgUGFnZV0fDgpOHxpoHxACBGRkAj0PDxYIHwAFC1tOZXh0IFBhZ2VdHw4KTh8QAgQfGmhkZAI/DxBkZBYBAgVkAkEPFgIfF2gWAgIBDw8WAh8ABbgEPGJyIC8+DQpNYXRlcmlhbHMgZmxhZ2dlZCBhcyBkaXNjb250aW51ZWQgKDxpbWcgc3JjPSIvaW1hZ2VzL2J1dHRvbnMvaWNvbkRpc2NvbnRpbnVlZC5qcGciIGFsdD0iIiAvPikgYXJlIG5vIGxvbmdlciBwYXJ0IG9mIHRoZSBtYW51ZmFjdHVyZXLigJlzIHN0YW5kYXJkIHByb2R1Y3QgbGluZSBhY2NvcmRpbmcgdG8gb3VyIGxhdGVzdCBpbmZvcm1hdGlvbi4gIFRoZXNlIG1hdGVyaWFscyBtYXkgYmUgYXZhaWxhYmxlIGJ5IHNwZWNpYWwgb3JkZXIsIGluIGRpc3RyaWJ1dGlvbiBpbnZlbnRvcnksIG9yIHJlaW5zdGF0ZWQgYXMgYW4gYWN0aXZlIHByb2R1Y3QuICBEYXRhIHNoZWV0cyBmcm9tIG1hdGVyaWFscyB0aGF0IGFyZSBubyBsb25nZXIgYXZhaWxhYmxlIHJlbWFpbiBpbiBNYXRXZWIgdG8gYXNzaXN0IHVzZXJzIGluIGZpbmRpbmcgcmVwbGFjZW1lbnQgbWF0ZXJpYWxzLiAgDQo8YnIgLz48YnIgLz4NClVzZXJzIG9mIG91ciBBZHZhbmNlZCBTZWFyY2ggKHJlZ2lzdHJhdGlvbiByZXF1aXJlZCkgbWF5IGV4Y2x1ZGUgZGlzY29udGludWVkIG1hdGVyaWFscyBmcm9tIHNlYXJjaCByZXN1bHRzLmRkAhYPZBYCZg8WAh8ABXA8YSBocmVmPSIvY2xpY2t0aHJvdWdoLmFzcHg/YWRkYXRhaWQ9Mjc3IiBjbGFzcz0iZm9vdGxpbmsiPjxzcGFuIGNsYXNzPSJmb290Ij5UcmFkZSZuYnNwO1B1YmxpY2F0aW9uczwvc3Bhbj48L2E+ZBgBBR5fX0NvbnRyb2xzUmVxdWlyZVBvc3RCYWNrS2V5X18WBAU2Y3RsMDAkQ29udGVudE1haW4kVWNNYXRHcm91cEZpbmRlcjEkc2VsZWN0Q2F0ZWdvcnlMaXN0BStjdGwwMCRDb250ZW50TWFpbiR1Y01hdEdyb3VwVHJlZSRtc1RyZWVWaWV3BRtjdGwwMCRDb250ZW50TWFpbiRidG5TdWJtaXQFGmN0bDAwJENvbnRlbnRNYWluJGJ0blJlc2V0t8ufYDKUchasYRgqgKsKlK8JC0I=" />
</div>

<script type="text/javascript">
//<![CDATA[
var theForm = document.forms['aspnetForm'];
if (!theForm) {
    theForm = document.aspnetForm;
}
function __doPostBack(eventTarget, eventArgument) {
    if (!theForm.onsubmit || (theForm.onsubmit() != false)) {
        theForm.__EVENTTARGET.value = eventTarget;
        theForm.__EVENTARGUMENT.value = eventArgument;
        theForm.submit();
    }
}
//]]>
</script>


<script src="/WebResource.axd?d=M68BSyLvwBMHRYzsSV62mvczr7w5VyKnq2JbgqcAAU1ioThlC6FNEVBncBdaA4oqf8cXPzy7QERzajvFfM67hSR6S0w1&amp;t=638313619312541215" type="text/javascript"></script>


<script src="/WebResource.axd?d=ClzPoKUn1iQnMGy7NeMJw70TvMayZ5w9nNmB7h_dY64i4BFFgjVcppyp69BFqhuzHctzc-rEantAjczzQ6UVMYkaWJw1&amp;t=638313619312541215" type="text/javascript"></script>
<script type="text/javascript">
//<![CDATA[

    function TreeView_PopulateNodeDoCallBack(context,param) {
        WebForm_DoCallback(context.data.treeViewID,param,TreeView_ProcessNodeData,context,TreeView_ProcessNodeData,false);
    }
var ctl00_ContentMain_ucMatGroupTree_msTreeView_Data = null;//]]>
</script>

<script src="/ScriptResource.axd?d=148LWaywfmset9PYVR0m-KYXX9JfyGYGqOtIYBGEAzoJh5VhFBwSI6JxnmoNvgtlAprhvntpc6lTDbRMgdKxckUmc8m2qw_fOCsX0G9LC8ojUpw4cTHWXgfuT_vU6m2Kpv3mt01iCczrTE2-J5X6EeBa2281&amp;t=637769794779625837" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=v_V1UgDrDAchg8Zuc1FL4qkvmjt-bwap7ZxwVGGPleYiJ6zfDOTL6wtCmVvILjbwo_sqb2PMUTNkjO4vp6nd-E7WWXTPgSI5-nvn3YDnHrVk5bgVX4Qv6F0fV4wTSslmaLZ3k98tckojDLWAEI7jm8h0H4d7BoGYxkqsyh8-Iujfrjjm0&amp;t=637769794779625837" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=V7Zma3AISNyGTbHVRPw8svJ4Ub7xUUaeoKwImcvnuDiy-XBR331l3eTjiEP8J_QlX2qgyAHxZFLywNbNyZ50fJLVEOCjx11lIDJOiQxc0oGA3qi7XXiggiwEcIWwzb9j2Is1n1D9sU40Zusk35AtkqRKL3Y1&amp;t=633177911400000000" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=wOguQo_yd3hCG4ajcSXPC6-XdeL0hmYWSMaFjvMXt1CBqR9neJqTXyTfPki4lIz4B29Zz_83btvS12BY6KpBKT-AgP0eaXp7V3h6XRdjiozV5w_n3oQRZg5Yq1QW8UNGj0Cy9jsap_7EcU2M99CapUyVG6I1&amp;t=633177911400000000" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=MAJxSy62Z3EtRwHXuuMc8KC6PFmrPqmkpcwdpcPGeqtqUQyJ3FBQ3cFyYWN1zNID_ngA53aMZdG_dpRu3TRDH8-iqKAjPAcc2BWe63WYGrGbyHc9syJxR6lypTgvRaYmwwEmkLEsaGUniMurHc16yr1VrjmzG3HWOeu9UXNr23Wv2xzh0&amp;t=633177911400000000" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=ukx9eyyt483y_CGgCF1OToQPWzPYyCynhpMvhxN7idzW3WaKjHjpgAOcMgtXZIEcspnbyDEADdRWCSHB-hF39Ycl13KWsqdlOHElw2qgSkGmkL8iNH37htb0BNS0lxw6oHvtqYiVpw-nZeYoQ1NkgX2qXOI1&amp;t=633177911400000000" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=Ovkr-MWDypjnq0C7FVbNgOs-Mt3NaktNVf8SEOX721PoQ5mQqTM6S4igSQL1_PXvh96km3stLiqrorOgm8nnnUU6y6edTkhxmLiwfFtEXQEAAnFFQpUepgsxEd0ahMvWcDfEac0EE7J8iDb2rqSMTxk51ydfY-c-8zcKvo5Mrn83cvfN0&amp;t=633177911400000000" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=pKQF4iQYwkSn5cAwTNDM8ez4TLepowWnai-WD6NVL5h0ay9Qtt0a9vPCaI-KN9OHcv1fL8VnwxEDM6EAVtgZY06V8XJTlVrbmb8WlMvGmhwBeL3_SVtKKOS-QExY8AnVbTFl_wOknplhy8bz-9sIWNCh4XPq1WhRj7ALNAKaeruPhS7d0&amp;t=633177911400000000" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=uM6eUuIuIa_NiCgVrtwgqczDeqTqI37XalY8Ri_W2ZjXEzIKi84afVBjFk8gneIDXcFnw8yjCD1qhE8mzDTzAUSdKnbDte0yC23kMoyvxPyu7vRH9E-6JMZtE8-gELUPmoZdOZJ9nXitHKYBLw2P6skU1IkzFRegK74bETu2FdEtuaeR0&amp;t=633177911400000000" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=ewF5FMb1ckbNCdJ9rpE5xMSXy-WecqVGE8uma1_520KusuW7jTUE2tnPMG7JyR0FDIFqDUMQbsfTZlsQ0sD4YAW8MnN90GKIMPvzaP7eOG3ss8E050_l7Rpnaz7I_U3ITzBXkcJZIOPcI1PxOReT8FSeWmo1&amp;t=633177911400000000" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=3lB_EBCMPyiwzDhzWdoE-i4mLHhkIMbbkaLwMr-U1WcAoDqBHS-oYFn4gz3ZwnpzK_KZp97Ev8ZbiUNo7WMJ45h2xgOH59erMFo1BpNb87-yu3X0mFyWs5rB3ANm4lsVSIu7RZd7XUEwfZXzugJ3CoJqxvRqiTGLpcXquEF24hK3OUH-0&amp;t=633177911400000000" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=Z7-pZSKJ6vCyr9H9S4r9baB7TLiJ-T4EoMOvlH-iQ5-icaO-vWMrjXk3EPctuPrcO5qwJSTaLQtHyguwle7Kty78exrSwHataIriNUEu1DnUkixv4pxU7Dknp_GDVHby5XcMA4lMwazAE0AFG9Cndu0An_gAGACGlYXsGUG9W6okCQsH0&amp;t=633177911400000000" type="text/javascript"></script>
<script src="../WebServices/Materials.asmx/js" type="text/javascript"></script>
<script src="../WebServices/MatGroups.asmx/js" type="text/javascript"></script>
<script src="../WebServices/Folders.asmx/js" type="text/javascript"></script>
<script type="text/javascript">
//<![CDATA[
function WebForm_OnSubmit() {
null;
return true;
}
//]]>
</script>

<div>

	<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="640EFF8C" />
</div>

<!--
The ScriptManager control is required for all ASP AJAX functionality - especially used by search pages.
It generates the javascript required.
Only one script manager is allowed on a page, and since this is the master page, this is the main script manager
However, a ScriptManagerProxy control can be used to set properties and settings on indivual pages.
For an example, see /WebControls/ucSearchResults.ascx
-->
<script type="text/javascript">
//<![CDATA[
Sys.WebForms.PageRequestManager._initialize('ctl00$ajxScriptManager', document.getElementById('aspnetForm'));
Sys.WebForms.PageRequestManager.getInstance()._updateControls([], [], [], 90);
//]]>
</script>


<table class="tabletight" style="width:100%; text-align:center;background-image: url(/images/gray.gif);">
  <tr >
    <td rowspan="2"><a href="/index.aspx"><img src="/images/logo1.gif" width="270" height="42" alt="MatWeb - Material Property Data"  /></a></td>

                
                <td style="text-align:right;vertical-align:middle; width:90%" ><span class='greeting'><span class='hiText'><a href ='/services/advertising.aspx'>Advertise with MatWeb!</a></span>&nbsp;&nbsp;&nbsp;&nbsp;</span></td>
                <td style="text-align:right;vertical-align:middle; padding-right:5px;" ><a href='/membership/regstart.aspx'><img src='/images/buttons/btnRegisterBSF.gif' width="62" height="20" alt="Register Now" /></a></td>

                
  </tr>
  <tr >
    <td  align="left" valign="bottom" colspan="2"><span class="tagline">
    
    Data sheets for over <span class="greeting">180,000</span>  metals, plastics, ceramics, and composites.</span></td>
  </tr>
  <tr style="background-image:url(/images/blue.gif); background-color:#000099; " >
    <td bgcolor="#000099"><a href="/index.aspx"><img src="/images/logo2.gif" width="270" height="23px" alt="MatWeb - Material Property Data" border="0" /></a></td>
    <td class="topnavig8" style=" background-color:#000099;vertical-align:middle; text-align:right; white-space:nowrap;" colspan="2" align="right" valign="middle">
        <a href="/index.aspx" class="topnavlink">HOME</a>&nbsp;&nbsp;&#8226;&nbsp;&nbsp;
                        <a href="/search/search.aspx" class="topnavlink">SEARCH</a>&nbsp;&nbsp;&#8226;&nbsp;&nbsp;
                        <a href="/tools/tools.aspx" class="topnavlink">TOOLS</a>&nbsp;&nbsp;&#8226;&nbsp;&nbsp;
                        <a href="/reference/suppliers.aspx" class="topnavlink">SUPPLIERS</a>&nbsp;&nbsp;&#8226;&nbsp;&nbsp;
                        <a href="/folders/ListFolders.aspx" class="topnavlink">FOLDERS</a>&nbsp;&nbsp;&#8226;&nbsp;&nbsp;
                        <a href="/services/services.aspx" class="topnavlink">ABOUT US</a>&nbsp;&nbsp;&#8226;&nbsp;&nbsp;
                        <a href="/reference/faq.aspx" class="topnavlink">FAQ</a>&nbsp;&nbsp;&#8226;&nbsp;&nbsp;
                        
                        <a href="/membership/login.aspx" class="topnavlink">LOG IN</a>
                        
                        &nbsp;
                        
                        &nbsp;
    </td>
  </tr>
</table>

<div id="divRecentMatls" class="tableborder tight" style=" display:none; position:absolute; top:95px; left:150px; width:400px; height:400px;" onmouseover="clearTimeout(tmrtmp);" onmouseout="tmrtmp=setTimeout('RecentMatls.Hide()', 800)">
        <table class="headerblock tabletight" style="vertical-align:top; font-size:13px; width:100%;"><tr>
                <td style="padding:3px;"> Recently Viewed Materials (most recent at top)</td>
                <td style="padding:3px;text-align:right;"><input type="image" src="/images/buttons/btnCloseWhite.gif" onclick="RecentMatls.Hide();return false;" style="cursor:pointer;" />&nbsp;</td>
        </tr></table>
        <div id="divRecentMatlsBody" style="height:375px; overflow:auto; background-color:White;">

                <table id="tblRecentMatls" class="tabledataformat tableloose" style="background-color:White;">
    
                <tr><td>
                <p />
                <a href="/membership/login.aspx">Login</a> to see your most recently viewed materials here.<p />
                Or if you don't have an account with us yet, then <a href="/membership/regstart.aspx">click here to register.</a>
                </td></tr>
                
                </table>

        </div>
</div>

<script type="text/javascript">

        var RecentMatls = new _RecentMatls();
        var tmrtmp;

        function _RecentMatls(){

                var divRecentMatls;
                var tblRecentMatls;
                var trRecentMatlsRowTemplate;
                var trRecentMatlsNoData;
                var DataIsLoaded=false;

                //Using prototype.js methods...
                divRecentMatls = $("divRecentMatls");
                tblRecentMatls = $("tblRecentMatls");
    

                divRecentMatls.style.display = "none";

                this.Toggle = function(){
                        //alert("Toggle()");
                        if(divRecentMatls.style.display == "block"){this.Hide();}
                        else{this.Show();}
                }

                this.Show = function(){
                        //alert("Show()");
                        divRecentMatls.style.display = "block";
                        matweb.toggleSelect(divRecentMatls,0);
                        if(!DataIsLoaded){
            
                        }
                }

                this.Hide = function(){
                        //alert("Hide()");
                        divRecentMatls.style.display = "none";
                        matweb.toggleSelect(divRecentMatls,1);
                }

    

        }//end class

</script>

<table class="tabletight t_ableborder t_ablegrid" style="width:100%;" >
  <tr>
   <td style="width:100%; height:27px; white-space:nowrap; text-align:left; vertical-align:middle;" class="tagline" >
      <span class="navlink"><b>&nbsp;&nbsp;Searches:</b></span>
      &nbsp;&nbsp;<a href="/search/AdvancedSearch.aspx" class="navlink"><span class="nav"><font color="#CC0000">Advanced</font></span></a>
      &nbsp;|&nbsp;<a href="/search/MaterialGroupSearch.aspx" class="navlink"><span class="nav">Category</span></a>
      &nbsp;|&nbsp;<a href="/search/PropertySearch.aspx" class="navlink"><span class="nav">Property</span></a>
      &nbsp;|&nbsp;<a href="/search/CompositionSearch.aspx" class="navlink"><span class="nav">Metals</span></a>
      &nbsp;|&nbsp;<a href="/search/SearchTradeName.aspx" class="navlink"><span class="nav">Trade Name</span></a>
      &nbsp;|&nbsp;<a href="/search/SearchManufacturerName.aspx" class="navlink"><span class="nav">Manufacturer</span></a>
                &nbsp;|&nbsp;<a href="javascript:RecentMatls.Toggle();" id="ctl00_lnkRecentMatls" class="navlink"><span class="nav"><font color="#CC0000">Recently Viewed Materials</font></span></a>
    </td>

    <td style="text-align:right; vertical-align:bottom; padding-right:5px;">

      <table class="tabletight t_ableborder" ><tr>
        <td style=" white-space:nowrap; vertical-align:bottom; " >
                                        &nbsp;&nbsp;
                                        <input name="ctl00$txtQuickText" type="text" id="ctl00_txtQuickText" style="width:100px;" maxlength="40" class="quicksearchinput" onkeydown="txtQuickText_OnKeyDown(event);" />
                                        <input type="image" id="btnQuickTextSearch" style="vertical-align:top; width:60px; height:25px;" src="/images/buttons/btnSearch.gif" alt="Search" onclick="return btnQuickTextSearch_Click()"  />
                                        <script type="text/javascript">

                                                //SETUP THE AUTOPOST ON ENTER KEY EVENT FOR txtQuickText FIELD
                                                //document.getElementById("ctl00_txtQuickText").onkeydown=txtQuickText_OnKeyDown;

                                                function txtQuickText_OnKeyDown(evt){
                                                        //SUBMIT THE FORM AS IF THE BUTTON WAS PUSHED
                                                        //var UniqueID = "";
                                                        var txtQuickText = document.getElementById('ctl00_txtQuickText');
                                                        if(matweb.IsEnterKey(evt))
                                                                if(checkSearchText(txtQuickText))
                                                                        //matweb.DoPostBack(UniqueID, "");
                                                                        window.location.href="/search/QuickText.aspx?SearchText=" + escape(txtQuickText.value);
                                                        return false;
                                                }

                                                function btnQuickTextSearch_Click(){
                                                        var txtQuickText = document.getElementById('ctl00_txtQuickText');
                                                        if(checkSearchText(txtQuickText))
                                                                window.location.href="/search/QuickText.aspx?SearchText=" + escape(txtQuickText.value);
                                                        return false;
                                                }

                                                function checkSearchText(searchbox){
                                                        searchbox.value = trim(searchbox.value);
                                                        if(searchbox.value == ''){
                                                                //alert('Please enter something to search for');
                                                                matweb.alert('Please enter something to search for', 'Search Error');
                                                                //searchbox.focus();
                                                                return false;
                                                        }
                                                        return true;
                                                }

                                                function trim(s)  { return ltrim(rtrim(s)) }
                                                function ltrim(s) { return s.replace(/^\s+/g, "") }
                                                function rtrim(s) { return s.replace(/\s+$/g, "") }

                                        </script>
                                </td>
                                </tr>
                        </table>

    </td>
  </tr>
  <tr style="background-image:url(/images/shad.gif);height:7px;" ><td colspan="2"></td></tr>
</table>

<!-- ====================================== MAIN CONTENT ============================================= -->
<div style="vertical-align:top; padding-left:10px; padding-right:10px;" >





	<center><a href="/clickthrough.aspx?addataid=1349"><img src="/images/assets/metalmen-Aluminum.png" border=0 alt="Metalmen Sales" width = "728" height = "90"></a>
</center>
	
	<h2><a href="#help"><img src="/images/buttons/iconHelp.gif" alt="" style="vertical-align:bottom;" /></a>Material Category Search</h2>
	
	

<div id="ctl00_ContentMain_ucPopupMessage1_divPopupMessage" class="divPopupAlert" style="display:none;width:250px;">
	<table >
		<tr id="ctl00_ContentMain_ucPopupMessage1_trTitleRow" class="divPopupAlert_TitleRow" style="cursor:move;">
	<th><span id="ctl00_ContentMain_ucPopupMessage1_lblTitle">User Input Problem</span></th>
</tr>

		<tr class="divPopupAlert_ContentRow" ><td style="" ><span id="ctl00_ContentMain_ucPopupMessage1_lblMessage"></span></td></tr>
		<tr class="divPopupAlert_ButtonRow"><td><input name="ctl00$ContentMain$ucPopupMessage1$btnOK" type="button" id="ctl00_ContentMain_ucPopupMessage1_btnOK" value="OK" /></td></tr>
	</table>
</div>

<input type="hidden" name="ctl00$ContentMain$ucPopupMessage1$hndPopupControl" id="ctl00_ContentMain_ucPopupMessage1_hndPopupControl" />


	<table class="" style="width:100%;">
	<tr>
		<td style=" vertical-align:top; width:300px;" >
			<strong>Find a Material Category:</strong>
			

<input name="ctl00$ContentMain$UcMatGroupFinder1$txtSearchText" type="text" value="alum" id="ctl00_ContentMain_UcMatGroupFinder1_txtSearchText" style="Width:304px;" /><br />
<select name="ctl00$ContentMain$UcMatGroupFinder1$selectCategoryList" id="ctl00_ContentMain_UcMatGroupFinder1_selectCategoryList" style="position:absolute;display:none;Width:308px;" size="18">
</select>

<input type="hidden" name="ctl00$ContentMain$UcMatGroupFinder1$TextBoxWatermarkExtender2_ClientState" id="ctl00_ContentMain_UcMatGroupFinder1_TextBoxWatermarkExtender2_ClientState" />

<script type="text/javascript">

var ucMatGroupFinderManager = new _ucMatGroupFinderManager("ctl00_ContentMain_UcMatGroupFinder1_txtSearchText","ctl00_ContentMain_UcMatGroupFinder1_selectCategoryList");

function _ucMatGroupFinderManager(txtSearchTextID,selectCategoryListID){

	var txtSearchText = document.getElementById(txtSearchTextID);
	var selectCategoryList = document.getElementById(selectCategoryListID);
	var showtime;
	var blurtime;
	var MinTextLength = 4;
	var KeyStrokeActionDelay = 650;
	var FadeInOutDelay = 0.2;
	var LastUsedText;
	
	//The init function is currently run at the bottom of this class
	this.init=function(){

		txtSearchText.onfocus = txtSearchText_onfocus;
		txtSearchText.onkeyup = txtSearchText_checktext;
		txtSearchText.onblur = selectCategoryList_startblur;
		
		selectCategoryList.onfocus = selectCategoryList_clearblur;
		selectCategoryList.onchange = selectCategoryList_onchange;
		selectCategoryList.onblur = selectCategoryList_startblur;

	}

	txtSearchText_onfocus = function(){
		if(txtSearchText.value == "Type at least 4 characters here...")
			txtSearchText.value = "";
		selectCategoryList_clearblur();
		txtSearchText_checktext();
	}

	function txtSearchText_checktext(){
		searchText = txtSearchText.value;
		//alert(searchText);
		clearTimeout(showtime);
		if(searchText.length >= MinTextLength){
			showtime = setTimeout(function(){
				selectCategoryList.style.display = "block";
				if(txtSearchText.value!=LastUsedText){
					LastUsedText = searchText;
					aci.matweb.WebServices.MatGroups.FindMatGroups(searchText,DisplayCatData,matweb.WebServiceErrorHandler,"txtSearchText_checktext");
				}
				//AjaxControlToolkit.Animation.FadeInAnimation.play(selectCategoryList, FadeInOutDelay, 1, 25, 0, 1, true);
			},KeyStrokeActionDelay);
		}
		else{
			LastUsedText = "";
			showtime = setTimeout(function(){
				selectCategoryList.options.length = 0;
				selectCategoryList.style.display = "none";
				//AjaxControlToolkit.Animation.FadeOutAnimation.play(selectCategoryList,FadeInOutDelay);
				//AjaxControlToolkit.Animation.FadeOutAnimation.play(selectCategoryList,FadeInOutDelay, 25, 0, 1, true);
			},KeyStrokeActionDelay);
		}
	}

	function selectCategoryList_clearblur(){
		clearTimeout(blurtime);
	}

	function selectCategoryList_onchange(){
		var MatGroupID = selectCategoryList.options[selectCategoryList.selectedIndex].value;
		var MatGroupText = selectCategoryList.options[selectCategoryList.selectedIndex].text;
		//alert("Selected MatGroupID: " + MatGroupID);
		//alert("Selected MatGroupText: " + MatGroupText);
		//When no data is found, we have a "No Categories Contain Text..." message, but the MatGroupID is set to 0, so we can ignore if MatGroupID == 0.
		if(MatGroupID > 0){
			ucMatGroupFinder1_OnSelected(MatGroupID,MatGroupText)
		}
		selectCategoryList_startblur();
		//txtSearchText.value = "";
	}
	
	function selectCategoryList_startblur(){
		//alert("selectCategoryList_onblur()");
		clearTimeout(blurtime);
		blurtime = setTimeout(function(){
			selectCategoryList.style.display = "none";
			//AjaxControlToolkit.Animation.FadeOutAnimation.play(selectCategoryList,FadeInOutDelay, 25, 0, 1, true);
		},KeyStrokeActionDelay);
	}
	
	function DisplayCatData(MatGroupData){
		//alert("GetCatData()");
		//alert("vwMaterialGroupList: " + vwMaterialGroupList);
		var MatGroupID,GroupName, MatCount;
		selectCategoryList.options.length = 0;//clear any existing options....
		if(MatGroupData.length==0){
				//alert(vwMaterialGroupList[i].GroupName);
				MatGroupID = 0;
				GroupName = "No categories contain the text: " + txtSearchText.value
				selectCategoryList.options[0] = new Option(GroupName,MatGroupID);					
		}
		else{
			for(i=0;i<MatGroupData.length;i++){
				//alert(vwMaterialGroupList[i].GroupName);
				MatCount = MatGroupData[i].MatCount;
				MatGroupID = MatGroupData[i].MatGroupID;
				GroupName = MatGroupData[i].GroupName + " (" + MatCount + " matls)";
				selectCategoryList.options[i] = new Option(GroupName,MatGroupID);					
			}
		}
	}
	
	this.init();

}//end class
	
</script>



			<strong>Select a Material Category:</strong>
			
			<div class="tableborder" style=" height:225px; width:300px; overflow:scroll; ">
			<a href="#ctl00_ContentMain_ucMatGroupTree_msTreeView_SkipLink"><img alt="Skip Navigation Links." src="/WebResource.axd?d=vAkieGj3ugBpu6CKp7dqvCP5iHLzPbUd2XME-h1vQcC6d5hLg7Vc1kNVzB8A45Ui3K9XZ9m3-E2pxsaJQYh2uMkAul41&amp;t=638313619312541215" width="0" height="0" style="border-width:0px;" /></a><div id="ctl00_ContentMain_ucMatGroupTree_msTreeView">
	<table cellpadding="0" cellspacing="0" style="border-width:0;">
		<tr>
			<td><a id="ctl00_ContentMain_ucMatGroupTree_msTreeViewn0" href="javascript:TreeView_PopulateNode(ctl00_ContentMain_ucMatGroupTree_msTreeView_Data,0,document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewn0'),document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewt0'),null,'r','Carbon (866 matls)','283','f','','f')"><img src="/WebResource.axd?d=6v5qM6VKQYD3XNwwXEy7Tix7dzVdqDJ8QTRIlUPrzXQqNsXQgUwIfmn34ZzRORd_IZj0bF4vkZzID1HuSFE5uKyLgaPYeAYE8VT8HYfb3Rk6zsDw0&amp;t=638313619312541215" alt="Expand Carbon (866 matls)" style="border-width:0;" /></a></td><td class="ctl00_ContentMain_ucMatGroupTree_msTreeView_2"><a class="ctl00_ContentMain_ucMatGroupTree_msTreeView_0 ctl00_ContentMain_ucMatGroupTree_msTreeView_1" href="javascript:ucMatGroupTree_OnNodeClick('Carbon (866 matls)','283')" id="ctl00_ContentMain_ucMatGroupTree_msTreeViewt0">Carbon (866 matls)</a></td>
		</tr><tr style="height:0px;">
			<td></td>
		</tr>
	</table><table cellpadding="0" cellspacing="0" style="border-width:0;">
		<tr style="height:0px;">
			<td></td>
		</tr><tr>
			<td><a id="ctl00_ContentMain_ucMatGroupTree_msTreeViewn1" href="javascript:TreeView_PopulateNode(ctl00_ContentMain_ucMatGroupTree_msTreeView_Data,1,document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewn1'),document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewt1'),null,'t','Ceramic (10004 matls)','11','f','','f')"><img src="/WebResource.axd?d=TinC6igX1vPDgWh7DstWwBTHTyH4uuybDU4NW47jJD06wHdAO3YyJC_eKK1Zn5oIbwqRo4Cri8YwhA82G-LD0p14yEF1EfoIZvKleYMIuQ2K1hkE0&amp;t=638313619312541215" alt="Expand Ceramic (10004 matls)" style="border-width:0;" /></a></td><td class="ctl00_ContentMain_ucMatGroupTree_msTreeView_2"><a class="ctl00_ContentMain_ucMatGroupTree_msTreeView_0 ctl00_ContentMain_ucMatGroupTree_msTreeView_1" href="javascript:ucMatGroupTree_OnNodeClick('Ceramic (10004 matls)','11')" id="ctl00_ContentMain_ucMatGroupTree_msTreeViewt1">Ceramic (10004 matls)</a></td>
		</tr><tr style="height:0px;">
			<td></td>
		</tr>
	</table><table cellpadding="0" cellspacing="0" style="border-width:0;">
		<tr style="height:0px;">
			<td></td>
		</tr><tr>
			<td><a id="ctl00_ContentMain_ucMatGroupTree_msTreeViewn2" href="javascript:TreeView_PopulateNode(ctl00_ContentMain_ucMatGroupTree_msTreeView_Data,2,document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewn2'),document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewt2'),null,'t','Fluid (7562 matls)','5','f','','f')"><img src="/WebResource.axd?d=TinC6igX1vPDgWh7DstWwBTHTyH4uuybDU4NW47jJD06wHdAO3YyJC_eKK1Zn5oIbwqRo4Cri8YwhA82G-LD0p14yEF1EfoIZvKleYMIuQ2K1hkE0&amp;t=638313619312541215" alt="Expand Fluid (7562 matls)" style="border-width:0;" /></a></td><td class="ctl00_ContentMain_ucMatGroupTree_msTreeView_2"><a class="ctl00_ContentMain_ucMatGroupTree_msTreeView_0 ctl00_ContentMain_ucMatGroupTree_msTreeView_1" href="javascript:ucMatGroupTree_OnNodeClick('Fluid (7562 matls)','5')" id="ctl00_ContentMain_ucMatGroupTree_msTreeViewt2">Fluid (7562 matls)</a></td>
		</tr><tr style="height:0px;">
			<td></td>
		</tr>
	</table><table cellpadding="0" cellspacing="0" style="border-width:0;">
		<tr style="height:0px;">
			<td></td>
		</tr><tr>
			<td><a id="ctl00_ContentMain_ucMatGroupTree_msTreeViewn3" href="javascript:TreeView_PopulateNode(ctl00_ContentMain_ucMatGroupTree_msTreeView_Data,3,document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewn3'),document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewt3'),null,'t','Metal (17052 matls)','9','f','','f')"><img src="/WebResource.axd?d=TinC6igX1vPDgWh7DstWwBTHTyH4uuybDU4NW47jJD06wHdAO3YyJC_eKK1Zn5oIbwqRo4Cri8YwhA82G-LD0p14yEF1EfoIZvKleYMIuQ2K1hkE0&amp;t=638313619312541215" alt="Expand Metal (17052 matls)" style="border-width:0;" /></a></td><td class="ctl00_ContentMain_ucMatGroupTree_msTreeView_2"><a class="ctl00_ContentMain_ucMatGroupTree_msTreeView_0 ctl00_ContentMain_ucMatGroupTree_msTreeView_1" href="javascript:ucMatGroupTree_OnNodeClick('Metal (17052 matls)','9')" id="ctl00_ContentMain_ucMatGroupTree_msTreeViewt3">Metal (17052 matls)</a></td>
		</tr><tr style="height:0px;">
			<td></td>
		</tr>
	</table><table cellpadding="0" cellspacing="0" style="border-width:0;">
		<tr style="height:0px;">
			<td></td>
		</tr><tr>
			<td><a id="ctl00_ContentMain_ucMatGroupTree_msTreeViewn4" href="javascript:TreeView_PopulateNode(ctl00_ContentMain_ucMatGroupTree_msTreeView_Data,4,document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewn4'),document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewt4'),null,'t','Other Engineering Material (8063 matls)','285','f','','f')"><img src="/WebResource.axd?d=TinC6igX1vPDgWh7DstWwBTHTyH4uuybDU4NW47jJD06wHdAO3YyJC_eKK1Zn5oIbwqRo4Cri8YwhA82G-LD0p14yEF1EfoIZvKleYMIuQ2K1hkE0&amp;t=638313619312541215" alt="Expand Other Engineering Material (8063 matls)" style="border-width:0;" /></a></td><td class="ctl00_ContentMain_ucMatGroupTree_msTreeView_2"><a class="ctl00_ContentMain_ucMatGroupTree_msTreeView_0 ctl00_ContentMain_ucMatGroupTree_msTreeView_1" href="javascript:ucMatGroupTree_OnNodeClick('Other Engineering Material (8063 matls)','285')" id="ctl00_ContentMain_ucMatGroupTree_msTreeViewt4">Other Engineering Material (8063 matls)</a></td>
		</tr><tr style="height:0px;">
			<td></td>
		</tr>
	</table><table cellpadding="0" cellspacing="0" style="border-width:0;">
		<tr style="height:0px;">
			<td></td>
		</tr><tr>
			<td><a id="ctl00_ContentMain_ucMatGroupTree_msTreeViewn5" href="javascript:TreeView_PopulateNode(ctl00_ContentMain_ucMatGroupTree_msTreeView_Data,5,document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewn5'),document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewt5'),null,'t','Polymer (97635 matls)','10','f','','f')"><img src="/WebResource.axd?d=TinC6igX1vPDgWh7DstWwBTHTyH4uuybDU4NW47jJD06wHdAO3YyJC_eKK1Zn5oIbwqRo4Cri8YwhA82G-LD0p14yEF1EfoIZvKleYMIuQ2K1hkE0&amp;t=638313619312541215" alt="Expand Polymer (97635 matls)" style="border-width:0;" /></a></td><td class="ctl00_ContentMain_ucMatGroupTree_msTreeView_2"><a class="ctl00_ContentMain_ucMatGroupTree_msTreeView_0 ctl00_ContentMain_ucMatGroupTree_msTreeView_1" href="javascript:ucMatGroupTree_OnNodeClick('Polymer (97635 matls)','10')" id="ctl00_ContentMain_ucMatGroupTree_msTreeViewt5">Polymer (97635 matls)</a></td>
		</tr><tr style="height:0px;">
			<td></td>
		</tr>
	</table><table cellpadding="0" cellspacing="0" style="border-width:0;">
		<tr style="height:0px;">
			<td></td>
		</tr><tr>
			<td><img src="/WebResource.axd?d=7_V5n1KYSQsj6PoSqgIxb0egBsAa91jSXPkbmM6FaiF14-JsNnpUDnfSdxZcvfd0OQCLDtC24pMIXigO-41cL2rsq-0xea-SSG7yZUyRcliFFqe70&amp;t=638313619312541215" alt="" /></td><td class="ctl00_ContentMain_ucMatGroupTree_msTreeView_2"><a class="ctl00_ContentMain_ucMatGroupTree_msTreeView_0 ctl00_ContentMain_ucMatGroupTree_msTreeView_1" href="javascript:ucMatGroupTree_OnNodeClick('Pure Element (507 matls)','184')" id="ctl00_ContentMain_ucMatGroupTree_msTreeViewt6">Pure Element (507 matls)</a></td>
		</tr><tr style="height:0px;">
			<td></td>
		</tr>
	</table><table cellpadding="0" cellspacing="0" style="border-width:0;">
		<tr style="height:0px;">
			<td></td>
		</tr><tr>
			<td><a id="ctl00_ContentMain_ucMatGroupTree_msTreeViewn7" href="javascript:TreeView_PopulateNode(ctl00_ContentMain_ucMatGroupTree_msTreeView_Data,7,document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewn7'),document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewt7'),null,'l','Wood and Natural Products (398 matls)','277','f','','t')"><img src="/WebResource.axd?d=vUBrM2nNhECNbFO0AJKq52p2zWrY5XG8nWgCDL4AfsrexStb0HREDV6GHMvdkAGDInxYtTyWXuxFUIiQv3JX2fScVRwQsrVv5srHrjOIeC3ayAG20&amp;t=638313619312541215" alt="Expand Wood and Natural Products (398 matls)" style="border-width:0;" /></a></td><td class="ctl00_ContentMain_ucMatGroupTree_msTreeView_2"><a class="ctl00_ContentMain_ucMatGroupTree_msTreeView_0 ctl00_ContentMain_ucMatGroupTree_msTreeView_1" href="javascript:ucMatGroupTree_OnNodeClick('Wood and Natural Products (398 matls)','277')" id="ctl00_ContentMain_ucMatGroupTree_msTreeViewt7">Wood and Natural Products (398 matls)</a></td>
		</tr><tr style="height:0px;">
			<td></td>
		</tr>
	</table>
</div><a id="ctl00_ContentMain_ucMatGroupTree_msTreeView_SkipLink"></a>
<span id="ctl00_ContentMain_ucMatGroupTree_lblDebug"></span>

			</div>
			
			<strong>Selected Material Category: </strong>
			<div id="ctl00_ContentMain_divMatGroupName" class="selectedItem" style="width:306px;">3000 Series Aluminum Alloy (92 matls)</div>
			<input name="ctl00$ContentMain$txtMatGroupID" type="hidden" id="ctl00_ContentMain_txtMatGroupID" value="203" />
			<input name="ctl00$ContentMain$txtMatGroupText" type="hidden" id="ctl00_ContentMain_txtMatGroupText" value="3000 Series Aluminum Alloy (92 matls)" />
			
			

			<input type="image" name="ctl00$ContentMain$btnSubmit" id="ctl00_ContentMain_btnSubmit" src="../images/buttons/btnFind.gif" alt="Find" style="border-width:0px;" /> <!-- OnClientClick="return validate()" -->
			<input type="image" name="ctl00$ContentMain$btnReset" id="ctl00_ContentMain_btnReset" title="Clear Search" src="/images/buttons/btnReset.gif" alt="Clear Search" style="border-width:0px;" />

			<span id="ctl00_ContentMain_lblDebug"></span>

		</td>
		<td>&nbsp;</td>
		<td style=" vertical-align:top; width:80%;" >
			<strong>Search Results</strong>
			<div class="tableborder" id="divSearchResults" style="padding:5px;" >
				
				<div id="ctl00_ContentMain_pnlSearchResults">
	
					There are <span id="ctl00_ContentMain_lblMatlCount">92</span> materials in the category 
					<span id="ctl00_ContentMain_lblMaterialGroupName" class="selectedItem">3000 Series Aluminum Alloy</span>.
					If your material is not listed, please refer to our <a href="/help/strategy.aspx">search strategy</a> page for assistance in limiting your search.
				
</div>
			</div>
			<p />




<div id="ctl00_ContentMain_UcSearchResults1_pnlResultsFound" class="tableborder">
	

	<table style="background-color:Silver; width:100%;" class="" >
		<tr>
			<td><strong>Found <span id="ctl00_ContentMain_UcSearchResults1_lblResultCount">92</span> Results</strong> -- 
				Page 
				<select name="ctl00$ContentMain$UcSearchResults1$drpPageSelect1" onchange="javascript:setTimeout('__doPostBack(\'ctl00$ContentMain$UcSearchResults1$drpPageSelect1\',\'\')', 0)" id="ctl00_ContentMain_UcSearchResults1_drpPageSelect1">
		<option selected="selected" value="1">1</option>

	</select>
				of <span id="ctl00_ContentMain_UcSearchResults1_lblPageTotal">1</span> 
				-- 
				<a id="ctl00_ContentMain_UcSearchResults1_lnkPrevPage" disabled="disabled" style="color:Gray;">[Prev Page]</a>
				<a id="ctl00_ContentMain_UcSearchResults1_lnkNextPage" disabled="disabled" style="color:Gray;">[Next Page]</a>
				--&nbsp;			
				view
				<select name="ctl00$ContentMain$UcSearchResults1$drpPageSize1" onchange="javascript:setTimeout('__doPostBack(\'ctl00$ContentMain$UcSearchResults1$drpPageSize1\',\'\')', 0)" id="ctl00_ContentMain_UcSearchResults1_drpPageSize1">
		<option value="25">25</option>
		<option value="50">50</option>
		<option value="75">75</option>
		<option value="100">100</option>
		<option value="150">150</option>
		<option selected="selected" value="200">200</option>

	</select> per page
			</td>
		</tr>
		
	</table>
	
	
	
	
	<table class="t_ablegrid" style="width:100%;">
		<tr style="font-size:9px;">
			<td style="font-size:9px;">Use Folder</td>
			<td style="font-size:9px;">Contains</td>
			<td style="font-size:9px;">&nbsp;</td>
			
			<td style="font-size:9px;"><div id="divFolderName" style="display:none;">New Folder Name</div></td>
			<td style="font-size:9px;"></td>
			<td style="width:90%"></td>
		</tr>
		<tr style="vertical-align:top;">
			<td style="vertical-align:top;">
				<select name="ctl00$ContentMain$UcSearchResults1$drpFolderList" id="ctl00_ContentMain_UcSearchResults1_drpFolderList" onchange="SetSelectedFolder()" style="width:125px;">
		<option selected="selected" value="0">My Folder</option>

	</select>
			</td>
			<td style="vertical-align:top;">
				<strong><input name="ctl00$ContentMain$UcSearchResults1$txtFolderMatCount" type="text" id="ctl00_ContentMain_UcSearchResults1_txtFolderMatCount" readonly="readonly" style="border:none 0 white; background-color:White; color:Black; width: 35px; font-weight:bold;" value="0/0" /></strong>
			</td>
			<td>
				
				<input type="image" id="btnCompareFolders" src="/images/buttons/btnCompareMatls.gif" alt="Compare Checked Materials" onclick="CompareMaterials();return false;" />
			</td>
			
			<td style="white-space:nowrap;vertical-align:top;">
			
				<table id="tblFolderName" class="tabletight" style="display:none;"><tr><td><input type="text" id="txtFolderName2" style="display:inline; width:100px;" /><br /></td>
				<td><input type="image" id="btnApplyFolderName2" src="/images/buttons/btnSaveChanges.gif" style="display:inline;" onclick="ApplyFolderName();return false;" />
				<input type="image" id="btnHideFolderNameControls2" src="/images/buttons/btnCancel.gif" style="display:inline;" onclick="HideFolderNameControls();return false;" /></td>
				</tr></table>
				
			</td>
			<td style="white-space:nowrap;">
				<div class="usermessage" id="divUserMessage"></div>
			</td>
		</tr>
	</table>

	<table class="tabledataformat t_ablegrid" id="tblResults" style="table-layout:auto;"  >

		<tr class="">

			
		
			<th style="width:65px; white-space:nowrap;">Select</th>

			<th style="width:10px;">
				<!--Discontinued and Found Via Interpolated Indicators -->
			</th>
			
			<th style="width:auto;">
				<a id="ctl00_ContentMain_UcSearchResults1_lnkSortMatName" disabled="disabled" title="Click to Sort By Material Name">Material Name</a>
				<br />
				
				
			</th>
		
			
			
			

			

			

			

			

			

			

			

			
		</tr>

		<!-- this is where the search results are actually loaded.  See code behind -->		
		<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_81684"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;1</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_81684' href="/search/DataSheet.aspx?MatGUID=82c8a6ba80e641d9b872e7a62af33093" >Overview of materials for 3000 Series Aluminum Alloy</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_8974"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;2</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_8974' href="/search/DataSheet.aspx?MatGUID=5ad9fb4aa4d34f3e8097875d47decf1f" >3002 Aluminum Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_109178"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;3</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_109178' href="/search/DataSheet.aspx?MatGUID=a594182a80f24ed6bd45c93847eabbc4" >Alclad Aluminum 3003-H112</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_8975"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;4</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_8975' href="/search/DataSheet.aspx?MatGUID=533e94318f2649a594368b3dfd046ef2" >Alclad Aluminum 3003-H12</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_8976"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;5</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_8976' href="/search/DataSheet.aspx?MatGUID=a542868dbeda4214b23b26a4dfbe49a0" >Alclad Aluminum 3003-H14</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_8977"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;6</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_8977' href="/search/DataSheet.aspx?MatGUID=6746db0c00e94a6b823da09a35f0bd21" >Alclad Aluminum 3003-H16</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_8978"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;7</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_8978' href="/search/DataSheet.aspx?MatGUID=c8a589a030f14784a3bbf991b770bcd8" >Alclad Aluminum 3003-H18</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_8979"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;8</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_8979' href="/search/DataSheet.aspx?MatGUID=dbe8df6a132547b4982311b9698e63be" >Alclad Aluminum 3003-O</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_109179"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;9</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_109179' href="/search/DataSheet.aspx?MatGUID=a1534c6c96a94533b67f29eec924282e" >Aluminum 3003-H112</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_8980"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;10</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_8980' href="/search/DataSheet.aspx?MatGUID=5b30b87291e84c5e843a9b0025b7dfc6" >Aluminum 3003-H12</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_8981"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;11</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_8981' href="/search/DataSheet.aspx?MatGUID=09c63ea8e10e4eea8398256801bb8514" >Aluminum 3003-H14</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_8982"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;12</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_8982' href="/search/DataSheet.aspx?MatGUID=88e1df51ed674c6b9fac45ed79108f2d" >Aluminum 3003-H16</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_8983"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;13</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_8983' href="/search/DataSheet.aspx?MatGUID=91c99440833341fc8fbe1d26149bc35e" >Aluminum 3003-H18</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_8984"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;14</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_8984' href="/search/DataSheet.aspx?MatGUID=79e0d265f7534b62a1c88b5b540a1fb2" >Aluminum 3003-H19 Foil</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_8985"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;15</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_8985' href="/search/DataSheet.aspx?MatGUID=fd4a40f87d3f4912925e5e6eab1fbc40" >Aluminum 3003-O</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_8986"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;16</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_8986' href="/search/DataSheet.aspx?MatGUID=7c18acfb0e80482bbfa80bc6480da036" >Aluminum 3004A Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_109180"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;17</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_109180' href="/search/DataSheet.aspx?MatGUID=068d94cb2914442cae3e62d68e3a1737" >Alclad Aluminum 3004-H112</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_8987"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;18</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_8987' href="/search/DataSheet.aspx?MatGUID=9dd9826566544e30970baf005f930e87" >Alclad Aluminum 3004-H32</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_8988"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;19</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_8988' href="/search/DataSheet.aspx?MatGUID=3c657e1a8905453d97814faf6bb938c7" >Alclad Aluminum 3004-H34</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_8989"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;20</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_8989' href="/search/DataSheet.aspx?MatGUID=fe91e252f4dc4c4baf855b125fde8e5d" >Alclad Aluminum 3004-H36</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_8990"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;21</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_8990' href="/search/DataSheet.aspx?MatGUID=6f63ebbf0a3e48ed917544c9a7682b25" >Alclad Aluminum 3004-H38</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_8991"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;22</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_8991' href="/search/DataSheet.aspx?MatGUID=563e56adfab14712badfe876ae8062e2" >Alclad Aluminum 3004-O</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_109181"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;23</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_109181' href="/search/DataSheet.aspx?MatGUID=a8431a52f17b4fefb25e6ea88d3f813a" >Aluminum 3004-H112</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_8992"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;24</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_8992' href="/search/DataSheet.aspx?MatGUID=ec6a8753c110472ebcead3a2f95457ba" >Aluminum 3004-H19</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_8993"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;25</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_8993' href="/search/DataSheet.aspx?MatGUID=19c081b654764d7ea39d24bc38a72b2e" >Aluminum 3004-H32</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_8994"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;26</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_8994' href="/search/DataSheet.aspx?MatGUID=5912b02863574b8c9bf1eb2225926e2c" >Aluminum 3004-H34</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_8995"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;27</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_8995' href="/search/DataSheet.aspx?MatGUID=08e26bd47dc448cf9260834221adaa9a" >Aluminum 3004-H36</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_8996"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;28</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_8996' href="/search/DataSheet.aspx?MatGUID=e836f33f9ee74daeb5735745c88255e6" >Aluminum 3004-H38</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_8997"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;29</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_8997' href="/search/DataSheet.aspx?MatGUID=b40f2ca72f504f298d6f5399ce00440f" >Aluminum 3004-O</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_8998"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;30</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_8998' href="/search/DataSheet.aspx?MatGUID=9eedcc05f42f462dbe551a13b935c541" >Aluminum 3005A Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_8999"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;31</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_8999' href="/search/DataSheet.aspx?MatGUID=d40e2373e8494ad98aac1c83430a3fc2" >Aluminum 3005-H14</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9000"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;32</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9000' href="/search/DataSheet.aspx?MatGUID=a9a7930bdb2e41c98052184b60c8b34f" >Aluminum 3005-H18</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9001"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;33</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9001' href="/search/DataSheet.aspx?MatGUID=9f725b7b3bb74a9da484c5e622a8f185" >Aluminum 3005-O</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9002"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;34</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9002' href="/search/DataSheet.aspx?MatGUID=ad962dc521f44d69a129724ab43f8a53" >3006 Aluminum Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9003"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;35</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9003' href="/search/DataSheet.aspx?MatGUID=899b11430f314361983b8643ea338c00" >3007 Aluminum Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9004"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;36</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9004' href="/search/DataSheet.aspx?MatGUID=b872885a143f419e9ff7451a226fe43a" >3009 Aluminum Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9005"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;37</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9005' href="/search/DataSheet.aspx?MatGUID=d30763923f3f459c9b871da7617c1df3" >3010 Aluminum Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9006"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;38</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9006' href="/search/DataSheet.aspx?MatGUID=be8be559d78b466ab7fe7ce46e34cbd4" >3011 Aluminum Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9007"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;39</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9007' href="/search/DataSheet.aspx?MatGUID=f8e26f3279084b53b9059a2d4a6bb745" >3012 Aluminum Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_160165"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;40</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_160165' href="/search/DataSheet.aspx?MatGUID=f0240d95f38f466f9f6e8ee66b147580" >Aluminum 3012A Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9008"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;41</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9008' href="/search/DataSheet.aspx?MatGUID=7d9d0b458a7c4d64bb451ed5a832eb76" >3013-O Aluminum</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_130352"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;42</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_130352' href="/search/DataSheet.aspx?MatGUID=f3bb02fc6e0d46cea82999208c78fa9e" >3013-H14 Aluminum</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9009"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;43</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9009' href="/search/DataSheet.aspx?MatGUID=e6db8f4f59ae4578b85c8f47d0c06dbe" >3014 Aluminum Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9010"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;44</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9010' href="/search/DataSheet.aspx?MatGUID=8246a58d348c4f0cbf518c2350771441" >3015 Aluminum Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9011"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;45</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9011' href="/search/DataSheet.aspx?MatGUID=905f27ccc00c4742a2ef4856204e5e52" >3016 Aluminum Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9012"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;46</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9012' href="/search/DataSheet.aspx?MatGUID=094ffa38ca7f45e7bb831391f9711409" >3017 Aluminum Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9013"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;47</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9013' href="/search/DataSheet.aspx?MatGUID=5965eb48c4ab45bcaf20f86994f5fea2" >3019 Aluminum Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9014"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;48</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9014' href="/search/DataSheet.aspx?MatGUID=417bb4d7ebfb40d890ba1b9612527b17" >3020 Aluminum Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_160237"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;49</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_160237' href="/search/DataSheet.aspx?MatGUID=72b5fbd56c7644cf910cd50b35adef75" >Aluminum 3021 Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9015"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;50</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9015' href="/search/DataSheet.aspx?MatGUID=91d2adea156f4019ace67fd83bc0bf96" >3025 Aluminum Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_160166"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;51</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_160166' href="/search/DataSheet.aspx?MatGUID=461d4211b84f43cf8691c8ff6c2e26c7" >Aluminum 3026 Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9017"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;52</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9017' href="/search/DataSheet.aspx?MatGUID=f8adb58b21e948fe9bab45c16426962a" >3030 Aluminum Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_160261"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;53</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_160261' href="/search/DataSheet.aspx?MatGUID=70919fd3fcbb4de3a10b880297ccca5f" >Aluminum 3065 Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_192339"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;54</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_192339' href="/search/DataSheet.aspx?MatGUID=8dd735905fb84f4eb8d66a603bb5d5c3" >Aluminum 3102-H112</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9032"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;55</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9032' href="/search/DataSheet.aspx?MatGUID=c1f1c58973174bd5a4855d7773c4b1cf" >Aluminum 3102-O</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9033"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;56</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9033' href="/search/DataSheet.aspx?MatGUID=3d0f2fd9bba9458abc28e2c686cb7897" >3103 Aluminum Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9034"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;57</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9034' href="/search/DataSheet.aspx?MatGUID=ed15b281c5a1415a8dd6c63984e9b71a" >Aluminum 3103A Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_160161"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;58</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_160161' href="/search/DataSheet.aspx?MatGUID=fc76e97cc2ad484397e7a9284436c47e" >Aluminum 3103B Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9035"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;59</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9035' href="/search/DataSheet.aspx?MatGUID=aaaabe41a20a4ed2b48270f7f2ef1b2d" >Aluminum 3104-H19</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9036"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;60</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9036' href="/search/DataSheet.aspx?MatGUID=1e58d94dac6040c2b30185e88d20cccb" >Aluminum 3105A Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9037"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;61</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9037' href="/search/DataSheet.aspx?MatGUID=36592db340214b6198c21056b6e8ea0d" >Aluminum 3105B Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9038"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;62</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9038' href="/search/DataSheet.aspx?MatGUID=63d58cd8aa1a4e7a98471231b352442b" >Aluminum 3105-H12</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9039"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;63</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9039' href="/search/DataSheet.aspx?MatGUID=5d6c0ab396604b44a204458d8cd495d9" >Aluminum 3105-H14</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9040"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;64</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9040' href="/search/DataSheet.aspx?MatGUID=0965df1773724e579e9eee4248954ef9" >Aluminum 3105-H16</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9041"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;65</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9041' href="/search/DataSheet.aspx?MatGUID=773814ba2af54b23be7a9eddc1a95c04" >Aluminum 3105-H18</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9042"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;66</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9042' href="/search/DataSheet.aspx?MatGUID=0ef133e3b88d45a8a1399ed729001f15" >Aluminum 3105-H22</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9043"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;67</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9043' href="/search/DataSheet.aspx?MatGUID=bf1b556e57c5486a8418cf38c5ba41a7" >Aluminum 3105-H24</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9044"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;68</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9044' href="/search/DataSheet.aspx?MatGUID=28fa2908ba9d42208546dbaac1e5bc43" >Aluminum 3105-H25</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9045"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;69</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9045' href="/search/DataSheet.aspx?MatGUID=803c7fceac484f528a92e5fb2b460735" >Aluminum 3105-H26</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9046"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;70</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9046' href="/search/DataSheet.aspx?MatGUID=50d1903b4ab54e16a8c088ead6dbd91a" >Aluminum 3105-H28</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9047"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;71</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9047' href="/search/DataSheet.aspx?MatGUID=27fe301ef1a340639654dad0ffa28832" >Aluminum 3105-O</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9048"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;72</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9048' href="/search/DataSheet.aspx?MatGUID=33b5af800d4a4a9dae40cd5edeb04769" >3107 Aluminum Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_160164"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;73</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_160164' href="/search/DataSheet.aspx?MatGUID=d41b5b80dc9442c3b72de306e8a40c05" >Aluminum 3110 Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_160167"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;74</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_160167' href="/search/DataSheet.aspx?MatGUID=aa8f637afb104fd7bda1a2da51ce5a3e" >Aluminum 3130 Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9056"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;75</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9056' href="/search/DataSheet.aspx?MatGUID=ef9d6b0edfa04a409c90f236b4b92c61" >3203 Aluminum Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9057"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;76</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9057' href="/search/DataSheet.aspx?MatGUID=4f084e5f597441a9a30fc1b7369b07f1" >3204 Aluminum Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9058"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;77</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9058' href="/search/DataSheet.aspx?MatGUID=f79d6e47f102462e8d26dab1552108f0" >3207 Aluminum Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9059"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;78</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9059' href="/search/DataSheet.aspx?MatGUID=912afdaf8f4544bba06e46ad4017fcfe" >Aluminum 3207A Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_160163"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;79</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_160163' href="/search/DataSheet.aspx?MatGUID=b42f3a5a33bb419e8abd23874c84a79d" >Aluminum 3304 Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9060"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;80</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9060' href="/search/DataSheet.aspx?MatGUID=6522c45764a445e3b9a5ea05f5cc2adf" >3307 Aluminum Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_160162"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;81</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_160162' href="/search/DataSheet.aspx?MatGUID=7f5b1490ebbc45c2b3b5d9f4e56b0c82" >Aluminum 3403 Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_98678"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;82</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_98678' href="/search/DataSheet.aspx?MatGUID=dec53509d664493f9c891e1df1c0573b" >Hexcel® HexWeb® ACG®-1/4 Honeycomb Aluminum Commercial Grade</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_98679"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;83</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_98679' href="/search/DataSheet.aspx?MatGUID=65a8a84e92df402c8b942863da020fb6" >Hexcel® HexWeb® ACG®-3/8 Honeycomb Aluminum Commercial Grade</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_98680"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;84</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_98680' href="/search/DataSheet.aspx?MatGUID=ac9fcb848700469a838d6b61df51f885" >Hexcel® HexWeb® ACG®-1/2 Honeycomb Aluminum Commercial Grade</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_98681"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;85</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_98681' href="/search/DataSheet.aspx?MatGUID=d07d08c374ce4ae0ba2d185cc2d42e9f" >Hexcel® HexWeb® ACG®-3/4 Honeycomb Aluminum Commercial Grade</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_98682"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;86</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_98682' href="/search/DataSheet.aspx?MatGUID=4985c190b76c4c4abf2564169d57ca81" >Hexcel® HexWeb® ACG®-1 Honeycomb Aluminum Commercial Grade</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_98781"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;87</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_98781' href="/search/DataSheet.aspx?MatGUID=06fa6d64269047119d44eca34507213f" >Hexcel® HexWeb® CRF-3/4-ACG-.003-1.8 Chromate free ACG® Honeycomb</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_98782"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;88</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_98782' href="/search/DataSheet.aspx?MatGUID=82317827b063463b87871077d7b3c6aa" >Hexcel® HexWeb® CRF-3/8-ACG-.002-2.3 Chromate free ACG® Honeycomb</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_98783"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;89</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_98783' href="/search/DataSheet.aspx?MatGUID=c6013ebd3f86475c9ad0a6ed569317a1" >Hexcel® HexWeb® CRF-1/2-ACG-.003-2.6 Chromate free ACG® Honeycomb</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_98784"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;90</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_98784' href="/search/DataSheet.aspx?MatGUID=d8a2f2e946f54224918add698ed98286" >Hexcel® HexWeb® CRF-1/4-ACG-.002-3.4 Chromate free ACG® Honeycomb</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_98785"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;91</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_98785' href="/search/DataSheet.aspx?MatGUID=446c4b8635644f33ad7f3d6ce15b285f" >Hexcel® HexWeb® CRF-3/8-ACG-.003-3.7 Chromate free ACG® Honeycomb</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_98786"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;92</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_98786' href="/search/DataSheet.aspx?MatGUID=bd0d5d39ef494602baa54184413a2535" >Hexcel® HexWeb® CRF-1/4-ACG-.003-15.2 Chromate free ACG® Honeycomb</a></td></tr>

		
	</table>

	<table style="background-color:Silver;" class="tabledataformat" >

		<tr class="">
			<th>&nbsp;</th>
		</tr>

		<tr>
			<td><strong>Found <span id="ctl00_ContentMain_UcSearchResults1_lblResultCount2">92</span> Results</strong> -- 
				
				Page 
				<select name="ctl00$ContentMain$UcSearchResults1$drpPageSelect2" onchange="javascript:setTimeout('__doPostBack(\'ctl00$ContentMain$UcSearchResults1$drpPageSelect2\',\'\')', 0)" id="ctl00_ContentMain_UcSearchResults1_drpPageSelect2">
		<option selected="selected" value="1">1</option>

	</select>
				of <span id="ctl00_ContentMain_UcSearchResults1_lblPageTotal2">1</span> 
				
				-- 
				<a id="ctl00_ContentMain_UcSearchResults1_lnkPrevPage2" disabled="disabled" style="color:Gray;">[Prev Page]</a>
				<a id="ctl00_ContentMain_UcSearchResults1_lnkNextPage2" disabled="disabled" style="color:Gray;">[Next Page]</a>
				--&nbsp;

				view 
				<select name="ctl00$ContentMain$UcSearchResults1$drpPageSize2" onchange="javascript:setTimeout('__doPostBack(\'ctl00$ContentMain$UcSearchResults1$drpPageSize2\',\'\')', 0)" id="ctl00_ContentMain_UcSearchResults1_drpPageSize2">
		<option value="25">25</option>
		<option value="50">50</option>
		<option value="75">75</option>
		<option value="100">100</option>
		<option value="150">150</option>
		<option selected="selected" value="200">200</option>

	</select> per page
			</td>
		</tr>
	</table>
	
	 


</div>





<script type="text/javascript">

	//GLOBAL VARIABLES
	var gCurrentCheckbox;
	var gNewFolderID;
	var gOldFolderName;
	var gSelectedFolderID = 0;
	var gMaxFolders = 0;

	//GLOBAL FIELD REFERENCES
	var drpFolderList = document.getElementById("ctl00_ContentMain_UcSearchResults1_drpFolderList");
	var drpFolderAction = document.getElementById("ctl00_ContentMain_UcSearchResults1_drpFolderAction");
	var txtFolderMatCount = document.getElementById("ctl00_ContentMain_UcSearchResults1_txtFolderMatCount");
	var divUserMessage = document.getElementById("divUserMessage");
	var divFolderName = document.getElementById("divFolderName");
	var tblFolderName = document.getElementById("tblFolderName");
	var txtFolderName = document.getElementById("txtFolderName2");
	
	// See MS ajax.net client side documentation
	// http://ajax.asp.net/docs/ClientReference/Sys.Net/WebrequestManagerClass/	
	Sys.Net.WebRequestManager.add_invokingRequest(ucSearchResults_OnRequestStart);
	Sys.Net.WebRequestManager.add_completedRequest(ucSearchResults_OnRequestEnd);
	
	var timerID = 0;

	function ucSearchResults_OnRequestStart(){
		document.body.style.cursor="progress";
	}
	
	function ucSearchResults_OnRequestEnd(){
		document.body.style.cursor="auto";
	}

	//drpFolderAction pull down does not exist for anonymous users
	if(drpFolderAction) {
		drpFolderAction.onchange=RunFolderAction;
	}

	//RUN ONCE on page load
	//matweb.register_onload(SetSelectedFolder);
	
	SearchMatIDList = []; //an array of MatIDs
	//matweb.alert(MatIDList);
	
	//======================
	//Test Service 
	//======================
	//TestService("This is a test");
	//aci.matweb.WebServices.Folders.RaiseTestError(null,ErrorHandler,"testerror");
	
	function TestService(arg){
		aci.matweb.WebServices.Folders.HelloWorld(arg, TestService_CALLBACK,ErrorHandler,"TestService");
	}
	
	function TestService_CALLBACK(data,context){
		alert(data);
	}
	
	// =================================
	// This function is the onchange event for the material checkbox. See Code Behind.
	// The first time a material is checked, and there are no folders, the folder web service creates a NEW FOLDER
	// =================================
	function ToggleMat(chkBox,MatID,MaxFolderMats){
	
		HideFolderNameControls();

		var FolderID,Context;
		gCurrentCheckbox = chkBox;
		var matLink = $("lnkMatl_" + MatID);
		var MatName = matLink.innerHTML;
		//alert(MatName);

		var SelectedOption = drpFolderList.options[drpFolderList.selectedIndex];  

		if(gNewFolderID > 0)
			FolderID = gNewFolderID;
		else
			FolderID = SelectedOption.value; //this will be 0 if no folders exist yet
		//alert(FolderID);
		
		// call the folder web service 
		// See: ScriptManagerProxy object at the top of this page
		// Also See: /folders/FolderService.asmx & /App_Code/FolderService.cs
		if(chkBox.checked){
			Context = "AddMat";
			aci.matweb.WebServices.Folders.AddMaterial(FolderID, MatID, MatName, ToggleMat_CALLBACK,ErrorHandler,Context);
		}
		else{
			Context = "RemoveMat";
			aci.matweb.WebServices.Folders.RemoveMaterial(FolderID, MatID, ToggleMat_CALLBACK,ErrorHandler,Context);
		}
			
	}
	
	// =================================
	// CALLBACK FUNCTION
	// this function is called by the AJAX framework after the web service is invoked
	// The folder web service returns a ReturnData type object.
	// =================================
	function ToggleMat_CALLBACK(data,context){

		 //alert(data);
		// alert(context);

		var FolderMatCount = data.FolderMatCount
		var NewFolderID = data.NewFolderID
		var DisplayMessage = data.DisplayMessage
		var MaxFolderMats = data.FolderMaxMatls;
		
		txtFolderMatCount.value = FolderMatCount + "/" + MaxFolderMats;

		// If this is the first time the user checked the checkbox, and the user did not already have any folders, 
		// a new folder may have been created.
		// Save the new folderID to a hidden field, so we can reference it later.
		if(NewFolderID > 0){
			gNewFolderID = NewFolderID;
		}
		
		// alert(gNewFolderID);
		// alert(FolderCount);
		// alert(ReloadFolderList);
		// if(NewFolderID>0) alert(NewFolderID);
		// alert(MaxFolderMats);
		
		switch(context){
		case "AddMat":
			if(DisplayMessage){
				gCurrentCheckbox.checked = false;
				matweb.alert(DisplayMessage);
			}
			break    
		case "RemoveMat":
			break    
		default:
			matweb.Alert("Unhandeled Context in ToggleMat_CALLBACK() function: " + Context);
		}//end switch

	}//end function

	
	//=================================
	function CompareMaterials(){
	//=================================
		var SelectedFolderID = GetSelectedFolderID();
		window.location.href="/folders/Compare.aspx?FolderID=" + SelectedFolderID;
	}

	//=================================
	// Runs on drpFolderList.onchange, and also is called by other methods.
	// Sets the checkbox for each visible material in the folder.
	// Also sets txtFolderMatCount with the MatCount indicator for the selected folder.
	//=================================
	function SetSelectedFolder(){
		//alert("SetSelectedFolder()");
		var SelectedFolderID = GetSelectedFolderID();
		//alert("SelectedFolderID :" + SelectedFolderID)
		
		aci.matweb.WebServices.Folders.SetSelectedFolder(SelectedFolderID,SetSelectedFolder_CALLBACK,ErrorHandler,"update_txtFolderMatCount");
		
		HideFolderNameControls();
	}

	// =================================
	// CALLBACK FUNCTION
	// Returns a list of MatID's in the selected folder, and sets the checkbox for each material.
	// Also sets txtFolderMatCount with the MatCount indicator for the selected folder.
	// =================================
	function SetSelectedFolder_CALLBACK(data,context){

		// alert(data);
		// alert(context);

		// the folder web service returns an object with 4 fields
		//alert("data.FolderMatCount: " + data.FolderMatCount);
		//alert("data.FolderMaxMatls: " + data.FolderMaxMatls);
		//alert("data.DisplayMessage: " + data.DisplayMessage);
		//alert("data.MatIDList: " + data.FolderMatIDList);

		if(data.DisplayMessage){
			matweb.alert(data.DisplayMessage);
		}

		txtFolderMatCount.value = data.FolderMatCount + "/" + data.FolderMaxMatls;

		//alert(SearchMatIDList);
		var chkFolder;

		// Set checkboxes for any visible materials in the folder
		// loop over all materials in the search results for the current page...
		//alert('lenOfArray=' + SearchMatIDList.length);
		for(s=0;s<SearchMatIDList.length;s++){
			chkFolder = document.getElementById("chkFolder_" + SearchMatIDList[s]);
			//alert('index=' + SearchMatIDList[s]);
			chkFolder.checked = false; //clear any currently checked boxes...
			//loop over all materials in the folder...
			//If there is a match, make it checked.
			for(f=0;f<data.FolderMatIDList.length;f++){
				if(SearchMatIDList[s]==data.FolderMatIDList[f]){
					//alert("Match found: " + data.FolderMatIDList[f]);
					chkFolder.checked = true;
				}
			}
		}

	}//end function
	
	//=================================
	function RunFolderAction(){
	//=================================
	
		//alert("RunFolderAction()");
		var action = drpFolderAction.options[drpFolderAction.selectedIndex].value;
		var SelectedFolderName = drpFolderList.options[drpFolderList.selectedIndex].text;
		var FolderID = GetSelectedFolderID();
		var msg = "";
		
		switch(action){
			case "empty":
				aci.matweb.WebServices.Folders.EmptyFolder(FolderID,SetSelectedFolder_CALLBACK,ErrorHandler,action);
				msg = "Folder ["+SelectedFolderName+"] was cleared of materials";
				HideFolderNameControls();
				break;
			case "delete":
				var answer = confirm("Delete folder ["+SelectedFolderName+"] ?");
				if(answer){
					gOldFolderName = SelectedFolderName;
					aci.matweb.WebServices.Folders.DeleteFolder(FolderID,DeleteFolder_CALLBACK,ErrorHandler,action);
				}
				HideFolderNameControls();
				break;
			case "new":
				//setting up new folder controls
				if(drpFolderList.options.length >= gMaxFolders){
					matweb.alert("You have already added as many folders as you are currently allowed by the system (" + gMaxFolders + ").<p/> You may view our <a href=\"/membership/benefits.aspx\">benefits</a> page for the features that are allowed for your current user level.");
				}
				else{
				divFolderName.style.display="block";
				divFolderName.innerHTML="New Folder Name";
				tblFolderName.style.display="block";
				txtFolderName.value = "New Folder Name";
				txtFolderName.focus();
				txtFolderName.select();
				}
				break;
			case "rename":
				//setting up rename folder controls
				divFolderName.style.display="block";
				divFolderName.innerHTML="Rename Folder";
				tblFolderName.style.display="block";
				txtFolderName.value = SelectedFolderName;
				txtFolderName.focus();
				txtFolderName.select();
				break;
			case "managefolders":
				window.location.href="/folders/ListFolders.aspx";
				break;
			case "expSolidworks":
				ExportFolder(FolderID, 2, null);
				break;
			case "expALGOR":
				ExportFolder(FolderID, 3, null);
				break;
			case "expANSYS":
				ExportFolder(FolderID, 4, null);
				break;
			case "expNEiWorks":
				ExportFolder(FolderID, 5, null);
				break;
			case "expCOMSOL3":
				ExportFolder(FolderID, 6, '3.4 or newer');
				break;
			case "expCOMSOL4":
				ExportFolder(FolderID, 10, null);
				break;
			case "expETBX":
				ExportFolder(FolderID, 8, null);
				break;
			case "expSpaceClaim":
				ExportFolder(FolderID, 9, null);
				break;
			case "0":
				//do nothing
				drpFolderAction.selectedIndex = 0;
				break;
			default: 
				alert("Unhandled Action: " + action);
		}

		//drpFolderAction.selectedIndex=0;
		divUserMessage.innerHTML = msg;
		//alert("End RunFolderAction()");
		
	}
	
	function DeleteFolder_CALLBACK(DeleteWasSuccessfull,context){
		divUserMessage.innerHTML = "Folder ["+gOldFolderName+"] was deleted.";
		//Refresh the folder list and selected checkboxes
		aci.matweb.WebServices.Folders.GetFolderList(GetFolderList_CALLBACK,ErrorHandler,"DeleteFolder_CALLBACK");
	}	

	// rsw
	function ExportFolder(folderid, modeid, comsolversion){
		if(txtFolderMatCount.value.substring(0,1) == '0'){
			alert('Folder is empty, nothing to export.');
			drpFolderAction.selectedIndex = 0;
			return;
		}

		var ExportPath = "/folders/ListFolders.aspx";
		
		//alert(ExportPath);
		location.href = ExportPath;
		//window.open(ExportPath, '', '', '');
	}
	
	
	
	
	//===============================================================
	// This runs on the onclick event for btnApplyFolderName.
	// It handles creating a new folder, or renaming an existing one.
	//===============================================================
	function ApplyFolderName(){
	
		//alert("ApplyFolderName()");
		var action = drpFolderAction.options[drpFolderAction.selectedIndex].value;
		var NewFolderName = txtFolderName.value;
		var FolderID = GetSelectedFolderID();
		var msg = "";

		//alert(FolderID);

		//If we are showing the non-existant "virtual" folder "My Folder",
		//create it first, and then rename it.
		if(action=="rename" && FolderID==0)
			action="add_and_rename";

		switch(action){
			case "new":
				aci.matweb.WebServices.Folders.AddFolder(NewFolderName,AddRenameFolder_CALLBACK,ErrorHandler,action);
				break;
			case "rename":
				gOldFolderName = drpFolderList.options[drpFolderList.selectedIndex].text;
				aci.matweb.WebServices.Folders.RenameFolder(FolderID,NewFolderName,AddRenameFolder_CALLBACK,ErrorHandler,action);
				break;
			case "add_and_rename":
				gOldFolderName = drpFolderList.options[drpFolderList.selectedIndex].text;
				aci.matweb.WebServices.Folders.AddFolder(NewFolderName,AddRenameFolder_CALLBACK,ErrorHandler,action);
				break;
			default: 
				alert("Unhandled Action in ApplyFolderName(): " + action);
		}

		HideFolderNameControls();
		
	}
	
	//=================================
	// CALLBACK FUNCTION
	//=================================
	function AddRenameFolder_CALLBACK(ReturnData,context){
		//alert("AddRenameFolder_CALLBACK()");
		
		var msg;
		//Folder = ReturnData.Folder;
	
		switch(context){
			case "new":
				//gNewFolderID = Folder.FolderID;
				gNewFolderID = ReturnData.NewFolderID;
				msg = "The folder [" + ReturnData.NewFolderName + "] was created.";
				break;
			case "rename":
				msg = "The folder [" + gOldFolderName+ "] was renamed to [" + ReturnData.NewFolderName + "].";
				break;
			case "add_and_rename":
				//gNewFolderID = Folder.FolderID;
				gNewFolderID = ReturnData.NewFolderID;
				msg = "The folder [" + gOldFolderName+ "] was renamed to [" + ReturnData.NewFolderName + "].";
				break;
		}
		
		divUserMessage.innerHTML = msg;
		
		//refresh the folder list, so it includes the new folder
		aci.matweb.WebServices.Folders.GetFolderList(GetFolderList_CALLBACK,ErrorHandler,"AddFolder");

	}
	
	//=================================
	function HideFolderNameControls(){
	//=================================
		//These fields will not exist for anonymous users
		if(drpFolderAction){
			drpFolderAction.selectedIndex=0;
			divFolderName.style.display="none";
			tblFolderName.style.display="none";
		}
	}
	
	//=================================
	function GetSelectedFolderID(){
	//=================================
		var FolderID = 0;
		//if one of the processeses created a new folder id, then use it.
		// alert('gNewFolderID=' + gNewFolderID);
		if(gNewFolderID > 0){
			FolderID = gNewFolderID;
			//gNewFolderID = 0;//clear the new folder id, so we don't use it again.
		}
		else 
			if(drpFolderList != null){
				if(drpFolderList.options.length == 0){
					FolderID = gSelectedFolderID;
				}
				else{
					var SelectedOption = drpFolderList.options[drpFolderList.selectedIndex];  
					FolderID = SelectedOption.value; //this will be 0 if no folders exist yet
				}
			}
		return FolderID;
	}

	//=================================
	function UnregisteredUserMessage(chkBox){
	//=================================
		matweb.alert("This feature is reserved for registered users.<p/><a href='/membership/regstart.aspx'>Click here to register.</a>","Reserved Feature");
		chkBox.checked = false;
	}

	// =================================
	// CALLBACK FUNCTION
	// The folder web service returns a JS array of iBatis data objects of type: aci.matweb.data.folder.Folder 
	// The drop down list of folders is set using this list.
	// =================================
	function GetFolderList_CALLBACK(iFolderList,Context){
	
		//alert(iFolderList);
		//alert('iFolderList.length: ' + iFolderList.length);

		if(drpFolderList != null){
		
		var SelectedFolderID = GetSelectedFolderID();//remember the current selected folder, so we can use it later
		var opt;
		var folder;

		//clear the options and reload them
		drpFolderList.options.length = 0;

		//add a default empty folder...		
		if(iFolderList.length==0){
			opt = document.createElement('OPTION');
			opt.value = "0";
			opt.text = "My Folder";
			drpFolderList.options.add(opt);
			i++;
		}
		
		for(var i = 0;i < iFolderList.length;i++){
			opt = document.createElement('OPTION');
			folder = iFolderList[i];
			//alert(folder.FolderID);
			//alert(folder.FolderName);
			opt.value = folder.FolderID;
			opt.text = folder.FolderName;
			if(folder.FolderID == SelectedFolderID) opt.selected = true;
			drpFolderList.options.add(opt);
		}

		//clear the new folder id, so we don't use it again. 
		gNewFolderID = 0;
		
		//Set checkboxes and folder count fields for the new selected folder
		SetSelectedFolder();
		}

	}
	
	// =================================
	// ON ERROR CALLBACK FUNCTION
	// =================================
	function ErrorHandler(error,context) {
	
			var stackTrace = error.get_stackTrace();
			var message = error.get_message();
			var statusCode = error.get_statusCode();
			var exceptionType = error.get_exceptionType();
			var timedout = error.get_timedOut();
			var Debug = false;

			// Compose the error message...
			var ErrMsg =     
					"<strong>Stack Trace</strong><br/>" +  stackTrace + "<br/>" +
					"<strong>WebService Error</strong><br/>" + message + "<br/>" +
					"<strong>Status Code</strong><br/>" + statusCode + "<br/>" +
					"<strong>Exception Type</strong><br/>" + exceptionType + "<br/>" +
					"<strong>JS Context</strong><br/>" + context + "<br/>" +
					"<strong>Timedout</strong><br/>" + timedout + "<br/>" +
					"<strong>Source Page</strong><br/>" + "/search/MaterialGroupSearch.aspx";
			//matweb.alert(ErrMsg,"Error in WebService",null,"500");
			if(Debug){
				matweb.alert(ErrMsg,"Error in WebService",null,"500");
				//alert(ErrMsg);
			}
			else{
				ErrMsg = escape(ErrMsg);
				//matweb.alert(ErrMsg);
				//window.location.href="/errorJS.aspx?ErrMsg=" + ErrMsg;
				divUserMessage.innerHTML = "Problem retrieving folder data...Some folder features may not work.";
			}
		
	}
	
	function ShowInterpolationMessage(){
		InterpolationMsg = "The material was found via interpolation, so no data points can be displayed. See the material data sheet for actual data points.";
		matweb.alert(InterpolationMsg);
	}
	
</script>

		</td>
	</tr>
	</table>

<hr />
<a id="help"></a>
<table><tr><td>
<p/><b>Instructions:</b> This page allows you to quickly access all of the polymers/plastics, metals, ceramics, fluids, and other engineering materials
in the MatWeb material property database.&nbsp; Just select the material category you would like to find from the tree.  Click on the [+] icon to open subcategory branches in the tree.
Then click the <b>'Find'</b> button next to its box.&nbsp; You can then follow the links to the most complete data sheets on the Web, with information on over 1000 available properties, including dielectric
constant, Poisson's ratio, glass transition temperature, dissipation factor, and Vicat softening point.
</td><td><a href="https://proto3000.com"><img src="/images/assets/proto3000.jpg" border = "0"><br>

Proto3000 - Rapid Prototyping</a>
</td></tr></table>

<p/><span class="hiText">Notes:</span>

<ul>
  <li>Visit MatWeb's <a href="/search/PropertySearch.aspx">property-based search page</a> to limit your results to materials that meet specific property performance criteria.</li>
  <li>Text searches can be done from any page - use the <b>Quick Search</b> box in the top right of the page.</li>
  <li>More <a href="/help/strategy.aspx"><strong>Search Strategies</strong></a> on how to find information in MatWeb.</li>
   <li><a href="/search/GetAllMatls.aspx">Show All Materials</a> - an inefficient drill-down to data sheets.  Registered users who are logged in omit a step in this drill down process.</li>
</ul>

<p/><a href="#Top">Top</a>

<script type="text/javascript">

	var txtMatGroupText = document.getElementById("ctl00_ContentMain_txtMatGroupText");
	var txtMatGroupID = document.getElementById("ctl00_ContentMain_txtMatGroupID");
	var divMatGroupName = document.getElementById("ctl00_ContentMain_divMatGroupName");

	function ucMatGroupFinder1_OnSelected(MatGroupID,MatGroupText){
		txtMatGroupText.value = MatGroupText;
		txtMatGroupID.value = MatGroupID;
		divMatGroupName.innerHTML = MatGroupText;
		//__doPostBack("ctl00$ContentMain$btnSubmit","");
	}

	function ucMatGroupTree_OnNodeClick(NodeText,MatGroupID){
		txtMatGroupText.value = NodeText;
		txtMatGroupID.value = MatGroupID;
		divMatGroupName.innerHTML = NodeText;
		//__doPostBack("ctl00$ContentMain$btnSubmit","");
	}
	
	function validate(){
		if (txtMatGroupID.value == ""){
			matweb.alert("Please select a material category","User Input Error");
			return false;
		}
		
		return true;
		
	}

</script>


</div>
<!-- ===================================== END MAIN CONTENT ========================================== -->

<table class="tabletight" style="width:100%" >
        <tr>
                <td align="center" class="footer" colspan="3">
                        <br /><br />
                
                        <a href="/membership/regupgrade.aspx" class="footlink"><span class="subscribeLink">Subscribe to Premium Services</span></a><br/>
                
                        <span class="footer"><b>Searches:</b>&nbsp;&nbsp;</span>
                        <a href="/search/AdvancedSearch.aspx" class="footlink"><span class="foot">Advanced</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/search/CompositionSearch.aspx" class="footlink"><span class="foot">Composition</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/search/PropertySearch.aspx" class="footlink"><span class="foot">Property</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/search/MaterialGroupSearch.aspx" class="footlink"><span class="foot">Material&nbsp;Type</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/search/SearchManufacturerName.aspx" class="footlink"><span class="foot">Manufacturer</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/search/SearchTradeName.aspx" class="footlink"><span class="foot">Trade&nbsp;Name</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/search/SearchUNS.aspx" class="footlink"><span class="foot">UNS Number</span></a>
                        <br />
                        <span class="footer"><b>Other&nbsp;Links:</b>&nbsp;&nbsp;</span>
                        <a href="/services/advertising.aspx" class="footlink"><span class="foot">Advertising</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/services/submitdata.aspx" class="footlink"><span class="foot">Submit&nbsp;Data</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/services/databaselicense.aspx" class="footlink"><span class="foot">Database&nbsp;Licensing</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/services/webhosting.aspx" class="footlink"><span class="foot">Web&nbsp;Design&nbsp;&amp;&nbsp;Hosting</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/clickthrough.aspx?addataid=277" class="footlink"><span class="foot">Trade&nbsp;Publications</span></a>

                        <br />
                        <a href="/reference/suppliers.aspx" class="footlink"><span class="foot">Supplier&nbsp;List</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/tools/unitconverter.aspx" class="footlink"><span class="foot">Unit&nbsp;Converter</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/tools/contents.aspx#reference" class="footlink"><span class="foot">Reference</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/reference/link.aspx" class="footlink"><span class="foot">Links</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/help/help.aspx" class="footlink"><span class="foot">Help</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/services/contact.aspx" class="footlink"><span class="foot">Contact&nbsp;Us</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/tools/contents.aspx" class="footlink"><span class="foot" style="color:red;">Site&nbsp;Map</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/reference/faq.aspx" class="footlink"><span class="foot">FAQ</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/index.aspx" class="footlink"><span class="foot">Home</span></a>
                        <br />&nbsp;
                        <div id="fb-root"></div>

<table><tr><td><script src="http://connect.facebook.net/en_US/all.js#xfbml=1"></script><fb:like-box href="https://www.facebook.com/MatWeb.LLC" width="175" show_faces="false" stream="false" header="false"></fb:like-box>

</td><td>

<a href="https://twitter.com/MatWeb" class="twitter-follow-button" data-show-count="false" data-show-screen-name="false">Follow @MatWeb</a> <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>

</td></tr></table>
                </td>
        </tr>
        <tr>
                <td colspan="3"><img src="/images/bluebar.gif" width="100%" height="5" alt="" /></td>
        </tr>
        <tr>
                <td style="background-image:url(/images/gray.gif);"><img src="/images/spacer.gif" width="5" height="1" alt=""/></td>
                <td style="background-image:url(/images/gray.gif);">
                        <span class="footer">
                        Please read our <a href="/reference/terms.aspx" class="footlink"><span class="foot">License Agreement</span></a> regarding materials data and our  <a href="/reference/privacy.aspx" class="footlink"><span class="foot">Privacy Policy</span></a>.
                        Questions or comments about MatWeb? Please contact us at
                        <a href="mailto:webmaster@matweb.com" class="footlink"><span class="foot">webmaster@matweb.com</span></a>. We appreciate your input.
                        <br /><br />
                        The contents of this web site, the MatWeb logo, and "MatWeb" are Copyright 1996-2024
                        by MatWeb, LLC. MatWeb is intended for personal, non-commercial use. The contents, results, and technical data from this site
                        may not be reproduced either electronically, photographically or substantively without permission from MatWeb, LLC.
                        </span>
                        <br />
                </td>

                <td style="background-image:url(/images/gray.gif);"><img src="/images/spacer.gif" width="5" height="1" alt=""/></td>
        </tr>
</table>




<script type="text/javascript">
//<![CDATA[
var ctl00_ContentMain_ucMatGroupTree_msTreeView_ImageArray =  new Array('', '', '', '/WebResource.axd?d=zLp3QJeZSj4-fsnL2_raV70Mk3pr-OdjL_rvrZzXUTCnZ6LU5Dg_Tc5XZg4eZpT9O0gxZJrJcxGb23XJofeGD9JhYLr2wl0VkQZnJOx0T0ShBosq0&t=638313619312541215', '/WebResource.axd?d=vFualGN2ZZ4fny_RX_gYiUTY6FBLUapne4VfY6P3sGiyrOYd-Ji2sWg_olT-exaztendUUEOoH87RS2l44bpZTscfmmv4MKRqJK8rgesmkrNfP2O0&t=638313619312541215', '/WebResource.axd?d=4nLCbQ2Yfc2i_msrQXqKzK8pLQD-GrNVZ7DeXU-La2qatczrgeCLazv0RzIkDDHyYfLFalCBO8s71lRS8yifxTChF9dUJvTLxNEeeFlghyuY0dtM0&t=638313619312541215', '/WebResource.axd?d=uEI6Enq15_q4pXO6g0E7vtAqtD5WIokAEQ2Vz8V3Yi36cefediLfyCwENkPd-w1GxTXu-HkNqcDlrYOw8bxgGqeHJtzc-PBlTa9nrZRfdIyj8fQh0&t=638313619312541215', '/WebResource.axd?d=hzeik1gGhZrfDZXUWCMsBOx5b43bIFor8UCTzm_wuh92b9Z-WQU4_fQCSUw6CaP5tnc9LTU0jl0sy4Y6Cf_c8q8HFXRqeHQuscXIqqLNSZkgl2cu0&t=638313619312541215', '/WebResource.axd?d=6v5qM6VKQYD3XNwwXEy7Tix7dzVdqDJ8QTRIlUPrzXQqNsXQgUwIfmn34ZzRORd_IZj0bF4vkZzID1HuSFE5uKyLgaPYeAYE8VT8HYfb3Rk6zsDw0&t=638313619312541215', '/WebResource.axd?d=d9nh8nEM1YCqIfE9jLzNhPyFIZqARvlFExwnFG0vbvskO-A2qORlhTyUVIxDtSwldGrOp8o9ef8lZ9SRhPQ9rv9TkGV-NVYvH9_yHT6CvWpIPF4S0&t=638313619312541215', '/WebResource.axd?d=7_V5n1KYSQsj6PoSqgIxb0egBsAa91jSXPkbmM6FaiF14-JsNnpUDnfSdxZcvfd0OQCLDtC24pMIXigO-41cL2rsq-0xea-SSG7yZUyRcliFFqe70&t=638313619312541215', '/WebResource.axd?d=TinC6igX1vPDgWh7DstWwBTHTyH4uuybDU4NW47jJD06wHdAO3YyJC_eKK1Zn5oIbwqRo4Cri8YwhA82G-LD0p14yEF1EfoIZvKleYMIuQ2K1hkE0&t=638313619312541215', '/WebResource.axd?d=4B0dvJqmzY4ukTboQq7lmxokRL9NTsoWh4yErB11Tb-ucvMw_p_0mxYVn4U1Uz6Jk795umrO0C4-B-8Yq1Kxxz6uyDT15O2BSCb8349f4YWDdIVy0&t=638313619312541215', '/WebResource.axd?d=TUiibr9hjS_MlNf09dKZECe1z56fbaPbfgJfC5eYJi0TtdBviUcA1JQJLV81aqunzpXysgruhL3ihpgd7ziL5p9SQ86xVLNiUqbYpJnW9Rj7sN1E0&t=638313619312541215', '/WebResource.axd?d=vUBrM2nNhECNbFO0AJKq52p2zWrY5XG8nWgCDL4AfsrexStb0HREDV6GHMvdkAGDInxYtTyWXuxFUIiQv3JX2fScVRwQsrVv5srHrjOIeC3ayAG20&t=638313619312541215', '/WebResource.axd?d=Z-LP5lKcsnv2A9UpExXu-5leQYIAZMYp6SXNognZi1r66YT8u24-YjJaxjisE9ruYU8NKnNz72H4shclsk8PM3S5M-VYMk_F_G5fcd3ra6Sj9CaS0&t=638313619312541215', '/WebResource.axd?d=QI4SACqRn7PxBBHPfkR7peFMAEv7lX1_iZ_7jncKwxRTlvhSAncd5K2oRqw8J8WmJjWQcbGgEblhrufy-hUjZMa1YLUC47mjfV1eS4LM3kUqLT960&t=638313619312541215', '/WebResource.axd?d=ayrFqNSzdiTAdowDVbEH-ZzKbvw5bCq7onyCSAcJhCVPVIHlUB9M_fo8BLUTeGpbtcoMiyFy1MoeEIsweMrpVandBKC4vAK-NCBcfn2FIreK8mEJ0&t=638313619312541215', '/WebResource.axd?d=OK_uWiuoGURaJ3CC0fVlQ7v9GUnIAkx03w03rQ2ENqcwdObLiQwK7B9yRFyAWj9tuUqsa0874ksgkaRo8KirB7q1ZL_huh7gCq_XehXfHMEKMiAl0&t=638313619312541215');
//]]>
</script>


<script type="text/javascript">
//<![CDATA[
(function() {var fn = function() {AjaxControlToolkit.ModalPopupBehavior.invokeViaServer('ctl00_ContentMain_ucPopupMessage1_ModalPopupExtender1', false); Sys.Application.remove_load(fn);};Sys.Application.add_load(fn);})();
var callBackFrameUrl='/WebResource.axd?d=Mt7MojvAyTqAEcx0MpKgcF_QVa09tLyMQS6c5JLNO4j2M_F6CMxYnN0HdVYxyrhggOpkTwyNqQ51iS6UoHXgJqHBROE1&t=638313619312541215';
WebForm_InitCallback();var ctl00_ContentMain_ucMatGroupTree_msTreeView_Data = new Object();
ctl00_ContentMain_ucMatGroupTree_msTreeView_Data.images = ctl00_ContentMain_ucMatGroupTree_msTreeView_ImageArray;
ctl00_ContentMain_ucMatGroupTree_msTreeView_Data.collapseToolTip = "Collapse {0}";
ctl00_ContentMain_ucMatGroupTree_msTreeView_Data.expandToolTip = "Expand {0}";
ctl00_ContentMain_ucMatGroupTree_msTreeView_Data.expandState = theForm.elements['ctl00_ContentMain_ucMatGroupTree_msTreeView_ExpandState'];
ctl00_ContentMain_ucMatGroupTree_msTreeView_Data.selectedNodeID = theForm.elements['ctl00_ContentMain_ucMatGroupTree_msTreeView_SelectedNode'];
for (var i=0;i<19;i++) {
var preLoad = new Image();
if (ctl00_ContentMain_ucMatGroupTree_msTreeView_ImageArray[i].length > 0)
preLoad.src = ctl00_ContentMain_ucMatGroupTree_msTreeView_ImageArray[i];
}
ctl00_ContentMain_ucMatGroupTree_msTreeView_Data.lastIndex = 8;
ctl00_ContentMain_ucMatGroupTree_msTreeView_Data.populateLog = theForm.elements['ctl00_ContentMain_ucMatGroupTree_msTreeView_PopulateLog'];
ctl00_ContentMain_ucMatGroupTree_msTreeView_Data.treeViewID = 'ctl00$ContentMain$ucMatGroupTree$msTreeView';
ctl00_ContentMain_ucMatGroupTree_msTreeView_Data.name = 'ctl00_ContentMain_ucMatGroupTree_msTreeView_Data';
Sys.Application.initialize();
Sys.Application.add_init(function() {
    $create(AjaxControlToolkit.ModalPopupBehavior, {"BackgroundCssClass":"modalBackground","OkControlID":"ctl00_ContentMain_ucPopupMessage1_btnOK","PopupControlID":"ctl00_ContentMain_ucPopupMessage1_divPopupMessage","PopupDragHandleControlID":"ctl00_ContentMain_ucPopupMessage1_trTitleRow","dynamicServicePath":"/search/MaterialGroupSearch.aspx","id":"ctl00_ContentMain_ucPopupMessage1_ModalPopupExtender1"}, null, null, $get("ctl00_ContentMain_ucPopupMessage1_hndPopupControl"));
});
Sys.Application.add_init(function() {
    $create(AjaxControlToolkit.TextBoxWatermarkBehavior, {"ClientStateFieldID":"ctl00_ContentMain_UcMatGroupFinder1_TextBoxWatermarkExtender2_ClientState","WatermarkCssClass":"greyOut","WatermarkText":"Type at least 4 characters here...","id":"ctl00_ContentMain_UcMatGroupFinder1_TextBoxWatermarkExtender2"}, null, null, $get("ctl00_ContentMain_UcMatGroupFinder1_txtSearchText"));
});
//]]>
</script>
</form>


<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-15290815-1");
pageTracker._setDomainName(".matweb.com");
pageTracker._trackPageview();
} catch(err) {}</script>

<!-- 20160905 insert SpecialChem javascript -->
<script type="text/javascript" src="//collect.specialchem.com/collect.js"></script>
<script type="text/javascript">
    //<![CDATA[
    var _spc = (_spc || []);
    _spc.push(['init',
    {
        partner: 'MatWeb'
        
        //, iid: '12345'
    }]);
    //]]>
</script>
<script> (function(){ var s = document.createElement('script'); var h = document.querySelector('head') || document.body; s.src = 'https://acsbapp.com/apps/app/dist/js/app.js'; s.async = true; s.onload = function(){ acsbJS.init({ statementLink : '', footerHtml : '', hideMobile : false, hideTrigger : false, disableBgProcess : false, language : 'en', position : 'right', leadColor : '#146FF8', triggerColor : '#146FF8', triggerRadius : '50%', triggerPositionX : 'right', triggerPositionY : 'bottom', triggerIcon : 'people', triggerSize : 'bottom', triggerOffsetX : 20, triggerOffsetY : 20, mobile : { triggerSize : 'small', triggerPositionX : 'right', triggerPositionY : 'bottom', triggerOffsetX : 20, triggerOffsetY : 20, triggerRadius : '20' } }); }; h.appendChild(s); })();</script>
</body>
</html>
