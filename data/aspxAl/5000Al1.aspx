

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head id="ctl00_mainHeader"><title>
	Metal, Plastic, and Ceramic Search Index
</title><meta http-equiv="Content-Style-Type" content="text/css" /><meta http-equiv="Content-Language" content="en" />

        <script src="/includes/flash.js" type="text/javascript"></script>

        <script src="/includes/prototype.js" type="text/javascript"></script>
        <!-- script src="/includes/scriptaculous/effects.js" type="text/javascript"></script -->
        <script src="/includes/matweb.js" type="text/javascript"></script>
        <link rel="stylesheet" href="/includes/main.css" />

        <script type="text/javascript">

                function _onload(){
                        matweb.debug = false; // ;
                        //matweb.writeOverlay();
                }

                function _onkeydown(evt){
                        //PREVENT GLOBAL AUTO-SUBMIT OF FORMS
                        return matweb.DisableAutoSubmit(evt);
                }

                //EXAMPLE USAGE:
                //matweb.register_onload(function(){alert("hello from onload event");});
                //matweb.writeOverlay();


        </script>

        <style type="text/css" media="screen">
        @import url("/ad_IES/css/styles.css");
        </style>
<meta name="KEYWORDS" content="carbon steels, low alloy steels, corrosion resistant stainless, high temperature stainless, heat resistant stainless steels, AISI steel alloy specifications, high strength steel alloys, tensile strength of steels, maraging, superalloy, lead alloy, magnesium, tin alloy, tungsten, zinc, precious metal, pure metallic element, TPE elastomer, dielectric constant, plastic dissipation factor, nylon glass transition temperature, ionomer vicat softening point, poisson's ratio, polyetherimide, fluoropolymer, silicone, polyester dielectric constant" /><meta name="DESCRIPTION" content="Searchable list of plastics, metals, and ceramics categorized into a fields like polypropylene, nylon, ABS, aluminum alloys, oxides, etc.  Search results are detailed data sheets with tensile strength, density, dielectric constant, dissipation factor, poisson's ratio, glass transition temperature, and Vicat softening point." /><meta name="ROBOTS" content="NOFOLLOW" /><style type="text/css">
	.ctl00_ContentMain_ucMatGroupTree_msTreeView_0 { text-decoration:none; }
	.ctl00_ContentMain_ucMatGroupTree_msTreeView_1 { color:Black; }
	.ctl00_ContentMain_ucMatGroupTree_msTreeView_2 { padding:0px 3px 0px 3px; }

</style></head>

<body onload="_onload();" onkeydown="return _onkeydown(event);">

<a id="Top"></a>

<div id="divSiteName" style="position:absolute; top:0; left:0 ;">
<a href="/index.aspx"><img src="/images/sitelive.gif" alt="" /></a>
</div>

<div id="divPopupOverlay" style="display:none;"></div>

<div id="divPopupAlert" class="popupAlert" style="display:none;">
        <table style="width:100%; height:100%; border-style:outset; border-width:5px; border-color:blue;" >
                <tr><th style=" height:25px; background-color:Maroon; font-size:14px; color:White; "><span id="spanPopupAlertTitle">System Message</span></th></tr>
                <tr><td style=" vertical-align:top; padding:3px; background-color:White;" id="tdPopupAlertMessage" ></td></tr>
                <tr><td style=" height:25px; text-align:center; background-color:Silver;"><input type="button" id="btnPopupAlert" onclick="matweb.alertClose();" style="" value="OK" /></td></tr>
        </table>
</div>

<form name="aspnetForm" method="post" action="MaterialGroupSearch.aspx" onsubmit="javascript:return WebForm_OnSubmit();" id="aspnetForm">
<div>
<input type="hidden" name="__EVENTTARGET" id="__EVENTTARGET" value="" />
<input type="hidden" name="__EVENTARGUMENT" id="__EVENTARGUMENT" value="" />
<input type="hidden" name="__LASTFOCUS" id="__LASTFOCUS" value="" />
<input type="hidden" name="ctl00_ContentMain_ucMatGroupTree_msTreeView_ExpandState" id="ctl00_ContentMain_ucMatGroupTree_msTreeView_ExpandState" value="ccccccnc" />
<input type="hidden" name="ctl00_ContentMain_ucMatGroupTree_msTreeView_SelectedNode" id="ctl00_ContentMain_ucMatGroupTree_msTreeView_SelectedNode" value="" />
<input type="hidden" name="ctl00_ContentMain_ucMatGroupTree_msTreeView_PopulateLog" id="ctl00_ContentMain_ucMatGroupTree_msTreeView_PopulateLog" value="" />
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="/wEPDwULLTE2Nzc3MDc2MDAPZBYCZg9kFgICAQ9kFioCAQ9kFgJmDxYCHgRUZXh0BWdEYXRhIHNoZWV0cyBmb3Igb3ZlciA8c3BhbiBjbGFzcz0iZ3JlZXRpbmciPjE4MCwwMDA8L3NwYW4+ICBtZXRhbHMsIHBsYXN0aWNzLCBjZXJhbWljcywgYW5kIGNvbXBvc2l0ZXMuZAICD2QWAmYPFgIfAAUESE9NRWQCAw9kFgJmDxYCHwAFBlNFQVJDSGQCBA9kFgJmDxYCHwAFBVRPT0xTZAIFD2QWAmYPFgIfAAUJU1VQUExJRVJTZAIGD2QWAmYPFgIfAAUHRk9MREVSU2QCBw9kFgJmDxYCHwAFCEFCT1VUIFVTZAIID2QWAmYPFgIfAAUDRkFRZAIJD2QWAmYPFgIfAAUHQUNDT1VOVGQCCg9kFgJmDxYCHwAFBkxPRyBJTmQCCw8QZGQWAWZkAgwPZBYCZg8WAh8ABQhTZWFyY2hlc2QCDQ9kFgJmDxYCHwAFCEFkdmFuY2VkZAIOD2QWAmYPFgIfAAUIQ2F0ZWdvcnlkAg8PZBYCZg8WAh8ABQhQcm9wZXJ0eWQCEA9kFgJmDxYCHwAFBk1ldGFsc2QCEQ9kFgJmDxYCHwAFClRyYWRlIE5hbWVkAhIPZBYCZg8WAh8ABQxNYW51ZmFjdHVyZXJkAhMPZBYCAgEPZBYCZg8WAh8ABRlSZWNlbnRseSBWaWV3ZWQgTWF0ZXJpYWxzZAIVD2QWEAIDD2QWAmYPFgIfAAWZATxhIGhyZWY9Ii9jbGlja3Rocm91Z2guYXNweD9hZGRhdGFpZD0xMzQ5Ij48aW1nIHNyYz0iL2ltYWdlcy9hc3NldHMvbWV0YWxtZW4tQWx1bWludW0ucG5nIiBib3JkZXI9MCBhbHQ9Ik1ldGFsbWVuIFNhbGVzIiB3aWR0aCA9ICI3MjgiIGhlaWdodCA9ICI5MCI+PC9hPmQCBA9kFgICAg8WAh4Fc3R5bGUFGWRpc3BsYXk6bm9uZTt3aWR0aDoyNTBweDsWBgIBD2QWAmYPZBYCZg8PFgIfAAUSVXNlciBJbnB1dCBQcm9ibGVtZGQCAw8PFgIfAGVkZAIFDxYCHgV2YWx1ZQUCT0tkAgUPZBYEAgEPD2QWAh8BBQxXaWR0aDozMDRweDtkAgIPEBYEHwEFK3Bvc2l0aW9uOmFic29sdXRlO2Rpc3BsYXk6bm9uZTtXaWR0aDozMDhweDseBHNpemUFAjE4ZGRkAgYPDxYEHghDbGllbnRJRAIBHgpMYW5ndWFnZUlEAgFkFgJmDxQrAAkPFg4eF1BvcHVsYXRlTm9kZXNGcm9tQ2xpZW50Zx4SRW5hYmxlQ2xpZW50U2NyaXB0Zx4JU2hvd0xpbmVzZx4ITm9kZVdyYXBnHg1OZXZlckV4cGFuZGVkZB4MU2VsZWN0ZWROb2RlZB4JTGFzdEluZGV4AghkFggeC05vZGVTcGFjaW5nGwAAAAAAAAAAAQAAAB4JRm9yZUNvbG9yCiMeEUhvcml6b250YWxQYWRkaW5nGwAAAAAAAAhAAQAAAB4EXyFTQgKEgBhkZGRkZGQUKwAJBSMyOjAsMDowLDA6MSwwOjIsMDozLDA6NCwwOjUsMDo2LDA6NxQrAAIWDB8ABRJDYXJib24gKDg2NiBtYXRscykeBVZhbHVlBQMyODMeCEV4cGFuZGVkaB4MU2VsZWN0QWN0aW9uCyouU3lzdGVtLldlYi5VSS5XZWJDb250cm9scy5UcmVlTm9kZVNlbGVjdEFjdGlvbgIeC05hdmlnYXRlVXJsBUFqYXZhc2NyaXB0OnVjTWF0R3JvdXBUcmVlX09uTm9kZUNsaWNrKCdDYXJib24gKDg2NiBtYXRscyknLCcyODMnKR4QUG9wdWxhdGVPbkRlbWFuZGdkFCsAAhYMHwAFFUNlcmFtaWMgKDEwMDA0IG1hdGxzKR8RBQIxMR8SaB8TCysEAh8UBUNqYXZhc2NyaXB0OnVjTWF0R3JvdXBUcmVlX09uTm9kZUNsaWNrKCdDZXJhbWljICgxMDAwNCBtYXRscyknLCcxMScpHxVnZBQrAAIWDB8ABRJGbHVpZCAoNzU2MiBtYXRscykfEQUBNR8SaB8TCysEAh8UBT9qYXZhc2NyaXB0OnVjTWF0R3JvdXBUcmVlX09uTm9kZUNsaWNrKCdGbHVpZCAoNzU2MiBtYXRscyknLCc1JykfFWdkFCsAAhYMHwAFE01ldGFsICgxNzA1MiBtYXRscykfEQUBOR8SaB8TCysEAh8UBUBqYXZhc2NyaXB0OnVjTWF0R3JvdXBUcmVlX09uTm9kZUNsaWNrKCdNZXRhbCAoMTcwNTIgbWF0bHMpJywnOScpHxVnZBQrAAIWDB8ABSdPdGhlciBFbmdpbmVlcmluZyBNYXRlcmlhbCAoODA2MyBtYXRscykfEQUDMjg1HxJoHxMLKwQCHxQFVmphdmFzY3JpcHQ6dWNNYXRHcm91cFRyZWVfT25Ob2RlQ2xpY2soJ090aGVyIEVuZ2luZWVyaW5nIE1hdGVyaWFsICg4MDYzIG1hdGxzKScsJzI4NScpHxVnZBQrAAIWDB8ABRVQb2x5bWVyICg5NzYzNSBtYXRscykfEQUCMTAfEmgfEwsrBAIfFAVDamF2YXNjcmlwdDp1Y01hdEdyb3VwVHJlZV9Pbk5vZGVDbGljaygnUG9seW1lciAoOTc2MzUgbWF0bHMpJywnMTAnKR8VZ2QUKwACFgwfAAUYUHVyZSBFbGVtZW50ICg1MDcgbWF0bHMpHxEFAzE4NB8SaB8TCysEAh8UBUdqYXZhc2NyaXB0OnVjTWF0R3JvdXBUcmVlX09uTm9kZUNsaWNrKCdQdXJlIEVsZW1lbnQgKDUwNyBtYXRscyknLCcxODQnKR8VaGQUKwACFgwfAAUlV29vZCBhbmQgTmF0dXJhbCBQcm9kdWN0cyAoMzk4IG1hdGxzKR8RBQMyNzcfEmgfEwsrBAIfFAVUamF2YXNjcmlwdDp1Y01hdEdyb3VwVHJlZV9Pbk5vZGVDbGljaygnV29vZCBhbmQgTmF0dXJhbCBQcm9kdWN0cyAoMzk4IG1hdGxzKScsJzI3NycpHxVnZGQCBw8WAh4JaW5uZXJodG1sBSY1MDAwIFNlcmllcyBBbHVtaW51bSBBbGxveSAoMjk5IG1hdGxzKWQCDQ8PFgIeB1Zpc2libGVoZGQCDg8PFgIfF2dkFgQCAQ8PFgIfAAUDMjk5ZGQCAw8PFgIfAAUaNTAwMCBTZXJpZXMgQWx1bWludW0gQWxsb3lkZAIPDw8WBh4LQ3VycmVudFBhZ2UCAR4PQ3VycmVudFNlYXJjaElEAr7G/BIfF2dkFgQCAg8PFgIfAGVkZAIDDw8WAh8XZ2QWPgIBDw8WAh8ABQMyOTlkZAIDDxBkEBUCATEBMhUCATEBMhQrAwJnZxYBZmQCBQ8PFgIfAAUBMmRkAgcPDxYIHwAFC1tQcmV2IFBhZ2VdHw4KTh4HRW5hYmxlZGgfEAIEZGQCCQ8PFggfAAULW05leHQgUGFnZV0fDgojHxACBB8aZ2RkAgsPEGRkFgECBWQCDQ8WAh8XaGQCDw8WAh8XaGQCEQ8QD2QWAh4Ib25jaGFuZ2UFE1NldFNlbGVjdGVkRm9sZGVyKClkZGQCFQ8WAh8XaBYCAgEPEGRkFgFmZAIXD2QWBAIBDw8WBB8AZR8aaGRkAgIPDxYCHxdoZGQCGQ8PFgQfAAUNTWF0ZXJpYWwgTmFtZR8aaGRkAhsPDxYCHxdoZGQCHQ8PFgIfF2hkZAIfD2QWBgIBDw8WBB8ABQVQcm9wMR8aaGRkAgMPDxYCHxdoZGQCBQ8PFgIfF2hkZAIhD2QWBgIBDw8WBB8ABQVQcm9wMh8aaGRkAgMPDxYCHxdoZGQCBQ8PFgIfF2hkZAIjD2QWBgIBDw8WBB8ABQVQcm9wMx8aaGRkAgMPDxYCHxdoZGQCBQ8PFgIfF2hkZAIlD2QWBgIBDw8WBB8ABQVQcm9wNB8aaGRkAgMPDxYCHxdoZGQCBQ8PFgIfF2hkZAInD2QWBgIBDw8WBB8ABQVQcm9wNR8aaGRkAgMPDxYCHxdoZGQCBQ8PFgIfF2hkZAIpD2QWBgIBDw8WBB8ABQVQcm9wNh8aaGRkAgMPDxYCHxdoZGQCBQ8PFgIfF2hkZAIrD2QWBgIBDw8WBB8ABQVQcm9wNx8aaGRkAgMPDxYCHxdoZGQCBQ8PFgIfF2hkZAItD2QWBgIBDw8WBB8ABQVQcm9wOB8aaGRkAgMPDxYCHxdoZGQCBQ8PFgIfF2hkZAIvD2QWBgIBDw8WBB8ABQVQcm9wOR8aaGRkAgMPDxYCHxdoZGQCBQ8PFgIfF2hkZAIxD2QWBgIBDw8WBB8ABQZQcm9wMTAfGmhkZAIDDw8WAh8XaGRkAgUPDxYCHxdoZGQCNQ8PFgIfAAUDMjk5ZGQCNw8QZBAVAgExATIVAgExATIUKwMCZ2cWAWZkAjkPDxYCHwAFATJkZAI7Dw8WCB8ABQtbUHJldiBQYWdlXR8OCk4fGmgfEAIEZGQCPQ8PFggfAAULW05leHQgUGFnZV0fDgojHxACBB8aZ2RkAj8PEGRkFgECBWQCQQ8WAh8XaBYCAgEPDxYCHwAFuAQ8YnIgLz4NCk1hdGVyaWFscyBmbGFnZ2VkIGFzIGRpc2NvbnRpbnVlZCAoPGltZyBzcmM9Ii9pbWFnZXMvYnV0dG9ucy9pY29uRGlzY29udGludWVkLmpwZyIgYWx0PSIiIC8+KSBhcmUgbm8gbG9uZ2VyIHBhcnQgb2YgdGhlIG1hbnVmYWN0dXJlcuKAmXMgc3RhbmRhcmQgcHJvZHVjdCBsaW5lIGFjY29yZGluZyB0byBvdXIgbGF0ZXN0IGluZm9ybWF0aW9uLiAgVGhlc2UgbWF0ZXJpYWxzIG1heSBiZSBhdmFpbGFibGUgYnkgc3BlY2lhbCBvcmRlciwgaW4gZGlzdHJpYnV0aW9uIGludmVudG9yeSwgb3IgcmVpbnN0YXRlZCBhcyBhbiBhY3RpdmUgcHJvZHVjdC4gIERhdGEgc2hlZXRzIGZyb20gbWF0ZXJpYWxzIHRoYXQgYXJlIG5vIGxvbmdlciBhdmFpbGFibGUgcmVtYWluIGluIE1hdFdlYiB0byBhc3Npc3QgdXNlcnMgaW4gZmluZGluZyByZXBsYWNlbWVudCBtYXRlcmlhbHMuICANCjxiciAvPjxiciAvPg0KVXNlcnMgb2Ygb3VyIEFkdmFuY2VkIFNlYXJjaCAocmVnaXN0cmF0aW9uIHJlcXVpcmVkKSBtYXkgZXhjbHVkZSBkaXNjb250aW51ZWQgbWF0ZXJpYWxzIGZyb20gc2VhcmNoIHJlc3VsdHMuZGQCFg9kFgJmDxYCHwAFcDxhIGhyZWY9Ii9jbGlja3Rocm91Z2guYXNweD9hZGRhdGFpZD0yNzciIGNsYXNzPSJmb290bGluayI+PHNwYW4gY2xhc3M9ImZvb3QiPlRyYWRlJm5ic3A7UHVibGljYXRpb25zPC9zcGFuPjwvYT5kGAEFHl9fQ29udHJvbHNSZXF1aXJlUG9zdEJhY2tLZXlfXxYEBTZjdGwwMCRDb250ZW50TWFpbiRVY01hdEdyb3VwRmluZGVyMSRzZWxlY3RDYXRlZ29yeUxpc3QFK2N0bDAwJENvbnRlbnRNYWluJHVjTWF0R3JvdXBUcmVlJG1zVHJlZVZpZXcFG2N0bDAwJENvbnRlbnRNYWluJGJ0blN1Ym1pdAUaY3RsMDAkQ29udGVudE1haW4kYnRuUmVzZXS1w7uItJXP7t33UEtMv2xQhhriMg==" />
</div>

<script type="text/javascript">
//<![CDATA[
var theForm = document.forms['aspnetForm'];
if (!theForm) {
    theForm = document.aspnetForm;
}
function __doPostBack(eventTarget, eventArgument) {
    if (!theForm.onsubmit || (theForm.onsubmit() != false)) {
        theForm.__EVENTTARGET.value = eventTarget;
        theForm.__EVENTARGUMENT.value = eventArgument;
        theForm.submit();
    }
}
//]]>
</script>


<script src="/WebResource.axd?d=M68BSyLvwBMHRYzsSV62mvczr7w5VyKnq2JbgqcAAU1ioThlC6FNEVBncBdaA4oqf8cXPzy7QERzajvFfM67hSR6S0w1&amp;t=638313619312541215" type="text/javascript"></script>


<script src="/WebResource.axd?d=ClzPoKUn1iQnMGy7NeMJw70TvMayZ5w9nNmB7h_dY64i4BFFgjVcppyp69BFqhuzHctzc-rEantAjczzQ6UVMYkaWJw1&amp;t=638313619312541215" type="text/javascript"></script>
<script type="text/javascript">
//<![CDATA[

    function TreeView_PopulateNodeDoCallBack(context,param) {
        WebForm_DoCallback(context.data.treeViewID,param,TreeView_ProcessNodeData,context,TreeView_ProcessNodeData,false);
    }
var ctl00_ContentMain_ucMatGroupTree_msTreeView_Data = null;//]]>
</script>

<script src="/ScriptResource.axd?d=148LWaywfmset9PYVR0m-KYXX9JfyGYGqOtIYBGEAzoJh5VhFBwSI6JxnmoNvgtlAprhvntpc6lTDbRMgdKxckUmc8m2qw_fOCsX0G9LC8ojUpw4cTHWXgfuT_vU6m2Kpv3mt01iCczrTE2-J5X6EeBa2281&amp;t=637769794779625837" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=v_V1UgDrDAchg8Zuc1FL4qkvmjt-bwap7ZxwVGGPleYiJ6zfDOTL6wtCmVvILjbwo_sqb2PMUTNkjO4vp6nd-E7WWXTPgSI5-nvn3YDnHrVk5bgVX4Qv6F0fV4wTSslmaLZ3k98tckojDLWAEI7jm8h0H4d7BoGYxkqsyh8-Iujfrjjm0&amp;t=637769794779625837" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=V7Zma3AISNyGTbHVRPw8svJ4Ub7xUUaeoKwImcvnuDiy-XBR331l3eTjiEP8J_QlX2qgyAHxZFLywNbNyZ50fJLVEOCjx11lIDJOiQxc0oGA3qi7XXiggiwEcIWwzb9j2Is1n1D9sU40Zusk35AtkqRKL3Y1&amp;t=633177911400000000" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=wOguQo_yd3hCG4ajcSXPC6-XdeL0hmYWSMaFjvMXt1CBqR9neJqTXyTfPki4lIz4B29Zz_83btvS12BY6KpBKT-AgP0eaXp7V3h6XRdjiozV5w_n3oQRZg5Yq1QW8UNGj0Cy9jsap_7EcU2M99CapUyVG6I1&amp;t=633177911400000000" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=MAJxSy62Z3EtRwHXuuMc8KC6PFmrPqmkpcwdpcPGeqtqUQyJ3FBQ3cFyYWN1zNID_ngA53aMZdG_dpRu3TRDH8-iqKAjPAcc2BWe63WYGrGbyHc9syJxR6lypTgvRaYmwwEmkLEsaGUniMurHc16yr1VrjmzG3HWOeu9UXNr23Wv2xzh0&amp;t=633177911400000000" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=ukx9eyyt483y_CGgCF1OToQPWzPYyCynhpMvhxN7idzW3WaKjHjpgAOcMgtXZIEcspnbyDEADdRWCSHB-hF39Ycl13KWsqdlOHElw2qgSkGmkL8iNH37htb0BNS0lxw6oHvtqYiVpw-nZeYoQ1NkgX2qXOI1&amp;t=633177911400000000" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=Ovkr-MWDypjnq0C7FVbNgOs-Mt3NaktNVf8SEOX721PoQ5mQqTM6S4igSQL1_PXvh96km3stLiqrorOgm8nnnUU6y6edTkhxmLiwfFtEXQEAAnFFQpUepgsxEd0ahMvWcDfEac0EE7J8iDb2rqSMTxk51ydfY-c-8zcKvo5Mrn83cvfN0&amp;t=633177911400000000" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=pKQF4iQYwkSn5cAwTNDM8ez4TLepowWnai-WD6NVL5h0ay9Qtt0a9vPCaI-KN9OHcv1fL8VnwxEDM6EAVtgZY06V8XJTlVrbmb8WlMvGmhwBeL3_SVtKKOS-QExY8AnVbTFl_wOknplhy8bz-9sIWNCh4XPq1WhRj7ALNAKaeruPhS7d0&amp;t=633177911400000000" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=uM6eUuIuIa_NiCgVrtwgqczDeqTqI37XalY8Ri_W2ZjXEzIKi84afVBjFk8gneIDXcFnw8yjCD1qhE8mzDTzAUSdKnbDte0yC23kMoyvxPyu7vRH9E-6JMZtE8-gELUPmoZdOZJ9nXitHKYBLw2P6skU1IkzFRegK74bETu2FdEtuaeR0&amp;t=633177911400000000" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=ewF5FMb1ckbNCdJ9rpE5xMSXy-WecqVGE8uma1_520KusuW7jTUE2tnPMG7JyR0FDIFqDUMQbsfTZlsQ0sD4YAW8MnN90GKIMPvzaP7eOG3ss8E050_l7Rpnaz7I_U3ITzBXkcJZIOPcI1PxOReT8FSeWmo1&amp;t=633177911400000000" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=3lB_EBCMPyiwzDhzWdoE-i4mLHhkIMbbkaLwMr-U1WcAoDqBHS-oYFn4gz3ZwnpzK_KZp97Ev8ZbiUNo7WMJ45h2xgOH59erMFo1BpNb87-yu3X0mFyWs5rB3ANm4lsVSIu7RZd7XUEwfZXzugJ3CoJqxvRqiTGLpcXquEF24hK3OUH-0&amp;t=633177911400000000" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=Z7-pZSKJ6vCyr9H9S4r9baB7TLiJ-T4EoMOvlH-iQ5-icaO-vWMrjXk3EPctuPrcO5qwJSTaLQtHyguwle7Kty78exrSwHataIriNUEu1DnUkixv4pxU7Dknp_GDVHby5XcMA4lMwazAE0AFG9Cndu0An_gAGACGlYXsGUG9W6okCQsH0&amp;t=633177911400000000" type="text/javascript"></script>
<script src="../WebServices/Materials.asmx/js" type="text/javascript"></script>
<script src="../WebServices/MatGroups.asmx/js" type="text/javascript"></script>
<script src="../WebServices/Folders.asmx/js" type="text/javascript"></script>
<script type="text/javascript">
//<![CDATA[
function WebForm_OnSubmit() {
null;
return true;
}
//]]>
</script>

<div>

	<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="640EFF8C" />
</div>

<!--
The ScriptManager control is required for all ASP AJAX functionality - especially used by search pages.
It generates the javascript required.
Only one script manager is allowed on a page, and since this is the master page, this is the main script manager
However, a ScriptManagerProxy control can be used to set properties and settings on indivual pages.
For an example, see /WebControls/ucSearchResults.ascx
-->
<script type="text/javascript">
//<![CDATA[
Sys.WebForms.PageRequestManager._initialize('ctl00$ajxScriptManager', document.getElementById('aspnetForm'));
Sys.WebForms.PageRequestManager.getInstance()._updateControls([], [], [], 90);
//]]>
</script>


<table class="tabletight" style="width:100%; text-align:center;background-image: url(/images/gray.gif);">
  <tr >
    <td rowspan="2"><a href="/index.aspx"><img src="/images/logo1.gif" width="270" height="42" alt="MatWeb - Material Property Data"  /></a></td>

                
                <td style="text-align:right;vertical-align:middle; width:90%" ><span class='greeting'><span class='hiText'><a href ='/services/advertising.aspx'>Advertise with MatWeb!</a></span>&nbsp;&nbsp;&nbsp;&nbsp;</span></td>
                <td style="text-align:right;vertical-align:middle; padding-right:5px;" ><a href='/membership/regstart.aspx'><img src='/images/buttons/btnRegisterBSF.gif' width="62" height="20" alt="Register Now" /></a></td>

                
  </tr>
  <tr >
    <td  align="left" valign="bottom" colspan="2"><span class="tagline">
    
    Data sheets for over <span class="greeting">180,000</span>  metals, plastics, ceramics, and composites.</span></td>
  </tr>
  <tr style="background-image:url(/images/blue.gif); background-color:#000099; " >
    <td bgcolor="#000099"><a href="/index.aspx"><img src="/images/logo2.gif" width="270" height="23px" alt="MatWeb - Material Property Data" border="0" /></a></td>
    <td class="topnavig8" style=" background-color:#000099;vertical-align:middle; text-align:right; white-space:nowrap;" colspan="2" align="right" valign="middle">
        <a href="/index.aspx" class="topnavlink">HOME</a>&nbsp;&nbsp;&#8226;&nbsp;&nbsp;
                        <a href="/search/search.aspx" class="topnavlink">SEARCH</a>&nbsp;&nbsp;&#8226;&nbsp;&nbsp;
                        <a href="/tools/tools.aspx" class="topnavlink">TOOLS</a>&nbsp;&nbsp;&#8226;&nbsp;&nbsp;
                        <a href="/reference/suppliers.aspx" class="topnavlink">SUPPLIERS</a>&nbsp;&nbsp;&#8226;&nbsp;&nbsp;
                        <a href="/folders/ListFolders.aspx" class="topnavlink">FOLDERS</a>&nbsp;&nbsp;&#8226;&nbsp;&nbsp;
                        <a href="/services/services.aspx" class="topnavlink">ABOUT US</a>&nbsp;&nbsp;&#8226;&nbsp;&nbsp;
                        <a href="/reference/faq.aspx" class="topnavlink">FAQ</a>&nbsp;&nbsp;&#8226;&nbsp;&nbsp;
                        
                        <a href="/membership/login.aspx" class="topnavlink">LOG IN</a>
                        
                        &nbsp;
                        
                        &nbsp;
    </td>
  </tr>
</table>

<div id="divRecentMatls" class="tableborder tight" style=" display:none; position:absolute; top:95px; left:150px; width:400px; height:400px;" onmouseover="clearTimeout(tmrtmp);" onmouseout="tmrtmp=setTimeout('RecentMatls.Hide()', 800)">
        <table class="headerblock tabletight" style="vertical-align:top; font-size:13px; width:100%;"><tr>
                <td style="padding:3px;"> Recently Viewed Materials (most recent at top)</td>
                <td style="padding:3px;text-align:right;"><input type="image" src="/images/buttons/btnCloseWhite.gif" onclick="RecentMatls.Hide();return false;" style="cursor:pointer;" />&nbsp;</td>
        </tr></table>
        <div id="divRecentMatlsBody" style="height:375px; overflow:auto; background-color:White;">

                <table id="tblRecentMatls" class="tabledataformat tableloose" style="background-color:White;">
    
                <tr><td>
                <p />
                <a href="/membership/login.aspx">Login</a> to see your most recently viewed materials here.<p />
                Or if you don't have an account with us yet, then <a href="/membership/regstart.aspx">click here to register.</a>
                </td></tr>
                
                </table>

        </div>
</div>

<script type="text/javascript">

        var RecentMatls = new _RecentMatls();
        var tmrtmp;

        function _RecentMatls(){

                var divRecentMatls;
                var tblRecentMatls;
                var trRecentMatlsRowTemplate;
                var trRecentMatlsNoData;
                var DataIsLoaded=false;

                //Using prototype.js methods...
                divRecentMatls = $("divRecentMatls");
                tblRecentMatls = $("tblRecentMatls");
    

                divRecentMatls.style.display = "none";

                this.Toggle = function(){
                        //alert("Toggle()");
                        if(divRecentMatls.style.display == "block"){this.Hide();}
                        else{this.Show();}
                }

                this.Show = function(){
                        //alert("Show()");
                        divRecentMatls.style.display = "block";
                        matweb.toggleSelect(divRecentMatls,0);
                        if(!DataIsLoaded){
            
                        }
                }

                this.Hide = function(){
                        //alert("Hide()");
                        divRecentMatls.style.display = "none";
                        matweb.toggleSelect(divRecentMatls,1);
                }

    

        }//end class

</script>

<table class="tabletight t_ableborder t_ablegrid" style="width:100%;" >
  <tr>
   <td style="width:100%; height:27px; white-space:nowrap; text-align:left; vertical-align:middle;" class="tagline" >
      <span class="navlink"><b>&nbsp;&nbsp;Searches:</b></span>
      &nbsp;&nbsp;<a href="/search/AdvancedSearch.aspx" class="navlink"><span class="nav"><font color="#CC0000">Advanced</font></span></a>
      &nbsp;|&nbsp;<a href="/search/MaterialGroupSearch.aspx" class="navlink"><span class="nav">Category</span></a>
      &nbsp;|&nbsp;<a href="/search/PropertySearch.aspx" class="navlink"><span class="nav">Property</span></a>
      &nbsp;|&nbsp;<a href="/search/CompositionSearch.aspx" class="navlink"><span class="nav">Metals</span></a>
      &nbsp;|&nbsp;<a href="/search/SearchTradeName.aspx" class="navlink"><span class="nav">Trade Name</span></a>
      &nbsp;|&nbsp;<a href="/search/SearchManufacturerName.aspx" class="navlink"><span class="nav">Manufacturer</span></a>
                &nbsp;|&nbsp;<a href="javascript:RecentMatls.Toggle();" id="ctl00_lnkRecentMatls" class="navlink"><span class="nav"><font color="#CC0000">Recently Viewed Materials</font></span></a>
    </td>

    <td style="text-align:right; vertical-align:bottom; padding-right:5px;">

      <table class="tabletight t_ableborder" ><tr>
        <td style=" white-space:nowrap; vertical-align:bottom; " >
                                        &nbsp;&nbsp;
                                        <input name="ctl00$txtQuickText" type="text" id="ctl00_txtQuickText" style="width:100px;" maxlength="40" class="quicksearchinput" onkeydown="txtQuickText_OnKeyDown(event);" />
                                        <input type="image" id="btnQuickTextSearch" style="vertical-align:top; width:60px; height:25px;" src="/images/buttons/btnSearch.gif" alt="Search" onclick="return btnQuickTextSearch_Click()"  />
                                        <script type="text/javascript">

                                                //SETUP THE AUTOPOST ON ENTER KEY EVENT FOR txtQuickText FIELD
                                                //document.getElementById("ctl00_txtQuickText").onkeydown=txtQuickText_OnKeyDown;

                                                function txtQuickText_OnKeyDown(evt){
                                                        //SUBMIT THE FORM AS IF THE BUTTON WAS PUSHED
                                                        //var UniqueID = "";
                                                        var txtQuickText = document.getElementById('ctl00_txtQuickText');
                                                        if(matweb.IsEnterKey(evt))
                                                                if(checkSearchText(txtQuickText))
                                                                        //matweb.DoPostBack(UniqueID, "");
                                                                        window.location.href="/search/QuickText.aspx?SearchText=" + escape(txtQuickText.value);
                                                        return false;
                                                }

                                                function btnQuickTextSearch_Click(){
                                                        var txtQuickText = document.getElementById('ctl00_txtQuickText');
                                                        if(checkSearchText(txtQuickText))
                                                                window.location.href="/search/QuickText.aspx?SearchText=" + escape(txtQuickText.value);
                                                        return false;
                                                }

                                                function checkSearchText(searchbox){
                                                        searchbox.value = trim(searchbox.value);
                                                        if(searchbox.value == ''){
                                                                //alert('Please enter something to search for');
                                                                matweb.alert('Please enter something to search for', 'Search Error');
                                                                //searchbox.focus();
                                                                return false;
                                                        }
                                                        return true;
                                                }

                                                function trim(s)  { return ltrim(rtrim(s)) }
                                                function ltrim(s) { return s.replace(/^\s+/g, "") }
                                                function rtrim(s) { return s.replace(/\s+$/g, "") }

                                        </script>
                                </td>
                                </tr>
                        </table>

    </td>
  </tr>
  <tr style="background-image:url(/images/shad.gif);height:7px;" ><td colspan="2"></td></tr>
</table>

<!-- ====================================== MAIN CONTENT ============================================= -->
<div style="vertical-align:top; padding-left:10px; padding-right:10px;" >





	<center><a href="/clickthrough.aspx?addataid=1349"><img src="/images/assets/metalmen-Aluminum.png" border=0 alt="Metalmen Sales" width = "728" height = "90"></a>
</center>
	
	<h2><a href="#help"><img src="/images/buttons/iconHelp.gif" alt="" style="vertical-align:bottom;" /></a>Material Category Search</h2>
	
	

<div id="ctl00_ContentMain_ucPopupMessage1_divPopupMessage" class="divPopupAlert" style="display:none;width:250px;">
	<table >
		<tr id="ctl00_ContentMain_ucPopupMessage1_trTitleRow" class="divPopupAlert_TitleRow" style="cursor:move;">
	<th><span id="ctl00_ContentMain_ucPopupMessage1_lblTitle">User Input Problem</span></th>
</tr>

		<tr class="divPopupAlert_ContentRow" ><td style="" ><span id="ctl00_ContentMain_ucPopupMessage1_lblMessage"></span></td></tr>
		<tr class="divPopupAlert_ButtonRow"><td><input name="ctl00$ContentMain$ucPopupMessage1$btnOK" type="button" id="ctl00_ContentMain_ucPopupMessage1_btnOK" value="OK" /></td></tr>
	</table>
</div>

<input type="hidden" name="ctl00$ContentMain$ucPopupMessage1$hndPopupControl" id="ctl00_ContentMain_ucPopupMessage1_hndPopupControl" />


	<table class="" style="width:100%;">
	<tr>
		<td style=" vertical-align:top; width:300px;" >
			<strong>Find a Material Category:</strong>
			

<input name="ctl00$ContentMain$UcMatGroupFinder1$txtSearchText" type="text" value="alum" id="ctl00_ContentMain_UcMatGroupFinder1_txtSearchText" style="Width:304px;" /><br />
<select name="ctl00$ContentMain$UcMatGroupFinder1$selectCategoryList" id="ctl00_ContentMain_UcMatGroupFinder1_selectCategoryList" style="position:absolute;display:none;Width:308px;" size="18">
</select>

<input type="hidden" name="ctl00$ContentMain$UcMatGroupFinder1$TextBoxWatermarkExtender2_ClientState" id="ctl00_ContentMain_UcMatGroupFinder1_TextBoxWatermarkExtender2_ClientState" />

<script type="text/javascript">

var ucMatGroupFinderManager = new _ucMatGroupFinderManager("ctl00_ContentMain_UcMatGroupFinder1_txtSearchText","ctl00_ContentMain_UcMatGroupFinder1_selectCategoryList");

function _ucMatGroupFinderManager(txtSearchTextID,selectCategoryListID){

	var txtSearchText = document.getElementById(txtSearchTextID);
	var selectCategoryList = document.getElementById(selectCategoryListID);
	var showtime;
	var blurtime;
	var MinTextLength = 4;
	var KeyStrokeActionDelay = 650;
	var FadeInOutDelay = 0.2;
	var LastUsedText;
	
	//The init function is currently run at the bottom of this class
	this.init=function(){

		txtSearchText.onfocus = txtSearchText_onfocus;
		txtSearchText.onkeyup = txtSearchText_checktext;
		txtSearchText.onblur = selectCategoryList_startblur;
		
		selectCategoryList.onfocus = selectCategoryList_clearblur;
		selectCategoryList.onchange = selectCategoryList_onchange;
		selectCategoryList.onblur = selectCategoryList_startblur;

	}

	txtSearchText_onfocus = function(){
		if(txtSearchText.value == "Type at least 4 characters here...")
			txtSearchText.value = "";
		selectCategoryList_clearblur();
		txtSearchText_checktext();
	}

	function txtSearchText_checktext(){
		searchText = txtSearchText.value;
		//alert(searchText);
		clearTimeout(showtime);
		if(searchText.length >= MinTextLength){
			showtime = setTimeout(function(){
				selectCategoryList.style.display = "block";
				if(txtSearchText.value!=LastUsedText){
					LastUsedText = searchText;
					aci.matweb.WebServices.MatGroups.FindMatGroups(searchText,DisplayCatData,matweb.WebServiceErrorHandler,"txtSearchText_checktext");
				}
				//AjaxControlToolkit.Animation.FadeInAnimation.play(selectCategoryList, FadeInOutDelay, 1, 25, 0, 1, true);
			},KeyStrokeActionDelay);
		}
		else{
			LastUsedText = "";
			showtime = setTimeout(function(){
				selectCategoryList.options.length = 0;
				selectCategoryList.style.display = "none";
				//AjaxControlToolkit.Animation.FadeOutAnimation.play(selectCategoryList,FadeInOutDelay);
				//AjaxControlToolkit.Animation.FadeOutAnimation.play(selectCategoryList,FadeInOutDelay, 25, 0, 1, true);
			},KeyStrokeActionDelay);
		}
	}

	function selectCategoryList_clearblur(){
		clearTimeout(blurtime);
	}

	function selectCategoryList_onchange(){
		var MatGroupID = selectCategoryList.options[selectCategoryList.selectedIndex].value;
		var MatGroupText = selectCategoryList.options[selectCategoryList.selectedIndex].text;
		//alert("Selected MatGroupID: " + MatGroupID);
		//alert("Selected MatGroupText: " + MatGroupText);
		//When no data is found, we have a "No Categories Contain Text..." message, but the MatGroupID is set to 0, so we can ignore if MatGroupID == 0.
		if(MatGroupID > 0){
			ucMatGroupFinder1_OnSelected(MatGroupID,MatGroupText)
		}
		selectCategoryList_startblur();
		//txtSearchText.value = "";
	}
	
	function selectCategoryList_startblur(){
		//alert("selectCategoryList_onblur()");
		clearTimeout(blurtime);
		blurtime = setTimeout(function(){
			selectCategoryList.style.display = "none";
			//AjaxControlToolkit.Animation.FadeOutAnimation.play(selectCategoryList,FadeInOutDelay, 25, 0, 1, true);
		},KeyStrokeActionDelay);
	}
	
	function DisplayCatData(MatGroupData){
		//alert("GetCatData()");
		//alert("vwMaterialGroupList: " + vwMaterialGroupList);
		var MatGroupID,GroupName, MatCount;
		selectCategoryList.options.length = 0;//clear any existing options....
		if(MatGroupData.length==0){
				//alert(vwMaterialGroupList[i].GroupName);
				MatGroupID = 0;
				GroupName = "No categories contain the text: " + txtSearchText.value
				selectCategoryList.options[0] = new Option(GroupName,MatGroupID);					
		}
		else{
			for(i=0;i<MatGroupData.length;i++){
				//alert(vwMaterialGroupList[i].GroupName);
				MatCount = MatGroupData[i].MatCount;
				MatGroupID = MatGroupData[i].MatGroupID;
				GroupName = MatGroupData[i].GroupName + " (" + MatCount + " matls)";
				selectCategoryList.options[i] = new Option(GroupName,MatGroupID);					
			}
		}
	}
	
	this.init();

}//end class
	
</script>



			<strong>Select a Material Category:</strong>
			
			<div class="tableborder" style=" height:225px; width:300px; overflow:scroll; ">
			<a href="#ctl00_ContentMain_ucMatGroupTree_msTreeView_SkipLink"><img alt="Skip Navigation Links." src="/WebResource.axd?d=vAkieGj3ugBpu6CKp7dqvCP5iHLzPbUd2XME-h1vQcC6d5hLg7Vc1kNVzB8A45Ui3K9XZ9m3-E2pxsaJQYh2uMkAul41&amp;t=638313619312541215" width="0" height="0" style="border-width:0px;" /></a><div id="ctl00_ContentMain_ucMatGroupTree_msTreeView">
	<table cellpadding="0" cellspacing="0" style="border-width:0;">
		<tr>
			<td><a id="ctl00_ContentMain_ucMatGroupTree_msTreeViewn0" href="javascript:TreeView_PopulateNode(ctl00_ContentMain_ucMatGroupTree_msTreeView_Data,0,document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewn0'),document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewt0'),null,'r','Carbon (866 matls)','283','f','','f')"><img src="/WebResource.axd?d=6v5qM6VKQYD3XNwwXEy7Tix7dzVdqDJ8QTRIlUPrzXQqNsXQgUwIfmn34ZzRORd_IZj0bF4vkZzID1HuSFE5uKyLgaPYeAYE8VT8HYfb3Rk6zsDw0&amp;t=638313619312541215" alt="Expand Carbon (866 matls)" style="border-width:0;" /></a></td><td class="ctl00_ContentMain_ucMatGroupTree_msTreeView_2"><a class="ctl00_ContentMain_ucMatGroupTree_msTreeView_0 ctl00_ContentMain_ucMatGroupTree_msTreeView_1" href="javascript:ucMatGroupTree_OnNodeClick('Carbon (866 matls)','283')" id="ctl00_ContentMain_ucMatGroupTree_msTreeViewt0">Carbon (866 matls)</a></td>
		</tr><tr style="height:0px;">
			<td></td>
		</tr>
	</table><table cellpadding="0" cellspacing="0" style="border-width:0;">
		<tr style="height:0px;">
			<td></td>
		</tr><tr>
			<td><a id="ctl00_ContentMain_ucMatGroupTree_msTreeViewn1" href="javascript:TreeView_PopulateNode(ctl00_ContentMain_ucMatGroupTree_msTreeView_Data,1,document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewn1'),document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewt1'),null,'t','Ceramic (10004 matls)','11','f','','f')"><img src="/WebResource.axd?d=TinC6igX1vPDgWh7DstWwBTHTyH4uuybDU4NW47jJD06wHdAO3YyJC_eKK1Zn5oIbwqRo4Cri8YwhA82G-LD0p14yEF1EfoIZvKleYMIuQ2K1hkE0&amp;t=638313619312541215" alt="Expand Ceramic (10004 matls)" style="border-width:0;" /></a></td><td class="ctl00_ContentMain_ucMatGroupTree_msTreeView_2"><a class="ctl00_ContentMain_ucMatGroupTree_msTreeView_0 ctl00_ContentMain_ucMatGroupTree_msTreeView_1" href="javascript:ucMatGroupTree_OnNodeClick('Ceramic (10004 matls)','11')" id="ctl00_ContentMain_ucMatGroupTree_msTreeViewt1">Ceramic (10004 matls)</a></td>
		</tr><tr style="height:0px;">
			<td></td>
		</tr>
	</table><table cellpadding="0" cellspacing="0" style="border-width:0;">
		<tr style="height:0px;">
			<td></td>
		</tr><tr>
			<td><a id="ctl00_ContentMain_ucMatGroupTree_msTreeViewn2" href="javascript:TreeView_PopulateNode(ctl00_ContentMain_ucMatGroupTree_msTreeView_Data,2,document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewn2'),document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewt2'),null,'t','Fluid (7562 matls)','5','f','','f')"><img src="/WebResource.axd?d=TinC6igX1vPDgWh7DstWwBTHTyH4uuybDU4NW47jJD06wHdAO3YyJC_eKK1Zn5oIbwqRo4Cri8YwhA82G-LD0p14yEF1EfoIZvKleYMIuQ2K1hkE0&amp;t=638313619312541215" alt="Expand Fluid (7562 matls)" style="border-width:0;" /></a></td><td class="ctl00_ContentMain_ucMatGroupTree_msTreeView_2"><a class="ctl00_ContentMain_ucMatGroupTree_msTreeView_0 ctl00_ContentMain_ucMatGroupTree_msTreeView_1" href="javascript:ucMatGroupTree_OnNodeClick('Fluid (7562 matls)','5')" id="ctl00_ContentMain_ucMatGroupTree_msTreeViewt2">Fluid (7562 matls)</a></td>
		</tr><tr style="height:0px;">
			<td></td>
		</tr>
	</table><table cellpadding="0" cellspacing="0" style="border-width:0;">
		<tr style="height:0px;">
			<td></td>
		</tr><tr>
			<td><a id="ctl00_ContentMain_ucMatGroupTree_msTreeViewn3" href="javascript:TreeView_PopulateNode(ctl00_ContentMain_ucMatGroupTree_msTreeView_Data,3,document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewn3'),document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewt3'),null,'t','Metal (17052 matls)','9','f','','f')"><img src="/WebResource.axd?d=TinC6igX1vPDgWh7DstWwBTHTyH4uuybDU4NW47jJD06wHdAO3YyJC_eKK1Zn5oIbwqRo4Cri8YwhA82G-LD0p14yEF1EfoIZvKleYMIuQ2K1hkE0&amp;t=638313619312541215" alt="Expand Metal (17052 matls)" style="border-width:0;" /></a></td><td class="ctl00_ContentMain_ucMatGroupTree_msTreeView_2"><a class="ctl00_ContentMain_ucMatGroupTree_msTreeView_0 ctl00_ContentMain_ucMatGroupTree_msTreeView_1" href="javascript:ucMatGroupTree_OnNodeClick('Metal (17052 matls)','9')" id="ctl00_ContentMain_ucMatGroupTree_msTreeViewt3">Metal (17052 matls)</a></td>
		</tr><tr style="height:0px;">
			<td></td>
		</tr>
	</table><table cellpadding="0" cellspacing="0" style="border-width:0;">
		<tr style="height:0px;">
			<td></td>
		</tr><tr>
			<td><a id="ctl00_ContentMain_ucMatGroupTree_msTreeViewn4" href="javascript:TreeView_PopulateNode(ctl00_ContentMain_ucMatGroupTree_msTreeView_Data,4,document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewn4'),document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewt4'),null,'t','Other Engineering Material (8063 matls)','285','f','','f')"><img src="/WebResource.axd?d=TinC6igX1vPDgWh7DstWwBTHTyH4uuybDU4NW47jJD06wHdAO3YyJC_eKK1Zn5oIbwqRo4Cri8YwhA82G-LD0p14yEF1EfoIZvKleYMIuQ2K1hkE0&amp;t=638313619312541215" alt="Expand Other Engineering Material (8063 matls)" style="border-width:0;" /></a></td><td class="ctl00_ContentMain_ucMatGroupTree_msTreeView_2"><a class="ctl00_ContentMain_ucMatGroupTree_msTreeView_0 ctl00_ContentMain_ucMatGroupTree_msTreeView_1" href="javascript:ucMatGroupTree_OnNodeClick('Other Engineering Material (8063 matls)','285')" id="ctl00_ContentMain_ucMatGroupTree_msTreeViewt4">Other Engineering Material (8063 matls)</a></td>
		</tr><tr style="height:0px;">
			<td></td>
		</tr>
	</table><table cellpadding="0" cellspacing="0" style="border-width:0;">
		<tr style="height:0px;">
			<td></td>
		</tr><tr>
			<td><a id="ctl00_ContentMain_ucMatGroupTree_msTreeViewn5" href="javascript:TreeView_PopulateNode(ctl00_ContentMain_ucMatGroupTree_msTreeView_Data,5,document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewn5'),document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewt5'),null,'t','Polymer (97635 matls)','10','f','','f')"><img src="/WebResource.axd?d=TinC6igX1vPDgWh7DstWwBTHTyH4uuybDU4NW47jJD06wHdAO3YyJC_eKK1Zn5oIbwqRo4Cri8YwhA82G-LD0p14yEF1EfoIZvKleYMIuQ2K1hkE0&amp;t=638313619312541215" alt="Expand Polymer (97635 matls)" style="border-width:0;" /></a></td><td class="ctl00_ContentMain_ucMatGroupTree_msTreeView_2"><a class="ctl00_ContentMain_ucMatGroupTree_msTreeView_0 ctl00_ContentMain_ucMatGroupTree_msTreeView_1" href="javascript:ucMatGroupTree_OnNodeClick('Polymer (97635 matls)','10')" id="ctl00_ContentMain_ucMatGroupTree_msTreeViewt5">Polymer (97635 matls)</a></td>
		</tr><tr style="height:0px;">
			<td></td>
		</tr>
	</table><table cellpadding="0" cellspacing="0" style="border-width:0;">
		<tr style="height:0px;">
			<td></td>
		</tr><tr>
			<td><img src="/WebResource.axd?d=7_V5n1KYSQsj6PoSqgIxb0egBsAa91jSXPkbmM6FaiF14-JsNnpUDnfSdxZcvfd0OQCLDtC24pMIXigO-41cL2rsq-0xea-SSG7yZUyRcliFFqe70&amp;t=638313619312541215" alt="" /></td><td class="ctl00_ContentMain_ucMatGroupTree_msTreeView_2"><a class="ctl00_ContentMain_ucMatGroupTree_msTreeView_0 ctl00_ContentMain_ucMatGroupTree_msTreeView_1" href="javascript:ucMatGroupTree_OnNodeClick('Pure Element (507 matls)','184')" id="ctl00_ContentMain_ucMatGroupTree_msTreeViewt6">Pure Element (507 matls)</a></td>
		</tr><tr style="height:0px;">
			<td></td>
		</tr>
	</table><table cellpadding="0" cellspacing="0" style="border-width:0;">
		<tr style="height:0px;">
			<td></td>
		</tr><tr>
			<td><a id="ctl00_ContentMain_ucMatGroupTree_msTreeViewn7" href="javascript:TreeView_PopulateNode(ctl00_ContentMain_ucMatGroupTree_msTreeView_Data,7,document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewn7'),document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewt7'),null,'l','Wood and Natural Products (398 matls)','277','f','','t')"><img src="/WebResource.axd?d=vUBrM2nNhECNbFO0AJKq52p2zWrY5XG8nWgCDL4AfsrexStb0HREDV6GHMvdkAGDInxYtTyWXuxFUIiQv3JX2fScVRwQsrVv5srHrjOIeC3ayAG20&amp;t=638313619312541215" alt="Expand Wood and Natural Products (398 matls)" style="border-width:0;" /></a></td><td class="ctl00_ContentMain_ucMatGroupTree_msTreeView_2"><a class="ctl00_ContentMain_ucMatGroupTree_msTreeView_0 ctl00_ContentMain_ucMatGroupTree_msTreeView_1" href="javascript:ucMatGroupTree_OnNodeClick('Wood and Natural Products (398 matls)','277')" id="ctl00_ContentMain_ucMatGroupTree_msTreeViewt7">Wood and Natural Products (398 matls)</a></td>
		</tr><tr style="height:0px;">
			<td></td>
		</tr>
	</table>
</div><a id="ctl00_ContentMain_ucMatGroupTree_msTreeView_SkipLink"></a>
<span id="ctl00_ContentMain_ucMatGroupTree_lblDebug"></span>

			</div>
			
			<strong>Selected Material Category: </strong>
			<div id="ctl00_ContentMain_divMatGroupName" class="selectedItem" style="width:306px;">5000 Series Aluminum Alloy (299 matls)</div>
			<input name="ctl00$ContentMain$txtMatGroupID" type="hidden" id="ctl00_ContentMain_txtMatGroupID" value="205" />
			<input name="ctl00$ContentMain$txtMatGroupText" type="hidden" id="ctl00_ContentMain_txtMatGroupText" value="5000 Series Aluminum Alloy (299 matls)" />
			
			

			<input type="image" name="ctl00$ContentMain$btnSubmit" id="ctl00_ContentMain_btnSubmit" src="../images/buttons/btnFind.gif" alt="Find" style="border-width:0px;" /> <!-- OnClientClick="return validate()" -->
			<input type="image" name="ctl00$ContentMain$btnReset" id="ctl00_ContentMain_btnReset" title="Clear Search" src="/images/buttons/btnReset.gif" alt="Clear Search" style="border-width:0px;" />

			<span id="ctl00_ContentMain_lblDebug"></span>

		</td>
		<td>&nbsp;</td>
		<td style=" vertical-align:top; width:80%;" >
			<strong>Search Results</strong>
			<div class="tableborder" id="divSearchResults" style="padding:5px;" >
				
				<div id="ctl00_ContentMain_pnlSearchResults">
	
					There are <span id="ctl00_ContentMain_lblMatlCount">299</span> materials in the category 
					<span id="ctl00_ContentMain_lblMaterialGroupName" class="selectedItem">5000 Series Aluminum Alloy</span>.
					If your material is not listed, please refer to our <a href="/help/strategy.aspx">search strategy</a> page for assistance in limiting your search.
				
</div>
			</div>
			<p />




<div id="ctl00_ContentMain_UcSearchResults1_pnlResultsFound" class="tableborder">
	

	<table style="background-color:Silver; width:100%;" class="" >
		<tr>
			<td><strong>Found <span id="ctl00_ContentMain_UcSearchResults1_lblResultCount">299</span> Results</strong> -- 
				Page 
				<select name="ctl00$ContentMain$UcSearchResults1$drpPageSelect1" onchange="javascript:setTimeout('__doPostBack(\'ctl00$ContentMain$UcSearchResults1$drpPageSelect1\',\'\')', 0)" id="ctl00_ContentMain_UcSearchResults1_drpPageSelect1">
		<option selected="selected" value="1">1</option>
		<option value="2">2</option>

	</select>
				of <span id="ctl00_ContentMain_UcSearchResults1_lblPageTotal">2</span> 
				-- 
				<a id="ctl00_ContentMain_UcSearchResults1_lnkPrevPage" disabled="disabled" style="color:Gray;">[Prev Page]</a>
				<a id="ctl00_ContentMain_UcSearchResults1_lnkNextPage" href="javascript:__doPostBack('ctl00$ContentMain$UcSearchResults1$lnkNextPage','')" style="color:Black;">[Next Page]</a>
				--&nbsp;			
				view
				<select name="ctl00$ContentMain$UcSearchResults1$drpPageSize1" onchange="javascript:setTimeout('__doPostBack(\'ctl00$ContentMain$UcSearchResults1$drpPageSize1\',\'\')', 0)" id="ctl00_ContentMain_UcSearchResults1_drpPageSize1">
		<option value="25">25</option>
		<option value="50">50</option>
		<option value="75">75</option>
		<option value="100">100</option>
		<option value="150">150</option>
		<option selected="selected" value="200">200</option>

	</select> per page
			</td>
		</tr>
		
	</table>
	
	
	
	
	<table class="t_ablegrid" style="width:100%;">
		<tr style="font-size:9px;">
			<td style="font-size:9px;">Use Folder</td>
			<td style="font-size:9px;">Contains</td>
			<td style="font-size:9px;">&nbsp;</td>
			
			<td style="font-size:9px;"><div id="divFolderName" style="display:none;">New Folder Name</div></td>
			<td style="font-size:9px;"></td>
			<td style="width:90%"></td>
		</tr>
		<tr style="vertical-align:top;">
			<td style="vertical-align:top;">
				<select name="ctl00$ContentMain$UcSearchResults1$drpFolderList" id="ctl00_ContentMain_UcSearchResults1_drpFolderList" onchange="SetSelectedFolder()" style="width:125px;">
		<option selected="selected" value="0">My Folder</option>

	</select>
			</td>
			<td style="vertical-align:top;">
				<strong><input name="ctl00$ContentMain$UcSearchResults1$txtFolderMatCount" type="text" id="ctl00_ContentMain_UcSearchResults1_txtFolderMatCount" readonly="readonly" style="border:none 0 white; background-color:White; color:Black; width: 35px; font-weight:bold;" value="0/0" /></strong>
			</td>
			<td>
				
				<input type="image" id="btnCompareFolders" src="/images/buttons/btnCompareMatls.gif" alt="Compare Checked Materials" onclick="CompareMaterials();return false;" />
			</td>
			
			<td style="white-space:nowrap;vertical-align:top;">
			
				<table id="tblFolderName" class="tabletight" style="display:none;"><tr><td><input type="text" id="txtFolderName2" style="display:inline; width:100px;" /><br /></td>
				<td><input type="image" id="btnApplyFolderName2" src="/images/buttons/btnSaveChanges.gif" style="display:inline;" onclick="ApplyFolderName();return false;" />
				<input type="image" id="btnHideFolderNameControls2" src="/images/buttons/btnCancel.gif" style="display:inline;" onclick="HideFolderNameControls();return false;" /></td>
				</tr></table>
				
			</td>
			<td style="white-space:nowrap;">
				<div class="usermessage" id="divUserMessage"></div>
			</td>
		</tr>
	</table>

	<table class="tabledataformat t_ablegrid" id="tblResults" style="table-layout:auto;"  >

		<tr class="">

			
		
			<th style="width:65px; white-space:nowrap;">Select</th>

			<th style="width:10px;">
				<!--Discontinued and Found Via Interpolated Indicators -->
			</th>
			
			<th style="width:auto;">
				<a id="ctl00_ContentMain_UcSearchResults1_lnkSortMatName" disabled="disabled" title="Click to Sort By Material Name">Material Name</a>
				<br />
				
				
			</th>
		
			
			
			

			

			

			

			

			

			

			

			
		</tr>

		<!-- this is where the search results are actually loaded.  See code behind -->		
		<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_81686"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;1</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_81686' href="/search/DataSheet.aspx?MatGUID=c71186d128cd423d9c6d51106c015e8f" >Overview of materials for 5000 Series Aluminum Alloy</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9104"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;2</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9104' href="/search/DataSheet.aspx?MatGUID=49ab7c96ab6b4565ae689dcffbcfa84b" >Aluminum 5005A Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9105"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;3</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9105' href="/search/DataSheet.aspx?MatGUID=ebb465d6e89f453582a6939177fd484b" >Aluminum 5005-H12</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9106"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;4</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9106' href="/search/DataSheet.aspx?MatGUID=4d68415cc11a4003818339237a515a33" >Aluminum 5005-H14</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9107"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;5</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9107' href="/search/DataSheet.aspx?MatGUID=ce9777e76ee9462d97a3862af8e65b2e" >Aluminum 5005-H16</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9108"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;6</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9108' href="/search/DataSheet.aspx?MatGUID=7126fcd3958f4bbeaa665c11dbba2314" >Aluminum 5005-H18</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9109"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;7</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9109' href="/search/DataSheet.aspx?MatGUID=546c214df90e44f893cb30db56d52f55" >Aluminum 5005-H32</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9110"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;8</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9110' href="/search/DataSheet.aspx?MatGUID=5882aa95b96644559ec51020cdc1169e" >Aluminum 5005-H34</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9111"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;9</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9111' href="/search/DataSheet.aspx?MatGUID=d96798a8cc574ef2bbb637eb34ff52e1" >Aluminum 5005-H36</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9112"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;10</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9112' href="/search/DataSheet.aspx?MatGUID=3687f205bc05443db8ba25ac384e91dd" >Aluminum 5005-H38</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9113"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;11</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9113' href="/search/DataSheet.aspx?MatGUID=5481a70aa799485f85f404ea3d729254" >Aluminum 5005-O</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9115"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;12</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9115' href="/search/DataSheet.aspx?MatGUID=c56e1f07426a4272a241eca57110f2db" >5006 Aluminum Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9117"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;13</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9117' href="/search/DataSheet.aspx?MatGUID=7a3a650ecf464d90b24e7576be660ce6" >5010 Aluminum Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9118"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;14</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9118' href="/search/DataSheet.aspx?MatGUID=abed14f76c5049e4ada9a17e840f6c17" >5016 Aluminum Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9119"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;15</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9119' href="/search/DataSheet.aspx?MatGUID=9630e96de03f4c42803c387811f7a9ec" >5017 Aluminum Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9120"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;16</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9120' href="/search/DataSheet.aspx?MatGUID=98ac15362a0346e796c55a1b5b02f4dd" >5018 Aluminum Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9121"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;17</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9121' href="/search/DataSheet.aspx?MatGUID=6382217c07c64b1da1033151d3aa3470" >Aluminum 5018A Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9122"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;18</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9122' href="/search/DataSheet.aspx?MatGUID=d63da73db61b4dfc8ea4fc9ebf88046a" >5019 Aluminum Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9123"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;19</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9123' href="/search/DataSheet.aspx?MatGUID=24f6b85cbfdb4869bb306b2650ffd54a" >Aluminum 5019A Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9125"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;20</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9125' href="/search/DataSheet.aspx?MatGUID=9ff8ac16f74447e9b5367ed5b4105e57" >5021 Aluminum Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9127"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;21</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9127' href="/search/DataSheet.aspx?MatGUID=b80a24e760fd484e94a58f2490981258" >5022 Aluminum Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9128"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;22</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9128' href="/search/DataSheet.aspx?MatGUID=6d867df4f2ac43d0805d6101134f2191" >5023 Aluminum Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_160175"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;23</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_160175' href="/search/DataSheet.aspx?MatGUID=7eb00877bc834c889e62909003ca476b" >Aluminum 5024-H116 Al-Scandium Alloy</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9130"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;24</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9130' href="/search/DataSheet.aspx?MatGUID=77074c2f3397473aa831ca0183654711" >5025 Aluminum Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_160176"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;25</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_160176' href="/search/DataSheet.aspx?MatGUID=2503c4725a0e4c199c5d6d23467c6d59" >Aluminum 5026 Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_160177"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;26</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_160177' href="/search/DataSheet.aspx?MatGUID=375c021bf59b4c618cf2139716a00f6a" >Aluminum 5027 Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9139"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;27</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9139' href="/search/DataSheet.aspx?MatGUID=360c1c3bc7bd4efb8009c07e03c5e744" >5040 Aluminum Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_160179"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;28</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_160179' href="/search/DataSheet.aspx?MatGUID=f5200afc22d74a2191640e0be801f6ee" >Aluminum 5041 Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9140"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;29</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9140' href="/search/DataSheet.aspx?MatGUID=5f24b55f13934215ba86700c4a8d4683" >Aluminum 5042-H19</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9141"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;30</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9141' href="/search/DataSheet.aspx?MatGUID=1fc2e77fa77c433a957440946d61facd" >5043 Aluminum Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9142"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;31</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9142' href="/search/DataSheet.aspx?MatGUID=73513387595a449e9a04efc114e3c5b1" >5049 Aluminum Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9144"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;32</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9144' href="/search/DataSheet.aspx?MatGUID=54a0769f70294808bb1460a9ee2a5428" >Aluminum 5050A Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_160240"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;33</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_160240' href="/search/DataSheet.aspx?MatGUID=72a192c04ec64f1ca22169480c5d905e" >Aluminum 5050C Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9145"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;34</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9145' href="/search/DataSheet.aspx?MatGUID=264a79a330914c589bdeae866556cb3d" >Aluminum 5050-H32</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9146"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;35</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9146' href="/search/DataSheet.aspx?MatGUID=2740847db1974008b6895021d086d9ed" >Aluminum 5050-H34</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9147"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;36</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9147' href="/search/DataSheet.aspx?MatGUID=1b28e2a12abb49bdad863ee383c7d1c2" >Aluminum 5050-H36</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9148"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;37</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9148' href="/search/DataSheet.aspx?MatGUID=950d7a5f25764b3db3814422de8f03ea" >Aluminum 5050-H38</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9149"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;38</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9149' href="/search/DataSheet.aspx?MatGUID=7062da04ef2945499ac40218b3cdab63" >Aluminum 5050-O</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9151"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;39</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9151' href="/search/DataSheet.aspx?MatGUID=ee4377ffb3ad4c5ca201dbed2f713430" >Aluminum 5051A Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9152"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;40</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9152' href="/search/DataSheet.aspx?MatGUID=ca371085b4bf4334860a8076b8f5d933" >5051 Aluminum Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9154"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;41</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9154' href="/search/DataSheet.aspx?MatGUID=a400cb1757a440e98540468b1b2b24a2" >Aluminum 5052-H19 Foil</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9155"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;42</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9155' href="/search/DataSheet.aspx?MatGUID=96d768abc51e4157a1b8f95856c49028" >Aluminum 5052-H32</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9156"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;43</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9156' href="/search/DataSheet.aspx?MatGUID=8644ac88ccb647869449d6ff6ddfbaed" >Aluminum 5052-H34</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9157"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;44</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9157' href="/search/DataSheet.aspx?MatGUID=1a5729196f264cc78a3233bf558aee8a" >Aluminum 5052-H36</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9158"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;45</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9158' href="/search/DataSheet.aspx?MatGUID=03ed379e437945a1a8dbff86c5907f74" >Aluminum 5052-H38</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9160"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;46</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9160' href="/search/DataSheet.aspx?MatGUID=b3430ccca1334449b0d59cde9f977b57" >Aluminum 5052-O</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9165"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;47</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9165' href="/search/DataSheet.aspx?MatGUID=4643f621e29c4dd08ddb763bdf526fc9" >Aluminum 5056-H18</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9166"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;48</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9166' href="/search/DataSheet.aspx?MatGUID=fa551627b92d449eb02e7991ee2b6c46" >Aluminum 5056-H191 Foil</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9167"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;49</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9167' href="/search/DataSheet.aspx?MatGUID=3db066d23e65472e8c6beb042d38bae3" >Aluminum 5056-H38</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9168"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;50</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9168' href="/search/DataSheet.aspx?MatGUID=4001c91f1bce4477b5c31c6204f0239e" >Aluminum 5056-O</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9171"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;51</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9171' href="/search/DataSheet.aspx?MatGUID=982e6cb1b7ef45fc9b8361979ae8a21c" >5058 Aluminum Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9172"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;52</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9172' href="/search/DataSheet.aspx?MatGUID=bcad3833bda74053a773a611084cdbc5" >5059 Aluminum Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_160186"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;53</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_160186' href="/search/DataSheet.aspx?MatGUID=2e50f56fb3a14e03be5fceadd1f3a569" >Aluminum 5070 Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9179"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;54</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9179' href="/search/DataSheet.aspx?MatGUID=aa522442695c40afa313e0e93f59514d" >Aluminum 5082-H19</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9180"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;55</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9180' href="/search/DataSheet.aspx?MatGUID=bd6317b19dd94faf8bff851e4f339e88" >Aluminum 5083-H112</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9181"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;56</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9181' href="/search/DataSheet.aspx?MatGUID=1efe7441a72f4a22a53c0dc1bd9c87ec" >Aluminum 5083-H116; 5083-H321</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9182"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;57</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9182' href="/search/DataSheet.aspx?MatGUID=74b0ba636be24399bebb5e4554a62c6d" >Aluminum 5083-H32; 5083-H323</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9183"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;58</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9183' href="/search/DataSheet.aspx?MatGUID=27b37b2c1bb347b4a6d62bc0f6b8504f" >Aluminum 5083-H34; 5083-H343</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9184"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;59</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9184' href="/search/DataSheet.aspx?MatGUID=d105c2a24d6942cdad79259f770fb806" >Aluminum 5083-O</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9187"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;60</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9187' href="/search/DataSheet.aspx?MatGUID=072983cca84046ee92d5051fadd8c68a" >Aluminum 5086-H112</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9188"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;61</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9188' href="/search/DataSheet.aspx?MatGUID=548b85fced2548a7be36d02b34db686f" >Aluminum 5086-H116; 5086-H32</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9189"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;62</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9189' href="/search/DataSheet.aspx?MatGUID=b6c19b4f40c243ec92b640ef8d4f36c1" >Aluminum 5086-H34</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_109182"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;63</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_109182' href="/search/DataSheet.aspx?MatGUID=6b36557503df4ee6bcb2a0247ea21151" >Aluminum 5086-H36</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_109183"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;64</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_109183' href="/search/DataSheet.aspx?MatGUID=90f530286ceb47a1b8d17b9323468765" >Aluminum 5086-H38</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9190"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;65</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9190' href="/search/DataSheet.aspx?MatGUID=d0271cf3b5f84d63a17e328d02419587" >Aluminum 5086-O</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9191"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;66</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9191' href="/search/DataSheet.aspx?MatGUID=f3f6161d727b48c0867cd130e6f89ba0" >5087 Aluminum Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_160191"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;67</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_160191' href="/search/DataSheet.aspx?MatGUID=806ee72480de4091b3b281c99eb26159" >Aluminum 5088 Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_160172"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;68</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_160172' href="/search/DataSheet.aspx?MatGUID=d5c263b75bc74cfbad5bb41e1b407fb7" >Aluminum 5106 Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9193"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;69</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9193' href="/search/DataSheet.aspx?MatGUID=523a82561e4043c89d066a8f193555fb" >5110 Aluminum Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_160173"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;70</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_160173' href="/search/DataSheet.aspx?MatGUID=b2bb51e088a7451b8c298b4f902fac5d" >Aluminum 5110A Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9194"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;71</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9194' href="/search/DataSheet.aspx?MatGUID=31a0334a07d5423e87115cfa96641785" >5119 Aluminum Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_160174"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;72</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_160174' href="/search/DataSheet.aspx?MatGUID=5cfeec79c7614a9281ce6f4a7efe6629" >Aluminum 5119A Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_160178"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;73</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_160178' href="/search/DataSheet.aspx?MatGUID=7c17daadfd6842eb8415f58fa4ac2c7e" >Aluminum 5140 Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9205"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;74</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9205' href="/search/DataSheet.aspx?MatGUID=510dc259eb614382a70f189c4594519f" >5149 Aluminum Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9206"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;75</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9206' href="/search/DataSheet.aspx?MatGUID=3cd57600ddf64e488e56ffac556084f8" >5150 Aluminum Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9207"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;76</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9207' href="/search/DataSheet.aspx?MatGUID=fcf1890f8e034b28a6c1e858d12c0df5" >5151 Aluminum Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9208"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;77</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9208' href="/search/DataSheet.aspx?MatGUID=e45a3a8e5323457a8495c816de9d2e1c" >Aluminum 5154A Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9209"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;78</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9209' href="/search/DataSheet.aspx?MatGUID=f22bbdd749b0490abd3a57d32a8f0844" >Aluminum 5154B Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_160180"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;79</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_160180' href="/search/DataSheet.aspx?MatGUID=15a7c8607b8c4efabac09a6b13239635" >Aluminum 5154C Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9210"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;80</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9210' href="/search/DataSheet.aspx?MatGUID=b825139a662b4bb392b089b4e9f36076" >Aluminum 5154-H112</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9211"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;81</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9211' href="/search/DataSheet.aspx?MatGUID=648345b051774f0ea64387b5a039ce23" >Aluminum 5154-H32</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9212"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;82</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9212' href="/search/DataSheet.aspx?MatGUID=315773ca816d4e7e8de37ceb5809b3b3" >Aluminum 5154-H34</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9213"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;83</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9213' href="/search/DataSheet.aspx?MatGUID=d1902cb08ca64f898fada08d75f7318c" >Aluminum 5154-H36</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9214"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;84</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9214' href="/search/DataSheet.aspx?MatGUID=94159edded56476eb1d871f27330855b" >Aluminum 5154-H38</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9215"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;85</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9215' href="/search/DataSheet.aspx?MatGUID=4232f533344d4cd0bd96eb07ae0bca02" >Aluminum 5154-O</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9216"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;86</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9216' href="/search/DataSheet.aspx?MatGUID=e0b5870bafb045c6bc18fb051f096d86" >5180 Aluminum Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_160187"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;87</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_160187' href="/search/DataSheet.aspx?MatGUID=cf03001381a140d394be8d0044941437" >Aluminum 5180A Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9217"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;88</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9217' href="/search/DataSheet.aspx?MatGUID=9ef482b0015a483194bcaf3d53e2bf52" >Aluminum 5182-H19</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9218"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;89</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9218' href="/search/DataSheet.aspx?MatGUID=430cd3b05cf44ab08a056da93a90bc40" >Aluminum 5182-H32</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9219"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;90</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9219' href="/search/DataSheet.aspx?MatGUID=8fd54d9fa33b4a61a24c88bbaedc3ecd" >Aluminum 5182-H34</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9220"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;91</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9220' href="/search/DataSheet.aspx?MatGUID=79cd55e17b2648409398393c4e47bb45" >Aluminum 5182-O</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9221"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;92</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9221' href="/search/DataSheet.aspx?MatGUID=de84f97be3984b3fbd30c2dac23f60a9" >5183 Aluminum Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_160188"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;93</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_160188' href="/search/DataSheet.aspx?MatGUID=144cffddd8a5435094f9d5f703573a2b" >Aluminum 5183A Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9222"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;94</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9222' href="/search/DataSheet.aspx?MatGUID=f259e508954a4d69bf74eca9449ad1b2" >5186 Aluminum Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_160190"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;95</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_160190' href="/search/DataSheet.aspx?MatGUID=03309f6d01114052b8a7cd0c0f3f4d9e" >Aluminum 5187 Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9223"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;96</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9223' href="/search/DataSheet.aspx?MatGUID=a5567530e17e4d1a967017728b723381" >5205 Aluminum Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9224"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;97</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9224' href="/search/DataSheet.aspx?MatGUID=25351dcaf66f4c87b97ddb7316738200" >5210 Aluminum Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9234"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;98</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9234' href="/search/DataSheet.aspx?MatGUID=d52e84a971a9407aad889994ef6f9c2f" >5249 Aluminum Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9235"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;99</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9235' href="/search/DataSheet.aspx?MatGUID=e6bd17564b0a437e89645a1877e1f81f" >5250 Aluminum Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9236"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;100</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9236' href="/search/DataSheet.aspx?MatGUID=16bb703f31d6429a95216564dbf857d5" >5251-O Aluminum</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9237"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;101</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9237' href="/search/DataSheet.aspx?MatGUID=dbd6d7483df7481bbfa96475bc9fd1d8" >Aluminum 5251A Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_130357"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;102</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_130357' href="/search/DataSheet.aspx?MatGUID=367cd6f932584eeca5a3ecef210f05a7" >5251-H22 Aluminum</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_130358"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;103</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_130358' href="/search/DataSheet.aspx?MatGUID=3026713d933c46e981f358bb8d72fe30" >5251-H24 Aluminum</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_130359"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;104</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_130359' href="/search/DataSheet.aspx?MatGUID=56e59ab224ac45ea81a74227baf86ec2" >5251-H26 Aluminum</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9238"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;105</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9238' href="/search/DataSheet.aspx?MatGUID=b4ffa4198e0441fe91664125908a2df8" >Aluminum 5252-H25; 5252-H38</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9239"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;106</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9239' href="/search/DataSheet.aspx?MatGUID=7546fc0c39b1400690d24b792a2976c9" >Aluminum 5252-H28</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9240"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;107</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9240' href="/search/DataSheet.aspx?MatGUID=6918b1249eb3498d9354c355765bb734" >Aluminum 5252-H38</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9241"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;108</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9241' href="/search/DataSheet.aspx?MatGUID=fe9372f9e0b94f308381b46008152624" >Aluminum 5252-O</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9242"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;109</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9242' href="/search/DataSheet.aspx?MatGUID=8326e8e1fda74b41ba0375e3010b463e" >Aluminum 5254-H112</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9243"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;110</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9243' href="/search/DataSheet.aspx?MatGUID=1d272812877349108b04ad03ec4b545d" >Aluminum 5254-H32</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9244"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;111</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9244' href="/search/DataSheet.aspx?MatGUID=8e59d8c0246f4dbeb48094d06bf72efa" >Aluminum 5254-H34</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9245"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;112</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9245' href="/search/DataSheet.aspx?MatGUID=227da91f8516421aad33a1fce880bf36" >Aluminum 5254-H36</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9246"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;113</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9246' href="/search/DataSheet.aspx?MatGUID=e83f9a8a6c404c5597f6f5d35ae2dde7" >Aluminum 5254-H38</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9247"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;114</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9247' href="/search/DataSheet.aspx?MatGUID=601ec151007b4f9299df93c5cb3cd84f" >Aluminum 5254-O</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9248"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;115</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9248' href="/search/DataSheet.aspx?MatGUID=ba8e580efd1047149df65fc91881a843" >5257 Aluminum Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9249"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;116</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9249' href="/search/DataSheet.aspx?MatGUID=dcb99a4de7bd48e7bdcdd889d6a6e129" >5283 Aluminum Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9250"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;117</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9250' href="/search/DataSheet.aspx?MatGUID=4353b734447c49edbc9e55ec1b91fa20" >Aluminum 5283A Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9251"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;118</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9251' href="/search/DataSheet.aspx?MatGUID=ffc39df7600c4e249f1bb7896758a57c" >Aluminum 5283B Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9252"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;119</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9252' href="/search/DataSheet.aspx?MatGUID=0f49dcc662b04896aa75aab7e8e04d97" >5305 Aluminum Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9253"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;120</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9253' href="/search/DataSheet.aspx?MatGUID=a26edb12253e420ca78fc1ddebb9eeae" >5310 Aluminum Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9254"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;121</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9254' href="/search/DataSheet.aspx?MatGUID=b5271305d5de46fc8ae194b4fe500fd2" >5349 Aluminum Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9255"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;122</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9255' href="/search/DataSheet.aspx?MatGUID=7e63a2c024654688818417bb02d50cab" >5351 Aluminum Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9256"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;123</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9256' href="/search/DataSheet.aspx?MatGUID=adb089ff23294881b706f6169bbdfb26" >5352 Aluminum Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9257"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;124</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9257' href="/search/DataSheet.aspx?MatGUID=53c953a604a946ae9338933d44f3ec75" >5354 Aluminum Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_160182"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;125</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_160182' href="/search/DataSheet.aspx?MatGUID=f36d358df225441fbb4ce27e25fd3fe0" >Aluminum 5356A Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9259"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;126</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9259' href="/search/DataSheet.aspx?MatGUID=61e953ece76c4f4a86dd28ff540f41fa" >Aluminum 5356</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9260"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;127</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9260' href="/search/DataSheet.aspx?MatGUID=1660cd09da6f4f4d865c987a4a621ea8" >Aluminum 5356-O</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9261"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;128</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9261' href="/search/DataSheet.aspx?MatGUID=1afc0c534acc49e18b06caeb43accf88" >5357 Aluminum Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9263"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;129</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9263' href="/search/DataSheet.aspx?MatGUID=e1fa0d6ecd6c48d689e79a04ac552e63" >Aluminum 5383-H321/H116</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9274"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;130</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9274' href="/search/DataSheet.aspx?MatGUID=c3deea90b6f84a3395c3dd5eeb2207d2" >5449 Aluminum Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_160251"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;131</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_160251' href="/search/DataSheet.aspx?MatGUID=2740db794e8947e59a102f973aeaedda" >Aluminum 5449A Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9275"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;132</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9275' href="/search/DataSheet.aspx?MatGUID=43202a96e3a04f97a2383287e41f5cb8" >5451 Aluminum Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9276"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;133</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9276' href="/search/DataSheet.aspx?MatGUID=29db1ed686a74e798e24c99a44aee7b4" >Aluminum 5454-H111; 5454-H311</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9277"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;134</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9277' href="/search/DataSheet.aspx?MatGUID=865b278a047447699023b1ca7385600e" >Aluminum 5454-H112</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9278"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;135</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9278' href="/search/DataSheet.aspx?MatGUID=56457d9be1864222ba076bef5fd4b3b3" >Aluminum 5454-H32</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9279"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;136</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9279' href="/search/DataSheet.aspx?MatGUID=ed6e9ae1a2cf4588847bc7163434c983" >Aluminum 5454-H34</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9280"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;137</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9280' href="/search/DataSheet.aspx?MatGUID=1c54fad97c2a46b8b5ee5a4a98c6168f" >Aluminum 5454-H38</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9281"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;138</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9281' href="/search/DataSheet.aspx?MatGUID=be299dc720e8427fb3b3d151c80fc0c9" >Aluminum 5454-O</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9282"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;139</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9282' href="/search/DataSheet.aspx?MatGUID=c037186d31264f87be98030c09cbe46e" >Aluminum 5456A Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_160183"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;140</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_160183' href="/search/DataSheet.aspx?MatGUID=d8fec3513a3e4c669789c787f59ac86b" >Aluminum 5456B Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9283"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;141</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9283' href="/search/DataSheet.aspx?MatGUID=598b6c6c81be463cba988c0f78775b24" >Aluminum 5456-H111</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9284"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;142</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9284' href="/search/DataSheet.aspx?MatGUID=592df9f0744e4c699c0ca57ae1c8b415" >Aluminum 5456-H112</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9285"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;143</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9285' href="/search/DataSheet.aspx?MatGUID=1766d37715124d948907dc0f33f4c685" >Aluminum 5456-H116; 5456-H321</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9286"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;144</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9286' href="/search/DataSheet.aspx?MatGUID=29e08e017960447ab2297d908698a616" >Aluminum 5456-H24</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9287"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;145</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9287' href="/search/DataSheet.aspx?MatGUID=b2e617681a3146a28110c66fc0f64cc3" >Aluminum 5456-H25</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9288"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;146</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9288' href="/search/DataSheet.aspx?MatGUID=bc900295ff734f7096480911269f0d0a" >Aluminum 5456-O</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9289"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;147</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9289' href="/search/DataSheet.aspx?MatGUID=0767a0fa6067442583fda2bbaea7e50e" >Aluminum 5457-H25</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9290"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;148</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9290' href="/search/DataSheet.aspx?MatGUID=493be12fbcb648b7941827ea29d4378e" >Aluminum 5457-H28; 5457-H38</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9291"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;149</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9291' href="/search/DataSheet.aspx?MatGUID=d64398b0aab8438fa1d85d6e3275acc9" >Aluminum 5457-O</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_160189"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;150</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_160189' href="/search/DataSheet.aspx?MatGUID=24ceb17cd4ba40c5a5a192614f04472e" >Aluminum 5483 Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9300"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;151</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9300' href="/search/DataSheet.aspx?MatGUID=97148315e8be439681a1870275a6d063" >5505 Aluminum Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9301"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;152</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9301' href="/search/DataSheet.aspx?MatGUID=e103f0105aab49fbbf24e6cdc5ba9fae" >5554 Aluminum Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9302"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;153</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9302' href="/search/DataSheet.aspx?MatGUID=5e9ed05ebcdd43a5a21f9c7cd4f52b97" >5556 Aluminum Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9303"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;154</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9303' href="/search/DataSheet.aspx?MatGUID=c5c3343888b4422290b67dce838a18ac" >Aluminum 5556A Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_160184"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;155</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_160184' href="/search/DataSheet.aspx?MatGUID=1cbb5495c02144f78036744b39269b11" >Aluminum 5556B Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_160185"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;156</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_160185' href="/search/DataSheet.aspx?MatGUID=1dcbc54e0cf248ecac5b1a8d817b27d4" >Aluminum 5556C Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9304"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;157</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9304' href="/search/DataSheet.aspx?MatGUID=ca9189b446bf45aab56143f76a0c7fa7" >5557 Aluminum Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9305"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;158</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9305' href="/search/DataSheet.aspx?MatGUID=f5bbf35d628b411ca8eabb6544fee8d2" >5605 Aluminum Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9311"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;159</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9311' href="/search/DataSheet.aspx?MatGUID=56fce5c1c328455ab7e6f9f706d5207b" >Aluminum 5652-H32</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9312"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;160</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9312' href="/search/DataSheet.aspx?MatGUID=cb90479b963c49b183250b7c0a7ada67" >Aluminum 5652-H34</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9313"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;161</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9313' href="/search/DataSheet.aspx?MatGUID=7eaead7e377c40c7a96fa3d62c8d16b8" >Aluminum 5652-H36</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9314"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;162</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9314' href="/search/DataSheet.aspx?MatGUID=a79a88e7246e4122bec0641135e7302b" >Aluminum 5652-H38</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9315"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;163</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9315' href="/search/DataSheet.aspx?MatGUID=b8afaeb8eb5144f7a12f7d23d987f2a9" >Aluminum 5652-O</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9316"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;164</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9316' href="/search/DataSheet.aspx?MatGUID=17887c57257649a0967df1c7a5f019d3" >5654 Aluminum Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_160181"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;165</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_160181' href="/search/DataSheet.aspx?MatGUID=7e7eb8f390b24caaba98f9762d631801" >Aluminum 5654A Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9317"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;166</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9317' href="/search/DataSheet.aspx?MatGUID=ae61332b47be4ae9a8e38a428dd00a6a" >Aluminum 5657-H25</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9318"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;167</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9318' href="/search/DataSheet.aspx?MatGUID=5b67d1e2587d43f9a4d70db0439a4628" >Aluminum 5657-H28; 5657-H38</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9319"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;168</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9319' href="/search/DataSheet.aspx?MatGUID=87c27d03069d4b2cbce2ab642e4883f2" >Aluminum 5657-O</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9323"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;169</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9323' href="/search/DataSheet.aspx?MatGUID=bb191b05a9424da5bdef4be5cad33182" >5754-H22 Aluminum</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_130360"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;170</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_130360' href="/search/DataSheet.aspx?MatGUID=e40b7c1abf33410a8ddd59c441faa5a8" >5754-H24 Aluminum</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_130361"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;171</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_130361' href="/search/DataSheet.aspx?MatGUID=b9183da49f3d417ca447dfe45b66d71b" >5754-H26 Aluminum</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_160260"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;172</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_160260' href="/search/DataSheet.aspx?MatGUID=15202c3d7952414ca600353df4bed6cd" >Aluminum 5854 Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9325"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;173</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9325' href="/search/DataSheet.aspx?MatGUID=e3dfee788a9d4f8f99d9c387d5ee7104" >5954 Aluminum Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_161566"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;174</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_161566' href="/search/DataSheet.aspx?MatGUID=c1de7c6e8a9d45efa06da745e07094a2" >Aleris AA5024-H116 Aluminum Magnesium Scandium Alloy</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_159715"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;175</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_159715' href="/search/DataSheet.aspx?MatGUID=7e6ef20fdf2a48d084736e901e0666a7" >Aleris HOGAL 5558 Aluminum 5026-O/H111</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_159716"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;176</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_159716' href="/search/DataSheet.aspx?MatGUID=43fe7ad5d4d3491297599f58d4264f90" >Aleris HOGAL 5558 Aluminum 5026 H14/H34</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_159717"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;177</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_159717' href="/search/DataSheet.aspx?MatGUID=b8de580814574dda958c8b2a4aebe710" >Aleris 55HX® Aluminum (5005-H14)</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_14756"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;178</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_14756' href="/search/DataSheet.aspx?MatGUID=e1d6d9ccf97d46258d3187438acd8c3a" >ALIMEX ACP 5080R Aluminum Cast Material</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_14757"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;179</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_14757' href="/search/DataSheet.aspx?MatGUID=ee4ac64e03a54e1984ef551bc9b6e9d9" >ALIMEX ACP 5080 Aluminum Cast Plate</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_14759"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;180</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_14759' href="/search/DataSheet.aspx?MatGUID=61c530f7b9264bda8b80047533dc402c" >ALIMEX PLANAL 5083 O/H111 Aluminum Alloy</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_14763"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;181</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_14763' href="/search/DataSheet.aspx?MatGUID=d7f7fef2bf32408ba7d80f7c93a890ac" >ALIMEX 5083 O/H111Aluminum Alloy Rolled</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_160264"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;182</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_160264' href="/search/DataSheet.aspx?MatGUID=1d5cdc6a16604271a1653bb08786115d" >Constellium PLAN 5083 Aluminum Rolled Plate</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_160265"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;183</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_160265' href="/search/DataSheet.aspx?MatGUID=afcdbfe0eac242e984ace8ecc5f161f7" >Constellium PLAN OX 5754 Aluminum Rolled Plate</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_160266"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;184</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_160266' href="/search/DataSheet.aspx?MatGUID=dc9f13b40ede422ab8c9237c08bbd0b9" >Constellium PLAN 5754 Aluminum Rolled Plate</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_160270"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;185</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_160270' href="/search/DataSheet.aspx?MatGUID=40ab94341f2c4b5b94d8abcc89d24a03" >Constellium ALPLAN® 5083 Rolled Precision Aluminum Plate, Milled Both Sides</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_160274"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;186</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_160274' href="/search/DataSheet.aspx?MatGUID=ea2c96f526f6470aa768a254556ca3c2" >Constellium Alumold® 110 Aluminum Cast Sliced Plate</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_160275"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;187</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_160275' href="/search/DataSheet.aspx?MatGUID=849f0714fb0847cba7a570b966810275" >Constellium Alumold® 350 Aluminum</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_160276"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;188</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_160276' href="/search/DataSheet.aspx?MatGUID=b96fac694c864065baf415b2c4ba135b" >Constellium Alumold® 400 Aluminum</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_160277"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;189</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_160277' href="/search/DataSheet.aspx?MatGUID=c1b3d09d78ce4b9bae4f3f2122042f60" >Constellium Alumold® 500 Rolled Aluminum</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_160278"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;190</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_160278' href="/search/DataSheet.aspx?MatGUID=24a7e2973fa54a88b655e6614df282fc" >Constellium Alumold® 500 Forged Aluminum</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_160283"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;191</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_160283' href="/search/DataSheet.aspx?MatGUID=efaefb2fa1b5425baca2f72396711880" >Constellium Fibral® Cast Aluminum Sliced Plate</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_160284"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;192</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_160284' href="/search/DataSheet.aspx?MatGUID=ad2eaca988de408db709228d8f6f1b70" >Constellium Formall® 545 Aluminum Alloy</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_160285"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;193</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_160285' href="/search/DataSheet.aspx?MatGUID=8a132ffc5cbb4441b62bec1eb23669ee" >Constellium Fortal® Aluminum Alloy</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_160286"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;194</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_160286' href="/search/DataSheet.aspx?MatGUID=8536ff05c6f943ae92bf626b22f15a9f" >Constellium Planoxal® 50 Aluminum plate</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17340"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;195</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17340' href="/search/DataSheet.aspx?MatGUID=b4224313f3c2433db70fd12eefd2de31" >Aleris Alustar Aluminum Alloy, H321/H116; 2 - 20 mm thickness</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17341"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;196</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17341' href="/search/DataSheet.aspx?MatGUID=1cd7843ff55043c8b9f739b57ac6dcad" >Aleris Alustar Aluminum Alloy, H321/H116; 20 - 40 mm thickness</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_159718"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;197</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_159718' href="/search/DataSheet.aspx?MatGUID=ebba0820e5324e6b8188b3a76c10b76c" >Aleris Alustar H131 Aluminum Alloy</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_159719"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;198</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_159719' href="/search/DataSheet.aspx?MatGUID=79b811dad7114df9a2d7b9206c69cad1" >Aleris Alustar H136 Aluminum Alloy</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17342"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;199</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17342' href="/search/DataSheet.aspx?MatGUID=4d3ad7eb711f47ca8ec23c4671542f54" >Aleris Alustar Aluminum Alloy, MIG-welded</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17343"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;200</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17343' href="/search/DataSheet.aspx?MatGUID=b483214d026d450281abeb8f485348d7" >Aleris Alustar Aluminum Alloy, O/H111</a></td></tr>

		
	</table>

	<table style="background-color:Silver;" class="tabledataformat" >

		<tr class="">
			<th>&nbsp;</th>
		</tr>

		<tr>
			<td><strong>Found <span id="ctl00_ContentMain_UcSearchResults1_lblResultCount2">299</span> Results</strong> -- 
				
				Page 
				<select name="ctl00$ContentMain$UcSearchResults1$drpPageSelect2" onchange="javascript:setTimeout('__doPostBack(\'ctl00$ContentMain$UcSearchResults1$drpPageSelect2\',\'\')', 0)" id="ctl00_ContentMain_UcSearchResults1_drpPageSelect2">
		<option selected="selected" value="1">1</option>
		<option value="2">2</option>

	</select>
				of <span id="ctl00_ContentMain_UcSearchResults1_lblPageTotal2">2</span> 
				
				-- 
				<a id="ctl00_ContentMain_UcSearchResults1_lnkPrevPage2" disabled="disabled" style="color:Gray;">[Prev Page]</a>
				<a id="ctl00_ContentMain_UcSearchResults1_lnkNextPage2" href="javascript:__doPostBack('ctl00$ContentMain$UcSearchResults1$lnkNextPage2','')" style="color:Black;">[Next Page]</a>
				--&nbsp;

				view 
				<select name="ctl00$ContentMain$UcSearchResults1$drpPageSize2" onchange="javascript:setTimeout('__doPostBack(\'ctl00$ContentMain$UcSearchResults1$drpPageSize2\',\'\')', 0)" id="ctl00_ContentMain_UcSearchResults1_drpPageSize2">
		<option value="25">25</option>
		<option value="50">50</option>
		<option value="75">75</option>
		<option value="100">100</option>
		<option value="150">150</option>
		<option selected="selected" value="200">200</option>

	</select> per page
			</td>
		</tr>
	</table>
	
	 


</div>





<script type="text/javascript">

	//GLOBAL VARIABLES
	var gCurrentCheckbox;
	var gNewFolderID;
	var gOldFolderName;
	var gSelectedFolderID = 0;
	var gMaxFolders = 0;

	//GLOBAL FIELD REFERENCES
	var drpFolderList = document.getElementById("ctl00_ContentMain_UcSearchResults1_drpFolderList");
	var drpFolderAction = document.getElementById("ctl00_ContentMain_UcSearchResults1_drpFolderAction");
	var txtFolderMatCount = document.getElementById("ctl00_ContentMain_UcSearchResults1_txtFolderMatCount");
	var divUserMessage = document.getElementById("divUserMessage");
	var divFolderName = document.getElementById("divFolderName");
	var tblFolderName = document.getElementById("tblFolderName");
	var txtFolderName = document.getElementById("txtFolderName2");
	
	// See MS ajax.net client side documentation
	// http://ajax.asp.net/docs/ClientReference/Sys.Net/WebrequestManagerClass/	
	Sys.Net.WebRequestManager.add_invokingRequest(ucSearchResults_OnRequestStart);
	Sys.Net.WebRequestManager.add_completedRequest(ucSearchResults_OnRequestEnd);
	
	var timerID = 0;

	function ucSearchResults_OnRequestStart(){
		document.body.style.cursor="progress";
	}
	
	function ucSearchResults_OnRequestEnd(){
		document.body.style.cursor="auto";
	}

	//drpFolderAction pull down does not exist for anonymous users
	if(drpFolderAction) {
		drpFolderAction.onchange=RunFolderAction;
	}

	//RUN ONCE on page load
	//matweb.register_onload(SetSelectedFolder);
	
	SearchMatIDList = []; //an array of MatIDs
	//matweb.alert(MatIDList);
	
	//======================
	//Test Service 
	//======================
	//TestService("This is a test");
	//aci.matweb.WebServices.Folders.RaiseTestError(null,ErrorHandler,"testerror");
	
	function TestService(arg){
		aci.matweb.WebServices.Folders.HelloWorld(arg, TestService_CALLBACK,ErrorHandler,"TestService");
	}
	
	function TestService_CALLBACK(data,context){
		alert(data);
	}
	
	// =================================
	// This function is the onchange event for the material checkbox. See Code Behind.
	// The first time a material is checked, and there are no folders, the folder web service creates a NEW FOLDER
	// =================================
	function ToggleMat(chkBox,MatID,MaxFolderMats){
	
		HideFolderNameControls();

		var FolderID,Context;
		gCurrentCheckbox = chkBox;
		var matLink = $("lnkMatl_" + MatID);
		var MatName = matLink.innerHTML;
		//alert(MatName);

		var SelectedOption = drpFolderList.options[drpFolderList.selectedIndex];  

		if(gNewFolderID > 0)
			FolderID = gNewFolderID;
		else
			FolderID = SelectedOption.value; //this will be 0 if no folders exist yet
		//alert(FolderID);
		
		// call the folder web service 
		// See: ScriptManagerProxy object at the top of this page
		// Also See: /folders/FolderService.asmx & /App_Code/FolderService.cs
		if(chkBox.checked){
			Context = "AddMat";
			aci.matweb.WebServices.Folders.AddMaterial(FolderID, MatID, MatName, ToggleMat_CALLBACK,ErrorHandler,Context);
		}
		else{
			Context = "RemoveMat";
			aci.matweb.WebServices.Folders.RemoveMaterial(FolderID, MatID, ToggleMat_CALLBACK,ErrorHandler,Context);
		}
			
	}
	
	// =================================
	// CALLBACK FUNCTION
	// this function is called by the AJAX framework after the web service is invoked
	// The folder web service returns a ReturnData type object.
	// =================================
	function ToggleMat_CALLBACK(data,context){

		 //alert(data);
		// alert(context);

		var FolderMatCount = data.FolderMatCount
		var NewFolderID = data.NewFolderID
		var DisplayMessage = data.DisplayMessage
		var MaxFolderMats = data.FolderMaxMatls;
		
		txtFolderMatCount.value = FolderMatCount + "/" + MaxFolderMats;

		// If this is the first time the user checked the checkbox, and the user did not already have any folders, 
		// a new folder may have been created.
		// Save the new folderID to a hidden field, so we can reference it later.
		if(NewFolderID > 0){
			gNewFolderID = NewFolderID;
		}
		
		// alert(gNewFolderID);
		// alert(FolderCount);
		// alert(ReloadFolderList);
		// if(NewFolderID>0) alert(NewFolderID);
		// alert(MaxFolderMats);
		
		switch(context){
		case "AddMat":
			if(DisplayMessage){
				gCurrentCheckbox.checked = false;
				matweb.alert(DisplayMessage);
			}
			break    
		case "RemoveMat":
			break    
		default:
			matweb.Alert("Unhandeled Context in ToggleMat_CALLBACK() function: " + Context);
		}//end switch

	}//end function

	
	//=================================
	function CompareMaterials(){
	//=================================
		var SelectedFolderID = GetSelectedFolderID();
		window.location.href="/folders/Compare.aspx?FolderID=" + SelectedFolderID;
	}

	//=================================
	// Runs on drpFolderList.onchange, and also is called by other methods.
	// Sets the checkbox for each visible material in the folder.
	// Also sets txtFolderMatCount with the MatCount indicator for the selected folder.
	//=================================
	function SetSelectedFolder(){
		//alert("SetSelectedFolder()");
		var SelectedFolderID = GetSelectedFolderID();
		//alert("SelectedFolderID :" + SelectedFolderID)
		
		aci.matweb.WebServices.Folders.SetSelectedFolder(SelectedFolderID,SetSelectedFolder_CALLBACK,ErrorHandler,"update_txtFolderMatCount");
		
		HideFolderNameControls();
	}

	// =================================
	// CALLBACK FUNCTION
	// Returns a list of MatID's in the selected folder, and sets the checkbox for each material.
	// Also sets txtFolderMatCount with the MatCount indicator for the selected folder.
	// =================================
	function SetSelectedFolder_CALLBACK(data,context){

		// alert(data);
		// alert(context);

		// the folder web service returns an object with 4 fields
		//alert("data.FolderMatCount: " + data.FolderMatCount);
		//alert("data.FolderMaxMatls: " + data.FolderMaxMatls);
		//alert("data.DisplayMessage: " + data.DisplayMessage);
		//alert("data.MatIDList: " + data.FolderMatIDList);

		if(data.DisplayMessage){
			matweb.alert(data.DisplayMessage);
		}

		txtFolderMatCount.value = data.FolderMatCount + "/" + data.FolderMaxMatls;

		//alert(SearchMatIDList);
		var chkFolder;

		// Set checkboxes for any visible materials in the folder
		// loop over all materials in the search results for the current page...
		//alert('lenOfArray=' + SearchMatIDList.length);
		for(s=0;s<SearchMatIDList.length;s++){
			chkFolder = document.getElementById("chkFolder_" + SearchMatIDList[s]);
			//alert('index=' + SearchMatIDList[s]);
			chkFolder.checked = false; //clear any currently checked boxes...
			//loop over all materials in the folder...
			//If there is a match, make it checked.
			for(f=0;f<data.FolderMatIDList.length;f++){
				if(SearchMatIDList[s]==data.FolderMatIDList[f]){
					//alert("Match found: " + data.FolderMatIDList[f]);
					chkFolder.checked = true;
				}
			}
		}

	}//end function
	
	//=================================
	function RunFolderAction(){
	//=================================
	
		//alert("RunFolderAction()");
		var action = drpFolderAction.options[drpFolderAction.selectedIndex].value;
		var SelectedFolderName = drpFolderList.options[drpFolderList.selectedIndex].text;
		var FolderID = GetSelectedFolderID();
		var msg = "";
		
		switch(action){
			case "empty":
				aci.matweb.WebServices.Folders.EmptyFolder(FolderID,SetSelectedFolder_CALLBACK,ErrorHandler,action);
				msg = "Folder ["+SelectedFolderName+"] was cleared of materials";
				HideFolderNameControls();
				break;
			case "delete":
				var answer = confirm("Delete folder ["+SelectedFolderName+"] ?");
				if(answer){
					gOldFolderName = SelectedFolderName;
					aci.matweb.WebServices.Folders.DeleteFolder(FolderID,DeleteFolder_CALLBACK,ErrorHandler,action);
				}
				HideFolderNameControls();
				break;
			case "new":
				//setting up new folder controls
				if(drpFolderList.options.length >= gMaxFolders){
					matweb.alert("You have already added as many folders as you are currently allowed by the system (" + gMaxFolders + ").<p/> You may view our <a href=\"/membership/benefits.aspx\">benefits</a> page for the features that are allowed for your current user level.");
				}
				else{
				divFolderName.style.display="block";
				divFolderName.innerHTML="New Folder Name";
				tblFolderName.style.display="block";
				txtFolderName.value = "New Folder Name";
				txtFolderName.focus();
				txtFolderName.select();
				}
				break;
			case "rename":
				//setting up rename folder controls
				divFolderName.style.display="block";
				divFolderName.innerHTML="Rename Folder";
				tblFolderName.style.display="block";
				txtFolderName.value = SelectedFolderName;
				txtFolderName.focus();
				txtFolderName.select();
				break;
			case "managefolders":
				window.location.href="/folders/ListFolders.aspx";
				break;
			case "expSolidworks":
				ExportFolder(FolderID, 2, null);
				break;
			case "expALGOR":
				ExportFolder(FolderID, 3, null);
				break;
			case "expANSYS":
				ExportFolder(FolderID, 4, null);
				break;
			case "expNEiWorks":
				ExportFolder(FolderID, 5, null);
				break;
			case "expCOMSOL3":
				ExportFolder(FolderID, 6, '3.4 or newer');
				break;
			case "expCOMSOL4":
				ExportFolder(FolderID, 10, null);
				break;
			case "expETBX":
				ExportFolder(FolderID, 8, null);
				break;
			case "expSpaceClaim":
				ExportFolder(FolderID, 9, null);
				break;
			case "0":
				//do nothing
				drpFolderAction.selectedIndex = 0;
				break;
			default: 
				alert("Unhandled Action: " + action);
		}

		//drpFolderAction.selectedIndex=0;
		divUserMessage.innerHTML = msg;
		//alert("End RunFolderAction()");
		
	}
	
	function DeleteFolder_CALLBACK(DeleteWasSuccessfull,context){
		divUserMessage.innerHTML = "Folder ["+gOldFolderName+"] was deleted.";
		//Refresh the folder list and selected checkboxes
		aci.matweb.WebServices.Folders.GetFolderList(GetFolderList_CALLBACK,ErrorHandler,"DeleteFolder_CALLBACK");
	}	

	// rsw
	function ExportFolder(folderid, modeid, comsolversion){
		if(txtFolderMatCount.value.substring(0,1) == '0'){
			alert('Folder is empty, nothing to export.');
			drpFolderAction.selectedIndex = 0;
			return;
		}

		var ExportPath = "/folders/ListFolders.aspx";
		
		//alert(ExportPath);
		location.href = ExportPath;
		//window.open(ExportPath, '', '', '');
	}
	
	
	
	
	//===============================================================
	// This runs on the onclick event for btnApplyFolderName.
	// It handles creating a new folder, or renaming an existing one.
	//===============================================================
	function ApplyFolderName(){
	
		//alert("ApplyFolderName()");
		var action = drpFolderAction.options[drpFolderAction.selectedIndex].value;
		var NewFolderName = txtFolderName.value;
		var FolderID = GetSelectedFolderID();
		var msg = "";

		//alert(FolderID);

		//If we are showing the non-existant "virtual" folder "My Folder",
		//create it first, and then rename it.
		if(action=="rename" && FolderID==0)
			action="add_and_rename";

		switch(action){
			case "new":
				aci.matweb.WebServices.Folders.AddFolder(NewFolderName,AddRenameFolder_CALLBACK,ErrorHandler,action);
				break;
			case "rename":
				gOldFolderName = drpFolderList.options[drpFolderList.selectedIndex].text;
				aci.matweb.WebServices.Folders.RenameFolder(FolderID,NewFolderName,AddRenameFolder_CALLBACK,ErrorHandler,action);
				break;
			case "add_and_rename":
				gOldFolderName = drpFolderList.options[drpFolderList.selectedIndex].text;
				aci.matweb.WebServices.Folders.AddFolder(NewFolderName,AddRenameFolder_CALLBACK,ErrorHandler,action);
				break;
			default: 
				alert("Unhandled Action in ApplyFolderName(): " + action);
		}

		HideFolderNameControls();
		
	}
	
	//=================================
	// CALLBACK FUNCTION
	//=================================
	function AddRenameFolder_CALLBACK(ReturnData,context){
		//alert("AddRenameFolder_CALLBACK()");
		
		var msg;
		//Folder = ReturnData.Folder;
	
		switch(context){
			case "new":
				//gNewFolderID = Folder.FolderID;
				gNewFolderID = ReturnData.NewFolderID;
				msg = "The folder [" + ReturnData.NewFolderName + "] was created.";
				break;
			case "rename":
				msg = "The folder [" + gOldFolderName+ "] was renamed to [" + ReturnData.NewFolderName + "].";
				break;
			case "add_and_rename":
				//gNewFolderID = Folder.FolderID;
				gNewFolderID = ReturnData.NewFolderID;
				msg = "The folder [" + gOldFolderName+ "] was renamed to [" + ReturnData.NewFolderName + "].";
				break;
		}
		
		divUserMessage.innerHTML = msg;
		
		//refresh the folder list, so it includes the new folder
		aci.matweb.WebServices.Folders.GetFolderList(GetFolderList_CALLBACK,ErrorHandler,"AddFolder");

	}
	
	//=================================
	function HideFolderNameControls(){
	//=================================
		//These fields will not exist for anonymous users
		if(drpFolderAction){
			drpFolderAction.selectedIndex=0;
			divFolderName.style.display="none";
			tblFolderName.style.display="none";
		}
	}
	
	//=================================
	function GetSelectedFolderID(){
	//=================================
		var FolderID = 0;
		//if one of the processeses created a new folder id, then use it.
		// alert('gNewFolderID=' + gNewFolderID);
		if(gNewFolderID > 0){
			FolderID = gNewFolderID;
			//gNewFolderID = 0;//clear the new folder id, so we don't use it again.
		}
		else 
			if(drpFolderList != null){
				if(drpFolderList.options.length == 0){
					FolderID = gSelectedFolderID;
				}
				else{
					var SelectedOption = drpFolderList.options[drpFolderList.selectedIndex];  
					FolderID = SelectedOption.value; //this will be 0 if no folders exist yet
				}
			}
		return FolderID;
	}

	//=================================
	function UnregisteredUserMessage(chkBox){
	//=================================
		matweb.alert("This feature is reserved for registered users.<p/><a href='/membership/regstart.aspx'>Click here to register.</a>","Reserved Feature");
		chkBox.checked = false;
	}

	// =================================
	// CALLBACK FUNCTION
	// The folder web service returns a JS array of iBatis data objects of type: aci.matweb.data.folder.Folder 
	// The drop down list of folders is set using this list.
	// =================================
	function GetFolderList_CALLBACK(iFolderList,Context){
	
		//alert(iFolderList);
		//alert('iFolderList.length: ' + iFolderList.length);

		if(drpFolderList != null){
		
		var SelectedFolderID = GetSelectedFolderID();//remember the current selected folder, so we can use it later
		var opt;
		var folder;

		//clear the options and reload them
		drpFolderList.options.length = 0;

		//add a default empty folder...		
		if(iFolderList.length==0){
			opt = document.createElement('OPTION');
			opt.value = "0";
			opt.text = "My Folder";
			drpFolderList.options.add(opt);
			i++;
		}
		
		for(var i = 0;i < iFolderList.length;i++){
			opt = document.createElement('OPTION');
			folder = iFolderList[i];
			//alert(folder.FolderID);
			//alert(folder.FolderName);
			opt.value = folder.FolderID;
			opt.text = folder.FolderName;
			if(folder.FolderID == SelectedFolderID) opt.selected = true;
			drpFolderList.options.add(opt);
		}

		//clear the new folder id, so we don't use it again. 
		gNewFolderID = 0;
		
		//Set checkboxes and folder count fields for the new selected folder
		SetSelectedFolder();
		}

	}
	
	// =================================
	// ON ERROR CALLBACK FUNCTION
	// =================================
	function ErrorHandler(error,context) {
	
			var stackTrace = error.get_stackTrace();
			var message = error.get_message();
			var statusCode = error.get_statusCode();
			var exceptionType = error.get_exceptionType();
			var timedout = error.get_timedOut();
			var Debug = false;

			// Compose the error message...
			var ErrMsg =     
					"<strong>Stack Trace</strong><br/>" +  stackTrace + "<br/>" +
					"<strong>WebService Error</strong><br/>" + message + "<br/>" +
					"<strong>Status Code</strong><br/>" + statusCode + "<br/>" +
					"<strong>Exception Type</strong><br/>" + exceptionType + "<br/>" +
					"<strong>JS Context</strong><br/>" + context + "<br/>" +
					"<strong>Timedout</strong><br/>" + timedout + "<br/>" +
					"<strong>Source Page</strong><br/>" + "/search/MaterialGroupSearch.aspx";
			//matweb.alert(ErrMsg,"Error in WebService",null,"500");
			if(Debug){
				matweb.alert(ErrMsg,"Error in WebService",null,"500");
				//alert(ErrMsg);
			}
			else{
				ErrMsg = escape(ErrMsg);
				//matweb.alert(ErrMsg);
				//window.location.href="/errorJS.aspx?ErrMsg=" + ErrMsg;
				divUserMessage.innerHTML = "Problem retrieving folder data...Some folder features may not work.";
			}
		
	}
	
	function ShowInterpolationMessage(){
		InterpolationMsg = "The material was found via interpolation, so no data points can be displayed. See the material data sheet for actual data points.";
		matweb.alert(InterpolationMsg);
	}
	
</script>

		</td>
	</tr>
	</table>

<hr />
<a id="help"></a>
<table><tr><td>
<p/><b>Instructions:</b> This page allows you to quickly access all of the polymers/plastics, metals, ceramics, fluids, and other engineering materials
in the MatWeb material property database.&nbsp; Just select the material category you would like to find from the tree.  Click on the [+] icon to open subcategory branches in the tree.
Then click the <b>'Find'</b> button next to its box.&nbsp; You can then follow the links to the most complete data sheets on the Web, with information on over 1000 available properties, including dielectric
constant, Poisson's ratio, glass transition temperature, dissipation factor, and Vicat softening point.
</td><td><a href="https://proto3000.com"><img src="/images/assets/proto3000.jpg" border = "0"><br>

Proto3000 - Rapid Prototyping</a>
</td></tr></table>

<p/><span class="hiText">Notes:</span>

<ul>
  <li>Visit MatWeb's <a href="/search/PropertySearch.aspx">property-based search page</a> to limit your results to materials that meet specific property performance criteria.</li>
  <li>Text searches can be done from any page - use the <b>Quick Search</b> box in the top right of the page.</li>
  <li>More <a href="/help/strategy.aspx"><strong>Search Strategies</strong></a> on how to find information in MatWeb.</li>
   <li><a href="/search/GetAllMatls.aspx">Show All Materials</a> - an inefficient drill-down to data sheets.  Registered users who are logged in omit a step in this drill down process.</li>
</ul>

<p/><a href="#Top">Top</a>

<script type="text/javascript">

	var txtMatGroupText = document.getElementById("ctl00_ContentMain_txtMatGroupText");
	var txtMatGroupID = document.getElementById("ctl00_ContentMain_txtMatGroupID");
	var divMatGroupName = document.getElementById("ctl00_ContentMain_divMatGroupName");

	function ucMatGroupFinder1_OnSelected(MatGroupID,MatGroupText){
		txtMatGroupText.value = MatGroupText;
		txtMatGroupID.value = MatGroupID;
		divMatGroupName.innerHTML = MatGroupText;
		//__doPostBack("ctl00$ContentMain$btnSubmit","");
	}

	function ucMatGroupTree_OnNodeClick(NodeText,MatGroupID){
		txtMatGroupText.value = NodeText;
		txtMatGroupID.value = MatGroupID;
		divMatGroupName.innerHTML = NodeText;
		//__doPostBack("ctl00$ContentMain$btnSubmit","");
	}
	
	function validate(){
		if (txtMatGroupID.value == ""){
			matweb.alert("Please select a material category","User Input Error");
			return false;
		}
		
		return true;
		
	}

</script>


</div>
<!-- ===================================== END MAIN CONTENT ========================================== -->

<table class="tabletight" style="width:100%" >
        <tr>
                <td align="center" class="footer" colspan="3">
                        <br /><br />
                
                        <a href="/membership/regupgrade.aspx" class="footlink"><span class="subscribeLink">Subscribe to Premium Services</span></a><br/>
                
                        <span class="footer"><b>Searches:</b>&nbsp;&nbsp;</span>
                        <a href="/search/AdvancedSearch.aspx" class="footlink"><span class="foot">Advanced</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/search/CompositionSearch.aspx" class="footlink"><span class="foot">Composition</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/search/PropertySearch.aspx" class="footlink"><span class="foot">Property</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/search/MaterialGroupSearch.aspx" class="footlink"><span class="foot">Material&nbsp;Type</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/search/SearchManufacturerName.aspx" class="footlink"><span class="foot">Manufacturer</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/search/SearchTradeName.aspx" class="footlink"><span class="foot">Trade&nbsp;Name</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/search/SearchUNS.aspx" class="footlink"><span class="foot">UNS Number</span></a>
                        <br />
                        <span class="footer"><b>Other&nbsp;Links:</b>&nbsp;&nbsp;</span>
                        <a href="/services/advertising.aspx" class="footlink"><span class="foot">Advertising</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/services/submitdata.aspx" class="footlink"><span class="foot">Submit&nbsp;Data</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/services/databaselicense.aspx" class="footlink"><span class="foot">Database&nbsp;Licensing</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/services/webhosting.aspx" class="footlink"><span class="foot">Web&nbsp;Design&nbsp;&amp;&nbsp;Hosting</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/clickthrough.aspx?addataid=277" class="footlink"><span class="foot">Trade&nbsp;Publications</span></a>

                        <br />
                        <a href="/reference/suppliers.aspx" class="footlink"><span class="foot">Supplier&nbsp;List</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/tools/unitconverter.aspx" class="footlink"><span class="foot">Unit&nbsp;Converter</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/tools/contents.aspx#reference" class="footlink"><span class="foot">Reference</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/reference/link.aspx" class="footlink"><span class="foot">Links</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/help/help.aspx" class="footlink"><span class="foot">Help</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/services/contact.aspx" class="footlink"><span class="foot">Contact&nbsp;Us</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/tools/contents.aspx" class="footlink"><span class="foot" style="color:red;">Site&nbsp;Map</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/reference/faq.aspx" class="footlink"><span class="foot">FAQ</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/index.aspx" class="footlink"><span class="foot">Home</span></a>
                        <br />&nbsp;
                        <div id="fb-root"></div>

<table><tr><td><script src="http://connect.facebook.net/en_US/all.js#xfbml=1"></script><fb:like-box href="https://www.facebook.com/MatWeb.LLC" width="175" show_faces="false" stream="false" header="false"></fb:like-box>

</td><td>

<a href="https://twitter.com/MatWeb" class="twitter-follow-button" data-show-count="false" data-show-screen-name="false">Follow @MatWeb</a> <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>

</td></tr></table>
                </td>
        </tr>
        <tr>
                <td colspan="3"><img src="/images/bluebar.gif" width="100%" height="5" alt="" /></td>
        </tr>
        <tr>
                <td style="background-image:url(/images/gray.gif);"><img src="/images/spacer.gif" width="5" height="1" alt=""/></td>
                <td style="background-image:url(/images/gray.gif);">
                        <span class="footer">
                        Please read our <a href="/reference/terms.aspx" class="footlink"><span class="foot">License Agreement</span></a> regarding materials data and our  <a href="/reference/privacy.aspx" class="footlink"><span class="foot">Privacy Policy</span></a>.
                        Questions or comments about MatWeb? Please contact us at
                        <a href="mailto:webmaster@matweb.com" class="footlink"><span class="foot">webmaster@matweb.com</span></a>. We appreciate your input.
                        <br /><br />
                        The contents of this web site, the MatWeb logo, and "MatWeb" are Copyright 1996-2024
                        by MatWeb, LLC. MatWeb is intended for personal, non-commercial use. The contents, results, and technical data from this site
                        may not be reproduced either electronically, photographically or substantively without permission from MatWeb, LLC.
                        </span>
                        <br />
                </td>

                <td style="background-image:url(/images/gray.gif);"><img src="/images/spacer.gif" width="5" height="1" alt=""/></td>
        </tr>
</table>




<script type="text/javascript">
//<![CDATA[
var ctl00_ContentMain_ucMatGroupTree_msTreeView_ImageArray =  new Array('', '', '', '/WebResource.axd?d=zLp3QJeZSj4-fsnL2_raV70Mk3pr-OdjL_rvrZzXUTCnZ6LU5Dg_Tc5XZg4eZpT9O0gxZJrJcxGb23XJofeGD9JhYLr2wl0VkQZnJOx0T0ShBosq0&t=638313619312541215', '/WebResource.axd?d=vFualGN2ZZ4fny_RX_gYiUTY6FBLUapne4VfY6P3sGiyrOYd-Ji2sWg_olT-exaztendUUEOoH87RS2l44bpZTscfmmv4MKRqJK8rgesmkrNfP2O0&t=638313619312541215', '/WebResource.axd?d=4nLCbQ2Yfc2i_msrQXqKzK8pLQD-GrNVZ7DeXU-La2qatczrgeCLazv0RzIkDDHyYfLFalCBO8s71lRS8yifxTChF9dUJvTLxNEeeFlghyuY0dtM0&t=638313619312541215', '/WebResource.axd?d=uEI6Enq15_q4pXO6g0E7vtAqtD5WIokAEQ2Vz8V3Yi36cefediLfyCwENkPd-w1GxTXu-HkNqcDlrYOw8bxgGqeHJtzc-PBlTa9nrZRfdIyj8fQh0&t=638313619312541215', '/WebResource.axd?d=hzeik1gGhZrfDZXUWCMsBOx5b43bIFor8UCTzm_wuh92b9Z-WQU4_fQCSUw6CaP5tnc9LTU0jl0sy4Y6Cf_c8q8HFXRqeHQuscXIqqLNSZkgl2cu0&t=638313619312541215', '/WebResource.axd?d=6v5qM6VKQYD3XNwwXEy7Tix7dzVdqDJ8QTRIlUPrzXQqNsXQgUwIfmn34ZzRORd_IZj0bF4vkZzID1HuSFE5uKyLgaPYeAYE8VT8HYfb3Rk6zsDw0&t=638313619312541215', '/WebResource.axd?d=d9nh8nEM1YCqIfE9jLzNhPyFIZqARvlFExwnFG0vbvskO-A2qORlhTyUVIxDtSwldGrOp8o9ef8lZ9SRhPQ9rv9TkGV-NVYvH9_yHT6CvWpIPF4S0&t=638313619312541215', '/WebResource.axd?d=7_V5n1KYSQsj6PoSqgIxb0egBsAa91jSXPkbmM6FaiF14-JsNnpUDnfSdxZcvfd0OQCLDtC24pMIXigO-41cL2rsq-0xea-SSG7yZUyRcliFFqe70&t=638313619312541215', '/WebResource.axd?d=TinC6igX1vPDgWh7DstWwBTHTyH4uuybDU4NW47jJD06wHdAO3YyJC_eKK1Zn5oIbwqRo4Cri8YwhA82G-LD0p14yEF1EfoIZvKleYMIuQ2K1hkE0&t=638313619312541215', '/WebResource.axd?d=4B0dvJqmzY4ukTboQq7lmxokRL9NTsoWh4yErB11Tb-ucvMw_p_0mxYVn4U1Uz6Jk795umrO0C4-B-8Yq1Kxxz6uyDT15O2BSCb8349f4YWDdIVy0&t=638313619312541215', '/WebResource.axd?d=TUiibr9hjS_MlNf09dKZECe1z56fbaPbfgJfC5eYJi0TtdBviUcA1JQJLV81aqunzpXysgruhL3ihpgd7ziL5p9SQ86xVLNiUqbYpJnW9Rj7sN1E0&t=638313619312541215', '/WebResource.axd?d=vUBrM2nNhECNbFO0AJKq52p2zWrY5XG8nWgCDL4AfsrexStb0HREDV6GHMvdkAGDInxYtTyWXuxFUIiQv3JX2fScVRwQsrVv5srHrjOIeC3ayAG20&t=638313619312541215', '/WebResource.axd?d=Z-LP5lKcsnv2A9UpExXu-5leQYIAZMYp6SXNognZi1r66YT8u24-YjJaxjisE9ruYU8NKnNz72H4shclsk8PM3S5M-VYMk_F_G5fcd3ra6Sj9CaS0&t=638313619312541215', '/WebResource.axd?d=QI4SACqRn7PxBBHPfkR7peFMAEv7lX1_iZ_7jncKwxRTlvhSAncd5K2oRqw8J8WmJjWQcbGgEblhrufy-hUjZMa1YLUC47mjfV1eS4LM3kUqLT960&t=638313619312541215', '/WebResource.axd?d=ayrFqNSzdiTAdowDVbEH-ZzKbvw5bCq7onyCSAcJhCVPVIHlUB9M_fo8BLUTeGpbtcoMiyFy1MoeEIsweMrpVandBKC4vAK-NCBcfn2FIreK8mEJ0&t=638313619312541215', '/WebResource.axd?d=OK_uWiuoGURaJ3CC0fVlQ7v9GUnIAkx03w03rQ2ENqcwdObLiQwK7B9yRFyAWj9tuUqsa0874ksgkaRo8KirB7q1ZL_huh7gCq_XehXfHMEKMiAl0&t=638313619312541215');
//]]>
</script>


<script type="text/javascript">
//<![CDATA[
(function() {var fn = function() {AjaxControlToolkit.ModalPopupBehavior.invokeViaServer('ctl00_ContentMain_ucPopupMessage1_ModalPopupExtender1', false); Sys.Application.remove_load(fn);};Sys.Application.add_load(fn);})();
var callBackFrameUrl='/WebResource.axd?d=Mt7MojvAyTqAEcx0MpKgcF_QVa09tLyMQS6c5JLNO4j2M_F6CMxYnN0HdVYxyrhggOpkTwyNqQ51iS6UoHXgJqHBROE1&t=638313619312541215';
WebForm_InitCallback();var ctl00_ContentMain_ucMatGroupTree_msTreeView_Data = new Object();
ctl00_ContentMain_ucMatGroupTree_msTreeView_Data.images = ctl00_ContentMain_ucMatGroupTree_msTreeView_ImageArray;
ctl00_ContentMain_ucMatGroupTree_msTreeView_Data.collapseToolTip = "Collapse {0}";
ctl00_ContentMain_ucMatGroupTree_msTreeView_Data.expandToolTip = "Expand {0}";
ctl00_ContentMain_ucMatGroupTree_msTreeView_Data.expandState = theForm.elements['ctl00_ContentMain_ucMatGroupTree_msTreeView_ExpandState'];
ctl00_ContentMain_ucMatGroupTree_msTreeView_Data.selectedNodeID = theForm.elements['ctl00_ContentMain_ucMatGroupTree_msTreeView_SelectedNode'];
for (var i=0;i<19;i++) {
var preLoad = new Image();
if (ctl00_ContentMain_ucMatGroupTree_msTreeView_ImageArray[i].length > 0)
preLoad.src = ctl00_ContentMain_ucMatGroupTree_msTreeView_ImageArray[i];
}
ctl00_ContentMain_ucMatGroupTree_msTreeView_Data.lastIndex = 8;
ctl00_ContentMain_ucMatGroupTree_msTreeView_Data.populateLog = theForm.elements['ctl00_ContentMain_ucMatGroupTree_msTreeView_PopulateLog'];
ctl00_ContentMain_ucMatGroupTree_msTreeView_Data.treeViewID = 'ctl00$ContentMain$ucMatGroupTree$msTreeView';
ctl00_ContentMain_ucMatGroupTree_msTreeView_Data.name = 'ctl00_ContentMain_ucMatGroupTree_msTreeView_Data';
Sys.Application.initialize();
Sys.Application.add_init(function() {
    $create(AjaxControlToolkit.ModalPopupBehavior, {"BackgroundCssClass":"modalBackground","OkControlID":"ctl00_ContentMain_ucPopupMessage1_btnOK","PopupControlID":"ctl00_ContentMain_ucPopupMessage1_divPopupMessage","PopupDragHandleControlID":"ctl00_ContentMain_ucPopupMessage1_trTitleRow","dynamicServicePath":"/search/MaterialGroupSearch.aspx","id":"ctl00_ContentMain_ucPopupMessage1_ModalPopupExtender1"}, null, null, $get("ctl00_ContentMain_ucPopupMessage1_hndPopupControl"));
});
Sys.Application.add_init(function() {
    $create(AjaxControlToolkit.TextBoxWatermarkBehavior, {"ClientStateFieldID":"ctl00_ContentMain_UcMatGroupFinder1_TextBoxWatermarkExtender2_ClientState","WatermarkCssClass":"greyOut","WatermarkText":"Type at least 4 characters here...","id":"ctl00_ContentMain_UcMatGroupFinder1_TextBoxWatermarkExtender2"}, null, null, $get("ctl00_ContentMain_UcMatGroupFinder1_txtSearchText"));
});
//]]>
</script>
</form>


<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-15290815-1");
pageTracker._setDomainName(".matweb.com");
pageTracker._trackPageview();
} catch(err) {}</script>

<!-- 20160905 insert SpecialChem javascript -->
<script type="text/javascript" src="//collect.specialchem.com/collect.js"></script>
<script type="text/javascript">
    //<![CDATA[
    var _spc = (_spc || []);
    _spc.push(['init',
    {
        partner: 'MatWeb'
        
        //, iid: '12345'
    }]);
    //]]>
</script>
<script> (function(){ var s = document.createElement('script'); var h = document.querySelector('head') || document.body; s.src = 'https://acsbapp.com/apps/app/dist/js/app.js'; s.async = true; s.onload = function(){ acsbJS.init({ statementLink : '', footerHtml : '', hideMobile : false, hideTrigger : false, disableBgProcess : false, language : 'en', position : 'right', leadColor : '#146FF8', triggerColor : '#146FF8', triggerRadius : '50%', triggerPositionX : 'right', triggerPositionY : 'bottom', triggerIcon : 'people', triggerSize : 'bottom', triggerOffsetX : 20, triggerOffsetY : 20, mobile : { triggerSize : 'small', triggerPositionX : 'right', triggerPositionY : 'bottom', triggerOffsetX : 20, triggerOffsetY : 20, triggerRadius : '20' } }); }; h.appendChild(s); })();</script>
</body>
</html>
