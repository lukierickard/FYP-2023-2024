

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head id="ctl00_mainHeader"><title>
	Metal, Plastic, and Ceramic Search Index
</title><meta http-equiv="Content-Style-Type" content="text/css" /><meta http-equiv="Content-Language" content="en" />

        <script src="/includes/flash.js" type="text/javascript"></script>

        <script src="/includes/prototype.js" type="text/javascript"></script>
        <!-- script src="/includes/scriptaculous/effects.js" type="text/javascript"></script -->
        <script src="/includes/matweb.js" type="text/javascript"></script>
        <link rel="stylesheet" href="/includes/main.css" />

        <script type="text/javascript">

                function _onload(){
                        matweb.debug = false; // ;
                        //matweb.writeOverlay();
                }

                function _onkeydown(evt){
                        //PREVENT GLOBAL AUTO-SUBMIT OF FORMS
                        return matweb.DisableAutoSubmit(evt);
                }

                //EXAMPLE USAGE:
                //matweb.register_onload(function(){alert("hello from onload event");});
                //matweb.writeOverlay();


        </script>

        <style type="text/css" media="screen">
        @import url("/ad_IES/css/styles.css");
        </style>
<meta name="KEYWORDS" content="carbon steels, low alloy steels, corrosion resistant stainless, high temperature stainless, heat resistant stainless steels, AISI steel alloy specifications, high strength steel alloys, tensile strength of steels, maraging, superalloy, lead alloy, magnesium, tin alloy, tungsten, zinc, precious metal, pure metallic element, TPE elastomer, dielectric constant, plastic dissipation factor, nylon glass transition temperature, ionomer vicat softening point, poisson's ratio, polyetherimide, fluoropolymer, silicone, polyester dielectric constant" /><meta name="DESCRIPTION" content="Searchable list of plastics, metals, and ceramics categorized into a fields like polypropylene, nylon, ABS, aluminum alloys, oxides, etc.  Search results are detailed data sheets with tensile strength, density, dielectric constant, dissipation factor, poisson's ratio, glass transition temperature, and Vicat softening point." /><meta name="ROBOTS" content="NOFOLLOW" /><style type="text/css">
	.ctl00_ContentMain_ucMatGroupTree_msTreeView_0 { text-decoration:none; }
	.ctl00_ContentMain_ucMatGroupTree_msTreeView_1 { color:Black; }
	.ctl00_ContentMain_ucMatGroupTree_msTreeView_2 { padding:0px 3px 0px 3px; }

</style></head>

<body onload="_onload();" onkeydown="return _onkeydown(event);">

<a id="Top"></a>

<div id="divSiteName" style="position:absolute; top:0; left:0 ;">
<a href="/index.aspx"><img src="/images/sitelive.gif" alt="" /></a>
</div>

<div id="divPopupOverlay" style="display:none;"></div>

<div id="divPopupAlert" class="popupAlert" style="display:none;">
        <table style="width:100%; height:100%; border-style:outset; border-width:5px; border-color:blue;" >
                <tr><th style=" height:25px; background-color:Maroon; font-size:14px; color:White; "><span id="spanPopupAlertTitle">System Message</span></th></tr>
                <tr><td style=" vertical-align:top; padding:3px; background-color:White;" id="tdPopupAlertMessage" ></td></tr>
                <tr><td style=" height:25px; text-align:center; background-color:Silver;"><input type="button" id="btnPopupAlert" onclick="matweb.alertClose();" style="" value="OK" /></td></tr>
        </table>
</div>

<form name="aspnetForm" method="post" action="MaterialGroupSearch.aspx" onsubmit="javascript:return WebForm_OnSubmit();" id="aspnetForm">
<div>
<input type="hidden" name="__EVENTTARGET" id="__EVENTTARGET" value="" />
<input type="hidden" name="__EVENTARGUMENT" id="__EVENTARGUMENT" value="" />
<input type="hidden" name="__LASTFOCUS" id="__LASTFOCUS" value="" />
<input type="hidden" name="ctl00_ContentMain_ucMatGroupTree_msTreeView_ExpandState" id="ctl00_ContentMain_ucMatGroupTree_msTreeView_ExpandState" value="ccccccnc" />
<input type="hidden" name="ctl00_ContentMain_ucMatGroupTree_msTreeView_SelectedNode" id="ctl00_ContentMain_ucMatGroupTree_msTreeView_SelectedNode" value="" />
<input type="hidden" name="ctl00_ContentMain_ucMatGroupTree_msTreeView_PopulateLog" id="ctl00_ContentMain_ucMatGroupTree_msTreeView_PopulateLog" value="" />
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="/wEPDwULLTE2Nzc3MDc2MDAPZBYCZg9kFgICAQ9kFioCAQ9kFgJmDxYCHgRUZXh0BWdEYXRhIHNoZWV0cyBmb3Igb3ZlciA8c3BhbiBjbGFzcz0iZ3JlZXRpbmciPjE4MCwwMDA8L3NwYW4+ICBtZXRhbHMsIHBsYXN0aWNzLCBjZXJhbWljcywgYW5kIGNvbXBvc2l0ZXMuZAICD2QWAmYPFgIfAAUESE9NRWQCAw9kFgJmDxYCHwAFBlNFQVJDSGQCBA9kFgJmDxYCHwAFBVRPT0xTZAIFD2QWAmYPFgIfAAUJU1VQUExJRVJTZAIGD2QWAmYPFgIfAAUHRk9MREVSU2QCBw9kFgJmDxYCHwAFCEFCT1VUIFVTZAIID2QWAmYPFgIfAAUDRkFRZAIJD2QWAmYPFgIfAAUHQUNDT1VOVGQCCg9kFgJmDxYCHwAFBkxPRyBJTmQCCw8QZGQWAWZkAgwPZBYCZg8WAh8ABQhTZWFyY2hlc2QCDQ9kFgJmDxYCHwAFCEFkdmFuY2VkZAIOD2QWAmYPFgIfAAUIQ2F0ZWdvcnlkAg8PZBYCZg8WAh8ABQhQcm9wZXJ0eWQCEA9kFgJmDxYCHwAFBk1ldGFsc2QCEQ9kFgJmDxYCHwAFClRyYWRlIE5hbWVkAhIPZBYCZg8WAh8ABQxNYW51ZmFjdHVyZXJkAhMPZBYCAgEPZBYCZg8WAh8ABRlSZWNlbnRseSBWaWV3ZWQgTWF0ZXJpYWxzZAIVD2QWEAIDD2QWAmYPFgIfAAWZATxhIGhyZWY9Ii9jbGlja3Rocm91Z2guYXNweD9hZGRhdGFpZD0xMzQ5Ij48aW1nIHNyYz0iL2ltYWdlcy9hc3NldHMvbWV0YWxtZW4tQWx1bWludW0ucG5nIiBib3JkZXI9MCBhbHQ9Ik1ldGFsbWVuIFNhbGVzIiB3aWR0aCA9ICI3MjgiIGhlaWdodCA9ICI5MCI+PC9hPmQCBA9kFgICAg8WAh4Fc3R5bGUFGWRpc3BsYXk6bm9uZTt3aWR0aDoyNTBweDsWBgIBD2QWAmYPZBYCZg8PFgIfAAUSVXNlciBJbnB1dCBQcm9ibGVtZGQCAw8PFgIfAGVkZAIFDxYCHgV2YWx1ZQUCT0tkAgUPZBYEAgEPD2QWAh8BBQxXaWR0aDozMDRweDtkAgIPEBYEHwEFK3Bvc2l0aW9uOmFic29sdXRlO2Rpc3BsYXk6bm9uZTtXaWR0aDozMDhweDseBHNpemUFAjE4ZGRkAgYPDxYEHghDbGllbnRJRAIBHgpMYW5ndWFnZUlEAgFkFgJmDxQrAAkPFg4eF1BvcHVsYXRlTm9kZXNGcm9tQ2xpZW50Zx4SRW5hYmxlQ2xpZW50U2NyaXB0Zx4JU2hvd0xpbmVzZx4ITm9kZVdyYXBnHg1OZXZlckV4cGFuZGVkZB4MU2VsZWN0ZWROb2RlZB4JTGFzdEluZGV4AghkFggeC05vZGVTcGFjaW5nGwAAAAAAAAAAAQAAAB4JRm9yZUNvbG9yCiMeEUhvcml6b250YWxQYWRkaW5nGwAAAAAAAAhAAQAAAB4EXyFTQgKEgBhkZGRkZGQUKwAJBSMyOjAsMDowLDA6MSwwOjIsMDozLDA6NCwwOjUsMDo2LDA6NxQrAAIWDB8ABRJDYXJib24gKDg2NiBtYXRscykeBVZhbHVlBQMyODMeCEV4cGFuZGVkaB4MU2VsZWN0QWN0aW9uCyouU3lzdGVtLldlYi5VSS5XZWJDb250cm9scy5UcmVlTm9kZVNlbGVjdEFjdGlvbgIeC05hdmlnYXRlVXJsBUFqYXZhc2NyaXB0OnVjTWF0R3JvdXBUcmVlX09uTm9kZUNsaWNrKCdDYXJib24gKDg2NiBtYXRscyknLCcyODMnKR4QUG9wdWxhdGVPbkRlbWFuZGdkFCsAAhYMHwAFFUNlcmFtaWMgKDEwMDA0IG1hdGxzKR8RBQIxMR8SaB8TCysEAh8UBUNqYXZhc2NyaXB0OnVjTWF0R3JvdXBUcmVlX09uTm9kZUNsaWNrKCdDZXJhbWljICgxMDAwNCBtYXRscyknLCcxMScpHxVnZBQrAAIWDB8ABRJGbHVpZCAoNzU2MiBtYXRscykfEQUBNR8SaB8TCysEAh8UBT9qYXZhc2NyaXB0OnVjTWF0R3JvdXBUcmVlX09uTm9kZUNsaWNrKCdGbHVpZCAoNzU2MiBtYXRscyknLCc1JykfFWdkFCsAAhYMHwAFE01ldGFsICgxNzA1MiBtYXRscykfEQUBOR8SaB8TCysEAh8UBUBqYXZhc2NyaXB0OnVjTWF0R3JvdXBUcmVlX09uTm9kZUNsaWNrKCdNZXRhbCAoMTcwNTIgbWF0bHMpJywnOScpHxVnZBQrAAIWDB8ABSdPdGhlciBFbmdpbmVlcmluZyBNYXRlcmlhbCAoODA2MyBtYXRscykfEQUDMjg1HxJoHxMLKwQCHxQFVmphdmFzY3JpcHQ6dWNNYXRHcm91cFRyZWVfT25Ob2RlQ2xpY2soJ090aGVyIEVuZ2luZWVyaW5nIE1hdGVyaWFsICg4MDYzIG1hdGxzKScsJzI4NScpHxVnZBQrAAIWDB8ABRVQb2x5bWVyICg5NzYzNSBtYXRscykfEQUCMTAfEmgfEwsrBAIfFAVDamF2YXNjcmlwdDp1Y01hdEdyb3VwVHJlZV9Pbk5vZGVDbGljaygnUG9seW1lciAoOTc2MzUgbWF0bHMpJywnMTAnKR8VZ2QUKwACFgwfAAUYUHVyZSBFbGVtZW50ICg1MDcgbWF0bHMpHxEFAzE4NB8SaB8TCysEAh8UBUdqYXZhc2NyaXB0OnVjTWF0R3JvdXBUcmVlX09uTm9kZUNsaWNrKCdQdXJlIEVsZW1lbnQgKDUwNyBtYXRscyknLCcxODQnKR8VaGQUKwACFgwfAAUlV29vZCBhbmQgTmF0dXJhbCBQcm9kdWN0cyAoMzk4IG1hdGxzKR8RBQMyNzcfEmgfEwsrBAIfFAVUamF2YXNjcmlwdDp1Y01hdEdyb3VwVHJlZV9Pbk5vZGVDbGljaygnV29vZCBhbmQgTmF0dXJhbCBQcm9kdWN0cyAoMzk4IG1hdGxzKScsJzI3NycpHxVnZGQCBw8WAh4JaW5uZXJodG1sBSY2MDAwIFNlcmllcyBBbHVtaW51bSBBbGxveSAoMTkyIG1hdGxzKWQCDQ8PFgIeB1Zpc2libGVoZGQCDg8PFgIfF2dkFgQCAQ8PFgIfAAUDMTk4ZGQCAw8PFgIfAAUaNjAwMCBTZXJpZXMgQWx1bWludW0gQWxsb3lkZAIPDw8WBh4LQ3VycmVudFBhZ2UCAR4PQ3VycmVudFNlYXJjaElEAr7G/BIfF2dkFgQCAg8PFgIfAGVkZAIDDw8WAh8XZ2QWPgIBDw8WAh8ABQMxOThkZAIDDxBkEBUBATEVAQExFCsDAWcWAWZkAgUPDxYCHwAFATFkZAIHDw8WCB8ABQtbUHJldiBQYWdlXR8OCk4eB0VuYWJsZWRoHxACBGRkAgkPDxYIHwAFC1tOZXh0IFBhZ2VdHw4KTh8QAgQfGmhkZAILDxBkZBYBAgVkAg0PFgIfF2hkAg8PFgIfF2hkAhEPEA9kFgIeCG9uY2hhbmdlBRNTZXRTZWxlY3RlZEZvbGRlcigpZGRkAhUPFgIfF2gWAgIBDxBkZBYBZmQCFw9kFgQCAQ8PFgQfAGUfGmhkZAICDw8WAh8XaGRkAhkPDxYEHwAFDU1hdGVyaWFsIE5hbWUfGmhkZAIbDw8WAh8XaGRkAh0PDxYCHxdoZGQCHw9kFgYCAQ8PFgQfAAUFUHJvcDEfGmhkZAIDDw8WAh8XaGRkAgUPDxYCHxdoZGQCIQ9kFgYCAQ8PFgQfAAUFUHJvcDIfGmhkZAIDDw8WAh8XaGRkAgUPDxYCHxdoZGQCIw9kFgYCAQ8PFgQfAAUFUHJvcDMfGmhkZAIDDw8WAh8XaGRkAgUPDxYCHxdoZGQCJQ9kFgYCAQ8PFgQfAAUFUHJvcDQfGmhkZAIDDw8WAh8XaGRkAgUPDxYCHxdoZGQCJw9kFgYCAQ8PFgQfAAUFUHJvcDUfGmhkZAIDDw8WAh8XaGRkAgUPDxYCHxdoZGQCKQ9kFgYCAQ8PFgQfAAUFUHJvcDYfGmhkZAIDDw8WAh8XaGRkAgUPDxYCHxdoZGQCKw9kFgYCAQ8PFgQfAAUFUHJvcDcfGmhkZAIDDw8WAh8XaGRkAgUPDxYCHxdoZGQCLQ9kFgYCAQ8PFgQfAAUFUHJvcDgfGmhkZAIDDw8WAh8XaGRkAgUPDxYCHxdoZGQCLw9kFgYCAQ8PFgQfAAUFUHJvcDkfGmhkZAIDDw8WAh8XaGRkAgUPDxYCHxdoZGQCMQ9kFgYCAQ8PFgQfAAUGUHJvcDEwHxpoZGQCAw8PFgIfF2hkZAIFDw8WAh8XaGRkAjUPDxYCHwAFAzE5OGRkAjcPEGQQFQEBMRUBATEUKwMBZxYBZmQCOQ8PFgIfAAUBMWRkAjsPDxYIHwAFC1tQcmV2IFBhZ2VdHw4KTh8aaB8QAgRkZAI9Dw8WCB8ABQtbTmV4dCBQYWdlXR8OCk4fEAIEHxpoZGQCPw8QZGQWAQIFZAJBDxYCHxdoFgICAQ8PFgIfAAW4BDxiciAvPg0KTWF0ZXJpYWxzIGZsYWdnZWQgYXMgZGlzY29udGludWVkICg8aW1nIHNyYz0iL2ltYWdlcy9idXR0b25zL2ljb25EaXNjb250aW51ZWQuanBnIiBhbHQ9IiIgLz4pIGFyZSBubyBsb25nZXIgcGFydCBvZiB0aGUgbWFudWZhY3R1cmVy4oCZcyBzdGFuZGFyZCBwcm9kdWN0IGxpbmUgYWNjb3JkaW5nIHRvIG91ciBsYXRlc3QgaW5mb3JtYXRpb24uICBUaGVzZSBtYXRlcmlhbHMgbWF5IGJlIGF2YWlsYWJsZSBieSBzcGVjaWFsIG9yZGVyLCBpbiBkaXN0cmlidXRpb24gaW52ZW50b3J5LCBvciByZWluc3RhdGVkIGFzIGFuIGFjdGl2ZSBwcm9kdWN0LiAgRGF0YSBzaGVldHMgZnJvbSBtYXRlcmlhbHMgdGhhdCBhcmUgbm8gbG9uZ2VyIGF2YWlsYWJsZSByZW1haW4gaW4gTWF0V2ViIHRvIGFzc2lzdCB1c2VycyBpbiBmaW5kaW5nIHJlcGxhY2VtZW50IG1hdGVyaWFscy4gIA0KPGJyIC8+PGJyIC8+DQpVc2VycyBvZiBvdXIgQWR2YW5jZWQgU2VhcmNoIChyZWdpc3RyYXRpb24gcmVxdWlyZWQpIG1heSBleGNsdWRlIGRpc2NvbnRpbnVlZCBtYXRlcmlhbHMgZnJvbSBzZWFyY2ggcmVzdWx0cy5kZAIWD2QWAmYPFgIfAAVwPGEgaHJlZj0iL2NsaWNrdGhyb3VnaC5hc3B4P2FkZGF0YWlkPTI3NyIgY2xhc3M9ImZvb3RsaW5rIj48c3BhbiBjbGFzcz0iZm9vdCI+VHJhZGUmbmJzcDtQdWJsaWNhdGlvbnM8L3NwYW4+PC9hPmQYAQUeX19Db250cm9sc1JlcXVpcmVQb3N0QmFja0tleV9fFgQFNmN0bDAwJENvbnRlbnRNYWluJFVjTWF0R3JvdXBGaW5kZXIxJHNlbGVjdENhdGVnb3J5TGlzdAUrY3RsMDAkQ29udGVudE1haW4kdWNNYXRHcm91cFRyZWUkbXNUcmVlVmlldwUbY3RsMDAkQ29udGVudE1haW4kYnRuU3VibWl0BRpjdGwwMCRDb250ZW50TWFpbiRidG5SZXNldAaPOBWWfUcKubHb5PucvLzUlOac" />
</div>

<script type="text/javascript">
//<![CDATA[
var theForm = document.forms['aspnetForm'];
if (!theForm) {
    theForm = document.aspnetForm;
}
function __doPostBack(eventTarget, eventArgument) {
    if (!theForm.onsubmit || (theForm.onsubmit() != false)) {
        theForm.__EVENTTARGET.value = eventTarget;
        theForm.__EVENTARGUMENT.value = eventArgument;
        theForm.submit();
    }
}
//]]>
</script>


<script src="/WebResource.axd?d=M68BSyLvwBMHRYzsSV62mvczr7w5VyKnq2JbgqcAAU1ioThlC6FNEVBncBdaA4oqf8cXPzy7QERzajvFfM67hSR6S0w1&amp;t=638313619312541215" type="text/javascript"></script>


<script src="/WebResource.axd?d=ClzPoKUn1iQnMGy7NeMJw70TvMayZ5w9nNmB7h_dY64i4BFFgjVcppyp69BFqhuzHctzc-rEantAjczzQ6UVMYkaWJw1&amp;t=638313619312541215" type="text/javascript"></script>
<script type="text/javascript">
//<![CDATA[

    function TreeView_PopulateNodeDoCallBack(context,param) {
        WebForm_DoCallback(context.data.treeViewID,param,TreeView_ProcessNodeData,context,TreeView_ProcessNodeData,false);
    }
var ctl00_ContentMain_ucMatGroupTree_msTreeView_Data = null;//]]>
</script>

<script src="/ScriptResource.axd?d=148LWaywfmset9PYVR0m-KYXX9JfyGYGqOtIYBGEAzoJh5VhFBwSI6JxnmoNvgtlAprhvntpc6lTDbRMgdKxckUmc8m2qw_fOCsX0G9LC8ojUpw4cTHWXgfuT_vU6m2Kpv3mt01iCczrTE2-J5X6EeBa2281&amp;t=637769794779625837" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=v_V1UgDrDAchg8Zuc1FL4qkvmjt-bwap7ZxwVGGPleYiJ6zfDOTL6wtCmVvILjbwo_sqb2PMUTNkjO4vp6nd-E7WWXTPgSI5-nvn3YDnHrVk5bgVX4Qv6F0fV4wTSslmaLZ3k98tckojDLWAEI7jm8h0H4d7BoGYxkqsyh8-Iujfrjjm0&amp;t=637769794779625837" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=V7Zma3AISNyGTbHVRPw8svJ4Ub7xUUaeoKwImcvnuDiy-XBR331l3eTjiEP8J_QlX2qgyAHxZFLywNbNyZ50fJLVEOCjx11lIDJOiQxc0oGA3qi7XXiggiwEcIWwzb9j2Is1n1D9sU40Zusk35AtkqRKL3Y1&amp;t=633177911400000000" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=wOguQo_yd3hCG4ajcSXPC6-XdeL0hmYWSMaFjvMXt1CBqR9neJqTXyTfPki4lIz4B29Zz_83btvS12BY6KpBKT-AgP0eaXp7V3h6XRdjiozV5w_n3oQRZg5Yq1QW8UNGj0Cy9jsap_7EcU2M99CapUyVG6I1&amp;t=633177911400000000" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=MAJxSy62Z3EtRwHXuuMc8KC6PFmrPqmkpcwdpcPGeqtqUQyJ3FBQ3cFyYWN1zNID_ngA53aMZdG_dpRu3TRDH8-iqKAjPAcc2BWe63WYGrGbyHc9syJxR6lypTgvRaYmwwEmkLEsaGUniMurHc16yr1VrjmzG3HWOeu9UXNr23Wv2xzh0&amp;t=633177911400000000" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=ukx9eyyt483y_CGgCF1OToQPWzPYyCynhpMvhxN7idzW3WaKjHjpgAOcMgtXZIEcspnbyDEADdRWCSHB-hF39Ycl13KWsqdlOHElw2qgSkGmkL8iNH37htb0BNS0lxw6oHvtqYiVpw-nZeYoQ1NkgX2qXOI1&amp;t=633177911400000000" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=Ovkr-MWDypjnq0C7FVbNgOs-Mt3NaktNVf8SEOX721PoQ5mQqTM6S4igSQL1_PXvh96km3stLiqrorOgm8nnnUU6y6edTkhxmLiwfFtEXQEAAnFFQpUepgsxEd0ahMvWcDfEac0EE7J8iDb2rqSMTxk51ydfY-c-8zcKvo5Mrn83cvfN0&amp;t=633177911400000000" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=pKQF4iQYwkSn5cAwTNDM8ez4TLepowWnai-WD6NVL5h0ay9Qtt0a9vPCaI-KN9OHcv1fL8VnwxEDM6EAVtgZY06V8XJTlVrbmb8WlMvGmhwBeL3_SVtKKOS-QExY8AnVbTFl_wOknplhy8bz-9sIWNCh4XPq1WhRj7ALNAKaeruPhS7d0&amp;t=633177911400000000" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=uM6eUuIuIa_NiCgVrtwgqczDeqTqI37XalY8Ri_W2ZjXEzIKi84afVBjFk8gneIDXcFnw8yjCD1qhE8mzDTzAUSdKnbDte0yC23kMoyvxPyu7vRH9E-6JMZtE8-gELUPmoZdOZJ9nXitHKYBLw2P6skU1IkzFRegK74bETu2FdEtuaeR0&amp;t=633177911400000000" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=ewF5FMb1ckbNCdJ9rpE5xMSXy-WecqVGE8uma1_520KusuW7jTUE2tnPMG7JyR0FDIFqDUMQbsfTZlsQ0sD4YAW8MnN90GKIMPvzaP7eOG3ss8E050_l7Rpnaz7I_U3ITzBXkcJZIOPcI1PxOReT8FSeWmo1&amp;t=633177911400000000" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=3lB_EBCMPyiwzDhzWdoE-i4mLHhkIMbbkaLwMr-U1WcAoDqBHS-oYFn4gz3ZwnpzK_KZp97Ev8ZbiUNo7WMJ45h2xgOH59erMFo1BpNb87-yu3X0mFyWs5rB3ANm4lsVSIu7RZd7XUEwfZXzugJ3CoJqxvRqiTGLpcXquEF24hK3OUH-0&amp;t=633177911400000000" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=Z7-pZSKJ6vCyr9H9S4r9baB7TLiJ-T4EoMOvlH-iQ5-icaO-vWMrjXk3EPctuPrcO5qwJSTaLQtHyguwle7Kty78exrSwHataIriNUEu1DnUkixv4pxU7Dknp_GDVHby5XcMA4lMwazAE0AFG9Cndu0An_gAGACGlYXsGUG9W6okCQsH0&amp;t=633177911400000000" type="text/javascript"></script>
<script src="../WebServices/Materials.asmx/js" type="text/javascript"></script>
<script src="../WebServices/MatGroups.asmx/js" type="text/javascript"></script>
<script src="../WebServices/Folders.asmx/js" type="text/javascript"></script>
<script type="text/javascript">
//<![CDATA[
function WebForm_OnSubmit() {
null;
return true;
}
//]]>
</script>

<div>

	<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="640EFF8C" />
</div>

<!--
The ScriptManager control is required for all ASP AJAX functionality - especially used by search pages.
It generates the javascript required.
Only one script manager is allowed on a page, and since this is the master page, this is the main script manager
However, a ScriptManagerProxy control can be used to set properties and settings on indivual pages.
For an example, see /WebControls/ucSearchResults.ascx
-->
<script type="text/javascript">
//<![CDATA[
Sys.WebForms.PageRequestManager._initialize('ctl00$ajxScriptManager', document.getElementById('aspnetForm'));
Sys.WebForms.PageRequestManager.getInstance()._updateControls([], [], [], 90);
//]]>
</script>


<table class="tabletight" style="width:100%; text-align:center;background-image: url(/images/gray.gif);">
  <tr >
    <td rowspan="2"><a href="/index.aspx"><img src="/images/logo1.gif" width="270" height="42" alt="MatWeb - Material Property Data"  /></a></td>

                
                <td style="text-align:right;vertical-align:middle; width:90%" ><span class='greeting'><span class='hiText'><a href ='/services/advertising.aspx'>Advertise with MatWeb!</a></span>&nbsp;&nbsp;&nbsp;&nbsp;</span></td>
                <td style="text-align:right;vertical-align:middle; padding-right:5px;" ><a href='/membership/regstart.aspx'><img src='/images/buttons/btnRegisterBSF.gif' width="62" height="20" alt="Register Now" /></a></td>

                
  </tr>
  <tr >
    <td  align="left" valign="bottom" colspan="2"><span class="tagline">
    
    Data sheets for over <span class="greeting">180,000</span>  metals, plastics, ceramics, and composites.</span></td>
  </tr>
  <tr style="background-image:url(/images/blue.gif); background-color:#000099; " >
    <td bgcolor="#000099"><a href="/index.aspx"><img src="/images/logo2.gif" width="270" height="23px" alt="MatWeb - Material Property Data" border="0" /></a></td>
    <td class="topnavig8" style=" background-color:#000099;vertical-align:middle; text-align:right; white-space:nowrap;" colspan="2" align="right" valign="middle">
        <a href="/index.aspx" class="topnavlink">HOME</a>&nbsp;&nbsp;&#8226;&nbsp;&nbsp;
                        <a href="/search/search.aspx" class="topnavlink">SEARCH</a>&nbsp;&nbsp;&#8226;&nbsp;&nbsp;
                        <a href="/tools/tools.aspx" class="topnavlink">TOOLS</a>&nbsp;&nbsp;&#8226;&nbsp;&nbsp;
                        <a href="/reference/suppliers.aspx" class="topnavlink">SUPPLIERS</a>&nbsp;&nbsp;&#8226;&nbsp;&nbsp;
                        <a href="/folders/ListFolders.aspx" class="topnavlink">FOLDERS</a>&nbsp;&nbsp;&#8226;&nbsp;&nbsp;
                        <a href="/services/services.aspx" class="topnavlink">ABOUT US</a>&nbsp;&nbsp;&#8226;&nbsp;&nbsp;
                        <a href="/reference/faq.aspx" class="topnavlink">FAQ</a>&nbsp;&nbsp;&#8226;&nbsp;&nbsp;
                        
                        <a href="/membership/login.aspx" class="topnavlink">LOG IN</a>
                        
                        &nbsp;
                        
                        &nbsp;
    </td>
  </tr>
</table>

<div id="divRecentMatls" class="tableborder tight" style=" display:none; position:absolute; top:95px; left:150px; width:400px; height:400px;" onmouseover="clearTimeout(tmrtmp);" onmouseout="tmrtmp=setTimeout('RecentMatls.Hide()', 800)">
        <table class="headerblock tabletight" style="vertical-align:top; font-size:13px; width:100%;"><tr>
                <td style="padding:3px;"> Recently Viewed Materials (most recent at top)</td>
                <td style="padding:3px;text-align:right;"><input type="image" src="/images/buttons/btnCloseWhite.gif" onclick="RecentMatls.Hide();return false;" style="cursor:pointer;" />&nbsp;</td>
        </tr></table>
        <div id="divRecentMatlsBody" style="height:375px; overflow:auto; background-color:White;">

                <table id="tblRecentMatls" class="tabledataformat tableloose" style="background-color:White;">
    
                <tr><td>
                <p />
                <a href="/membership/login.aspx">Login</a> to see your most recently viewed materials here.<p />
                Or if you don't have an account with us yet, then <a href="/membership/regstart.aspx">click here to register.</a>
                </td></tr>
                
                </table>

        </div>
</div>

<script type="text/javascript">

        var RecentMatls = new _RecentMatls();
        var tmrtmp;

        function _RecentMatls(){

                var divRecentMatls;
                var tblRecentMatls;
                var trRecentMatlsRowTemplate;
                var trRecentMatlsNoData;
                var DataIsLoaded=false;

                //Using prototype.js methods...
                divRecentMatls = $("divRecentMatls");
                tblRecentMatls = $("tblRecentMatls");
    

                divRecentMatls.style.display = "none";

                this.Toggle = function(){
                        //alert("Toggle()");
                        if(divRecentMatls.style.display == "block"){this.Hide();}
                        else{this.Show();}
                }

                this.Show = function(){
                        //alert("Show()");
                        divRecentMatls.style.display = "block";
                        matweb.toggleSelect(divRecentMatls,0);
                        if(!DataIsLoaded){
            
                        }
                }

                this.Hide = function(){
                        //alert("Hide()");
                        divRecentMatls.style.display = "none";
                        matweb.toggleSelect(divRecentMatls,1);
                }

    

        }//end class

</script>

<table class="tabletight t_ableborder t_ablegrid" style="width:100%;" >
  <tr>
   <td style="width:100%; height:27px; white-space:nowrap; text-align:left; vertical-align:middle;" class="tagline" >
      <span class="navlink"><b>&nbsp;&nbsp;Searches:</b></span>
      &nbsp;&nbsp;<a href="/search/AdvancedSearch.aspx" class="navlink"><span class="nav"><font color="#CC0000">Advanced</font></span></a>
      &nbsp;|&nbsp;<a href="/search/MaterialGroupSearch.aspx" class="navlink"><span class="nav">Category</span></a>
      &nbsp;|&nbsp;<a href="/search/PropertySearch.aspx" class="navlink"><span class="nav">Property</span></a>
      &nbsp;|&nbsp;<a href="/search/CompositionSearch.aspx" class="navlink"><span class="nav">Metals</span></a>
      &nbsp;|&nbsp;<a href="/search/SearchTradeName.aspx" class="navlink"><span class="nav">Trade Name</span></a>
      &nbsp;|&nbsp;<a href="/search/SearchManufacturerName.aspx" class="navlink"><span class="nav">Manufacturer</span></a>
                &nbsp;|&nbsp;<a href="javascript:RecentMatls.Toggle();" id="ctl00_lnkRecentMatls" class="navlink"><span class="nav"><font color="#CC0000">Recently Viewed Materials</font></span></a>
    </td>

    <td style="text-align:right; vertical-align:bottom; padding-right:5px;">

      <table class="tabletight t_ableborder" ><tr>
        <td style=" white-space:nowrap; vertical-align:bottom; " >
                                        &nbsp;&nbsp;
                                        <input name="ctl00$txtQuickText" type="text" id="ctl00_txtQuickText" style="width:100px;" maxlength="40" class="quicksearchinput" onkeydown="txtQuickText_OnKeyDown(event);" />
                                        <input type="image" id="btnQuickTextSearch" style="vertical-align:top; width:60px; height:25px;" src="/images/buttons/btnSearch.gif" alt="Search" onclick="return btnQuickTextSearch_Click()"  />
                                        <script type="text/javascript">

                                                //SETUP THE AUTOPOST ON ENTER KEY EVENT FOR txtQuickText FIELD
                                                //document.getElementById("ctl00_txtQuickText").onkeydown=txtQuickText_OnKeyDown;

                                                function txtQuickText_OnKeyDown(evt){
                                                        //SUBMIT THE FORM AS IF THE BUTTON WAS PUSHED
                                                        //var UniqueID = "";
                                                        var txtQuickText = document.getElementById('ctl00_txtQuickText');
                                                        if(matweb.IsEnterKey(evt))
                                                                if(checkSearchText(txtQuickText))
                                                                        //matweb.DoPostBack(UniqueID, "");
                                                                        window.location.href="/search/QuickText.aspx?SearchText=" + escape(txtQuickText.value);
                                                        return false;
                                                }

                                                function btnQuickTextSearch_Click(){
                                                        var txtQuickText = document.getElementById('ctl00_txtQuickText');
                                                        if(checkSearchText(txtQuickText))
                                                                window.location.href="/search/QuickText.aspx?SearchText=" + escape(txtQuickText.value);
                                                        return false;
                                                }

                                                function checkSearchText(searchbox){
                                                        searchbox.value = trim(searchbox.value);
                                                        if(searchbox.value == ''){
                                                                //alert('Please enter something to search for');
                                                                matweb.alert('Please enter something to search for', 'Search Error');
                                                                //searchbox.focus();
                                                                return false;
                                                        }
                                                        return true;
                                                }

                                                function trim(s)  { return ltrim(rtrim(s)) }
                                                function ltrim(s) { return s.replace(/^\s+/g, "") }
                                                function rtrim(s) { return s.replace(/\s+$/g, "") }

                                        </script>
                                </td>
                                </tr>
                        </table>

    </td>
  </tr>
  <tr style="background-image:url(/images/shad.gif);height:7px;" ><td colspan="2"></td></tr>
</table>

<!-- ====================================== MAIN CONTENT ============================================= -->
<div style="vertical-align:top; padding-left:10px; padding-right:10px;" >





	<center><a href="/clickthrough.aspx?addataid=1349"><img src="/images/assets/metalmen-Aluminum.png" border=0 alt="Metalmen Sales" width = "728" height = "90"></a>
</center>
	
	<h2><a href="#help"><img src="/images/buttons/iconHelp.gif" alt="" style="vertical-align:bottom;" /></a>Material Category Search</h2>
	
	

<div id="ctl00_ContentMain_ucPopupMessage1_divPopupMessage" class="divPopupAlert" style="display:none;width:250px;">
	<table >
		<tr id="ctl00_ContentMain_ucPopupMessage1_trTitleRow" class="divPopupAlert_TitleRow" style="cursor:move;">
	<th><span id="ctl00_ContentMain_ucPopupMessage1_lblTitle">User Input Problem</span></th>
</tr>

		<tr class="divPopupAlert_ContentRow" ><td style="" ><span id="ctl00_ContentMain_ucPopupMessage1_lblMessage"></span></td></tr>
		<tr class="divPopupAlert_ButtonRow"><td><input name="ctl00$ContentMain$ucPopupMessage1$btnOK" type="button" id="ctl00_ContentMain_ucPopupMessage1_btnOK" value="OK" /></td></tr>
	</table>
</div>

<input type="hidden" name="ctl00$ContentMain$ucPopupMessage1$hndPopupControl" id="ctl00_ContentMain_ucPopupMessage1_hndPopupControl" />


	<table class="" style="width:100%;">
	<tr>
		<td style=" vertical-align:top; width:300px;" >
			<strong>Find a Material Category:</strong>
			

<input name="ctl00$ContentMain$UcMatGroupFinder1$txtSearchText" type="text" value="alum" id="ctl00_ContentMain_UcMatGroupFinder1_txtSearchText" style="Width:304px;" /><br />
<select name="ctl00$ContentMain$UcMatGroupFinder1$selectCategoryList" id="ctl00_ContentMain_UcMatGroupFinder1_selectCategoryList" style="position:absolute;display:none;Width:308px;" size="18">
</select>

<input type="hidden" name="ctl00$ContentMain$UcMatGroupFinder1$TextBoxWatermarkExtender2_ClientState" id="ctl00_ContentMain_UcMatGroupFinder1_TextBoxWatermarkExtender2_ClientState" />

<script type="text/javascript">

var ucMatGroupFinderManager = new _ucMatGroupFinderManager("ctl00_ContentMain_UcMatGroupFinder1_txtSearchText","ctl00_ContentMain_UcMatGroupFinder1_selectCategoryList");

function _ucMatGroupFinderManager(txtSearchTextID,selectCategoryListID){

	var txtSearchText = document.getElementById(txtSearchTextID);
	var selectCategoryList = document.getElementById(selectCategoryListID);
	var showtime;
	var blurtime;
	var MinTextLength = 4;
	var KeyStrokeActionDelay = 650;
	var FadeInOutDelay = 0.2;
	var LastUsedText;
	
	//The init function is currently run at the bottom of this class
	this.init=function(){

		txtSearchText.onfocus = txtSearchText_onfocus;
		txtSearchText.onkeyup = txtSearchText_checktext;
		txtSearchText.onblur = selectCategoryList_startblur;
		
		selectCategoryList.onfocus = selectCategoryList_clearblur;
		selectCategoryList.onchange = selectCategoryList_onchange;
		selectCategoryList.onblur = selectCategoryList_startblur;

	}

	txtSearchText_onfocus = function(){
		if(txtSearchText.value == "Type at least 4 characters here...")
			txtSearchText.value = "";
		selectCategoryList_clearblur();
		txtSearchText_checktext();
	}

	function txtSearchText_checktext(){
		searchText = txtSearchText.value;
		//alert(searchText);
		clearTimeout(showtime);
		if(searchText.length >= MinTextLength){
			showtime = setTimeout(function(){
				selectCategoryList.style.display = "block";
				if(txtSearchText.value!=LastUsedText){
					LastUsedText = searchText;
					aci.matweb.WebServices.MatGroups.FindMatGroups(searchText,DisplayCatData,matweb.WebServiceErrorHandler,"txtSearchText_checktext");
				}
				//AjaxControlToolkit.Animation.FadeInAnimation.play(selectCategoryList, FadeInOutDelay, 1, 25, 0, 1, true);
			},KeyStrokeActionDelay);
		}
		else{
			LastUsedText = "";
			showtime = setTimeout(function(){
				selectCategoryList.options.length = 0;
				selectCategoryList.style.display = "none";
				//AjaxControlToolkit.Animation.FadeOutAnimation.play(selectCategoryList,FadeInOutDelay);
				//AjaxControlToolkit.Animation.FadeOutAnimation.play(selectCategoryList,FadeInOutDelay, 25, 0, 1, true);
			},KeyStrokeActionDelay);
		}
	}

	function selectCategoryList_clearblur(){
		clearTimeout(blurtime);
	}

	function selectCategoryList_onchange(){
		var MatGroupID = selectCategoryList.options[selectCategoryList.selectedIndex].value;
		var MatGroupText = selectCategoryList.options[selectCategoryList.selectedIndex].text;
		//alert("Selected MatGroupID: " + MatGroupID);
		//alert("Selected MatGroupText: " + MatGroupText);
		//When no data is found, we have a "No Categories Contain Text..." message, but the MatGroupID is set to 0, so we can ignore if MatGroupID == 0.
		if(MatGroupID > 0){
			ucMatGroupFinder1_OnSelected(MatGroupID,MatGroupText)
		}
		selectCategoryList_startblur();
		//txtSearchText.value = "";
	}
	
	function selectCategoryList_startblur(){
		//alert("selectCategoryList_onblur()");
		clearTimeout(blurtime);
		blurtime = setTimeout(function(){
			selectCategoryList.style.display = "none";
			//AjaxControlToolkit.Animation.FadeOutAnimation.play(selectCategoryList,FadeInOutDelay, 25, 0, 1, true);
		},KeyStrokeActionDelay);
	}
	
	function DisplayCatData(MatGroupData){
		//alert("GetCatData()");
		//alert("vwMaterialGroupList: " + vwMaterialGroupList);
		var MatGroupID,GroupName, MatCount;
		selectCategoryList.options.length = 0;//clear any existing options....
		if(MatGroupData.length==0){
				//alert(vwMaterialGroupList[i].GroupName);
				MatGroupID = 0;
				GroupName = "No categories contain the text: " + txtSearchText.value
				selectCategoryList.options[0] = new Option(GroupName,MatGroupID);					
		}
		else{
			for(i=0;i<MatGroupData.length;i++){
				//alert(vwMaterialGroupList[i].GroupName);
				MatCount = MatGroupData[i].MatCount;
				MatGroupID = MatGroupData[i].MatGroupID;
				GroupName = MatGroupData[i].GroupName + " (" + MatCount + " matls)";
				selectCategoryList.options[i] = new Option(GroupName,MatGroupID);					
			}
		}
	}
	
	this.init();

}//end class
	
</script>



			<strong>Select a Material Category:</strong>
			
			<div class="tableborder" style=" height:225px; width:300px; overflow:scroll; ">
			<a href="#ctl00_ContentMain_ucMatGroupTree_msTreeView_SkipLink"><img alt="Skip Navigation Links." src="/WebResource.axd?d=vAkieGj3ugBpu6CKp7dqvCP5iHLzPbUd2XME-h1vQcC6d5hLg7Vc1kNVzB8A45Ui3K9XZ9m3-E2pxsaJQYh2uMkAul41&amp;t=638313619312541215" width="0" height="0" style="border-width:0px;" /></a><div id="ctl00_ContentMain_ucMatGroupTree_msTreeView">
	<table cellpadding="0" cellspacing="0" style="border-width:0;">
		<tr>
			<td><a id="ctl00_ContentMain_ucMatGroupTree_msTreeViewn0" href="javascript:TreeView_PopulateNode(ctl00_ContentMain_ucMatGroupTree_msTreeView_Data,0,document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewn0'),document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewt0'),null,'r','Carbon (866 matls)','283','f','','f')"><img src="/WebResource.axd?d=6v5qM6VKQYD3XNwwXEy7Tix7dzVdqDJ8QTRIlUPrzXQqNsXQgUwIfmn34ZzRORd_IZj0bF4vkZzID1HuSFE5uKyLgaPYeAYE8VT8HYfb3Rk6zsDw0&amp;t=638313619312541215" alt="Expand Carbon (866 matls)" style="border-width:0;" /></a></td><td class="ctl00_ContentMain_ucMatGroupTree_msTreeView_2"><a class="ctl00_ContentMain_ucMatGroupTree_msTreeView_0 ctl00_ContentMain_ucMatGroupTree_msTreeView_1" href="javascript:ucMatGroupTree_OnNodeClick('Carbon (866 matls)','283')" id="ctl00_ContentMain_ucMatGroupTree_msTreeViewt0">Carbon (866 matls)</a></td>
		</tr><tr style="height:0px;">
			<td></td>
		</tr>
	</table><table cellpadding="0" cellspacing="0" style="border-width:0;">
		<tr style="height:0px;">
			<td></td>
		</tr><tr>
			<td><a id="ctl00_ContentMain_ucMatGroupTree_msTreeViewn1" href="javascript:TreeView_PopulateNode(ctl00_ContentMain_ucMatGroupTree_msTreeView_Data,1,document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewn1'),document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewt1'),null,'t','Ceramic (10004 matls)','11','f','','f')"><img src="/WebResource.axd?d=TinC6igX1vPDgWh7DstWwBTHTyH4uuybDU4NW47jJD06wHdAO3YyJC_eKK1Zn5oIbwqRo4Cri8YwhA82G-LD0p14yEF1EfoIZvKleYMIuQ2K1hkE0&amp;t=638313619312541215" alt="Expand Ceramic (10004 matls)" style="border-width:0;" /></a></td><td class="ctl00_ContentMain_ucMatGroupTree_msTreeView_2"><a class="ctl00_ContentMain_ucMatGroupTree_msTreeView_0 ctl00_ContentMain_ucMatGroupTree_msTreeView_1" href="javascript:ucMatGroupTree_OnNodeClick('Ceramic (10004 matls)','11')" id="ctl00_ContentMain_ucMatGroupTree_msTreeViewt1">Ceramic (10004 matls)</a></td>
		</tr><tr style="height:0px;">
			<td></td>
		</tr>
	</table><table cellpadding="0" cellspacing="0" style="border-width:0;">
		<tr style="height:0px;">
			<td></td>
		</tr><tr>
			<td><a id="ctl00_ContentMain_ucMatGroupTree_msTreeViewn2" href="javascript:TreeView_PopulateNode(ctl00_ContentMain_ucMatGroupTree_msTreeView_Data,2,document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewn2'),document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewt2'),null,'t','Fluid (7562 matls)','5','f','','f')"><img src="/WebResource.axd?d=TinC6igX1vPDgWh7DstWwBTHTyH4uuybDU4NW47jJD06wHdAO3YyJC_eKK1Zn5oIbwqRo4Cri8YwhA82G-LD0p14yEF1EfoIZvKleYMIuQ2K1hkE0&amp;t=638313619312541215" alt="Expand Fluid (7562 matls)" style="border-width:0;" /></a></td><td class="ctl00_ContentMain_ucMatGroupTree_msTreeView_2"><a class="ctl00_ContentMain_ucMatGroupTree_msTreeView_0 ctl00_ContentMain_ucMatGroupTree_msTreeView_1" href="javascript:ucMatGroupTree_OnNodeClick('Fluid (7562 matls)','5')" id="ctl00_ContentMain_ucMatGroupTree_msTreeViewt2">Fluid (7562 matls)</a></td>
		</tr><tr style="height:0px;">
			<td></td>
		</tr>
	</table><table cellpadding="0" cellspacing="0" style="border-width:0;">
		<tr style="height:0px;">
			<td></td>
		</tr><tr>
			<td><a id="ctl00_ContentMain_ucMatGroupTree_msTreeViewn3" href="javascript:TreeView_PopulateNode(ctl00_ContentMain_ucMatGroupTree_msTreeView_Data,3,document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewn3'),document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewt3'),null,'t','Metal (17052 matls)','9','f','','f')"><img src="/WebResource.axd?d=TinC6igX1vPDgWh7DstWwBTHTyH4uuybDU4NW47jJD06wHdAO3YyJC_eKK1Zn5oIbwqRo4Cri8YwhA82G-LD0p14yEF1EfoIZvKleYMIuQ2K1hkE0&amp;t=638313619312541215" alt="Expand Metal (17052 matls)" style="border-width:0;" /></a></td><td class="ctl00_ContentMain_ucMatGroupTree_msTreeView_2"><a class="ctl00_ContentMain_ucMatGroupTree_msTreeView_0 ctl00_ContentMain_ucMatGroupTree_msTreeView_1" href="javascript:ucMatGroupTree_OnNodeClick('Metal (17052 matls)','9')" id="ctl00_ContentMain_ucMatGroupTree_msTreeViewt3">Metal (17052 matls)</a></td>
		</tr><tr style="height:0px;">
			<td></td>
		</tr>
	</table><table cellpadding="0" cellspacing="0" style="border-width:0;">
		<tr style="height:0px;">
			<td></td>
		</tr><tr>
			<td><a id="ctl00_ContentMain_ucMatGroupTree_msTreeViewn4" href="javascript:TreeView_PopulateNode(ctl00_ContentMain_ucMatGroupTree_msTreeView_Data,4,document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewn4'),document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewt4'),null,'t','Other Engineering Material (8063 matls)','285','f','','f')"><img src="/WebResource.axd?d=TinC6igX1vPDgWh7DstWwBTHTyH4uuybDU4NW47jJD06wHdAO3YyJC_eKK1Zn5oIbwqRo4Cri8YwhA82G-LD0p14yEF1EfoIZvKleYMIuQ2K1hkE0&amp;t=638313619312541215" alt="Expand Other Engineering Material (8063 matls)" style="border-width:0;" /></a></td><td class="ctl00_ContentMain_ucMatGroupTree_msTreeView_2"><a class="ctl00_ContentMain_ucMatGroupTree_msTreeView_0 ctl00_ContentMain_ucMatGroupTree_msTreeView_1" href="javascript:ucMatGroupTree_OnNodeClick('Other Engineering Material (8063 matls)','285')" id="ctl00_ContentMain_ucMatGroupTree_msTreeViewt4">Other Engineering Material (8063 matls)</a></td>
		</tr><tr style="height:0px;">
			<td></td>
		</tr>
	</table><table cellpadding="0" cellspacing="0" style="border-width:0;">
		<tr style="height:0px;">
			<td></td>
		</tr><tr>
			<td><a id="ctl00_ContentMain_ucMatGroupTree_msTreeViewn5" href="javascript:TreeView_PopulateNode(ctl00_ContentMain_ucMatGroupTree_msTreeView_Data,5,document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewn5'),document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewt5'),null,'t','Polymer (97635 matls)','10','f','','f')"><img src="/WebResource.axd?d=TinC6igX1vPDgWh7DstWwBTHTyH4uuybDU4NW47jJD06wHdAO3YyJC_eKK1Zn5oIbwqRo4Cri8YwhA82G-LD0p14yEF1EfoIZvKleYMIuQ2K1hkE0&amp;t=638313619312541215" alt="Expand Polymer (97635 matls)" style="border-width:0;" /></a></td><td class="ctl00_ContentMain_ucMatGroupTree_msTreeView_2"><a class="ctl00_ContentMain_ucMatGroupTree_msTreeView_0 ctl00_ContentMain_ucMatGroupTree_msTreeView_1" href="javascript:ucMatGroupTree_OnNodeClick('Polymer (97635 matls)','10')" id="ctl00_ContentMain_ucMatGroupTree_msTreeViewt5">Polymer (97635 matls)</a></td>
		</tr><tr style="height:0px;">
			<td></td>
		</tr>
	</table><table cellpadding="0" cellspacing="0" style="border-width:0;">
		<tr style="height:0px;">
			<td></td>
		</tr><tr>
			<td><img src="/WebResource.axd?d=7_V5n1KYSQsj6PoSqgIxb0egBsAa91jSXPkbmM6FaiF14-JsNnpUDnfSdxZcvfd0OQCLDtC24pMIXigO-41cL2rsq-0xea-SSG7yZUyRcliFFqe70&amp;t=638313619312541215" alt="" /></td><td class="ctl00_ContentMain_ucMatGroupTree_msTreeView_2"><a class="ctl00_ContentMain_ucMatGroupTree_msTreeView_0 ctl00_ContentMain_ucMatGroupTree_msTreeView_1" href="javascript:ucMatGroupTree_OnNodeClick('Pure Element (507 matls)','184')" id="ctl00_ContentMain_ucMatGroupTree_msTreeViewt6">Pure Element (507 matls)</a></td>
		</tr><tr style="height:0px;">
			<td></td>
		</tr>
	</table><table cellpadding="0" cellspacing="0" style="border-width:0;">
		<tr style="height:0px;">
			<td></td>
		</tr><tr>
			<td><a id="ctl00_ContentMain_ucMatGroupTree_msTreeViewn7" href="javascript:TreeView_PopulateNode(ctl00_ContentMain_ucMatGroupTree_msTreeView_Data,7,document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewn7'),document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewt7'),null,'l','Wood and Natural Products (398 matls)','277','f','','t')"><img src="/WebResource.axd?d=vUBrM2nNhECNbFO0AJKq52p2zWrY5XG8nWgCDL4AfsrexStb0HREDV6GHMvdkAGDInxYtTyWXuxFUIiQv3JX2fScVRwQsrVv5srHrjOIeC3ayAG20&amp;t=638313619312541215" alt="Expand Wood and Natural Products (398 matls)" style="border-width:0;" /></a></td><td class="ctl00_ContentMain_ucMatGroupTree_msTreeView_2"><a class="ctl00_ContentMain_ucMatGroupTree_msTreeView_0 ctl00_ContentMain_ucMatGroupTree_msTreeView_1" href="javascript:ucMatGroupTree_OnNodeClick('Wood and Natural Products (398 matls)','277')" id="ctl00_ContentMain_ucMatGroupTree_msTreeViewt7">Wood and Natural Products (398 matls)</a></td>
		</tr><tr style="height:0px;">
			<td></td>
		</tr>
	</table>
</div><a id="ctl00_ContentMain_ucMatGroupTree_msTreeView_SkipLink"></a>
<span id="ctl00_ContentMain_ucMatGroupTree_lblDebug"></span>

			</div>
			
			<strong>Selected Material Category: </strong>
			<div id="ctl00_ContentMain_divMatGroupName" class="selectedItem" style="width:306px;">6000 Series Aluminum Alloy (192 matls)</div>
			<input name="ctl00$ContentMain$txtMatGroupID" type="hidden" id="ctl00_ContentMain_txtMatGroupID" value="206" />
			<input name="ctl00$ContentMain$txtMatGroupText" type="hidden" id="ctl00_ContentMain_txtMatGroupText" value="6000 Series Aluminum Alloy (192 matls)" />
			
			

			<input type="image" name="ctl00$ContentMain$btnSubmit" id="ctl00_ContentMain_btnSubmit" src="../images/buttons/btnFind.gif" alt="Find" style="border-width:0px;" /> <!-- OnClientClick="return validate()" -->
			<input type="image" name="ctl00$ContentMain$btnReset" id="ctl00_ContentMain_btnReset" title="Clear Search" src="/images/buttons/btnReset.gif" alt="Clear Search" style="border-width:0px;" />

			<span id="ctl00_ContentMain_lblDebug"></span>

		</td>
		<td>&nbsp;</td>
		<td style=" vertical-align:top; width:80%;" >
			<strong>Search Results</strong>
			<div class="tableborder" id="divSearchResults" style="padding:5px;" >
				
				<div id="ctl00_ContentMain_pnlSearchResults">
	
					There are <span id="ctl00_ContentMain_lblMatlCount">198</span> materials in the category 
					<span id="ctl00_ContentMain_lblMaterialGroupName" class="selectedItem">6000 Series Aluminum Alloy</span>.
					If your material is not listed, please refer to our <a href="/help/strategy.aspx">search strategy</a> page for assistance in limiting your search.
				
</div>
			</div>
			<p />




<div id="ctl00_ContentMain_UcSearchResults1_pnlResultsFound" class="tableborder">
	

	<table style="background-color:Silver; width:100%;" class="" >
		<tr>
			<td><strong>Found <span id="ctl00_ContentMain_UcSearchResults1_lblResultCount">198</span> Results</strong> -- 
				Page 
				<select name="ctl00$ContentMain$UcSearchResults1$drpPageSelect1" onchange="javascript:setTimeout('__doPostBack(\'ctl00$ContentMain$UcSearchResults1$drpPageSelect1\',\'\')', 0)" id="ctl00_ContentMain_UcSearchResults1_drpPageSelect1">
		<option selected="selected" value="1">1</option>

	</select>
				of <span id="ctl00_ContentMain_UcSearchResults1_lblPageTotal">1</span> 
				-- 
				<a id="ctl00_ContentMain_UcSearchResults1_lnkPrevPage" disabled="disabled" style="color:Gray;">[Prev Page]</a>
				<a id="ctl00_ContentMain_UcSearchResults1_lnkNextPage" disabled="disabled" style="color:Gray;">[Next Page]</a>
				--&nbsp;			
				view
				<select name="ctl00$ContentMain$UcSearchResults1$drpPageSize1" onchange="javascript:setTimeout('__doPostBack(\'ctl00$ContentMain$UcSearchResults1$drpPageSize1\',\'\')', 0)" id="ctl00_ContentMain_UcSearchResults1_drpPageSize1">
		<option value="25">25</option>
		<option value="50">50</option>
		<option value="75">75</option>
		<option value="100">100</option>
		<option value="150">150</option>
		<option selected="selected" value="200">200</option>

	</select> per page
			</td>
		</tr>
		
	</table>
	
	
	
	
	<table class="t_ablegrid" style="width:100%;">
		<tr style="font-size:9px;">
			<td style="font-size:9px;">Use Folder</td>
			<td style="font-size:9px;">Contains</td>
			<td style="font-size:9px;">&nbsp;</td>
			
			<td style="font-size:9px;"><div id="divFolderName" style="display:none;">New Folder Name</div></td>
			<td style="font-size:9px;"></td>
			<td style="width:90%"></td>
		</tr>
		<tr style="vertical-align:top;">
			<td style="vertical-align:top;">
				<select name="ctl00$ContentMain$UcSearchResults1$drpFolderList" id="ctl00_ContentMain_UcSearchResults1_drpFolderList" onchange="SetSelectedFolder()" style="width:125px;">
		<option selected="selected" value="0">My Folder</option>

	</select>
			</td>
			<td style="vertical-align:top;">
				<strong><input name="ctl00$ContentMain$UcSearchResults1$txtFolderMatCount" type="text" id="ctl00_ContentMain_UcSearchResults1_txtFolderMatCount" readonly="readonly" style="border:none 0 white; background-color:White; color:Black; width: 35px; font-weight:bold;" value="0/0" /></strong>
			</td>
			<td>
				
				<input type="image" id="btnCompareFolders" src="/images/buttons/btnCompareMatls.gif" alt="Compare Checked Materials" onclick="CompareMaterials();return false;" />
			</td>
			
			<td style="white-space:nowrap;vertical-align:top;">
			
				<table id="tblFolderName" class="tabletight" style="display:none;"><tr><td><input type="text" id="txtFolderName2" style="display:inline; width:100px;" /><br /></td>
				<td><input type="image" id="btnApplyFolderName2" src="/images/buttons/btnSaveChanges.gif" style="display:inline;" onclick="ApplyFolderName();return false;" />
				<input type="image" id="btnHideFolderNameControls2" src="/images/buttons/btnCancel.gif" style="display:inline;" onclick="HideFolderNameControls();return false;" /></td>
				</tr></table>
				
			</td>
			<td style="white-space:nowrap;">
				<div class="usermessage" id="divUserMessage"></div>
			</td>
		</tr>
	</table>

	<table class="tabledataformat t_ablegrid" id="tblResults" style="table-layout:auto;"  >

		<tr class="">

			
		
			<th style="width:65px; white-space:nowrap;">Select</th>

			<th style="width:10px;">
				<!--Discontinued and Found Via Interpolated Indicators -->
			</th>
			
			<th style="width:auto;">
				<a id="ctl00_ContentMain_UcSearchResults1_lnkSortMatName" disabled="disabled" title="Click to Sort By Material Name">Material Name</a>
				<br />
				
				
			</th>
		
			
			
			

			

			

			

			

			

			

			

			
		</tr>

		<!-- this is where the search results are actually loaded.  See code behind -->		
		<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_81687"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;1</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_81687' href="/search/DataSheet.aspx?MatGUID=26d19f2d20654a489aefc0d9c247cebf" >Overview of materials for 6000 Series Aluminum Alloy</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9327"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;2</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9327' href="/search/DataSheet.aspx?MatGUID=a7339d57d08b49c886a7295702d3dadd" >6002 Aluminum Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9328"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;3</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9328' href="/search/DataSheet.aspx?MatGUID=0fc1a734aae0489cb7718e75a47e82c1" >6003 Aluminum Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9330"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;4</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9330' href="/search/DataSheet.aspx?MatGUID=2fb88f5c03054675bca1529cc987e17f" >6004 Aluminum Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9331"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;5</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9331' href="/search/DataSheet.aspx?MatGUID=2f1679ed1ada446c8f329a76c72a701a" >Aluminum 6005A Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9332"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;6</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9332' href="/search/DataSheet.aspx?MatGUID=e826b33ef1bf4ba8855a80b096082c96" >Aluminum 6005B Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_160193"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;7</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_160193' href="/search/DataSheet.aspx?MatGUID=353a0606aa07453aaaca464796904c6f" >Aluminum 6005C Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9333"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;8</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9333' href="/search/DataSheet.aspx?MatGUID=5549c15257814a918aaa4fb2edfa70a7" >Aluminum 6005-T1</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9334"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;9</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9334' href="/search/DataSheet.aspx?MatGUID=662c43b0f45043db9e0a4a852d34bf96" >Aluminum 6005-T5</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9336"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;10</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9336' href="/search/DataSheet.aspx?MatGUID=e930e85fde5948418a6f6b39258bbcc9" >6006 Aluminum Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9337"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;11</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9337' href="/search/DataSheet.aspx?MatGUID=10e81dbe36ba4bc1814f39fd13413c81" >6007 Aluminum Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9338"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;12</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9338' href="/search/DataSheet.aspx?MatGUID=bc65a9ff80cc4954bbf0fb2943d7068f" >6008 Aluminum Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9339"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;13</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9339' href="/search/DataSheet.aspx?MatGUID=d79f2942f0ba4501b75b693d7e3cd971" >Aluminum 6009-T4</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9340"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;14</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9340' href="/search/DataSheet.aspx?MatGUID=2315d8484ea24501a2663083670917ad" >Aluminum 6009-T6</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9342"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;15</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9342' href="/search/DataSheet.aspx?MatGUID=7e21d94ac56e41f9b38092df79738278" >Aluminum 6010-T4</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9343"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;16</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9343' href="/search/DataSheet.aspx?MatGUID=60a5b9f6fefb4ab0afe031a606f59c70" >6011 Aluminum Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9344"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;17</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9344' href="/search/DataSheet.aspx?MatGUID=0c6ca7c45de84160b6f2dd380e1f42e5" >6012 Aluminum Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9345"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;18</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9345' href="/search/DataSheet.aspx?MatGUID=46c6987d8d4a4ec180082e90952a5d16" >Aluminum 6012A Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_133710"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;19</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_133710' href="/search/DataSheet.aspx?MatGUID=30c783afc390492f9dfdcc36315f0fc0" >Aluminum 6013-T651</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_133711"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;20</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_133711' href="/search/DataSheet.aspx?MatGUID=0f8c759f0ac34e29bdc7aba77937d637" >Aluminum 6013-T8</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9349"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;21</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9349' href="/search/DataSheet.aspx?MatGUID=2995f4ac0ff84c219ae7e44716057e75" >6014 Aluminum Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9350"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;22</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9350' href="/search/DataSheet.aspx?MatGUID=899580e82afa431eb150b96bdfd0bcd9" >6015 Aluminum Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9352"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;23</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9352' href="/search/DataSheet.aspx?MatGUID=508e67d81bac44e29fefacc493384dd1" >Aluminum 6016A Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9353"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;24</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9353' href="/search/DataSheet.aspx?MatGUID=f6ed02219a874e868afa00abf4967314" >6016 Aluminum Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9355"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;25</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9355' href="/search/DataSheet.aspx?MatGUID=b35faf5223a84ec6802cb8ae68e7bb4b" >6018 Aluminum Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9357"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;26</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9357' href="/search/DataSheet.aspx?MatGUID=0e7c6d2ecef245e99632fb5a2dd7d839" >6019 Aluminum Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_133636"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;27</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_133636' href="/search/DataSheet.aspx?MatGUID=1fe8f51f301e4f4c8fc59be25c210879" >Aluminum 6020-T651</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_133637"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;28</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_133637' href="/search/DataSheet.aspx?MatGUID=e533a578c4df479f949f5f9c1701fbe0" >Aluminum 6020-T8</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_133638"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;29</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_133638' href="/search/DataSheet.aspx?MatGUID=9d32bcddeb7342d5b4d76fdb599073c0" >Aluminum 6020-T9</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_160194"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;30</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_160194' href="/search/DataSheet.aspx?MatGUID=3ffa7f3099eb45b1b3d4fc492adc4448" >Aluminum 6021 Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9359"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;31</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9359' href="/search/DataSheet.aspx?MatGUID=e49acbe8717f4983a2dd6c4fcbb850fd" >6022 Aluminum Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_160195"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;32</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_160195' href="/search/DataSheet.aspx?MatGUID=96be7b8b0c2b47b7b5573902cf294d85" >Aluminum 6023 Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_160196"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;33</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_160196' href="/search/DataSheet.aspx?MatGUID=a2ac74eb2e4c419aaa33d7de2d253a29" >Aluminum 6024 Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_160197"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;34</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_160197' href="/search/DataSheet.aspx?MatGUID=edfbedf12ea743bf84fa29bfee4e41c2" >Aluminum 6025 Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_160198"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;35</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_160198' href="/search/DataSheet.aspx?MatGUID=2e182078249549e3ad963b07997b77ac" >Aluminum 6026 Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_160247"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;36</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_160247' href="/search/DataSheet.aspx?MatGUID=37dcb360168241e4bf1b7def2bbbdaa0" >Aluminum 6027 Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_160199"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;37</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_160199' href="/search/DataSheet.aspx?MatGUID=b26bb43d072143219363f883b8622db7" >Aluminum 6028 Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9364"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;38</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9364' href="/search/DataSheet.aspx?MatGUID=94ab49baaca348e6b31d6acdb89f7f44" >Aluminum X6030 Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_160200"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;39</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_160200' href="/search/DataSheet.aspx?MatGUID=16c0d78231ea459385be6cc5483e1a86" >Aluminum 6033 Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_160201"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;40</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_160201' href="/search/DataSheet.aspx?MatGUID=203411b63c0c4067a4e536bdb5918510" >Aluminum 6040 Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_160202"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;41</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_160202' href="/search/DataSheet.aspx?MatGUID=a65b65f22fe94f0f9aca786ff1f12c7e" >Aluminum 6041 Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_160203"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;42</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_160203' href="/search/DataSheet.aspx?MatGUID=82c55edcbb7b4f41b1e803f4fca372ea" >Aluminum 6042 Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_160204"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;43</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_160204' href="/search/DataSheet.aspx?MatGUID=190383e490774bbb86c885d386d1fc3f" >Aluminum 6043 Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9378"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;44</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9378' href="/search/DataSheet.aspx?MatGUID=e67555e1950a4007958209967392d8a1" >Aluminum 6053-O</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9379"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;45</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9379' href="/search/DataSheet.aspx?MatGUID=e07e427c89ae460b963136828bb27db4" >Aluminum 6053-T4</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9380"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;46</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9380' href="/search/DataSheet.aspx?MatGUID=779d41ab5fc7444b8b7d965a178a5d81" >Aluminum 6053-T6</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_160239"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;47</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_160239' href="/search/DataSheet.aspx?MatGUID=8fce05572b3a4a219b267d8282a4fbc9" >Aluminum 6055 Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9382"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;48</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9382' href="/search/DataSheet.aspx?MatGUID=5ec2a310b0ae459fa2b7b5803b9d01e7" >6056 Aluminum Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9384"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;49</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9384' href="/search/DataSheet.aspx?MatGUID=4fece4acd9d9487d8e6da9472f42966a" >6060 Aluminum Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9385"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;50</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9385' href="/search/DataSheet.aspx?MatGUID=98d7fcd57e3845d68124c1bc4376618a" >Aluminum 6061A Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9386"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;51</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9386' href="/search/DataSheet.aspx?MatGUID=6c41b0bdea564d20b9fd287c03c97923" >Alclad Aluminum 6061-O</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9387"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;52</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9387' href="/search/DataSheet.aspx?MatGUID=1318cc5c380f46e59cd00339fb7d3a91" >Alclad Aluminum 6061-T4, T451</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9388"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;53</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9388' href="/search/DataSheet.aspx?MatGUID=3a2e111b27ef4e5d813bad6044b3f318" >Alclad Aluminum 6061-T6, T651</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9389"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;54</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9389' href="/search/DataSheet.aspx?MatGUID=626ec8cdca604f1994be4fc2bc6f7f63" >Aluminum 6061-O</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9390"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;55</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9390' href="/search/DataSheet.aspx?MatGUID=d5ea75577b1b49e8ad03caf007db5ba8" >Aluminum 6061-T4; 6061-T451</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_192050"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;56</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_192050' href="/search/DataSheet.aspx?MatGUID=b8d536e0b9b54bd7b69e4124d8f1d20a" >Aluminum 6061-T6; 6061-T651</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9392"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;57</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9392' href="/search/DataSheet.aspx?MatGUID=90404a0c001c4016b2b359a6c19f9127" >Aluminum 6061-T8</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9393"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;58</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9393' href="/search/DataSheet.aspx?MatGUID=e6212a3df98d4a7eb51edc1b1d3927ed" >Aluminum 6061-T91</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9394"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;59</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9394' href="/search/DataSheet.aspx?MatGUID=0f6b9e4702884eadbe6a8450cf89a925" >Aluminum 6061-T913</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9395"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;60</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9395' href="/search/DataSheet.aspx?MatGUID=1eacd7e5584c408d9c623a4f7d02787e" >Aluminum 6063A Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9396"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;61</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9396' href="/search/DataSheet.aspx?MatGUID=bcd1abbd8d6d47b1b9896af80a3759c6" >Aluminum 6063-O</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9397"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;62</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9397' href="/search/DataSheet.aspx?MatGUID=cf21d1a6f4244720a72bd38103dde0d7" >Aluminum 6063-T1</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9398"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;63</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9398' href="/search/DataSheet.aspx?MatGUID=abcbb46a1c9549e5b2e6ee5014cad5cf" >Aluminum 6063-T4</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9399"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;64</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9399' href="/search/DataSheet.aspx?MatGUID=79875d1b30c94af39029470988004fb6" >Aluminum 6063-T5</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9400"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;65</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9400' href="/search/DataSheet.aspx?MatGUID=333b3a557aeb49b2b17266558e5d0dc0" >Aluminum 6063-T6</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9401"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;66</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9401' href="/search/DataSheet.aspx?MatGUID=aa21b89cb6e24cffac3fc82413cbcee6" >Aluminum 6063-T83</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9402"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;67</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9402' href="/search/DataSheet.aspx?MatGUID=f0bf29074c9d46ca9043f98788c99034" >Aluminum 6063-T831</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9403"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;68</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9403' href="/search/DataSheet.aspx?MatGUID=efe924163241446ea3b6f2f139e890dd" >Aluminum 6063-T832</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9404"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;69</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9404' href="/search/DataSheet.aspx?MatGUID=ae29ac25bdab40748a2f38f7721b6799" >Aluminum 6063-T835</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_160211"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;70</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_160211' href="/search/DataSheet.aspx?MatGUID=37e5b8549ee549d3935838688b388dfe" >Aluminum 6064 Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_160212"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;71</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_160212' href="/search/DataSheet.aspx?MatGUID=64f6440f23dc436080447d7b042359e2" >Aluminum 6064A Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_160213"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;72</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_160213' href="/search/DataSheet.aspx?MatGUID=d24f94ab8e154fa7b659a816986ba10e" >Aluminum 6065 Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9407"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;73</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9407' href="/search/DataSheet.aspx?MatGUID=cbd5de5b95ae41e5bb071ce0320b7e45" >Aluminum 6066-O</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9408"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;74</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9408' href="/search/DataSheet.aspx?MatGUID=80c60b90667642a0a3ed159943fec41a" >Aluminum 6066-T4; 6066-T451</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9409"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;75</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9409' href="/search/DataSheet.aspx?MatGUID=2af4dd4c6d5740489dc5b342ef1ab84b" >Aluminum 6066-T6; 6066-T651</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_160259"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;76</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_160259' href="/search/DataSheet.aspx?MatGUID=9e791b39bc684d5dbff52712a922e35f" >Aluminum 6068 Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9410"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;77</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9410' href="/search/DataSheet.aspx?MatGUID=78814e7347b348f397a4152dd7b10a09" >Aluminum 6069-O</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9411"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;78</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9411' href="/search/DataSheet.aspx?MatGUID=2c4d5d28ad64471580c3cc4b107d25dd" >Aluminum 6069-T4</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9412"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;79</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9412' href="/search/DataSheet.aspx?MatGUID=92b9421f42e5491e9bd283c3a595a1c1" >Aluminum 6069-T6, 0.5 in x 3 in. Bar, Hot Hollow Extrusion, Average Tensile Values</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9413"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;80</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9413' href="/search/DataSheet.aspx?MatGUID=ac0400a58b9b43958df14468f213b205" >Aluminum 6069-T6, 0.75 in x 2.2 in. Bar, Hot Hollow Extrusion, Average Tensile Values</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9414"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;81</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9414' href="/search/DataSheet.aspx?MatGUID=1c6725ecec9c4362893738bf6ef819fa" >Aluminum 6069-T6, 1.25 Round Bar, Hot Hollow Extrusion, Average Tensile Values</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9415"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;82</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9415' href="/search/DataSheet.aspx?MatGUID=28e2a176e2274a8d8978302413ae9075" >Aluminum 6069-T6, Cold Impact Extruded Cylinder, Minimum Tensile Values</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9416"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;83</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9416' href="/search/DataSheet.aspx?MatGUID=4a66b13aef0d41bbacd9d734da9c1d91" >Aluminum 6069-T6, Hot Hollow Extrusion, Average Tensile Values</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9417"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;84</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9417' href="/search/DataSheet.aspx?MatGUID=e971f0e9181e4b5596428047c45648e1" >Aluminum 6069-T6, Hot Impact Extruded Cylinder, Minimum Tensile Values</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9419"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;85</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9419' href="/search/DataSheet.aspx?MatGUID=eb90d79ab3f8485ca1fa225b6f5b6093" >Aluminum 6070-O</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9420"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;86</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9420' href="/search/DataSheet.aspx?MatGUID=0c264dbb4caa49afa3c5cb06580b9ffc" >Aluminum 6070-T4</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9421"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;87</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9421' href="/search/DataSheet.aspx?MatGUID=aaf35a3d15ff42eeb4b4b0b7e156b7ff" >Aluminum 6070-T6</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9429"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;88</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9429' href="/search/DataSheet.aspx?MatGUID=70dc6c0b2b21412ea0f7ce2b4cec72e4" >6081 Aluminum Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9430"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;89</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9430' href="/search/DataSheet.aspx?MatGUID=a2dc020d04fe4657a0441f3edc71630f" >Aluminum 6082A Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_130362"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;90</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_130362' href="/search/DataSheet.aspx?MatGUID=04ff101281a44a4196d5f887fd9191af" >6082-O Aluminum</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9431"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;91</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9431' href="/search/DataSheet.aspx?MatGUID=117e133f428e40949528be5a86250108" >Aluminum 6082-T4</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9432"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;92</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9432' href="/search/DataSheet.aspx?MatGUID=fad29be6e64d4e95a241690f1f6e1eb7" >Aluminum 6082-T6</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9435"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;93</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9435' href="/search/DataSheet.aspx?MatGUID=3c360d4cd90546e38fd3f6924f7ecdb4" >6091 Aluminum Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9436"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;94</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9436' href="/search/DataSheet.aspx?MatGUID=62e593475b1d404a8887cb757856ef7d" >6092 Aluminum Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9439"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;95</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9439' href="/search/DataSheet.aspx?MatGUID=91820fc254e040e2a81b32c5a6f185b1" >Aluminum 6101A Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9440"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;96</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9440' href="/search/DataSheet.aspx?MatGUID=9f1c2bea184f4739ab6ae4409d202b62" >Aluminum 6101B Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9441"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;97</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9441' href="/search/DataSheet.aspx?MatGUID=9a2aa99692164dd485873681e4dafc2d" >Aluminum 6101-H111</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9442"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;98</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9442' href="/search/DataSheet.aspx?MatGUID=4303c5b908ff4cbd91a02fed7d4e8202" >Aluminum 6101-T6</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9443"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;99</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9443' href="/search/DataSheet.aspx?MatGUID=8e7ced37d7f34af29ce93bc89d7059d9" >Aluminum 6101-T61</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9444"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;100</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9444' href="/search/DataSheet.aspx?MatGUID=eadd3c4b94c044779d3946f754295819" >Aluminum 6101-T63</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9445"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;101</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9445' href="/search/DataSheet.aspx?MatGUID=56c6452582f043d49b272c88ccbd91ae" >Aluminum 6101-T64</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9446"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;102</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9446' href="/search/DataSheet.aspx?MatGUID=7f01f74db514492ea1f8a4531132c7f2" >Aluminum 6101-T65</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9447"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;103</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9447' href="/search/DataSheet.aspx?MatGUID=13f7771c64a24f08938f4fa45709258f" >6103 Aluminum Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9450"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;104</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9450' href="/search/DataSheet.aspx?MatGUID=75dc91b7f2a74c1b90ea742db8863682" >Aluminum 6105-T1</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9451"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;105</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9451' href="/search/DataSheet.aspx?MatGUID=9d1c81ac4e2b4e5590e5781f842b4446" >Aluminum 6105-T5</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9452"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;106</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9452' href="/search/DataSheet.aspx?MatGUID=533b8628fca44bdeaa20644b3c2c69d3" >6106 Aluminum Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9453"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;107</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9453' href="/search/DataSheet.aspx?MatGUID=bddb2be6db114296b7797435ca018b1f" >6110 Aluminum Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9454"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;108</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9454' href="/search/DataSheet.aspx?MatGUID=2fe01ffbc6ff4d4eb6a08205543d5f2f" >Aluminum 6110A Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9456"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;109</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9456' href="/search/DataSheet.aspx?MatGUID=9f0dd123a389483295924d3e8ef4c66d" >6111 Aluminum Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9457"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;110</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9457' href="/search/DataSheet.aspx?MatGUID=095099693f0d4c69be40a4992bb62391" >6113 Aluminum Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9459"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;111</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9459' href="/search/DataSheet.aspx?MatGUID=3be493e081f446c4a0c463ac7df33e39" >6116 Aluminum Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9460"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;112</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9460' href="/search/DataSheet.aspx?MatGUID=d25176485d7e44529ea00475ba1f1f51" >Aluminum 6151-O</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9461"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;113</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9461' href="/search/DataSheet.aspx?MatGUID=b27d20dbe83d4eb999c5b8c7b9101fcc" >Aluminum 6151-T4</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9462"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;114</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9462' href="/search/DataSheet.aspx?MatGUID=2ae86e9e609742cf962f9bb7be61cc75" >Aluminum 6151-T6</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_160206"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;115</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_160206' href="/search/DataSheet.aspx?MatGUID=eabd400cccb248e0a3073c3c7c7e58ba" >Aluminum 6156 Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9464"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;116</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9464' href="/search/DataSheet.aspx?MatGUID=528f334d9fe74ffa91a715d34a3bf26b" >6160 Aluminum Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9465"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;117</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9465' href="/search/DataSheet.aspx?MatGUID=bb2cb560ad134a3aabfeab78a9988572" >6162 Aluminum Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9466"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;118</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9466' href="/search/DataSheet.aspx?MatGUID=d481608521104ee59283c3bc76a8791d" >6181 Aluminum Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9467"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;119</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9467' href="/search/DataSheet.aspx?MatGUID=0826ac5f8e4d4ff5b14a6aeda1e1e93c" >Aluminum 6181A Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_160214"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;120</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_160214' href="/search/DataSheet.aspx?MatGUID=5c6ea83da0bf4786b635c88d48ec5240" >Aluminum 6182 Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9468"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;121</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9468' href="/search/DataSheet.aspx?MatGUID=0731bb2f74e84b1ba850e2b5114deffc" >Aluminum 6201A Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9469"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;122</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9469' href="/search/DataSheet.aspx?MatGUID=21a761c947e24155b7e95baf05d7743e" >Aluminum 6201-T6</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9470"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;123</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9470' href="/search/DataSheet.aspx?MatGUID=e032f5f4165f4901b823b3b6b0e64b74" >Aluminum 6201-T81</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9471"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;124</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9471' href="/search/DataSheet.aspx?MatGUID=de32655c8dd24ee19d89077c9e32f558" >Aluminum 6205-T1</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9472"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;125</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9472' href="/search/DataSheet.aspx?MatGUID=d77270418a294a989dea09629722d462" >Aluminum 6205-T5</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9473"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;126</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9473' href="/search/DataSheet.aspx?MatGUID=300f8e1ea52543918d1edc9df77fcae7" >6206 Aluminum Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9481"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;127</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9481' href="/search/DataSheet.aspx?MatGUID=8a308f83d0704ff7892183c5f9dab477" >Aluminum 6253</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9483"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;128</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9483' href="/search/DataSheet.aspx?MatGUID=265aba543c6b4405aa8b5a66dc072671" >6260 Aluminum Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9484"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;129</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9484' href="/search/DataSheet.aspx?MatGUID=9db8f2e3560b4b4b958b4ac37490e537" >6261 Aluminum Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_160210"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;130</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_160210' href="/search/DataSheet.aspx?MatGUID=2bd6e110c4714b55bfc8164ca693ba20" >Aluminum 6262A Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9485"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;131</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9485' href="/search/DataSheet.aspx?MatGUID=5437e9998e3d4d4897869f79e9ccfa7d" >Aluminum 6262-T6</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9486"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;132</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9486' href="/search/DataSheet.aspx?MatGUID=57fce0c06d044207901073e6105ea962" >Aluminum 6262-T651</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9487"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;133</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9487' href="/search/DataSheet.aspx?MatGUID=e43fda6d15c84dce841381932fa6a51c" >Aluminum 6262-T8</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9488"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;134</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9488' href="/search/DataSheet.aspx?MatGUID=01760d1b9b8e4aa4b0f894a03b64b154" >Aluminum 6262-T9</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9489"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;135</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9489' href="/search/DataSheet.aspx?MatGUID=78e662caa9c945408f92a58593037c3e" >6301 Aluminum Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_160233"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;136</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_160233' href="/search/DataSheet.aspx?MatGUID=aa6ba951ab2b4a588d62a00f4f98686f" >Aluminum 6305 Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9490"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;137</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9490' href="/search/DataSheet.aspx?MatGUID=e8af3ae055904ff19e2bbd89fb990fa9" >6306 Aluminum Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9495"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;138</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9495' href="/search/DataSheet.aspx?MatGUID=51742a69ad73440bbaafa727096f346a" >Aluminum 6351A Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9496"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;139</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9496' href="/search/DataSheet.aspx?MatGUID=01fcebd70e3a4d51be1ca3c24174f138" >Aluminum 6351-T4; 6351-T451</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9497"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;140</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9497' href="/search/DataSheet.aspx?MatGUID=36bdc523d25b4d738dabbe3f434bb69f" >Aluminum 6351-T54</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9498"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;141</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9498' href="/search/DataSheet.aspx?MatGUID=ca1cf891973c4745b41677780dad6240" >Aluminum 6351-T6; 6351-T651</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_160207"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;142</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_160207' href="/search/DataSheet.aspx?MatGUID=1b8464f680e04a0d9c2ae694f517baf8" >Aluminum 6360 Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9499"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;143</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9499' href="/search/DataSheet.aspx?MatGUID=e0b5696cf2af4af59a46b3ab7f0eb47f" >6401 Aluminum Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_160205"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;144</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_160205' href="/search/DataSheet.aspx?MatGUID=372b115a829449349bd5c516041b28d2" >Aluminum 6451 Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_160208"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;145</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_160208' href="/search/DataSheet.aspx?MatGUID=d3eb21c576f34ee888be7af58d3525a2" >Aluminum 6460 Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_160253"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;146</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_160253' href="/search/DataSheet.aspx?MatGUID=355eaec12a194e94839515da06e1486e" >Aluminum 6460B Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9505"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;147</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9505' href="/search/DataSheet.aspx?MatGUID=25812fe98b404860a34dd7bb4c91d76b" >Aluminum 6463A Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9506"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;148</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9506' href="/search/DataSheet.aspx?MatGUID=98ee6d27e4e04ebda80b658bcf791389" >Aluminum 6463-O</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9507"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;149</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9507' href="/search/DataSheet.aspx?MatGUID=defd03cb0b8c40b0813e5812a915407a" >Aluminum 6463-T1</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9508"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;150</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9508' href="/search/DataSheet.aspx?MatGUID=2f9c730a25cf4d9bb6a4f5169168cc27" >Aluminum 6463-T4</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9509"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;151</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9509' href="/search/DataSheet.aspx?MatGUID=48f9f056dfa94bb99f72adcf8d0b46ae" >Aluminum 6463-T5</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9510"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;152</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9510' href="/search/DataSheet.aspx?MatGUID=aa6090d830d2499f96fc2b2a094c06ac" >Aluminum 6463-T6</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_160192"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;153</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_160192' href="/search/DataSheet.aspx?MatGUID=beb45e6bf95046b3a5f4b17f63ec94ed" >Aluminum 6501 Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_160209"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;154</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_160209' href="/search/DataSheet.aspx?MatGUID=056604e24b974265a191f7977511a300" >Aluminum 6560 Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_160243"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;155</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_160243' href="/search/DataSheet.aspx?MatGUID=437137d1b8b042e0a010242887eba078" >Aluminum 6660 Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9513"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;156</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9513' href="/search/DataSheet.aspx?MatGUID=77c31483de134b3d923419592a170437" >6763 Aluminum Composition Spec</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9516"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;157</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9516' href="/search/DataSheet.aspx?MatGUID=9c0e2ff60a2d4deb9359bbff4bf15553" >Aluminum 6951-O</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9517"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;158</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9517' href="/search/DataSheet.aspx?MatGUID=6a7676bba4af422f9def1dbc411df75b" >Aluminum 6951-T6</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_9518"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;159</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_9518' href="/search/DataSheet.aspx?MatGUID=67f1829b702a4d5ea27a2ed803b0859e" >6963 Aluminum Composition Spec</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_14423"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;160</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_14423' href="/search/DataSheet.aspx?MatGUID=03160dbfd4bd40d0a08c163043530547" >Composition Spec for Aluminum 6060H2</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_14424"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;161</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_14424' href="/search/DataSheet.aspx?MatGUID=4358024081ae4a00b5ff1731d24ce569" >Composition Spec for Aluminum 6105H</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_14745"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;162</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_14745' href="/search/DataSheet.aspx?MatGUID=20817c80e16042dd8672bf001bd7863e" >Alcoa Excalibar® 6013-T651 Aluminum</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_14746"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;163</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_14746' href="/search/DataSheet.aspx?MatGUID=15350c433a51463287d37f8354bd290a" >Alcoa Excalibar® 6013-T8 Aluminum, 0.3-1.9 cm diameter</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_14747"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;164</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_14747' href="/search/DataSheet.aspx?MatGUID=3d11f20dd78c4265a6a0069a39cadbaf" >Alcoa Excalibar® 6013-T8 Aluminum, 1.9-3.8 cm diameter</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_14748"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;165</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_14748' href="/search/DataSheet.aspx?MatGUID=388da46684bb427a992d2e9c1055f670" >Alcoa Excalibar® 6013-T8 Aluminum, 3.8-8.2 cm diameter</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_14752"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;166</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_14752' href="/search/DataSheet.aspx?MatGUID=aae0cab5ea414630b4b22c8baf8a30da" >Alcoa UltrAlloy® 6020-T651 Aluminum</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_14753"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;167</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_14753' href="/search/DataSheet.aspx?MatGUID=cae92c0029764c1c9df27beea579cacf" >Alcoa UltrAlloy® 6020-T8 Aluminum</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_14754"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;168</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_14754' href="/search/DataSheet.aspx?MatGUID=9742a1d292e34b75b071c6e868ba784d" >Alcoa UltrAlloy® 6020-T9 Aluminum</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_14760"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;169</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_14760' href="/search/DataSheet.aspx?MatGUID=afdd89176e824073979f39e723afb5a0" >ALIMEX PLANAL 6082 T651 Aluminum Alloy</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_14764"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;170</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_14764' href="/search/DataSheet.aspx?MatGUID=39d199eebf034c3cb424caaee9d4be08" >ALIMEX 6082 T651 Aluminum Alloy Rolled</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_192331"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;171</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_192331' href="/search/DataSheet.aspx?MatGUID=e09aa5d2b592419090ef498f5f1bc643" >Materion SupremEX® 640XA Aluminum - Silicon Carbide Metal Matrix Composite, T6 CWQ Billet</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_192332"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;172</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_192332' href="/search/DataSheet.aspx?MatGUID=097c557940134cd59b77d1c06fc285c8" >Materion SupremEX® 640XA Aluminum - Silicon Carbide Metal Matrix Composite, T6 PGQ Billet</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_192333"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;173</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_192333' href="/search/DataSheet.aspx?MatGUID=940a702af9c74387bccfe9bb4bb2309d" >Materion SupremEX® 640XA Aluminum - Silicon Carbide Metal Matrix Composite, T6 PGQ Forged Plate</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_192334"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;174</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_192334' href="/search/DataSheet.aspx?MatGUID=d12c27e558eb48fbaf380e3829cb6291" >Materion SupremEX® 640XA Aluminum - Silicon Carbide Metal Matrix Composite, T7 Forged Plate</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_160267"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;175</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_160267' href="/search/DataSheet.aspx?MatGUID=908f2ea7545546c4a0ed56439356b248" >Constellium PLAN 6061 Aluminum Rolled Plate</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_160268"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;176</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_160268' href="/search/DataSheet.aspx?MatGUID=0823ba4108834d9b830043339e68339a" >Constellium PLAN 6082 Aluminum Rolled Plate</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_160271"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;177</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_160271' href="/search/DataSheet.aspx?MatGUID=b92d6bd5c3c24f58bcbe76c46ff0f496" >Constellium ALPLAN® 6061 Rolled Precision Aluminum Plate, Milled Both Sides</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_160272"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;178</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_160272' href="/search/DataSheet.aspx?MatGUID=4d40b15a16d34c1f86d293ed7b4fa0f3" >Constellium ALPLAN® 6082 Rolled Precision Aluminum Plate, Milled Both Sides</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_160282"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;179</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_160282' href="/search/DataSheet.aspx?MatGUID=d8e2e3306755478793cdd0ceb4f9a2cb" >Constellium DOKIMA® Rolled Plate Aluminum</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_160287"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;180</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_160287' href="/search/DataSheet.aspx?MatGUID=e614bffd9a9d43bc9a5a066a2eafe99b" >Constellium Planoxal® 60 Aluminum plate</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_231153"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;181</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_231153' href="/search/DataSheet.aspx?MatGUID=ccf8e70590bb45c3bddacd16efc19701" >DWA 6092/SiC/17.5p-T6 Aluminum SiC Metal-Matrix-Composite (Al MMC), Extruded</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_231154"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;182</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_231154' href="/search/DataSheet.aspx?MatGUID=179aaeb42a7346ddba6f9172b93c4109" >DWA 6092/SiC/17.5p-T6 Aluminum SiC Metal-Matrix-Composite (Al MMC), Sheet</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_231155"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;183</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_231155' href="/search/DataSheet.aspx?MatGUID=7a97a2a865544ac3a56bf7144c8dd151" >DWA 6092/SiC/25p-T6 Aluminum SiC Metal-Matrix-Composite (Al MMC), Extruded</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_231156"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;184</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_231156' href="/search/DataSheet.aspx?MatGUID=9f7fd2a219e943be84ff8898b22dfbde" >DWA 6092/SiC/40p-T6 Aluminum SiC Metal-Matrix-Composite (Al MMC)</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_231157"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;185</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_231157' href="/search/DataSheet.aspx?MatGUID=4716e9e0d3a94c5f9c570ef90e528da8" >DWA 6091/SiC/20p-T6 Aluminum SiC Metal-Matrix-Composite (Al MMC)</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_231158"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;186</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_231158' href="/search/DataSheet.aspx?MatGUID=3c3780d1480a4f3db1016976248bfa86" >DWA 6091/SiC/40p-T6 Aluminum SiC Metal-Matrix-Composite (Al MMC)</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17665"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;187</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17665' href="/search/DataSheet.aspx?MatGUID=977023dd638d4885879e7c5ec6bd68d6" >Kaiser 6033 T6, T651 Rod & Bar</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17666"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;188</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17666' href="/search/DataSheet.aspx?MatGUID=74b8278010ef427ab58de7d70f807d2c" >Kaiser 6033 T8 Rod & Bar</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17667"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;189</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17667' href="/search/DataSheet.aspx?MatGUID=4f7b3a263e99421ca57de43230641574" >Kaiser 6040 T6, T651 Rod & Bar</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17668"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;190</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17668' href="/search/DataSheet.aspx?MatGUID=d42f80894fa64c1286fcd72d5e5069e4" >Kaiser 6040 T8 Rod & Bar</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17669"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;191</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17669' href="/search/DataSheet.aspx?MatGUID=558b4e5701dc4fc6ae70b93bf74b0ca8" >Kaiser 6040 T9 Rod & Bar</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_100834"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;192</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_100834' href="/search/DataSheet.aspx?MatGUID=a62118962f134ac39469cd7232c3c113" >RSP Technology RSA-6061 T6 Aluminum Super Alloy</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_318910"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;193</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_318910' href="/search/DataSheet.aspx?MatGUID=3b28bd9bda3e497f8ec094db7654b273" >Traid Villarroya TVH® TV 6026-T6 Extruded (AA 6026) Aluminum alloy</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_318923"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;194</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_318923' href="/search/DataSheet.aspx?MatGUID=dd4cbfa7b171449f86959553e5172251" >Traid Villarroya TVH® TV 6026-T6 Drawn (AA 6026) Aluminum alloy</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_318911"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;195</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_318911' href="/search/DataSheet.aspx?MatGUID=a577a5666a44437d9b449593379031c7" >Traid Villarroya TVH® TV 6061-T6 Extruded (AA 6061) Aluminum alloy</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_318924"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;196</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_318924' href="/search/DataSheet.aspx?MatGUID=a95620a975494584a64e0aae2f6e1eed" >Traid Villarroya TVH® TV 6061-T6 Drawn (AA 6061) Aluminum alloy</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_318912"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;197</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_318912' href="/search/DataSheet.aspx?MatGUID=8cf48c78daaa460a9fda7faca36585f1" >Traid Villarroya TVH® TV 6082-T6 Extruded (AA 6082) Aluminum alloy</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_318925"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;198</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_318925' href="/search/DataSheet.aspx?MatGUID=73512d3c37ba4429bdb74b0e020c9999" >Traid Villarroya TVH® TV 6082 (AA 6082) Aluminum alloy</a></td></tr>

		
	</table>

	<table style="background-color:Silver;" class="tabledataformat" >

		<tr class="">
			<th>&nbsp;</th>
		</tr>

		<tr>
			<td><strong>Found <span id="ctl00_ContentMain_UcSearchResults1_lblResultCount2">198</span> Results</strong> -- 
				
				Page 
				<select name="ctl00$ContentMain$UcSearchResults1$drpPageSelect2" onchange="javascript:setTimeout('__doPostBack(\'ctl00$ContentMain$UcSearchResults1$drpPageSelect2\',\'\')', 0)" id="ctl00_ContentMain_UcSearchResults1_drpPageSelect2">
		<option selected="selected" value="1">1</option>

	</select>
				of <span id="ctl00_ContentMain_UcSearchResults1_lblPageTotal2">1</span> 
				
				-- 
				<a id="ctl00_ContentMain_UcSearchResults1_lnkPrevPage2" disabled="disabled" style="color:Gray;">[Prev Page]</a>
				<a id="ctl00_ContentMain_UcSearchResults1_lnkNextPage2" disabled="disabled" style="color:Gray;">[Next Page]</a>
				--&nbsp;

				view 
				<select name="ctl00$ContentMain$UcSearchResults1$drpPageSize2" onchange="javascript:setTimeout('__doPostBack(\'ctl00$ContentMain$UcSearchResults1$drpPageSize2\',\'\')', 0)" id="ctl00_ContentMain_UcSearchResults1_drpPageSize2">
		<option value="25">25</option>
		<option value="50">50</option>
		<option value="75">75</option>
		<option value="100">100</option>
		<option value="150">150</option>
		<option selected="selected" value="200">200</option>

	</select> per page
			</td>
		</tr>
	</table>
	
	 


</div>





<script type="text/javascript">

	//GLOBAL VARIABLES
	var gCurrentCheckbox;
	var gNewFolderID;
	var gOldFolderName;
	var gSelectedFolderID = 0;
	var gMaxFolders = 0;

	//GLOBAL FIELD REFERENCES
	var drpFolderList = document.getElementById("ctl00_ContentMain_UcSearchResults1_drpFolderList");
	var drpFolderAction = document.getElementById("ctl00_ContentMain_UcSearchResults1_drpFolderAction");
	var txtFolderMatCount = document.getElementById("ctl00_ContentMain_UcSearchResults1_txtFolderMatCount");
	var divUserMessage = document.getElementById("divUserMessage");
	var divFolderName = document.getElementById("divFolderName");
	var tblFolderName = document.getElementById("tblFolderName");
	var txtFolderName = document.getElementById("txtFolderName2");
	
	// See MS ajax.net client side documentation
	// http://ajax.asp.net/docs/ClientReference/Sys.Net/WebrequestManagerClass/	
	Sys.Net.WebRequestManager.add_invokingRequest(ucSearchResults_OnRequestStart);
	Sys.Net.WebRequestManager.add_completedRequest(ucSearchResults_OnRequestEnd);
	
	var timerID = 0;

	function ucSearchResults_OnRequestStart(){
		document.body.style.cursor="progress";
	}
	
	function ucSearchResults_OnRequestEnd(){
		document.body.style.cursor="auto";
	}

	//drpFolderAction pull down does not exist for anonymous users
	if(drpFolderAction) {
		drpFolderAction.onchange=RunFolderAction;
	}

	//RUN ONCE on page load
	//matweb.register_onload(SetSelectedFolder);
	
	SearchMatIDList = []; //an array of MatIDs
	//matweb.alert(MatIDList);
	
	//======================
	//Test Service 
	//======================
	//TestService("This is a test");
	//aci.matweb.WebServices.Folders.RaiseTestError(null,ErrorHandler,"testerror");
	
	function TestService(arg){
		aci.matweb.WebServices.Folders.HelloWorld(arg, TestService_CALLBACK,ErrorHandler,"TestService");
	}
	
	function TestService_CALLBACK(data,context){
		alert(data);
	}
	
	// =================================
	// This function is the onchange event for the material checkbox. See Code Behind.
	// The first time a material is checked, and there are no folders, the folder web service creates a NEW FOLDER
	// =================================
	function ToggleMat(chkBox,MatID,MaxFolderMats){
	
		HideFolderNameControls();

		var FolderID,Context;
		gCurrentCheckbox = chkBox;
		var matLink = $("lnkMatl_" + MatID);
		var MatName = matLink.innerHTML;
		//alert(MatName);

		var SelectedOption = drpFolderList.options[drpFolderList.selectedIndex];  

		if(gNewFolderID > 0)
			FolderID = gNewFolderID;
		else
			FolderID = SelectedOption.value; //this will be 0 if no folders exist yet
		//alert(FolderID);
		
		// call the folder web service 
		// See: ScriptManagerProxy object at the top of this page
		// Also See: /folders/FolderService.asmx & /App_Code/FolderService.cs
		if(chkBox.checked){
			Context = "AddMat";
			aci.matweb.WebServices.Folders.AddMaterial(FolderID, MatID, MatName, ToggleMat_CALLBACK,ErrorHandler,Context);
		}
		else{
			Context = "RemoveMat";
			aci.matweb.WebServices.Folders.RemoveMaterial(FolderID, MatID, ToggleMat_CALLBACK,ErrorHandler,Context);
		}
			
	}
	
	// =================================
	// CALLBACK FUNCTION
	// this function is called by the AJAX framework after the web service is invoked
	// The folder web service returns a ReturnData type object.
	// =================================
	function ToggleMat_CALLBACK(data,context){

		 //alert(data);
		// alert(context);

		var FolderMatCount = data.FolderMatCount
		var NewFolderID = data.NewFolderID
		var DisplayMessage = data.DisplayMessage
		var MaxFolderMats = data.FolderMaxMatls;
		
		txtFolderMatCount.value = FolderMatCount + "/" + MaxFolderMats;

		// If this is the first time the user checked the checkbox, and the user did not already have any folders, 
		// a new folder may have been created.
		// Save the new folderID to a hidden field, so we can reference it later.
		if(NewFolderID > 0){
			gNewFolderID = NewFolderID;
		}
		
		// alert(gNewFolderID);
		// alert(FolderCount);
		// alert(ReloadFolderList);
		// if(NewFolderID>0) alert(NewFolderID);
		// alert(MaxFolderMats);
		
		switch(context){
		case "AddMat":
			if(DisplayMessage){
				gCurrentCheckbox.checked = false;
				matweb.alert(DisplayMessage);
			}
			break    
		case "RemoveMat":
			break    
		default:
			matweb.Alert("Unhandeled Context in ToggleMat_CALLBACK() function: " + Context);
		}//end switch

	}//end function

	
	//=================================
	function CompareMaterials(){
	//=================================
		var SelectedFolderID = GetSelectedFolderID();
		window.location.href="/folders/Compare.aspx?FolderID=" + SelectedFolderID;
	}

	//=================================
	// Runs on drpFolderList.onchange, and also is called by other methods.
	// Sets the checkbox for each visible material in the folder.
	// Also sets txtFolderMatCount with the MatCount indicator for the selected folder.
	//=================================
	function SetSelectedFolder(){
		//alert("SetSelectedFolder()");
		var SelectedFolderID = GetSelectedFolderID();
		//alert("SelectedFolderID :" + SelectedFolderID)
		
		aci.matweb.WebServices.Folders.SetSelectedFolder(SelectedFolderID,SetSelectedFolder_CALLBACK,ErrorHandler,"update_txtFolderMatCount");
		
		HideFolderNameControls();
	}

	// =================================
	// CALLBACK FUNCTION
	// Returns a list of MatID's in the selected folder, and sets the checkbox for each material.
	// Also sets txtFolderMatCount with the MatCount indicator for the selected folder.
	// =================================
	function SetSelectedFolder_CALLBACK(data,context){

		// alert(data);
		// alert(context);

		// the folder web service returns an object with 4 fields
		//alert("data.FolderMatCount: " + data.FolderMatCount);
		//alert("data.FolderMaxMatls: " + data.FolderMaxMatls);
		//alert("data.DisplayMessage: " + data.DisplayMessage);
		//alert("data.MatIDList: " + data.FolderMatIDList);

		if(data.DisplayMessage){
			matweb.alert(data.DisplayMessage);
		}

		txtFolderMatCount.value = data.FolderMatCount + "/" + data.FolderMaxMatls;

		//alert(SearchMatIDList);
		var chkFolder;

		// Set checkboxes for any visible materials in the folder
		// loop over all materials in the search results for the current page...
		//alert('lenOfArray=' + SearchMatIDList.length);
		for(s=0;s<SearchMatIDList.length;s++){
			chkFolder = document.getElementById("chkFolder_" + SearchMatIDList[s]);
			//alert('index=' + SearchMatIDList[s]);
			chkFolder.checked = false; //clear any currently checked boxes...
			//loop over all materials in the folder...
			//If there is a match, make it checked.
			for(f=0;f<data.FolderMatIDList.length;f++){
				if(SearchMatIDList[s]==data.FolderMatIDList[f]){
					//alert("Match found: " + data.FolderMatIDList[f]);
					chkFolder.checked = true;
				}
			}
		}

	}//end function
	
	//=================================
	function RunFolderAction(){
	//=================================
	
		//alert("RunFolderAction()");
		var action = drpFolderAction.options[drpFolderAction.selectedIndex].value;
		var SelectedFolderName = drpFolderList.options[drpFolderList.selectedIndex].text;
		var FolderID = GetSelectedFolderID();
		var msg = "";
		
		switch(action){
			case "empty":
				aci.matweb.WebServices.Folders.EmptyFolder(FolderID,SetSelectedFolder_CALLBACK,ErrorHandler,action);
				msg = "Folder ["+SelectedFolderName+"] was cleared of materials";
				HideFolderNameControls();
				break;
			case "delete":
				var answer = confirm("Delete folder ["+SelectedFolderName+"] ?");
				if(answer){
					gOldFolderName = SelectedFolderName;
					aci.matweb.WebServices.Folders.DeleteFolder(FolderID,DeleteFolder_CALLBACK,ErrorHandler,action);
				}
				HideFolderNameControls();
				break;
			case "new":
				//setting up new folder controls
				if(drpFolderList.options.length >= gMaxFolders){
					matweb.alert("You have already added as many folders as you are currently allowed by the system (" + gMaxFolders + ").<p/> You may view our <a href=\"/membership/benefits.aspx\">benefits</a> page for the features that are allowed for your current user level.");
				}
				else{
				divFolderName.style.display="block";
				divFolderName.innerHTML="New Folder Name";
				tblFolderName.style.display="block";
				txtFolderName.value = "New Folder Name";
				txtFolderName.focus();
				txtFolderName.select();
				}
				break;
			case "rename":
				//setting up rename folder controls
				divFolderName.style.display="block";
				divFolderName.innerHTML="Rename Folder";
				tblFolderName.style.display="block";
				txtFolderName.value = SelectedFolderName;
				txtFolderName.focus();
				txtFolderName.select();
				break;
			case "managefolders":
				window.location.href="/folders/ListFolders.aspx";
				break;
			case "expSolidworks":
				ExportFolder(FolderID, 2, null);
				break;
			case "expALGOR":
				ExportFolder(FolderID, 3, null);
				break;
			case "expANSYS":
				ExportFolder(FolderID, 4, null);
				break;
			case "expNEiWorks":
				ExportFolder(FolderID, 5, null);
				break;
			case "expCOMSOL3":
				ExportFolder(FolderID, 6, '3.4 or newer');
				break;
			case "expCOMSOL4":
				ExportFolder(FolderID, 10, null);
				break;
			case "expETBX":
				ExportFolder(FolderID, 8, null);
				break;
			case "expSpaceClaim":
				ExportFolder(FolderID, 9, null);
				break;
			case "0":
				//do nothing
				drpFolderAction.selectedIndex = 0;
				break;
			default: 
				alert("Unhandled Action: " + action);
		}

		//drpFolderAction.selectedIndex=0;
		divUserMessage.innerHTML = msg;
		//alert("End RunFolderAction()");
		
	}
	
	function DeleteFolder_CALLBACK(DeleteWasSuccessfull,context){
		divUserMessage.innerHTML = "Folder ["+gOldFolderName+"] was deleted.";
		//Refresh the folder list and selected checkboxes
		aci.matweb.WebServices.Folders.GetFolderList(GetFolderList_CALLBACK,ErrorHandler,"DeleteFolder_CALLBACK");
	}	

	// rsw
	function ExportFolder(folderid, modeid, comsolversion){
		if(txtFolderMatCount.value.substring(0,1) == '0'){
			alert('Folder is empty, nothing to export.');
			drpFolderAction.selectedIndex = 0;
			return;
		}

		var ExportPath = "/folders/ListFolders.aspx";
		
		//alert(ExportPath);
		location.href = ExportPath;
		//window.open(ExportPath, '', '', '');
	}
	
	
	
	
	//===============================================================
	// This runs on the onclick event for btnApplyFolderName.
	// It handles creating a new folder, or renaming an existing one.
	//===============================================================
	function ApplyFolderName(){
	
		//alert("ApplyFolderName()");
		var action = drpFolderAction.options[drpFolderAction.selectedIndex].value;
		var NewFolderName = txtFolderName.value;
		var FolderID = GetSelectedFolderID();
		var msg = "";

		//alert(FolderID);

		//If we are showing the non-existant "virtual" folder "My Folder",
		//create it first, and then rename it.
		if(action=="rename" && FolderID==0)
			action="add_and_rename";

		switch(action){
			case "new":
				aci.matweb.WebServices.Folders.AddFolder(NewFolderName,AddRenameFolder_CALLBACK,ErrorHandler,action);
				break;
			case "rename":
				gOldFolderName = drpFolderList.options[drpFolderList.selectedIndex].text;
				aci.matweb.WebServices.Folders.RenameFolder(FolderID,NewFolderName,AddRenameFolder_CALLBACK,ErrorHandler,action);
				break;
			case "add_and_rename":
				gOldFolderName = drpFolderList.options[drpFolderList.selectedIndex].text;
				aci.matweb.WebServices.Folders.AddFolder(NewFolderName,AddRenameFolder_CALLBACK,ErrorHandler,action);
				break;
			default: 
				alert("Unhandled Action in ApplyFolderName(): " + action);
		}

		HideFolderNameControls();
		
	}
	
	//=================================
	// CALLBACK FUNCTION
	//=================================
	function AddRenameFolder_CALLBACK(ReturnData,context){
		//alert("AddRenameFolder_CALLBACK()");
		
		var msg;
		//Folder = ReturnData.Folder;
	
		switch(context){
			case "new":
				//gNewFolderID = Folder.FolderID;
				gNewFolderID = ReturnData.NewFolderID;
				msg = "The folder [" + ReturnData.NewFolderName + "] was created.";
				break;
			case "rename":
				msg = "The folder [" + gOldFolderName+ "] was renamed to [" + ReturnData.NewFolderName + "].";
				break;
			case "add_and_rename":
				//gNewFolderID = Folder.FolderID;
				gNewFolderID = ReturnData.NewFolderID;
				msg = "The folder [" + gOldFolderName+ "] was renamed to [" + ReturnData.NewFolderName + "].";
				break;
		}
		
		divUserMessage.innerHTML = msg;
		
		//refresh the folder list, so it includes the new folder
		aci.matweb.WebServices.Folders.GetFolderList(GetFolderList_CALLBACK,ErrorHandler,"AddFolder");

	}
	
	//=================================
	function HideFolderNameControls(){
	//=================================
		//These fields will not exist for anonymous users
		if(drpFolderAction){
			drpFolderAction.selectedIndex=0;
			divFolderName.style.display="none";
			tblFolderName.style.display="none";
		}
	}
	
	//=================================
	function GetSelectedFolderID(){
	//=================================
		var FolderID = 0;
		//if one of the processeses created a new folder id, then use it.
		// alert('gNewFolderID=' + gNewFolderID);
		if(gNewFolderID > 0){
			FolderID = gNewFolderID;
			//gNewFolderID = 0;//clear the new folder id, so we don't use it again.
		}
		else 
			if(drpFolderList != null){
				if(drpFolderList.options.length == 0){
					FolderID = gSelectedFolderID;
				}
				else{
					var SelectedOption = drpFolderList.options[drpFolderList.selectedIndex];  
					FolderID = SelectedOption.value; //this will be 0 if no folders exist yet
				}
			}
		return FolderID;
	}

	//=================================
	function UnregisteredUserMessage(chkBox){
	//=================================
		matweb.alert("This feature is reserved for registered users.<p/><a href='/membership/regstart.aspx'>Click here to register.</a>","Reserved Feature");
		chkBox.checked = false;
	}

	// =================================
	// CALLBACK FUNCTION
	// The folder web service returns a JS array of iBatis data objects of type: aci.matweb.data.folder.Folder 
	// The drop down list of folders is set using this list.
	// =================================
	function GetFolderList_CALLBACK(iFolderList,Context){
	
		//alert(iFolderList);
		//alert('iFolderList.length: ' + iFolderList.length);

		if(drpFolderList != null){
		
		var SelectedFolderID = GetSelectedFolderID();//remember the current selected folder, so we can use it later
		var opt;
		var folder;

		//clear the options and reload them
		drpFolderList.options.length = 0;

		//add a default empty folder...		
		if(iFolderList.length==0){
			opt = document.createElement('OPTION');
			opt.value = "0";
			opt.text = "My Folder";
			drpFolderList.options.add(opt);
			i++;
		}
		
		for(var i = 0;i < iFolderList.length;i++){
			opt = document.createElement('OPTION');
			folder = iFolderList[i];
			//alert(folder.FolderID);
			//alert(folder.FolderName);
			opt.value = folder.FolderID;
			opt.text = folder.FolderName;
			if(folder.FolderID == SelectedFolderID) opt.selected = true;
			drpFolderList.options.add(opt);
		}

		//clear the new folder id, so we don't use it again. 
		gNewFolderID = 0;
		
		//Set checkboxes and folder count fields for the new selected folder
		SetSelectedFolder();
		}

	}
	
	// =================================
	// ON ERROR CALLBACK FUNCTION
	// =================================
	function ErrorHandler(error,context) {
	
			var stackTrace = error.get_stackTrace();
			var message = error.get_message();
			var statusCode = error.get_statusCode();
			var exceptionType = error.get_exceptionType();
			var timedout = error.get_timedOut();
			var Debug = false;

			// Compose the error message...
			var ErrMsg =     
					"<strong>Stack Trace</strong><br/>" +  stackTrace + "<br/>" +
					"<strong>WebService Error</strong><br/>" + message + "<br/>" +
					"<strong>Status Code</strong><br/>" + statusCode + "<br/>" +
					"<strong>Exception Type</strong><br/>" + exceptionType + "<br/>" +
					"<strong>JS Context</strong><br/>" + context + "<br/>" +
					"<strong>Timedout</strong><br/>" + timedout + "<br/>" +
					"<strong>Source Page</strong><br/>" + "/search/MaterialGroupSearch.aspx";
			//matweb.alert(ErrMsg,"Error in WebService",null,"500");
			if(Debug){
				matweb.alert(ErrMsg,"Error in WebService",null,"500");
				//alert(ErrMsg);
			}
			else{
				ErrMsg = escape(ErrMsg);
				//matweb.alert(ErrMsg);
				//window.location.href="/errorJS.aspx?ErrMsg=" + ErrMsg;
				divUserMessage.innerHTML = "Problem retrieving folder data...Some folder features may not work.";
			}
		
	}
	
	function ShowInterpolationMessage(){
		InterpolationMsg = "The material was found via interpolation, so no data points can be displayed. See the material data sheet for actual data points.";
		matweb.alert(InterpolationMsg);
	}
	
</script>

		</td>
	</tr>
	</table>

<hr />
<a id="help"></a>
<table><tr><td>
<p/><b>Instructions:</b> This page allows you to quickly access all of the polymers/plastics, metals, ceramics, fluids, and other engineering materials
in the MatWeb material property database.&nbsp; Just select the material category you would like to find from the tree.  Click on the [+] icon to open subcategory branches in the tree.
Then click the <b>'Find'</b> button next to its box.&nbsp; You can then follow the links to the most complete data sheets on the Web, with information on over 1000 available properties, including dielectric
constant, Poisson's ratio, glass transition temperature, dissipation factor, and Vicat softening point.
</td><td><a href="https://proto3000.com"><img src="/images/assets/proto3000.jpg" border = "0"><br>

Proto3000 - Rapid Prototyping</a>
</td></tr></table>

<p/><span class="hiText">Notes:</span>

<ul>
  <li>Visit MatWeb's <a href="/search/PropertySearch.aspx">property-based search page</a> to limit your results to materials that meet specific property performance criteria.</li>
  <li>Text searches can be done from any page - use the <b>Quick Search</b> box in the top right of the page.</li>
  <li>More <a href="/help/strategy.aspx"><strong>Search Strategies</strong></a> on how to find information in MatWeb.</li>
   <li><a href="/search/GetAllMatls.aspx">Show All Materials</a> - an inefficient drill-down to data sheets.  Registered users who are logged in omit a step in this drill down process.</li>
</ul>

<p/><a href="#Top">Top</a>

<script type="text/javascript">

	var txtMatGroupText = document.getElementById("ctl00_ContentMain_txtMatGroupText");
	var txtMatGroupID = document.getElementById("ctl00_ContentMain_txtMatGroupID");
	var divMatGroupName = document.getElementById("ctl00_ContentMain_divMatGroupName");

	function ucMatGroupFinder1_OnSelected(MatGroupID,MatGroupText){
		txtMatGroupText.value = MatGroupText;
		txtMatGroupID.value = MatGroupID;
		divMatGroupName.innerHTML = MatGroupText;
		//__doPostBack("ctl00$ContentMain$btnSubmit","");
	}

	function ucMatGroupTree_OnNodeClick(NodeText,MatGroupID){
		txtMatGroupText.value = NodeText;
		txtMatGroupID.value = MatGroupID;
		divMatGroupName.innerHTML = NodeText;
		//__doPostBack("ctl00$ContentMain$btnSubmit","");
	}
	
	function validate(){
		if (txtMatGroupID.value == ""){
			matweb.alert("Please select a material category","User Input Error");
			return false;
		}
		
		return true;
		
	}

</script>


</div>
<!-- ===================================== END MAIN CONTENT ========================================== -->

<table class="tabletight" style="width:100%" >
        <tr>
                <td align="center" class="footer" colspan="3">
                        <br /><br />
                
                        <a href="/membership/regupgrade.aspx" class="footlink"><span class="subscribeLink">Subscribe to Premium Services</span></a><br/>
                
                        <span class="footer"><b>Searches:</b>&nbsp;&nbsp;</span>
                        <a href="/search/AdvancedSearch.aspx" class="footlink"><span class="foot">Advanced</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/search/CompositionSearch.aspx" class="footlink"><span class="foot">Composition</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/search/PropertySearch.aspx" class="footlink"><span class="foot">Property</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/search/MaterialGroupSearch.aspx" class="footlink"><span class="foot">Material&nbsp;Type</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/search/SearchManufacturerName.aspx" class="footlink"><span class="foot">Manufacturer</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/search/SearchTradeName.aspx" class="footlink"><span class="foot">Trade&nbsp;Name</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/search/SearchUNS.aspx" class="footlink"><span class="foot">UNS Number</span></a>
                        <br />
                        <span class="footer"><b>Other&nbsp;Links:</b>&nbsp;&nbsp;</span>
                        <a href="/services/advertising.aspx" class="footlink"><span class="foot">Advertising</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/services/submitdata.aspx" class="footlink"><span class="foot">Submit&nbsp;Data</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/services/databaselicense.aspx" class="footlink"><span class="foot">Database&nbsp;Licensing</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/services/webhosting.aspx" class="footlink"><span class="foot">Web&nbsp;Design&nbsp;&amp;&nbsp;Hosting</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/clickthrough.aspx?addataid=277" class="footlink"><span class="foot">Trade&nbsp;Publications</span></a>

                        <br />
                        <a href="/reference/suppliers.aspx" class="footlink"><span class="foot">Supplier&nbsp;List</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/tools/unitconverter.aspx" class="footlink"><span class="foot">Unit&nbsp;Converter</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/tools/contents.aspx#reference" class="footlink"><span class="foot">Reference</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/reference/link.aspx" class="footlink"><span class="foot">Links</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/help/help.aspx" class="footlink"><span class="foot">Help</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/services/contact.aspx" class="footlink"><span class="foot">Contact&nbsp;Us</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/tools/contents.aspx" class="footlink"><span class="foot" style="color:red;">Site&nbsp;Map</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/reference/faq.aspx" class="footlink"><span class="foot">FAQ</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/index.aspx" class="footlink"><span class="foot">Home</span></a>
                        <br />&nbsp;
                        <div id="fb-root"></div>

<table><tr><td><script src="http://connect.facebook.net/en_US/all.js#xfbml=1"></script><fb:like-box href="https://www.facebook.com/MatWeb.LLC" width="175" show_faces="false" stream="false" header="false"></fb:like-box>

</td><td>

<a href="https://twitter.com/MatWeb" class="twitter-follow-button" data-show-count="false" data-show-screen-name="false">Follow @MatWeb</a> <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>

</td></tr></table>
                </td>
        </tr>
        <tr>
                <td colspan="3"><img src="/images/bluebar.gif" width="100%" height="5" alt="" /></td>
        </tr>
        <tr>
                <td style="background-image:url(/images/gray.gif);"><img src="/images/spacer.gif" width="5" height="1" alt=""/></td>
                <td style="background-image:url(/images/gray.gif);">
                        <span class="footer">
                        Please read our <a href="/reference/terms.aspx" class="footlink"><span class="foot">License Agreement</span></a> regarding materials data and our  <a href="/reference/privacy.aspx" class="footlink"><span class="foot">Privacy Policy</span></a>.
                        Questions or comments about MatWeb? Please contact us at
                        <a href="mailto:webmaster@matweb.com" class="footlink"><span class="foot">webmaster@matweb.com</span></a>. We appreciate your input.
                        <br /><br />
                        The contents of this web site, the MatWeb logo, and "MatWeb" are Copyright 1996-2024
                        by MatWeb, LLC. MatWeb is intended for personal, non-commercial use. The contents, results, and technical data from this site
                        may not be reproduced either electronically, photographically or substantively without permission from MatWeb, LLC.
                        </span>
                        <br />
                </td>

                <td style="background-image:url(/images/gray.gif);"><img src="/images/spacer.gif" width="5" height="1" alt=""/></td>
        </tr>
</table>




<script type="text/javascript">
//<![CDATA[
var ctl00_ContentMain_ucMatGroupTree_msTreeView_ImageArray =  new Array('', '', '', '/WebResource.axd?d=zLp3QJeZSj4-fsnL2_raV70Mk3pr-OdjL_rvrZzXUTCnZ6LU5Dg_Tc5XZg4eZpT9O0gxZJrJcxGb23XJofeGD9JhYLr2wl0VkQZnJOx0T0ShBosq0&t=638313619312541215', '/WebResource.axd?d=vFualGN2ZZ4fny_RX_gYiUTY6FBLUapne4VfY6P3sGiyrOYd-Ji2sWg_olT-exaztendUUEOoH87RS2l44bpZTscfmmv4MKRqJK8rgesmkrNfP2O0&t=638313619312541215', '/WebResource.axd?d=4nLCbQ2Yfc2i_msrQXqKzK8pLQD-GrNVZ7DeXU-La2qatczrgeCLazv0RzIkDDHyYfLFalCBO8s71lRS8yifxTChF9dUJvTLxNEeeFlghyuY0dtM0&t=638313619312541215', '/WebResource.axd?d=uEI6Enq15_q4pXO6g0E7vtAqtD5WIokAEQ2Vz8V3Yi36cefediLfyCwENkPd-w1GxTXu-HkNqcDlrYOw8bxgGqeHJtzc-PBlTa9nrZRfdIyj8fQh0&t=638313619312541215', '/WebResource.axd?d=hzeik1gGhZrfDZXUWCMsBOx5b43bIFor8UCTzm_wuh92b9Z-WQU4_fQCSUw6CaP5tnc9LTU0jl0sy4Y6Cf_c8q8HFXRqeHQuscXIqqLNSZkgl2cu0&t=638313619312541215', '/WebResource.axd?d=6v5qM6VKQYD3XNwwXEy7Tix7dzVdqDJ8QTRIlUPrzXQqNsXQgUwIfmn34ZzRORd_IZj0bF4vkZzID1HuSFE5uKyLgaPYeAYE8VT8HYfb3Rk6zsDw0&t=638313619312541215', '/WebResource.axd?d=d9nh8nEM1YCqIfE9jLzNhPyFIZqARvlFExwnFG0vbvskO-A2qORlhTyUVIxDtSwldGrOp8o9ef8lZ9SRhPQ9rv9TkGV-NVYvH9_yHT6CvWpIPF4S0&t=638313619312541215', '/WebResource.axd?d=7_V5n1KYSQsj6PoSqgIxb0egBsAa91jSXPkbmM6FaiF14-JsNnpUDnfSdxZcvfd0OQCLDtC24pMIXigO-41cL2rsq-0xea-SSG7yZUyRcliFFqe70&t=638313619312541215', '/WebResource.axd?d=TinC6igX1vPDgWh7DstWwBTHTyH4uuybDU4NW47jJD06wHdAO3YyJC_eKK1Zn5oIbwqRo4Cri8YwhA82G-LD0p14yEF1EfoIZvKleYMIuQ2K1hkE0&t=638313619312541215', '/WebResource.axd?d=4B0dvJqmzY4ukTboQq7lmxokRL9NTsoWh4yErB11Tb-ucvMw_p_0mxYVn4U1Uz6Jk795umrO0C4-B-8Yq1Kxxz6uyDT15O2BSCb8349f4YWDdIVy0&t=638313619312541215', '/WebResource.axd?d=TUiibr9hjS_MlNf09dKZECe1z56fbaPbfgJfC5eYJi0TtdBviUcA1JQJLV81aqunzpXysgruhL3ihpgd7ziL5p9SQ86xVLNiUqbYpJnW9Rj7sN1E0&t=638313619312541215', '/WebResource.axd?d=vUBrM2nNhECNbFO0AJKq52p2zWrY5XG8nWgCDL4AfsrexStb0HREDV6GHMvdkAGDInxYtTyWXuxFUIiQv3JX2fScVRwQsrVv5srHrjOIeC3ayAG20&t=638313619312541215', '/WebResource.axd?d=Z-LP5lKcsnv2A9UpExXu-5leQYIAZMYp6SXNognZi1r66YT8u24-YjJaxjisE9ruYU8NKnNz72H4shclsk8PM3S5M-VYMk_F_G5fcd3ra6Sj9CaS0&t=638313619312541215', '/WebResource.axd?d=QI4SACqRn7PxBBHPfkR7peFMAEv7lX1_iZ_7jncKwxRTlvhSAncd5K2oRqw8J8WmJjWQcbGgEblhrufy-hUjZMa1YLUC47mjfV1eS4LM3kUqLT960&t=638313619312541215', '/WebResource.axd?d=ayrFqNSzdiTAdowDVbEH-ZzKbvw5bCq7onyCSAcJhCVPVIHlUB9M_fo8BLUTeGpbtcoMiyFy1MoeEIsweMrpVandBKC4vAK-NCBcfn2FIreK8mEJ0&t=638313619312541215', '/WebResource.axd?d=OK_uWiuoGURaJ3CC0fVlQ7v9GUnIAkx03w03rQ2ENqcwdObLiQwK7B9yRFyAWj9tuUqsa0874ksgkaRo8KirB7q1ZL_huh7gCq_XehXfHMEKMiAl0&t=638313619312541215');
//]]>
</script>


<script type="text/javascript">
//<![CDATA[
(function() {var fn = function() {AjaxControlToolkit.ModalPopupBehavior.invokeViaServer('ctl00_ContentMain_ucPopupMessage1_ModalPopupExtender1', false); Sys.Application.remove_load(fn);};Sys.Application.add_load(fn);})();
var callBackFrameUrl='/WebResource.axd?d=Mt7MojvAyTqAEcx0MpKgcF_QVa09tLyMQS6c5JLNO4j2M_F6CMxYnN0HdVYxyrhggOpkTwyNqQ51iS6UoHXgJqHBROE1&t=638313619312541215';
WebForm_InitCallback();var ctl00_ContentMain_ucMatGroupTree_msTreeView_Data = new Object();
ctl00_ContentMain_ucMatGroupTree_msTreeView_Data.images = ctl00_ContentMain_ucMatGroupTree_msTreeView_ImageArray;
ctl00_ContentMain_ucMatGroupTree_msTreeView_Data.collapseToolTip = "Collapse {0}";
ctl00_ContentMain_ucMatGroupTree_msTreeView_Data.expandToolTip = "Expand {0}";
ctl00_ContentMain_ucMatGroupTree_msTreeView_Data.expandState = theForm.elements['ctl00_ContentMain_ucMatGroupTree_msTreeView_ExpandState'];
ctl00_ContentMain_ucMatGroupTree_msTreeView_Data.selectedNodeID = theForm.elements['ctl00_ContentMain_ucMatGroupTree_msTreeView_SelectedNode'];
for (var i=0;i<19;i++) {
var preLoad = new Image();
if (ctl00_ContentMain_ucMatGroupTree_msTreeView_ImageArray[i].length > 0)
preLoad.src = ctl00_ContentMain_ucMatGroupTree_msTreeView_ImageArray[i];
}
ctl00_ContentMain_ucMatGroupTree_msTreeView_Data.lastIndex = 8;
ctl00_ContentMain_ucMatGroupTree_msTreeView_Data.populateLog = theForm.elements['ctl00_ContentMain_ucMatGroupTree_msTreeView_PopulateLog'];
ctl00_ContentMain_ucMatGroupTree_msTreeView_Data.treeViewID = 'ctl00$ContentMain$ucMatGroupTree$msTreeView';
ctl00_ContentMain_ucMatGroupTree_msTreeView_Data.name = 'ctl00_ContentMain_ucMatGroupTree_msTreeView_Data';
Sys.Application.initialize();
Sys.Application.add_init(function() {
    $create(AjaxControlToolkit.ModalPopupBehavior, {"BackgroundCssClass":"modalBackground","OkControlID":"ctl00_ContentMain_ucPopupMessage1_btnOK","PopupControlID":"ctl00_ContentMain_ucPopupMessage1_divPopupMessage","PopupDragHandleControlID":"ctl00_ContentMain_ucPopupMessage1_trTitleRow","dynamicServicePath":"/search/MaterialGroupSearch.aspx","id":"ctl00_ContentMain_ucPopupMessage1_ModalPopupExtender1"}, null, null, $get("ctl00_ContentMain_ucPopupMessage1_hndPopupControl"));
});
Sys.Application.add_init(function() {
    $create(AjaxControlToolkit.TextBoxWatermarkBehavior, {"ClientStateFieldID":"ctl00_ContentMain_UcMatGroupFinder1_TextBoxWatermarkExtender2_ClientState","WatermarkCssClass":"greyOut","WatermarkText":"Type at least 4 characters here...","id":"ctl00_ContentMain_UcMatGroupFinder1_TextBoxWatermarkExtender2"}, null, null, $get("ctl00_ContentMain_UcMatGroupFinder1_txtSearchText"));
});
//]]>
</script>
</form>


<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-15290815-1");
pageTracker._setDomainName(".matweb.com");
pageTracker._trackPageview();
} catch(err) {}</script>

<!-- 20160905 insert SpecialChem javascript -->
<script type="text/javascript" src="//collect.specialchem.com/collect.js"></script>
<script type="text/javascript">
    //<![CDATA[
    var _spc = (_spc || []);
    _spc.push(['init',
    {
        partner: 'MatWeb'
        
        //, iid: '12345'
    }]);
    //]]>
</script>
<script> (function(){ var s = document.createElement('script'); var h = document.querySelector('head') || document.body; s.src = 'https://acsbapp.com/apps/app/dist/js/app.js'; s.async = true; s.onload = function(){ acsbJS.init({ statementLink : '', footerHtml : '', hideMobile : false, hideTrigger : false, disableBgProcess : false, language : 'en', position : 'right', leadColor : '#146FF8', triggerColor : '#146FF8', triggerRadius : '50%', triggerPositionX : 'right', triggerPositionY : 'bottom', triggerIcon : 'people', triggerSize : 'bottom', triggerOffsetX : 20, triggerOffsetY : 20, mobile : { triggerSize : 'small', triggerPositionX : 'right', triggerPositionY : 'bottom', triggerOffsetX : 20, triggerOffsetY : 20, triggerRadius : '20' } }); }; h.appendChild(s); })();</script>
</body>
</html>
