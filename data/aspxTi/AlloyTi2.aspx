

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head id="ctl00_mainHeader"><title>
	Metal, Plastic, and Ceramic Search Index
</title><meta http-equiv="Content-Style-Type" content="text/css" /><meta http-equiv="Content-Language" content="en" />

        <script src="/includes/flash.js" type="text/javascript"></script>

        <script src="/includes/prototype.js" type="text/javascript"></script>
        <!-- script src="/includes/scriptaculous/effects.js" type="text/javascript"></script -->
        <script src="/includes/matweb.js" type="text/javascript"></script>
        <link rel="stylesheet" href="/includes/main.css" />

        <script type="text/javascript">

                function _onload(){
                        matweb.debug = false; // ;
                        //matweb.writeOverlay();
                }

                function _onkeydown(evt){
                        //PREVENT GLOBAL AUTO-SUBMIT OF FORMS
                        return matweb.DisableAutoSubmit(evt);
                }

                //EXAMPLE USAGE:
                //matweb.register_onload(function(){alert("hello from onload event");});
                //matweb.writeOverlay();


        </script>

        <style type="text/css" media="screen">
        @import url("/ad_IES/css/styles.css");
        </style>
<meta name="KEYWORDS" content="carbon steels, low alloy steels, corrosion resistant stainless, high temperature stainless, heat resistant stainless steels, AISI steel alloy specifications, high strength steel alloys, tensile strength of steels, maraging, superalloy, lead alloy, magnesium, tin alloy, tungsten, zinc, precious metal, pure metallic element, TPE elastomer, dielectric constant, plastic dissipation factor, nylon glass transition temperature, ionomer vicat softening point, poisson's ratio, polyetherimide, fluoropolymer, silicone, polyester dielectric constant" /><meta name="DESCRIPTION" content="Searchable list of plastics, metals, and ceramics categorized into a fields like polypropylene, nylon, ABS, aluminum alloys, oxides, etc.  Search results are detailed data sheets with tensile strength, density, dielectric constant, dissipation factor, poisson's ratio, glass transition temperature, and Vicat softening point." /><meta name="ROBOTS" content="NOFOLLOW" /><style type="text/css">
	.ctl00_ContentMain_ucMatGroupTree_msTreeView_0 { text-decoration:none; }
	.ctl00_ContentMain_ucMatGroupTree_msTreeView_1 { color:Black; }
	.ctl00_ContentMain_ucMatGroupTree_msTreeView_2 { padding:0px 3px 0px 3px; }

</style></head>

<body onload="_onload();" onkeydown="return _onkeydown(event);">

<a id="Top"></a>

<div id="divSiteName" style="position:absolute; top:0; left:0 ;">
<a href="/index.aspx"><img src="/images/sitelive.gif" alt="" /></a>
</div>

<div id="divPopupOverlay" style="display:none;"></div>

<div id="divPopupAlert" class="popupAlert" style="display:none;">
        <table style="width:100%; height:100%; border-style:outset; border-width:5px; border-color:blue;" >
                <tr><th style=" height:25px; background-color:Maroon; font-size:14px; color:White; "><span id="spanPopupAlertTitle">System Message</span></th></tr>
                <tr><td style=" vertical-align:top; padding:3px; background-color:White;" id="tdPopupAlertMessage" ></td></tr>
                <tr><td style=" height:25px; text-align:center; background-color:Silver;"><input type="button" id="btnPopupAlert" onclick="matweb.alertClose();" style="" value="OK" /></td></tr>
        </table>
</div>

<form name="aspnetForm" method="post" action="MaterialGroupSearch.aspx" onsubmit="javascript:return WebForm_OnSubmit();" id="aspnetForm">
<div>
<input type="hidden" name="__EVENTTARGET" id="__EVENTTARGET" value="" />
<input type="hidden" name="__EVENTARGUMENT" id="__EVENTARGUMENT" value="" />
<input type="hidden" name="__LASTFOCUS" id="__LASTFOCUS" value="" />
<input type="hidden" name="ctl00_ContentMain_ucMatGroupTree_msTreeView_ExpandState" id="ctl00_ContentMain_ucMatGroupTree_msTreeView_ExpandState" value="ccccccnc" />
<input type="hidden" name="ctl00_ContentMain_ucMatGroupTree_msTreeView_SelectedNode" id="ctl00_ContentMain_ucMatGroupTree_msTreeView_SelectedNode" value="" />
<input type="hidden" name="ctl00_ContentMain_ucMatGroupTree_msTreeView_PopulateLog" id="ctl00_ContentMain_ucMatGroupTree_msTreeView_PopulateLog" value="" />
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="/wEPDwULLTE2Nzc3MDc2MDAPZBYCZg9kFgICAQ9kFioCAQ9kFgJmDxYCHgRUZXh0BWdEYXRhIHNoZWV0cyBmb3Igb3ZlciA8c3BhbiBjbGFzcz0iZ3JlZXRpbmciPjE4MCwwMDA8L3NwYW4+ICBtZXRhbHMsIHBsYXN0aWNzLCBjZXJhbWljcywgYW5kIGNvbXBvc2l0ZXMuZAICD2QWAmYPFgIfAAUESE9NRWQCAw9kFgJmDxYCHwAFBlNFQVJDSGQCBA9kFgJmDxYCHwAFBVRPT0xTZAIFD2QWAmYPFgIfAAUJU1VQUExJRVJTZAIGD2QWAmYPFgIfAAUHRk9MREVSU2QCBw9kFgJmDxYCHwAFCEFCT1VUIFVTZAIID2QWAmYPFgIfAAUDRkFRZAIJD2QWAmYPFgIfAAUHQUNDT1VOVGQCCg9kFgJmDxYCHwAFBkxPRyBJTmQCCw8QZGQWAWZkAgwPZBYCZg8WAh8ABQhTZWFyY2hlc2QCDQ9kFgJmDxYCHwAFCEFkdmFuY2VkZAIOD2QWAmYPFgIfAAUIQ2F0ZWdvcnlkAg8PZBYCZg8WAh8ABQhQcm9wZXJ0eWQCEA9kFgJmDxYCHwAFBk1ldGFsc2QCEQ9kFgJmDxYCHwAFClRyYWRlIE5hbWVkAhIPZBYCZg8WAh8ABQxNYW51ZmFjdHVyZXJkAhMPZBYCAgEPZBYCZg8WAh8ABRlSZWNlbnRseSBWaWV3ZWQgTWF0ZXJpYWxzZAIVD2QWEAIDD2QWAmYPFgIfAAWZATxhIGhyZWY9Ii9jbGlja3Rocm91Z2guYXNweD9hZGRhdGFpZD0xMzUwIj48aW1nIHNyYz0iL2ltYWdlcy9hc3NldHMvbWV0YWxtZW4tVGl0YW5pdW0ucG5nIiBib3JkZXI9MCBhbHQ9Ik1ldGFsbWVuIFNhbGVzIiB3aWR0aCA9ICI3MjgiIGhlaWdodCA9ICI5MCI+PC9hPmQCBA9kFgICAg8WAh4Fc3R5bGUFGWRpc3BsYXk6bm9uZTt3aWR0aDoyNTBweDsWBgIBD2QWAmYPZBYCZg8PFgIfAAUSVXNlciBJbnB1dCBQcm9ibGVtZGQCAw8PFgIfAGVkZAIFDxYCHgV2YWx1ZQUCT0tkAgUPZBYEAgEPD2QWAh8BBQxXaWR0aDozMDRweDtkAgIPEBYEHwEFK3Bvc2l0aW9uOmFic29sdXRlO2Rpc3BsYXk6bm9uZTtXaWR0aDozMDhweDseBHNpemUFAjE4ZGRkAgYPDxYEHghDbGllbnRJRAIBHgpMYW5ndWFnZUlEAgFkFgJmDxQrAAkPFg4eF1BvcHVsYXRlTm9kZXNGcm9tQ2xpZW50Zx4SRW5hYmxlQ2xpZW50U2NyaXB0Zx4JU2hvd0xpbmVzZx4ITm9kZVdyYXBnHg1OZXZlckV4cGFuZGVkZB4MU2VsZWN0ZWROb2RlZB4JTGFzdEluZGV4AghkFggeC05vZGVTcGFjaW5nGwAAAAAAAAAAAQAAAB4JRm9yZUNvbG9yCiMeEUhvcml6b250YWxQYWRkaW5nGwAAAAAAAAhAAQAAAB4EXyFTQgKEgBhkZGRkZGQUKwAJBSMyOjAsMDowLDA6MSwwOjIsMDozLDA6NCwwOjUsMDo2LDA6NxQrAAIWDB8ABRJDYXJib24gKDg2NiBtYXRscykeBVZhbHVlBQMyODMeCEV4cGFuZGVkaB4MU2VsZWN0QWN0aW9uCyouU3lzdGVtLldlYi5VSS5XZWJDb250cm9scy5UcmVlTm9kZVNlbGVjdEFjdGlvbgIeC05hdmlnYXRlVXJsBUFqYXZhc2NyaXB0OnVjTWF0R3JvdXBUcmVlX09uTm9kZUNsaWNrKCdDYXJib24gKDg2NiBtYXRscyknLCcyODMnKR4QUG9wdWxhdGVPbkRlbWFuZGdkFCsAAhYMHwAFFUNlcmFtaWMgKDEwMDA0IG1hdGxzKR8RBQIxMR8SaB8TCysEAh8UBUNqYXZhc2NyaXB0OnVjTWF0R3JvdXBUcmVlX09uTm9kZUNsaWNrKCdDZXJhbWljICgxMDAwNCBtYXRscyknLCcxMScpHxVnZBQrAAIWDB8ABRJGbHVpZCAoNzU2MiBtYXRscykfEQUBNR8SaB8TCysEAh8UBT9qYXZhc2NyaXB0OnVjTWF0R3JvdXBUcmVlX09uTm9kZUNsaWNrKCdGbHVpZCAoNzU2MiBtYXRscyknLCc1JykfFWdkFCsAAhYMHwAFE01ldGFsICgxNzA1MiBtYXRscykfEQUBOR8SaB8TCysEAh8UBUBqYXZhc2NyaXB0OnVjTWF0R3JvdXBUcmVlX09uTm9kZUNsaWNrKCdNZXRhbCAoMTcwNTIgbWF0bHMpJywnOScpHxVnZBQrAAIWDB8ABSdPdGhlciBFbmdpbmVlcmluZyBNYXRlcmlhbCAoODA2MyBtYXRscykfEQUDMjg1HxJoHxMLKwQCHxQFVmphdmFzY3JpcHQ6dWNNYXRHcm91cFRyZWVfT25Ob2RlQ2xpY2soJ090aGVyIEVuZ2luZWVyaW5nIE1hdGVyaWFsICg4MDYzIG1hdGxzKScsJzI4NScpHxVnZBQrAAIWDB8ABRVQb2x5bWVyICg5NzYzNSBtYXRscykfEQUCMTAfEmgfEwsrBAIfFAVDamF2YXNjcmlwdDp1Y01hdEdyb3VwVHJlZV9Pbk5vZGVDbGljaygnUG9seW1lciAoOTc2MzUgbWF0bHMpJywnMTAnKR8VZ2QUKwACFgwfAAUYUHVyZSBFbGVtZW50ICg1MDcgbWF0bHMpHxEFAzE4NB8SaB8TCysEAh8UBUdqYXZhc2NyaXB0OnVjTWF0R3JvdXBUcmVlX09uTm9kZUNsaWNrKCdQdXJlIEVsZW1lbnQgKDUwNyBtYXRscyknLCcxODQnKR8VaGQUKwACFgwfAAUlV29vZCBhbmQgTmF0dXJhbCBQcm9kdWN0cyAoMzk4IG1hdGxzKR8RBQMyNzcfEmgfEwsrBAIfFAVUamF2YXNjcmlwdDp1Y01hdEdyb3VwVHJlZV9Pbk5vZGVDbGljaygnV29vZCBhbmQgTmF0dXJhbCBQcm9kdWN0cyAoMzk4IG1hdGxzKScsJzI3NycpHxVnZGQCBw8WAh4JaW5uZXJodG1sBRpUaXRhbml1bSBBbGxveSAoMzg2IG1hdGxzKWQCDQ8PFgIeB1Zpc2libGVoZGQCDg8PFgIfF2dkFgQCAQ8PFgIfAAUDNDI5ZGQCAw8PFgIfAAUOVGl0YW5pdW0gQWxsb3lkZAIPDw8WBh4LQ3VycmVudFBhZ2UCAh4PQ3VycmVudFNlYXJjaElEAoOnhhMfF2dkFgQCAg8PFgIfAGVkZAIDDw8WAh8XZ2QWPAIBDw8WAh8ABQM0MjlkZAIDDxBkEBUDATEBMgEzFQMBMQEyATMUKwMDZ2dnFgECAWQCBQ8PFgIfAAUBM2RkAgcPDxYIHwAFC1tQcmV2IFBhZ2VdHw4KIx4HRW5hYmxlZGcfEAIEZGQCCQ8PFgYfAAULW05leHQgUGFnZV0fDgojHxACBGRkAgsPEGRkFgECBWQCDw8WAh8XaGQCEQ8QD2QWAh4Ib25jaGFuZ2UFE1NldFNlbGVjdGVkRm9sZGVyKClkZGQCFQ8WAh8XaBYCAgEPEGRkFgFmZAIXD2QWBAIBDw8WBB8AZR8aaGRkAgIPDxYCHxdoZGQCGQ8PFgQfAAUNTWF0ZXJpYWwgTmFtZR8aaGRkAhsPDxYCHxdoZGQCHQ8PFgIfF2hkZAIfD2QWBgIBDw8WBB8ABQVQcm9wMR8aaGRkAgMPDxYCHxdoZGQCBQ8PFgIfF2hkZAIhD2QWBgIBDw8WBB8ABQVQcm9wMh8aaGRkAgMPDxYCHxdoZGQCBQ8PFgIfF2hkZAIjD2QWBgIBDw8WBB8ABQVQcm9wMx8aaGRkAgMPDxYCHxdoZGQCBQ8PFgIfF2hkZAIlD2QWBgIBDw8WBB8ABQVQcm9wNB8aaGRkAgMPDxYCHxdoZGQCBQ8PFgIfF2hkZAInD2QWBgIBDw8WBB8ABQVQcm9wNR8aaGRkAgMPDxYCHxdoZGQCBQ8PFgIfF2hkZAIpD2QWBgIBDw8WBB8ABQVQcm9wNh8aaGRkAgMPDxYCHxdoZGQCBQ8PFgIfF2hkZAIrD2QWBgIBDw8WBB8ABQVQcm9wNx8aaGRkAgMPDxYCHxdoZGQCBQ8PFgIfF2hkZAItD2QWBgIBDw8WBB8ABQVQcm9wOB8aaGRkAgMPDxYCHxdoZGQCBQ8PFgIfF2hkZAIvD2QWBgIBDw8WBB8ABQVQcm9wOR8aaGRkAgMPDxYCHxdoZGQCBQ8PFgIfF2hkZAIxD2QWBgIBDw8WBB8ABQZQcm9wMTAfGmhkZAIDDw8WAh8XaGRkAgUPDxYCHxdoZGQCNQ8PFgIfAAUDNDI5ZGQCNw8QZBAVAwExATIBMxUDATEBMgEzFCsDA2dnZxYBAgFkAjkPDxYCHwAFATNkZAI7Dw8WCB8ABQtbUHJldiBQYWdlXR8OCiMfGmcfEAIEZGQCPQ8PFgYfAAULW05leHQgUGFnZV0fDgojHxACBGRkAj8PEGRkFgECBWQCQQ8WAh8XZxYCAgEPDxYCHwAFuAQ8YnIgLz4NCk1hdGVyaWFscyBmbGFnZ2VkIGFzIGRpc2NvbnRpbnVlZCAoPGltZyBzcmM9Ii9pbWFnZXMvYnV0dG9ucy9pY29uRGlzY29udGludWVkLmpwZyIgYWx0PSIiIC8+KSBhcmUgbm8gbG9uZ2VyIHBhcnQgb2YgdGhlIG1hbnVmYWN0dXJlcuKAmXMgc3RhbmRhcmQgcHJvZHVjdCBsaW5lIGFjY29yZGluZyB0byBvdXIgbGF0ZXN0IGluZm9ybWF0aW9uLiAgVGhlc2UgbWF0ZXJpYWxzIG1heSBiZSBhdmFpbGFibGUgYnkgc3BlY2lhbCBvcmRlciwgaW4gZGlzdHJpYnV0aW9uIGludmVudG9yeSwgb3IgcmVpbnN0YXRlZCBhcyBhbiBhY3RpdmUgcHJvZHVjdC4gIERhdGEgc2hlZXRzIGZyb20gbWF0ZXJpYWxzIHRoYXQgYXJlIG5vIGxvbmdlciBhdmFpbGFibGUgcmVtYWluIGluIE1hdFdlYiB0byBhc3Npc3QgdXNlcnMgaW4gZmluZGluZyByZXBsYWNlbWVudCBtYXRlcmlhbHMuICANCjxiciAvPjxiciAvPg0KVXNlcnMgb2Ygb3VyIEFkdmFuY2VkIFNlYXJjaCAocmVnaXN0cmF0aW9uIHJlcXVpcmVkKSBtYXkgZXhjbHVkZSBkaXNjb250aW51ZWQgbWF0ZXJpYWxzIGZyb20gc2VhcmNoIHJlc3VsdHMuZGQCFg9kFgJmDxYCHwAFcDxhIGhyZWY9Ii9jbGlja3Rocm91Z2guYXNweD9hZGRhdGFpZD0yNzciIGNsYXNzPSJmb290bGluayI+PHNwYW4gY2xhc3M9ImZvb3QiPlRyYWRlJm5ic3A7UHVibGljYXRpb25zPC9zcGFuPjwvYT5kGAEFHl9fQ29udHJvbHNSZXF1aXJlUG9zdEJhY2tLZXlfXxYEBTZjdGwwMCRDb250ZW50TWFpbiRVY01hdEdyb3VwRmluZGVyMSRzZWxlY3RDYXRlZ29yeUxpc3QFK2N0bDAwJENvbnRlbnRNYWluJHVjTWF0R3JvdXBUcmVlJG1zVHJlZVZpZXcFG2N0bDAwJENvbnRlbnRNYWluJGJ0blN1Ym1pdAUaY3RsMDAkQ29udGVudE1haW4kYnRuUmVzZXSyBtOj59Ra95PIBNaFLp2HlAWE2w==" />
</div>

<script type="text/javascript">
//<![CDATA[
var theForm = document.forms['aspnetForm'];
if (!theForm) {
    theForm = document.aspnetForm;
}
function __doPostBack(eventTarget, eventArgument) {
    if (!theForm.onsubmit || (theForm.onsubmit() != false)) {
        theForm.__EVENTTARGET.value = eventTarget;
        theForm.__EVENTARGUMENT.value = eventArgument;
        theForm.submit();
    }
}
//]]>
</script>


<script src="/WebResource.axd?d=M68BSyLvwBMHRYzsSV62mvczr7w5VyKnq2JbgqcAAU1ioThlC6FNEVBncBdaA4oqf8cXPzy7QERzajvFfM67hSR6S0w1&amp;t=638313619312541215" type="text/javascript"></script>


<script src="/WebResource.axd?d=ClzPoKUn1iQnMGy7NeMJw70TvMayZ5w9nNmB7h_dY64i4BFFgjVcppyp69BFqhuzHctzc-rEantAjczzQ6UVMYkaWJw1&amp;t=638313619312541215" type="text/javascript"></script>
<script type="text/javascript">
//<![CDATA[

    function TreeView_PopulateNodeDoCallBack(context,param) {
        WebForm_DoCallback(context.data.treeViewID,param,TreeView_ProcessNodeData,context,TreeView_ProcessNodeData,false);
    }
var ctl00_ContentMain_ucMatGroupTree_msTreeView_Data = null;//]]>
</script>

<script src="/ScriptResource.axd?d=148LWaywfmset9PYVR0m-KYXX9JfyGYGqOtIYBGEAzoJh5VhFBwSI6JxnmoNvgtlAprhvntpc6lTDbRMgdKxckUmc8m2qw_fOCsX0G9LC8ojUpw4cTHWXgfuT_vU6m2Kpv3mt01iCczrTE2-J5X6EeBa2281&amp;t=637769794779625837" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=v_V1UgDrDAchg8Zuc1FL4qkvmjt-bwap7ZxwVGGPleYiJ6zfDOTL6wtCmVvILjbwo_sqb2PMUTNkjO4vp6nd-E7WWXTPgSI5-nvn3YDnHrVk5bgVX4Qv6F0fV4wTSslmaLZ3k98tckojDLWAEI7jm8h0H4d7BoGYxkqsyh8-Iujfrjjm0&amp;t=637769794779625837" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=V7Zma3AISNyGTbHVRPw8svJ4Ub7xUUaeoKwImcvnuDiy-XBR331l3eTjiEP8J_QlX2qgyAHxZFLywNbNyZ50fJLVEOCjx11lIDJOiQxc0oGA3qi7XXiggiwEcIWwzb9j2Is1n1D9sU40Zusk35AtkqRKL3Y1&amp;t=633177911400000000" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=wOguQo_yd3hCG4ajcSXPC6-XdeL0hmYWSMaFjvMXt1CBqR9neJqTXyTfPki4lIz4B29Zz_83btvS12BY6KpBKT-AgP0eaXp7V3h6XRdjiozV5w_n3oQRZg5Yq1QW8UNGj0Cy9jsap_7EcU2M99CapUyVG6I1&amp;t=633177911400000000" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=MAJxSy62Z3EtRwHXuuMc8KC6PFmrPqmkpcwdpcPGeqtqUQyJ3FBQ3cFyYWN1zNID_ngA53aMZdG_dpRu3TRDH8-iqKAjPAcc2BWe63WYGrGbyHc9syJxR6lypTgvRaYmwwEmkLEsaGUniMurHc16yr1VrjmzG3HWOeu9UXNr23Wv2xzh0&amp;t=633177911400000000" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=ukx9eyyt483y_CGgCF1OToQPWzPYyCynhpMvhxN7idzW3WaKjHjpgAOcMgtXZIEcspnbyDEADdRWCSHB-hF39Ycl13KWsqdlOHElw2qgSkGmkL8iNH37htb0BNS0lxw6oHvtqYiVpw-nZeYoQ1NkgX2qXOI1&amp;t=633177911400000000" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=Ovkr-MWDypjnq0C7FVbNgOs-Mt3NaktNVf8SEOX721PoQ5mQqTM6S4igSQL1_PXvh96km3stLiqrorOgm8nnnUU6y6edTkhxmLiwfFtEXQEAAnFFQpUepgsxEd0ahMvWcDfEac0EE7J8iDb2rqSMTxk51ydfY-c-8zcKvo5Mrn83cvfN0&amp;t=633177911400000000" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=pKQF4iQYwkSn5cAwTNDM8ez4TLepowWnai-WD6NVL5h0ay9Qtt0a9vPCaI-KN9OHcv1fL8VnwxEDM6EAVtgZY06V8XJTlVrbmb8WlMvGmhwBeL3_SVtKKOS-QExY8AnVbTFl_wOknplhy8bz-9sIWNCh4XPq1WhRj7ALNAKaeruPhS7d0&amp;t=633177911400000000" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=uM6eUuIuIa_NiCgVrtwgqczDeqTqI37XalY8Ri_W2ZjXEzIKi84afVBjFk8gneIDXcFnw8yjCD1qhE8mzDTzAUSdKnbDte0yC23kMoyvxPyu7vRH9E-6JMZtE8-gELUPmoZdOZJ9nXitHKYBLw2P6skU1IkzFRegK74bETu2FdEtuaeR0&amp;t=633177911400000000" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=ewF5FMb1ckbNCdJ9rpE5xMSXy-WecqVGE8uma1_520KusuW7jTUE2tnPMG7JyR0FDIFqDUMQbsfTZlsQ0sD4YAW8MnN90GKIMPvzaP7eOG3ss8E050_l7Rpnaz7I_U3ITzBXkcJZIOPcI1PxOReT8FSeWmo1&amp;t=633177911400000000" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=3lB_EBCMPyiwzDhzWdoE-i4mLHhkIMbbkaLwMr-U1WcAoDqBHS-oYFn4gz3ZwnpzK_KZp97Ev8ZbiUNo7WMJ45h2xgOH59erMFo1BpNb87-yu3X0mFyWs5rB3ANm4lsVSIu7RZd7XUEwfZXzugJ3CoJqxvRqiTGLpcXquEF24hK3OUH-0&amp;t=633177911400000000" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=Z7-pZSKJ6vCyr9H9S4r9baB7TLiJ-T4EoMOvlH-iQ5-icaO-vWMrjXk3EPctuPrcO5qwJSTaLQtHyguwle7Kty78exrSwHataIriNUEu1DnUkixv4pxU7Dknp_GDVHby5XcMA4lMwazAE0AFG9Cndu0An_gAGACGlYXsGUG9W6okCQsH0&amp;t=633177911400000000" type="text/javascript"></script>
<script src="../WebServices/Materials.asmx/js" type="text/javascript"></script>
<script src="../WebServices/MatGroups.asmx/js" type="text/javascript"></script>
<script src="../WebServices/Folders.asmx/js" type="text/javascript"></script>
<script type="text/javascript">
//<![CDATA[
function WebForm_OnSubmit() {
null;
return true;
}
//]]>
</script>

<div>

	<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="640EFF8C" />
</div>

<!--
The ScriptManager control is required for all ASP AJAX functionality - especially used by search pages.
It generates the javascript required.
Only one script manager is allowed on a page, and since this is the master page, this is the main script manager
However, a ScriptManagerProxy control can be used to set properties and settings on indivual pages.
For an example, see /WebControls/ucSearchResults.ascx
-->
<script type="text/javascript">
//<![CDATA[
Sys.WebForms.PageRequestManager._initialize('ctl00$ajxScriptManager', document.getElementById('aspnetForm'));
Sys.WebForms.PageRequestManager.getInstance()._updateControls([], [], [], 90);
//]]>
</script>


<table class="tabletight" style="width:100%; text-align:center;background-image: url(/images/gray.gif);">
  <tr >
    <td rowspan="2"><a href="/index.aspx"><img src="/images/logo1.gif" width="270" height="42" alt="MatWeb - Material Property Data"  /></a></td>

                
                <td style="text-align:right;vertical-align:middle; width:90%" ><span class='greeting'><span class='hiText'><a href ='/services/advertising.aspx'>Advertise with MatWeb!</a></span>&nbsp;&nbsp;&nbsp;&nbsp;</span></td>
                <td style="text-align:right;vertical-align:middle; padding-right:5px;" ><a href='/membership/regstart.aspx'><img src='/images/buttons/btnRegisterBSF.gif' width="62" height="20" alt="Register Now" /></a></td>

                
  </tr>
  <tr >
    <td  align="left" valign="bottom" colspan="2"><span class="tagline">
    
    Data sheets for over <span class="greeting">180,000</span>  metals, plastics, ceramics, and composites.</span></td>
  </tr>
  <tr style="background-image:url(/images/blue.gif); background-color:#000099; " >
    <td bgcolor="#000099"><a href="/index.aspx"><img src="/images/logo2.gif" width="270" height="23px" alt="MatWeb - Material Property Data" border="0" /></a></td>
    <td class="topnavig8" style=" background-color:#000099;vertical-align:middle; text-align:right; white-space:nowrap;" colspan="2" align="right" valign="middle">
        <a href="/index.aspx" class="topnavlink">HOME</a>&nbsp;&nbsp;&#8226;&nbsp;&nbsp;
                        <a href="/search/search.aspx" class="topnavlink">SEARCH</a>&nbsp;&nbsp;&#8226;&nbsp;&nbsp;
                        <a href="/tools/tools.aspx" class="topnavlink">TOOLS</a>&nbsp;&nbsp;&#8226;&nbsp;&nbsp;
                        <a href="/reference/suppliers.aspx" class="topnavlink">SUPPLIERS</a>&nbsp;&nbsp;&#8226;&nbsp;&nbsp;
                        <a href="/folders/ListFolders.aspx" class="topnavlink">FOLDERS</a>&nbsp;&nbsp;&#8226;&nbsp;&nbsp;
                        <a href="/services/services.aspx" class="topnavlink">ABOUT US</a>&nbsp;&nbsp;&#8226;&nbsp;&nbsp;
                        <a href="/reference/faq.aspx" class="topnavlink">FAQ</a>&nbsp;&nbsp;&#8226;&nbsp;&nbsp;
                        
                        <a href="/membership/login.aspx" class="topnavlink">LOG IN</a>
                        
                        &nbsp;
                        
                        &nbsp;
    </td>
  </tr>
</table>

<div id="divRecentMatls" class="tableborder tight" style=" display:none; position:absolute; top:95px; left:150px; width:400px; height:400px;" onmouseover="clearTimeout(tmrtmp);" onmouseout="tmrtmp=setTimeout('RecentMatls.Hide()', 800)">
        <table class="headerblock tabletight" style="vertical-align:top; font-size:13px; width:100%;"><tr>
                <td style="padding:3px;"> Recently Viewed Materials (most recent at top)</td>
                <td style="padding:3px;text-align:right;"><input type="image" src="/images/buttons/btnCloseWhite.gif" onclick="RecentMatls.Hide();return false;" style="cursor:pointer;" />&nbsp;</td>
        </tr></table>
        <div id="divRecentMatlsBody" style="height:375px; overflow:auto; background-color:White;">

                <table id="tblRecentMatls" class="tabledataformat tableloose" style="background-color:White;">
    
                <tr><td>
                <p />
                <a href="/membership/login.aspx">Login</a> to see your most recently viewed materials here.<p />
                Or if you don't have an account with us yet, then <a href="/membership/regstart.aspx">click here to register.</a>
                </td></tr>
                
                </table>

        </div>
</div>

<script type="text/javascript">

        var RecentMatls = new _RecentMatls();
        var tmrtmp;

        function _RecentMatls(){

                var divRecentMatls;
                var tblRecentMatls;
                var trRecentMatlsRowTemplate;
                var trRecentMatlsNoData;
                var DataIsLoaded=false;

                //Using prototype.js methods...
                divRecentMatls = $("divRecentMatls");
                tblRecentMatls = $("tblRecentMatls");
    

                divRecentMatls.style.display = "none";

                this.Toggle = function(){
                        //alert("Toggle()");
                        if(divRecentMatls.style.display == "block"){this.Hide();}
                        else{this.Show();}
                }

                this.Show = function(){
                        //alert("Show()");
                        divRecentMatls.style.display = "block";
                        matweb.toggleSelect(divRecentMatls,0);
                        if(!DataIsLoaded){
            
                        }
                }

                this.Hide = function(){
                        //alert("Hide()");
                        divRecentMatls.style.display = "none";
                        matweb.toggleSelect(divRecentMatls,1);
                }

    

        }//end class

</script>

<table class="tabletight t_ableborder t_ablegrid" style="width:100%;" >
  <tr>
   <td style="width:100%; height:27px; white-space:nowrap; text-align:left; vertical-align:middle;" class="tagline" >
      <span class="navlink"><b>&nbsp;&nbsp;Searches:</b></span>
      &nbsp;&nbsp;<a href="/search/AdvancedSearch.aspx" class="navlink"><span class="nav"><font color="#CC0000">Advanced</font></span></a>
      &nbsp;|&nbsp;<a href="/search/MaterialGroupSearch.aspx" class="navlink"><span class="nav">Category</span></a>
      &nbsp;|&nbsp;<a href="/search/PropertySearch.aspx" class="navlink"><span class="nav">Property</span></a>
      &nbsp;|&nbsp;<a href="/search/CompositionSearch.aspx" class="navlink"><span class="nav">Metals</span></a>
      &nbsp;|&nbsp;<a href="/search/SearchTradeName.aspx" class="navlink"><span class="nav">Trade Name</span></a>
      &nbsp;|&nbsp;<a href="/search/SearchManufacturerName.aspx" class="navlink"><span class="nav">Manufacturer</span></a>
                &nbsp;|&nbsp;<a href="javascript:RecentMatls.Toggle();" id="ctl00_lnkRecentMatls" class="navlink"><span class="nav"><font color="#CC0000">Recently Viewed Materials</font></span></a>
    </td>

    <td style="text-align:right; vertical-align:bottom; padding-right:5px;">

      <table class="tabletight t_ableborder" ><tr>
        <td style=" white-space:nowrap; vertical-align:bottom; " >
                                        &nbsp;&nbsp;
                                        <input name="ctl00$txtQuickText" type="text" id="ctl00_txtQuickText" style="width:100px;" maxlength="40" class="quicksearchinput" onkeydown="txtQuickText_OnKeyDown(event);" />
                                        <input type="image" id="btnQuickTextSearch" style="vertical-align:top; width:60px; height:25px;" src="/images/buttons/btnSearch.gif" alt="Search" onclick="return btnQuickTextSearch_Click()"  />
                                        <script type="text/javascript">

                                                //SETUP THE AUTOPOST ON ENTER KEY EVENT FOR txtQuickText FIELD
                                                //document.getElementById("ctl00_txtQuickText").onkeydown=txtQuickText_OnKeyDown;

                                                function txtQuickText_OnKeyDown(evt){
                                                        //SUBMIT THE FORM AS IF THE BUTTON WAS PUSHED
                                                        //var UniqueID = "";
                                                        var txtQuickText = document.getElementById('ctl00_txtQuickText');
                                                        if(matweb.IsEnterKey(evt))
                                                                if(checkSearchText(txtQuickText))
                                                                        //matweb.DoPostBack(UniqueID, "");
                                                                        window.location.href="/search/QuickText.aspx?SearchText=" + escape(txtQuickText.value);
                                                        return false;
                                                }

                                                function btnQuickTextSearch_Click(){
                                                        var txtQuickText = document.getElementById('ctl00_txtQuickText');
                                                        if(checkSearchText(txtQuickText))
                                                                window.location.href="/search/QuickText.aspx?SearchText=" + escape(txtQuickText.value);
                                                        return false;
                                                }

                                                function checkSearchText(searchbox){
                                                        searchbox.value = trim(searchbox.value);
                                                        if(searchbox.value == ''){
                                                                //alert('Please enter something to search for');
                                                                matweb.alert('Please enter something to search for', 'Search Error');
                                                                //searchbox.focus();
                                                                return false;
                                                        }
                                                        return true;
                                                }

                                                function trim(s)  { return ltrim(rtrim(s)) }
                                                function ltrim(s) { return s.replace(/^\s+/g, "") }
                                                function rtrim(s) { return s.replace(/\s+$/g, "") }

                                        </script>
                                </td>
                                </tr>
                        </table>

    </td>
  </tr>
  <tr style="background-image:url(/images/shad.gif);height:7px;" ><td colspan="2"></td></tr>
</table>

<!-- ====================================== MAIN CONTENT ============================================= -->
<div style="vertical-align:top; padding-left:10px; padding-right:10px;" >





	<center><a href="/clickthrough.aspx?addataid=1350"><img src="/images/assets/metalmen-Titanium.png" border=0 alt="Metalmen Sales" width = "728" height = "90"></a>
</center>
	
	<h2><a href="#help"><img src="/images/buttons/iconHelp.gif" alt="" style="vertical-align:bottom;" /></a>Material Category Search</h2>
	
	

<div id="ctl00_ContentMain_ucPopupMessage1_divPopupMessage" class="divPopupAlert" style="display:none;width:250px;">
	<table >
		<tr id="ctl00_ContentMain_ucPopupMessage1_trTitleRow" class="divPopupAlert_TitleRow" style="cursor:move;">
	<th><span id="ctl00_ContentMain_ucPopupMessage1_lblTitle">User Input Problem</span></th>
</tr>

		<tr class="divPopupAlert_ContentRow" ><td style="" ><span id="ctl00_ContentMain_ucPopupMessage1_lblMessage"></span></td></tr>
		<tr class="divPopupAlert_ButtonRow"><td><input name="ctl00$ContentMain$ucPopupMessage1$btnOK" type="button" id="ctl00_ContentMain_ucPopupMessage1_btnOK" value="OK" /></td></tr>
	</table>
</div>

<input type="hidden" name="ctl00$ContentMain$ucPopupMessage1$hndPopupControl" id="ctl00_ContentMain_ucPopupMessage1_hndPopupControl" />


	<table class="" style="width:100%;">
	<tr>
		<td style=" vertical-align:top; width:300px;" >
			<strong>Find a Material Category:</strong>
			

<input name="ctl00$ContentMain$UcMatGroupFinder1$txtSearchText" type="text" value="tita" id="ctl00_ContentMain_UcMatGroupFinder1_txtSearchText" style="Width:304px;" /><br />
<select name="ctl00$ContentMain$UcMatGroupFinder1$selectCategoryList" id="ctl00_ContentMain_UcMatGroupFinder1_selectCategoryList" style="position:absolute;display:none;Width:308px;" size="18">
</select>

<input type="hidden" name="ctl00$ContentMain$UcMatGroupFinder1$TextBoxWatermarkExtender2_ClientState" id="ctl00_ContentMain_UcMatGroupFinder1_TextBoxWatermarkExtender2_ClientState" />

<script type="text/javascript">

var ucMatGroupFinderManager = new _ucMatGroupFinderManager("ctl00_ContentMain_UcMatGroupFinder1_txtSearchText","ctl00_ContentMain_UcMatGroupFinder1_selectCategoryList");

function _ucMatGroupFinderManager(txtSearchTextID,selectCategoryListID){

	var txtSearchText = document.getElementById(txtSearchTextID);
	var selectCategoryList = document.getElementById(selectCategoryListID);
	var showtime;
	var blurtime;
	var MinTextLength = 4;
	var KeyStrokeActionDelay = 650;
	var FadeInOutDelay = 0.2;
	var LastUsedText;
	
	//The init function is currently run at the bottom of this class
	this.init=function(){

		txtSearchText.onfocus = txtSearchText_onfocus;
		txtSearchText.onkeyup = txtSearchText_checktext;
		txtSearchText.onblur = selectCategoryList_startblur;
		
		selectCategoryList.onfocus = selectCategoryList_clearblur;
		selectCategoryList.onchange = selectCategoryList_onchange;
		selectCategoryList.onblur = selectCategoryList_startblur;

	}

	txtSearchText_onfocus = function(){
		if(txtSearchText.value == "Type at least 4 characters here...")
			txtSearchText.value = "";
		selectCategoryList_clearblur();
		txtSearchText_checktext();
	}

	function txtSearchText_checktext(){
		searchText = txtSearchText.value;
		//alert(searchText);
		clearTimeout(showtime);
		if(searchText.length >= MinTextLength){
			showtime = setTimeout(function(){
				selectCategoryList.style.display = "block";
				if(txtSearchText.value!=LastUsedText){
					LastUsedText = searchText;
					aci.matweb.WebServices.MatGroups.FindMatGroups(searchText,DisplayCatData,matweb.WebServiceErrorHandler,"txtSearchText_checktext");
				}
				//AjaxControlToolkit.Animation.FadeInAnimation.play(selectCategoryList, FadeInOutDelay, 1, 25, 0, 1, true);
			},KeyStrokeActionDelay);
		}
		else{
			LastUsedText = "";
			showtime = setTimeout(function(){
				selectCategoryList.options.length = 0;
				selectCategoryList.style.display = "none";
				//AjaxControlToolkit.Animation.FadeOutAnimation.play(selectCategoryList,FadeInOutDelay);
				//AjaxControlToolkit.Animation.FadeOutAnimation.play(selectCategoryList,FadeInOutDelay, 25, 0, 1, true);
			},KeyStrokeActionDelay);
		}
	}

	function selectCategoryList_clearblur(){
		clearTimeout(blurtime);
	}

	function selectCategoryList_onchange(){
		var MatGroupID = selectCategoryList.options[selectCategoryList.selectedIndex].value;
		var MatGroupText = selectCategoryList.options[selectCategoryList.selectedIndex].text;
		//alert("Selected MatGroupID: " + MatGroupID);
		//alert("Selected MatGroupText: " + MatGroupText);
		//When no data is found, we have a "No Categories Contain Text..." message, but the MatGroupID is set to 0, so we can ignore if MatGroupID == 0.
		if(MatGroupID > 0){
			ucMatGroupFinder1_OnSelected(MatGroupID,MatGroupText)
		}
		selectCategoryList_startblur();
		//txtSearchText.value = "";
	}
	
	function selectCategoryList_startblur(){
		//alert("selectCategoryList_onblur()");
		clearTimeout(blurtime);
		blurtime = setTimeout(function(){
			selectCategoryList.style.display = "none";
			//AjaxControlToolkit.Animation.FadeOutAnimation.play(selectCategoryList,FadeInOutDelay, 25, 0, 1, true);
		},KeyStrokeActionDelay);
	}
	
	function DisplayCatData(MatGroupData){
		//alert("GetCatData()");
		//alert("vwMaterialGroupList: " + vwMaterialGroupList);
		var MatGroupID,GroupName, MatCount;
		selectCategoryList.options.length = 0;//clear any existing options....
		if(MatGroupData.length==0){
				//alert(vwMaterialGroupList[i].GroupName);
				MatGroupID = 0;
				GroupName = "No categories contain the text: " + txtSearchText.value
				selectCategoryList.options[0] = new Option(GroupName,MatGroupID);					
		}
		else{
			for(i=0;i<MatGroupData.length;i++){
				//alert(vwMaterialGroupList[i].GroupName);
				MatCount = MatGroupData[i].MatCount;
				MatGroupID = MatGroupData[i].MatGroupID;
				GroupName = MatGroupData[i].GroupName + " (" + MatCount + " matls)";
				selectCategoryList.options[i] = new Option(GroupName,MatGroupID);					
			}
		}
	}
	
	this.init();

}//end class
	
</script>



			<strong>Select a Material Category:</strong>
			
			<div class="tableborder" style=" height:225px; width:300px; overflow:scroll; ">
			<a href="#ctl00_ContentMain_ucMatGroupTree_msTreeView_SkipLink"><img alt="Skip Navigation Links." src="/WebResource.axd?d=vAkieGj3ugBpu6CKp7dqvCP5iHLzPbUd2XME-h1vQcC6d5hLg7Vc1kNVzB8A45Ui3K9XZ9m3-E2pxsaJQYh2uMkAul41&amp;t=638313619312541215" width="0" height="0" style="border-width:0px;" /></a><div id="ctl00_ContentMain_ucMatGroupTree_msTreeView">
	<table cellpadding="0" cellspacing="0" style="border-width:0;">
		<tr>
			<td><a id="ctl00_ContentMain_ucMatGroupTree_msTreeViewn0" href="javascript:TreeView_PopulateNode(ctl00_ContentMain_ucMatGroupTree_msTreeView_Data,0,document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewn0'),document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewt0'),null,'r','Carbon (866 matls)','283','f','','f')"><img src="/WebResource.axd?d=6v5qM6VKQYD3XNwwXEy7Tix7dzVdqDJ8QTRIlUPrzXQqNsXQgUwIfmn34ZzRORd_IZj0bF4vkZzID1HuSFE5uKyLgaPYeAYE8VT8HYfb3Rk6zsDw0&amp;t=638313619312541215" alt="Expand Carbon (866 matls)" style="border-width:0;" /></a></td><td class="ctl00_ContentMain_ucMatGroupTree_msTreeView_2"><a class="ctl00_ContentMain_ucMatGroupTree_msTreeView_0 ctl00_ContentMain_ucMatGroupTree_msTreeView_1" href="javascript:ucMatGroupTree_OnNodeClick('Carbon (866 matls)','283')" id="ctl00_ContentMain_ucMatGroupTree_msTreeViewt0">Carbon (866 matls)</a></td>
		</tr><tr style="height:0px;">
			<td></td>
		</tr>
	</table><table cellpadding="0" cellspacing="0" style="border-width:0;">
		<tr style="height:0px;">
			<td></td>
		</tr><tr>
			<td><a id="ctl00_ContentMain_ucMatGroupTree_msTreeViewn1" href="javascript:TreeView_PopulateNode(ctl00_ContentMain_ucMatGroupTree_msTreeView_Data,1,document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewn1'),document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewt1'),null,'t','Ceramic (10004 matls)','11','f','','f')"><img src="/WebResource.axd?d=TinC6igX1vPDgWh7DstWwBTHTyH4uuybDU4NW47jJD06wHdAO3YyJC_eKK1Zn5oIbwqRo4Cri8YwhA82G-LD0p14yEF1EfoIZvKleYMIuQ2K1hkE0&amp;t=638313619312541215" alt="Expand Ceramic (10004 matls)" style="border-width:0;" /></a></td><td class="ctl00_ContentMain_ucMatGroupTree_msTreeView_2"><a class="ctl00_ContentMain_ucMatGroupTree_msTreeView_0 ctl00_ContentMain_ucMatGroupTree_msTreeView_1" href="javascript:ucMatGroupTree_OnNodeClick('Ceramic (10004 matls)','11')" id="ctl00_ContentMain_ucMatGroupTree_msTreeViewt1">Ceramic (10004 matls)</a></td>
		</tr><tr style="height:0px;">
			<td></td>
		</tr>
	</table><table cellpadding="0" cellspacing="0" style="border-width:0;">
		<tr style="height:0px;">
			<td></td>
		</tr><tr>
			<td><a id="ctl00_ContentMain_ucMatGroupTree_msTreeViewn2" href="javascript:TreeView_PopulateNode(ctl00_ContentMain_ucMatGroupTree_msTreeView_Data,2,document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewn2'),document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewt2'),null,'t','Fluid (7562 matls)','5','f','','f')"><img src="/WebResource.axd?d=TinC6igX1vPDgWh7DstWwBTHTyH4uuybDU4NW47jJD06wHdAO3YyJC_eKK1Zn5oIbwqRo4Cri8YwhA82G-LD0p14yEF1EfoIZvKleYMIuQ2K1hkE0&amp;t=638313619312541215" alt="Expand Fluid (7562 matls)" style="border-width:0;" /></a></td><td class="ctl00_ContentMain_ucMatGroupTree_msTreeView_2"><a class="ctl00_ContentMain_ucMatGroupTree_msTreeView_0 ctl00_ContentMain_ucMatGroupTree_msTreeView_1" href="javascript:ucMatGroupTree_OnNodeClick('Fluid (7562 matls)','5')" id="ctl00_ContentMain_ucMatGroupTree_msTreeViewt2">Fluid (7562 matls)</a></td>
		</tr><tr style="height:0px;">
			<td></td>
		</tr>
	</table><table cellpadding="0" cellspacing="0" style="border-width:0;">
		<tr style="height:0px;">
			<td></td>
		</tr><tr>
			<td><a id="ctl00_ContentMain_ucMatGroupTree_msTreeViewn3" href="javascript:TreeView_PopulateNode(ctl00_ContentMain_ucMatGroupTree_msTreeView_Data,3,document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewn3'),document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewt3'),null,'t','Metal (17052 matls)','9','f','','f')"><img src="/WebResource.axd?d=TinC6igX1vPDgWh7DstWwBTHTyH4uuybDU4NW47jJD06wHdAO3YyJC_eKK1Zn5oIbwqRo4Cri8YwhA82G-LD0p14yEF1EfoIZvKleYMIuQ2K1hkE0&amp;t=638313619312541215" alt="Expand Metal (17052 matls)" style="border-width:0;" /></a></td><td class="ctl00_ContentMain_ucMatGroupTree_msTreeView_2"><a class="ctl00_ContentMain_ucMatGroupTree_msTreeView_0 ctl00_ContentMain_ucMatGroupTree_msTreeView_1" href="javascript:ucMatGroupTree_OnNodeClick('Metal (17052 matls)','9')" id="ctl00_ContentMain_ucMatGroupTree_msTreeViewt3">Metal (17052 matls)</a></td>
		</tr><tr style="height:0px;">
			<td></td>
		</tr>
	</table><table cellpadding="0" cellspacing="0" style="border-width:0;">
		<tr style="height:0px;">
			<td></td>
		</tr><tr>
			<td><a id="ctl00_ContentMain_ucMatGroupTree_msTreeViewn4" href="javascript:TreeView_PopulateNode(ctl00_ContentMain_ucMatGroupTree_msTreeView_Data,4,document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewn4'),document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewt4'),null,'t','Other Engineering Material (8063 matls)','285','f','','f')"><img src="/WebResource.axd?d=TinC6igX1vPDgWh7DstWwBTHTyH4uuybDU4NW47jJD06wHdAO3YyJC_eKK1Zn5oIbwqRo4Cri8YwhA82G-LD0p14yEF1EfoIZvKleYMIuQ2K1hkE0&amp;t=638313619312541215" alt="Expand Other Engineering Material (8063 matls)" style="border-width:0;" /></a></td><td class="ctl00_ContentMain_ucMatGroupTree_msTreeView_2"><a class="ctl00_ContentMain_ucMatGroupTree_msTreeView_0 ctl00_ContentMain_ucMatGroupTree_msTreeView_1" href="javascript:ucMatGroupTree_OnNodeClick('Other Engineering Material (8063 matls)','285')" id="ctl00_ContentMain_ucMatGroupTree_msTreeViewt4">Other Engineering Material (8063 matls)</a></td>
		</tr><tr style="height:0px;">
			<td></td>
		</tr>
	</table><table cellpadding="0" cellspacing="0" style="border-width:0;">
		<tr style="height:0px;">
			<td></td>
		</tr><tr>
			<td><a id="ctl00_ContentMain_ucMatGroupTree_msTreeViewn5" href="javascript:TreeView_PopulateNode(ctl00_ContentMain_ucMatGroupTree_msTreeView_Data,5,document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewn5'),document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewt5'),null,'t','Polymer (97635 matls)','10','f','','f')"><img src="/WebResource.axd?d=TinC6igX1vPDgWh7DstWwBTHTyH4uuybDU4NW47jJD06wHdAO3YyJC_eKK1Zn5oIbwqRo4Cri8YwhA82G-LD0p14yEF1EfoIZvKleYMIuQ2K1hkE0&amp;t=638313619312541215" alt="Expand Polymer (97635 matls)" style="border-width:0;" /></a></td><td class="ctl00_ContentMain_ucMatGroupTree_msTreeView_2"><a class="ctl00_ContentMain_ucMatGroupTree_msTreeView_0 ctl00_ContentMain_ucMatGroupTree_msTreeView_1" href="javascript:ucMatGroupTree_OnNodeClick('Polymer (97635 matls)','10')" id="ctl00_ContentMain_ucMatGroupTree_msTreeViewt5">Polymer (97635 matls)</a></td>
		</tr><tr style="height:0px;">
			<td></td>
		</tr>
	</table><table cellpadding="0" cellspacing="0" style="border-width:0;">
		<tr style="height:0px;">
			<td></td>
		</tr><tr>
			<td><img src="/WebResource.axd?d=7_V5n1KYSQsj6PoSqgIxb0egBsAa91jSXPkbmM6FaiF14-JsNnpUDnfSdxZcvfd0OQCLDtC24pMIXigO-41cL2rsq-0xea-SSG7yZUyRcliFFqe70&amp;t=638313619312541215" alt="" /></td><td class="ctl00_ContentMain_ucMatGroupTree_msTreeView_2"><a class="ctl00_ContentMain_ucMatGroupTree_msTreeView_0 ctl00_ContentMain_ucMatGroupTree_msTreeView_1" href="javascript:ucMatGroupTree_OnNodeClick('Pure Element (507 matls)','184')" id="ctl00_ContentMain_ucMatGroupTree_msTreeViewt6">Pure Element (507 matls)</a></td>
		</tr><tr style="height:0px;">
			<td></td>
		</tr>
	</table><table cellpadding="0" cellspacing="0" style="border-width:0;">
		<tr style="height:0px;">
			<td></td>
		</tr><tr>
			<td><a id="ctl00_ContentMain_ucMatGroupTree_msTreeViewn7" href="javascript:TreeView_PopulateNode(ctl00_ContentMain_ucMatGroupTree_msTreeView_Data,7,document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewn7'),document.getElementById('ctl00_ContentMain_ucMatGroupTree_msTreeViewt7'),null,'l','Wood and Natural Products (398 matls)','277','f','','t')"><img src="/WebResource.axd?d=vUBrM2nNhECNbFO0AJKq52p2zWrY5XG8nWgCDL4AfsrexStb0HREDV6GHMvdkAGDInxYtTyWXuxFUIiQv3JX2fScVRwQsrVv5srHrjOIeC3ayAG20&amp;t=638313619312541215" alt="Expand Wood and Natural Products (398 matls)" style="border-width:0;" /></a></td><td class="ctl00_ContentMain_ucMatGroupTree_msTreeView_2"><a class="ctl00_ContentMain_ucMatGroupTree_msTreeView_0 ctl00_ContentMain_ucMatGroupTree_msTreeView_1" href="javascript:ucMatGroupTree_OnNodeClick('Wood and Natural Products (398 matls)','277')" id="ctl00_ContentMain_ucMatGroupTree_msTreeViewt7">Wood and Natural Products (398 matls)</a></td>
		</tr><tr style="height:0px;">
			<td></td>
		</tr>
	</table>
</div><a id="ctl00_ContentMain_ucMatGroupTree_msTreeView_SkipLink"></a>
<span id="ctl00_ContentMain_ucMatGroupTree_lblDebug"></span>

			</div>
			
			<strong>Selected Material Category: </strong>
			<div id="ctl00_ContentMain_divMatGroupName" class="selectedItem" style="width:306px;">Titanium Alloy (386 matls)</div>
			<input name="ctl00$ContentMain$txtMatGroupID" type="hidden" id="ctl00_ContentMain_txtMatGroupID" value="181" />
			<input name="ctl00$ContentMain$txtMatGroupText" type="hidden" id="ctl00_ContentMain_txtMatGroupText" value="Titanium Alloy (386 matls)" />
			
			

			<input type="image" name="ctl00$ContentMain$btnSubmit" id="ctl00_ContentMain_btnSubmit" src="../images/buttons/btnFind.gif" alt="Find" style="border-width:0px;" /> <!-- OnClientClick="return validate()" -->
			<input type="image" name="ctl00$ContentMain$btnReset" id="ctl00_ContentMain_btnReset" title="Clear Search" src="/images/buttons/btnReset.gif" alt="Clear Search" style="border-width:0px;" />

			<span id="ctl00_ContentMain_lblDebug"></span>

		</td>
		<td>&nbsp;</td>
		<td style=" vertical-align:top; width:80%;" >
			<strong>Search Results</strong>
			<div class="tableborder" id="divSearchResults" style="padding:5px;" >
				
				<div id="ctl00_ContentMain_pnlSearchResults">
	
					There are <span id="ctl00_ContentMain_lblMatlCount">429</span> materials in the category 
					<span id="ctl00_ContentMain_lblMaterialGroupName" class="selectedItem">Titanium Alloy</span>.
					If your material is not listed, please refer to our <a href="/help/strategy.aspx">search strategy</a> page for assistance in limiting your search.
				
</div>
			</div>
			<p />




<div id="ctl00_ContentMain_UcSearchResults1_pnlResultsFound" class="tableborder">
	

	<table style="background-color:Silver; width:100%;" class="" >
		<tr>
			<td><strong>Found <span id="ctl00_ContentMain_UcSearchResults1_lblResultCount">429</span> Results</strong> -- 
				Page 
				<select name="ctl00$ContentMain$UcSearchResults1$drpPageSelect1" onchange="javascript:setTimeout('__doPostBack(\'ctl00$ContentMain$UcSearchResults1$drpPageSelect1\',\'\')', 0)" id="ctl00_ContentMain_UcSearchResults1_drpPageSelect1">
		<option value="1">1</option>
		<option selected="selected" value="2">2</option>
		<option value="3">3</option>

	</select>
				of <span id="ctl00_ContentMain_UcSearchResults1_lblPageTotal">3</span> 
				-- 
				<a id="ctl00_ContentMain_UcSearchResults1_lnkPrevPage" href="javascript:__doPostBack('ctl00$ContentMain$UcSearchResults1$lnkPrevPage','')" style="color:Black;">[Prev Page]</a>
				<a id="ctl00_ContentMain_UcSearchResults1_lnkNextPage" href="javascript:__doPostBack('ctl00$ContentMain$UcSearchResults1$lnkNextPage','')" style="color:Black;">[Next Page]</a>
				--&nbsp;			
				view
				<select name="ctl00$ContentMain$UcSearchResults1$drpPageSize1" onchange="javascript:setTimeout('__doPostBack(\'ctl00$ContentMain$UcSearchResults1$drpPageSize1\',\'\')', 0)" id="ctl00_ContentMain_UcSearchResults1_drpPageSize1">
		<option value="25">25</option>
		<option value="50">50</option>
		<option value="75">75</option>
		<option value="100">100</option>
		<option value="150">150</option>
		<option selected="selected" value="200">200</option>

	</select> per page
			</td>
		</tr>
		
	</table>
	
	
	
	
	<table class="t_ablegrid" style="width:100%;">
		<tr style="font-size:9px;">
			<td style="font-size:9px;">Use Folder</td>
			<td style="font-size:9px;">Contains</td>
			<td style="font-size:9px;">&nbsp;</td>
			
			<td style="font-size:9px;"><div id="divFolderName" style="display:none;">New Folder Name</div></td>
			<td style="font-size:9px;"></td>
			<td style="width:90%"></td>
		</tr>
		<tr style="vertical-align:top;">
			<td style="vertical-align:top;">
				<select name="ctl00$ContentMain$UcSearchResults1$drpFolderList" id="ctl00_ContentMain_UcSearchResults1_drpFolderList" onchange="SetSelectedFolder()" style="width:125px;">
		<option selected="selected" value="0">My Folder</option>

	</select>
			</td>
			<td style="vertical-align:top;">
				<strong><input name="ctl00$ContentMain$UcSearchResults1$txtFolderMatCount" type="text" id="ctl00_ContentMain_UcSearchResults1_txtFolderMatCount" readonly="readonly" style="border:none 0 white; background-color:White; color:Black; width: 35px; font-weight:bold;" value="0/0" /></strong>
			</td>
			<td>
				
				<input type="image" id="btnCompareFolders" src="/images/buttons/btnCompareMatls.gif" alt="Compare Checked Materials" onclick="CompareMaterials();return false;" />
			</td>
			
			<td style="white-space:nowrap;vertical-align:top;">
			
				<table id="tblFolderName" class="tabletight" style="display:none;"><tr><td><input type="text" id="txtFolderName2" style="display:inline; width:100px;" /><br /></td>
				<td><input type="image" id="btnApplyFolderName2" src="/images/buttons/btnSaveChanges.gif" style="display:inline;" onclick="ApplyFolderName();return false;" />
				<input type="image" id="btnHideFolderNameControls2" src="/images/buttons/btnCancel.gif" style="display:inline;" onclick="HideFolderNameControls();return false;" /></td>
				</tr></table>
				
			</td>
			<td style="white-space:nowrap;">
				<div class="usermessage" id="divUserMessage"></div>
			</td>
		</tr>
	</table>

	<table class="tabledataformat t_ablegrid" id="tblResults" style="table-layout:auto;"  >

		<tr class="">

			
		
			<th style="width:65px; white-space:nowrap;">Select</th>

			<th style="width:10px;">
				<!--Discontinued and Found Via Interpolated Indicators -->
			</th>
			
			<th style="width:auto;">
				<a id="ctl00_ContentMain_UcSearchResults1_lnkSortMatName" disabled="disabled" title="Click to Sort By Material Name">Material Name</a>
				<br />
				
				
			</th>
		
			
			
			

			

			

			

			

			

			

			

			
		</tr>

		<!-- this is where the search results are actually loaded.  See code behind -->		
		<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_14799"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;201</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_14799' href="/search/DataSheet.aspx?MatGUID=0c990333061a488a9a32aac93a4cfdc1" >ATI Allvac® 6-2-4-2 Titanium Alloy, Heat Treatment: 982°C (1800°F) + Age</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_14800"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;202</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_14800' href="/search/DataSheet.aspx?MatGUID=402f3e583d8142d380cf2cc563a305fd" >ATI Allvac® 6-2-4-6 Titanium Alloy, Heat Treatment: 885°C (1625°F) + Age</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_14801"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;203</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_14801' href="/search/DataSheet.aspx?MatGUID=6ceaf92e6959439f8c9659a7e2161bd4" >ATI Allvac® Ti-17 Titanium Alloy, Heat Treatment: 899°C (1650°F) + Age</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_14932"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;204</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_14932' href="/search/DataSheet.aspx?MatGUID=ae6b626ddd9f4e899f4b5a5c9bbea940" >ATI Allvac® Grade 7 UNS R52400 Titanium Alloy</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_14933"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;205</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_14933' href="/search/DataSheet.aspx?MatGUID=29153414f93243b2b7c024532d182d3c" >ATI Allvac® Grade 12 UNS R53400 Titanium Alloy</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_14934"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;206</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_14934' href="/search/DataSheet.aspx?MatGUID=5b14810ce03b4e5aaa69155788417275" >ATI Allvac® 6-7 UNS R56700 Titanium Alloy</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_14935"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;207</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_14935' href="/search/DataSheet.aspx?MatGUID=35d1dd3df6d04a0b88e113a2af37d675" >ATI Allvac® 6-2-4-2-Si UNS R54620 modified Titanium Alloy</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_14936"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;208</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_14936' href="/search/DataSheet.aspx?MatGUID=fbd82d32e36a433da721e235b4a868fb" >ATI Allvac® Grade 15-3-3-3 UNS R58153 Titanium Alloy</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_14937"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;209</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_14937' href="/search/DataSheet.aspx?MatGUID=b3868046ea6e44e4aa68cb6c8d8e22cc" >ATI Allvac® 38-644 UNS R58640 Titanium Alloy</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_287946"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;210</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_287946' href="/search/DataSheet.aspx?MatGUID=c224c0d0e85e4b0bb9f8110ee30b850e" >ALLVAR Alloy 30 Negative CTE Titanium Alloy</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_15063"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;211</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_15063' href="/search/DataSheet.aspx?MatGUID=1afb9279197e4a728bea50071194693b" >ATI Wah Chang 425™ Titanium Alloy, Cold Rolled Sheet</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_15064"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;212</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_15064' href="/search/DataSheet.aspx?MatGUID=9e620304850d4994b50d1aee6423e729" >ATI Wah Chang 425™ Titanium Alloy, Hot Rolled & Annealed Plate</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_15065"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;213</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_15065' href="/search/DataSheet.aspx?MatGUID=7a3996d6aff2428e812f7f937003ba35" >ATI Wah Chang 425™ Titanium Alloy, Hot Rolled & Annealed Plate, Solution Treated Annealed</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_15066"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;214</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_15066' href="/search/DataSheet.aspx?MatGUID=2bc47cb2d6c14725b21b88577c72cfa5" >ATI Wah Chang 425™ Titanium Alloy, Annealed Bar 0.5 - 1.23 in. dia</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_15067"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;215</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_15067' href="/search/DataSheet.aspx?MatGUID=8b92278287c54b5cbe0fa5ec251422c4" >ATI Wah Chang 425™ Titanium Alloy, Solution Treated Annealed Bar 0.5 - 1.23 in. dia</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_15068"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;216</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_15068' href="/search/DataSheet.aspx?MatGUID=47a05584cdd641abaa4209adfa86fd45" >ATI Wah Chang 425™ Titanium Alloy, Tubular Extruded</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_15069"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;217</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_15069' href="/search/DataSheet.aspx?MatGUID=28a7c807b1c7407eb527fb6e9fe9efbf" >ATI Wah Chang 425™ Titanium Alloy, Tubular Extruded, Mill Annealed</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_15070"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;218</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_15070' href="/search/DataSheet.aspx?MatGUID=87ba2246b58e431d8cb4e59a0b9a9c58" >ATI Wah Chang TI-3Al-2.5V</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_15071"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;219</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_15071' href="/search/DataSheet.aspx?MatGUID=d4a84f06d085480389dfde4f5e62d6e4" >ATI Wah Chang TI-45 Nb</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_15076"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;220</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_15076' href="/search/DataSheet.aspx?MatGUID=e86dbcfe6f9442e89b11c037be214343" >ATI Wah Chang Tiadyne™ 3510</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_16096"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;221</td><td style="width:auto;"><img src="/images/buttons/iconDiscontinued.jpg" alt="Discontinued" title="Discontinued"  /></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_16096' href="/search/DataSheet.aspx?MatGUID=48e8f6f5218c440a83b3e527990b9c74" >Deutsche Titan Tikrutan RT 12 Commercially Pure Titanium</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_16097"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;222</td><td style="width:auto;"><img src="/images/buttons/iconDiscontinued.jpg" alt="Discontinued" title="Discontinued"  /></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_16097' href="/search/DataSheet.aspx?MatGUID=83300555b98d425593b1b563d2c1a3bf" >Deutsche Titan Tikrutan RT 15 Commercially Pure Titanium</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_16098"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;223</td><td style="width:auto;"><img src="/images/buttons/iconDiscontinued.jpg" alt="Discontinued" title="Discontinued"  /></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_16098' href="/search/DataSheet.aspx?MatGUID=b549f1f613b64a969a4c5cbf6b4b3066" >Deutsche Titan Tikrutan RT 18 Commercially Pure Titanium</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_16099"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;224</td><td style="width:auto;"><img src="/images/buttons/iconDiscontinued.jpg" alt="Discontinued" title="Discontinued"  /></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_16099' href="/search/DataSheet.aspx?MatGUID=3994f80ced0d4e7697a46e8f0df80e07" >Deutsche Titan Tikrutan RT 20 Commercially Pure Titanium</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_16100"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;225</td><td style="width:auto;"><img src="/images/buttons/iconDiscontinued.jpg" alt="Discontinued" title="Discontinued"  /></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_16100' href="/search/DataSheet.aspx?MatGUID=717d071a237d49eb940acc54ee32b95b" >Deutsche Titan Tikrutan RT 12 Pd 1/2 Low-Alloyed Titanium</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_16101"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;226</td><td style="width:auto;"><img src="/images/buttons/iconDiscontinued.jpg" alt="Discontinued" title="Discontinued"  /></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_16101' href="/search/DataSheet.aspx?MatGUID=1efa50f6080740dca4d2db99b84e7dc5" >Deutsche Titan Tikrutan RT 12 Pd Low-Alloyed Titanium</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_16102"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;227</td><td style="width:auto;"><img src="/images/buttons/iconDiscontinued.jpg" alt="Discontinued" title="Discontinued"  /></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_16102' href="/search/DataSheet.aspx?MatGUID=033a2f23115c4880a6ac77cdfde296e1" >Deutsche Titan Tikrutan RT 15 Pd 1/2 Low-Alloyed Titanium</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_16103"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;228</td><td style="width:auto;"><img src="/images/buttons/iconDiscontinued.jpg" alt="Discontinued" title="Discontinued"  /></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_16103' href="/search/DataSheet.aspx?MatGUID=2a0fa6a84ed44037a065d688aff203ee" >Deutsche Titan Tikrutan RT 15 Pd Low-Alloyed Titanium</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_16104"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;229</td><td style="width:auto;"><img src="/images/buttons/iconDiscontinued.jpg" alt="Discontinued" title="Discontinued"  /></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_16104' href="/search/DataSheet.aspx?MatGUID=8231459922da45b6a528ca1017dbc58d" >Deutsche Titan Tikrutan RT 18 Pd 1/2 Low-Alloyed Titanium</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_16105"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;230</td><td style="width:auto;"><img src="/images/buttons/iconDiscontinued.jpg" alt="Discontinued" title="Discontinued"  /></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_16105' href="/search/DataSheet.aspx?MatGUID=d182b97042744decade22f3579890b9c" >Deutsche Titan Tikrutan RT 18 Pd Low-Alloyed Titanium</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_16106"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;231</td><td style="width:auto;"><img src="/images/buttons/iconDiscontinued.jpg" alt="Discontinued" title="Discontinued"  /></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_16106' href="/search/DataSheet.aspx?MatGUID=3c8924437ca9405eab7cc66ea1273806" >Deutsche Titan Tikrutan LT 27 Low-Alloyed Titanium</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_16107"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;232</td><td style="width:auto;"><img src="/images/buttons/iconDiscontinued.jpg" alt="Discontinued" title="Discontinued"  /></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_16107' href="/search/DataSheet.aspx?MatGUID=ada0fabb64c4484ab4f42d4089c0933e" >Deutsche Titan Tikrutan LT 24 Alloyed Titanium</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_16108"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;233</td><td style="width:auto;"><img src="/images/buttons/iconDiscontinued.jpg" alt="Discontinued" title="Discontinued"  /></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_16108' href="/search/DataSheet.aspx?MatGUID=ded34be095ae4367bc0941805c8e2e3c" >Deutsche Titan Tikrutan LT 26 Alloyed Titanium</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_16109"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;234</td><td style="width:auto;"><img src="/images/buttons/iconDiscontinued.jpg" alt="Discontinued" title="Discontinued"  /></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_16109' href="/search/DataSheet.aspx?MatGUID=c20690172a8947738d17a554e2b221b2" >Deutsche Titan Tikrutan LT 31 Alloyed Titanium</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_16110"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;235</td><td style="width:auto;"><img src="/images/buttons/iconDiscontinued.jpg" alt="Discontinued" title="Discontinued"  /></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_16110' href="/search/DataSheet.aspx?MatGUID=cd1778311bd04c5bad6cc5a004a624ac" >Deutsche Titan Tikrutan LT 33 Alloyed Titanium</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_16111"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;236</td><td style="width:auto;"><img src="/images/buttons/iconDiscontinued.jpg" alt="Discontinued" title="Discontinued"  /></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_16111' href="/search/DataSheet.aspx?MatGUID=93aef4a0c17a43809e93c8e5724612c3" >Deutsche Titan Tikrutan LT 34 Alloyed Titanium</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_280586"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;237</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_280586' href="/search/DataSheet.aspx?MatGUID=7f960a508c8448c48b64fb8ef747751b" >EOS Copper Cu DMSL on EOS M 290; 20 µm Layer Thickness</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_280598"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;238</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_280598' href="/search/DataSheet.aspx?MatGUID=6cad2455ad834d9ba4637e109ac409e3" >EOS Tungsten W1 Powder</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_280612"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;239</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_280612' href="/search/DataSheet.aspx?MatGUID=6ef7f6fbb1bc4449a294592d2b37d12b" >EOS Titanium Ti64 Flexline DMSL on EOS M 100</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_280613"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;240</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_280613' href="/search/DataSheet.aspx?MatGUID=86a35325b1b34297ac4e09b05a43ed1f" >EOS Titanium Ti64 DMSL on EOS M 290</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_280614"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;241</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_280614' href="/search/DataSheet.aspx?MatGUID=c570f61560d244be90ad904de85453c9" >EOS Titanium Ti64 DMSL on EOS M 290/400 W</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_280615"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;242</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_280615' href="/search/DataSheet.aspx?MatGUID=4df3632d966346d08e6dc5e106c7aced" >EOS Titanium Ti64 DMSL on EOS M 400 SF</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_280616"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;243</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_280616' href="/search/DataSheet.aspx?MatGUID=70ef090e1b8348f7820affa977b59aef" >EOS Titanium Ti64 DMSL on EOS M 400-4</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_280617"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;244</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_280617' href="/search/DataSheet.aspx?MatGUID=238f0193c40d4e1ea42fcb3d0b99510a" >EOS Titanium Ti64 ELI DMSL on EOS M 290</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_280618"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;245</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_280618' href="/search/DataSheet.aspx?MatGUID=764d6f8cf4d7425ebe23ef70e5d0a42e" >EOS Titanium Ti64 ELI DMSL on EOS M 400-4</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_280619"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;246</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_280619' href="/search/DataSheet.aspx?MatGUID=848dfcabb25345a8b41a7668349b20fa" >EOS Titanium Ti64 Grade 5 DMSL on EOS M 290; 40 µm Layer Thickness</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_280620"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;247</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_280620' href="/search/DataSheet.aspx?MatGUID=2c53023814964dfab4326a6ee033678f" >EOS Titanium Ti64 Grade 5 DMSL on EOS M 290; 80 µm Layer Thickness</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_280621"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;248</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_280621' href="/search/DataSheet.aspx?MatGUID=b6601bbf09244b618522b82ebae198e2" >EOS Titanium Ti64 Grade 5 DMSL on EOS M 400-4; 80 µm Layer Thickness</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_280622"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;249</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_280622' href="/search/DataSheet.aspx?MatGUID=5493316bf9884c99a5a9f3cd98373143" >EOS Titanium Ti64 Grade 23 DMSL on EOS M 290; 40 µm Layer Thickness</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_280623"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;250</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_280623' href="/search/DataSheet.aspx?MatGUID=ba492c86133d4ea4ac2c31aa3589335d" >EOS Titanium Ti64 Grade 23 DMSL on EOS M 290; 80 µm Layer Thickness</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_280624"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;251</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_280624' href="/search/DataSheet.aspx?MatGUID=7ea75cb88d834c30a15600bb803a4013" >EOS Titanium Ti64 Grade 23 DMSL on EOS M 400-4; 80 µm Layer Thickness</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_280625"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;252</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_280625' href="/search/DataSheet.aspx?MatGUID=32621e6b206144b9a64691a83d3344b4" >EOS Titanium TiCP grade 2 DMSL on EOS M 290</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_16214"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;253</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_16214' href="/search/DataSheet.aspx?MatGUID=f0842dcb06ad4d12a46b864f49068136" >Goodfellow Copper 56.4/Titanium 43.6 Alloy</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_16286"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;254</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_16286' href="/search/DataSheet.aspx?MatGUID=79d34f98e4a84323a1e9d510aec76021" >Goodfellow Titanium Bronze</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_16287"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;255</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_16287' href="/search/DataSheet.aspx?MatGUID=7d9bd321db5148058570e8a5c2c17b65" >Goodfellow Titanium 65/Aluminum 35 Alloy</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_16288"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;256</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_16288' href="/search/DataSheet.aspx?MatGUID=2c8aaca715f749ada6f506b39c470d22" >Goodfellow Titanium 88/Aluminum 12 Alloy</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_16289"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;257</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_16289' href="/search/DataSheet.aspx?MatGUID=59b7ac1aeed24f11ae40b96959d1a3ab" >Goodfellow Titanium 78/Aluminum 22 Alloy</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_16290"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;258</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_16290' href="/search/DataSheet.aspx?MatGUID=c28d01cf8f734d9e8d1c9de3645c5581" >Goodfellow Titanium 80/Chromium 20 Alloy</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_16293"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;259</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_16293' href="/search/DataSheet.aspx?MatGUID=cd3e28d4b0c6439092fa2601c8069847" >Goodfellow Titanium 50/Iron 50 Alloy</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_16294"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;260</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_16294' href="/search/DataSheet.aspx?MatGUID=1ea5bcb6511d42f6aa5772a54d33085f" >Goodfellow Titanium 84/Molybdenum 16 Alloy</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_16295"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;261</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_16295' href="/search/DataSheet.aspx?MatGUID=fbeea0dfe41440e6a24d599f5f2ac537" >Goodfellow Titanium 92/Silicon 8 Alloy</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_16296"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;262</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_16296' href="/search/DataSheet.aspx?MatGUID=d1b4004f15744ba8a1e6e205970d795d" >Goodfellow Titanium 90/Vanadium 10 Alloy</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_162660"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;263</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_162660' href="/search/DataSheet.aspx?MatGUID=98042e4e6088426dafc022af1449de62" >Haynes Ti-3Al-2.5V Alloy Titanium Tubing</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17531"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;264</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17531' href="/search/DataSheet.aspx?MatGUID=1e9687d919244e51a7d0a7fc0459b356" >Special Metals Nitinol Superelastic Ni-Ti alloy</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17532"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;265</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17532' href="/search/DataSheet.aspx?MatGUID=7bf10705e0494dfbbc07626e82729b04" >Special Metals Nitinol High-Strength Superelastic Ni-Ti alloy</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17533"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;266</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17533' href="/search/DataSheet.aspx?MatGUID=ba21dac525d44344a68aa93cda906b79" >Special Metals Nitinol High-Temperature Shape Memory Ni-Ti alloy Ribbon</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17534"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;267</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17534' href="/search/DataSheet.aspx?MatGUID=d79a3e1ffa6f4cfa8c3083b7dbdb4f89" >Special Metals Nitinol High-Temperature Shape Memory Ni-Ti alloy</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17535"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;268</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17535' href="/search/DataSheet.aspx?MatGUID=d307e63b36044fec9f039fe95ee0343b" >Special Metals Nitinol Body-Temperature Ni-Ti alloy</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17536"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;269</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17536' href="/search/DataSheet.aspx?MatGUID=9bcbd98b380e40bdaf3b806611a5f276" >Special Metals Nitinol Chrome-Doped Superelastic Ni-Ti alloy</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17537"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;270</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17537' href="/search/DataSheet.aspx?MatGUID=a7c1135b6c454b67b52904d623d33abe" >Special Metals Nitinol High-Strength Superelastic Ni-Ti-Fe alloy</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_186287"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;271</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_186287' href="/search/DataSheet.aspx?MatGUID=4a8814312d3c4e46b6f566bd6873b065" >Johnson Matthey Tri-flo™ 950 Titanium Brazing Filler Metal</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17747"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;272</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17747' href="/search/DataSheet.aspx?MatGUID=4288fb89283845de9bb597e5f6bd4b9b" >Kobe Steel KS40S Commercially Pure Titanium, ASTM Gr. 1, Annealed</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17748"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;273</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17748' href="/search/DataSheet.aspx?MatGUID=8f9e333bb12f4b52902afed292f4110f" >Kobe Steel KS40 Commercially Pure Titanium, ASTM Gr. 1, Annealed</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17749"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;274</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17749' href="/search/DataSheet.aspx?MatGUID=0ec8e06b2fe14f319159cba2336d5d73" >Kobe Steel KS60 Commercially Pure Titanium, ASTM Gr. 2, Annealed</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17750"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;275</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17750' href="/search/DataSheet.aspx?MatGUID=c7d40cacd4524b78963a4bf0c3364210" >Kobe Steel KS70 Commercially Pure Titanium, ASTM Gr. 3, Annealed</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17751"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;276</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17751' href="/search/DataSheet.aspx?MatGUID=e20db5ca6e924c7e9ea11d0daecfdd89" >Kobe Steel KS85 Commercially Pure Titanium, ASTM Gr. 4, Annealed</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17752"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;277</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17752' href="/search/DataSheet.aspx?MatGUID=b510cf8aba9b4279881d6fdb8f39f08a" >Kobe Steel KS100 Commercially Pure Titanium, Annealed</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17753"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;278</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17753' href="/search/DataSheet.aspx?MatGUID=37a7f8dee32140cc841f3f8a51046847" >Kobe Steel KS120SI Titanium alloy with low alloying elements, Annealed</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17754"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;279</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17754' href="/search/DataSheet.aspx?MatGUID=d29f50004d954e9cbb2ff1c2302cd54b" >Kobe Steel Titanium alloy with low alloying elements, ASTM Gr. 7, Annealed</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17755"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;280</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17755' href="/search/DataSheet.aspx?MatGUID=9eb0027aa33445ec97e76da9e6d3d7d4" >Kobe Steel KS40PDA Titanium alloy with low alloying elements, ASTM Gr. 11, Annealed</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17756"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;281</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17756' href="/search/DataSheet.aspx?MatGUID=abaa5eec683341ba831d85cdcde232f1" >Kobe Steel KSG12 Titanium alloy with low alloying elements, ASTM Gr. 12, Annealed</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17757"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;282</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17757' href="/search/DataSheet.aspx?MatGUID=7444c51a69e941f38f6cf127821e08b8" >Kobe Steel KS60AKOT Titanium alloy with low alloying elements, ASTM Gr. 33, Annealed</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17758"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;283</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17758' href="/search/DataSheet.aspx?MatGUID=aa3e8d7a14bc41d5b90bcdd122c5c208" >Kobe Steel KSTI-1.5AL Alpha Titanium Alloy, Annealed</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17759"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;284</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17759' href="/search/DataSheet.aspx?MatGUID=61c77414e74c4ddf88b52c2f86592659" >Kobe Steel KS6-2-4-2 Alpha Titanium Alloy, AMS 4976, Solution treated and aged</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17760"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;285</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17760' href="/search/DataSheet.aspx?MatGUID=b11bf5655a584cbf92dd7b1675b4cd0c" >Kobe Steel KS3-2.5 Alpha-Beta Titanium Alloy, ASTM Gr. 9, Annealed</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17761"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;286</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17761' href="/search/DataSheet.aspx?MatGUID=f6f3309f48aa4a34b00a4a3c45657ed4" >Kobe Steel KS6-4 Alpha-Beta Titanium Alloy, ASTM Gr. 5, Annealed</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17762"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;287</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17762' href="/search/DataSheet.aspx?MatGUID=aa21e15bb9ce4bdebef8afd3a62dafc9" >Kobe Steel KSTI-9 Alpha-Beta Titanium Alloy, ASTM Gr. 35, Annealed (coil, sheets)</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17763"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;288</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17763' href="/search/DataSheet.aspx?MatGUID=5c9923e20bd54eceaeae4752d9ea9bab" >Kobe Steel KS EL-F Alpha-Beta Titanium Alloy, Annealed</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17764"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;289</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17764' href="/search/DataSheet.aspx?MatGUID=aa53ecd3b3a044fc8585f4c42ceb14bf" >Kobe Steel KS-6-2-4-6 Alpha-Beta Titanium Alloy, AMS 4981, Solution treated and aged. Thickness &le; 3 in.</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17765"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;290</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17765' href="/search/DataSheet.aspx?MatGUID=23a792e648e3458481cc971113026b4a" >Kobe Steel KS15-3-3-3 Beta Titanium Alloy, AMS 4914, Solution treated</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17766"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;291</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17766' href="/search/DataSheet.aspx?MatGUID=0f8f9f18e17348479dfb5d2ecbb21547" >Kobe Steel KS15-3-3-3 Beta Titanium Alloy, AMS 4914, Solution treated and aged 482°C, 16 hrs - AC</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_17767"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;292</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_17767' href="/search/DataSheet.aspx?MatGUID=d1591911c7c442df84201dc09897a507" >Kobe Steel KS15-5-3 Beta Titanium Alloy, AMS 4914, Solution treated and aged, Forgings</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18051"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;293</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18051' href="/search/DataSheet.aspx?MatGUID=9b8d9280f69240cbab18130d7da2b516" >ATI Allegheny Ludlum Grade 1 Titanium (UNS R50250)</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18052"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;294</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18052' href="/search/DataSheet.aspx?MatGUID=b2efee5582df4aae8c397d0009ce7c37" >ATI Allegheny Ludlum Grade 2 Titanium (UNS R50400)</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18053"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;295</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18053' href="/search/DataSheet.aspx?MatGUID=357b13c1b9234030950c21f6ecb2835c" >ATI Allegheny Ludlum Grade 3 Titanium (UNS R50550)</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18054"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;296</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18054' href="/search/DataSheet.aspx?MatGUID=30dba497b27848ada223e0646704db71" >ATI Allegheny Ludlum Grade 4 Titanium (UNS R50700)</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18055"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;297</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18055' href="/search/DataSheet.aspx?MatGUID=78b68415e7f94c5bb42608aae72eeea4" >ATI Allegheny Ludlum Grade 5 Titanium 6Al-4V (UNS R56400)</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18056"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;298</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18056' href="/search/DataSheet.aspx?MatGUID=ee91ee4d423f43bbaf4d156b2495b1cf" >ATI Allegheny Ludlum Grade 23 (6-4 ELI) Titanium (UNS R56401)</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18057"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;299</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18057' href="/search/DataSheet.aspx?MatGUID=ed3c1d6031a64048a075962f62278a80" >ATI Allegheny Ludlum Grade 7 Titanium (UNS R52400)</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18058"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;300</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18058' href="/search/DataSheet.aspx?MatGUID=285b698cede54d18a4aff2ceb39c7336" >ATI Allegheny Ludlum Grade 9 Titanium (UNS R56320)</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18059"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;301</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18059' href="/search/DataSheet.aspx?MatGUID=a569cd1a46944bb6bf73f3e346b5e0e7" >ATI Allegheny Ludlum Grade 11 Titanium (UNS R52250)</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18060"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;302</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18060' href="/search/DataSheet.aspx?MatGUID=7f3253bfabf74c3ebe3a37222776a9f8" >ATI Allegheny Ludlum Grade 12 Titanium (UNS R53400)</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18061"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;303</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18061' href="/search/DataSheet.aspx?MatGUID=648219a994ca4a2b844b40dffbaaa1a5" >ATI Allegheny Ludlum Grade 16 Titanium (UNS R52402)</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18062"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;304</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18062' href="/search/DataSheet.aspx?MatGUID=b8df3bab8d3a43238307781c10c69301" >ATI Allegheny Ludlum Grade 17 Titanium (UNS R52252)</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_18063"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;305</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_18063' href="/search/DataSheet.aspx?MatGUID=1ce9826349d24ab4a5b7a411ba6f094f" >ATI Allegheny Ludlum Grade 18 Titanium (UNS R56322)</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_254751"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;306</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_254751' href="/search/DataSheet.aspx?MatGUID=8713191ee73940bcaf04dd960063dde0" >Lucas-Milhaupt 69-273 Titanium Joining Alloy</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_322913"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;307</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_322913' href="/search/DataSheet.aspx?MatGUID=7195434042c646078f1da44c26741cab" >EOS Titanium Ti64 Processed by DMSL on EOSINT M 270 and EOSINT M 280</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_322904"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;308</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_322904' href="/search/DataSheet.aspx?MatGUID=d4e2eeacc8474f8db96db4d899b2031c" >Desktop Metal Ti64 Studio System™ 3D Printed; As-Sintered</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_322905"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;309</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_322905' href="/search/DataSheet.aspx?MatGUID=14748fe435724fadb71f0614651961c2" >Proto3000 Ti64 MIM - ASTM F2885 As-Sintered</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_322906"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;310</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_322906' href="/search/DataSheet.aspx?MatGUID=feea0e91debd4094996cefdd7bf726f2" >Proto3000 Ti64 MIM - ASTM F2885 Densified Post Sintering</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_232389"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;311</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_232389' href="/search/DataSheet.aspx?MatGUID=12529329832a4a0295dbf5451b8c4c75" >Proto Labs Titanium Grade 5 Ti-6Al-4V - DMLS</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_19182"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;312</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_19182' href="/search/DataSheet.aspx?MatGUID=90e9a8f1fcda41ed8380dee928aa2935" >Sandvik Ti3Al2.5V Titanium Tubing</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_19183"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;313</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_19183' href="/search/DataSheet.aspx?MatGUID=26e19cd8019e4ae0869fb1efb9a7d8e0" >Sandvik Ti Grade 2 Seamless Titanium Tubing</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_199770"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;314</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_199770' href="/search/DataSheet.aspx?MatGUID=21727963e323415da50476633ab76f39" >Sandvik Bioline Ti6Al4V ELI Bar</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_199803"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;315</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_199803' href="/search/DataSheet.aspx?MatGUID=8584c90bfb5b4f4293394b0cfe71a433" >Sandvik Ti Grade 12 Tube and pipe, seamless</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_199808"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;316</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_199808' href="/search/DataSheet.aspx?MatGUID=d2e490cf1c244de495347ba65fcac568" >Sandvik Ti Grade 28 Tube and pipe, seamless</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_199811"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;317</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_199811' href="/search/DataSheet.aspx?MatGUID=178c1cbd2aac473d8d3cc41f012ea4fe" >Sandvik Ti Grade 9 Tube and pipe, seamless</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_78552"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;318</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_78552' href="/search/DataSheet.aspx?MatGUID=237df255c8324fe6a06dc004e86d86b6" >Arcam Ti6Al4V Titanium Alloy</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_19278"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;319</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_19278' href="/search/DataSheet.aspx?MatGUID=02989dc74d27499781538af7da1bd801" >TIMET 100A CP Titanium</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_19279"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;320</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_19279' href="/search/DataSheet.aspx?MatGUID=dfac17a35816412cb701fc7bede7de54" >TIMET TIMETAL® 10-2-3 Titanium Alloy (Ti-10V-2Fe-3Al), Aged Billet/Bar per ASTM 4984</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_19280"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;321</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_19280' href="/search/DataSheet.aspx?MatGUID=56fbc0e558dc4c958c1e5275540cf6a5" >TIMET TIMETAL® 10-2-3 Titanium Alloy (Ti-10V-2Fe-3Al), Aged Billet/Bar per ASTM 4986</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_19281"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;322</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_19281' href="/search/DataSheet.aspx?MatGUID=5a3be4e08ca74da3850c2545d88635e9" >TIMET TIMETAL® 10-2-3 Titanium Alloy (Ti-10V-2Fe-3Al), Aged Billet/Bar per ASTM 4987</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_200038"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;323</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_200038' href="/search/DataSheet.aspx?MatGUID=8f180575eba94ee6a5ab29937c6b5405" >TIMET TIMETAL® 10-2-3 Titanium Alloy (Ti-10V-2Fe-3Al), Solution Treated and Aged 950°F (510°C)</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_19282"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;324</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_19282' href="/search/DataSheet.aspx?MatGUID=489a00dc22d64bd5bea6fc222d32d826" >TIMET TIMETAL® 1100 Titanium Alloy (Ti-6Al-2.7Sn-4Zr-0.4Mo-0.45Si), Beta Anneal + 1400°F</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_200041"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;325</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_200041' href="/search/DataSheet.aspx?MatGUID=02abdbbd9153449fa6b8a5f04f4317e8" >TIMET TIMETAL® 1100 Titanium Alloy (Ti-6Al-2.7Sn-4Zr-0.4Mo-0.45Si), Mill Anneal</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_19283"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;326</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_19283' href="/search/DataSheet.aspx?MatGUID=eb1881d1b15f4dd5b1c102b031d47583" >TIMET TIMETAL® Code 12 Titanium Alloy (ASTM Grade 12)</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_19284"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;327</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_19284' href="/search/DataSheet.aspx?MatGUID=fcc07a1bcc044dab98fb713db6d5e336" >TIMET TIMETAL® 15-3 Titanium Alloy (Ti-15V-3Cr-3Sn-3Al), Anneal Strip/Sheet</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_19285"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;328</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_19285' href="/search/DataSheet.aspx?MatGUID=1917a998896446c58d2cb4ada5e12110" >TIMET TIMETAL® 15-3 Titanium Alloy (Ti-15V-3Cr-3Sn-3Al); Aged at 482°C/16 hr</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_19286"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;329</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_19286' href="/search/DataSheet.aspx?MatGUID=4ae09f72f3f742fd88f805a70ef185c4" >TIMET TIMETAL® 15-3 Titanium Alloy (Ti-15V-3Cr-3Sn-3Al); Aged at 538°C/8 hr.</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_200042"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;330</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_200042' href="/search/DataSheet.aspx?MatGUID=a0c43dad9eb849e0bf112aee0e30179f" >TIMET TIMETAL® 15-3 Titanium Alloy (Ti-15V-3Cr-3Sn-3Al); Aged at 496°C/8 hr</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_200043"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;331</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_200043' href="/search/DataSheet.aspx?MatGUID=948326f27d474d9a9c496d2cc0a79781" >TIMET TIMETAL® 15-3 Titanium Alloy (Ti-15V-3Cr-3Sn-3Al); Casting</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_19287"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;332</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_19287' href="/search/DataSheet.aspx?MatGUID=dae0636e1fca49a0905fa1bc941ab822" >TIMET TIMETAL® 17 Titanium Alloy (Ti-5Al-2Sn-4Mo-2Zr-6Mo); STA</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_19288"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;333</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_19288' href="/search/DataSheet.aspx?MatGUID=b3a3f05128584518914caaecff795b93" >TIMET TIMETAL® 21S Titanium Alloy (Ti-15Mo-3Nb-3Al-0.2Si, ASTM Grade 21); Annealed Strip/Sheet</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_19289"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;334</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_19289' href="/search/DataSheet.aspx?MatGUID=3ba5e0bf83ab47d69563c7fbe7d5295e" >TIMET TIMETAL® 21S Titanium Alloy (Ti-15Mo-3Nb-3Al-0.2Si, ASTM Grade 21); ST 1550°F (843°C) + Aged at 538°C</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_19290"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;335</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_19290' href="/search/DataSheet.aspx?MatGUID=88b153d02f07471f8034bedddf9ba894" >TIMET TIMETAL® 21S Titanium Alloy (Ti-15Mo-3Nb-3Al-0.2Si, ASTM Grade 21); ST 1550°F (843°C) + Aged at 593°C</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_19291"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;336</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_19291' href="/search/DataSheet.aspx?MatGUID=4a9f3bec3abc4178bfeb290a68784b5a" >TIMET TIMETAL® 21S Titanium Alloy (Ti-15Mo-3Nb-3Al-0.2Si, ASTM Grade 21); Overaged</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_199878"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;337</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_199878' href="/search/DataSheet.aspx?MatGUID=81a79528cadb406f93167fc9a928742e" >TIMET TIMETAL® 21S Titanium Alloy (Ti-15Mo-3Nb-3Al-0.2Si, ASTM Grade 21); ST 1550°F (843°C), No Age</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_199879"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;338</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_199879' href="/search/DataSheet.aspx?MatGUID=51c6e03c961c4d5788bc22b18ad2cd16" >TIMET TIMETAL® 21S Titanium Alloy (Ti-15Mo-3Nb-3Al-0.2Si, ASTM Grade 21); ST 1550°F (843°C) + Age 1275°F (691°C) and 1200°F (645°C)</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_199874"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;339</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_199874' href="/search/DataSheet.aspx?MatGUID=3ac057f7eed048dfbf52d3c31de65b19" >TIMET TIMETAL® 21S Titanium Alloy (Ti-15Mo-3Nb-3Al-0.2Si, ASTM Grade 21); ST 1550°F (843°C) - 1650°F (900°C), No Age</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_199875"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;340</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_199875' href="/search/DataSheet.aspx?MatGUID=077054fa7d6b4043878a27a5b2c6cc0c" >TIMET TIMETAL® 21S Titanium Alloy (Ti-15Mo-3Nb-3Al-0.2Si, ASTM Grade 21); ST 1550°F (843°C) - 1650°F (900°C) + Age 1000°F (538°C)</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_199876"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;341</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_199876' href="/search/DataSheet.aspx?MatGUID=0f3d69a8577b453cae6aced6bd0fc988" >TIMET TIMETAL® 21S Titanium Alloy (Ti-15Mo-3Nb-3Al-0.2Si, ASTM Grade 21); ST 1550°F (843°C) - 1650°F (900°C) + Age 1100°F (593°C)</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_199877"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;342</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_199877' href="/search/DataSheet.aspx?MatGUID=f788d706b9e344d891db321817b4fb09" >TIMET TIMETAL® 21S Titanium Alloy (Ti-15Mo-3Nb-3Al-0.2Si, ASTM Grade 21); ST 1550°F (843°C) - 1650°F (900°C) + Age 1275°F (691°C) and 1200°F (645°C)</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_200037"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;343</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_200037' href="/search/DataSheet.aspx?MatGUID=a707deacaf9a4ffd9e49a72bb865ffa9" >TIMET TIMETAL® 21S Titanium Alloy (Ti-15Mo-3Nb-3Al-0.2Si, ASTM Grade 21); ST 1500°F (816°C) + Age 1000°F (538°C)</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_199880"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;344</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_199880' href="/search/DataSheet.aspx?MatGUID=da9bfe2c0de640b29cb51d9642615b3e" >TIMET TIMETAL® 21S Titanium Alloy (Ti-15Mo-3Nb-3Al-0.2Si, ASTM Grade 21); 40% Cold Work</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_199881"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;345</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_199881' href="/search/DataSheet.aspx?MatGUID=d30859093636499eac9510d4ce5a23a2" >TIMET TIMETAL® 21S Titanium Alloy (Ti-15Mo-3Nb-3Al-0.2Si, ASTM Grade 21); 60% Cold Work</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_199882"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;346</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_199882' href="/search/DataSheet.aspx?MatGUID=6ec46d6bb9074356a9bad1c0b14a1bfe" >TIMET TIMETAL® 21S Titanium Alloy (Ti-15Mo-3Nb-3Al-0.2Si, ASTM Grade 21); 80% Cold Work</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_199564"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;347</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_199564' href="/search/DataSheet.aspx?MatGUID=aa72b70ce95a4b51adf7efc5cdfbaae8" >TIMET TIMETAL® 21S Titanium + Silicon Carbide Fiber Composite</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_19292"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;348</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_19292' href="/search/DataSheet.aspx?MatGUID=45dd8a85509d46e5a55a584b3d7c07f4" >TIMET TIMETAL® 230 Titanium Alloy (Ti-2.5Cu); Annealed</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_19293"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;349</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_19293' href="/search/DataSheet.aspx?MatGUID=b4bbeefc2d6c4a83846df6d9afed507b" >TIMET TIMETAL® 230 Titanium Alloy (Ti-2.5Cu); STA</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_19294"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;350</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_19294' href="/search/DataSheet.aspx?MatGUID=3695f9f62f87457db57cb148a93399de" >TIMET TIMETAL® 3-2.5 Titanium Alloy (Ti-3Al-2.5V; ASTM Grade 9) Typical Properties</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_19295"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;351</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_19295' href="/search/DataSheet.aspx?MatGUID=5ee03d6f98b7453681190e3ab5a84e1f" >TIMET TIMETAL® 3-2.5 Titanium Alloy (Ti-3Al-2.5V; ASTM Grade 9) Annealed</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_19296"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;352</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_19296' href="/search/DataSheet.aspx?MatGUID=c1f696e84947442a8049120cdfc12013" >TIMET TIMETAL® 3-2.5 Titanium Alloy (Ti-3Al-2.5V; ASTM Grade 9) Aged</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_19297"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;353</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_19297' href="/search/DataSheet.aspx?MatGUID=34154c2a7d02402c9e1383687b46ee54" >TIMET TIMETAL® 3-2.5 Titanium Alloy (Ti-3Al-2.5V; ASTM Grade 9) CWSR</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_19298"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;354</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_19298' href="/search/DataSheet.aspx?MatGUID=7dd9c5ca85e74782a3551d307bccc24a" >TIMET TIMETAL® 3-2.5-0.05Pd Titanium Alloy</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_19299"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;355</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_19299' href="/search/DataSheet.aspx?MatGUID=23dd0844e7b34d1894638845471ccf22" >TIMET TIMETAL® 35A CP Titanium (ASTM Grade 1)</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_19300"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;356</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_19300' href="/search/DataSheet.aspx?MatGUID=2d9cc872710c47aab45325a2135eee72" >TIMET TIMETAL® 367 (Ti-6Al-7Nb) Implant Grade Titanium Alloy</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_19301"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;357</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_19301' href="/search/DataSheet.aspx?MatGUID=247af0c81c8a4a9da817bafa791d20bf" >TIMET TIMETAL® 50A CP Titanium (ASTM Grade 2)</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_19302"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;358</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_19302' href="/search/DataSheet.aspx?MatGUID=65f65c787723483d904e642d0a6d8576" >TIMET TIMETAL® 5111 Titanium Alloy (Ti-5Al-1Sn-1Zr-1V-0.8Mo; ASTM Grade 32)</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_19303"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;359</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_19303' href="/search/DataSheet.aspx?MatGUID=1d8fc8ee9e0146aeac6bdceb2ba8b304" >TIMET TIMETAL® 550 (Ti-4Al-4Mo-2Sn) Titanium Alloy Solution Treated</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_19304"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;360</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_19304' href="/search/DataSheet.aspx?MatGUID=904e68864cd74d7787a0602d00ae0875" >TIMET TIMETAL® 550 (Ti-4Al-4Mo-2Sn) Titanium Alloy Solution Treated and Aged</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_19305"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;361</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_19305' href="/search/DataSheet.aspx?MatGUID=b60cfb9aa7ed4c4cb3e2690163609cb5" >TIMET TIMETAL® 551 (Ti-4Al-4Mo-4Sn-0.5Si) Titanium Alloy; <25 mm</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_19306"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;362</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_19306' href="/search/DataSheet.aspx?MatGUID=82caf528ab1946ccaf3aa9cc6d321b48" >TIMET TIMETAL® 551 (Ti-4Al-4Mo-4Sn-0.5Si) Titanium Alloy; 25-100 mm</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_19307"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;363</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_19307' href="/search/DataSheet.aspx?MatGUID=8cac2808180543e59117f97c3942567f" >TIMET TIMETAL® 6-2-4-2 (Ti-6Al-2Sn-4Zr-2Mo-0.08Si) Titanium Alloy</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_19308"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;364</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_19308' href="/search/DataSheet.aspx?MatGUID=aee14a161823408e93c80aa88b6e6d5e" >TIMET TIMETAL® 6-2-4-6 Titanium Alloy (Ti-6Al-2Sn-4Zr-6Mo), Typical</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_19309"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;365</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_19309' href="/search/DataSheet.aspx?MatGUID=1b15322ebe0d4f4b964c01e23c8aeedf" >TIMET TIMETAL® 6-2-4-6 Titanium Alloy (Ti-6Al-2Sn-4Zr-6Mo), 10% Equiaxed Alpha + Annealed</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_19310"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;366</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_19310' href="/search/DataSheet.aspx?MatGUID=23eeb21f2a6146759a7f4271303a3ced" >TIMET TIMETAL® 6-2-4-6 Titanium Alloy (Ti-6Al-2Sn-4Zr-6Mo), 10% Equiaxed Alpha + STA</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_19311"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;367</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_19311' href="/search/DataSheet.aspx?MatGUID=b621eead5c224d8b88c3430b1958dbe1" >TIMET TIMETAL® 6-2-4-6 Titanium Alloy (Ti-6Al-2Sn-4Zr-6Mo), 50% Equiaxed Alpha + Annealed</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_19312"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;368</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_19312' href="/search/DataSheet.aspx?MatGUID=3ef181d3659f4e0ea7ac05ba28344888" >TIMET TIMETAL® 6-2-4-6 Titanium Alloy (Ti-6Al-2Sn-4Zr-6Mo), 50% Equiaxed Alpha + STA</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_19313"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;369</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_19313' href="/search/DataSheet.aspx?MatGUID=e1df51088d5c4407837914b0ee275867" >TIMET TIMETAL® 6-2-4-6 Titanium Alloy (Ti-6Al-2Sn-4Zr-6Mo), 50% Equiaxed Alpha + STOA</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_19314"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;370</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_19314' href="/search/DataSheet.aspx?MatGUID=01a794651a32416abda93441b1ad8948" >TIMET TIMETAL® 6-2-4-6 Titanium Alloy (Ti-6Al-2Sn-4Zr-6Mo), 50% Elongated Alpha + STA</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_19315"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;371</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_19315' href="/search/DataSheet.aspx?MatGUID=aaa9b6120db84df7a9d9c4d33b8570d1" >TIMET TIMETAL® 6-2-4-6 Titanium Alloy (Ti-6Al-2Sn-4Zr-6Mo), 20% Elongated Alpha + STA</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_19316"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;372</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_19316' href="/search/DataSheet.aspx?MatGUID=1f2e324ad8e54f2986f788808fc02456" >TIMET TIMETAL® 6-2-4-6 Titanium Alloy (Ti-6Al-2Sn-4Zr-6Mo), Beta Forged + STA</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_200039"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;373</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_200039' href="/search/DataSheet.aspx?MatGUID=63fd03322d53437abc07c34a31a8b4f7" >TIMET TIMETAL® 6-2-4-6 Titanium Alloy (Ti-6Al-2Sn-4Zr-6Mo), Triplex Annealed Sheet</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_19317"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;374</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_19317' href="/search/DataSheet.aspx?MatGUID=47d3efba08a74220b4dfaf2b5da30885" >TIMET TIMETAL® 62S Titanium Alloy (Ti-6Al-2Fe-0.1Si); Annealed Sheet/Plate & Billet/Bar</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_19318"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;375</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_19318' href="/search/DataSheet.aspx?MatGUID=a2bc9f5b27c04530a29dff7699a3cf78" >TIMET TIMETAL® 62S Titanium Alloy (Ti-6Al-2Fe-0.1Si); Alpha-Beta Forged, RA</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_19319"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;376</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_19319' href="/search/DataSheet.aspx?MatGUID=e7e88c92e9cf428ba2197520c548beb5" >TIMET TIMETAL® 62S Titanium Alloy (Ti-6Al-2Fe-0.1Si); Beta Forged</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_200040"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;377</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_200040' href="/search/DataSheet.aspx?MatGUID=1830b010cd20413d850810c8fee7024d" >TIMET TIMETAL® 62S Titanium Alloy (Ti-6Al-2Fe-0.1Si); Beta Rolled plus Mill Annealed</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_19320"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;378</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_19320' href="/search/DataSheet.aspx?MatGUID=2b38b0d17e284ffcb41a7188f37f2d4e" >TIMET TIMETAL® 6-4 Titanium Alloy (Ti-6Al-4V; ASTM Grade 5) Sheet</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_19321"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;379</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_19321' href="/search/DataSheet.aspx?MatGUID=f0d81a62a0564398b1b17e851841e0c4" >TIMET TIMETAL® 6-4 Titanium Alloy (Ti-6Al-4V; ASTM Grade 5) Rod</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_19322"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;380</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_19322' href="/search/DataSheet.aspx?MatGUID=d98573ff5b2d4c14a7f59e7eac2f49cf" >TIMET TIMETAL® 6-4 Titanium Alloy (Ti-6Al-4V; ASTM Grade 5) Fastener Stock</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_19323"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;381</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_19323' href="/search/DataSheet.aspx?MatGUID=a1fc12bc5d04434a8e617a745f46624b" >TIMET TIMETAL® 6-4 Titanium Alloy (Ti-6Al-4V; ASTM Grade 5) Sheet and Plate, 0.025-1.000 in, Annealed, Per ASTM B265</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_19324"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;382</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_19324' href="/search/DataSheet.aspx?MatGUID=b978fc9e4f194184b038fdb3c9bae53a" >TIMET TIMETAL® 6-4 Titanium Alloy (Ti-6Al-4V; ASTM Grade 5) Rod or Thickness, <3.00 in, Annealed, Per ASTM B348</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_19325"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;383</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_19325' href="/search/DataSheet.aspx?MatGUID=dae30e94b34144e2a29052461c535320" >TIMET TIMETAL® 6-4 Titanium Alloy (Ti-6Al-4V; ASTM Grade 5) Rod or Thickness, <4.00 in, Annealed, Per Mil-T-9047G</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_19326"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;384</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_19326' href="/search/DataSheet.aspx?MatGUID=aadfbac5c7c24d91a5a1ed61d396068c" >TIMET TIMETAL® 6-4 Titanium Alloy (Ti-6Al-4V; ASTM Grade 5) Rod or Thickness, 4.00-6.00 in, Annealed, Per Mil-T-9047G</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_19327"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;385</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_19327' href="/search/DataSheet.aspx?MatGUID=98ef3be0d87943829b400f5342aab97e" >TIMET TIMETAL® 6-4 Titanium Alloy (Ti-6Al-4V; ASTM Grade 5), <0.500 in, STD, Per Mil-T-9047G</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_19328"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;386</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_19328' href="/search/DataSheet.aspx?MatGUID=34494e6d7e66478f97ea9bdfda34e077" >TIMET TIMETAL® 6-4 Titanium Alloy (Ti-6Al-4V; ASTM Grade 5), >0.500-1.000 in, STD, Per Mil-T-9047G</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_19329"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;387</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_19329' href="/search/DataSheet.aspx?MatGUID=14e0485466c84f1fa51139279400f4ca" >TIMET TIMETAL® 6-4 Titanium Alloy (Ti-6Al-4V; ASTM Grade 5), >1.000-1.500 in, STD, Per Mil-T-9047G</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_19330"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;388</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_19330' href="/search/DataSheet.aspx?MatGUID=b38280eaca1340ec8f73978c2398ea55" >TIMET TIMETAL® 6-4 Titanium Alloy (Ti-6Al-4V; ASTM Grade 5), Rod, Square, or Hex, >1.500-2.000 in, Per Mil-T-9047G</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_19331"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;389</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_19331' href="/search/DataSheet.aspx?MatGUID=d54db3927418407c9fe43e1355401054" >TIMET TIMETAL® 6-4 Titanium Alloy (Ti-6Al-4V; ASTM Grade 5), Rod, Square, or Hex, >2.000-3.000 in, Per Mil-T-9047G</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_19332"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;390</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_19332' href="/search/DataSheet.aspx?MatGUID=77ef1dee1ed947d680fb67b9f0026df2" >TIMET TIMETAL® 6-4 Titanium Alloy (Ti-6Al-4V-0.1Ru; ASTM Grade 29)</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_19333"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;391</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_19333' href="/search/DataSheet.aspx?MatGUID=833c10adb9e1455582eec3489428ec06" >TIMET TIMETAL® 65A CP Titanium (ASTM Grade 3)</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_19334"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;392</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_19334' href="/search/DataSheet.aspx?MatGUID=9d40aceb99694b7cb59be63442cd0087" >TIMET 6-6-2 Titanium Alloy (Ti-6Al-6V-2Sn), Annealed</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_19335"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;393</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_19335' href="/search/DataSheet.aspx?MatGUID=ba67c8ef282d4e238a399c4003708da2" >TIMET 6-6-2 Titanium Alloy (Ti-6Al-6V-2Sn), Solution Treated and Aged</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_19336"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;394</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_19336' href="/search/DataSheet.aspx?MatGUID=576b880a91d44be6b4c03b52520b0c14" >TIMET TIMETAL® 679 (Ti-11Sn-5Zr-2.25Al-1Mo-0.2Si) Titanium Alloy; Oil Quenched & Aged</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_200045"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;395</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_200045' href="/search/DataSheet.aspx?MatGUID=a8ecacc208334a949a132254ed4f9504" >TIMET TIMETAL® 679 (Ti-11Sn-5Zr-2.25Al-1Mo-0.2Si) Titanium Alloy; Air Cooled & Aged</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_19337"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;396</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_19337' href="/search/DataSheet.aspx?MatGUID=4061d9ec31784f4a9e9468b19acc34b5" >TIMET TIMETAL® 685 (Ti-6Al-5Zr-0.5Mo-0.25Si) Titanium Alloy</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_199547"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;397</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_199547' href="/search/DataSheet.aspx?MatGUID=a15d4347365046459a3601a3341208fe" >TIMET TIMETAL® 7-4 Alpha-Beta Titanium (Ti-7Al-4Mo)</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_19338"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;398</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_19338' href="/search/DataSheet.aspx?MatGUID=fb6c8590eea042bea6622c1e51b43438" >TIMET 75A CP Titanium (ASTM Grade 4)</a></td></tr>
<tr class="altrow" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_19339"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;399</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_19339' href="/search/DataSheet.aspx?MatGUID=dcf13a02c6d74284ba1a5317405f0fea" >TIMET 8-1-1 Titanium Alloy (Ti-8Al-1Mo-1V); Annealed Sheet</a></td></tr>
<tr class="" onmouseover="matweb.DataRow_OnMouseOver(this)" onmouseout="matweb.DataRow_OnMouseOut(this)" ><td style="width:65px;" ><input type="checkbox" id="chkFolder_19340"  title="Add Material to Folder" onclick='UnregisteredUserMessage(this);'>&nbsp;400</td><td style="width:auto;"></td><td style="width:auto; font-weight:bold;"><a id='lnkMatl_19340' href="/search/DataSheet.aspx?MatGUID=c5498093128c46c78f442efe667a586b" >TIMET TIMETAL® 829 (Ti-5.5Al-3.5Sn-3Zr-1Nb-0.25Mo-0.3Si) Titanium Alloy</a></td></tr>

		
	</table>

	<table style="background-color:Silver;" class="tabledataformat" >

		<tr class="">
			<th>&nbsp;</th>
		</tr>

		<tr>
			<td><strong>Found <span id="ctl00_ContentMain_UcSearchResults1_lblResultCount2">429</span> Results</strong> -- 
				
				Page 
				<select name="ctl00$ContentMain$UcSearchResults1$drpPageSelect2" onchange="javascript:setTimeout('__doPostBack(\'ctl00$ContentMain$UcSearchResults1$drpPageSelect2\',\'\')', 0)" id="ctl00_ContentMain_UcSearchResults1_drpPageSelect2">
		<option value="1">1</option>
		<option selected="selected" value="2">2</option>
		<option value="3">3</option>

	</select>
				of <span id="ctl00_ContentMain_UcSearchResults1_lblPageTotal2">3</span> 
				
				-- 
				<a id="ctl00_ContentMain_UcSearchResults1_lnkPrevPage2" href="javascript:__doPostBack('ctl00$ContentMain$UcSearchResults1$lnkPrevPage2','')" style="color:Black;">[Prev Page]</a>
				<a id="ctl00_ContentMain_UcSearchResults1_lnkNextPage2" href="javascript:__doPostBack('ctl00$ContentMain$UcSearchResults1$lnkNextPage2','')" style="color:Black;">[Next Page]</a>
				--&nbsp;

				view 
				<select name="ctl00$ContentMain$UcSearchResults1$drpPageSize2" onchange="javascript:setTimeout('__doPostBack(\'ctl00$ContentMain$UcSearchResults1$drpPageSize2\',\'\')', 0)" id="ctl00_ContentMain_UcSearchResults1_drpPageSize2">
		<option value="25">25</option>
		<option value="50">50</option>
		<option value="75">75</option>
		<option value="100">100</option>
		<option value="150">150</option>
		<option selected="selected" value="200">200</option>

	</select> per page
			</td>
		</tr>
	</table>
	
	<span id="ctl00_ContentMain_UcSearchResults1_divDiscontinued" style="font-size:11px;color:red">
	<span id="ctl00_ContentMain_UcSearchResults1_lblDisclaimer"><br />
Materials flagged as discontinued (<img src="/images/buttons/iconDiscontinued.jpg" alt="" />) are no longer part of the manufacturer’s standard product line according to our latest information.  These materials may be available by special order, in distribution inventory, or reinstated as an active product.  Data sheets from materials that are no longer available remain in MatWeb to assist users in finding replacement materials.  
<br /><br />
Users of our Advanced Search (registration required) may exclude discontinued materials from search results.</span>
	</span> 


</div>





<script type="text/javascript">

	//GLOBAL VARIABLES
	var gCurrentCheckbox;
	var gNewFolderID;
	var gOldFolderName;
	var gSelectedFolderID = 0;
	var gMaxFolders = 0;

	//GLOBAL FIELD REFERENCES
	var drpFolderList = document.getElementById("ctl00_ContentMain_UcSearchResults1_drpFolderList");
	var drpFolderAction = document.getElementById("ctl00_ContentMain_UcSearchResults1_drpFolderAction");
	var txtFolderMatCount = document.getElementById("ctl00_ContentMain_UcSearchResults1_txtFolderMatCount");
	var divUserMessage = document.getElementById("divUserMessage");
	var divFolderName = document.getElementById("divFolderName");
	var tblFolderName = document.getElementById("tblFolderName");
	var txtFolderName = document.getElementById("txtFolderName2");
	
	// See MS ajax.net client side documentation
	// http://ajax.asp.net/docs/ClientReference/Sys.Net/WebrequestManagerClass/	
	Sys.Net.WebRequestManager.add_invokingRequest(ucSearchResults_OnRequestStart);
	Sys.Net.WebRequestManager.add_completedRequest(ucSearchResults_OnRequestEnd);
	
	var timerID = 0;

	function ucSearchResults_OnRequestStart(){
		document.body.style.cursor="progress";
	}
	
	function ucSearchResults_OnRequestEnd(){
		document.body.style.cursor="auto";
	}

	//drpFolderAction pull down does not exist for anonymous users
	if(drpFolderAction) {
		drpFolderAction.onchange=RunFolderAction;
	}

	//RUN ONCE on page load
	//matweb.register_onload(SetSelectedFolder);
	
	SearchMatIDList = []; //an array of MatIDs
	//matweb.alert(MatIDList);
	
	//======================
	//Test Service 
	//======================
	//TestService("This is a test");
	//aci.matweb.WebServices.Folders.RaiseTestError(null,ErrorHandler,"testerror");
	
	function TestService(arg){
		aci.matweb.WebServices.Folders.HelloWorld(arg, TestService_CALLBACK,ErrorHandler,"TestService");
	}
	
	function TestService_CALLBACK(data,context){
		alert(data);
	}
	
	// =================================
	// This function is the onchange event for the material checkbox. See Code Behind.
	// The first time a material is checked, and there are no folders, the folder web service creates a NEW FOLDER
	// =================================
	function ToggleMat(chkBox,MatID,MaxFolderMats){
	
		HideFolderNameControls();

		var FolderID,Context;
		gCurrentCheckbox = chkBox;
		var matLink = $("lnkMatl_" + MatID);
		var MatName = matLink.innerHTML;
		//alert(MatName);

		var SelectedOption = drpFolderList.options[drpFolderList.selectedIndex];  

		if(gNewFolderID > 0)
			FolderID = gNewFolderID;
		else
			FolderID = SelectedOption.value; //this will be 0 if no folders exist yet
		//alert(FolderID);
		
		// call the folder web service 
		// See: ScriptManagerProxy object at the top of this page
		// Also See: /folders/FolderService.asmx & /App_Code/FolderService.cs
		if(chkBox.checked){
			Context = "AddMat";
			aci.matweb.WebServices.Folders.AddMaterial(FolderID, MatID, MatName, ToggleMat_CALLBACK,ErrorHandler,Context);
		}
		else{
			Context = "RemoveMat";
			aci.matweb.WebServices.Folders.RemoveMaterial(FolderID, MatID, ToggleMat_CALLBACK,ErrorHandler,Context);
		}
			
	}
	
	// =================================
	// CALLBACK FUNCTION
	// this function is called by the AJAX framework after the web service is invoked
	// The folder web service returns a ReturnData type object.
	// =================================
	function ToggleMat_CALLBACK(data,context){

		 //alert(data);
		// alert(context);

		var FolderMatCount = data.FolderMatCount
		var NewFolderID = data.NewFolderID
		var DisplayMessage = data.DisplayMessage
		var MaxFolderMats = data.FolderMaxMatls;
		
		txtFolderMatCount.value = FolderMatCount + "/" + MaxFolderMats;

		// If this is the first time the user checked the checkbox, and the user did not already have any folders, 
		// a new folder may have been created.
		// Save the new folderID to a hidden field, so we can reference it later.
		if(NewFolderID > 0){
			gNewFolderID = NewFolderID;
		}
		
		// alert(gNewFolderID);
		// alert(FolderCount);
		// alert(ReloadFolderList);
		// if(NewFolderID>0) alert(NewFolderID);
		// alert(MaxFolderMats);
		
		switch(context){
		case "AddMat":
			if(DisplayMessage){
				gCurrentCheckbox.checked = false;
				matweb.alert(DisplayMessage);
			}
			break    
		case "RemoveMat":
			break    
		default:
			matweb.Alert("Unhandeled Context in ToggleMat_CALLBACK() function: " + Context);
		}//end switch

	}//end function

	
	//=================================
	function CompareMaterials(){
	//=================================
		var SelectedFolderID = GetSelectedFolderID();
		window.location.href="/folders/Compare.aspx?FolderID=" + SelectedFolderID;
	}

	//=================================
	// Runs on drpFolderList.onchange, and also is called by other methods.
	// Sets the checkbox for each visible material in the folder.
	// Also sets txtFolderMatCount with the MatCount indicator for the selected folder.
	//=================================
	function SetSelectedFolder(){
		//alert("SetSelectedFolder()");
		var SelectedFolderID = GetSelectedFolderID();
		//alert("SelectedFolderID :" + SelectedFolderID)
		
		aci.matweb.WebServices.Folders.SetSelectedFolder(SelectedFolderID,SetSelectedFolder_CALLBACK,ErrorHandler,"update_txtFolderMatCount");
		
		HideFolderNameControls();
	}

	// =================================
	// CALLBACK FUNCTION
	// Returns a list of MatID's in the selected folder, and sets the checkbox for each material.
	// Also sets txtFolderMatCount with the MatCount indicator for the selected folder.
	// =================================
	function SetSelectedFolder_CALLBACK(data,context){

		// alert(data);
		// alert(context);

		// the folder web service returns an object with 4 fields
		//alert("data.FolderMatCount: " + data.FolderMatCount);
		//alert("data.FolderMaxMatls: " + data.FolderMaxMatls);
		//alert("data.DisplayMessage: " + data.DisplayMessage);
		//alert("data.MatIDList: " + data.FolderMatIDList);

		if(data.DisplayMessage){
			matweb.alert(data.DisplayMessage);
		}

		txtFolderMatCount.value = data.FolderMatCount + "/" + data.FolderMaxMatls;

		//alert(SearchMatIDList);
		var chkFolder;

		// Set checkboxes for any visible materials in the folder
		// loop over all materials in the search results for the current page...
		//alert('lenOfArray=' + SearchMatIDList.length);
		for(s=0;s<SearchMatIDList.length;s++){
			chkFolder = document.getElementById("chkFolder_" + SearchMatIDList[s]);
			//alert('index=' + SearchMatIDList[s]);
			chkFolder.checked = false; //clear any currently checked boxes...
			//loop over all materials in the folder...
			//If there is a match, make it checked.
			for(f=0;f<data.FolderMatIDList.length;f++){
				if(SearchMatIDList[s]==data.FolderMatIDList[f]){
					//alert("Match found: " + data.FolderMatIDList[f]);
					chkFolder.checked = true;
				}
			}
		}

	}//end function
	
	//=================================
	function RunFolderAction(){
	//=================================
	
		//alert("RunFolderAction()");
		var action = drpFolderAction.options[drpFolderAction.selectedIndex].value;
		var SelectedFolderName = drpFolderList.options[drpFolderList.selectedIndex].text;
		var FolderID = GetSelectedFolderID();
		var msg = "";
		
		switch(action){
			case "empty":
				aci.matweb.WebServices.Folders.EmptyFolder(FolderID,SetSelectedFolder_CALLBACK,ErrorHandler,action);
				msg = "Folder ["+SelectedFolderName+"] was cleared of materials";
				HideFolderNameControls();
				break;
			case "delete":
				var answer = confirm("Delete folder ["+SelectedFolderName+"] ?");
				if(answer){
					gOldFolderName = SelectedFolderName;
					aci.matweb.WebServices.Folders.DeleteFolder(FolderID,DeleteFolder_CALLBACK,ErrorHandler,action);
				}
				HideFolderNameControls();
				break;
			case "new":
				//setting up new folder controls
				if(drpFolderList.options.length >= gMaxFolders){
					matweb.alert("You have already added as many folders as you are currently allowed by the system (" + gMaxFolders + ").<p/> You may view our <a href=\"/membership/benefits.aspx\">benefits</a> page for the features that are allowed for your current user level.");
				}
				else{
				divFolderName.style.display="block";
				divFolderName.innerHTML="New Folder Name";
				tblFolderName.style.display="block";
				txtFolderName.value = "New Folder Name";
				txtFolderName.focus();
				txtFolderName.select();
				}
				break;
			case "rename":
				//setting up rename folder controls
				divFolderName.style.display="block";
				divFolderName.innerHTML="Rename Folder";
				tblFolderName.style.display="block";
				txtFolderName.value = SelectedFolderName;
				txtFolderName.focus();
				txtFolderName.select();
				break;
			case "managefolders":
				window.location.href="/folders/ListFolders.aspx";
				break;
			case "expSolidworks":
				ExportFolder(FolderID, 2, null);
				break;
			case "expALGOR":
				ExportFolder(FolderID, 3, null);
				break;
			case "expANSYS":
				ExportFolder(FolderID, 4, null);
				break;
			case "expNEiWorks":
				ExportFolder(FolderID, 5, null);
				break;
			case "expCOMSOL3":
				ExportFolder(FolderID, 6, '3.4 or newer');
				break;
			case "expCOMSOL4":
				ExportFolder(FolderID, 10, null);
				break;
			case "expETBX":
				ExportFolder(FolderID, 8, null);
				break;
			case "expSpaceClaim":
				ExportFolder(FolderID, 9, null);
				break;
			case "0":
				//do nothing
				drpFolderAction.selectedIndex = 0;
				break;
			default: 
				alert("Unhandled Action: " + action);
		}

		//drpFolderAction.selectedIndex=0;
		divUserMessage.innerHTML = msg;
		//alert("End RunFolderAction()");
		
	}
	
	function DeleteFolder_CALLBACK(DeleteWasSuccessfull,context){
		divUserMessage.innerHTML = "Folder ["+gOldFolderName+"] was deleted.";
		//Refresh the folder list and selected checkboxes
		aci.matweb.WebServices.Folders.GetFolderList(GetFolderList_CALLBACK,ErrorHandler,"DeleteFolder_CALLBACK");
	}	

	// rsw
	function ExportFolder(folderid, modeid, comsolversion){
		if(txtFolderMatCount.value.substring(0,1) == '0'){
			alert('Folder is empty, nothing to export.');
			drpFolderAction.selectedIndex = 0;
			return;
		}

		var ExportPath = "/folders/ListFolders.aspx";
		
		//alert(ExportPath);
		location.href = ExportPath;
		//window.open(ExportPath, '', '', '');
	}
	
	
	
	
	//===============================================================
	// This runs on the onclick event for btnApplyFolderName.
	// It handles creating a new folder, or renaming an existing one.
	//===============================================================
	function ApplyFolderName(){
	
		//alert("ApplyFolderName()");
		var action = drpFolderAction.options[drpFolderAction.selectedIndex].value;
		var NewFolderName = txtFolderName.value;
		var FolderID = GetSelectedFolderID();
		var msg = "";

		//alert(FolderID);

		//If we are showing the non-existant "virtual" folder "My Folder",
		//create it first, and then rename it.
		if(action=="rename" && FolderID==0)
			action="add_and_rename";

		switch(action){
			case "new":
				aci.matweb.WebServices.Folders.AddFolder(NewFolderName,AddRenameFolder_CALLBACK,ErrorHandler,action);
				break;
			case "rename":
				gOldFolderName = drpFolderList.options[drpFolderList.selectedIndex].text;
				aci.matweb.WebServices.Folders.RenameFolder(FolderID,NewFolderName,AddRenameFolder_CALLBACK,ErrorHandler,action);
				break;
			case "add_and_rename":
				gOldFolderName = drpFolderList.options[drpFolderList.selectedIndex].text;
				aci.matweb.WebServices.Folders.AddFolder(NewFolderName,AddRenameFolder_CALLBACK,ErrorHandler,action);
				break;
			default: 
				alert("Unhandled Action in ApplyFolderName(): " + action);
		}

		HideFolderNameControls();
		
	}
	
	//=================================
	// CALLBACK FUNCTION
	//=================================
	function AddRenameFolder_CALLBACK(ReturnData,context){
		//alert("AddRenameFolder_CALLBACK()");
		
		var msg;
		//Folder = ReturnData.Folder;
	
		switch(context){
			case "new":
				//gNewFolderID = Folder.FolderID;
				gNewFolderID = ReturnData.NewFolderID;
				msg = "The folder [" + ReturnData.NewFolderName + "] was created.";
				break;
			case "rename":
				msg = "The folder [" + gOldFolderName+ "] was renamed to [" + ReturnData.NewFolderName + "].";
				break;
			case "add_and_rename":
				//gNewFolderID = Folder.FolderID;
				gNewFolderID = ReturnData.NewFolderID;
				msg = "The folder [" + gOldFolderName+ "] was renamed to [" + ReturnData.NewFolderName + "].";
				break;
		}
		
		divUserMessage.innerHTML = msg;
		
		//refresh the folder list, so it includes the new folder
		aci.matweb.WebServices.Folders.GetFolderList(GetFolderList_CALLBACK,ErrorHandler,"AddFolder");

	}
	
	//=================================
	function HideFolderNameControls(){
	//=================================
		//These fields will not exist for anonymous users
		if(drpFolderAction){
			drpFolderAction.selectedIndex=0;
			divFolderName.style.display="none";
			tblFolderName.style.display="none";
		}
	}
	
	//=================================
	function GetSelectedFolderID(){
	//=================================
		var FolderID = 0;
		//if one of the processeses created a new folder id, then use it.
		// alert('gNewFolderID=' + gNewFolderID);
		if(gNewFolderID > 0){
			FolderID = gNewFolderID;
			//gNewFolderID = 0;//clear the new folder id, so we don't use it again.
		}
		else 
			if(drpFolderList != null){
				if(drpFolderList.options.length == 0){
					FolderID = gSelectedFolderID;
				}
				else{
					var SelectedOption = drpFolderList.options[drpFolderList.selectedIndex];  
					FolderID = SelectedOption.value; //this will be 0 if no folders exist yet
				}
			}
		return FolderID;
	}

	//=================================
	function UnregisteredUserMessage(chkBox){
	//=================================
		matweb.alert("This feature is reserved for registered users.<p/><a href='/membership/regstart.aspx'>Click here to register.</a>","Reserved Feature");
		chkBox.checked = false;
	}

	// =================================
	// CALLBACK FUNCTION
	// The folder web service returns a JS array of iBatis data objects of type: aci.matweb.data.folder.Folder 
	// The drop down list of folders is set using this list.
	// =================================
	function GetFolderList_CALLBACK(iFolderList,Context){
	
		//alert(iFolderList);
		//alert('iFolderList.length: ' + iFolderList.length);

		if(drpFolderList != null){
		
		var SelectedFolderID = GetSelectedFolderID();//remember the current selected folder, so we can use it later
		var opt;
		var folder;

		//clear the options and reload them
		drpFolderList.options.length = 0;

		//add a default empty folder...		
		if(iFolderList.length==0){
			opt = document.createElement('OPTION');
			opt.value = "0";
			opt.text = "My Folder";
			drpFolderList.options.add(opt);
			i++;
		}
		
		for(var i = 0;i < iFolderList.length;i++){
			opt = document.createElement('OPTION');
			folder = iFolderList[i];
			//alert(folder.FolderID);
			//alert(folder.FolderName);
			opt.value = folder.FolderID;
			opt.text = folder.FolderName;
			if(folder.FolderID == SelectedFolderID) opt.selected = true;
			drpFolderList.options.add(opt);
		}

		//clear the new folder id, so we don't use it again. 
		gNewFolderID = 0;
		
		//Set checkboxes and folder count fields for the new selected folder
		SetSelectedFolder();
		}

	}
	
	// =================================
	// ON ERROR CALLBACK FUNCTION
	// =================================
	function ErrorHandler(error,context) {
	
			var stackTrace = error.get_stackTrace();
			var message = error.get_message();
			var statusCode = error.get_statusCode();
			var exceptionType = error.get_exceptionType();
			var timedout = error.get_timedOut();
			var Debug = false;

			// Compose the error message...
			var ErrMsg =     
					"<strong>Stack Trace</strong><br/>" +  stackTrace + "<br/>" +
					"<strong>WebService Error</strong><br/>" + message + "<br/>" +
					"<strong>Status Code</strong><br/>" + statusCode + "<br/>" +
					"<strong>Exception Type</strong><br/>" + exceptionType + "<br/>" +
					"<strong>JS Context</strong><br/>" + context + "<br/>" +
					"<strong>Timedout</strong><br/>" + timedout + "<br/>" +
					"<strong>Source Page</strong><br/>" + "/search/MaterialGroupSearch.aspx";
			//matweb.alert(ErrMsg,"Error in WebService",null,"500");
			if(Debug){
				matweb.alert(ErrMsg,"Error in WebService",null,"500");
				//alert(ErrMsg);
			}
			else{
				ErrMsg = escape(ErrMsg);
				//matweb.alert(ErrMsg);
				//window.location.href="/errorJS.aspx?ErrMsg=" + ErrMsg;
				divUserMessage.innerHTML = "Problem retrieving folder data...Some folder features may not work.";
			}
		
	}
	
	function ShowInterpolationMessage(){
		InterpolationMsg = "The material was found via interpolation, so no data points can be displayed. See the material data sheet for actual data points.";
		matweb.alert(InterpolationMsg);
	}
	
</script>

		</td>
	</tr>
	</table>

<hr />
<a id="help"></a>
<table><tr><td>
<p/><b>Instructions:</b> This page allows you to quickly access all of the polymers/plastics, metals, ceramics, fluids, and other engineering materials
in the MatWeb material property database.&nbsp; Just select the material category you would like to find from the tree.  Click on the [+] icon to open subcategory branches in the tree.
Then click the <b>'Find'</b> button next to its box.&nbsp; You can then follow the links to the most complete data sheets on the Web, with information on over 1000 available properties, including dielectric
constant, Poisson's ratio, glass transition temperature, dissipation factor, and Vicat softening point.
</td><td><a href="https://proto3000.com"><img src="/images/assets/proto3000.jpg" border = "0"><br>

Proto3000 - Rapid Prototyping</a>
</td></tr></table>

<p/><span class="hiText">Notes:</span>

<ul>
  <li>Visit MatWeb's <a href="/search/PropertySearch.aspx">property-based search page</a> to limit your results to materials that meet specific property performance criteria.</li>
  <li>Text searches can be done from any page - use the <b>Quick Search</b> box in the top right of the page.</li>
  <li>More <a href="/help/strategy.aspx"><strong>Search Strategies</strong></a> on how to find information in MatWeb.</li>
   <li><a href="/search/GetAllMatls.aspx">Show All Materials</a> - an inefficient drill-down to data sheets.  Registered users who are logged in omit a step in this drill down process.</li>
</ul>

<p/><a href="#Top">Top</a>

<script type="text/javascript">

	var txtMatGroupText = document.getElementById("ctl00_ContentMain_txtMatGroupText");
	var txtMatGroupID = document.getElementById("ctl00_ContentMain_txtMatGroupID");
	var divMatGroupName = document.getElementById("ctl00_ContentMain_divMatGroupName");

	function ucMatGroupFinder1_OnSelected(MatGroupID,MatGroupText){
		txtMatGroupText.value = MatGroupText;
		txtMatGroupID.value = MatGroupID;
		divMatGroupName.innerHTML = MatGroupText;
		//__doPostBack("ctl00$ContentMain$btnSubmit","");
	}

	function ucMatGroupTree_OnNodeClick(NodeText,MatGroupID){
		txtMatGroupText.value = NodeText;
		txtMatGroupID.value = MatGroupID;
		divMatGroupName.innerHTML = NodeText;
		//__doPostBack("ctl00$ContentMain$btnSubmit","");
	}
	
	function validate(){
		if (txtMatGroupID.value == ""){
			matweb.alert("Please select a material category","User Input Error");
			return false;
		}
		
		return true;
		
	}

</script>


</div>
<!-- ===================================== END MAIN CONTENT ========================================== -->

<table class="tabletight" style="width:100%" >
        <tr>
                <td align="center" class="footer" colspan="3">
                        <br /><br />
                
                        <a href="/membership/regupgrade.aspx" class="footlink"><span class="subscribeLink">Subscribe to Premium Services</span></a><br/>
                
                        <span class="footer"><b>Searches:</b>&nbsp;&nbsp;</span>
                        <a href="/search/AdvancedSearch.aspx" class="footlink"><span class="foot">Advanced</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/search/CompositionSearch.aspx" class="footlink"><span class="foot">Composition</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/search/PropertySearch.aspx" class="footlink"><span class="foot">Property</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/search/MaterialGroupSearch.aspx" class="footlink"><span class="foot">Material&nbsp;Type</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/search/SearchManufacturerName.aspx" class="footlink"><span class="foot">Manufacturer</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/search/SearchTradeName.aspx" class="footlink"><span class="foot">Trade&nbsp;Name</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/search/SearchUNS.aspx" class="footlink"><span class="foot">UNS Number</span></a>
                        <br />
                        <span class="footer"><b>Other&nbsp;Links:</b>&nbsp;&nbsp;</span>
                        <a href="/services/advertising.aspx" class="footlink"><span class="foot">Advertising</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/services/submitdata.aspx" class="footlink"><span class="foot">Submit&nbsp;Data</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/services/databaselicense.aspx" class="footlink"><span class="foot">Database&nbsp;Licensing</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/services/webhosting.aspx" class="footlink"><span class="foot">Web&nbsp;Design&nbsp;&amp;&nbsp;Hosting</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/clickthrough.aspx?addataid=277" class="footlink"><span class="foot">Trade&nbsp;Publications</span></a>

                        <br />
                        <a href="/reference/suppliers.aspx" class="footlink"><span class="foot">Supplier&nbsp;List</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/tools/unitconverter.aspx" class="footlink"><span class="foot">Unit&nbsp;Converter</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/tools/contents.aspx#reference" class="footlink"><span class="foot">Reference</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/reference/link.aspx" class="footlink"><span class="foot">Links</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/help/help.aspx" class="footlink"><span class="foot">Help</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/services/contact.aspx" class="footlink"><span class="foot">Contact&nbsp;Us</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/tools/contents.aspx" class="footlink"><span class="foot" style="color:red;">Site&nbsp;Map</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/reference/faq.aspx" class="footlink"><span class="foot">FAQ</span></a>&nbsp; &#8226; &nbsp;
                        <a href="/index.aspx" class="footlink"><span class="foot">Home</span></a>
                        <br />&nbsp;
                        <div id="fb-root"></div>

<table><tr><td><script src="http://connect.facebook.net/en_US/all.js#xfbml=1"></script><fb:like-box href="https://www.facebook.com/MatWeb.LLC" width="175" show_faces="false" stream="false" header="false"></fb:like-box>

</td><td>

<a href="https://twitter.com/MatWeb" class="twitter-follow-button" data-show-count="false" data-show-screen-name="false">Follow @MatWeb</a> <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>

</td></tr></table>
                </td>
        </tr>
        <tr>
                <td colspan="3"><img src="/images/bluebar.gif" width="100%" height="5" alt="" /></td>
        </tr>
        <tr>
                <td style="background-image:url(/images/gray.gif);"><img src="/images/spacer.gif" width="5" height="1" alt=""/></td>
                <td style="background-image:url(/images/gray.gif);">
                        <span class="footer">
                        Please read our <a href="/reference/terms.aspx" class="footlink"><span class="foot">License Agreement</span></a> regarding materials data and our  <a href="/reference/privacy.aspx" class="footlink"><span class="foot">Privacy Policy</span></a>.
                        Questions or comments about MatWeb? Please contact us at
                        <a href="mailto:webmaster@matweb.com" class="footlink"><span class="foot">webmaster@matweb.com</span></a>. We appreciate your input.
                        <br /><br />
                        The contents of this web site, the MatWeb logo, and "MatWeb" are Copyright 1996-2024
                        by MatWeb, LLC. MatWeb is intended for personal, non-commercial use. The contents, results, and technical data from this site
                        may not be reproduced either electronically, photographically or substantively without permission from MatWeb, LLC.
                        </span>
                        <br />
                </td>

                <td style="background-image:url(/images/gray.gif);"><img src="/images/spacer.gif" width="5" height="1" alt=""/></td>
        </tr>
</table>




<script type="text/javascript">
//<![CDATA[
var ctl00_ContentMain_ucMatGroupTree_msTreeView_ImageArray =  new Array('', '', '', '/WebResource.axd?d=zLp3QJeZSj4-fsnL2_raV70Mk3pr-OdjL_rvrZzXUTCnZ6LU5Dg_Tc5XZg4eZpT9O0gxZJrJcxGb23XJofeGD9JhYLr2wl0VkQZnJOx0T0ShBosq0&t=638313619312541215', '/WebResource.axd?d=vFualGN2ZZ4fny_RX_gYiUTY6FBLUapne4VfY6P3sGiyrOYd-Ji2sWg_olT-exaztendUUEOoH87RS2l44bpZTscfmmv4MKRqJK8rgesmkrNfP2O0&t=638313619312541215', '/WebResource.axd?d=4nLCbQ2Yfc2i_msrQXqKzK8pLQD-GrNVZ7DeXU-La2qatczrgeCLazv0RzIkDDHyYfLFalCBO8s71lRS8yifxTChF9dUJvTLxNEeeFlghyuY0dtM0&t=638313619312541215', '/WebResource.axd?d=uEI6Enq15_q4pXO6g0E7vtAqtD5WIokAEQ2Vz8V3Yi36cefediLfyCwENkPd-w1GxTXu-HkNqcDlrYOw8bxgGqeHJtzc-PBlTa9nrZRfdIyj8fQh0&t=638313619312541215', '/WebResource.axd?d=hzeik1gGhZrfDZXUWCMsBOx5b43bIFor8UCTzm_wuh92b9Z-WQU4_fQCSUw6CaP5tnc9LTU0jl0sy4Y6Cf_c8q8HFXRqeHQuscXIqqLNSZkgl2cu0&t=638313619312541215', '/WebResource.axd?d=6v5qM6VKQYD3XNwwXEy7Tix7dzVdqDJ8QTRIlUPrzXQqNsXQgUwIfmn34ZzRORd_IZj0bF4vkZzID1HuSFE5uKyLgaPYeAYE8VT8HYfb3Rk6zsDw0&t=638313619312541215', '/WebResource.axd?d=d9nh8nEM1YCqIfE9jLzNhPyFIZqARvlFExwnFG0vbvskO-A2qORlhTyUVIxDtSwldGrOp8o9ef8lZ9SRhPQ9rv9TkGV-NVYvH9_yHT6CvWpIPF4S0&t=638313619312541215', '/WebResource.axd?d=7_V5n1KYSQsj6PoSqgIxb0egBsAa91jSXPkbmM6FaiF14-JsNnpUDnfSdxZcvfd0OQCLDtC24pMIXigO-41cL2rsq-0xea-SSG7yZUyRcliFFqe70&t=638313619312541215', '/WebResource.axd?d=TinC6igX1vPDgWh7DstWwBTHTyH4uuybDU4NW47jJD06wHdAO3YyJC_eKK1Zn5oIbwqRo4Cri8YwhA82G-LD0p14yEF1EfoIZvKleYMIuQ2K1hkE0&t=638313619312541215', '/WebResource.axd?d=4B0dvJqmzY4ukTboQq7lmxokRL9NTsoWh4yErB11Tb-ucvMw_p_0mxYVn4U1Uz6Jk795umrO0C4-B-8Yq1Kxxz6uyDT15O2BSCb8349f4YWDdIVy0&t=638313619312541215', '/WebResource.axd?d=TUiibr9hjS_MlNf09dKZECe1z56fbaPbfgJfC5eYJi0TtdBviUcA1JQJLV81aqunzpXysgruhL3ihpgd7ziL5p9SQ86xVLNiUqbYpJnW9Rj7sN1E0&t=638313619312541215', '/WebResource.axd?d=vUBrM2nNhECNbFO0AJKq52p2zWrY5XG8nWgCDL4AfsrexStb0HREDV6GHMvdkAGDInxYtTyWXuxFUIiQv3JX2fScVRwQsrVv5srHrjOIeC3ayAG20&t=638313619312541215', '/WebResource.axd?d=Z-LP5lKcsnv2A9UpExXu-5leQYIAZMYp6SXNognZi1r66YT8u24-YjJaxjisE9ruYU8NKnNz72H4shclsk8PM3S5M-VYMk_F_G5fcd3ra6Sj9CaS0&t=638313619312541215', '/WebResource.axd?d=QI4SACqRn7PxBBHPfkR7peFMAEv7lX1_iZ_7jncKwxRTlvhSAncd5K2oRqw8J8WmJjWQcbGgEblhrufy-hUjZMa1YLUC47mjfV1eS4LM3kUqLT960&t=638313619312541215', '/WebResource.axd?d=ayrFqNSzdiTAdowDVbEH-ZzKbvw5bCq7onyCSAcJhCVPVIHlUB9M_fo8BLUTeGpbtcoMiyFy1MoeEIsweMrpVandBKC4vAK-NCBcfn2FIreK8mEJ0&t=638313619312541215', '/WebResource.axd?d=OK_uWiuoGURaJ3CC0fVlQ7v9GUnIAkx03w03rQ2ENqcwdObLiQwK7B9yRFyAWj9tuUqsa0874ksgkaRo8KirB7q1ZL_huh7gCq_XehXfHMEKMiAl0&t=638313619312541215');
//]]>
</script>


<script type="text/javascript">
//<![CDATA[
(function() {var fn = function() {AjaxControlToolkit.ModalPopupBehavior.invokeViaServer('ctl00_ContentMain_ucPopupMessage1_ModalPopupExtender1', false); Sys.Application.remove_load(fn);};Sys.Application.add_load(fn);})();
var callBackFrameUrl='/WebResource.axd?d=Mt7MojvAyTqAEcx0MpKgcF_QVa09tLyMQS6c5JLNO4j2M_F6CMxYnN0HdVYxyrhggOpkTwyNqQ51iS6UoHXgJqHBROE1&t=638313619312541215';
WebForm_InitCallback();var ctl00_ContentMain_ucMatGroupTree_msTreeView_Data = new Object();
ctl00_ContentMain_ucMatGroupTree_msTreeView_Data.images = ctl00_ContentMain_ucMatGroupTree_msTreeView_ImageArray;
ctl00_ContentMain_ucMatGroupTree_msTreeView_Data.collapseToolTip = "Collapse {0}";
ctl00_ContentMain_ucMatGroupTree_msTreeView_Data.expandToolTip = "Expand {0}";
ctl00_ContentMain_ucMatGroupTree_msTreeView_Data.expandState = theForm.elements['ctl00_ContentMain_ucMatGroupTree_msTreeView_ExpandState'];
ctl00_ContentMain_ucMatGroupTree_msTreeView_Data.selectedNodeID = theForm.elements['ctl00_ContentMain_ucMatGroupTree_msTreeView_SelectedNode'];
for (var i=0;i<19;i++) {
var preLoad = new Image();
if (ctl00_ContentMain_ucMatGroupTree_msTreeView_ImageArray[i].length > 0)
preLoad.src = ctl00_ContentMain_ucMatGroupTree_msTreeView_ImageArray[i];
}
ctl00_ContentMain_ucMatGroupTree_msTreeView_Data.lastIndex = 8;
ctl00_ContentMain_ucMatGroupTree_msTreeView_Data.populateLog = theForm.elements['ctl00_ContentMain_ucMatGroupTree_msTreeView_PopulateLog'];
ctl00_ContentMain_ucMatGroupTree_msTreeView_Data.treeViewID = 'ctl00$ContentMain$ucMatGroupTree$msTreeView';
ctl00_ContentMain_ucMatGroupTree_msTreeView_Data.name = 'ctl00_ContentMain_ucMatGroupTree_msTreeView_Data';
Sys.Application.initialize();
Sys.Application.add_init(function() {
    $create(AjaxControlToolkit.ModalPopupBehavior, {"BackgroundCssClass":"modalBackground","OkControlID":"ctl00_ContentMain_ucPopupMessage1_btnOK","PopupControlID":"ctl00_ContentMain_ucPopupMessage1_divPopupMessage","PopupDragHandleControlID":"ctl00_ContentMain_ucPopupMessage1_trTitleRow","dynamicServicePath":"/search/MaterialGroupSearch.aspx","id":"ctl00_ContentMain_ucPopupMessage1_ModalPopupExtender1"}, null, null, $get("ctl00_ContentMain_ucPopupMessage1_hndPopupControl"));
});
Sys.Application.add_init(function() {
    $create(AjaxControlToolkit.TextBoxWatermarkBehavior, {"ClientStateFieldID":"ctl00_ContentMain_UcMatGroupFinder1_TextBoxWatermarkExtender2_ClientState","WatermarkCssClass":"greyOut","WatermarkText":"Type at least 4 characters here...","id":"ctl00_ContentMain_UcMatGroupFinder1_TextBoxWatermarkExtender2"}, null, null, $get("ctl00_ContentMain_UcMatGroupFinder1_txtSearchText"));
});
//]]>
</script>
</form>


<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-15290815-1");
pageTracker._setDomainName(".matweb.com");
pageTracker._trackPageview();
} catch(err) {}</script>

<!-- 20160905 insert SpecialChem javascript -->
<script type="text/javascript" src="//collect.specialchem.com/collect.js"></script>
<script type="text/javascript">
    //<![CDATA[
    var _spc = (_spc || []);
    _spc.push(['init',
    {
        partner: 'MatWeb'
        
        //, iid: '12345'
    }]);
    //]]>
</script>
<script> (function(){ var s = document.createElement('script'); var h = document.querySelector('head') || document.body; s.src = 'https://acsbapp.com/apps/app/dist/js/app.js'; s.async = true; s.onload = function(){ acsbJS.init({ statementLink : '', footerHtml : '', hideMobile : false, hideTrigger : false, disableBgProcess : false, language : 'en', position : 'right', leadColor : '#146FF8', triggerColor : '#146FF8', triggerRadius : '50%', triggerPositionX : 'right', triggerPositionY : 'bottom', triggerIcon : 'people', triggerSize : 'bottom', triggerOffsetX : 20, triggerOffsetY : 20, mobile : { triggerSize : 'small', triggerPositionX : 'right', triggerPositionY : 'bottom', triggerOffsetX : 20, triggerOffsetY : 20, triggerRadius : '20' } }); }; h.appendChild(s); })();</script>
</body>
</html>
